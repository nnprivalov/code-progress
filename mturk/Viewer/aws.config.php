<?php

return [
    'region'      => 'us-east-1',
    'version'     => 'latest',
    'credentials' => [
        'key'    => '',
        'secret' => ''
    ],
];