<?php

namespace SkyCob\Viewer\AWSAdapter;

use Aws\MTurk\Exception\MTurkException;
use Aws\MTurk\MTurkClient;

class AWSAdapter
{
    private $key;
    private $secret;

    public function __construct($key = null, $secret = null)
    {
        $this->key    = $key;
        $this->secret = $secret;
    }

    public function setIdentities($key, $secret)
    {
        $this->key    = $key;
        $this->secret = $secret;
    }

    public function hasConnection(array $config)
    {

        $config['credentials']['key']    = $this->key;
        $config['credentials']['secret'] = $this->secret;

        $mturk = new MTurkClient($config);

        try {
            $result = (bool)$mturk->getAccountBalance();
        } catch (MTurkException $e) {
            $result = false;
        }

        return $result;
    }

    public function getKeys()
    {
        $result = [
            'key'    => $this->key,
            'secret' => $this->secret
        ];

        return $result;
    }

}