<?php

namespace SkyCob\Viewer\AWSMTurk;

use Aws\MTurk\Exception\MTurkException;
use Aws\MTurk\MTurkClient;
use SkyCob\Viewer\AWSAdapter\AWSAdapter;
use SkyCob\Viewer\AWSConfig\AWSConfig;
use SkyCob\Viewer\AWSMTurk\Hit\Hit;

class AWSMTurk
{

    protected $mturk;
    protected $config;

    public function __construct(AWSConfig $config, AWSAdapter $adapter)
    {
        $configs     = $this->config = $this->transformConfig($config, $adapter);
        $this->mturk = new MTurkClient($configs);
    }

    private function transformConfig(AWSConfig $config, AWSAdapter $adapter)
    {
        $conf                = $config->getArray();
        $conf['credentials'] = $adapter->getKeys();

        return $conf;
    }

    public function getBalance()
    {
        $result = $this->mturk->getAccountBalance();

        return $result;
    }

    public function getListHits()
    {
        $result = $this->mturk->listHITs();

        return $result;
    }

    public function getListReviewableHits()
    {
        $result = $this->mturk->listReviewableHITs();

        return $result;
    }

    public function getAnswerToHit($hitId)
    {
        try {
            $hit = $this->mturk->getHIT(['HITId' => $hitId]);
        } catch (MTurkException $e) {
            return null;
        }

        $status = $hit['HIT']['HITStatus'];

        $result = null;

        if ($status === 'Reviewable') {
            $assignment = $this->getAssignmentsForHit($hitId);

            $result = [
                'Status'     => $status,
                'Answer'     => $assignment['Assignments'][0]['Answer'],
                'SubmitTime' => $assignment['Assignments'][0]['SubmitTime']
            ];
            /*
            $xml = simplexml_load_string($assignment['Assignments'][0]['Answer']);
            $json = json_encode($xml);
            $array = json_decode($json, TRUE);

            $result = $array['Answer']['FreeText'];
            */
        }

        return $result;
    }

    /**
     * @return array|bool
     */
    public function getAnswerToReviewableHits()
    {
        $reviewableHitsId = $this->getListReviewableHits()['HITs'];
        if (count($reviewableHitsId) > 0) {
            foreach ($reviewableHitsId as $hit) {
                $current = $this->getAssignmentsForHit($hit['HITId'])['Assignments'];
                foreach ($current as $assignment) {
                    $xml                 = simplexml_load_string($assignment['Answer']);
                    $json                = json_encode($xml);
                    $array               = json_decode($json, true);
                    $hits[$hit['HITId']] = $array['Answer']['FreeText'];
                }
            }
        } else {
            $hits = false;
        }

        return $hits;
    }

    public function approveAssignmentReviewableHits()
    {
        $reviewable = $this->getListReviewableHits()['HITs'];
        foreach ($reviewable as $hit) {
            $current = $this->getAssignmentsForHit($hit['HITId'])['Assignments'];
            foreach ($current as $assignment) {
                $assignmentId = $assignment['AssignmentId'];
                $this->mturk->approveAssignment(['AssignmentId' => $assignmentId]);
            }
        }
    }

    public function approveAssignment($hitId)
    {
        $assignments = $this->getAssignmentsForHit($hitId);
        $numResults  = $assignments['NumResults'];
        $status      = $assignments['Assignments'][0]['AssignmentStatus'];

        if ($numResults < 1) {
            return false;
        }

        if ($status === 'Approved') {
            return false;
        }

        $assignmentId = $this->getAssignmentsForHit($hitId)['Assignments'][0]['AssignmentId'];

        $this->mturk->approveAssignment(['AssignmentId' => $assignmentId]);

        return true;
    }

    public function deleteReviewableHits()
    {
        $reviewable = $this->getListReviewableHits()['HITs'];
        foreach ($reviewable as $hit) {
            $this->mturk->deleteHIT(['HITId' => $hit['HITId']]);
        }
    }

    public function deleteHit($hitId)
    {
        $assignments = $this->getAssignmentsForHit($hitId);
        $status      = $assignments['Assignments'][0]['AssignmentStatus'];

        if ($status !== 'Approved') {
            return false;
        }

        try {
            $this->mturk->deleteHIT(['HITId' => $hitId]);
        } catch (MTurkException $e) {
            return false;
        }

        return true;
    }

    public function getHit($id)
    {
        $result = $this->mturk->getHIT(['HITId' => $id]);

        return $result;
    }

    public function getAssignmentsForHit($hitId)
    {
        $result = $this->mturk->listAssignmentsForHIT(['HITId' => $hitId]);

        return $result;
    }

    public function rejectAssignment($id, $message = 'Bad response')
    {
        try {
            $result = $this->mturk->rejectAssignment(['AssignmentId' => $id, 'RequesterFeedback' => $message]);
        } catch (MTurkException $e) {
            $log = 'RejectAssignments: ' . $e;
            file_put_contents(LOGS_PATH . 'log.txt', $log);
        }

        return $result;
    }

    public function rejectAssignmentByHitId($id)
    {
        $assignments  = $this->getAssignmentsForHit($id);
        $assignmentId = $assignments['Assignments'][0]['AssignmentId'];
        if ($assignments['Assignments'][0]['AssignmentStatus'] == 'Submitted') {
            $this->rejectAssignment($assignmentId);
        }
    }

    public function updateHitReviewStatus($hitId, $revert = false)
    {
        $this->mturk->updateHITReviewStatus(['HITId' => $hitId, 'Revert' => $revert]);
    }

    public function createHitUsingLayout(array $args)
    {
        if (!isset($args['layoutParameter'])) {
            return false;
        }

        $hit = new Hit();

        $params = [
            [
                'Name'  => 'channel_link',
                'Value' => $args['layoutParameter']
            ]
        ];

        $result = $hit->layoutParameters($params)
            ->submit($this->mturk)['HIT'];

        return $result;
    }


}