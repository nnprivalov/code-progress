<?php

namespace SkyCob\Viewer\AWSMTurk\Hit;

use Aws\MTurk\Exception\MTurkException;
use Aws\MTurk\MTurkClient;

class Hit
{
    protected $defaultLayoutId = '3W59D418LGJ19Y4IS67JYBI254KCKG';

    protected $title;
    protected $description;
    protected $question;
    protected $layoutId;
    protected $layoutParameters;
    protected $reward;
    protected $assignmentDuration;
    protected $lifeTime;
    protected $keywords;
    protected $maxAssignments;
    protected $autoApprovalDelayInSeconds;
    protected $qualificationRequirements;
    protected $assignmentReviewPolicy;
    protected $reviewPolicy;
    protected $requesterAnnotation;
    protected $uniqueRequestToken;

    public function __construct()
    {
        $this->setDefault();
    }

    public function title($title)
    {
        $this->title = $title;

        return $this;
    }

    public function description($description)
    {
        $this->description = $description;

        return $this;
    }

    public function question($question)
    {
        $this->question = $question;

        return $this;
    }

    public function layoutId($layoutId)
    {
        $this->layoutId = $layoutId;

        return $this;
    }

    public function layoutParameters($layoutParameters)
    {
        $this->layoutParameters = $layoutParameters;

        return $this;
    }

    public function reward($reward)
    {
        $this->reward = $reward;

        return $this;
    }

    public function assignmentDuration($assignmentDuration)
    {
        $this->assignmentDuration = $assignmentDuration;

        return $this;
    }

    public function lifeTime($lifeTime)
    {
        $this->lifeTime = $lifeTime;

        return $this;
    }

    public function keywords($keywords)
    {
        $this->keywords = $keywords;

        return $this;
    }

    public function maxAssignments($maxAssignments)
    {
        $this->maxAssignments = $maxAssignments;

        return $this;
    }

    public function autoApprovalDelayInSeconds($autoApprovalDelayInSeconds)
    {
        $this->autoApprovalDelayInSeconds = $autoApprovalDelayInSeconds;

        return $this;
    }

    public function qualificationRequirements($qualificationRequirements)
    {
        $this->qualificationRequirements = $qualificationRequirements;

        return $this;
    }

    public function assignmentReviewPolicy($assignmentReviewPolicy)
    {
        $this->assignmentReviewPolicy = $assignmentReviewPolicy;

        return $this;
    }

    public function reviewPolicy($reviewPolicy)
    {
        $this->reviewPolicy = $reviewPolicy;

        return $this;
    }

    public function requesterAnnotation($requesterAnnotation)
    {
        $this->requesterAnnotation = $requesterAnnotation;

        return $this;
    }

    public function uniqueRequestToken($uniqueRequestToken)
    {
        $this->uniqueRequestToken = $uniqueRequestToken;

        return $this;
    }

    public function setDefault()
    {
        $params = [
            'title'              => 'The email address of the YouTube channel',
            'description'        => 'We need an email address for the YouTube channel: ',
            'reward'             => '0.01',
            'assignmentDuration' => 60 * 5,
            'lifeTime'           => 60 * 60 * 24 * 3,
            'keywords'           => 'youtube, channel, email, address',
            'maxAssignments'     => 1,
            'layoutId'           => $this->defaultLayoutId
        ];

        foreach ($params as $key => $value) {
            $this->$key = $value;
        }

        return $this;
    }

    public function getArray()
    {
        $params = [
            'Title'                       => $this->title,
            'Description'                 => $this->description,
            'HITLayoutId'                 => $this->layoutId,
            'HITLayoutParameters'         => $this->layoutParameters,
            'Reward'                      => $this->reward,
            'AssignmentDurationInSeconds' => $this->assignmentDuration,
            'LifetimeInSeconds'           => $this->lifeTime,
            'Keywords'                    => $this->keywords,
            'MaxAssignments'              => $this->maxAssignments
            //'AutoApprovalDelayInSeconds' => '',
            //'QualificationRequirements' => '',
            //'AssigmentReviewPolicy' => '',
            //'HITReviewPolicy' => '',
            //'RequesterAnnotation' => '',
            //'UniqueRequestToken' => ''
        ];

        return $params;
    }

    public function submit(MTurkClient $mturk)
    {
        try {
            $result = $mturk->createHIT($this->getArray());

            return $result;
        } catch (MTurkException $e) {
            return false;
        }
    }

}