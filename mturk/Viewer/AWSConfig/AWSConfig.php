<?php

namespace SkyCob\Viewer\AWSConfig;

class AWSConfig
{

    protected $region;
    protected $version;
    protected $endpoint;

    public function __construct(array $args = [])
    {

        $this->setConfigs($args);
    }

    public function setConfigs(array $args)
    {

        $this->region   = isset($args['region']) ? $args['region'] : null;
        $this->version  = isset($args['version']) ? $args['version'] : null;
        $this->endpoint = isset($args['endpoint']) ? $args['endpoint'] : null;
    }

    public function getArray()
    {

        $result = [
            'region'   => $this->region,
            'version'  => $this->version,
            'endpoint' => $this->endpoint
        ];

        return $result;
    }

    public function set($name, $value)
    {

        $this->$name = $value;

        return $this;
    }

    public function get($name)
    {

        $result = isset($this->$name) ? $this->$name : (bool)false;

        return $result;
    }

}