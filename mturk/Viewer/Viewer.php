<?php

namespace SkyCob\Viewer;

use SkyCob\MturkHelper;
use SkyCob\Viewer\AWSAdapter\AWSAdapter;
use SkyCob\Viewer\AWSConfig\AWSConfig;
use SkyCob\Viewer\AWSMTurk\AWSMTurk;

class Viewer
{
    protected $adapter;
    protected $config;
    protected $helper;

    public function send(array $listParameters)
    { // {link_1, link_2, ... link_n}
        if (empty($listParameters)) {
            return false;
        }

        $mturk = $this->getMTurk();

        foreach ($listParameters as $param) {
            $result[$param] = $mturk->createHitUsingLayout(['layoutParameter' => $param]);
        }

        return $result;
    }

    public function check(array $hitIds)
    { // {hitId_1, hitId_2, ... hitId_n}
        foreach ($hitIds as $hit) {
            $current = $this->checkOne($hit);

            if ($current != 'Rejected') {
                $result[$hit] = $current;
            }
        }

        return $result;
    }

    public function checkOne($hitId)
    { // {link_1, link_2, ... link_n}
        $mturk  = $this->getMTurk();
        $helper = $this->helper;

        $answer = $mturk->getAnswerToHit($hitId);

        if ($answer['Status'] == 'Reviewable') {
            $text = $helper->parseResponseFromXml($answer['Answer']);
            if ($helper->validateResponse($text)) {
                $result = $answer;
                $mturk->approveAssignment($hitId);
                $mturk->deleteHit($hitId);
            } else {
                $mturk->rejectAssignmentByHitId($hitId);
                $param = [];
                //$mturk->createHitUsingLayout(['layoutParameter' => $param]);
                $result = 'Rejected';
            }
        }

        return $result;
    }

    /**
     * return array of answers to reviewable HITs
     * return null if count of reviewable HITs < 1
     *
     * @return array|null
     */
    public function checkAll()
    {
        $mturk      = $this->getMTurk();
        $reviewable = $mturk->getListReviewableHits();
        $numResults = $reviewable['NumResults'];

        if ($numResults > 0) {
            $answers = $mturk->getAnswerToReviewableHits();
            $mturk->approveAssignmentReviewableHits();
            $mturk->deleteReviewableHits();

            return $answers;
        }

        return null;

    }

    public function __construct()
    {
        $this->init();
        $this->config  = new AWSConfig();
        $this->adapter = new AWSAdapter();
        $this->helper  = new MturkHelper();
    }

    public function getConfig()
    {
        return $this->config;
    }

    public function setConfig(AWSConfig $config)
    {
        $this->config = $config;
    }

    public function getAdapter()
    {
        return $this->adapter;
    }

    public function setAdapter(AWSAdapter $adapter)
    {
        $this->adapter = $adapter;
    }

    public function getMTurk()
    {
        $result = new AWSMTurk($this->config, $this->adapter);

        return $result;
    }

    private function init()
    {
        require '../vendor/autoload.php';
        require '../lib/Viewer/autoload.php';
    }


}