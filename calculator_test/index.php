<?php 

define('ABSPATH', __DIR__ . DIRECTORY_SEPARATOR);

spl_autoload_register(function ($class) {
    $parts = explode('\\', $class);
    @include __DIR__ . DIRECTORY_SEPARATOR . 'Lib' . DIRECTORY_SEPARATOR . end($parts) . '.php';
	@include __DIR__ . DIRECTORY_SEPARATOR . 'Lib' . DIRECTORY_SEPARATOR . 'Model' . DIRECTORY_SEPARATOR . end($parts) . '.php';
	@include __DIR__ . DIRECTORY_SEPARATOR . 'Lib' . DIRECTORY_SEPARATOR . 'Model' . DIRECTORY_SEPARATOR . 'CustomDecorators' . DIRECTORY_SEPARATOR . end($parts) . '.php';
	@include __DIR__ . DIRECTORY_SEPARATOR . 'Lib' . DIRECTORY_SEPARATOR . 'Model' . DIRECTORY_SEPARATOR . 'Operation' . DIRECTORY_SEPARATOR . end($parts) . '.php';
	@include __DIR__ . DIRECTORY_SEPARATOR . 'Lib' . DIRECTORY_SEPARATOR . 'Model' . DIRECTORY_SEPARATOR . 'Operation' . DIRECTORY_SEPARATOR . 'Exception' . DIRECTORY_SEPARATOR . end($parts) . '.php';
	@include __DIR__ . DIRECTORY_SEPARATOR . 'Lib' . DIRECTORY_SEPARATOR . 'Model' . DIRECTORY_SEPARATOR . 'Factory' . DIRECTORY_SEPARATOR . end($parts) . '.php';
	@include __DIR__ . DIRECTORY_SEPARATOR . 'Lib' . DIRECTORY_SEPARATOR . 'Model' . DIRECTORY_SEPARATOR . 'FactoryToFactory' . DIRECTORY_SEPARATOR . end($parts) . '.php';
	@include __DIR__ . DIRECTORY_SEPARATOR . 'Api' . DIRECTORY_SEPARATOR . end($parts) . '.php';
	@include __DIR__ . DIRECTORY_SEPARATOR . 'Api' . DIRECTORY_SEPARATOR . 'Data' . DIRECTORY_SEPARATOR . end($parts) . '.php';
	@include __DIR__ . DIRECTORY_SEPARATOR . 'Api' . DIRECTORY_SEPARATOR . 'Data' . DIRECTORY_SEPARATOR . 'Factory' . DIRECTORY_SEPARATOR . end($parts) . '.php';
	@include __DIR__ . DIRECTORY_SEPARATOR . 'Api' . DIRECTORY_SEPARATOR . 'Handler' . DIRECTORY_SEPARATOR . end($parts) . '.php';
});

$commandsStart = ['s', 'start', '1'];
$commandsExit = ['q', 'exit'];
$inputDataRequest = [
	'operandFirst' => 'First Operand',
	'operation' => 'Operation',
	'operandSecond' => 'Second Operand',
];
$inputData = [];
echo "Hello!".PHP_EOL;
echo "Enter command (start/exit):".PHP_EOL;
$command = readline();
if (in_array($command, $commandsExit)) {
	echo "Byeee";
	exit();
}
if (in_array($command, $commandsStart)) {
	echo "Lets calculate something!".PHP_EOL;
	foreach ($inputDataRequest as $var => $label) {
		echo $label.PHP_EOL;
		$inputData[$var] = readline();
	}
}
$factoryToFactory = new Lib\Model\FactoryToFactory();
$result = $factoryToFactory->run($inputData);
echo $result;

