<?php

/** 
	Use to map operation by symbol and set operation execution class.
	<"symbol" => "class">
*/
return [
	'+' => 'Lib\Model\Operation\OperationSum',
	'-' => 'Lib\Model\Operation\OperationDifference',
	'*' => 'Lib\Model\Operation\OperationMultiplication', 
	'/' => 'Lib\Model\Operation\OperationDivision', 

];









?>