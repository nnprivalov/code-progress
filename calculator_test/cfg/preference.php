<?php

/** 
	Use in factories.
	<"interface" => "class">
*/
return [
	'Api\Handler\HandlerInterface' => 'Lib\Handler',
	'Api\Data\OperandInterface' => 'Lib\Model\Operand',
	'Api\PullOperandsInterface' => 'Lib\Model\PullOperands',
	'Api\Data\CalcResultInterface' => 'Lib\Model\CalcResult',
	//'Api\Data\CalcResultDecoratorInterface' => 'Lib\Model\CustomDecorators\CustomResultDecorator',
	'Api\Data\CalcResultDecoratorInterface' => 'Lib\Model\CalcResultDecorator', 

];









?>