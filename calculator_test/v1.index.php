<?php 

define('ABSPATH', __DIR__ . DIRECTORY_SEPARATOR);

spl_autoload_register(function ($class) {
    $parts = explode('\\', $class);
    @include __DIR__ . DIRECTORY_SEPARATOR . 'Lib' . DIRECTORY_SEPARATOR . end($parts) . '.php';
	@include __DIR__ . DIRECTORY_SEPARATOR . 'Lib' . DIRECTORY_SEPARATOR . 'Model' . DIRECTORY_SEPARATOR . end($parts) . '.php';
	@include __DIR__ . DIRECTORY_SEPARATOR . 'Lib' . DIRECTORY_SEPARATOR . 'Model' . DIRECTORY_SEPARATOR . 'CustomDecorators' . DIRECTORY_SEPARATOR . end($parts) . '.php';
	@include __DIR__ . DIRECTORY_SEPARATOR . 'Lib' . DIRECTORY_SEPARATOR . 'Model' . DIRECTORY_SEPARATOR . 'Operation' . DIRECTORY_SEPARATOR . end($parts) . '.php';
	@include __DIR__ . DIRECTORY_SEPARATOR . 'Lib' . DIRECTORY_SEPARATOR . 'Model' . DIRECTORY_SEPARATOR . 'Operation' . DIRECTORY_SEPARATOR . 'Exception' . DIRECTORY_SEPARATOR . end($parts) . '.php';
	@include __DIR__ . DIRECTORY_SEPARATOR . 'Lib' . DIRECTORY_SEPARATOR . 'Model' . DIRECTORY_SEPARATOR . 'Factory' . DIRECTORY_SEPARATOR . end($parts) . '.php';
	@include __DIR__ . DIRECTORY_SEPARATOR . 'Api' . DIRECTORY_SEPARATOR . end($parts) . '.php';
	@include __DIR__ . DIRECTORY_SEPARATOR . 'Api' . DIRECTORY_SEPARATOR . 'Data' . DIRECTORY_SEPARATOR . end($parts) . '.php';
	@include __DIR__ . DIRECTORY_SEPARATOR . 'Api' . DIRECTORY_SEPARATOR . 'Data' . DIRECTORY_SEPARATOR . 'Factory' . DIRECTORY_SEPARATOR . end($parts) . '.php';
	@include __DIR__ . DIRECTORY_SEPARATOR . 'Api' . DIRECTORY_SEPARATOR . 'Handler' . DIRECTORY_SEPARATOR . end($parts) . '.php';
});
$LINE_BRAKER = php_sapi_name() == 'cli' ? PHP_EOL : "<br>";
//$LINE_BRAKER = PHP_EOL;

// INIT TO TEST IT WORKS

$config = new Lib\Config();

$operandFirst = new Lib\Model\Operand(6.66);
$operandSecond = new Lib\Model\Operand(3.33);

$pullOperands = new Lib\Model\PullOperands(
	$operandFirst,
	$operandSecond
);

$operation = new Lib\Model\Operation\OperationSum();
//$operation = new Lib\Model\Operation\OperationDifference();
//$operation = new Lib\Model\Operation\OperationMultiplication();
//$operation = new Lib\Model\Operation\OperationDivision();

$executor = new Lib\Executor(
	$pullOperands,
	$operation
);

$decoratorFactory = new Lib\Model\Factory\CalcResultDecoratorFactory($config);

$calcResultFactory = new Lib\Model\Factory\CalcResultFactory(
	$decoratorFactory,
	$config
); 

$handler = new Lib\Handler(
	$executor,
	$calcResultFactory
);

$calculator = new Lib\Calculator($handler);

//////////// RUN CALCULATING ////////////////

$result = $calculator->calculate();
echo $result->print();
echo $LINE_BRAKER;

// CHANGE FIRST OPERAND TO PREVIOUS RESULT (obj 'result' can be 'operand' because CalcResult implements OperandInterface)
$pullOperands->set($result, $operandSecond);
$result = $calculator->calculate();
echo $result->print();
echo $LINE_BRAKER;

// CHANGE OPERATION TO '*' AND EXECUTE IT ON PREVIOUS RESULT
$pullOperands->set($result, $operandSecond);
$executor->setOperation(new Lib\Model\Operation\OperationMultiplication());
$result = $calculator->calculate();
echo $result->print();
echo $LINE_BRAKER;

// CHANGE OPERATION TO '-' AND EXECUTE IT ON PREVIOUS RESULT
$pullOperands->set($result, $operandSecond);
$executor->setOperation(new Lib\Model\Operation\OperationDifference());
$result = $calculator->calculate();
echo $result->print();
echo $LINE_BRAKER;

// CHANGE OPERATION TO '/' AND EXECUTE IT ON PREVIOUS RESULT
$pullOperands->set($result, $operandSecond);
$executor->setOperation(new Lib\Model\Operation\OperationDivision());
$result = $calculator->calculate();
echo $result->print();
echo $LINE_BRAKER;


