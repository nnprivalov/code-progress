<?php

namespace Api;

use Api\Reflector;
use Api\Handler\HandlerInterface;
use Api\Data\CalcResultInterface;

interface CalculatorInterface
{
	public function __construct(
		HandlerInterface $handler
	);
	public function calculate(): CalcResultInterface;
}