<?php

namespace Api;

use Api\Data\OperandInterface;
use Api\Data\OperationInterface;
use Api\CalculatorInterface;

interface Reflector
{	
	public function __construct(CalculatorInterface $calculator);
	public function getOperand(): OperandInterface;
	public function getOperation(): OperationInterface;
}