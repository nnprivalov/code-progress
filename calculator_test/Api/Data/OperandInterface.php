<?php

namespace Api\Data;

interface OperandInterface
{
	public function set(float $value): void;
	public function get(): float;
	public function __invoke(): float;
	public function __toString(): string;
}