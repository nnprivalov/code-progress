<?php

namespace Api\Data;

interface CalcResultInterface
{
	public function set(float $value = null): void;
	public function get(): float;
	public function __toString(): string;
}