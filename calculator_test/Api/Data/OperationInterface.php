<?php

namespace Api\Data;

use Api\Data\OperandInterface;

interface OperationInterface
{	
	public function get(): string;
	public function execute(OperandInterface $operandFirst, OperandInterface $operandSecond): float;
}