<?php

namespace Api\Data;

use Api\Data\OperationInterface;
use Api\PullOperandsInterface;

interface CalcResultDecoratorInterface
{
	public function __construct(
		OperationInterface $operation,
		PullOperandsInterface $pullOperands,
		float $resultValue
	);

	public function __toString(): string;
}