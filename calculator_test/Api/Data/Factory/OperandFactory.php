<?php 

namespace Api\Data\Factory;

use Api\Data\OperandInterface;

interface OperandFactory
{
	public function create(float $value = null): OperandInterface;
}