<?php 

namespace Api\Data\Factory;

interface Factory
{
	public function create(array $data = []): Object;
}