<?php 

namespace Api\Data\Factory;

use Api\Data\CalcResultInterface;
use Api\Data\OperationInterface;
use Api\PullOperandsInterface;

interface CalcResultFactoryInterface
{
	public function create(float $value): CalcResultInterface;
}