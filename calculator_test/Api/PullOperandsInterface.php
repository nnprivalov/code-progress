<?php 

namespace Api;

use Api\Data\OperandInterface;

interface PullOperandsInterface
{
	public function __construct(
		OperandInterface $operandFirst,
		OperandInterface $operandSecond
	);
	
	public function get(): array;
}