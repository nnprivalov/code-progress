<?php

namespace Api\Handler;

use Api\Data\OperationInterface;
use Api\PullOperandsInterface;

interface ExecutorInterface
{
	public function __construct(
		PullOperandsInterface $pullOperands,
		OperationInterface $operation
	);
	
	public function __invoke(): float;
	
	public function execute(): float;
}