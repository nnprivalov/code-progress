<?php

namespace Api\Handler;

use Api\Handler\ExecutorInterface;
use Api\Data\CalcResultInterface;
use Api\Data\Factory\CalcResultFactoryInterface;

interface HandlerInterface
{
	public function __construct(
		ExecutorInterface $executor,
		CalcResultFactoryInterface $calcResultFactory
	);
	
	public function getResult(): CalcResultInterface;
}