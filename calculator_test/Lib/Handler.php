<?php

namespace Lib;

use Api\Handler\HandlerInterface;
use Api\Handler\ExecutorInterface;
use Api\Data\CalcResultInterface;
use Api\Data\Factory\CalcResultFactoryInterface;

class Handler implements HandlerInterface
{
	/** ExecutorInterface */
	protected $executor;
	
	/** CalcResultFactory */
	protected $calcResultFactory;
	
	public function __construct(
		ExecutorInterface $executor,
		CalcResultFactoryInterface $calcResultFactory
	){
		$this->executor = $executor;
		$this->calcResultFactory = $calcResultFactory;
	}
	
	public function getResult(): CalcResultInterface
	{
		$executor = $this->executor;
		$executorResult = $executor();
		
		return $this->calcResultFactory->create($executorResult);
	}
	
}