<?php

namespace Lib;

use Api\Data\OperationInterface;
use Api\PullOperandsInterface;
use Api\Handler\ExecutorInterface;

class Executor implements ExecutorInterface
{
	/** PullOperandsInterface */
	protected $pull;
	/** OperationInterface */
	protected $operation;
	
	public function __construct(
		PullOperandsInterface $pullOperands,
		OperationInterface $operation
	){
		$this->pull = $pullOperands;
		$this->operation = $operation;
	}
	
	public function __invoke(): float
	{
		$result = $this->execute();
		
		return $result;
	}
	
	public function execute(): float
	{
		$result = null;
		//TODO: change to use array_walk/array_map
		foreach ($this->pull->get() as $operand) {
			if (!isset($result)) {
				$result = $operand;
				continue;
			}
			$result = $this->operation->execute($result, $operand);
		}
		
		return $result;
	}

	public function setOperation(OperationInterface $operation): void
	{
		$this->operation = $operation;
	}

	public function getOperation(): OperationInterface
	{
		return $this->operation;
	}

	public function getPullOperands(): PullOperandsInterface
	{
		return $this->pull;
	}
}