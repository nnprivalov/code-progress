<?php

namespace Lib\Model\Factory;

use Lib\Config;

class ConfigFactory
{
	/** Config */
	protected $instance;
	
	public function create(): Config
	{
		$this->instance = new Config();
		
		return $this->instance;
	}
}