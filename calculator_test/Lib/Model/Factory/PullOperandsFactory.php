<?php

namespace Lib\Model\Factory;

use Api\PullOperandsInterface;
use Lib\Model\PullOperands;
use Lib\Config;

class PullOperandsFactory
{
	const INSTANCE_INTERFACE = PullOperandsInterface::class;
	const INSTANCE_DEFAULT = PullOperands::class;
	/** PullOperandsInterface */
	protected $instance;
	/** Config */
	protected $config;

	public function __construct(
		Config $config
	){
		$this->config = $config;
	}	
	
	public function create(array $operands = []): PullOperandsInterface
	{
		$instanceClass = $this->config->getPreferenceFor(self::INSTANCE_INTERFACE, self::INSTANCE_DEFAULT);

		$this->instance = new $instanceClass(array_shift($operands), array_shift($operands));
		
		return $this->instance;
	}
}