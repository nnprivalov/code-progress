<?php
namespace Lib\Model\Factory;

use Api\Data\OperandInterface;
use Lib\Model\Operand;

class OperandFactory
{
	const INSTANCE_INTERFACE = OperandInterface::class;
	const INSTANCE_DEFAULT = Operand::class;
	/** OperandInterface */
	protected $instance;
	/** Config */
	protected $config;

	public function __construct(
		Config $config
	){
		$this->config = $config;
	}	

	public function create(float $value = 0): OperandInterface
	{
		$instanceClass = $this->config->getPreferenceFor(self::INSTANCE_INTERFACE, self::INSTANCE_DEFAULT);

		$this->instance = new $instanceClass($value);
		
		return $this->instance;
	}
}