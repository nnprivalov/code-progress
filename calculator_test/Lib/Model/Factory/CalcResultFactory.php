<?php

namespace Lib\Model\Factory;

use Api\Data\Factory\CalcResultFactoryInterface;
use Api\Data\CalcResultInterface;
use Api\Data\OperationInterface;
use Api\PullOperandsInterface;
use Lib\Model\CalcResult;
use Lib\Model\Factory\CalcResultDecoratorFactory;
use Lib\Config;

class CalcResultFactory implements CalcResultFactoryInterface
{
	const INSTANCE_INTERFACE = CalcResultInterface::class;
	const INSTANCE_DEFAULT = CalcResult::class;
	/** CalcResultInterface */
	protected $instance;
	/** CalcResultDecoratorFactory */
	protected $decoratorFactory;
	/** Config */
	protected $config;

	public function __construct(
		CalcResultDecoratorFactory $decoratorFactory,
		Config $config
	){
		$this->decoratorFactory = $decoratorFactory;
		$this->config = $config;
	}
	
	public function create(float $value): CalcResultInterface
	{
		$decorator = $this->decoratorFactory->create($operation, $pullOperands, $value);

		$instanceClass = $this->config->getPreferenceFor(self::INSTANCE_INTERFACE, self::INSTANCE_DEFAULT);

		$this->instance = new $instanceClass($decorator);
		
		//$this->instance = new CalcResult($decorator);
		if (isset($value)) {
			$this->instance->set($value);
		}
		
		return $this->instance;
	}
}