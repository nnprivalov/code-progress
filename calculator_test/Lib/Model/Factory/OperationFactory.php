<?php

namespace Lib\Model\Factory;

use Api\Data\OperationInterface;
use Lib\Config;
use Lib\Model\Operation\Exception\OperationMissingException;

class OperationFactory
{
	/** OperationInterface */
	protected $instance;
	/** Config */
	protected $config;
	
	public function __construct(
		Config $config
	){
		$this->config = $config;
	}

	public function create(string $type): OperationInterface
	{
		$instanceClass = $this->config->getOperationExecutableClass($type);
		if (null === $instanceClass) {
			throw new OperationMissingException('Operation not exists: '.$type);
		}
		$this->instance = new $instanceClass();
		
		return $this->instance;
	}
}