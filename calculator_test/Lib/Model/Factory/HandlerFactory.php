<?php

namespace Lib\Model\Factory;

use Api\Handler\HandlerInterface;
use Api\Handler\ExecutorInterface;
use Api\Data\Factory\CalcResultFactoryInterface;
use Lib\Handler;

class HandlerFactory
{
	const INSTANCE_INTERFACE = HandlerInterface::class;
	const INSTANCE_DEFAULT = Handler::class;
	/** HandlerInterface */
	protected $instance;
	/** Config */
	protected $config;

	public function __construct(
		Config $config
	){
		$this->config = $config;
	}
	
	public function create(ExecutorInterface $executor, CalcResultFactoryInterface $resultFactory): HandlerInterface
	{
		$instanceClass = $this->config->getPreferenceFor(self::INSTANCE_INTERFACE, self::INSTANCE_DEFAULT);

		$this->instance = new $instanceClass($executor, $resultFactory);
		
		return $this->instance;
	}
}