<?php
namespace Lib\Model\Factory;

use Api\Data\CalcResultDecoratorInterface;
use Api\Data\OperationInterface;
use Api\PullOperandsInterface;
use Lib\Model\CalcResultDecorator;
use Lib\Config;

class CalcResultDecoratorFactory
{
	const INSTANCE_INTERFACE = CalcResultDecoratorInterface::class;
	const INSTANCE_DEFAULT = CalcResultDecorator::class;
	/** CalcResultDecoratorInterface */
	protected $instance;
	/** Config */
	protected $config;

	public function __construct(
		Config $config
	){
		$this->config = $config;
	}
	
	public function create(OperationInterface $operation, PullOperandsInterface $pullOperands, float $value = null): CalcResultDecoratorInterface
	{
		$instanceClass = $this->config->getPreferenceFor(self::INSTANCE_INTERFACE, self::INSTANCE_DEFAULT);

		$this->instance = new $instanceClass($operation, $pullOperands, $value);

		return $this->instance;
	}
}