<?php

namespace Lib\Model\Factory;

use Api\Handler\ExecutorInterface;
use Api\PullOperandsInterface;
use Api\Data\OperationInterface;
use Lib\Executor;

class ExecutorFactory
{
	/** ExecutorInterface */
	protected $instance;
	
	public function create(PullOperandsInterface $pull, OperationInterface $operation): ExecutorInterface
	{
		$this->instance = new Executor($pull, $operation);
		
		return $this->instance;
	}
}