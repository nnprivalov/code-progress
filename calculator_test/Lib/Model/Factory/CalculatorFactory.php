<?php

namespace Lib\Model\Factory;

use Api\CalculatorInterface;
use Api\Handler\HandlerInterface;
use Lib\Calculator;

class CalculatorFactory
{
	/** CalculatorInterface */
	protected $instance;
	
	public function create(HandlerInterface $handler): CalculatorInterface
	{
		$this->instance = new Calculator($handler);
		
		return $this->instance;
	}

}