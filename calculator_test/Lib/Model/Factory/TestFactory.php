<?php

namespace Lib\Model\Factory;

use Api\CalculatorInterface;
use Api\Data\Factory\Factory as FactoryInterface;

class TestFactory implements FactoryInterface
{
	/** CalculatorInterface */
	protected $instance;
	/** HandlerFactory */
	protected $handlerFactory;
	/** ExecutorFactory */
	protected $executorFactory;
	/** PullOperandsFactory */
	protected $pullOperandsFactory;
	/** OperationFactory */
	protected $operationFactory;
	/** OperandFactory */
	protected $operandFactory;
	/** CalcResultFactory */
	protected $calcResultFactory;

	public function __construct(
		HandlerFactory $handlerFactory,
		ExecutorFactory $executorFactory,
		PullOperandsFactory $pullOperandsFactory,
		OperationFactory $operationFactory,
		OperandFactory $operandFactory,
		CalcResultFactory $calcResultFactory
	){
		$this->handlerFactory = $handlerFactory;
		$this->executorFactory = $executorFactory;
		$this->pullOperandsFactory = $pullOperandsFactory;
		$this->operationFactory = $operationFactory;
		$this->operandFactory = $operandFactory;
		$this->calcResultFactory = $calcResultFactory;
	}

	public function create(array $data = []): CalculatorInterface
	{
		/**
		$executor = $this->executorFactory()->create();

		$handler = $this->handlerFactory()->create(
			$executor
			$this->calcResultFactory
		);

		$this->instance = new CalculatorInterface($handler);

		if (isset($value)) {
			$this->instance->set($value);
		}
		*/
		return $this->instance;
	}
	
	public function get(): CalculatorInterface
	{
		if (!isset($this->instance)) {
			$this->create();
		}
		
		return $this->instance;
	}
}