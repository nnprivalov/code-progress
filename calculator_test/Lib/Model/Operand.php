<?php

namespace Lib\Model;

use Api\Data\OperandInterface;

class Operand implements OperandInterface
{
	/** float */
	protected $value;
	
	public function __construct(
		float $value
	){
		$this->value = $value;
	}
	
	public function set(float $value): void
	{
		$this->value = $value;
	}
	
	public function get(): float
	{
		return $this->value;
	}
	
	public function __invoke(): float
	{
		return $this->get();
	}

	public function __toString(): string
	{
		return strval($this->value);
	}

}
	