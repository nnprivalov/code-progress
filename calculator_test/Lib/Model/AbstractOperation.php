<?php

namespace Lib\Model;

use Api\Data\OperationInterface;
use Api\Data\OperandInterface;
use Lib\Model\Operation\Exception\OperationMissingException;

abstract class AbstractOperation implements OperationInterface
{
	const OPERATION = null;

	public function get(): string
	{
		if ($this::OPERATION !== null) {
			return $this::OPERATION;
		}else{
			throw new OperationMissingException('Operation not defined');
		}
	}
	
	public function __toString(): string
	{
		return $this->get();
	}
	
	public function __invoke(): string
	{
		return $this->__toString();
	}

	abstract public function execute(OperandInterface $operandFirst, OperandInterface $operandSecond): float;
}
	