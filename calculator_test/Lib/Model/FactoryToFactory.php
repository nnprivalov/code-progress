<?php

namespace Lib\Model;

use Lib\Model\FactoryToFactory\InputDataFactory;
use Lib\Model\FactoryToFactory\ConfigFactory;
use Lib\Model\FactoryToFactory\OperandFactory;
use Lib\Model\FactoryToFactory\OperationFactory;
use Lib\Model\FactoryToFactory\PullOperandsFactory;
use Lib\Model\FactoryToFactory\ExecutorFactory;
use Lib\Model\FactoryToFactory\HandlerFactory;
use Lib\Model\FactoryToFactory\CalculatorFactory;
use Lib\Model\FactoryToFactory\CalcResultFactory;
use Lib\Model\FactoryToFactory\CalcResultDecoratorFactory;

class FactoryToFactory
{
	
	public function run(array $inputData): string
	{
		$inputDataFactory = new InputDataFactory($inputData);
		$configFactory = new ConfigFactory();
		$operandFactory = new OperandFactory($inputDataFactory);
		$operationFactory = new OperationFactory($inputDataFactory, $configFactory);
		$pullOperandsFactory = new PullOperandsFactory($operandFactory);
		$executorFactory = new ExecutorFactory($pullOperandsFactory, $operationFactory);
		$calcResultDecoratorFactory = new CalcResultDecoratorFactory($pullOperandsFactory, $operationFactory);
		$calcResultFactory = new CalcResultFactory($calcResultDecoratorFactory);
		$handlerFactory = new HandlerFactory($executorFactory, $calcResultFactory);
		$calculatorFactory = new CalculatorFactory($handlerFactory);
		
		$calculator = $calculatorFactory->create();
		return $calculator->calculate()->print();
	}

}