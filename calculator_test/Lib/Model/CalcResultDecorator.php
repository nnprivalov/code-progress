<?php

namespace Lib\Model;

use Api\Data\OperationInterface;
use Api\PullOperandsInterface;
use Api\Data\CalcResultDecoratorInterface;

class CalcResultDecorator implements CalcResultDecoratorInterface
{
	/** OperationInterface */
	protected $operation;
	/** PullOperandsInterface */
	protected $pull;
	/** float */
	protected $value;

	public function __construct(
		OperationInterface $operation,
		PullOperandsInterface $pullOperands,
		float $resultValue
	){
		$this->operation = $operation;
		$this->pull = $pullOperands;
		$this->value = $resultValue;
	}

	public function __toString(): string
	{
		$separator = ' '.$this->operation->get().' ';
		$result = implode($separator, $this->pull->get());
		$result .= ' = '.$this->value;
		
		return $result;
	}
}