<?php

namespace Lib\Model\CustomDecorators;

use Api\Data\OperationInterface;
use Api\PullOperandsInterface;
use Api\Data\CalcResultDecoratorInterface;

class CustomResultDecorator implements CalcResultDecoratorInterface
{
	/** OperationInterface */
	protected $operation;
	/** PullOperandsInterface */
	protected $pull;
	/** float */
	protected $value;

	public function __construct(
		OperationInterface $operation,
		PullOperandsInterface $pullOperands,
		float $resultValue
	){
		$this->operation = $operation;
		$this->pull = $pullOperands;
		$this->value = $resultValue;
	}

	public function __toString(): string
	{
		$operands = $this->pull->get();
		$result = 'Custom decorator for result: <'.$this->value.'>, with first operand <'.array_shift($operands).'> and second operand <'.array_shift($operands).'>, for wich applied operation: "'.$this->operation->get().'"';

		return $result;
	}
}