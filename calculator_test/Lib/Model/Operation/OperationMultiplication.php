<?php

namespace Lib\Model\Operation;

use Lib\Model\AbstractOperation;
use Api\Data\OperandInterface;

class OperationMultiplication extends AbstractOperation
{
	const OPERATION = '*';

	public function execute(OperandInterface $operandFirst, OperandInterface $operandSecond): float
	{
		return floatval($operandFirst() * $operandSecond());
	}
	
}
	