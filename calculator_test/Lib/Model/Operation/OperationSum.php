<?php

namespace Lib\Model\Operation;

use Lib\Model\AbstractOperation;
use Api\Data\OperandInterface;

class OperationSum extends AbstractOperation
{
	const OPERATION = '+';
	
	public function execute(OperandInterface $operandFirst, OperandInterface $operandSecond): float
	{
		return floatval($operandFirst() + $operandSecond());
	}
}