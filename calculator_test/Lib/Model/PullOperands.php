<?php 

namespace Lib\Model;

use Api\Data\OperandInterface;
use Api\PullOperandsInterface;

class PullOperands implements PullOperandsInterface
{
	/** array */
	protected $pull = []; 
	
	public function __construct(
		OperandInterface $operandFirst,
		OperandInterface $operandSecond
	){
		array_push($this->pull, $operandFirst);
		array_push($this->pull, $operandSecond);
	}
	
	public function get(): array
	{
		return $this->pull;
	}

	public function set(OperandInterface $operandFirst, OperandInterface $operandSecond): void
	{
		$this->pull = [];
		array_push($this->pull, $operandFirst);
		array_push($this->pull, $operandSecond);
	}

}