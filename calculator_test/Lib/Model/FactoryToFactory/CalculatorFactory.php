<?php

namespace Lib\Model\FactoryToFactory;

use Api\CalculatorInterface;
use Lib\Model\FactoryToFactory\HandlerFactory;
use Lib\Calculator;

class CalculatorFactory
{
	protected $instance;
	protected $handlerFactory;

	public function __construct(
		HandlerFactory $handlerFactory
	){
		$this->handlerFactory = $handlerFactory;
	}
	
	public function create(): CalculatorInterface
	{
		$this->instance = new Calculator(
			$this->handlerFactory->get()
		);

		return $this->instance;
	}

	public function get(): CalculatorInterface
	{
		if (!isset($this->instance)) {
			$this->create();
		}

		return $this->instance;
	}
}