<?php

namespace Lib\Model\FactoryToFactory;

use Api\PullOperandsInterface;
use Lib\Model\PullOperands;
use Lib\Model\FactoryToFactory\OperandFactory;

class PullOperandsFactory
{
	protected $operandFactory;
	protected $instance;

	public function __construct(
		OperandFactory $operandFactory
	){
		$this->operandFactory = $operandFactory;
	}	
	
	public function create(): PullOperandsInterface
	{
		$this->instance = new PullOperands(
			$this->operandFactory->create(),
			$this->operandFactory->create()
		);

		return $this->instance;
	}

	public function get(): PullOperandsInterface
	{
		if (!isset($this->instance)) {
			$this->create();
		}

		return $this->instance;
	}
}