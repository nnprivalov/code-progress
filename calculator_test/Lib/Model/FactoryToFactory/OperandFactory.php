<?php
namespace Lib\Model\FactoryToFactory;

use Api\Data\OperandInterface;
use Lib\Model\Operand;
use Lib\Model\InputData;
use Lib\Model\FactoryToFactory\InputDataFactory;

class OperandFactory
{
	protected $inputDataFactory;
	protected $instances = [];
	
	public function __construct(
		InputDataFactory $inputDataFactory
	){
		$this->inputDataFactory = $inputDataFactory;
	}

	public function create(): OperandInterface
	{
		$inputData = $this->inputDataFactory->get();

		$this->instances[] = $instance = new Operand($inputData->getNext());

		return $instance;
	}

	public function get(): array
	{
		return $this->instances;
	}
}