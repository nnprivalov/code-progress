<?php

namespace Lib\Model\FactoryToFactory;

use Api\Handler\HandlerInterface;
use Lib\Handler;
use Lib\Model\FactoryToFactory\ExecutorFactory;
use Lib\Model\FactoryToFactory\CalcResultFactory;

class HandlerFactory
{
	/** HandlerInterface */
	protected $instance;
	protected $executorFactory;
	protected $calcResultFactory;


	public function __construct(
		ExecutorFactory $executorFactory,
		CalcResultFactory $calcResultFactory
	){
		$this->executorFactory = $executorFactory;
		$this->calcResultFactory = $calcResultFactory;
	}
	
	public function create(): HandlerInterface
	{
		$this->instance = new Handler(
			$this->executorFactory->get(),
			$this->calcResultFactory
		);

		return $this->instance;
	}

	public function get(): HandlerInterface
	{
		if (!isset($this->instance)) {
			$this->create();
		}

		return $this->instance;
	}
}