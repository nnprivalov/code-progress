<?php

namespace Lib\Model\FactoryToFactory;

use Lib\Model\InputData;

class InputDataFactory
{
	protected $instance;
	protected $inputData;

	public function __construct(
		array $inputData
	){
		$this->inputData = $inputData;
	}
	
	public function create(): InputData
	{
		$this->instance = new InputData(
			$this->inputData['operandFirst'],
			$this->inputData['operandSecond'],
			$this->inputData['operation']
		);

		return $this->instance;
	}

	public function get(): InputData
	{
		if (!isset($this->instance)) {
			$this->create();
		}

		return $this->instance;
	}
}