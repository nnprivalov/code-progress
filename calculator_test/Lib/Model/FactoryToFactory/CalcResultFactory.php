<?php

namespace Lib\Model\FactoryToFactory;

use Api\Data\Factory\CalcResultFactoryInterface;
use Api\Data\CalcResultInterface;
use Lib\Model\CalcResult;
use Lib\Model\FactoryToFactory\CalcResultDecoratorFactory;

class CalcResultFactory implements CalcResultFactoryInterface
{
	/** CalcResultInterface */
	protected $instance;
	/** CalcResultDecoratorFactory */
	protected $decoratorFactory;

	public function __construct(
		CalcResultDecoratorFactory $decoratorFactory
	){
		$this->decoratorFactory = $decoratorFactory;
	}
	
	public function create(float $resultValue): CalcResultInterface
	{
		$this->instance = new CalcResult(
			$this->decoratorFactory->create($resultValue)
		);
		$this->instance->set($resultValue);

		return $this->instance;
	}

	public function get(): CalcResultInterface
	{
		return $this->instance;
	}
}