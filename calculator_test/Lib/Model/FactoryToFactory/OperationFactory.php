<?php

namespace Lib\Model\FactoryToFactory;

use Api\Data\OperationInterface;
use Lib\Model\FactoryToFactory\ConfigFactory;
use Lib\Model\Operation\Exception\OperationMissingException;
use Lib\Model\FactoryToFactory\InputDataFactory;

class OperationFactory
{
	protected $inputDataFactory;
	protected $configFactory;
	protected $instance;

	public function __construct(
		InputDataFactory $inputDataFactory,
		ConfigFactory $configFactory
	){
		$this->inputDataFactory = $inputDataFactory;
		$this->configFactory = $configFactory;
	}

	public function create(): OperationInterface
	{
		$config = $this->configFactory->get();
		$inputData = $this->inputDataFactory->get();
		$type = $inputData->getOperation();
		$instanceClass = $config->getOperationExecutableClass($type);
		if (null === $instanceClass) {
			throw new OperationMissingException('Operation not exists: '.$type);
		}
		$this->instance = new $instanceClass();

		return $this->instance;
	}

	public function get(): OperationInterface
	{
		if (!isset($this->instance)) {
			$this->create();
		}

		return $this->instance;
	}
}