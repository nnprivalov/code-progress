<?php

namespace Lib\Model\FactoryToFactory;

use Api\Handler\ExecutorInterface;
use Lib\Executor;
use Lib\Model\FactoryToFactory\PullOperandsFactory;
use Lib\Model\FactoryToFactory\OperationFactory;

class ExecutorFactory
{
	protected $instance;
	protected $pullFactory;
	protected $operationFactory;

	public function __construct(
		PullOperandsFactory $pullFactory,
		OperationFactory $operationFactory
	){
		$this->pullFactory = $pullFactory;
		$this->operationFactory = $operationFactory;
	}
	
	public function create(): ExecutorInterface
	{
		$this->instance = new Executor(
			$this->pullFactory->get(),
			$this->operationFactory->get()
		);

		return $this->instance;
	}

	public function get(): ExecutorInterface
	{
		if (!isset($this->instance)) {
			$this->create();
		}

		return $this->instance;
	}
}