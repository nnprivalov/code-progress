<?php

namespace Lib\Model\FactoryToFactory;

use Lib\Config;

class ConfigFactory
{
	/** Config */
	protected $instance;
	
	public function create(): Config
	{
		$this->instance = new Config();
		
		return $this->instance;
	}

	public function get(): Config
	{
		if (!isset($this->instance)) {
			$this->create();
		}
		return $this->instance;
	}
}