<?php
namespace Lib\Model\FactoryToFactory;

use Api\Data\CalcResultDecoratorInterface;
use Lib\Model\CalcResultDecorator;
use Lib\Model\FactoryToFactory\PullOperandsFactory;
use Lib\Model\FactoryToFactory\OperationFactory;

class CalcResultDecoratorFactory
{
	/** CalcResultDecoratorInterface */
	protected $instance;
	protected $pullFactory;
	protected $operationFactory;

	public function __construct(
		PullOperandsFactory $pullFactory,
		OperationFactory $operationFactory
	){
		$this->pullFactory = $pullFactory;
		$this->operationFactory = $operationFactory;
	}
	
	public function create(float $resultValue): CalcResultDecoratorInterface
	{
		$this->instance = new CalcResultDecorator(
			$this->operationFactory->get(),
			$this->pullFactory->get(),
			$resultValue
		);

		return $this->instance;
	}

	public function get(): CalcResultDecoratorInterface
	{
		return $this->instance;
	}
}