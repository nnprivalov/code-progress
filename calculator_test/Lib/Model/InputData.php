<?php 

namespace Lib\Model;

class InputData
{
	protected $operation;
	protected $operands = [];
	protected $_retrievedOperands = [];

	public function __construct(
		float $operandFirst,
		float $operandSecond,
		string $operation
	){
		array_push($this->operands, $operandFirst);
		array_push($this->operands, $operandSecond);
		$this->operation = $operation;
	}

	public function getNext(): float
	{
		$operand = array_shift($this->operands);
		array_push($this->_retrievedOperands, $operand);
		if (empty($this->operands)) {
			$this->operands = $this->_retrievedOperands;
			$this->_retrievedOperands = [];
		}
		return $operand;
	}

	public function getOperation(): string
	{
		return $this->operation;
	}
}