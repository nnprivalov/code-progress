<?php

namespace Lib\Model;

use Api\Data\CalcResultInterface;
use Api\Data\OperandInterface;
use Api\Data\CalcResultDecoratorInterface;

class CalcResult implements CalcResultInterface, OperandInterface
{
	protected $value;
	protected $decorator;

	public function __construct(
		CalcResultDecoratorInterface $decorator
	){
		$this->decorator = $decorator;
	}
	
	public function set(float $value = null): void
	{
		$this->value = $value;
	}
	
	public function get(): float
	{
		return $this->value;
	}
	
	public function __toString(): string
	{
		return strval($this->value);
	}

	public function __invoke(): float
	{
		return $this->get();
	}

	public function print(): string
	{
		return strval($this->decorator);
	}
}