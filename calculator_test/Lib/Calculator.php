<?php

namespace Lib;

use Api\Handler\HandlerInterface;
use Api\Data\CalcResultInterface;
use Api\CalculatorInterface;

class Calculator implements CalculatorInterface
{
	/** HandlerInterface */
	protected $handler;
	
	public function __construct(
		HandlerInterface $handler
	){
		$this->handler = $handler;
	}
	
	public function calculate(): CalcResultInterface
	{
		return $this->handler->getResult();
	}
	
	
}