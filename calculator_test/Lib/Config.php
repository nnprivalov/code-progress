<?php

namespace Lib;

class Config
{
	const CONFIG_DIR = ABSPATH.'cfg'.DIRECTORY_SEPARATOR;
	const CONFIG_PREFERENCE = self::CONFIG_DIR.'preference.php';
	const CONFIG_OPERATIONS = self::CONFIG_DIR.'operation.php';

	public function getPreferenceFor(string $interface, string $default = null): string
	{
		$configPreference = include self::CONFIG_PREFERENCE;
		if (isset($configPreference[$interface]) && $this->isClassImplementsInterface($configPreference[$interface], $interface)) {
			return $configPreference[$interface];
		} else {
			return $default;
		}
	}

	public function getOperationExecutableClass(string $symbol, string $default = null): string
	{
		$configOperations = include self::CONFIG_OPERATIONS;
		if (isset($configOperations[$symbol])) {
			return $configOperations[$symbol];
		} else {
			return $default;
		}
	}
	private function isClassImplementsInterface(string $class, string $interface): bool
	{
		return isset(class_implements($class)[$interface]);
	}


}