��    !      $  /   ,      �     �     �     �  	   �                    /     H     U     e     t     �     �     �     �     �  .   �     �  -   �                ;     D     J     Q     c     h  "   y     �     �     �  L  �               -     3     ?     K     ]  1   q     �     �     �     �     �  	   �     �            :   &     a  <   f     �  (   �  
   �     �     �     	  	   	     "	  ,   A	     n	  
   �	     �	                                                  
                          	             !                                                                      Active Cell City Companies Company Company consumers: Company details Company has been updated Company name Company profile Company slogan Company website Country Create Create date Delete Description Information about the company is not available Logo Module to handle data for Dealers, companies. New company New company has been added Overview Phone Postal Registration date Save Search Companies Selected records have been deleted Service email address State Street and number Project-Id-Version: LS-Companies
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2019-03-01 11:13+0100
PO-Revision-Date: 2019-03-01 11:13+0100
Last-Translator: 
Language-Team: 
Language: de_DE
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;__ngettext:1,2;_n:1,2;__ngettext_noop:1,2;_n_noop:1,2;_c,_nc:4c,1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2
X-Poedit-Basepath: ..
X-Textdomain-Support: yes
X-Generator: Poedit 2.2.1
X-Poedit-SearchPath-0: .
 aktiv Mobilfunknummer Stadt Unternehmen Unternehmen Unternehmensname: Unternehmensdetails Die Unternehmensinformationen wurden aktualisiert Unternehmensname Unternehmensprofil Unternehmens-Slogan Unternehmens Website Land erstellen Erstellungsdatum Löschen Beschreibung Die Information über das Unternehmen ist nicht verfügbar Logo Modul zur Verwaltung von Unternehmensdaten und Mitarbeitern. Neues Unternehmen Ein neues Unternehmen wurde hinzugefügt Überblick Telefonnummer Postleitzahl Registrierungsdatum speichern Unternehmen suchen/durchsuchen Die ausgewählten Einträge wurden gelöscht Service E-Mail Adresse Bundesland Strasse und Hausnummer 