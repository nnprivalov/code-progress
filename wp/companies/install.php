<?php
function companyInstallLoyaltySuite()
{
    $wpdb = LS()->wpdb;

    $tb = $wpdb->get_var(sprintf("SHOW TABLES LIKE '{$wpdb->prefix}ls_companies'"));
    if (empty($tb)) {
        $wpdb->query(sprintf("CREATE TABLE `{$wpdb->prefix}ls_companies` (
            `companyId` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
            `parentCompanyId` INT(10) UNSIGNED NOT NULL DEFAULT '0',
            `companyName` VARCHAR(100) NOT NULL,
            `companyInternalName` VARCHAR(100) NOT NULL,
            `companyWebsite` VARCHAR(2000) NOT NULL,
            `companySlogan` VARCHAR(500) NOT NULL,
            `companyAddress` VARCHAR(300) NOT NULL,
            `companyPostal` VARCHAR(10) NOT NULL,
            `companyCity` VARCHAR(60) NOT NULL,
            `companyState` VARCHAR(200) NOT NULL,
            `companyCountry` VARCHAR(2) NOT NULL,
            `companyPhone` VARCHAR(40) NOT NULL,
            `companyCell` VARCHAR(40) NOT NULL,
            `companyFax` VARCHAR(40) NOT NULL,
            `companyServiceEmail` VARCHAR(255) NOT NULL,
            `companyLogo` VARCHAR(2000) NOT NULL,
            `companyActive` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
            `companyCreateDate` INT(10) UNSIGNED NOT NULL,
            `companyPageId` BIGINT(20) UNSIGNED NULL DEFAULT NULL,
            `numberOfEmployees` INT(10) UNSIGNED NULL DEFAULT '0',
            `locations` TEXT NULL,
            PRIMARY KEY (`companyId`),
	        INDEX `parentCompanyId` (`parentCompanyId`)
        ) %s ENGINE=InnoDB", LS\Library::getCharsetCollate()));
    }

    $tb = $wpdb->get_var(sprintf("SHOW TABLES LIKE '{$wpdb->prefix}ls_companies_fields_values'"));
    if (empty($tb)) {
        $wpdb->query(sprintf("CREATE TABLE `{$wpdb->prefix}ls_companies_fields_values` (
            `companyId` INT(10) UNSIGNED NOT NULL,
            `fieldName` VARCHAR(64) NOT NULL,
            `value` TEXT NOT NULL,
            PRIMARY KEY (`companyId`, `fieldName`),
            INDEX `companyId` (`companyId`)
        ) %s ENGINE=InnoDB", LS\Library::getCharsetCollate()));
    }
}