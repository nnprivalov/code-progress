<?php
namespace LS;

class Company extends Model implements CompanyInterface
{
    /** @var int */
    protected $companyId;

    /* @var int */
    protected $parentCompanyId;

    /** @var string */
    protected $companyName;

    /** @var string */
    protected $companyInternalName;

    /** @var string */
    protected $companyWebsite;

    /** @var string */
    protected $companySlogan;

    /** @var string */
    protected $companyAddress;

    /** @var string */
    protected $companyPostal;

    /** @var string */
    protected $companyCity;

    /** @var string */
    protected $companyState;

    /** @var string */
    protected $companyCountry;

    /** @var string */
    protected $companyPhone;

    /** @var string */
    protected $companyCell;

    /** @var string */
    protected $companyFax;

    /** @var string */
    protected $companyServiceEmail;

    /** @var string */
    protected $companyLogo;

    /** @var bool */
    protected $companyActive;

    /** @var int */
    protected $companyCreateDate;

    /** @var int */
    protected $companyPageId;

    /** @var int */
    protected $numberOfEmployees;

    /** @var array */
    protected $locations;

    /** @var string[] */
    protected $primaryKeys = [
        'companyId'
    ];

    /** @var string[] */
    protected $dbCols = [
        'companyId',
        'parentCompanyId',
        'companyName',
        'companyInternalName',
        'companyWebsite',
        'companySlogan',
        'companyAddress',
        'companyPostal',
        'companyCity',
        'companyState',
        'companyCountry',
        'companyPhone',
        'companyCell',
        'companyFax',
        'companyServiceEmail',
        'companyLogo',
        'companyActive',
        'companyCreateDate',
        'companyPageId',
        'numberOfEmployees',
        'locations'
    ];

    /** @var array */
    private $dynamicData;

    /** @var null|Company */
    private $parentCompany;

    /**
     * Set default data
     * @since 2.0.0
     * @return self
     */
    public function setDefault()
    {
        $this->companyId =
        $this->parentCompanyId =
        $this->companyCreateDate =
        $this->companyPageId =
        $this->numberOfEmployees = 0;

        $this->companyName =
        $this->companyInternalName =
        $this->companyWebsite =
        $this->companySlogan =
        $this->companyAddress =
        $this->companyPostal =
        $this->companyCity =
        $this->companyState =
        $this->companyCountry =
        $this->companyPhone =
        $this->companyCell =
        $this->companyFax =
        $this->companyServiceEmail =
        $this->companyLogo = '';

        $this->locations = [];

        $this->companyActive = false;

        $this->dynamicData = null;

        return $this;
    }

    /**
     * Format the data
     *
     * @since 2.0.0
     *
     * @param array $data Data to be formatted
     * @return array Formatted data
     */
    protected function format($data)
    {
        foreach ($data as $key => $value) {
            if (in_array($key, ['companyId', 'parentCompanyId', 'companyPageId', 'numberOfEmployees'])) {
                $data[$key] = (int) $value;
            } elseif ($key == 'companyCreateDate') {
                $data[$key] = max(0, !empty($value) && !is_numeric($value) ? strtotime($value) : (int) $value);
            } elseif ($key == 'companyActive') {
                $data[$key] = (bool) $value;
            } elseif ($key == 'locations') {
                $data[$key] = is_array($value) ? $value : Library::maybeUnserialize($value);
            }
        }

        return $data;
    }

    /**
     * Retrieve company Id
     * @since 2.0.0
     * @return int Company Id
     */
    public function getCompanyId()
    {
        return $this->companyId;
    }

    /**
     * Set company Id
     *
     * @since 2.2.0
     *
     * @param int $companyId Company Id
     * @return self
     */
    public function setCompanyId($companyId)
    {
        $this->companyId = (int) $companyId;

        return $this;
    }

    /**
     * Retrieve parent company Id
     * @since 2.0.0
     * @return int Parent company Id
     */
    public function getParentCompanyId()
    {
        return $this->parentCompanyId;
    }

    /**
     * Check if the company has parent company
     * @since 2.0.0
     * @return bool True if company has parent company
     */
    public function hasParent()
    {
        return $this->parentCompanyId > 0;
    }

    /**
     * Set parent company
     *
     * @since 2.1.0
     *
     * @param Company $company Company
     * @return self
     */
    public function setParentCompany(Company $company)
    {
        $this->parentCompany = $company;

        return $this;
    }

    /**
     * Remove parent company
     * @since 2.8.0
     * @return self
     */
    public function removeParentCompany()
    {
        $this->parentCompany = false;

        return $this;
    }

    /**
     * Retrieve parent company
     * @since 2.1.0
     * @return Company|null Parent company or null if not available
     */
    private function getParentCompany()
    {
        return $this->parentCompany;
    }

    /**
     * Retrieve company name
     *
     * @since 2.0.0
     *
     * @param bool $hierarchical True to get data from the parent company if empty
     * @return string Company name
     */
    public function getCompanyName($hierarchical = false)
    {
        $name = $this->getStaticFieldValue('companyName', $hierarchical);

        if (empty($name) && !$hierarchical) {

            $name = $this->getCompanyInternalName();

            if (empty($name)) {
                $name = 'ID:' . $this->getCompanyId();
            }
        }

        return $name;
    }

    /**
     * Set company name
     *
     * @since 2.2.0
     *
     * @param string $companyName Company name
     * @return self
     */
    public function setCompanyName($companyName)
    {
        $this->companyName = $companyName;

        return $this;
    }

    /**
     * Retrieve company internal name
     * @since 2.9.7
     * @return string Company name
     */
    public function getCompanyInternalName()
    {
        return $this->getStaticFieldValue('companyInternalName', false);
    }

    /**
     * Retrieve company website
     *
     * @since 2.0.0
     *
     * @param bool $hierarchical True to get data from the parent company if empty
     * @return string Company website
     */
    public function getCompanyWebsite($hierarchical = false)
    {
        return $this->getStaticFieldValue('companyWebsite', $hierarchical);
    }

    /**
     * Retrieve company slogan
     *
     * @since 2.0.0
     *
     * @param bool $hierarchical True to get data from the parent company if empty
     * @return string Company slogan
     */
    public function getCompanySlogan($hierarchical = false)
    {
        return $this->getStaticFieldValue('companySlogan', $hierarchical);
    }

    /**
     * Retrieve company address
     *
     * @since 2.0.0
     *
     * @param bool $hierarchical True to get data from the parent company if empty
     * @return string Company address
     */
    public function getCompanyAddress($hierarchical = false)
    {
        return $this->getStaticFieldValue('companyAddress', $hierarchical);
    }

    /**
     * Retrieve company postal
     *
     * @since 2.0.0
     *
     * @param bool $hierarchical True to get data from the parent company if empty
     * @return string Company postal
     */
    public function getCompanyPostal($hierarchical = false)
    {
        return $this->getStaticFieldValue('companyPostal', $hierarchical);
    }

    /**
     * Retrieve company city
     *
     * @since 2.0.0
     *
     * @param bool $hierarchical True to get data from the parent company if empty
     * @return string Company city
     */
    public function getCompanyCity($hierarchical = false)
    {
        return $this->getStaticFieldValue('companyCity', $hierarchical);
    }

    /**
     * Retrieve company state
     *
     * @since 2.0.0
     *
     * @param bool $hierarchical True to get data from the parent company if empty
     * @return string Company state
     */
    public function getCompanyState($hierarchical = false)
    {
        return $this->getStaticFieldValue('companyState', $hierarchical);
    }

    /**
     * Retrieve company country
     *
     * @since 2.0.0
     *
     * @param bool $hierarchical True to get data from the parent company if empty
     * @return string Company country
     */
    public function getCompanyCountry($hierarchical = false)
    {
        return $this->getStaticFieldValue('companyCountry', $hierarchical);
    }

    /**
     * Retrieve company phone
     *
     * @since 2.0.0
     *
     * @param bool $hierarchical True to get data from the parent company if empty
     * @return string Company phone
     */
    public function getCompanyPhone($hierarchical = false)
    {
        return $this->getStaticFieldValue('companyPhone', $hierarchical);
    }

    /**
     * Retrieve company cell
     *
     * @since 2.0.0
     *
     * @param bool $hierarchical True to get data from the parent company if empty
     * @return string Company cell
     */
    public function getCompanyCell($hierarchical = false)
    {
        return $this->getStaticFieldValue('companyCell', $hierarchical);
    }

    /**
     * Retrieve company fax
     *
     * @since 2.0.0
     *
     * @param bool $hierarchical True to get data from the parent company if empty
     * @return string Company fax
     */
    public function getCompanyFax($hierarchical = false)
    {
        return $this->getStaticFieldValue('companyFax', $hierarchical);
    }

    /**
     * Retrieve company service email
     *
     * @since 2.0.0
     *
     * @param bool $hierarchical True to get data from the parent company if empty
     * @return string Company service email
     */
    public function getCompanyServiceEmail($hierarchical = false)
    {
        return $this->getStaticFieldValue('companyServiceEmail', $hierarchical);
    }

    /**
     * Retrieve company logo
     *
     * @since 2.0.0
     *
     * @param bool $hierarchical True to get data from the parent company if empty
     * @return string Company logo
     */
    public function getCompanyLogo($hierarchical = false)
    {
        return $this->getStaticFieldValue('companyLogo', $hierarchical);
    }

    /**
     * Check if the company has logo
     *
     * @since 2.0.0
     *
     * @param bool $hierarchical True to get data from the parent company if empty
     * @return bool True if company has logo
     */
    public function hasCompanyLogo($hierarchical = false)
    {
        return $this->getCompanyLogo($hierarchical) != '';
    }

    /**
     * Check if the company is active
     * @since 2.2.0
     * @return bool True if company is active
     */
    public function isCompanyActive()
    {
        return $this->companyActive;
    }

    /**
     * Activate company
     * @since 2.2.0
     * @return self
     */
    public function markAsActive()
    {
        $this->companyActive = true;

        return $this;
    }

    /**
     * Deactivate company
     * @since 2.2.0
     * @return self
     */
    public function markAsInactive()
    {
        $this->companyActive = false;

        return $this;
    }

    /**
     * Retrieve company create date
     * @since 2.0.0
     * @return int Company create date
     */
    public function getCompanyCreateDate()
    {
        return $this->companyCreateDate;
    }

    /**
     * Mark company as created - prepare it to be added to the database
     * @since 2.0.0
     * @return self
     */
    public function markAsCreated()
    {
        $this->companyCreateDate = time();

        return $this;
    }

    /**
     * Retrieve company page Id
     *
     * @since 2.0.0
     *
     * @param bool $hierarchical True to get data from the parent company if empty
     * @return int Company page Id
     */
    public function getCompanyPageId($hierarchical = false)
    {
        return $this->getStaticFieldValue('companyPageId', $hierarchical);
    }

    /**
     * Retrieve company page
     *
     * @since 2.0.0
     *
     * @param bool $hierarchical True to get data from the parent company if empty
     * @param false|string $default Default value
     * @return string URL
     */
    public function getCompanyPageLink($hierarchical = false, $default = '')
    {
        $pageId = $this->getCompanyPageId($hierarchical);

        return $pageId > 0 ? get_permalink($pageId) : $default;
    }

    /**
     * Retrieve Retrieve number of the company employees
     *
     * @since 2.0.0
     *
     * @param bool $hierarchical True to get data from the parent company if empty
     * @return int Number of employees
     */
    public function getNumberOfEmployees($hierarchical = false)
    {
        return $this->getStaticFieldValue('numberOfEmployees', $hierarchical);
    }

    /**
     * Retrieve company locations
     *
     * @since 2.0.0
     *
     * @param bool $hierarchical True to fetch value from the parent company
     * @return array Company locations
     */
    public function getCompanyLocations($hierarchical = false)
    {
        return $this->getStaticFieldValue('locations', $hierarchical);
    }

    /**
     * Retrieve record of the object data which can be stored in the database table
     *
     * @since 2.0.0
     *
     * @param bool $withPrimaryKeys True to include primary keys data
     * @return array An array of columns and their values
     */
    public function getRecord($withPrimaryKeys = false)
    {
        $data = [];

        foreach ($this->getDbColumns($withPrimaryKeys) as $col) {
            if ($col == 'companyActive') {
                $data[$col] = (int) $this->{$col};
            } elseif ($col == 'locations') {
                $data[$col] = maybe_serialize($this->{$col});
            } else {
                $data[$col] = $this->{$col};
            }
        }

        return $data;
    }

    /**
     * Check if the dynamic fields available for the current company
     * @since 2.0.0
     * @return bool True if the dynamic fields available for the current company
     */
    public function hasDynamicData()
    {
        return is_array($this->dynamicData);
    }

    /**
     * Retrieve all the dynamic field related to the company
     * @since 2.0.0
     * @return array Dynamic data
     */
    public function getDynamicData()
    {
        return $this->hasDynamicData() ? $this->dynamicData : [];
    }

    /**
     * Set dynamic data of the company
     *
     * @since 2.0.0
     *
     * @param array $fields Array of fields and their values
     * @return self
     */
    public function setDynamicData($fields)
    {
        foreach ($fields as $fieldName => $value) {
            $this->setDynamicFieldValue($fieldName, $value);
        }

        return $this;
    }

    /**
     * Retrieve dynamic field value by field name
     *
     * @since 2.0.0
     *
     * @param string $fieldName Field name
     * @param bool $hierarchical True to fetch value from the parent company
     * @param mixed $default Default value
     * @return mixed Field value
     */
    public function getDynamicFieldValue($fieldName, $hierarchical = false, $default = '')
    {
        $value = $this->hasDynamicData() && isset($this->dynamicData[$fieldName]) ? $this->dynamicData[$fieldName] : '';

        if (empty($value) && $hierarchical && $this->hasParent()) {
            $parentCompany = $this->getParentCompany();
            if ($parentCompany) {
                $value = $parentCompany->getDynamicFieldValue($fieldName, $hierarchical, $default);
            } elseif ($this->getParentCompanyId() > 0) {
                _doing_it_wrong(__FUNCTION__, 'Seems that getCompany() was called with $hierarchical=false because field ' . $fieldName . ' tried to get data from the parent company', '2.9.3');
            }
        }

        return empty($value) ? $default : $value;
    }

    /**
     * Set the field value
     *
     * @since 2.0.0
     *
     * @param string $fieldName Field name
     * @param mixed $value Field value
     * @return self
     */
    public function setDynamicFieldValue($fieldName, $value)
    {
        if (!$this->hasDynamicData()) {
            $this->dynamicData = [];
        }

        $this->dynamicData[$fieldName] = $value;

        return $this;
    }

    /**
     * Remove dynamic field
     *
     * @since 2.8.2
     *
     * @param string $fieldName Field name
     * @return bool True if removed
     */
    public function removeDynamicField(string $fieldName): bool
    {
        if (isset($this->dynamicData[$fieldName])) {
            unset($this->dynamicData[$fieldName]);
            return true;
        }

        return false;
    }

    /**
     * Retrieve static field value by field name
     *
     * @since 2.1.0
     *
     * @param string $fieldName Field name
     * @param bool $hierarchical True to fetch value from the parent company
     * @return mixed Field value
     */
    public function getStaticFieldValue($fieldName, $hierarchical = false)
    {
        $value = $this->get($fieldName);

        if (empty($value) && $hierarchical && $this->hasParent()) {

            $parentCompany = $this->getParentCompany();

            if ($parentCompany) {
                $value = $parentCompany->getStaticFieldValue($fieldName, $hierarchical);
            } elseif ($this->getParentCompanyId() > 0) {
                _doing_it_wrong(__FUNCTION__, 'Seems that getCompany() was called with $hierarchical=false because field ' . $fieldName . ' tried to get data from the parent company', '2.9.3');
            }
        }

        return $value;
    }

    /**
     * Retrieve company field value
     *
     * @since 2.1.0
     *
     * @param string $fieldName Field name
     * @param bool $hierarchical True to fetch the value from the parent company if it's empty
     * @param null $deprecated 04.2018 Legacy
     * @return mixed Field value
     */
    public function getFieldValue($fieldName, $hierarchical = false, $deprecated = null)
    {
        if ($deprecated !== null) {
            _deprecated_argument('Company::getFieldValue() 2nd param is deprecated', '2.5.4');
            $hierarchical = $deprecated;
        }

        return
            in_array($fieldName, $this->getDbColumns(true))
                ? $this->getStaticFieldValue($fieldName, $hierarchical)
                : $this->getDynamicFieldValue($fieldName, $hierarchical);
    }

    /**
     * Set record data from the array
     *
     * @since 2.0.0
     *
     * @param array $data Table record data as array
     * @return self
     */
    public function setFromArray($data)
    {
        foreach ($this->format($data) as $column => $value) {
            if (property_exists($this, $column)) {
                $this->{$column} = $value;
            } else {
                $this->setDynamicFieldValue($column, $value);
            }
        }

        return $this;
    }
}

/** @deprecated 07.2019 Legacy */
class_alias(Company::class, '\LSCompany');