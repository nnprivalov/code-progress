jQuery(function($) {

    // company location
    $('select[name="companyId"]').change(function() {

        var cl = $('select[name="companyLocation"]'),
            companyId = $(this).val();

        if(cl.length) {

            cl.empty().append($('<option></option>')).attr('disabled', 'disabled');

            if(companyId > 0) {
                $.post(ajaxurl, {
                        action: 'getCompanyLocations',
                        companyId: companyId
                    }
                ).done(function(locations) {

                    if(!$.isEmptyObject(locations)) {

                        for(var id in locations) {
                            cl.append($('<option></option>').attr('value', id).text(locations[id]));
                        }

                        cl.removeAttr('disabled');
                    }
                });
            }
        }
    });
});