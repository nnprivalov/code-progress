jQuery(function($) {

    addDeleteConfirmation('wipe-company');
    addDeleteConfirmation('delete-company');

    // Dynamic field management
    if($('#ls-companies-field').length) {

        var fieldTitle = $('input[name="title"]'),
            fieldName = $('input[name="name"]');

        // sanitize new field name
        fieldTitle.change(function() {

            if(!fieldName.val().length) {
                $.post(ajaxurl, {
                    action: 'companiesSanitizeText',
                    text: fieldTitle.val()
                }, function(response) {
                    fieldName.val(response);
                });
            }
        });

        $('.ls-companies-field').submit(function() {

            fieldTitle.removeClass('error');
            fieldName.removeClass('error');

            if(!fieldTitle.val().length) {
                fieldTitle.addClass('error');
                return false;
            } else if(!fieldName.val().length) {
                fieldName.addClass('error');
                return false;
            }
        });
    }
});