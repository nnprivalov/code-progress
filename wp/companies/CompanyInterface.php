<?php
namespace LS;

interface CompanyInterface extends ModelInterface
{
    /**
     * Retrieve company Id
     * @since 2.0.0
     * @return int Company Id
     */
    public function getCompanyId();

    /**
     * Set company Id
     *
     * @since 2.2.0
     *
     * @param int $companyId Company Id
     * @return self
     */
    public function setCompanyId($companyId);

    /**
     * Retrieve parent company Id
     * @since 2.0.0
     * @return int Parent company Id
     */
    public function getParentCompanyId();

    /**
     * Check if the company has parent company
     * @since 2.0.0
     * @return bool True if company has parent company
     */
    public function hasParent();

    /**
     * Set parent company
     * @since 2.1.0
     * @param Company $company Company
     */
    public function setParentCompany(Company $company);

    /**
     * Retrieve company name
     *
     * @since 2.0.0
     *
     * @param bool $hierarchical True to get data from the parent company if empty
     * @return string Company name
     */
    public function getCompanyName($hierarchical = false);

    /**
     * Set company name
     *
     * @since 2.2.0
     *
     * @param string $companyName Company name
     * @return self
     */
    public function setCompanyName($companyName);

    /**
     * Retrieve company internal name
     * @since 2.9.7
     * @return string Company name
     */
    public function getCompanyInternalName();

    /**
     * Retrieve company website
     *
     * @since 2.0.0
     *
     * @param bool $hierarchical True to get data from the parent company if empty
     * @return string Company website
     */
    public function getCompanyWebsite($hierarchical = false);

    /**
     * Retrieve company slogan
     *
     * @since 2.0.0
     *
     * @param bool $hierarchical True to get data from the parent company if empty
     * @return string Company slogan
     */
    public function getCompanySlogan($hierarchical = false);

    /**
     * Retrieve company address
     *
     * @since 2.0.0
     *
     * @param bool $hierarchical True to get data from the parent company if empty
     * @return string Company address
     */
    public function getCompanyAddress($hierarchical = false);

    /**
     * Retrieve company postal
     *
     * @since 2.0.0
     *
     * @param bool $hierarchical True to get data from the parent company if empty
     * @return string Company postal
     */
    public function getCompanyPostal($hierarchical = false);

    /**
     * Retrieve company city
     *
     * @since 2.0.0
     *
     * @param bool $hierarchical True to get data from the parent company if empty
     * @return string Company city
     */
    public function getCompanyCity($hierarchical = false);

    /**
     * Retrieve company state
     *
     * @since 2.0.0
     *
     * @param bool $hierarchical True to get data from the parent company if empty
     * @return string Company state
     */
    public function getCompanyState($hierarchical = false);

    /**
     * Retrieve company country
     *
     * @since 2.0.0
     *
     * @param bool $hierarchical True to get data from the parent company if empty
     * @return string Company country
     */
    public function getCompanyCountry($hierarchical = false);

    /**
     * Retrieve company phone
     *
     * @since 2.0.0
     *
     * @param bool $hierarchical True to get data from the parent company if empty
     * @return string Company phone
     */
    public function getCompanyPhone($hierarchical = false);

    /**
     * Retrieve company cell
     *
     * @since 2.0.0
     *
     * @param bool $hierarchical True to get data from the parent company if empty
     * @return string Company cell
     */
    public function getCompanyCell($hierarchical = false);

    /**
     * Retrieve company fax
     *
     * @since 2.0.0
     *
     * @param bool $hierarchical True to get data from the parent company if empty
     * @return string Company fax
     */
    public function getCompanyFax($hierarchical = false);

    /**
     * Retrieve company service email
     *
     * @since 2.0.0
     *
     * @param bool $hierarchical True to get data from the parent company if empty
     * @return string Company service email
     */
    public function getCompanyServiceEmail($hierarchical = false);

    /**
     * Retrieve company logo
     *
     * @since 2.0.0
     *
     * @param bool $hierarchical True to get data from the parent company if empty
     * @return string Company logo
     */
    public function getCompanyLogo($hierarchical = false);

    /**
     * Check if the company has logo
     *
     * @since 2.0.0
     *
     * @param bool $hierarchical True to get data from the parent company if empty
     * @return bool True if company has logo
     */
    public function hasCompanyLogo($hierarchical = false);

    /**
     * Check if the company is active
     * @since 2.2.0
     * @return bool True if company is active
     */
    public function isCompanyActive();

    /**
     * Activate company
     * @since 2.2.0
     * @return self
     */
    public function markAsActive();

    /**
     * Deactivate company
     * @since 2.2.0
     * @return self
     */
    public function markAsInactive();

    /**
     * Retrieve company create date
     * @since 2.0.0
     * @return int Company create date
     */
    public function getCompanyCreateDate();

    /**
     * Mark company as created - prepare it to be added to the database
     * @since 2.0.0
     * @return self
     */
    public function markAsCreated();

    /**
     * Retrieve company page Id
     *
     * @since 2.0.0
     *
     * @param bool $hierarchical True to get data from the parent company if empty
     * @return int Company page Id
     */
    public function getCompanyPageId($hierarchical = false);

    /**
     * Retrieve company page
     *
     * @since 2.0.0
     *
     * @param bool $hierarchical True to get data from the parent company if empty
     * @param false|string $default Default value
     * @return string URL
     */
    public function getCompanyPageLink($hierarchical = false, $default = '');

    /**
     * Retrieve Retrieve number of the company employees
     *
     * @since 2.0.0
     *
     * @param bool $hierarchical True to get data from the parent company if empty
     * @return int Number of employees
     */
    public function getNumberOfEmployees($hierarchical = false);

    /**
     * Retrieve company locations
     *
     * @since 2.0.0
     *
     * @param bool $hierarchical True to fetch value from the parent company
     * @return array Company locations
     */
    public function getCompanyLocations($hierarchical = false);

    /**
     * Check if the dynamic fields available for the current company
     * @since 2.0.0
     * @return bool True if the dynamic fields available for the current company
     */
    public function hasDynamicData();

    /**
     * Retrieve all the dynamic field related to the company
     * @since 2.0.0
     * @return array Dynamic data
     */
    public function getDynamicData();

    /**
     * Set dynamic data of the company
     *
     * @since 2.0.0
     *
     * @param array $fields Array of fields and their values
     * @return self
     */
    public function setDynamicData($fields);

    /**
     * Retrieve dynamic field value by field name
     *
     * @since 2.0.0
     *
     * @param string $fieldName Field name
     * @param bool $hierarchical True to fetch value from the parent company
     * @param mixed $default Default value
     * @return mixed Field value
     */
    public function getDynamicFieldValue($fieldName, $hierarchical = false, $default = '');

    /**
     * Set the field value
     *
     * @since 2.0.0
     *
     * @param string $fieldName Field name
     * @param mixed $value Field value
     * @return self
     */
    public function setDynamicFieldValue($fieldName, $value);

    /**
     * Retrieve static field value by field name
     *
     * @since 2.1.0
     *
     * @param string $fieldName Field name
     * @param bool $hierarchical True to fetch value from the parent company
     * @return mixed Field value
     */
    public function getStaticFieldValue($fieldName, $hierarchical = false);

    /**
     * Retrieve company field value
     *
     * @since 2.1.0
     *
     * @param string $fieldName Field name
     * @param bool $hierarchical True to fetch the value from the parent company if it's empty
     * @param null $deprecated 04.2018 Legacy
     * @return mixed Field value
     */
    public function getFieldValue($fieldName, $hierarchical = false, $deprecated = null);
}