<?php
/**
 * @module companiesLoyaltySuite
 * @title Companies
 * @description Manage Loyalty Suite companies
 * @version 2.9.7
 * @priority 4
 * @depends consumersLoyaltySuite 3.5.5
 */
class companiesLoyaltySuite extends LS\Module
{
    /** @var LSCompaniesHelper */
    protected $helper;

    /**
     * @since 2.1.5
     * @return string
     */
    public function getVersion()
    {
        return '2.9.7';
    }

    /**
     * @since 2.1.5
     * @return string
     */
    public function getCapability()
    {
        return 'ls_companies';
    }

    /**
     * @since 2.1.5
     * @return array
     */
    public function setDepends()
    {
        return [
            'consumersLoyaltySuite' => '3.5.5'
        ];
    }

    /**
     * Retrieve module title
     * @since 1.7.0
     * @return string Module title
     */
    public function getTitle()
    {
        return __('Companies', 'ls');
    }

    /**
     * Retrieve module description
     * @since 1.7.0
     * @return string Module description
     */
    public function getDescription()
    {
        return __('Module to handle data for Dealers, companies.', 'ls');
    }

    /**
     * Main function to load models, create actions and filters, init variables
     * @since 1.0
     * @internal
     */
    public function load()
    {
        require_once __DIR__ . '/CompanyInterface.php';
        require_once __DIR__ . '/Company.php';
        require_once __DIR__ . '/LSCompaniesHelper.php';

        // Consumers integration
        add_action('consumerStaticFields', [$this, 'consumerStaticFields']);
        add_filter('consumerFixedFieldsList', [$this, 'consumerFixedFieldsList'], 10, 2);
        add_filter('consumerDynamicFields', [$this, 'consumerDynamicFields']);
        add_filter('consumerAccountFields', [$this, 'consumerAccountFields']);
        add_filter('ls_consumers_custom_filter_companyId', [$this, 'consumersCustomFilter']);
        add_filter('ls_consumers_pwd_reset_request_content', [$this, 'appendCompanyShortcodes'], 10, 2);
        add_filter('ls_consumers_pwd_reset_content', [$this, 'appendCompanyShortcodes'], 10, 2);
        add_filter('ls_consumers_verification_email_content', [$this, 'appendCompanyShortcodes'], 10, 2);
        add_filter('ls_consumers_auth_email_content', [$this, 'appendCompanyShortcodes'], 10, 2);
        add_filter('ls_consumers_quit_email_content', [$this, 'appendCompanyShortcodes'], 10, 2);

        // Com.Center integration
        add_filter('ls_cc_filters', [$this, 'initRecipients']);
    }

    /**
     * Create shortcodes
     * @since 1.4
     * @internal
     */
    public function initShortcodes()
    {
        add_shortcode('companyName', [$this, 'companyNameShortcode']);
        add_shortcode('company', [$this, 'companyShortcode']);
        add_shortcode('company-page', [$this, 'companyPageShortcode']);
    }

    /**
     * Retrieve companies module helper
     * @since 2.9.2
     * @return LSCompaniesHelper
     */
    public function helper(): LSCompaniesHelper
    {
        if ($this->helper === null) {
            $this->helper = new LSCompaniesHelper();
        }

        return $this->helper;
    }

    /**
     * Retrieve default company fields
     * @since 1.0
     * @return LS\FieldAbstract[]
     */
    protected function getCompanyFields()
    {
        return array_merge(
            LSCompaniesHelper::getDefaultCompanyFields(),
            LSCompaniesHelper::getDynamicCompanyFields()
        );
    }

    /**
     * Retrieve company name
     *
     * @since 1.6.0
     * @internal
     *
     * @param array $atts Attributes: 'company_id' with the ID of company OR leave empty to get company for the current consumer
     * @return string Company name
     */
    public function companyNameShortcode($atts)
    {
        return $this->companyShortcode(
            array_merge(is_array($atts) ? $atts : [], ['field' => 'companyName'])
        );
    }

    /**
     * Shortcode [company]
     * Generate shortcodes depends on company fields.
     * Examples: [company field="companyName"], [company field="companyPhone" company_id="5"]
     *
     * @since 1.0
     * @internal
     *
     * @param array $atts Attributes: 'field' - field name (required); 'company_id' - ID of company to retrieve data (optional), otherwise company of the current consumer will be used
     * @param string $content Text to display if value for specific field can not be obtained
     * @return string Specific company data depends on field name
     */
    public function companyShortcode($atts, $content = '')
    {
        $fieldName = $atts['field'] ?? '';
        if (empty($fieldName)) {
            return esc_html(do_shortcode($content));
        }

        $field = false;

        foreach ($this->getCompanyFields() as $cfName => $cf) {
            if ($cfName == $fieldName) {
                $field = $cf;
                break;
            }
        }

        if ($field) {

            if (!$field->getArg('allowShortcode', true)) {
                return esc_html(do_shortcode($content));
            }

            $company =
                isset($atts['company_id']) && $atts['company_id'] > 0
                    ? LSCompaniesHelper::getCompany((int) $atts['company_id'], $field->getArg('fetchFromParent'))
                    : $this->getCompanyOfAuthorizedConsumer();

            if ($company) {

                $value = $company->getFieldValue($fieldName, $field->getArg('fetchFromParent'));
                $value = in_array($field->getType(), ['text', 'textarea']) ? do_shortcode(preg_replace('/\s\s+/', ' ', $value)) : $value;

                $field->setValue($value);

                if ($fieldName == 'companyName') {
                    $content = $company->getCompanyName(true);
                } elseif ($fieldName == 'companyLogo') {
                    $logo = $field->getValue();
                    $content = empty($logo) ? '' : '<img class="ls-company-logo" src="' . esc_url($logo) . '" alt="" />';
                } else {
                    $content = $field->getDisplayValue();
                }
            }

        } else {
            $content = ls_kses($content);
        }

        return $content;
    }

    /**
     * Shortcode [company-page]
     * Shows link to the company page
     *
     * @since 2.2.0
     *
     * @param array $atts Attributes:
     * - companyId -> company Id
     * - class -> button class name
     * @param string $content Text in the button
     * @return string HTML link
     */
    public function companyPageShortcode($atts, $content)
    {
        $company =
            isset($atts['company_id']) && $atts['company_id'] > 0
                ? LSCompaniesHelper::getCompany((int) $atts['company_id'])
                : $this->getCompanyOfAuthorizedConsumer();

        $url = $company ? $company->getCompanyPageLink(true) : '';

        $class = isset($atts['class']) && !empty($atts['class']) ? ' class="' . esc_attr($atts['class']) . '"' : '';

        return empty($content) ? $url : '<a href="' . $url . '"' . $class . '>' . do_shortcode($content) . '</a>';
    }

    /**
     * Add support for field "Company" to consumer static fields with a list of available companies
     *
     * @since 1.2
     * @internal
     *
     * @param \LS\FieldAbstract[] $fields A list of consumer static fields
     * @return \LS\FieldAbstract[] List of consumer static fields
     */
    public function consumerStaticFields($fields)
    {
        $fields['companyId'] = new LS\Field\Select('companyId', __('Company', 'ls'),
            static function () {
                return LSCompaniesHelper::getCompaniesList(true, true);
            }, [
                'allowEmpty' => true,
                'emptyText'  => '—'
            ]);

        return $fields;
    }

    /**
     * Always display the following fields in the form(s)
     *
     * @since 1.6
     * @internal
     *
     * @param \LS\FieldAbstract[] $fields A list of fixed fields
     * @param string $formName Form name
     * @return \LS\FieldAbstract[] A list of fixed fields
     */
    public function consumerFixedFieldsList($fields, $formName)
    {
        if (in_array($formName, ['adminAdd', 'adminEdit'])) {
            $fields = array_merge($fields, ['companyId']);
        }

        return $fields;
    }

    /**
     * Add "Company location" field to the consumer profile
     *
     * @since 1.9.0
     * @internal
     *
     * @param \LS\FieldAbstract[] $fields
     * @return LS\FieldAbstract[]
     */
    public function consumerDynamicFields($fields)
    {
        $fields['companyLocation'] = new LS\Field\Select('companyLocation', __('Company location', 'ls'));

        return $fields;
    }

    /**
     * Customize values for the company location on the consumer account form
     *
     * @since 1.9.0
     *
     * @param \LS\FieldAbstract[]
     * @return \LS\FieldAbstract[]
     */
    public function consumerAccountFields($fields)
    {
        if (isset($fields['companyLocation']) && ($company = $this->getCompanyOfAuthorizedConsumer()) !== false) {

            $locations = LSCompaniesHelper::getCompanyLocations($company->getCompanyId());

            // assign company location the current consumer..
            if (!empty($locations)) {

                /** @var LS\Field\Select $field */
                $field = $fields['companyLocation'];
                $field->setOptions($locations);

                // ..otherwise hide the company locations field
            } else {
                unset($fields['companyLocation']);
            }
        }

        return $fields;
    }

    /**
     * Change query to retrieve a list of consumers
     *
     * @since 2.0.2
     * @internal
     *
     * @param array $filter Filter query
     * @return array Array ['filterQuery' => filter query, 'joins' => an array of extra joins]
     */
    public function consumersCustomFilter($filter)
    {
        $condition = $filter['c'] ?? '';
        $value = $filter['s_companyId'] ?? 0;
        $value = !is_array($value) && $value > 0 ? LSCompaniesHelper::includeChildrenCompaniesIds((int) $value) : $value;

        return [
            'filterQuery' => $this->generateQueryFromFilter('c.companyId', $condition, $value),
            'joins'       => ''
        ];
    }

    /**
     * Retrieve company of the current consumer
     * @since 1.7.0
     * @return false|\LS\Company Company object or false on failure
     */
    public static function getCompanyOfAuthorizedConsumer()
    {
        static $companyOfCurrentConsumer;

        if ($companyOfCurrentConsumer === null) {
            if (($consumer = consumersLoyaltySuite::getAuthorizedConsumer()) != false && $consumer->getCompanyId() > 0) {
                $companyOfCurrentConsumer = LSCompaniesHelper::getCompany($consumer->getCompanyId());
            } else {
                $companyOfCurrentConsumer = false;
            }
        }

        return $companyOfCurrentConsumer;
    }

    /**
     * Retrieve URL to edit company
     *
     * @since 2.4.0
     * @deprecated 02.2019 Moved to admin
     *
     * @param int $companyId Company Id
     * @param string $referer Referer
     * @return string URL
     */
    public static function companyEditLink($companyId, $referer = '')
    {
        _deprecated_function(__FUNCTION__, '2.7.0', 'Use companiesAdminLoyaltySuite::companyEditLink()');

        return companiesAdminLoyaltySuite::companyEditLink($companyId, $referer);
    }

    /**
     * Init recipients conditions
     *
     * @since 2.4.0
     * @internal
     *
     * @param LSConditionalEmailFilter[] $recipients Array of recipients conditions
     * @return LSConditionalEmailFilter[] Updated array of recipients conditions
     */
    public function initRecipients($recipients)
    {
        $recipients['cmp'] =
            (new LSConditionalEmailFilter('cmp'))
                ->setForm(static function ($selected, $ident) {
                    (new LS\Template(__DIR__ . '/views/backend'))
                        ->assign('selected', $selected)
                        ->assign('ident', $ident)
                        ->render('admin-com-center-email.phtml');
                })
                ->setFilter(static function ($recipients, $ident) {

                    $companiesIds = wp_parse_id_list($ident);

                    foreach ($companiesIds as $companyId) {
                        $companiesIds = array_merge($companiesIds, LSCompaniesHelper::getChildrenCompaniesIdsTo($companyId));
                    }

                    $companiesIds = array_unique($companiesIds);

                    return array_filter($recipients, static function(int $consumerId) use ($companiesIds) {
                        return in_array((int) \LS\ConsumersHelper::getConsumerFieldValue($consumerId, 'companyId', false), $companiesIds);
                    });
                })
                ->setColumn(static function ($ident) {

                    $companies = [];

                    foreach (wp_parse_id_list($ident) as $companyId) {
                        $company = LSCompaniesHelper::getCompany($companyId, false);
                        if ($company) {
                            $companies[] = '<a href="' . companiesAdminLoyaltySuite::companyEditLink($companyId) . '" target="_blank">' . \LS\Library::substrDots($company->getCompanyName(), 0, 30) . '</a>';
                        }
                    }

                    return (empty($companies) ? '—' : implode('<br />', $companies));
                });

        return $recipients;
    }

    /**
     * Format company shortcodes in the consumer system emails
     *
     * @since 2.8.0
     * @internal
     *
     * @param string $content Content
     * @param \LS\ConsumerInterface $consumer Consumer to get the company Id
     * @return string Formatted content
     */
    public function appendCompanyShortcodes(string $content, \LS\ConsumerInterface $consumer)
    {
        return $consumer->getCompanyId() > 0
            ? LSCompaniesHelper::prepareCompanyShortcodes($consumer->getCompanyId(), $content)
            : $content;
    }
}