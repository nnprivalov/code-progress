<?php
class companiesLoyaltySuiteUpdate implements \LS\UpdateInterface
{
    /**
     * @param \LS\Module $module
     * @param string $oldVersion
     * @param string $newVersion
     */
    public function update(\LS\Module $module, string $oldVersion, string $newVersion)
    {
        $wpdb = LS()->wpdb;

        // 1.6.8
        if (version_compare($oldVersion, '1.6.8', '<')) {

            $query = sprintf("SELECT companyId, companyLogo, companyLogoSmall FROM {$wpdb->prefix}ls_companies");
            foreach ($wpdb->get_results($query) as $result) {

                if (!empty($result->companyLogo)) {
                    $picture = empty($result->companyLogo) ? '' : maybe_unserialize($result->companyLogo);
                    $picture = is_array($picture) && isset($picture['src']) ? $picture['src'] : '';
                    $wpdb->update($wpdb->prefix . 'ls_companies', ['companyLogo' => $picture], ['companyId' => $result->companyId]);
                }

                if (!empty($result->companyLogoSmall)) {
                    $picture = empty($result->companyLogoSmall) ? '' : maybe_unserialize($result->companyLogoSmall);
                    $picture = is_array($picture) && isset($picture['src']) ? $picture['src'] : '';
                    $wpdb->update($wpdb->prefix . 'ls_companies', ['companyLogoSmall' => $picture], ['companyId' => $result->companyId]);
                }
            }
        }

        // 1.6.9
        if (version_compare($oldVersion, '1.6.9', '<')) {
            $wpdb->query(sprintf("ALTER TABLE `{$wpdb->prefix}ls_companies`
            CHANGE COLUMN `companyName` `companyName` VARCHAR(100) NOT NULL AFTER `companyId`,
            CHANGE COLUMN `companyWebsite` `companyWebsite` VARCHAR(2000) NOT NULL AFTER `companyType`,
            CHANGE COLUMN `companySlogan` `companySlogan` VARCHAR(500) NOT NULL AFTER `companyWebsite`,
            CHANGE COLUMN `companyAddress` `companyAddress` VARCHAR(300) NOT NULL AFTER `companyGoogleMap`,
            CHANGE COLUMN `companyPostal` `companyPostal` VARCHAR(10) NOT NULL AFTER `companyAddress`,
            CHANGE COLUMN `companyCity` `companyCity` VARCHAR(60) NOT NULL AFTER `companyPostal`,
            CHANGE COLUMN `companyState` `companyState` VARCHAR(200) NOT NULL AFTER `companyCity`,
            CHANGE COLUMN `companyCountry` `companyCountry` VARCHAR(2) NOT NULL AFTER `companyState`,
            CHANGE COLUMN `companyLogo` `companyLogo` VARCHAR(2000) NOT NULL AFTER `companyServiceEmail`,
            CHANGE COLUMN `companyLogoSmall` `companyLogoSmall` VARCHAR(2000) NOT NULL AFTER `companyLogo`"));
        }

        // 1.7.0
        if (version_compare($oldVersion, '1.7.0', '<')) {
            $wpdb->query(sprintf("ALTER TABLE `{$wpdb->prefix}ls_companies` DROP COLUMN `companyIncentiveCodes`"));
            $wpdb->query(sprintf("ALTER TABLE `{$wpdb->prefix}ls_companies` ADD COLUMN `companyPageId` BIGINT(20) UNSIGNED NULL DEFAULT NULL AFTER `companyCreateDate`"));
        }

        // 1.8.0
        if (version_compare($oldVersion, '1.8.0', '<')) {
            $wpdb->query(sprintf("CREATE TABLE `{$wpdb->prefix}ls_companies_messages` (
                    `companyId` INT(10) UNSIGNED NOT NULL,
                    `keyId` TINYINT(3) NOT NULL,
                    `message` TEXT NOT NULL,
                    `notice` VARCHAR(500) NOT NULL,
                    PRIMARY KEY (`companyId`, `keyId`)
            ) %s ENGINE=InnoDB", LS\Library::getCharsetCollate()));
        }

        // 1.9.0
        if (version_compare($oldVersion, '1.9.0', '<')) {
            $wpdb->query(sprintf("ALTER TABLE `{$wpdb->prefix}ls_companies` ADD COLUMN `numberOfEmployees` INT(10) UNSIGNED null DEFAULT '0' AFTER `companyPageId`"));
            $wpdb->query(sprintf("ALTER TABLE `{$wpdb->prefix}ls_companies` ADD COLUMN `locations` LONGTEXT null DEFAULT null AFTER `numberOfEmployees`"));
        }

        // 1.9.2
        if (version_compare($oldVersion, '1.9.2', '<')) {
            $wpdb->query(sprintf("ALTER TABLE `{$wpdb->prefix}ls_companies` CHANGE COLUMN `locations` `locations` TEXT null AFTER `numberOfEmployees`"));
        }

        // 1.9.6
        if (version_compare($oldVersion, '1.9.6', '<')) {
            $wpdb->query(sprintf("UPDATE `{$wpdb->prefix}ls_companies` SET companyLogo = companyLogoSmall"));
            $wpdb->query(sprintf("ALTER TABLE `{$wpdb->prefix}ls_companies`	DROP COLUMN `companyLogoSmall`"));
        }

        // 2.0.0
        if (version_compare($oldVersion, '2.0.0', '<')) {

            $wpdb->query(sprintf("ALTER TABLE `{$wpdb->prefix}ls_companies`
                ADD COLUMN `parentCompanyId` INT(10) UNSIGNED NOT NULL DEFAULT '0' AFTER `companyId`,
                ADD INDEX `parentCompanyId` (`parentCompanyId`)"));

            $tb = $wpdb->get_var(sprintf("SHOW TABLES LIKE '{$wpdb->prefix}ls_companies_fields_values'"));
            if (empty($tb)) {
                $wpdb->query(sprintf("CREATE TABLE `{$wpdb->prefix}ls_companies_fields_values` (
                    `companyId` INT(10) UNSIGNED NOT NULL,
                    `fieldName` VARCHAR(64) NOT NULL,
                    `value` TEXT NOT NULL,
                    PRIMARY KEY (`companyId`, `fieldName`),
                    INDEX `companyId` (`companyId`)
                ) %s ENGINE=InnoDB", LS\Library::getCharsetCollate()));
            }

            require_once __DIR__ . '/LSCompaniesHelper.php';

            $data = $wpdb->get_results(sprintf("SELECT keyId, notice FROM {$wpdb->prefix}ls_companies_messages GROUP BY keyId"));
            foreach ($data as $item) {
                LSCompaniesHelper::saveDynamicField((object) [
                    'type'            => 'textarea',
                    'name'            => 'company-message-' . $item->keyId,
                    'title'           => 'Company message ' . $item->keyId,
                    'description'     => $item->notice,
                    'fetchFromParent' => true
                ]);
            }

            $messages = $wpdb->get_results(sprintf("SELECT * FROM {$wpdb->prefix}ls_companies_messages"));
            foreach ($messages as $message) {
                LSCompaniesHelper::saveDynamicFieldValue(['company-message-' . $message->keyId => $message->message], $message->companyId);
            }

            $wpdb->query(sprintf("DROP TABLE {$wpdb->prefix}ls_companies_messages"));

            get_role('author')->add_cap('ls_companies_manage');
            get_role('editor')->add_cap('ls_companies_manage');
            get_role('administrator')->add_cap('ls_companies_manage');
            get_role('administrator')->add_cap('ls_companies_settings');
        }

        // 2.2.0
        if (version_compare($oldVersion, '2.2.0', '<')) {

            $wpdb->query(sprintf("ALTER TABLE `{$wpdb->prefix}ls_companies` CHANGE COLUMN `companyVerified` `companyActive` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' AFTER `companyLogo`"));

            $module->setOption('incChildren', true);
            $module->setOption('incConsumers', true);
        }

        // 2.2.3
        if (version_compare($oldVersion, '2.2.3', '<')) {
            $module->deleteOption('incChildren');
            $module->deleteOption('incConsumers');
        }

        // 2.5.0
        if (version_compare($oldVersion, '2.5.0', '<')) {
            $wpdb->query(sprintf("ALTER TABLE `{$wpdb->prefix}ls_companies`
                DROP COLUMN `companyType`,
                DROP COLUMN `companyGoogleMap`,
                DROP COLUMN `companyWorkDaysMoFr`,
                DROP COLUMN `companyWorkDaysSa`,
                DROP COLUMN `companyWorkDaysSu`"));
        }

        // 2.8.0
        if (version_compare($oldVersion, '2.8.0', '<')) {
            $module->setOptions(['version' => '2.8.0']);
        }

        // 2.9.7
        if (version_compare($oldVersion, '2.9.7', '<')) {
            $wpdb->query("ALTER TABLE `{$wpdb->prefix}ls_companies` ADD COLUMN `companyInternalName` VARCHAR(100) NOT null AFTER `companyName`");
            $wpdb->query("UPDATE {$wpdb->prefix}ls_companies SET companyInternalName = ''");
        }
    }
}