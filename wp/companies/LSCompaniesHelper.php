<?php
class LSCompaniesHelper
{
    /** Static fields + dynamic fields (ls_companies + ls_companies_fields_values) */
    const DT_ANY = 0;

    /** Static fields only (ls_companies) */
    const DT_STATIC = 1;

    /** Dynamic fields only (ls_companies_fields_values) */
    const DT_DYNAMIC = 2;

    /**
     * Delete module tables
     * @since 1.0
     */
    public static function deleteTables()
    {
        $wpdb = LS()->wpdb;

        $wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}ls_companies");
        $wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}ls_companies_fields_values");
    }

    /**
     * Delete company
     *
     * @since 2.2.0
     *
     * @param \LS\CompanyInterface $company Company to delete
     * @param bool $deleteChildren True to delete children companies
     * @param bool $quitConsumers True to delete/wipe related consumers
     * @return bool True if deleted
     */
    public static function deleteCompany($company, $deleteChildren = true, $quitConsumers = true): bool
    {
        // @deprecated 03.2019 Legacy validations
        // @TODO Strict type hints after 09.2019
        $deleteChildren = \LS\Library::toBool($deleteChildren);
        $quitConsumers = \LS\Library::toBool($quitConsumers);

        // @deprecated 07.2019
        if (is_numeric($company)) {
            // @TODO Uncomment after 09.2019
            //_doing_it_wrong(__FUNCTION__, '1st param should be instance of CompanyInterface', '2.9.5');
            $companyId = $company;
            $company = self::getCompany($companyId, false);
            if (!$company) {
                return false;
            }
        } else {
            $companyId = $company->getCompanyId();
        }

        $wpdb = LS()->wpdb;

        \LS\TransactionManager::start();

        try {

            if ($wpdb->delete($wpdb->prefix . 'ls_companies', ['companyId' => $companyId], '%d') === false) {
                throw new Exception('Can not delete company');
            }

            if ($wpdb->delete($wpdb->prefix . 'ls_companies_fields_values', ['companyId' => $companyId], '%d') === false) {
                throw new Exception('Can not delete company field values');
            }

            foreach (self::getChildrenCompaniesIdsTo($companyId) as $childCompanyId) {

                $childCompany = self::getCompany($childCompanyId, false);

                if ($childCompany) {
                    if ($deleteChildren) {
                        self::deleteCompany($childCompany, true, $quitConsumers);
                    } else {
                        $childCompany->removeParentCompany();
                        self::saveCompany($childCompany);
                    }
                }
            }

            if ($quitConsumers) {

                /** @var consumersLoyaltySuite $consumersModule */
                $consumersModule = LS()->getModule('consumers');

                foreach (\LS\ConsumersHelper::getConsumersIdsByCompanyId($companyId) as $consumerId) {
                    $consumer = $consumersModule->helper()->getConsumer($consumerId);
                    if ($consumer) {
                        $consumersModule->quitConsumer($consumer);
                    }
                }
            }

            \LS\TransactionManager::commit();

            $success = true;

        } catch (Exception $e) {
            $success = false;
            \LS\TransactionManager::rollback();
            _doing_it_wrong(__FUNCTION__, $e->getMessage(), '2.8.0');
        }

        if ($success) {
            do_action('ls_company_deleted', $company, $deleteChildren, $quitConsumers);
        }

        return $success;
    }

    /**
     * Wipe company
     *
     * @since 2.2.0
     * @deprecated 04.2019 Legacy
     *
     * @param int $companyId Company Id
     * @param bool $wipeChildren True to delete children companies
     * @param bool $wipeConsumers True to wipe related consumers
     * @return bool True if wiped
     */
    public static function wipeCompany($companyId, $wipeChildren = true, $wipeConsumers = true): bool
    {
        _deprecated_function(__FUNCTION__, '2.8.1', 'Use delete instead of wipe');

        // @deprecated 03.2019 Legacy validations
        // @TODO Strict type hints after 09.2019
        $companyId = \LS\Library::toInt($companyId);
        $wipeChildren = \LS\Library::toBool($wipeChildren);
        $wipeConsumers = \LS\Library::toBool($wipeConsumers);

        \LS\TransactionManager::start();

        try {

            $company = self::getCompany($companyId, false);

            if (!$company) {
                throw new Exception('Invalid company Id');
            }

            $company
                ->setCompanyName('')
                ->markAsInactive();

            if (!self::saveCompany($company)) {
                throw new Exception('Can not wipe company');
            }

            if ($wipeChildren) {
                foreach (self::getChildrenCompaniesIdsTo($companyId) as $childCompanyId) {
                    self::wipeCompany($childCompanyId, $wipeChildren, $wipeConsumers);
                }
            }

            if ($wipeConsumers) {
                foreach (\LS\ConsumersHelper::getConsumersIdsByCompanyId($companyId) as $consumerId) {
                    \LS\ConsumersHelper::wipeConsumer($consumerId);
                }
            }

            do_action('ls_company_wiped', $company, $wipeChildren, $wipeConsumers);

            \LS\TransactionManager::commit();

        } catch (Exception $e) {

            \LS\TransactionManager::rollback();
            _doing_it_wrong(__FUNCTION__, $e->getMessage(), '2.8.0');

            return false;
        }

        return true;
    }

    /**
     * Activate company consumers
     * @since 2.9.6
     * @param \LS\CompanyInterface $company
     */
    private static function activateCompanyConsumers(\LS\CompanyInterface $company)
    {
        foreach (\LS\ConsumersHelper::getConsumersIdsByCompanyId($company->getCompanyId()) as $consumerId) {
            $consumer = \LS\ConsumersHelper::getConsumer($consumerId);
            if ($consumer) {
                \LS\ConsumersHelper::activateConsumer($consumer);
                \LS\ConsumersHelper::insertLog('profile', $consumerId, 'admin-activate');
            }
        }
    }

    /**
     * Deactivate company consumers
     * @since 2.9.6
     * @param \LS\CompanyInterface $company
     */
    private static function deactivateCompanyConsumers(\LS\CompanyInterface $company)
    {
        foreach (\LS\ConsumersHelper::getConsumersIdsByCompanyId($company->getCompanyId()) as $consumerId) {
            $consumer = \LS\ConsumersHelper::getConsumer($consumerId);
            if ($consumer) {
                \LS\ConsumersHelper::deactivateConsumer($consumer);
                \LS\ConsumersHelper::insertLog('profile', $consumerId, 'admin-deactivate');
            }
        }
    }

    /**
     * Deactivate children companies of the specified company
     * @since 2.9.6
     * @param \LS\CompanyInterface $company
     */
    private static function deactivateCompanyChildren(\LS\CompanyInterface $company)
    {
        foreach (self::getChildrenCompaniesIdsTo($company->getCompanyId(), true) as $childCompanyId) {
            $childCompany = self::getCompany($childCompanyId, false);
            if ($childCompany) {
                self::deactivateCompany($childCompany);
            }
        }
    }

    /**
     * Activate company
     *
     * @since 2.2.0
     *
     * @param \LS\CompanyInterface $company Company to activate
     * @return bool True if company activated
     */
    public static function activateCompany($company): bool
    {
        // @deprecated 07.2019
        if (is_numeric($company)) {
            // @TODO Uncomment after 09.2019
            //_doing_it_wrong(__FUNCTION__, '1st param should be instance of CompanyInterface', '2.9.5');
            $companyId = $company;
            $company = self::getCompany($companyId, false);
            if (!$company) {
                return false;
            }
        }

        $company->markAsActive();

        \LS\TransactionManager::start();

        try {

            if (!self::saveCompany($company)) {
                throw new Exception('Can not activate company');
            }

            self::activateCompanyConsumers($company);

            \LS\TransactionManager::commit();

            $success = true;

        } catch (Exception $e) {
            $success = false;
            \LS\TransactionManager::rollback();
            _doing_it_wrong(__FUNCTION__, $e->getMessage(), '2.8.0');
        }

        if ($success) {
            do_action('ls_company_activated', $company);
        }

        return $success;
    }

    /**
     * Deactivate company
     *
     * @since 2.2.0
     *
     * @param \LS\CompanyInterface $company Company to deactivate
     * @return bool True if company deactivated
     */
    public static function deactivateCompany($company): bool
    {
        // @deprecated 07.2019
        if (is_numeric($company)) {
            // @TODO Uncomment after 09.2019
            //_doing_it_wrong(__FUNCTION__, '1st param should be instance of CompanyInterface', '2.9.5');
            $companyId = $company;
            $company = self::getCompany($companyId, false);
            if (!$company) {
                return false;
            }
        }

        $company->markAsInactive();

        \LS\TransactionManager::start();

        try {

            if (!self::saveCompany($company)) {
                throw new Exception('Can not deactivate company');
            }

            self::deactivateCompanyChildren($company);
            self::deactivateCompanyConsumers($company);

            \LS\TransactionManager::commit();

            $success = true;

        } catch (Exception $e) {
            $success = false;
            \LS\TransactionManager::rollback();
            _doing_it_wrong(__FUNCTION__, $e->getMessage(), '2.8.0');
        }

        if ($success) {
            do_action('ls_company_deactivated', $company);
        }

        return $success;
    }

    /**
     * Retrieve company
     *
     * @since 1.0
     *
     * @param int $companyId Company ID
     * @param bool $hierarchical True to get parent company
     * @return false|\LS\Company Company object or false on failure
     */
    public static function getCompany($companyId, $hierarchical = true)
    {
        // @deprecated 03.2019 Legacy validations
        // @TODO Strict type hints after 09.2019
        $companyId = \LS\Library::toInt($companyId);
        $hierarchical = \LS\Library::toBool($hierarchical);

        if ($companyId < 0) {
            return false;
        }

        $wpdb = LS()->wpdb;
        $record = $wpdb->get_row(sprintf("SELECT * FROM {$wpdb->prefix}ls_companies WHERE companyId = %d LIMIT 1", $companyId));

        if (!$record) {
            return false;
        }

        $company = new \LS\Company($record);

        $company->setDynamicData(
            self::getCompanyDynamicValues($companyId)
        );

        if ($hierarchical && $company->hasParent()) {

            $parentCompany = self::getCompany($company->getParentCompanyId(), $hierarchical);

            if ($parentCompany) {
                $company->setParentCompany($parentCompany);
            } else {
                _doing_it_wrong(__FUNCTION__, 'Parent company not defined but for companyId=' . $companyId, '2.9.3');
            }
        }

        return $company;
    }

    /**
     * Retrieve a list of companies
     *
     * @since 1.0
     *
     * @param bool $activeOnly True to return only active companies
     * @param bool $hierarchical True to retrieve in hierarchical view (parents-children)
     * @return array[int]string A list of companies
     */
    public static function getCompaniesList($activeOnly = true, $hierarchical = false)
    {
        // @deprecated 03.2019 Legacy validations
        // @TODO Strict type hints after 09.2019
        $activeOnly = \LS\Library::toBool($activeOnly);
        $hierarchical = \LS\Library::toBool($hierarchical);

        $result = [];
        $data = self::paginateCompanies(0);

        /** @var \LS\Company $company */
        foreach ($data['rows'] as $company) {
            if (!$activeOnly || $company->isCompanyActive()) {
                $result[$company->getCompanyId()] = ($hierarchical ? str_repeat('&#8212; ', self::getCompanyLevel($company->getCompanyId(), $data['relations'])) : '') . $company->getCompanyName();
            }
        }

        return $result;
    }

    /**
     * Paginate companies by specified conditions and filters
     *
     * @since 2.5.10
     *
     * @param int $itemsPerPage Number of companies to get
     * @param int $page Page number for pagination. To get all companies without pagination set this value to null
     * @param string $orderBy Field to order
     * @param string $order Order direction
     * @param string $searchTerm Search query
     * @param array $filterTerms Filters
     *
     * @return array Array(
     *   'rows' - companies list,
     *   'totalCount' - total number of found companies,
     *   'totalPages' - total number of pages (for pagination),
     *   'itemsPerPage' - number of companies per page
     * )
     */
    public static function paginateCompanies($itemsPerPage = 20, $page = 1, $orderBy = '', $order = '', $searchTerm = '', $filterTerms = [])
    {
        // @deprecated 03.2019 Legacy validations
        // @TODO Strict type hints after 09.2019
        $itemsPerPage = \LS\Library::toInt($itemsPerPage);
        $page = \LS\Library::toInt($page, true);

        $wpdb = LS()->wpdb;
        $order = empty($order) ? 'ASC' : $order;
        $whereConditions = [];
        $joins = [];
        $orderField = '';
        $staticFields = self::getDefaultCompanyFields();
        $dynamicFields = self::getDynamicCompanyFields();

        if (isset($staticFields[$orderBy])) {
            /** @var LS\FieldAbstract $f */
            $f = $staticFields[$orderBy];
            $orderField = 'c.' . $orderBy;
            if ($f->getType() == 'value') {
                $orderField = 'CAST(' . $orderField . ' AS DECIMAL)';
            }
        } elseif (isset($dynamicFields[$orderBy])) {
            /** @var LS\FieldAbstract $f */
            $f = $dynamicFields[$orderBy];
            $joins[] = sprintf("LEFT JOIN {$wpdb->prefix}ls_companies_fields_values as cmpfv ON cfv.companyId = c.companyId AND cfv.fieldName = '%s'", esc_sql($orderBy));
            $orderField = 'cfv.value';
            if ($f->getType() == 'value') {
                $orderField = 'CAST(' . $orderField . ' AS DECIMAL)';
            }
        } elseif ($orderBy === 'ac' || empty($orderBy)) {
            $orderField = 'c.companyName';
        }

        if (!empty($searchTerm)) {

            $columns = [];

            /** @var LS\FieldAbstract $f */
            foreach ($staticFields as $fieldName => $f) {
                if (in_array($f->getType(), ['text', 'textarea', 'email', 'url', 'value', 'phone', 'label'])) {
                    $columns[] = 'c.' . esc_sql($fieldName);
                }
            }

            $joins[] = sprintf("LEFT JOIN {$wpdb->prefix}ls_companies_fields_values as cfv_s ON cfv_s.companyId = c.companyId");

            /** @var LS\FieldAbstract $f */
            foreach ($dynamicFields as $fieldName => $f) {
                if (in_array($f->getType(), ['text', 'textarea', 'email', 'url', 'value', 'phone', 'label']) && !$f->isRetype()) {
                    $columns[] = 'cfv_s.fieldName = "' . esc_sql($fieldName) . '" AND cfv_s.value';
                }
            }

            $searchConditions = [];
            $searchConditions[] = sprintf("c.companyId = %d", $searchTerm);
            foreach ($columns as $c) {
                $searchConditions[] = sprintf("%s LIKE '%%%s%%'", $c, esc_sql(str_replace('\\', '\\\\', $searchTerm)));
            }

            if (!empty($searchConditions)) {
                $whereConditions[] = implode(' OR ', $searchConditions);
            }
        }

        if (!empty($filterTerms)) {

            $filtersWhere = [];

            foreach ($filterTerms as $n => $filter) {

                if (!isset($filter['r'])) {
                    continue;
                }

                $record = '';
                $field = $filter['r'];
                $condition = $filter['c'] ?? '';
                $value = $filter['t'] ?? '';
                $filterQuery = '';

                if (isset($staticFields[$field])) {
                    $record = 'c.' . $field;
                    if ($field == 'companyCreateDate') {
                        $value = is_numeric($value) ? $value : strtotime($value);
                    }
                } elseif (isset($dynamicFields[$field])) {
                    $joins[] = sprintf("LEFT JOIN {$wpdb->prefix}ls_companies_fields_values as cfv_f%d ON cfv_f%d.companyId = c.companyId AND cfv_f%d.fieldName = '%s'", $n, $n, $n, esc_sql($field));
                    $record = 'cfv_f' . $n . '.value';
                    /** @var LS\FieldAbstract $f */
                    $f = $dynamicFields[$field];
                    if ($f->getType() == 'select') {
                        $value = $filter['s_' . $field];
                    } elseif (in_array($f->getType(), ['date', 'datetime'])) {
                        $value = is_numeric($value) ? $value : strtotime($value);
                    }
                } elseif (has_filter('ls_companies_custom_filter_' . $field)) {
                    list($filterQuery, $extraJoins) = array_values(apply_filters('ls_companies_custom_filter_' . $field, $filter));
                    $joins = empty($extraJoins) ? $joins : array_merge($joins, $extraJoins);
                }

                if (!empty($record) && empty($filterQuery)) {
                    $filterQuery = LS\Library::generateQueryFromFilter($record, $condition, $value);
                }

                if (!empty($filterQuery)) {
                    $filtersWhere[] = $filterQuery;
                }
            }

            if (!empty($filtersWhere)) {
                $xor = isset($filterTerms['x']) && in_array($filterTerms['x'], ['AND', 'OR']) ? $filterTerms['x'] : 'AND';
                $whereConditions[] = '(' . implode(') ' . $xor . ' (', $filtersWhere) . ')';
            }
        }

        $where = empty($whereConditions) ? '' : sprintf(" WHERE (%s)", implode(') AND (', $whereConditions));
        $query = sprintf("SELECT c.companyId, c.parentCompanyId
                          FROM {$wpdb->prefix}ls_companies as c
                          %s %s
                          GROUP BY c.companyId
                          ORDER BY %s %s", implode(' ', $joins), $where, $orderField, $order);
        $companiesRelations = $wpdb->get_results($query, OBJECT_K);

        $companiesRelations = array_map(static function ($cr) {
            $cr->companyId = (int) $cr->companyId;
            $cr->parentCompanyId = (int) $cr->parentCompanyId;
            return $cr;
        }, $companiesRelations);

        $totalCount = count($companiesRelations);
        $itemsPerPage = $itemsPerPage > 0 ? $itemsPerPage : $totalCount;
        $totalPages = $totalCount > 0 && $itemsPerPage > 0 ? ceil($totalCount / $itemsPerPage) : 0;

        $companies = [];
        if ($totalCount > 0) {

            $groupCompaniesByParent = static function ($relations, $companyId = 0) use (&$groupCompaniesByParent) {

                $companies = [];

                foreach ($relations as $item) {
                    if ($item->parentCompanyId == $companyId) {
                        $companies = array_merge($companies, [(int) $item->companyId], $groupCompaniesByParent($relations, $item->companyId));
                    }
                }

                return $companies;
            };

            $companiesIds = $groupCompaniesByParent($companiesRelations);

            // add companies without parents to the result stack
            foreach ($companiesRelations as $relation) {
                if (!in_array($relation->companyId, $companiesIds)) {
                    $companiesIds[] = $relation->companyId;
                }
            }

            if ($page > 0 && $itemsPerPage > 0) {
                $companiesIds = array_slice($companiesIds, ($page - 1) * $itemsPerPage, $itemsPerPage);
            }

            if (!empty($companiesIds)) {

                $query = sprintf("SELECT c.*
                                  FROM {$wpdb->prefix}ls_companies as c
                                  %s 
                                  WHERE c.companyId IN (%s) 
                                  GROUP BY c.companyId
                                  ORDER BY FIELD(c.companyId, %s)", implode(' ', $joins), implode(',', $companiesIds), implode(',', $companiesIds));

                $companies = [];

                foreach ($wpdb->get_results($query) as $record) {
                    $companies[] = new \LS\Company($record);
                }

                $companies = apply_filters('ls_get_companies', $companies);
            }
        }

        return [
            'rows'         => $companies,
            'totalCount'   => $totalCount,
            'totalPages'   => $totalPages,
            'itemsPerPage' => $itemsPerPage,
            'relations'    => $companiesRelations
        ];
    }

    /**
     * Retrieve companies by specified conditions and filters
     *
     * @since 1.0.0
     *
     * @param string $searchTerm Search query
     * @param array $filterTerms Filters
     * @return \LS\Company[]
     */
    public static function getCompanies($searchTerm = '', $filterTerms = [])
    {
        return self::paginateCompanies(0, 0, '', '', $searchTerm, $filterTerms)['rows'];
    }

    /**
     * Retrieve company level depends on relations table
     *
     * @since 2.2.0
     *
     * @param int $companyId Company Id
     * @param array[int]int $relations Companies relations table (companyId => parentCompanyId)
     * @param int $level Recursion variable
     * @return int Level
     */
    public static function getCompanyLevel($companyId, $relations, $level = 0)
    {
        // @deprecated 03.2019 Legacy validations
        // @TODO Strict type hints after 09.2019
        $companyId = \LS\Library::toInt($companyId);
        $level = \LS\Library::toInt($level);

        // company without parents always should have level 0
        if (!isset($relations[$companyId])) {
            return 0;
        }

        return
            $relations[$companyId]->parentCompanyId > 0
                ? self::getCompanyLevel($relations[$companyId]->parentCompanyId, $relations, $level + 1)
                : $level;
    }

    /**
     * Retrieve ID's of the children companies
     *
     * @since 2.2.6
     *
     * @param int $companyId Parent company Id
     * @param bool $activeOnly True to get active consumers only
     * @param int[] $ids Internal param / not in use
     * @return int[] Company Id's
     */
    private static function getChildrenCompaniesIdsToInternal($companyId, $activeOnly = false, $ids = [])
    {
        // @deprecated 03.2019 Legacy validations
        // @TODO Strict type hints after 09.2019
        $companyId = \LS\Library::toInt($companyId);
        $activeOnly = \LS\Library::toBool($activeOnly);

        if ($companyId > 0) {

            $wpdb = LS()->wpdb;
            $activeWhere = $activeOnly ? "AND companyActive = 1" : '';
            $query = sprintf("SELECT companyId FROM {$wpdb->prefix}ls_companies WHERE parentCompanyId = %d %s", $companyId, $activeWhere);

            foreach (array_map('intval', $wpdb->get_col($query)) as $newId) {
                $ids = array_merge([$newId], self::getChildrenCompaniesIdsToInternal($newId, $activeOnly, $ids));
            }
        }

        return array_unique($ids);
    }

    /**
     * Retrieve ID's of the children companies
     *
     * @since 2.0.2
     *
     * @param int $companyId Parent company Id
     * @param bool $activeOnly True to get active consumers only
     * @return int[] Company Id's
     */
    public static function getChildrenCompaniesIdsTo($companyId, $activeOnly = false)
    {
        // @deprecated 03.2019 Legacy validations
        // @TODO Strict type hints after 09.2019
        $companyId = \LS\Library::toInt($companyId);
        $activeOnly = \LS\Library::toBool($activeOnly);

        return self::getChildrenCompaniesIdsToInternal($companyId, $activeOnly);
    }

    /**
     * Retrieve ID's of the child companies together with parent company Id
     *
     * @since 2.0.3
     *
     * @param int $companyId Parent company Id
     * @param bool $activeOnly True to get active consumers only
     * @return int[] Company Id's
     */
    public static function includeChildrenCompaniesIds($companyId, $activeOnly = false)
    {
        // @deprecated 03.2019 Legacy validations
        // @TODO Strict type hints after 09.2019
        $companyId = \LS\Library::toInt($companyId);
        $activeOnly = \LS\Library::toBool($activeOnly);

        return $companyId > 0 ? array_merge([(int) $companyId], self::getChildrenCompaniesIdsTo($companyId, $activeOnly)) : [];
    }

    /**
     * Retrieve default company fields
     * Note 1: name of each field should be available in the database table '{prefix}ls_companies'
     * Note 2: 'fetchFromParent' here is just to show a sign [hierarchy] on the page to edit company
     *
     * @since 1.0
     *
     * @return LS\FieldAbstract[]
     */
    public static function getDefaultCompanyFields()
    {
        static $fields;

        if ($fields === null) {

            $companiesList = static function () {
                return self::getCompaniesList(false, true);
            };

            $fields = [
                'companyId'           => new LS\Field\Value('companyId', 'ID', ['fixed' => true, 'disabled' => true]),
                'companyName'         => new LS\Field\Text('companyName', __('Company name', 'ls'), ['maxlength' => 100, 'fetchFromParent' => true]),
                'companyInternalName' => new LS\Field\Text('companyInternalName', __('Company internal name', 'ls'), ['maxlength' => 100]),
                'parentCompanyId'     => new LS\Field\Select('parentCompanyId', __('Parent company', 'ls'), $companiesList),
                'companyWebsite'      => new LS\Field\Url('companyWebsite', __('Company website', 'ls'), ['maxlength' => 2000, 'fetchFromParent' => true]),
                'companySlogan'       => new LS\Field\Text('companySlogan', __('Company slogan', 'ls'), ['maxlength' => 500, 'fetchFromParent' => true]),
                'companyAddress'      => new LS\Field\Text('companyAddress', __('Street and number', 'ls'), ['maxlength' => 300, 'fetchFromParent' => true]),
                'companyPostal'       => new LS\Field\Value('companyPostal', __('Postal', 'ls'), ['maxlength' => 10, 'minlength' => 5, 'fetchFromParent' => true]),
                'companyCity'         => new LS\Field\Text('companyCity', __('City', 'ls'), ['maxlength' => 60, 'fetchFromParent' => true]),
                'companyState'        => new LS\Field\Text('companyState', __('State', 'ls'), ['maxlength' => 200, 'fetchFromParent' => true]),
                'companyCountry'      => new LS\Field\Country('companyCountry', __('Country', 'ls'), ['fetchFromParent' => true]),
                'companyPhone'        => new LS\Field\Phone('companyPhone', __('Phone', 'ls'), ['fetchFromParent' => true]),
                'companyFax'          => new LS\Field\Phone('companyFax', __('Fax', 'ls'), ['fetchFromParent' => true]),
                'companyCell'         => new LS\Field\Phone('companyCell', __('Cell', 'ls'), ['fetchFromParent' => true]),
                'companyServiceEmail' => new LS\Field\Email('companyServiceEmail', __('Service email address', 'ls'), ['fetchFromParent' => true]),
                'companyLogo'         => new LS\Field\Image('companyLogo', __('Logo', 'ls'), ['return' => 'src', 'fetchFromParent' => true]),
                'companyPageId'       => new LS\Field\Postpicker('companyPageId', __('Company page', 'ls'), ['fetchFromParent' => true]),
                'numberOfEmployees'   => new LS\Field\Value('numberOfEmployees', __('Number of employees', 'ls'), ['fetchFromParent' => true]),
                'locations'           => new LS\Field\SmartList('locations', __('Locations', 'ls'), ['fetchFromParent' => true]),
                'companyCreateDate'   => new LS\Field\Datetime('companyCreateDate', __('Create date', 'ls'), ['maxDate' => 0, 'disabled' => true]),
                'companyActive'       => new LS\Field\Checkbox('companyActive', __('Active', 'ls'))
            ];

            $fields = apply_filters('companyStaticFields', $fields);
        }

        return $fields;
    }

    /**
     * Retrieve company fields which can be edited through the backend
     * @since 2.5.13
     * @return LS\FieldAbstract[]
     */
    public static function getEditableCompanyFields()
    {
        $fields = [];
        $fieldsFactory = new \LS\FieldsFactory();

        foreach (get_option('ls_companies_dynamic_fields', []) as $fieldName => $fieldData) {

            $field = $fieldsFactory->createField($fieldData->type);

            if (!($field instanceof \LS\FieldAbstract)) {
                _doing_it_wrong(__FUNCTION__, 'Invalid company field ' . $fieldData->type, '2.8.2');
                continue;
            }

            $field
                ->setName($fieldData->name)
                ->setLabel($fieldData->title)
                ->setArg('description', $fieldData->description)
                ->setArg('fetchFromParent', $fieldData->fetchFromParent)
                ->setArg('isDynamic', true);

            if ($field->getType() == 'textarea') {
                $field->setArg('richEditor', true);
            }

            $fields[$fieldName] = $field;
        }

        return apply_filters('ls_companies_dynamic_fields', $fields);
    }

    /**
     * Get dynamic fields declarations
     * @since 2.0.0
     * @return LS\FieldAbstract[]
     */
    public static function getDynamicCompanyFields()
    {
        $fields = self::getEditableCompanyFields();

        /** @var LS\FieldAbstract $field */
        foreach (apply_filters('ls_company_extra_fields', []) as $fieldName => $field) {

            $field->setArg('extraField', true);
            $field->setArg('fetchFromParent', $field->getArg('fetchFromParent', true));
            $field->setArg('isDynamic', true);

            $fields[$fieldName] = $field;
        }

        return $fields;
    }

    /**
     * Save dynamic field declaration
     *
     * @since 2.0.0
     *
     * @param object $field Dynamic field data
     * @param string $oldName Old name (to replace)
     */
    public static function saveDynamicField($field, $oldName = '')
    {
        if (is_object($field) && isset($field->name) && !empty($field->name)) {

            $fields = get_option('ls_companies_dynamic_fields', []);

            if ($oldName != '' && $oldName !== $field->name) {

                unset($fields[$oldName]);

                $wpdb = LS()->wpdb;
                $wpdb->update($wpdb->prefix . 'ls_companies_fields_values', ['fieldName' => $field->name], ['fieldName' => $oldName]);
            }

            $fields[$field->name] = $field;

            update_option('ls_companies_dynamic_fields', $fields);
        }
    }

    /**
     * Delete dynamic field declaration + all data
     * @since 2.6.1
     * @param string $fieldName Field name to delete
     */
    public static function deleteDynamicField($fieldName)
    {
        if (!empty($fieldName)) {

            // delete field declaration
            $fields = get_option('ls_companies_dynamic_fields', []);
            unset($fields[$fieldName]);
            update_option('ls_companies_dynamic_fields', $fields);

            // delete field data
            $wpdb = LS()->wpdb;
            $wpdb->delete($wpdb->prefix . 'ls_companies_fields_values', ['fieldName' => $fieldName]);
        }
    }

    /**
     * Delete dynamic field declaration + all data
     *
     * @since 2.0.0
     * @deprecated 02.2019 Use deleteDynamicField()
     *
     * @param string $fieldName Field name to delete
     */
    public static function removeDynamicField($fieldName)
    {
        _deprecated_function(__FUNCTION__, '2.6.1', 'deleteDynamicField');

        self::deleteDynamicField($fieldName);
    }

    /**
     * Update dynamic field(s) value(s) for specified company
     *
     * @since 2.0.0
     *
     * @param array $data Associative array of dynamic fields names and values ('field name' => 'value', ...)
     * @param int $companyId Company ID
     */
    public static function saveDynamicFieldValue($data, $companyId)
    {
        // @deprecated 03.2019 Legacy validations
        // @TODO Strict type hints after 09.2019
        $companyId = \LS\Library::toInt($companyId);

        if (empty($data) || !is_array($data) || $companyId < 1) {
            return;
        }

        $rm = $upd = [];
        foreach ($data as $fieldName => $value) {
            if (empty($value) && ($value !== 0 && $value !== '0')) {
                $rm[] = "'" . $fieldName . "'";
            } else {
                $upd[] = "('" . $companyId . "','" . esc_sql($fieldName) . "', '" . esc_sql(maybe_serialize($value)) . "')";
            }
        }

        $wpdb = LS()->wpdb;

        if (!empty($rm)) {
            $wpdb->query(sprintf("DELETE FROM {$wpdb->prefix}ls_companies_fields_values WHERE companyId = %d AND fieldName IN (%s)", $companyId, implode(',', $rm)));
        }

        if (!empty($upd)) {
            $wpdb->query(sprintf("INSERT INTO {$wpdb->prefix}ls_companies_fields_values (companyId, fieldName, `value`) VALUES %s
                                  ON DUPLICATE KEY UPDATE value = VALUES(value)", implode(',', $upd)));
        }
    }

    /**
     * Get dynamic field
     *
     * @since 1.10.0
     *
     * @param string $fieldName Field name
     * @return false|object Field data or false if not exist
     */
    public static function getDynamicField($fieldName)
    {
        return self::getDynamicCompanyFields()[$fieldName] ?? false;
    }

    /**
     * Get editable dynamic field
     *
     * @since 2.5.13
     *
     * @param string $fieldName Field name
     * @return false|LS\FieldAbstract Field data or false if not exist
     */
    public static function getEditableField($fieldName)
    {
        return self::getEditableCompanyFields()[$fieldName] ?? false;
    }

    /**
     * Get company's dynamic fields values
     *
     * @since 1.8
     *
     * @param int $companyId Company Id
     * @return array Company dynamic fields values
     */
    private static function getCompanyDynamicValues($companyId)
    {
        // @deprecated 03.2019 Legacy validations
        // @TODO Strict type hints after 09.2019
        $companyId = \LS\Library::toInt($companyId);

        if ($companyId < 1) {
            return [];
        }

        $wpdb = LS()->wpdb;
        $data = $wpdb->get_results(sprintf("SELECT fieldName, value FROM {$wpdb->prefix}ls_companies_fields_values WHERE companyId = %d", $companyId));

        $companyData = [];

        foreach ($data as $field) {
            $companyData[$field->fieldName] = LS\Library::maybeUnserialize($field->value);
        }

        return $companyData;
    }

    /**
     * Check if parent company is valid for the specified company
     *
     * @2.8.1
     *
     * @param \LS\CompanyInterface $company Company
     * @return bool True if valid
     */
    public static function validParentCompany(\LS\CompanyInterface $company)
    {
        return $company->getParentCompanyId() === 0 || $company->getParentCompanyId() !== $company->getCompanyId();
    }

    /**
     * Save the company
     *
     * @since 2.2.0
     *
     * @param \LS\CompanyInterface $company Company to save
     * @return bool True if company saved
     */
    public static function saveCompany(\LS\CompanyInterface $company)
    {
        $wpdb = LS()->wpdb;
        $success = true;

        if ($company->getCompanyId() > 0) {

            $oldCompany = self::getCompany($company->getCompanyId(), false);

            if ($oldCompany) {

                $wpdb->update($wpdb->prefix . 'ls_companies', $company->getRecord(), ['companyId' => $company->getCompanyId()], null, '%d');

                do_action('ls_company_updated', $company);
            }

            if ($oldCompany->isCompanyActive() !== $company->isCompanyActive()) {
                if ($company->isCompanyActive()) {
                    self::activateCompany($company);
                } else {
                    self::deactivateCompany($company);
                }
            }

        } elseif ($wpdb->insert($wpdb->prefix . 'ls_companies', $company->markAsCreated()->getRecord())) {

            $company->setCompanyId((int) $wpdb->insert_id);

            do_action('ls_company_added', $company);

        } else {
            $success = false;
        }

        if ($success && $company->getCompanyId() > 0) {

            self::saveDynamicFieldValue($company->getDynamicData(), $company->getCompanyId());

            do_action('ls_company_saved', $company);
        }

        return $success;
    }

    /**
     * Retrieve company locations list
     *
     * @since 1.9.0
     *
     * @param int $companyId Company Id
     * @return array An array of locations
     */
    public static function getCompanyLocations($companyId)
    {
        // @deprecated 03.2019 Legacy validations
        // @TODO Strict type hints after 09.2019
        $companyId = \LS\Library::toInt($companyId);

        $locations = [];

        if (($company = self::getCompany($companyId)) !== false) {
            $locations = $company->getCompanyLocations(true);
        }

        return $locations;
    }

    /**
     * Duplicate company
     *
     * @since 2.0.0
     *
     * @param int $oldCompanyId Company Id
     * @return int Duplicated company Id
     */
    public static function duplicateCompany($oldCompanyId)
    {
        // @deprecated 03.2019 Legacy validations
        // @TODO Strict type hints after 09.2019
        $oldCompanyId = \LS\Library::toInt($oldCompanyId);

        $newCompanyId = 0;
        $company = self::getCompany($oldCompanyId, false);

        if ($company) {

            $newCompany = clone $company;

            $newCompany
                ->setCompanyId(0)
                ->setCompanyName(__('Copy of ', 'ls') . $company->getCompanyName())
                ->markAsCreated();

            if (self::saveCompany($newCompany)) {
                do_action('ls_company_duplicated', $oldCompanyId, $newCompany->getCompanyId());
            }
        }

        return $newCompanyId;
    }

    /**
     * Retrieve companies Id's by dynamic field/value
     *
     * @since 2.2.0
     *
     * @param string $fieldName Field name
     * @param mixed $fieldValue Field value
     * @return int[] Companies Id's
     */
    public static function getCompaniesIdsByDynamicValue($fieldName, $fieldValue = '')
    {
        $wpdb = LS()->wpdb;
        $valueWhere = $fieldValue == '' ? '' : sprintf(" AND `value` = '%s'", esc_sql($fieldValue));
        $query = sprintf("SELECT companyId FROM {$wpdb->prefix}ls_companies_fields_values WHERE fieldName = '%s'%s", esc_sql($fieldName), $valueWhere);

        return array_unique(array_map('intval', $wpdb->get_col($query)));
    }

    /**
     * Retrieve companies Id's and values of the specified dynamic field
     *
     * @since 2.4.1
     *
     * @param string $fieldName Field name
     * @return array Array (companyId => field_value)
     */
    public static function getCompaniesDynamicValues($fieldName)
    {
        $wpdb = LS()->wpdb;
        $query = sprintf("SELECT companyId, value FROM {$wpdb->prefix}ls_companies_fields_values WHERE fieldName = '%s'", esc_sql($fieldName));

        return array_column($wpdb->get_results($query), 'value', 'companyId');
    }

    /**
     * Retrieve a list of possible companies dynamic fields
     * @since 2.7.0
     * @return array Array of field types and their labels
     */
    public static function getDynamicFieldTypes()
    {
        return [
            'text'     => __('Text (single line)', 'ls'),
            'textarea' => __('Text (multiple lines)', 'ls'),
            'date'     => __('Date', 'ls'),
            'value'    => __('Number', 'ls'),
            'url'      => __('URL', 'ls'),
            'checkbox' => __('Checkbox', 'ls')
        ];
    }

    /**
     * Append company Id to every company shortcode
     *
     * @since 2.8.0
     *
     * @param int $companyId Company Id
     * @param string $content Content with shortcodes
     * @return string Modified content
     */
    public static function prepareCompanyShortcodes(int $companyId, string $content): string
    {
        if ($companyId < 1 || empty($content)) {
            return $content;
        }

        $result = str_replace(['[companyName]', '[company-page]'], ['[companyName company_id=' . $companyId . ']', '[company-page company_id=' . $companyId . ']'], $content);
        $result = preg_replace('/\[company field=\"(\w+)\"\]/', '[company field="${1}" company_id=' . $companyId . ']', $result);

        return empty($result) ? $content : $result;
    }
}