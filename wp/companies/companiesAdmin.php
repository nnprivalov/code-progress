<?php
class companiesAdminLoyaltySuite extends companiesLoyaltySuite implements \LS\ModuleAdminInterface
{
    /**
     * Determine if the module has administration page
     * @since 2.7.0
     * @return bool
     */
    public function hasAdministrationPage()
    {
        return true;
    }

    /**
     * Main function to load models, create actions and filters, init variables
     * @since 2.7.0
     * @internal
     */
    public function load()
    {
        parent::load();

        add_action('admin_enqueue_scripts', [$this, 'adminScripts']);
        add_action('admin_init', [$this, 'adminInit']);
        add_action('wp_ajax_getCompanyLocations', [$this, 'getCompanyLocationsAjax']);
        add_action('wp_ajax_companiesSanitizeText', [$this, 'companiesSanitizeText']);

        // Consumers
        add_filter('consumerFieldsToAdminAdd', [$this, 'consumerFieldsToAdmin']);
        add_filter('consumerFieldsToAdminEdit', [$this, 'consumerFieldsToAdmin']);
        add_filter('ls_consumers_overview_filters', [$this, 'consumersOverviewFilters']);
        add_filter('ls_consumers_overview_column_companyLocation', [$this, 'consumerOverviewColumnCompanyLocation'], 10, 2);
        add_filter('ls_consumers_overview_columns', [$this, 'consumersOverviewColumns']);
    }

    /**
     * Create shortcodes
     * @since 2.7.0
     */
    public function initShortcodes()
    {
        parent::initShortcodes();

        LS()->addShortcodeInfo('companyName', __('Display company name', 'ls'), '', $this->getName(), ['company_id' => __('Company Id', 'ls')]);
        LS()->addShortcodeInfo('company-page', __('Display company page', 'ls'), '', $this->getName(), ['company_id' => __('Company Id', 'ls'), 'class' => __('Link class name', 'ls')], '[company-page class="normal-button large"]button[/company-page]');

        $fields = [];

        foreach ($this->getCompanyFields() as $cfName => $cf) {
            $fields[] = '&nbsp;&nbsp;&nbsp;<strong>' . $cfName . '</strong> - ' . ls_kses($cf->getLabel());
        }

        $fields = implode('<br />', $fields);

        LS()->addShortcodeInfo('company', __('Company field value', 'ls'), __('Display specific company field value.', 'ls'), $this->getName(), ['company_id' => __('Company Id', 'ls'), 'field' => __('Field name', 'ls') . ':<br />' . $fields]);
    }

    /**
     * After module has been installed
     * @since 2.7.0
     * @internal
     */
    public function onInstall()
    {
        require_once __DIR__ . '/install.php';

        companyInstallLoyaltySuite();
    }

    /**
     * On uninstall module
     * @since 2.7.0
     * @internal
     */
    public function onUninstall()
    {
        LSCompaniesHelper::deleteTables();
    }

    /**
     * After module activation
     * @since 2.7.0
     * @internal
     */
    public function afterActivate()
    {
        $this->addCapsToRole(['ls_companies'], 'contributor');
        $this->addCapsToRole(['ls_companies', 'ls_companies_manage'], 'author');
        $this->addCapsToRole(['ls_companies', 'ls_companies_manage'], 'editor');
        $this->addCapsToRole(['ls_companies', 'ls_companies_manage', 'ls_companies_settings'], 'administrator');
    }

    /**
     * Process admin actions and requests
     * @since 2.7.0
     * @internal
     */
    public function adminInit()
    {
        if (!$this->isCurrentPluginPage() || !$this->hasAction() || !current_user_can('ls_companies')) {
            return;
        }

        if ($this->isAction('new-company') && isset($_POST['submit']) && current_user_can('ls_companies_manage')) {

            $companyData = [];
            $formFields = $this->getCompanyFieldsToAdminAdd();

            foreach ($formFields as $fieldName => $field) {
                $companyData[$fieldName] = $this->getParam($fieldName, '', 'POST');
            }

            $result = $this->validateAndSaveCompany($formFields, $companyData);

            if ($result['success']) {
                $this->setAlert(__('New company has been added', 'ls'));
                LS()->logUserAction(sprintf('Companies: new company added (ID: %d)', $result['companyId']));
                \LS\Library::redirect($this->companyEditLink($result['companyId'], $this->getReferer()));
            }

            foreach ($result['errors'] as $error) {
                $this->setError($error);
            }
        }

        if ($this->isAction('save-company') && isset($_POST['submit'], $_POST['companyId']) && current_user_can('ls_companies_manage')) {

            $companyData = [];
            $companyId = $this->getParam('companyId', 'id', 'POST');
            $company = LSCompaniesHelper::getCompany($companyId, false);

            if (!$company) {
                $this->setError(__('Invalid company to save', 'ls'));
                \LS\Library::redirect(remove_query_arg('action'));
            }

            $formFields = $this->getCompanyFieldsToAdminEdit($companyId);

            foreach ($formFields as $fieldName => $field) {
                $companyData[$fieldName] = $this->getParam($fieldName, '', 'POST');
            }

            $result = $this->validateAndSaveCompany($formFields, $companyData, $company);

            if ($result['success']) {
                $this->setAlert(__('Company has been updated', 'ls'));
                LS()->logUserAction(sprintf('Companies: company updated (ID: %d)', $result['companyId']));
                \LS\Library::redirect(remove_query_arg('action'));
            }

            foreach ($result['errors'] as $error) {
                $this->setError($error);
            }
        }

        if ($this->isAction('delete-company') && isset($_REQUEST['company']) && current_user_can('ls_companies_manage')) {

            $companies = $this->getParam('company', 'int_array');
            foreach ($companies as $companyId) {
                $company = LSCompaniesHelper::getCompany($companyId, false);
                if ($company) {
                    LSCompaniesHelper::deleteCompany($company);
                }
            }

            $this->setAlert(__('Selected records have been deleted', 'ls'));
            LS()->logUserAction(sprintf('Companies: company(-ies) deleted (ID: %s)', implode(',', $companies)));

            \LS\Library::redirect(remove_query_arg(['action', 'action2', 'company']));
        }

        if ($this->isAction('wipe-company') && isset($_REQUEST['company']) && current_user_can('ls_companies_manage')) {

            $companies = $this->getParam('company', 'int_array');
            foreach ($companies as $companyId) {
                LSCompaniesHelper::wipeCompany($companyId);
            }

            $this->setAlert(__('Selected records have been wiped', 'ls'));
            LS()->logUserAction(sprintf('Companies: company(-ies) wiped (ID: %s)', implode(',', $companies)));
            \LS\Library::redirect(remove_query_arg(['action', 'action2', 'company']));
        }

        if ($this->isAction('activate-company') && isset($_REQUEST['company']) && current_user_can('ls_companies_manage')) {

            $companiesIds = $this->getParam('company', 'int_array');
            foreach ($companiesIds as $companyId) {
                $company = LSCompaniesHelper::getCompany($companyId, false);
                if ($company) {
                    LSCompaniesHelper::activateCompany($companyId);
                }
            }

            $this->setAlert(__('Selected companies have been activated', 'ls'));
            LS()->logUserAction(sprintf("Companies: activated (%s)", implode(',', $companiesIds)));
            \LS\Library::redirect(remove_query_arg(['action', 'action2', 'company']));
        }

        if ($this->isAction('deactivate-company') && isset($_REQUEST['company']) && current_user_can('ls_companies_manage')) {

            $companiesIds = $this->getParam('company', 'int_array');
            foreach ($companiesIds as $companyId) {
                $company = LSCompaniesHelper::getCompany($companyId, false);
                if ($company) {
                    LSCompaniesHelper::deactivateCompany($companyId);
                }
            }

            $this->setAlert(__('Selected companies have been deactivated', 'ls'));
            LS()->logUserAction(sprintf("Companies: deactivated (%s)", implode(',', $companiesIds)));
            \LS\Library::redirect(remove_query_arg(['action', 'action2', 'company']));
        }

        if ($this->isAction('duplicate-company') && isset($_REQUEST['company']) && current_user_can('ls_companies_manage')) {

            $companiesIds = $this->getParam('company', 'int_array');
            foreach ($companiesIds as $companyId) {
                LSCompaniesHelper::duplicateCompany($companyId);
            }

            $this->setAlert(__('Selected companies have been duplicated', 'ls'));
            LS()->logUserAction(sprintf("Companies: duplicated (%s)", implode(',', $companiesIds)));
            \LS\Library::redirect(remove_query_arg(['action', 'action2', 'company']));
        }

        if ($this->isAction('save-companies-field') && isset($_REQUEST['name']) && current_user_can('ls_companies_settings')) {

            $errors = [];
            $isCreation = $this->getParam('isCreation', 'int');
            $fieldName = $this->getParam('fieldName', 'text');

            $field = stripslashes_deep([
                'name'            => $this->getParam('name', 'text'),
                'title'           => $this->getParam('title', 'text'),
                'description'     => $this->getParam('description', 'text'),
                'type'            => $this->getParam('type', 'text'),
                'fetchFromParent' => $this->getParam('fetchFromParent', 'checkbox')
            ]);

            $field['name'] = $this->sanitizeUniqueFieldName($field['name']);

            if (empty($field['title'])) {
                $errors['fieldTitle'] = __('Please enter field title', 'ls');
            } elseif (empty($field['name'])) {
                $errors['fieldName'] = __('Please enter field name', 'ls');
            } elseif ($isCreation && !$this->isUniqueFieldName($field['name'])) {
                $errors['fieldName'] = __('There is another field with the same name. Please enter an unique field name.', 'ls');
            }

            if (!in_array($field['type'], ['text', 'textarea', 'date', 'datetime', 'value', 'url', 'checkbox'])) {
                $errors['fieldType'] = __('Incorrect field type', 'ls');
            }

            if (empty($errors)) {

                LSCompaniesHelper::saveDynamicField((object) $field, $fieldName);

                $this->setAlert(__('Field has been saved', 'ls'));
                LS()->logUserAction(sprintf('Companies: companies dynamic field "%s" saved.', $field['name']));
                \LS\Library::redirect('admin.php?page=companiesLoyaltySuite&view=companies-field&fieldName=' . esc_attr($field['name']) . '&referer=' . urlencode($this->getReferer()));
            }

            foreach ($errors as $error) {
                $this->setError($error);
            }
        }

        if ($this->isAction('delete-companies-field') && isset($_REQUEST['companiesFieldName']) && current_user_can('ls_companies_settings')) {

            $fields = $this->getParam('companiesFieldName', 'array');
            foreach ($fields as $fieldName) {
                LSCompaniesHelper::deleteDynamicField($fieldName);
            }

            $this->setAlert(__('Selected records have been deleted', 'ls'));
            LS()->logUserAction(sprintf('Companies: companies dynamic fields deleted: %s', implode($fields)));
            \LS\Library::redirect(remove_query_arg(['action', 'action2', 'companiesFieldName']));
        }
    }

    /**
     * Load module scripts
     * @since 2.7.0
     * @internal
     */
    public function adminScripts()
    {
        if (\LS\Library::isBackendRequest()) {

            wp_enqueue_style('ls-companies', $this->getUrl2Module() . 'css/admin-companies.css', [], $this->getVersion());

            if ($this->isCurrentPluginPage()) {
                wp_enqueue_script('ls-companies-admin', $this->getUrl2Module() . 'js/admin-companies.js', ['jquery'], $this->getVersion());
            }

            wp_enqueue_script('ls-companies-admin-ext', $this->getUrl2Module() . 'js/admin-companies-ext.js', ['jquery'], $this->getVersion());
        }
    }

    /**
     * Render administration menu
     * @since 2.7.0
     * @internal
     */
    public function moduleAdministrationPage()
    {
        include_once __DIR__ . '/tables/companies.php';
        include_once __DIR__ . '/tables/companies-fields.php';

        $template = new LS\Template(__DIR__ . '/views/backend');
        $template->assign('options', $this->getOptions());

        if (!isset($_REQUEST['view'])) {
            $template
                ->assign('tableCompanies', new companiesTableLoyaltySuite())
                ->assign('tableCompaniesFields', new companiesFieldsTableLoyaltySuite())
                ->assign('companiesFields', $this->getCompanyFieldsForShortcodes())
                ->assign('tabs', [
                    ['name' => 'ls-overview', 'title' => __('Overview', 'ls'), 'view' => 'admin-overview.phtml'],
                    ['name' => 'ls-companies-fields', 'title' => __('Dynamic fields', 'ls'), 'view' => 'admin-fields.phtml', 'capability' => 'ls_companies_settings']
                ]);
        }

        // company details page
        if ($this->isAction('details') || (isset($_REQUEST['view']) && $_REQUEST['view'] == 'company-details')) {

            $companyId = isset($_REQUEST['companyId']) ? (int) $_REQUEST['companyId'] : 0;

            $company = LSCompaniesHelper::getCompany($companyId, false);
            if ($company) {

                $company->setFromArray(stripslashes_deep($_POST));

                $template
                    ->assign('company', $company)
                    ->assign('companyFields', $this->getCompanyFieldsToAdminEdit($companyId))
                    ->assign('isCreation', false)
                    ->assign('page', ['name' => 'ls-company', 'title' => __('Company details', 'ls'), 'view' => 'admin-company.phtml']);

            } else {
                $template->assign('page', ['name' => 'ls-company', 'title' => __('Company details', 'ls'), 'content' => __('Can not find company', 'ls')]);
            }
        }

        // new company page
        if (isset($_REQUEST['view']) && $_REQUEST['view'] == 'new-company') {
            $template
                ->assign('companyFields', $this->getCompanyFieldsToAdminAdd())
                ->assign('company', new \LS\Company(stripslashes_deep($_POST)))
                ->assign('isCreation', true)
                ->assign('page', ['name' => 'ls-company', 'title' => __('Company details', 'ls'), 'view' => 'admin-company.phtml']);
        }

        // companies field details page
        if ($this->isAction('details') || (isset($_REQUEST['view']) && $_REQUEST['view'] == 'companies-field')) {

            $fieldName = $this->getParam('fieldName', 'text');
            $field = LSCompaniesHelper::getEditableField($fieldName);

            if (!$field) {
                $field = new LS\Field\Text();
                $isCreation = true;
            } else {
                $isCreation = false;
            }

            $fieldType = $field->getType();

            if (isset($_POST['save-companies-field'])) {

                $field
                    ->setName($this->getParam('name', 'text'))
                    ->setLabel($this->getParam('title', 'text'))
                    ->setArg('description', $this->getParam('description', 'html'))
                    ->setArg('fetchFromParent', $this->getParam('fetchFromParent', 'checkbox'));

                $fieldType = $this->getParam('type', 'text');
            }

            $template
                ->assign('field', $field)
                ->assign('fieldName', $fieldName)
                ->assign('isCreation', $isCreation)
                ->assign('fieldType', $fieldType)
                ->assign('page', ['name' => 'ls-companies-field', 'title' => $isCreation ? __('New companies field', 'ls') : __('Companies field details', 'ls'), 'view' => 'admin-companies-field.phtml']);
        }

        $template->renderAdminPage();
    }

    /**
     * Output a list of company locations
     *
     * @since 2.7.0
     * @ajax
     * @internal
     */
    public function getCompanyLocationsAjax()
    {
        if (current_user_can('ls_companies')) {
            $locations = LSCompaniesHelper::getCompanyLocations($this->getParam('companyId', 'id'));
            wp_send_json(empty($locations) ? '' : $locations);
        }

        exit;
    }

    /**
     * Sanitize incoming text
     *
     * @ajax
     * @since 2.7.0
     * @internal
     */
    public function companiesSanitizeText()
    {
        if (current_user_can('ls_companies')) {
            $text = $this->getParam('text', 'text');
            echo(empty($text) ? '' : $this->generateUniqueFieldName($text));
        }

        exit;
    }

    /**
     * Customize company location column
     *
     * @since 2.7.0
     *
     * @param string $id Company location Id
     * @param \LS\ConsumerInterface $consumer Consumer object
     * @return string Company location
     */
    public function consumerOverviewColumnCompanyLocation($id, \LS\ConsumerInterface $consumer)
    {
        $companyLocations = LSCompaniesHelper::getCompanyLocations($consumer->getCompanyId());
        if ($companyLocations && isset($companyLocations[$id])) {
            $id = $companyLocations[$id];
        }

        return $id;
    }

    /**
     * Customize company column on the consumers overview page
     *
     * @since 2.7.0
     * @internal
     *
     * @param array $tableData A list of table columns
     * @return array A list of table columns
     */
    public function consumersOverviewColumns($tableData)
    {
        $tableData['displays']['companyId'] = static function ($companyId) {

            if ($companyId > 0) {
                $companiesList = LSCompaniesHelper::getCompaniesList(false, false);
                foreach ($companiesList as $cid => $name) {
                    if ($cid == $companyId) {
                        return '<a href="' . companiesAdminLoyaltySuite::companyEditLink($companyId) . '">' . esc_html($name) . '</a>';
                    }
                }
            } else {
                $companyId = '—';
            }

            return $companyId;
        };

        return $tableData;
    }

    /**
     * Customize company location on admin add/edit form
     *
     * @since 2.7.0
     *
     * @param \LS\FieldAbstract[] $fields Form fields
     * @return \LS\FieldAbstract[] Form fields
     */
    public function consumerFieldsToAdmin($fields)
    {
        if (isset($fields['companyLocation'])) {

            /** @var \LS\Field\Select $f */
            $f = $fields['companyLocation'];

            // try to retrieve company Id and get their locations
            $f->setOptions(function () {

                $locations = [];
                $consumerId = $this->getParam('consumerId', 'id');
                $consumer = consumersLoyaltySuite::getConsumer($consumerId);
                $companyId = $this->getParam('companyId', 'id');

                if ($consumer && $consumer->getCompanyId() > 0) {
                    $companyId = $consumer->getCompanyId();
                }

                if ($companyId > 0) {
                    $locations = LSCompaniesHelper::getCompanyLocations($companyId);
                }

                return $locations;
            });
        }

        return $fields;
    }

    /**
     * Remove company location from an overview table filters
     *
     * @since 2.9.0
     * @internal
     *
     * @param array $fields Filters
     * @return array Overview Filters
     */
    public function consumersOverviewFilters(array $fields): array
    {
        foreach ($fields as $key => $field) {
            if ($field[0] == 'companyLocation') {
                unset($fields[$key]);
            }
        }

        return $fields;
    }

    /**
     * Save company
     *
     * @since 2.7.0
     *
     * @param array $fields Form fields
     * @param array $companyData Company data to be saved
     * @param null|\LS\CompanyInterface $company Company if updating company or null when creating a new company
     * @return array Array('companyId' - company  ID, 'errors' - an array of found errors)
     */
    private function validateAndSaveCompany($fields, $companyData, $company = null)
    {
        $errors = [];
        $dataToSave = [];

        // automatically save all data which is not in the $fields but provided as to be saved
        foreach ($companyData as $key => $val) {
            if (!isset($fields[$key])) {
                $dataToSave[$key] = $val;
            }
        }

        /** @var $field LS\FieldAbstract */
        foreach ($fields as $fieldName => $field) {

            if ($field->isDisabled() || $field->isRetype()) {
                continue;
            }

            $value = $companyData[$fieldName] ?? null;

            if (($error = $field->validate($value)) != '') {
                $errors[] = $error;
            } else {
                $dataToSave[$fieldName] = $field->formatToDatabase($value);
            }
        }

        if (empty($errors)) {

            if ($company === null) {
                $company = new \LS\Company();
            }

            $company->setFromArray($dataToSave);

            try {

                if (!LSCompaniesHelper::validParentCompany($company)) {
                    throw new Exception(__('The company can not be related to itself', 'ls'));
                }

                if (!LSCompaniesHelper::saveCompany($company)) {
                    throw new Exception(__('Can not save company', 'ls'));
                }

            } catch (Exception $e) {
                _doing_it_wrong(__FUNCTION__, $e->getMessage() . '[' . serialize($companyData) . ']', '2.9.0');
                $errors[] = $e->getMessage();
            }
        }

        return [
            'success'   => empty($errors) && $company->getCompanyId() > 0,
            'companyId' => $company ? $company->getCompanyId() : 0,
            'company'   => $company,
            'errors'    => $errors
        ];
    }

    /**
     * Retrieve company fields to admin add
     * @since 2.7.0
     * @return LS\FieldAbstract[] Company fields
     */
    private function getCompanyFieldsToAdminAdd()
    {
        $fields = [];

        foreach ($this->getCompanyFields() as $fieldName => $field) {

            if (in_array($fieldName, ['companyId', 'companyCreateDate'])) {
                continue;
            }

            $fields[$fieldName] = $field;
        }

        return apply_filters('companyFieldsToAdminAdd', $fields);
    }

    /**
     * Retrieve company fields to admin edit
     *
     * @since 2.7.0
     *
     * @param int $companyId Company Id
     * @return LS\FieldAbstract[] Company fields
     */
    private function getCompanyFieldsToAdminEdit(int $companyId)
    {
        $fields = [];

        foreach ($this->getCompanyFields() as $fieldName => $field) {

            // allow to edit all fields
            $field->setDisabled(false);

            $fields[$fieldName] = $field;
        }

        return apply_filters('companyFieldsToAdminEdit', $fields, $companyId);
    }

    /**
     * Retrieve a list of company fields accessible via shortcodes
     * @since 2.7.0
     * @return LS\FieldAbstract[] Company fields
     */
    private function getCompanyFieldsForShortcodes()
    {
        $fields = [];

        foreach ($this->getCompanyFields() as $name => $field) {

            if ($field->isRetype() || $field->getType() == 'password') {
                continue;
            }

            if (in_array($name, ['companyId', 'companyActive', 'companyCreateDate'])) {
                continue;
            }

            $fields[] = $field;
        }

        return apply_filters('companyFieldsForShortcodes', $fields);
    }

    /**
     * Generate unique field name by specified string
     *
     * @since 2.7.0
     *
     * @param string $text Field to generate unique field name from it
     * @return string Unique field name
     */
    private function generateUniqueFieldName($text)
    {
        if (empty($text)) {
            return '';
        }

        $text = $this->sanitizeUniqueFieldName($text);
        $name = $text;

        $i = 1;
        do {
            if (!$this->isUniqueFieldName($name)) {
                $name = $text . '_' . $i;
            } else {
                break;
            }
        } while ($i++ < 100);

        return $name;
    }

    /**
     * Sanitize field name
     *
     * @since 2.7.0
     *
     * @param string $text Text to sanitize
     * @return string Sanitized text
     */
    private function sanitizeUniqueFieldName($text)
    {
        return preg_replace('/[^A-Za-z0-9\-_]/', '', str_replace(' ', '_', trim($text)));
    }

    /**
     * Check if specified field name is unique
     *
     * @since 2.7.0
     *
     * @param string $name Field name
     * @return bool True if field name is unique
     */
    private function isUniqueFieldName($name)
    {
        foreach ($this->getCompanyFields() as $fieldName => $field) {
            if ($fieldName == $name) {
                return false;
            }
        }

        return true;
    }

    /**
     * Retrieve URL to edit company
     *
     * @since 2.7.0
     *
     * @param int $companyId Company Id
     * @param string $referer Referer
     * @return string URL
     */
    public static function companyEditLink($companyId, $referer = '')
    {
        $referer = empty($referer) ? $_SERVER['REQUEST_URI'] : $referer;
        $referer = urlencode($referer);

        return $companyId > 0 ? admin_url('admin.php?page=companiesLoyaltySuite&view=company-details&companyId=' . $companyId . '&referer=' . $referer) : '';
    }
}