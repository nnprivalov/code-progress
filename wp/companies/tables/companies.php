<?php
class companiesTableLoyaltySuite extends LS\WP_List_Table
{
    protected $_column_headers;

    public $referer;

    /** @var int[] */
    protected $companiesRelations = [];

    private $extraColumns = [
        'cols'          => [],
        'sortable_cols' => [],
        'filter_cols'   => [],
        'actions'       => [],
        'displays'      => []
    ];

    public function __construct()
    {
        parent::__construct([
            'singular' => 'wp_ls_company',
            'plural'   => 'wp_ls_companies',
        ]);

        $this->extraColumns = apply_filters('ls_companies_overview_columns', $this->extraColumns);
    }

    /**
     * @return array
     */
    public function get_columns()
    {
        return array_merge([
            'cb'                => '<input type="checkbox" />',
            'companyId'         => 'ID',
            'companyName'       => __('Company name', 'ls'),
            'companyCreateDate' => __('Registration date', 'ls'),
            'companyPage'       => __('Company page', 'ls')
        ], $this->extraColumns['cols'], [
            'companyActive' => __('Active', 'ls'),
            'details'       => __('Details', 'ls')
        ]);
    }

    /**
     * @return array
     */
    public function get_sortable_columns()
    {
        return array_merge([
            'companyId'         => ['companyId', false],
            'companyName'       => ['companyName', true],
            'companyCreateDate' => ['companyCreateDate', true],
            'companyActive'     => ['companyActive', true]
        ], $this->extraColumns['sortable_cols']);
    }

    public function filter_box()
    {
        $this->__filter_box(array_merge([
            ['companyName', 'string'],
            ['companyCreateDate', 'date'],
            ['companyActive', 'boolean']
        ], $this->extraColumns['filter_cols']), 3);
    }

    public function prepare_items()
    {
        $options = $this->get_table_options();
        $companies = LSCompaniesHelper::paginateCompanies(20, $options['page'], $options['orderBy'], $options['order'], $options['searchTerm'], $options['filterTerms']);

        $this->set_pagination_args([
            "total_items" => $companies['totalCount'],
            "total_pages" => $companies['totalPages'],
            "per_page"    => $companies['itemsPerPage']
        ]);

        $this->items = $companies['rows'];
        $this->_column_headers = [$this->get_columns(), [], $this->get_sortable_columns()];

        $this->companiesRelations = $companies['relations'];
    }

    /**
     * @return array
     */
    public function get_bulk_actions()
    {
        $defaultActions = [];

        if (current_user_can('ls_companies_manage')) {
            $defaultActions['activate-company'] = __('Activate', 'ls');
            $defaultActions['deactivate-company'] = __('Deactivate', 'ls');
            $defaultActions['duplicate-company'] = __('Duplicate', 'ls');
            $defaultActions['wipe-company'] = __('Wipe', 'ls');
            $defaultActions['delete-company'] = __('Delete', 'ls');
        }

        return array_merge($defaultActions, $this->extraColumns['actions']);
    }

    /**
     * @param \LS\CompanyInterface $company
     * @return string
     */
    public function column_cb($company)
    {
        return sprintf('<input type="checkbox" name="company[]" value="%s" />', $company->getCompanyId());
    }

    /**
     * @param \LS\CompanyInterface $company
     * @param string $columnName
     * @return string
     */
    public function column_default($company, $columnName)
    {
        $value = ' ';

        switch ($columnName) {
            case 'companyId' :
                $value = $company->getCompanyId();
                break;
            case 'companyName' :
                $text = str_repeat('&#8212; ', LSCompaniesHelper::getCompanyLevel($company->getCompanyId(), $this->companiesRelations)) . $company->getCompanyName();
                $value = '<strong><a href="' . companiesAdminLoyaltySuite::companyEditLink($company->getCompanyId(), $this->referer) . '">' . $text . '</a></strong>';
                break;
            case 'companyPage' :
                $companyPage = $company->getCompanyPageLink();
                if (LS()->isModuleActive('acm')) {
                    $link = ACMHelper::getSecureURL($company->getCompanyPageId());
                    $link = empty($link) ? $companyPage : $link;
                } else {
                    $link = $companyPage;
                }
                $value = empty($companyPage) ? ' ' : '<a href="' . esc_url($link) . '" target="_blank">' . $companyPage . '</a>';
                break;
            case 'companyCreateDate' :
                $value = $company->getCompanyCreateDate() < 0 ? ' ' : date('d.m.Y H:i', $company->getCompanyCreateDate() + TIMEZONE_DIFF);
                break;
            case 'companyActive' :
                $value = $company->isCompanyActive() ? '<span class="ls-sign-plus">+</span>' : '<span class="ls-sign-cross">-</span>';
                break;
            case 'details' :
                $value = '<a href="' . companiesAdminLoyaltySuite::companyEditLink($company->getCompanyId(), $this->referer) . '" class="button">' . __('Details', 'ls') . '</a>';
                break;
        }

        foreach ($this->extraColumns['displays'] as $name => $func) {
            if ($name == $columnName && is_callable($func)) {
                echo $func($company->getFieldValue($columnName), $company);
            }
        }

        return apply_filters('ls_companies_overview_column_' . $columnName, $value, $company);
    }
}