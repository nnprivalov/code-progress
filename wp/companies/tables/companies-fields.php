<?php
class companiesFieldsTableLoyaltySuite extends LS\WP_List_Table
{
    protected $_column_headers;
    public $referer;

    public function __construct()
    {
        parent::__construct([
            'singular' => 'wp_ls_company_fields',
            'plural'   => 'wp_ls_companies_fields',
        ]);
    }

    /**
     * @return array
     */
    public function get_columns()
    {
        return [
            'cb'              => '<input type="checkbox" />',
            'title'           => __('Title', 'ls'),
            'type'            => __('Type', 'ls'),
            'shortcode'       => __('Shortcode', 'ls'),
            'fetchFromParent' => __('Hierarchical', 'ls'),
            'details'         => __('Details', 'ls')
        ];
    }

    public function prepare_items()
    {
        $fields = LSCompaniesHelper::getEditableCompanyFields();

        $this->set_pagination_args([
            "total_items" => count($fields),
            "total_pages" => 1,
            "per_page"    => count($fields)
        ]);

        $this->items = $fields;
        $this->_column_headers = [$this->get_columns(), [], $this->get_sortable_columns()];
    }

    /**
     * @return array
     */
    public function get_bulk_actions()
    {
        return ['delete-companies-field' => __('Delete', 'ls')];
    }

    /**
     * @param LS\FieldAbstract $item
     * @return string
     */
    public function column_cb($item)
    {
        return sprintf('<input type="checkbox" name="companiesFieldName[]" value="%s" />', $item->getName());
    }

    /**
     * @param LS\FieldAbstract $item
     * @param string $columnName
     * @return string
     */
    public function column_default($item, $columnName)
    {
        $value = ' ';

        switch ($columnName) {
            case 'title' :
                $value = $item->getLabel();
                break;
            case 'type' :
                $value = $item->getType();
                break;
            case 'shortcode' :
                $value = '[company field="' . $item->getName() . '"]';
                break;
            case 'fetchFromParent' :
                $value = $item->getArg('fetchFromParent') ? '<span class="ls-sign-plus">+</span>' : ' ';
                break;
            case 'details' :
                $value = '<a href="admin.php?page=companiesLoyaltySuite&view=companies-field&fieldName=' . $item->getName() . '&referer=' . urlencode($this->referer) . '" class="button">' . __('Details', 'ls') . '</a>';
                break;
        }

        return $value;
    }
}