<?php
class courseLoyaltySuiteUpdate implements \LS\UpdateInterface
{
    /**
     * @param \LS\Module $module
     * @param string $oldVersion
     * @param string $newVersion
     */
    public function update(\LS\Module $module, string $oldVersion, string $newVersion)
    {
        $wpdb = LS()->wpdb;

        // 1.1.0
        if (version_compare($oldVersion, '1.1.0', '<')) {

            $wpdb->query(sprintf("ALTER TABLE `{$wpdb->prefix}_course_relations` ADD COLUMN `order` MEDIUMINT(5) UNSIGNED NOT NULL AFTER `nextPostId`"));

            $buildCourse = static function ($courseId) {

                $wpdb = LS()->wpdb;
                $relations = $wpdb->get_results(sprintf("SELECT postId, nextPostId FROM {$wpdb->prefix}_course_relations WHERE courseId = %d ORDER BY `order`", $courseId));

                $getPos = static function ($needle, $haystack) {
                    foreach ($haystack as $k => $item) {
                        if ($item->postId === $needle) {
                            return $k;
                        }
                    }

                    return false;
                };

                $list = [];
                foreach ($relations as $relation) {
                    if (($pos = $getPos($relation->nextPostId, $list)) !== false) {
                        $list = array_merge(array_slice($list, 0, $pos, true), [$relation], array_slice($list, $pos, count($list), true));
                    } else {
                        $list[] = $relation;
                    }
                }

                $result = [];
                foreach ($list as $k => $relation) {
                    $result[] = (int) $relation->postId;
                }

                return $result;
            };

            $result = $wpdb->get_results(sprintf("SELECT courseId, courseName FROM {$wpdb->prefix}_course ORDER BY courseName ASC"));

            foreach ($result as $item) {
                $order = 1;
                foreach ($buildCourse($item->courseId) as $postId) {
                    $wpdb->update($wpdb->prefix . 'ls_course_posts', ['order' => $order++], ['postId' => $postId, 'courseId' => $item->courseId]);
                }
            }

            $wpdb->query(sprintf("ALTER TABLE `{$wpdb->prefix}_course_relations`
                DROP COLUMN `relationId`,
                DROP COLUMN `nextPostId`,
                DROP INDEX `nextPostId`,
                DROP INDEX `postId`,
                DROP INDEX `courseId`,
                DROP PRIMARY KEY,
                ADD PRIMARY KEY (`courseId`, `postId`)"));
        }

        // 1.2.0
        if (version_compare($oldVersion, '1.2.0', '<')) {

            $wpdb->query(sprintf("ALTER TABLE `{$wpdb->prefix}_course`
                ADD COLUMN `startDate` DATE NULL DEFAULT '0000-00-00' AFTER `courseName`,
                ADD COLUMN `endDate` DATE NULL DEFAULT '0000-00-00' AFTER `startDate`,
                ADD COLUMN `active` TINYINT(1) UNSIGNED NULL DEFAULT '1' AFTER `endDate`"));

            $query = sprintf("SELECT a1.* FROM {$wpdb->prefix}_course_activities as a1
                              JOIN (
                                  SELECT activityId, MIN(actionDate) as minDate
                                  FROM {$wpdb->prefix}_course_activities GROUP BY consumerId, courseId
                                ) as a2
                                ON a1.actionDate = a2.minDate AND a1.activityId = a2.activityId");
            foreach ($wpdb->get_results($query) as $row) {
                $wpdb->insert($wpdb->prefix . 'ls_consumers_log', ['consumerId' => $row->consumerId, 'messageType' => 'course-started', 'note' => $row->courseId, 'createDate' => strtotime($row->actionDate)]);
            }

            $wpdb->query(sprintf("ALTER TABLE `{$wpdb->prefix}_course` ADD COLUMN `failureMessage` LONGTEXT NULL DEFAULT NULL AFTER `logBody`,
                                                            ADD COLUMN `defaultInterval` TINYINT(3) UNSIGNED NULL DEFAULT '3' AFTER `failureMessage`,
                                                            ADD COLUMN `minInterval` TINYINT(3) UNSIGNED NULL DEFAULT '1' AFTER `defaultInterval`,
                                                            ADD COLUMN `maxInterval` TINYINT(3) UNSIGNED NULL DEFAULT '7' AFTER `minInterval`"));
        }

        // 1.2.1
        if (version_compare($oldVersion, '1.2.1', '<')) {

            $wpdb->query(sprintf("CREATE TABLE `{$wpdb->prefix}_course_consumers` (
                    `courseId` SMALLINT(5) UNSIGNED NOT NULL,
                    `consumerId` INT(10) UNSIGNED NOT NULL,
                    `startDate` INT(9) UNSIGNED NULL DEFAULT NULL,
                    `lastActivityDate` INT(9) UNSIGNED NULL DEFAULT NULL,
                    `endDate` INT(9) UNSIGNED NULL DEFAULT NULL,
                    PRIMARY KEY (`courseId`, `consumerId`),
                    INDEX `courseId` (`courseId`),
                    INDEX `consumerId` (`consumerId`)
                ) %s ENGINE=InnoDB", LS\Library::getCharsetCollate()));

            require_once __DIR__ . '/LSCourseConsumer.php';
            require_once __DIR__ . '/LSCourseLesson.php';
            require_once __DIR__ . '/LSCourseHelper.php';

            $query = sprintf("SELECT a1.consumerId, a1.courseId, a1.actionDate FROM {$wpdb->prefix}_course_activities as a1
                                INNER JOIN (
                                    SELECT pi.activityId, MIN(pi.activityId) AS maxActivityId
                                   FROM {$wpdb->prefix}_course_activities pi GROUP BY pi.consumerId, pi.courseId
                                ) as a2 ON a2.maxActivityId = a1.activityId
                                GROUP BY a1.consumerId, a1.courseId");
            foreach ($wpdb->get_results($query) as $row) {
                $wpdb->insert($wpdb->prefix . 'ls_course_consumers', ['courseId' => $row->courseId, 'consumerId' => $row->consumerId, 'startDate' => strtotime($row->actionDate)]);
            }

            $query = sprintf("SELECT a1.consumerId, a1.courseId, a1.postId, a1.actionDate FROM {$wpdb->prefix}_course_activities as a1
                                INNER JOIN (
                                    SELECT pi.activityId, MAX(pi.activityId) AS maxActivityId
                                   FROM {$wpdb->prefix}_course_activities pi GROUP BY pi.consumerId, pi.courseId
                                ) as a2 ON a2.maxActivityId = a1.activityId
                                GROUP BY a1.consumerId, a1.courseId");
            foreach ($wpdb->get_results($query) as $row) {
                $lastPostId = LSCourseHelper::getCourseLastLessonPostId($row->courseId);
                if ($row->postId == $lastPostId) {
                    $wpdb->update($wpdb->prefix . 'ls_course_consumers', ['lastActivityDate' => strtotime($row->actionDate), 'endDate' => strtotime($row->actionDate)], ['courseId' => $row->courseId, 'consumerId' => $row->consumerId]);
                } else {
                    $wpdb->update($wpdb->prefix . 'ls_course_consumers', ['lastActivityDate' => strtotime($row->actionDate)], ['courseId' => $row->courseId, 'consumerId' => $row->consumerId]);
                }
            }
        }

        // 1.3.0
        if (version_compare($oldVersion, '1.3.0', '<')) {

            $wpdb->query(sprintf("ALTER TABLE `{$wpdb->prefix}_course`
                CHANGE COLUMN `logBody` `logBody` LONGTEXT NULL AFTER `logSubject`,
                CHANGE COLUMN `failureMessage` `failureMessage` TEXT NULL AFTER `logBody`"));

            $wpdb->query(sprintf("ALTER TABLE `{$wpdb->prefix}_course_consumers`
            CHANGE COLUMN `startDate` `startDate` INT(10) UNSIGNED NULL DEFAULT NULL AFTER `consumerId`,
            CHANGE COLUMN `lastActivityDate` `lastActivityDate` INT(10) UNSIGNED NULL DEFAULT NULL AFTER `startDate`,
            CHANGE COLUMN `endDate` `endDate` INT(10) UNSIGNED NULL DEFAULT NULL AFTER `lastActivityDate`"));
        }

        // 1.3.7
        if (version_compare($oldVersion, '1.3.7', '<')) {
            $tb = $wpdb->get_var(sprintf("SHOW TABLES LIKE '{$wpdb->prefix}_course_consumers'"));
            if (empty($tb)) {
                $wpdb->query(sprintf("CREATE TABLE `{$wpdb->prefix}_course_consumers` (
                `courseId` SMALLINT(5) UNSIGNED NOT NULL,
                `consumerId` INT(10) UNSIGNED NOT NULL,
                `startDate` INT(10) UNSIGNED NULL DEFAULT NULL,
                `lastActivityDate` INT(10) UNSIGNED NULL DEFAULT NULL,
                `endDate` INT(10) UNSIGNED NULL DEFAULT NULL,
                PRIMARY KEY (`courseId`, `consumerId`),
                INDEX `courseId` (`courseId`),
                INDEX `consumerId` (`consumerId`)
            ) %s ENGINE=InnoDB", LS\Library::getCharsetCollate()));
            }
        }

        // 2.0.0
        if (version_compare($oldVersion, '2.0.0', '<')) {

            $wpdb->query(sprintf("ALTER TABLE `{$wpdb->prefix}ls_course`
                ADD COLUMN `courseExcerpt` VARCHAR(1000) NOT NULL AFTER `courseName`,
                ADD COLUMN `coursePageId` INT(9) UNSIGNED NOT NULL AFTER `courseExcerpt`,        
                ADD COLUMN `courseObjective` VARCHAR(1000) NOT NULL AFTER `coursePageId`,
                ADD COLUMN `trainerName` VARCHAR(100) NOT NULL AFTER `courseObjective`,
                ADD COLUMN `trainerOccupation` VARCHAR(100) NOT NULL AFTER `trainerName`,
                ADD COLUMN `trainerImage` VARCHAR(1000) NOT NULL AFTER `trainerOccupation`"));

            $wpdb->query(sprintf("RENAME TABLE `{$wpdb->prefix}ls_course_relations` TO `{$wpdb->prefix}ls_course_posts`"));
        }

        // 2.1.0
        if (version_compare($oldVersion, '2.1.0', '<')) {

            $wpdb->query(sprintf("ALTER TABLE `{$wpdb->prefix}ls_course`
                CHANGE COLUMN `coursePageId` `coursePageId` INT(9) UNSIGNED NOT NULL AFTER `courseId`,
                DROP COLUMN `courseName`,
                DROP COLUMN `courseExcerpt`,
                DROP COLUMN `createDate`,
                DROP COLUMN `active`,
                ADD INDEX `coursePageId` (`coursePageId`)"));

            get_role('author')->add_cap('ls_course_manage');
            get_role('editor')->add_cap('ls_course_manage');
            get_role('administrator')->add_cap('ls_course_manage');
        }

        // 2.2.0
        if (version_compare($oldVersion, '2.2.0', '<')) {

            $wpdb->query(sprintf("ALTER TABLE `{$wpdb->prefix}ls_course_consumers`
                ADD COLUMN `lessonsRead` TINYINT(2) UNSIGNED NULL DEFAULT '0' AFTER `endDate`"));

            $activities = $wpdb->get_results(sprintf("SELECT consumerId, courseId, COUNT(activityId) as lr FROM {$wpdb->prefix}ls_course_activities GROUP BY consumerId, courseId"));
            foreach ($activities as $activity) {
                $wpdb->update($wpdb->prefix . 'ls_course_consumers', ['lessonsRead' => $activity->lr], ['consumerId' => $activity->consumerId, 'courseId' => $activity->courseId]);
            }
        }

        // 2.3.0
        if (version_compare($oldVersion, '2.3.0', '<')) {
            $wpdb->delete($wpdb->prefix . 'ls_consumers_fields_values', ['fieldName' => 'courseId']);
        }

        // 2.4.0
        if (version_compare($oldVersion, '2.4.0', '<')) {
            $module->setOptions($module->getDefaultOptions());
        }

        // 2.4.3
        if (version_compare($oldVersion, '2.4.3', '<')) {
            $module->setOption('acp', true);
        }

        // 2.4.4
        if (version_compare($oldVersion, '2.4.4', '<')) {
            get_role('administrator')->add_cap('ls_course_settings');
        }

        // 2.4.5
        if (version_compare($oldVersion, '2.4.5', '<')) {
            $activities = $wpdb->get_results(sprintf("SELECT consumerId, postId, actionDate FROM {$wpdb->prefix}ls_course_activities"));
            foreach ($activities as $activity) {
                \LS\ConsumersHelper::insertLog('course-lesson-read', $activity->consumerId, $activity->postId, strtotime($activity->actionDate));
            }
        }

        // 2.5.0
        if (version_compare($oldVersion, '2.5.0', '<')) {
            $wpdb->query(sprintf("ALTER TABLE `{$wpdb->prefix}ls_course` ADD COLUMN `courseTargetGroup` VARCHAR(1000) NOT NULL AFTER `courseObjective`"));
        }

        // 2.5.10
        if (version_compare($oldVersion, '2.5.10', '<')) {
            $wpdb->query(sprintf("DELETE FROM {$wpdb->postmeta} WHERE meta_key = '_trainer_name'"));
        }

        // 2.6.3
        if (version_compare($oldVersion, '2.6.3', '<')) {
            $wpdb->query(sprintf("ALTER TABLE `{$wpdb->prefix}ls_course_consumers` DROP COLUMN `lastActivityDate`"));
        }

        // 2.6.10
        if (version_compare($oldVersion, '2.6.10', '<')) {
            $wpdb->query("ALTER TABLE {$wpdb->prefix}ls_course_activities ADD COLUMN `actionTimestamp` INT(10) UNSIGNED NOT null AFTER `actionDate`");
            $wpdb->query("UPDATE {$wpdb->prefix}ls_course_activities SET actionTimestamp = UNIX_TIMESTAMP(actionDate)");
        }

        // 2.8.6
        if (version_compare($oldVersion, '2.8.6', '<')) {
            $wpdb->query("ALTER TABLE `{$wpdb->prefix}ls_course_consumers` DROP COLUMN `lessonsRead`");
        }
    }
}