<?php
class LSCourseHelper
{
    /** @var LSCourseLesson[] */
    protected static $coursePosts;

    /** @var LSCourseConsumer[] */
    protected static $consumersConnections = [];

    /**
     * ################  COURSE  ##########################
     */

    /**
     * Create new course
     *
     * @since 2.0.0
     *
     * @param LSCourse $course Course object
     * @return int Created course Id
     */
    public static function createCourse(LSCourse $course)
    {
        if(!$course->valid()) {
            return 0;
        }

        do_action('ls_course_before_create', $course);

        $wpdb = LS()->wpdb;

        if(!$wpdb->insert($wpdb->prefix . 'ls_course', $course->getRecord())) {
            return 0;
        }

        $course->setCourseId($wpdb->insert_id);

        do_action('ls_course_created', $course);

        return $course->getCourseId();
    }

    /**
     * Update course
     *
     * @since 2.0.0
     *
     * @param LSCourse $course Course object
     * @return bool True if updated or false on failure
     */
    public static function updateCourse(LSCourse $course)
    {
        if(!$course->valid() || $course->getCourseId() < 1) {
            return false;
        }

        do_action('ls_course_before_update', $course);

        $wpdb = LS()->wpdb;
        $wpdb->update($wpdb->prefix . 'ls_course', $course->getRecord(), ['courseId' => $course->getCourseId()], null, '%d');

        do_action('ls_course_updated', $course);

        return true;
    }

    /**
     * Retrieve course by id
     *
     * @since 2.0.0
     *
     * @param int $courseId Course Id to get
     * @return false|LSCourse Course object or false on failure
     */
    public static function getCourse($courseId)
    {
        if ($courseId < 1) {
            return false;
        }

        $wpdb = LS()->wpdb;
        $courseRecord = $wpdb->get_row(sprintf("SELECT * FROM {$wpdb->prefix}ls_course WHERE courseId = %d LIMIT 1", $courseId));

        if(empty($courseRecord)) {
            return false;
        }

        return new LSCourse($courseRecord);
    }

    /**
     * Retrieve courses
     *
     * @since 1.0.0
     *
     * @param string $orderBy Field to order
     * @param string $order Order direction
     * @param array $filters Filters
     * @return LSCourse[]
     */
    public static function getCourses($orderBy = '', $order = '', $filters = [])
    {
        $wpdb = LS()->wpdb;
        $order = empty($order) ? 'ASC' : $order;

        if(in_array($orderBy, ['startDate', 'endDate'])) {
            $orderBy = 'c.' . $orderBy;
        } elseif($orderBy == 'courseName') {
            $orderBy = 'p.post_title';
        } else {
            $orderBy = 'c.courseId';
        }

        $whereConditions = [];

        if(isset($filters['postId']) && $filters['postId'] > 0) {
            $whereConditions[] = sprintf("c.coursePageId = %d", $filters['postId']);
        }

        if(!empty($filters['started'])) {
            _deprecated_argument(__FUNCTION__, '2.7.0', 'Filter "started" deprecated');
            $filters['startedToday'] = true;
        }

        if(!empty($filters['ended'])) {
            _deprecated_argument(__FUNCTION__, '2.7.0', 'Filter "ended" deprecated');
            $filters['endedToday'] = true;
        }

        if(!empty($filters['startedToday'])) {
            $whereConditions[] = sprintf("c.startDate = '%s'", date('Y-m-d'));
        }

        if(!empty($filters['endedToday'])) {
            $whereConditions[] = sprintf("c.endDate = '%s'", date('Y-m-d', strtotime('YESTERDAY')));
        }

        $where = empty($whereConditions) ? '' : ' WHERE ' . implode(' AND ', $whereConditions);

        $query = sprintf("SELECT c.*
                          FROM {$wpdb->prefix}ls_course c
                          LEFT JOIN {$wpdb->posts} p ON p.ID = c.coursePageId
                          %s
                          GROUP BY c.courseId
                          ORDER BY %s %s", $where, $orderBy, $order);
        $result = $wpdb->get_results($query);
        $result = is_array($result) ? $result : [];

        $courses = [];

        foreach($result as $item) {
            $courses[] = new LSCourse($item);
        }

        return $courses;
    }

    /**
     * Retrieve started (today) courses
     * @since 2.7.0
     * @return LSCourse[]
     */
    public static function getCoursesStartedToday()
    {
        return self::getCourses('', '', [
            'startedToday' => true
        ]);
    }

    /**
     * Retrieve ended (today) courses
     * @since 2.7.0
     * @return LSCourse[]
     */
    public static function getCoursesEndedToday()
    {
        return self::getCourses('', '', [
            'endedToday' => true
        ]);
    }

    /**
     * Retrieve a part of sql-query to check courses for actuality
     * @since 1.2.0
     * @return string A part of MySQL query
     */
    private static function getCourseQueryForActualCheck()
    {
        return sprintf("p.post_status = 'publish' AND (c.startDate = '0000-00-00' OR '%s' >= c.startDate) AND (c.endDate = '0000-00-00' OR c.endDate >= '%s')", date('Y-m-d'), date('Y-m-d'));
    }

    /**
     * Retrieve a list of courses and their ID's
     *
     * @since 1.0.0
     *
     * @param bool $actualOnly True to retrieve only actual courses
     * @return array[int]string Key=>Value array
     */
    public static function getCoursesList($actualOnly = false)
    {
        $wpdb = LS()->wpdb;
        $actualWhere = $actualOnly ? sprintf("WHERE %s", self::getCourseQueryForActualCheck()) : '';
        $query = sprintf("SELECT c.courseId, p.post_title
                          FROM {$wpdb->prefix}ls_course c
                          INNER JOIN {$wpdb->posts} p ON p.ID = c.coursePageId
                          %s
                          ORDER BY p.post_title ASC", $actualWhere);
        $result = $wpdb->get_results($query);
        $result = is_array($result) ? $result : [];

        $list = [];

        foreach($result as $item) {
            $list[$item->courseId] = $item->post_title;
        }

        return $list;
    }

    /**
     * Retrieve course by course page Id
     *
     * @since 2.1.0
     *
     * @param int $coursePageId Course page Id
     * @return false|LSCourse LSCourse object or false on failure
     */
    public static function getCourseByCoursePageId($coursePageId)
    {
        $result = false;

        if($coursePageId > 0) {

            $wpdb = LS()->wpdb;
            $course = $wpdb->get_row(sprintf("SELECT * FROM {$wpdb->prefix}ls_course WHERE coursePageId = %d", $coursePageId));

            if(!empty($course)) {
                $result = new LSCourse($course);
            }
        }

        return $result;
    }

    /**
     * Retrieve course Id by course page Id
     *
     * @since 2.6.7
     *
     * @param int $coursePageId Course page Id
     * @return int Course Id
     */
    public static function getCourseIdByCoursePageId($coursePageId)
    {
        $course = self::getCourseByCoursePageId($coursePageId);

        return $course ? $course->getCourseId() : 0;
    }

    /**
     * Delete course
     *
     * @since 1.0.0
     *
     * @param int $courseId Course Id
     * @return bool True if deleted
     */
    public static function deleteCourse($courseId)
    {
        if($courseId < 1) {
            return false;
        }

        do_action('ls_course_before_delete', $courseId);

        $wpdb = LS()->wpdb;

        foreach(self::getConsumersConnectionsByCourseId($courseId) as $cc) {
            self::deleteConsumerToCourseConnection($cc);
        }

        if(!self::deleteAllLessonsByCourseId($courseId)) {
            return false;
        }

        if(!$wpdb->delete($wpdb->prefix . 'ls_course', ['courseId' => $courseId], '%d')) {
            return false;
        }

        $wpdb->delete($wpdb->prefix . 'ls_consumers_fields_values', ['fieldName' => 'courseId', 'value' => $courseId]);

        do_action('ls_course_deleted', $courseId);

        return true;
    }

    /**
     * ################  POSTS CONNECTIONS  ##########################
     */

    /**
     * Create course lesson
     *
     * @since 2.7.0
     *
     * @param LSCourseLesson $lesson Course post object
     * @return bool True if connection created
     */
    public static function createLesson(LSCourseLesson $lesson)
    {
        if(!$lesson->valid()) {
            return false;
        }

        $wpdb = LS()->wpdb;

        if(!$wpdb->insert($wpdb->prefix . 'ls_course_posts', $lesson->getRecord(true))) {
            return false;
        }

        self::$coursePosts = null;

        do_action('ls_course_connected_to_post', $lesson);

        return true;
    }

    /**
     * Update course lessons
     *
     * @since 2.7.0
     *
     * @param int $courseId Course Id
     * @param array $connections An array of post connections [<order> => <post ID>]
     * @return bool True if all connections were added to the course
     */
    public static function updateLessons($courseId, $connections)
    {
        $success = true;

        if(self::deleteAllLessonsByCourseId($courseId)) {

            foreach($connections as $order => $postId) {

                $lesson = new LSCourseLesson([
                    'courseId' => $courseId,
                    'postId'   => $postId,
                    'order'    => $order + 1
                ]);

                if($lesson->valid()) {
                    $success &= self::createLesson($lesson);
                }
            }

            self::$coursePosts = null;

            self::cleanUpActivities();
        }

        return (bool) $success;
    }

    /**
     * Update course lesson
     *
     * @since 2.7.0
     *
     * @param LSCourseLesson $lesson Course connection object
     * @return bool True if updated
     */
    public static function updateLesson(LSCourseLesson $lesson)
    {
        if(!$lesson->valid()) {
            return false;
        }

        $wpdb = LS()->wpdb;
        $wpdb->update($wpdb->prefix . 'ls_course_posts', $lesson->getRecord(), ['courseId' => $lesson->getCourseId(), 'postId' => $lesson->getPostId()], null, '%d');

        self::$coursePosts = null;

        return true;
    }

    /**
     * Retrieve all course lessons
     * @since 2.7.0
     * @return LSCourseLesson[]
     */
    public static function getAllLessons()
    {
        if(self::$coursePosts === null) {

            $wpdb = LS()->wpdb;
            $lessons = $wpdb->get_results(sprintf("SELECT * FROM {$wpdb->prefix}ls_course_posts ORDER BY `order` ASC"));
            $lessons = is_array($lessons) ? $lessons : [];

            $result = [];

            foreach($lessons as $lesson) {
                $result[] = new LSCourseLesson($lesson);
            }

            self::$coursePosts = $result;
        }

        return self::$coursePosts;
    }

    /**
     * Retrieve all post Id's with the course lessons
     * @since 2.7.0
     * @return int[] Post Id's
     */
    public static function getAllLessonPostIds()
    {
        return array_unique(array_map(static function(LSCourseLesson $lesson) {
            return $lesson->getPostId();
        }, self::getAllLessons()));
    }

    /**
     * Retrieve course lesson by lesson post Id
     *
     * @since 2.7.0
     *
     * @param int $postId Post ID
     * @return false|LSCourseLesson LSCourseLesson object or false on failure
     */
    public static function getLessonByLessonPostId($postId)
    {
        $result = false;

        if($postId > 0) {
            foreach(self::getAllLessons() as $lesson) {
                if($lesson->getPostId() == $postId) {
                    $result = $lesson;
                    break;
                }
            }
        }

        return $result;
    }

    /**
     * Retrieve course lessons
     *
     * @since 2.7.0
     *
     * @param int $courseId Course Id
     * @return LSCourseLesson[]
     */
    public static function getLessons($courseId)
    {
        if(empty($courseId)) {
            return [];
        }

        return array_values(array_filter(self::getAllLessons(), static function(LSCourseLesson $lesson) use ($courseId) {
            return $lesson->getCourseId() === $courseId;
        }));
    }

    /**
     * Retrieve all post Id's in the course
     *
     * @since 2.7.0
     *
     * @param int $courseId Course Id
     * @return int[] Post Id's
     */
    public static function getLessonIds($courseId)
    {
        return array_map(static function(LSCourseLesson $lesson) {
            return $lesson->getPostId();
        }, self::getLessons($courseId));
    }

    /**
     * Retrieve last course lesson
     *
     * @since 2.7.0
     *
     * @param int $courseId Course Id
     * @return false|LSCourseLesson LSCourseLesson on success or false on failure
     */
    public static function getLastLessonByCourseId($courseId)
    {
        $lessons = self::getLessons($courseId);

        return empty($lessons) ? false : end($lessons);
    }

    /**
     * Retrieve course Id by lesson post Id
     *
     * @since 2.7.0
     *
     * @param int $postId Post Id (lesson)
     * @return int Course Id
     */
    public static function getCourseIdByLessonPostId($postId)
    {
        $lesson = self::getLessonByLessonPostId($postId);

        return $lesson ? $lesson->getCourseId() : 0;
    }

    /**
     * Retrieve course by lesson post Id
     *
     * @since 2.1.0
     *
     * @param int $postId Post Id (lesson)
     * @return false|LSCourse Course object or false on failure
     */
    public static function getCourseByLessonPostId($postId)
    {
        return self::getCourse(
            self::getCourseIdByLessonPostId($postId)
        );
    }

    /**
     * Retrieve last course lesson post Id
     *
     * @since 2.7.0
     *
     * @param int $courseId Course Id
     * @return int Post Id
     */
    public static function getCourseLastLessonPostId($courseId)
    {
        $lesson = self::getLastLessonByCourseId($courseId);

        return $lesson ? $lesson->getPostId() : 0;
    }

    /**
     * Check if specified post is a course lesson post
     *
     * @since 2.7.0
     *
     * @param int $postId Post Id
     * @return bool True if specified post is course lesson post
     */
    public static function isCourseLesson($postId)
    {
        if($postId > 0) {
            foreach(self::getAllLessons() as $lesson) {
                if($lesson->getPostId() === $postId) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Delete course lesson
     *
     * @since 2.7.0
     *
     * @param LSCourseLesson $lesson Course lesson
     * @return bool True if course lesson has been deleted
     */
    public static function deleteLesson(LSCourseLesson $lesson)
    {
        if(!$lesson->valid()) {
            return false;
        }

        $wpdb = LS()->wpdb;
        $result = (bool) $wpdb->delete($wpdb->prefix . 'ls_course_posts', ['courseId' => $lesson->getCourseId(), 'postId' => $lesson->getPostId()], '%d');

        if($result) {

            self::$coursePosts = null;

            foreach(self::getCourseActivitiesByPostId($lesson->getCourseId(), $lesson->getPostId()) as $activity) {
                self::deleteActivity($activity);
            }

            do_action('ls_course_disconnected_from_post', $lesson);
        }

        return $result;
    }

    /**
     * Delete course lesson by specified post
     *
     * @since 2.7.0
     *
     * @param int $postId Post ID
     * @return bool True if course lesson has been deleted
     */
    public static function deleteLessonByLessonPostId($postId)
    {
        if($postId < 1) {
            return false;
        }

        $lesson = self::getLessonByLessonPostId($postId);
        if(!$lesson) {
            return false;
        }

        return self::deleteLesson($lesson);
    }

    /**
     * Delete all course lessons by course id
     *
     * @since 2.7.0
     *
     * @param int $courseId Course ID
     * @return bool True if all lessons have been deleted
     */
    public static function deleteAllLessonsByCourseId($courseId)
    {
        $success = true;

        foreach(self::getLessons($courseId) as $lesson) {
            $success &= self::deleteLesson($lesson);
        }

        return (bool) $success;
    }

    /**
     * ################  CONSUMER CONNECTIONS ##########################
     */

    /**
     * Create connection between consumer and course
     * Note: please check if consumer don't have specified course to prevent duplicates
     *
     * @since 2.0.0
     *
     * @param LSCourseConsumer $cc Course consumer object
     * @return bool True if connection has been created
     */
    public static function createConsumerConnection(LSCourseConsumer $cc)
    {
        if(!$cc->valid()) {
            return false;
        }

        if($cc->getStartDate() == 0) {
            $cc->setStartDate(time());
        }

        do_action('ls_course_connection_before_create', $cc);

        $wpdb = LS()->wpdb;

        if(!$wpdb->insert($wpdb->prefix . 'ls_course_consumers', $cc->getRecord(true))) {
            return false;
        }

        self::$consumersConnections = [];

        do_action('ls_course_connection_created', $cc);

        return true;
    }

    /**
     * Update consumer connection to the course
     *
     * @since 2.0.0
     *
     * @param LSCourseConsumer $cc Course consumer object
     * @return bool True if connection updated
     */
    public static function updateConsumerConnection(LSCourseConsumer $cc)
    {
        if(!$cc->valid()) {
            return false;
        }

        $wpdb = LS()->wpdb;
        $wpdb->update($wpdb->prefix . 'ls_course_consumers', $cc->getRecord(), ['consumerId' => $cc->getConsumerId(), 'courseId' => $cc->getCourseId()], null, '%d');

        self::$consumersConnections = [];

        return true;
    }

    /**
     * Retrieve all consumer connections to the posts (all courses in which consumer is participated)
     *
     * @since 2.1.0
     *
     * @param int $consumerId Consumer Id
     * @param bool $actualCourses True to get connections from only actual courses
     * @return LSCourseConsumer[]
     */
    public static function getConsumerConnections($consumerId, $actualCourses = false)
    {
        $result = [];

        if($consumerId > 0) {

            if(!isset(self::$consumersConnections[$consumerId])) {

                $wpdb = LS()->wpdb;
                $connections = $wpdb->get_results(sprintf("SELECT * FROM {$wpdb->prefix}ls_course_consumers WHERE consumerId = %d", $consumerId));
                $connections = is_array($connections) ? $connections : [];

                foreach($connections as $item) {
                    $result[] = new LSCourseConsumer($item);
                }

                self::$consumersConnections[$consumerId] = $result;

            } else {
                $result = self::$consumersConnections[$consumerId];
            }
        }

        if($actualCourses && !empty($result)) {
            $courses = self::getCoursesList(true);
            $result = array_values(array_filter($result, static function(LSCourseConsumer $cc) use($courses) {
                return isset($courses[$cc->getCourseId()]);
            }));
        }

        return $result;
    }

    /**
     * Retrieve consumer connection by specified course Id and consumer Id
     *
     * @since 2.0.0
     *
     * @param int $courseId Course Id
     * @param int $consumerId Consumer Id
     * @return false|LSCourseConsumer Course consumer object or false on failure
     */
    public static function getConsumerConnection($courseId, $consumerId)
    {
        $result = false;

        if($courseId > 0) {
            foreach(self::getConsumerConnections($consumerId) as $cc) {
                if($cc->getCourseId() === $courseId) {
                    $result = $cc;
                    break;
                }
            }
        }

        return $result;
    }

    /**
     * Check if consumer participated in the course
     *
     * @since 2.1.0
     *
     * @param int $consumerId Consumer Id
     * @param int $courseId Course Id
     * @return bool True if consumer participated
     */
    public static function isConsumerStartedCourse($consumerId, $courseId)
    {
        return self::getConsumerConnection($courseId, $consumerId) !== false;
    }

    /**
     * Check if consumer ended the course
     *
     * @since 2.2.0
     *
     * @param int $consumerId Consumer Id
     * @param int $courseId Course Id
     * @return bool True if consumer ended course
     */
    public static function isConsumerEndedCourse($consumerId, $courseId)
    {
        $cc = self::getConsumerConnection($courseId, $consumerId);

        return $cc && $cc->isEnded();
    }

    /**
     * Retrieve consumers connections by specified course Id
     *
     * @since 2.0.0
     *
     * @param int $courseId Course Id
     * @return LSCourseConsumer[]
     */
    public static function getConsumersConnectionsByCourseId($courseId)
    {
        $result = [];

        if($courseId > 0) {

            $wpdb = LS()->wpdb;
            $query = sprintf("SELECT * FROM {$wpdb->prefix}ls_course_consumers WHERE courseId = %d", $courseId);
            $data = $wpdb->get_results($query);
            $data = is_array($data) ? $data : [];

            foreach($data as $item) {
                $result[] = new LSCourseConsumer($item);
            }
        }

        return $result;
    }

    /**
     * Retrieve courses Id's in which consumer is participated
     *
     * @since 2.1.0
     *
     * @param int $consumerId Consumer Id
     * @return int[] Courses Id's
     */
    public static function getCoursesStartedByConsumer($consumerId)
    {
        $result = [];

        foreach(self::getConsumerConnections($consumerId) as $cc) {
            $result[] = $cc->getCourseId();
        }

        return $result;
    }

    /**
     * Retrieve courses Id's in which consumer finished
     *
     * @since 2.5.0
     *
     * @param int $consumerId Consumer Id
     * @return int[] Courses Id's
     */
    public static function getCoursesEndedByConsumer($consumerId)
    {
        return array_values(array_filter(self::getConsumerConnections($consumerId), static function(LSCourseConsumer $cc) {
            return $cc->isEnded();
        }));
    }

    /**
     * Disconnect consumer from the course
     *
     * @since 2.8.6
     *
     * @param LSCourseConsumer $cc Course consumer object
     * @return bool True if disconnected
     */
    public static function deleteConsumerToCourseConnection(LSCourseConsumer $cc)
    {
        if(!$cc->valid()) {
            return false;
        }

        do_action('ls_course_connection_before_delete', $cc);

        $wpdb = LS()->wpdb;

        if(!$wpdb->delete($wpdb->prefix . 'ls_course_consumers', ['consumerId' => $cc->getConsumerId(), 'courseId' => $cc->getCourseId()], '%d')) {
            return false;
        }

        self::$consumersConnections = [];

        foreach(self::getConsumerActivitiesInCourse($cc->getConsumerId(), $cc->getCourseId()) as $activity) {
            self::deleteActivity($activity);
        }

        do_action('ls_course_connection_deleted', $cc);

        return true;
    }

    /**
     * Delete course connection from the all consumers
     *
     * @since 2.8.6
     * @param int $courseId Course Id
     *
     * @return bool True if all connections delete
     */
    public static function disconnectCourseFromAllConsumers($courseId)
    {
        $success = true;

        foreach(self::getConsumersConnectionsByCourseId($courseId) as $cc) {
            $success &= self::deleteConsumerToCourseConnection($cc);
        }

        return $success;
    }

    /**
     * Delete consumer from all courses
     *
     * @since 2.8.6
     *
     * @param int $consumerId Consumer Id
     * @return bool True if consumer delete from all courses
     */
    public static function deleteConsumerFromAllCourses($consumerId) {

        $success = true;

        foreach(self::getConsumerConnections($consumerId) as $cc) {
            $success &= self::deleteConsumerToCourseConnection($cc);
        }

        return $success;
    }

    /**
     * ################  COURSE ACTIVITIES  ##########################
     */

    /**
     * Create new course activity
     *
     * @since 2.0.0
     *
     * @param LSCourseActivity $activity Course activity object
     * @return false|int Course activity Id if created or false on failure
     */
    public static function createActivity(LSCourseActivity $activity)
    {
        if(!$activity->valid()) {
            return false;
        }

        if($activity->getActionDate() == 0) {
            $activity->setActionDate(strtotime('TODAY'));
            $activity->setActionTimestamp(time());
        }

        do_action('ls_course_activity_before_create', $activity);

        $wpdb = LS()->wpdb;

        if(!$wpdb->insert($wpdb->prefix . 'ls_course_activities', $activity->getRecord())) {
            return false;
        }

        $activity->setActivityId($wpdb->insert_id);

        do_action('ls_course_activity_created', $activity);

        return $activity->getActivityId();
    }

    /**
     * Update course activity
     *
     * @since 2.0.0
     *
     * @param LSCourseActivity $activity Course activity object
     * @return bool True if updated or false on failure
     */
    public static function updateActivity(LSCourseActivity $activity)
    {
        if(!$activity->valid() || $activity->getActivityId() < 1) {
            return false;
        }

        do_action('ls_course_activity_before_update', $activity);

        $wpdb = LS()->wpdb;
        $wpdb->update($wpdb->prefix . 'ls_course_activities', $activity->getRecord(), ['activityId' => $activity->getActivityId()], null, '%d');

        do_action('ls_course_activity_updated', $activity);

        return true;
    }

    /**
     * Retrieve all activities related to specified consumer
     *
     * @since 2.6.8
     *
     * @param int $consumerId Consumer Id
     * @return LSCourseActivity[]
     */
    public static function getConsumerActivities($consumerId)
    {
        $result = [];

        if($consumerId > 0) {

            $wpdb = LS()->wpdb;
            $query = sprintf("SELECT * FROM {$wpdb->prefix}ls_course_activities WHERE consumerId = %d", $consumerId);
            $activities = $wpdb->get_results($query);

            foreach($activities as $activity) {
                $result[] = new LSCourseActivity($activity);
            }
        }

        return $result;
    }

    /**
     * Retrieve course activities related to specified consumer
     *
     * @since 1.0.0
     *
     * @param int $consumerId Consumer Id
     * @param int $courseId Course Id
     * @return LSCourseActivity[]
     */
    public static function getConsumerActivitiesInCourse($consumerId, $courseId)
    {
        $result = [];

        if($courseId > 0) {
            foreach(self::getConsumerActivities($consumerId) as $activity) {
                if($activity->getCourseId() == $courseId) {
                    $result[] = $activity;
                }
            }
        }

        return $result;
    }

    /**
     * Count the number of read articles in the course by consumer
     *
     * @since 2.8.6
     *
     * @param int $consumerId Consumer Id
     * @param int $courseId Course Id
     * @return int Number of read lessons
     */
    public static function getConsumerReadLessonsInCourse(int $consumerId, int $courseId): int
    {
        return count(self::getConsumerActivitiesInCourse($consumerId, $courseId));
    }

    /**
     * Delete course activity
     *
     * @since 2.0.0
     *
     * @param LSCourseActivity $activity Course activity object
     * @return bool True if course activity has been deleted
     */
    public static function deleteActivity(LSCourseActivity $activity)
    {
        if(!$activity->valid()) {
            return false;
        }

        $wpdb = LS()->wpdb;

        if(!$wpdb->delete($wpdb->prefix . 'ls_course_activities', ['activityId' => $activity->getActivityId()], '%d')) {
            return false;
        }

        do_action('ls_course_activity_deleted', $activity);

        return true;
    }

    /**
     * Retrieve all activities connected to specified post
     *
     * @since 2.7.0
     *
     * @param int $postId Post Id
     * @return LSCourseActivity[]
     */
    public static function getActivitiesByPostId($postId)
    {
        $wpdb = LS()->wpdb;
        $activities = $wpdb->get_results(sprintf("SELECT * FROM {$wpdb->prefix}ls_course_activities WHERE postId = %d", $postId));

        $result = [];

        foreach($activities as $activity) {
            $result[] = new LSCourseActivity($activity);
        }

        return $result;
    }

    /**
     * Retrieve course activities connected to specified post
     *
     * @since 2.7.0
     *
     * @param int $courseId Course Id
     * @param int $postId Post Id
     * @return LSCourseActivity[]
     */
    public static function getCourseActivitiesByPostId($courseId, $postId)
    {
        return array_values(array_filter(self::getActivitiesByPostId($postId), static function(LSCourseActivity $activity) use ($courseId) {
            return $activity->getCourseId() === $courseId;
        }));
    }

    /**
     * Delete all activities with lessons that are no longer available in a list of course lessons
     * @since 1.2.0
     */
    public static function cleanUpActivities()
    {
        $wpdb = LS()->wpdb;
        $wpdb->query(sprintf("DELETE FROM {$wpdb->prefix}ls_course_activities WHERE activityId IN (
                                SELECT * FROM (
		                          SELECT a.activityId FROM {$wpdb->prefix}ls_course_activities a
		                          LEFT OUTER JOIN {$wpdb->prefix}ls_course_posts r ON r.postId = a.postId AND r.courseId = a.courseId
		                          WHERE r.courseId IS NULL
		                        ) p
                              )"));
    }

    /**
     * ################  COURSE LOGIC  ##########################
     */

    /**
     * Retrieve consumers for reminder
     *
     * @since 1.0.0
     *
     * @param int $days Days after consumer missed course
     * @param int $courseId Course ID
     * @return LSCourseActivity[]
     */
    public static function getActivitiesForReminder($days, $courseId = 0)
    {
        if($days < 0) {
            return [];
        }

        $wpdb = LS()->wpdb;

        $courseWhere = [
            self::getCourseQueryForActualCheck()
        ];        if($courseId > 0) {
            $courseWhere[] = sprintf("c.courseId = %d", $courseId);
        }

        $courseWhere = empty($courseWhere) ? '' : implode(' AND ', $courseWhere) . ' AND ';

        $query = sprintf("SELECT a.*
                          FROM {$wpdb->prefix}ls_course_activities a
                          INNER JOIN ( 
                              SELECT MAX(activityId) as id from {$wpdb->prefix}ls_course_activities GROUP BY courseId, consumerId 
                            ) as a2 on a2.id = a.activityId
                          INNER JOIN {$wpdb->prefix}ls_consumers cn ON cn.consumerId = a.consumerId AND cn.active = 1 AND cn.verified = 1 AND cn.allowEmails = 1
                          INNER JOIN {$wpdb->prefix}ls_course c ON c.courseId = a.courseId
                          INNER JOIN {$wpdb->prefix}ls_course_consumers cc ON cc.consumerId = a.consumerId AND cc.courseId = a.courseId AND ( cc.endDate IS NULL OR cc.endDate = 0 )
                          INNER JOIN {$wpdb->posts} p ON p.ID = c.coursePageId
                          WHERE %s DATE_ADD(a.actionDate, INTERVAL a.interval + %d DAY) = '%s'
                          GROUP BY a.consumerId, a.courseId", $courseWhere, $days, date('Y-m-d'));

        $activities = [];

        foreach($wpdb->get_results($query) as $record) {
            $activities[] = new LSCourseActivity($record);
        }

        return $activities;
    }

    /**
     * Retrieve course participation statistics
     *
     * @since 1.0.0
     *
     * @param int $courseId Course Id
     * @param null|int $today Today date (timestamp) or null to use current day
     * @return array Statistics figures
     */
    public static function getStatistics($courseId, $today = null)
    {
        $today = $today ?? strtotime('TODAY');
        $startOfPrevWeek = date('N') == 7 ? strtotime('monday this week', $today) : strtotime('monday this week -1 week', $today);
        $endOfPrevWeek = $startOfPrevWeek + 7 * DAY_IN_SECONDS - 1;
        $startOfThisWeek = $endOfPrevWeek + 1;

        $default = [
            'today'                      => $today,
            'startOfPrevWeek'            => $startOfPrevWeek,
            'endOfPrevWeek'              => $endOfPrevWeek,
            'startOfThisWeek'            => $startOfThisWeek,
            'weeklyNewParticipants'      => 0,
            'weeklyFinishedParticipants' => 0,
            'readsLastWeek'              => 0,
            'weeklyParticipation'        => '',
            'twNewParticipants'          => 0,
            'twFinishedParticipants'     => 0,
            'readsThisWeek'              => 0,
            'overallParticipants'        => 0,
            'activeParticipants'         => 0,
            'finishedParticipants'       => 0,
            'overallParticipation'       => ''
        ];

        if($courseId < 1) {
            return $default;
        }

        $wpdb = LS()->wpdb;

        $result = $default;

        $query = sprintf("SELECT COUNT(DISTINCT cc.consumerId) FROM {$wpdb->prefix}ls_course_consumers cc WHERE cc.courseId = %d AND cc.startDate BETWEEN %d AND %d", $courseId, $startOfPrevWeek, $endOfPrevWeek);

        $result['weeklyNewParticipants'] = (int) $wpdb->get_var($query);

        $query = sprintf("SELECT COUNT(DISTINCT cc.consumerId) FROM {$wpdb->prefix}ls_course_consumers cc WHERE cc.courseId = %d AND cc.endDate BETWEEN %d AND %d", $courseId, $startOfPrevWeek, $endOfPrevWeek);

        $result['weeklyFinishedParticipants'] = (int) $wpdb->get_var($query);

        $query = sprintf("SELECT COUNT(DISTINCT activityId) FROM {$wpdb->prefix}ls_course_activities WHERE courseId = %d AND actionTimestamp BETWEEN %d AND %d", $courseId, $startOfPrevWeek, $endOfPrevWeek);

        $result['readsLastWeek'] = (int) $wpdb->get_var($query);

        $query = sprintf("SELECT COUNT(DISTINCT cc.consumerId) FROM {$wpdb->prefix}ls_course_consumers cc WHERE cc.courseId = %d AND cc.startDate >= %d", $courseId, $startOfThisWeek);

        $result['twNewParticipants'] = (int) $wpdb->get_var($query);

        $query = sprintf("SELECT COUNT(DISTINCT cc.consumerId) FROM {$wpdb->prefix}ls_course_consumers cc WHERE cc.courseId = %d AND cc.endDate >= %d", $courseId, $startOfThisWeek);

        $result['twFinishedParticipants'] = (int) $wpdb->get_var($query);

        $query = sprintf("SELECT COUNT(DISTINCT activityId) FROM {$wpdb->prefix}ls_course_activities WHERE courseId = %d AND actionTimestamp >= %d", $courseId, $startOfThisWeek);

        $result['readsThisWeek'] = (int) $wpdb->get_var($query);

        $query = sprintf("SELECT r.postId, COUNT(a.consumerId) pcp FROM {$wpdb->prefix}ls_course_posts r
                          LEFT JOIN {$wpdb->prefix}ls_course_activities a ON a.courseId = r.courseId AND a.postId = r.postId AND a.actionTimestamp BETWEEN %d AND %d
                          WHERE r.courseId = %d
                          GROUP BY r.postId", $startOfPrevWeek, $endOfPrevWeek, $courseId);

        $result['weeklyParticipation'] = [];
        foreach($wpdb->get_results($query) as $rec) {
            $result['weeklyParticipation'][$rec->postId] = (int) $rec->pcp;
        }

        $query = sprintf("SELECT COUNT(DISTINCT cc.consumerId) FROM {$wpdb->prefix}ls_course_consumers cc WHERE cc.courseId = %d", $courseId);

        $result['overallParticipants'] = (int) $wpdb->get_var($query);

        $query = sprintf("SELECT COUNT(DISTINCT cc.consumerId) FROM {$wpdb->prefix}ls_course_consumers cc WHERE cc.courseId = %d AND cc.endDate > 0", $courseId);

        $result['finishedParticipants'] = (int) $wpdb->get_var($query);

        $result['activeParticipants'] = $result['overallParticipants'] - $result['finishedParticipants'];

        $query = sprintf("SELECT r.postId, COUNT(a.consumerId) pcp FROM {$wpdb->prefix}ls_course_posts r
                          LEFT JOIN {$wpdb->prefix}ls_course_activities a ON a.courseId = r.courseId AND r.postId = a.postId
                          WHERE r.courseId = %d
                          GROUP BY r.postId", $courseId);

        $result['overallParticipation'] = [];
        foreach($wpdb->get_results($query) as $rec) {
            $result['overallParticipation'][$rec->postId] = (int) $rec->pcp;
        }

        return $result;
    }

    /**
     * Retrieve an array of courses to send notification (log) email
     * @since 1.0.0
     * @return LSCourse[] Courses
     */
    public static function getCoursesToSendNotificationEmail()
    {
        $wpdb = LS()->wpdb;
        $query = sprintf("SELECT c.courseId 
                          FROM {$wpdb->prefix}ls_course c 
                          INNER JOIN {$wpdb->posts} p ON p.ID = c.coursePageId
                          WHERE c.sendLog = 1 AND %s", self::getCourseQueryForActualCheck());
        $records = $wpdb->get_col($query);
        $records = is_array($records) ? $records : [];

        $result = [];

        foreach($records as $record) {
            $result[] = new LSCourse($record);
        }

        return $result;
    }

    /**
     * Calculate consumer progress in the course
     *
     * @since 1.2.0
     *
     * @param int $courseId Course Id
     * @param int $consumerId Consumer Id
     * @return int Progress in %
     */
    public static function calculateConsumerProgress($courseId, $consumerId)
    {
        $connectionsInCourse = count(self::getLessons($courseId));
        $consumerActivities = count(self::getConsumerActivitiesInCourse($consumerId, $courseId));

        return min(100, round($connectionsInCourse > 0 ? $consumerActivities / $connectionsInCourse * 100 : 0));
    }

    /**
     * Retrieve course activities stats
     *
     * @since 1.3.0
     *
     * @param int $courseId Course Id
     * @param int $startDate Start date
     * @param int $endDate End date
     * @param array $columns Columns to fetch
     * @return array Results
     */
    public static function getStats($courseId, $startDate = 0, $endDate = 0, $columns = [])
    {
        if($startDate < 1 || $startDate > $endDate || $endDate >= strtotime('TOMORROW')) {
            $startDate = strtotime('TODAY -1 MONTH');
            $endDate = strtotime('TODAY');
        }

        // round to start of the day
        $startDate = strtotime('TODAY', $startDate);
        $endDate = strtotime('TODAY', $endDate);

        $sd = $startDate;
        $ed = $endDate;
        $columns = array_values(array_unique(array_intersect($columns, ['overallParticipants', 'newParticipants', 'activeParticipants', 'finishedParticipants', 'reads'])));
        $monthInterval = strtotime(date('Y-m-d', $endDate) . ' -1 YEAR') >= $startDate; // switch view to months
        $weekInterval = !$monthInterval && strtotime(date('Y-m-d', $endDate) . '-3 MONTH') >= $startDate; // switch view to weeks

        // format start date depends on the range
        if($monthInterval) {
            $startDate = strtotime(date('Y-m-01', $startDate)); // round to start of the month
        } elseif($weekInterval) {
            $startDate = LS\Library::getMonday($startDate); // round to start of the week
        }

        // prepare group condition depends on the range
        if($monthInterval) {
            $groupBy = static function($field, $timestamp = true) {
                $field = $timestamp ? sprintf("FROM_UNIXTIME(%s)", $field) : $field;

                return sprintf("CONCAT(YEAR(%s), MONTH(%s))", $field, $field); // take every 1th day of the month
            };
        } elseif($weekInterval) {
            $groupBy = static function($field, $timestamp = true) {
                $field = $timestamp ? sprintf("FROM_UNIXTIME(%s)", $field) : $field;

                return sprintf("CONCAT(YEAR(%s), WEEKOFYEAR(%s))", $field, $field); // take every Monday
            };
        } else {
            $groupBy = static function($field, $timestamp = true) {
                $field = $timestamp ? sprintf("FROM_UNIXTIME(%s)", $field) : $field;

                return sprintf("DATE(%s)", $field); // take every Monday
            };
        }

        $data = $labels = [];
        $wpdb = LS()->wpdb;

        foreach($columns as $key => $column) {
            $columns[$key] = esc_sql($column);
        }

        if(!empty($columns) && $courseId > 0) {

            $instantData = $dbData = [];

            $arrayColumn = static function($data) {
                $result = [];
                foreach($data as $row) {
                    $result[$row->eventDate] = $row->items;
                }

                return $result;
            };

            /* Instant-increasing figures */
            if(in_array('overallParticipants', $columns)) {

                $query = sprintf("SELECT COUNT(consumerId) items, DATE(FROM_UNIXTIME(startDate)) eventDate
                              FROM {$wpdb->prefix}ls_course_consumers 
                              WHERE courseId = %d AND startDate BETWEEN %d AND %d 
                              GROUP BY %s", $courseId, $startDate, $endDate + DAY_IN_SECONDS - 1, $groupBy('startDate'));
                $instantData['overallParticipants'] = $arrayColumn($wpdb->get_results($query));

                $query = sprintf("SELECT COUNT(consumerId) 
                              FROM {$wpdb->prefix}ls_course_consumers 
                              WHERE courseId = %d AND startDate < %d", $courseId, $startDate);
                $instantData['overallParticipantsStartCount'] = (int) $wpdb->get_var($query);
            }

            if(in_array('activeParticipants', $columns)) {

                $query = sprintf("SELECT COUNT(consumerId) items, DATE(FROM_UNIXTIME(startDate)) eventDate
                              FROM {$wpdb->prefix}ls_course_consumers 
                              WHERE courseId = %d AND startDate BETWEEN %d AND %d
                              GROUP BY %s", $courseId, $startDate, $endDate + DAY_IN_SECONDS - 1, $groupBy('startDate'));
                $started = $arrayColumn($wpdb->get_results($query));

                $query = sprintf("SELECT COUNT(consumerId) items, DATE(FROM_UNIXTIME(endDate)) eventDate
                              FROM {$wpdb->prefix}ls_course_consumers 
                              WHERE courseId = %d AND endDate BETWEEN %d AND %d
                              GROUP BY %s", $courseId, $startDate, $endDate + DAY_IN_SECONDS - 1, $groupBy('endDate'));
                $finished = $arrayColumn($wpdb->get_results($query));

                $dates = $started;
                foreach($finished as $eventDate => $items) {
                    if(isset($dates[$eventDate])) {
                        $dates[$eventDate] -= $items;
                    } else {
                        $dates[$eventDate] = -$items;
                    }
                }

                ksort($dates);

                $instantData['activeParticipants'] = $dates;

                $query = sprintf("SELECT COUNT(consumerId) 
                              FROM {$wpdb->prefix}ls_course_consumers 
                              WHERE courseId = %d AND endDate IS NULL AND startDate < %d", $courseId, $startDate);
                $instantData['activeParticipantsStartCount'] = (int) $wpdb->get_var($query);
            }

            /* Day-based figures */
            if(in_array('newParticipants', $columns)) {
                $query = sprintf("SELECT COUNT(consumerId) items, DATE(FROM_UNIXTIME(startDate)) eventDate
                              FROM {$wpdb->prefix}ls_course_consumers 
                              WHERE courseId = %d AND startDate BETWEEN %d AND %d
                              GROUP BY %s", $courseId, $startDate, $endDate + DAY_IN_SECONDS - 1, $groupBy('startDate'));
                $dbData['newParticipants'] = $arrayColumn($wpdb->get_results($query));
            }

            if(in_array('finishedParticipants', $columns)) {
                $query = sprintf("SELECT COUNT(consumerId) items, DATE(FROM_UNIXTIME(endDate)) eventDate
                              FROM {$wpdb->prefix}ls_course_consumers 
                              WHERE courseId = %d AND endDate BETWEEN %d AND %d
                              GROUP BY %s", $courseId, $startDate, $endDate + DAY_IN_SECONDS - 1, $groupBy('endDate'));
                $dbData['finishedParticipants'] = $arrayColumn($wpdb->get_results($query));
            }

            if(in_array('reads', $columns)) {
                $query = sprintf("SELECT COUNT(activityId) items, actionDate eventDate
                              FROM {$wpdb->prefix}ls_course_activities 
                              WHERE courseId = %d AND actionDate BETWEEN '%s' AND '%s' 
                              GROUP BY %s", $courseId, date('Y-m-d', $startDate), date('Y-m-d', $endDate), $groupBy('actionDate', false));
                $dbData['reads'] = $arrayColumn($wpdb->get_results($query));
            }

            foreach($columns as $colNum => $column) {

                $data[$column] = [];

                for($day = $startDate; $day <= $endDate; $day += DAY_IN_SECONDS) {

                    // get only Mondays for weekly presentation
                    if($weekInterval && date('N', $day) != 1) {
                        continue;
                    }

                    // get first days of each year for month presentation
                    if($monthInterval && date('j', $day) != 1) {
                        continue;
                    }

                    // default value
                    $data[$column][date('Y-m-d', $day)] = 0;

                    // create label
                    if($colNum === 0) {
                        if($monthInterval) {
                            $labels[] = date('m.Y', $day);
                        } elseif($weekInterval) {
                            $labels[] = LS\Library::getShortDatesRange($day, min(time(), $day + WEEK_IN_SECONDS - 1));
                        } else {
                            $labels[] = date('d.m.Y', $day);
                        }
                    }
                }
            }

            // fill data with instant values
            foreach($columns as $column) {
                if(isset($instantData[$column])) {

                    $itemsCount = $instantData[$column . 'StartCount'];

                    for($day = $startDate; $day <= $endDate; $day += DAY_IN_SECONDS) {

                        $eventDate = date('Y-m-d', $day);

                        if(isset($instantData[$column][$eventDate])) {
                            $itemsCount += $instantData[$column][$eventDate];
                        }

                        if($monthInterval) {
                            $data[$column][date('Y-m-01', $day)] = $itemsCount;
                        } elseif($weekInterval) {
                            $data[$column][date('Y-m-d', LS\Library::getMonday($day))] = $itemsCount;
                        } else {
                            $data[$column][$eventDate] = $itemsCount;
                        }
                    }
                }
            }

            // fill data with daily values
            foreach($columns as $column) {
                if(isset($dbData[$column])) {
                    foreach($dbData[$column] as $eventDate => $itemsCount) {
                        if($monthInterval) {
                            $data[$column][date('Y-m-01', strtotime($eventDate))] = $itemsCount;
                        } elseif($weekInterval) {
                            $data[$column][date('Y-m-d', LS\Library::getMonday(strtotime($eventDate)))] = $itemsCount;
                        } else {
                            $data[$column][$eventDate] = $itemsCount;
                        }
                    }
                }
            }

            // skip keys
            foreach($data as $n => $col) {
                $data[$n] = array_values($col);
            }

            // custom view for a single chart line
            if(count($labels) === 1) {

                $labels[] = $labels[0];

                foreach($columns as $column) {
                    $data[$column][] = $data[$column][0];
                }
            }
        }

        return [
            'startDate' => date('Y-m-d', $sd),
            'endDate'   => date('Y-m-d', $ed),
            'columns'   => $columns,
            'labels'    => $labels,
            'data'      => $data
        ];
    }

    /**
     * ################  MISC  ##########################
     */

    /**
     * Delete course tables
     * @since 1.0.0
     */
    public static function deleteTables()
    {
        $wpdb = LS()->wpdb;

        foreach(['ls_course_activities', 'ls_course_consumers', 'ls_course', 'ls_course_posts'] as $t) {
            $wpdb->query(sprintf("DROP TABLE IF EXISTS %s", $wpdb->prefix . $t));
        }
    }

    /**
     * Retrieve posts statuses for the specified post Id's
     *
     * @since 2.0.0
     *
     * @param int[] $postIds Post Id's
     * @return array[int]string Array of pairs (id => post_status)
     */
    public static function getPostsStatuses($postIds)
    {
        if(empty($postIds)) {
            return [];
        }

        $wpdb = LS()->wpdb;
        $query = sprintf("SELECT ID, post_status FROM {$wpdb->posts} WHERE ID IN (%s)", implode(',', $postIds));
        $statuses = $wpdb->get_results($query, ARRAY_A);
        $statuses = array_column($statuses, 'post_status', 'ID');

        return $statuses;
    }

    /**
     * ################  DEPRECATED  ##########################
     */

    /**
     * Disconnect consumer from the course
     *
     * @since 2.0.0
     * @deprecated 02.2019 Use deleteConsumerConnection()
     *
     * @param LSCourseConsumer $cc Course consumer object
     * @return bool True if disconnected
     */
    public static function deleteConsumerConnection(LSCourseConsumer $cc)
    {
        _deprecated_function(__FUNCTION__, '2.8.6', 'deleteConsumerToCourseConnection');

        return self::deleteConsumerToCourseConnection($cc);
    }

    /**
     * Delete all consumers from the course
     *
     * @since 1.3.0
     * @deprecated 02.2019 Use disconnectCourseFromAllConsumers()
     *
     * @param int $courseId Course Id
     */
    public static function deleteConsumersFromCourse($courseId)
    {
        _deprecated_function(__FUNCTION__, '2.8.6', 'disconnectCourseFromAllConsumers');

        self::disconnectCourseFromAllConsumers($courseId);
    }

    /**
     * Delete consumer from all courses
     *
     * @since 1.3.0
     * @deprecated 02.2019 Use deleteConsumerFromAllCourses()
     *
     * @param int $consumerId Consumer Id
     */
    public static function deleteConsumerFromCourses($consumerId)
    {
        _deprecated_function(__FUNCTION__, '2.8.6', 'deleteConsumerFromAllCourses');

        self::deleteConsumerFromAllCourses($consumerId);
    }
}