<?php
class courseAdminLoyaltySuite extends courseLoyaltySuite implements \LS\ModuleAdminInterface
{
    /**
     * @since 2.7.0
     * @return bool
     */
    public function hasAdministrationPage()
    {
        return true;
    }

    /**
     * Main function to load models, create actions and filters, init variables
     * @since 2.7.0
     * @internal
     */
    public function load()
    {
        parent::load();

        add_action('admin_enqueue_scripts', [$this, 'printScripts']);
        add_action('admin_init', [$this, 'adminInit']);
        add_action('wp_ajax_courseDashboardFigures', [$this, 'courseDashboardFigures']);
        add_action('wp_ajax_courseActivities', [$this, 'courseActivities']);
        add_action('add_meta_boxes', [$this, 'courseMetaBox'], 5);
        add_action('save_post', [$this, 'courseSavePost']);
        add_filter('manage_course-lesson_posts_columns', [$this, 'addColumn']);
        add_action('manage_course-lesson_posts_custom_column', [$this, 'addRowForColumn'], 10, 2);
        add_action('ls_cron_daily', [$this, 'cronJobDaily']);

        // connect to WP posts
        add_action('delete_post', [$this, 'postDelete']); // might be ajax request

        // connect to Consumers
        add_filter('ls_consumer_log__course-started', [$this, 'consumerLog']);
        add_filter('ls_consumer_log__course-lesson-read', [$this, 'consumerLog']);
        add_filter('ls_consumer_log__course-ended', [$this, 'consumerLog']);
        add_filter('ls_consumer_log__course-stopped', [$this, 'consumerLog']);
        add_filter('consumerDetails', [$this, 'consumerDetailsFilter'], 10, 2);
    }

    /**
     * Create shortcodes
     * @since 2.7.0
     */
    public function initShortcodes()
    {
        parent::initShortcodes();

        LS()->addShortcodeInfo('course', __('Course', 'ls'), __('Display course', 'ls'), self::getName(), [
            'id' => __('Course ID', 'ls')
        ], '[course id=1]');

        LS()->addShortcodeInfo('course-start', __('Link to start the course', 'ls'), '', self::getName(), [
            'id'    => __('Course ID', 'ls'),
            'class' => __('CSS class attributes', 'ls') . ' (' . __('optional', 'ls') . ')'
        ], '[course-start id=1]Start [course-name id=1][/course-start]');

        LS()->addShortcodeInfo('course-lesson-cockpit', __('Display cockpit for the current course lesson', 'ls'), '', self::getName(), [
            'class' => __('CSS class attributes', 'ls') . ' (' . __('optional', 'ls') . ')'
        ]);

        LS()->addShortcodeInfo('course-accept', __('Link to mark post as read', 'ls'), '', self::getName(), [
            'class'           => __('CSS class attributes', 'ls') . ' (' . __('optional', 'ls') . ')',
            'accessible_text' => __('Text to display after the article marked as read', 'ls') . ' (' . __('optional', 'ls') . ')'
        ]);

        LS()->addShortcodeInfo('course-name', __('Course name', 'ls'), '', self::getName(), [
            'id' => __('Course ID', 'ls')
        ], '[course-name id=1]');

        LS()->addShortcodeInfo('course-progress-bar', __('Display consumer progress bar in the challenge', 'ls'), '', self::getName(), [
            'id'    => __('Course ID', 'ls'),
            'class' => __('CSS class attributes', 'ls') . ' (' . __('optional', 'ls') . ')'
        ]);

        LS()->addShortcodeInfo('courses-overview', __('Display consumer courses overview', 'ls'), '', self::getName());
    }

    /**
     * After module has been installed
     * @since 2.7.0
     * @internal
     */
    public function onInstall()
    {
        require_once __DIR__ . '/install.php';

        courseInstallLoyaltySuite();
    }

    /**
     * On uninstall module
     * @since 2.7.0
     * @internal
     */
    public function onUninstall()
    {
        LSCourseHelper::deleteTables();
    }

    /**
     * After module activation
     * @since 2.7.0
     * @internal
     */
    public function afterActivate()
    {
        self::addCapsToRole(['ls_course'], 'contributor');
        self::addCapsToRole(['ls_course', 'ls_course_manage'], 'author');
        self::addCapsToRole(['ls_course', 'ls_course_manage'], 'editor');
        self::addCapsToRole(['ls_course', 'ls_course_manage', 'ls_course_settings'], 'administrator');
    }

    /**
     * Load module scripts
     * @since 2.7.0
     * @internal
     */
    public function printScripts()
    {
        wp_enqueue_style('ls-course-admin', $this->getUrl2Module() . 'css/admin-course.css', [], $this->getVersion());
        wp_enqueue_script('jquery-ui-sortable');
        wp_enqueue_script('ls-course-admin', $this->getUrl2Module() . 'js/admin-course.js', [], $this->getVersion(), true);
    }

    /**
     * Render administration menu
     * @since 2.7.0
     * @internal
     */
    public function moduleAdministrationPage()
    {
        include_once __DIR__ . '/tables/courses.php';

        $template = new LS\Template(__DIR__ . '/views/backend');
        $template->assign('options', $this->getOptions());

        if(!isset($_REQUEST['view'])) {

            $courses = LSCourseHelper::getCourses();
            $dashboardCourseId = 0;

            foreach($courses as $course) {
                if($dashboardCourseId === 0 && $course->isActual()) {
                    $dashboardCourseId = $course->getCourseId();
                }
            }

            $template
                ->assign('coursesTable', new LSCoursesTable())
                ->assign('courses', $courses)
                ->assign('dashboardCourseId', $dashboardCourseId)
                ->assign('dashboard', LSCourseHelper::getStatistics($dashboardCourseId))
                ->assign('tabs', [
                    ['name' => 'ls-courses', 'title' => __('Courses', 'ls'), 'view' => 'admin-courses.phtml'],
                    ['name' => 'ls-settings', 'title' => __('Settings', 'ls'), 'view' => 'admin-settings.phtml', 'capability' => 'ls_course_settings']
                ]);
        }

        if($this->isAction('course-save-settings')) {
            $template->assign('options', stripslashes_deep($_POST));
        }

        $template->renderAdminPage();
    }

    /**
     * Process admin actions and requests
     * @since 2.7.0
     * @internal
     */
    public function adminInit()
    {
        if(!$this->hasAction() || !current_user_can('ls_course')) {
            return;
        }

        if($this->isAction('course-reset') && current_user_can('ls_course_manage')) {

            $consumerId = $this->getParam('consumerId', 'id');
            $courseId = $this->getParam('courseId', 'id');

            if($this->stopCourse($courseId, $consumerId)) {
                $this->setAlert(__('Course has been reset', 'ls'));
                LS()->logUserAction(sprintf('Course: course reset (ID: %d)', $courseId), 'warning');
            }

            \LS\Library::redirect(remove_query_arg(['action', 'action2']));
        }

        if($this->isCurrentPluginPage()) {

            if($this->isAction('course-delete') && isset($_REQUEST['courseId']) && current_user_can('ls_course_manage')) {

                $courseIds = $this->getParam('courseId', 'int_array');
                if(!empty($courseIds)) {

                    foreach($courseIds as $courseId) {
                        LSCourseHelper::deleteCourse($courseId);
                    }

                    $this->setAlert(__('Selected records have been deleted', 'ls'));
                    LS()->logUserAction(sprintf('Course: course(s) deleted (ID: %s)', implode(',', $courseIds)), 'warning');
                }

                \LS\Library::redirect(remove_query_arg(['action', 'action2', 'courseId']));
            }

            if($this->isAction('course-save-settings') && isset($_REQUEST['submit']) && current_user_can('ls_course_settings')) {
                if($this->updateSettingsPage()) {
                    $this->setAlert(__('Settings have been saved', 'ls'));
                    LS()->logUserAction('Course: settings updated');
                    \LS\Library::redirect(remove_query_arg('action'));
                }
            }
        }
    }

    /**
     * Save module settings
     * @since 2.7.0
     * @return bool Returns true if settings have been saved
     */
    private function updateSettingsPage()
    {
        $success = true;

        $options = [
            'acp'                  => $this->getParam('acp', 'checkbox'),
            'courseTypeSlug'       => $this->getParam('courseTypeSlug', 'text'),
            'courseLessonTypeSlug' => $this->getParam('courseLessonTypeSlug', 'text')
        ];

        flush_rewrite_rules();

        if(!$this->hasErrors()) {
            $this->setOptions(array_merge($this->getOptions(), $options));
        } else {
            $success = false;
        }

        return $success;
    }

    /**
     * Add course meta-box to the course page
     * @since 2.7.0
     * @internal
     */
    public function courseMetaBox()
    {
        add_meta_box('course', __('Course details', 'ls'), [$this, 'courseMetaBoxContent'], 'course', 'normal', 'high');
    }

    /**
     * Display course meta-box
     *
     * @since 2.7.0
     * @internal
     *
     * @param WP_Post $post
     */
    public function courseMetaBoxContent($post)
    {
        $course = LSCourseHelper::getCourseByCoursePageId($post->ID);
        $course = $course ?: new LSCourse();

        if(!empty($_POST)) {
            $course->setFromArray(stripslashes_deep($_POST));
        }

        (new LS\Template(__DIR__ . '/views/backend'))
            ->assign('nonce', wp_create_nonce('course-meta-box'))
            ->assign('course', $course)
            ->assign('courseId', $course->getCourseId())
            ->assign('postConnections', LSCourseHelper::getLessons($course->getCourseId()))
            ->render('admin-course.phtml');
    }

    /**
     * Save course meta
     *
     * @since 2.7.0
     * @internal
     *
     * @param int $postId Post Id
     */
    public function courseSavePost($postId)
    {
        if(defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
            return;
        }

        if(!isset($_POST['courseMetaBoxNonce']) || !wp_verify_nonce($_POST['courseMetaBoxNonce'], 'course-meta-box')) {
            return;
        }

        $post = get_post($postId);
        $postType = get_post_type_object($post->post_type);
        if(in_array($post->post_status, ['inherit', 'trash']) || !$postType || !current_user_can($postType->cap->edit_post, $postId)) {
            return;
        }

        $courseId = $this->getParam('courseId', 'id');

        $course = LSCourseHelper::getCourse($courseId);
        $course = $course ?: new LSCourse();

        $courseObjective = $this->getParam('courseObjective', 'html');
        $courseTargetGroup = $this->getParam('courseTargetGroup', 'html');
        $trainerName = $this->getParam('trainerName', 'text');
        $trainerOccupation = $this->getParam('trainerOccupation', 'text');
        $trainerImage = $this->getParam('trainerImage', 'ls_image');
        $startDate = $this->getParam('startDate', 'date');
        $endDate = $this->getParam('endDate', 'date');
        $sendLog = $this->getParam('sendLog', 'checkbox');
        $logEmail = $this->getParam('logEmail', 'emails');
        $logSubject = $this->getParam('logSubject', 'text');
        $logBody = $this->getParam('logBody', 'html');
        $failureMessage = $this->getParam('failureMessage', 'html');
        $defaultInterval = $this->getParam('defaultInterval', 'int');
        $minInterval = $this->getParam('minInterval', 'int');
        $maxInterval = $this->getParam('maxInterval', 'int');
        $email = $this->getParam('testCourseEmail', 'email');
        $sendTestEmail = $this->getParam('sendTestCourseEmail', 'checkbox');

        if($sendLog && empty($logEmail)) {

            $this->setError(__('Please enter notification email recipients', 'ls'));

        } elseif($sendLog && empty($logSubject)) {

            $this->setError(__('Please enter notification email subject', 'ls'));

        } elseif($sendLog && empty($logBody)) {

            $this->setError(__('Please enter notification email body', 'ls'));

        } elseif($startDate > 0 && $endDate > 0 && $startDate > $endDate) {

            $this->setError(__('Course end date must be after the start date', 'ls'));

        } elseif($minInterval > $maxInterval) {

            $this->setError(__('The minimum interval value can not be after the maximum interval value', 'ls'));

        } elseif($minInterval > $defaultInterval || $defaultInterval > $maxInterval) {

            $this->setError(__('The default interval value must be between the minimum and maximum interval values', 'ls'));

        } else {

            $course->setFromArray([
                'coursePageId'      => $postId,
                'courseObjective'   => $courseObjective,
                'courseTargetGroup' => $courseTargetGroup,
                'trainerName'       => $trainerName,
                'trainerOccupation' => $trainerOccupation,
                'trainerImage'      => $trainerImage,
                'startDate'         => $startDate,
                'endDate'           => $endDate,
                'sendLog'           => $sendLog,
                'logEmail'          => $logEmail,
                'logSubject'        => $logSubject,
                'logBody'           => $logBody,
                'failureMessage'    => $failureMessage,
                'defaultInterval'   => $defaultInterval,
                'minInterval'       => $minInterval,
                'maxInterval'       => $maxInterval
            ]);

            if($courseId > 0) {
                if(!LSCourseHelper::updateCourse($course)) {
                    $courseId = 0;
                }
            } else {
                $courseId = LSCourseHelper::createCourse($course);
            }

            if($courseId < 1) {
                $this->setError(__('Can not save course', 'ls'));
                self::logBreakpoint();
                return;
            }

            LSCourseHelper::updateLessons($courseId, $this->getParam('posts', 'int_array'));

            $this->setAlert(__('Course has been saved', 'ls'));

            if($sendTestEmail) {
                if($this->sendWeeklyNotificationEmail($course, $email, $logSubject, $logBody)) {
                    $this->setAlert(__('Email has been sent', 'ls'));
                } else {
                    $this->setError(__('Can not send email. Please check entered data.', 'ls'));
                }
            }
        }
    }

    /**
     * Send weekly notification email
     *
     * @since 2.7.0
     *
     * @param LSCourse $course Course object
     * @param string|array $recipients Email or list of emails
     * @param string $subject Email subject
     * @param string $body Email body
     * @return bool True if email has been sent out
     */
    private function sendWeeklyNotificationEmail($course, $recipients, $subject, $body)
    {
        $connections = LSCourseHelper::getLessons($course->getCourseId());
        $statistic = LSCourseHelper::getStatistics($course->getCourseId());

        $generateTable = static function($participants) use ($connections) {

            $content = '<table>';
            $content .= '<tr><th width="5%" align="center" style="width: 3%; text-align: center;">&nbsp;</th><th align="left" width="57%" style="width: 55%; text-align: left;">' . __('Course post', 'ls') . '</th><th align="left" width="40%" style="width: 40%; text-align: left; max-width: 200px;">' . __('Read (times)', 'ls') . '</th></tr>';

            foreach($connections as $i => $lesson) {
                if(isset($participants[$lesson->getPostId()])) {
                    $pcp = (int) $participants[$lesson->getPostId()];
                    $content .= '<tr>
                        <td>' . ($i + 1) . '.</td>
                        <td>' . get_the_title($lesson->getPostId()) . '</td>
                        <td>' . $pcp . '</td>
                    </tr>';
                }
            }

            $content .= '</table>';

            return $content;
        };

        $rep = [
            '{courseName}'                 => $course->getCourseName(),
            '{weekRange}'                  => date('d.m.Y', $statistic['startOfPrevWeek']) . ' - ' . date('d.m.Y', $statistic['endOfPrevWeek']),
            '{weeklyNewParticipants}'      => $statistic['weeklyNewParticipants'],
            '{weeklyFinishedParticipants}' => $statistic['weeklyFinishedParticipants'],
            '{readsLastWeek}'              => $statistic['readsLastWeek'],
            '{weeklyParticipation}'        => $generateTable($statistic['weeklyParticipation']),
            '{twNewParticipants}'          => $statistic['twNewParticipants'],
            '{twFinishedParticipants}'     => $statistic['twFinishedParticipants'],
            '{readsThisWeek}'              => $statistic['readsThisWeek'],
            '{overallParticipants}'        => $statistic['overallParticipants'],
            '{activeParticipants}'         => $statistic['activeParticipants'],
            '{finishedParticipants}'       => $statistic['finishedParticipants'],
            '{overallParticipation}'       => $generateTable($statistic['overallParticipation'])
        ];

        $subject = str_replace(array_keys($rep), array_values($rep), $subject);
        $body = str_replace(array_keys($rep), array_values($rep), $body);

        return LS\Mail::mail([
            'to'      => $recipients,
            'subject' => $subject,
            'body'    => $body
        ]);
    }

    /**
     * Post deleted. Delete all connections
     *
     * @since 2.7.0
     * @internal
     *
     * @param int $postId Deleted post ID
     */
    public function postDelete($postId)
    {
        $lesson = LSCourseHelper::getLessonByLessonPostId($postId);
        if($lesson) {
            LSCourseHelper::deleteLesson($lesson);
        }
    }

    /**
     * Retrieve URL to edit course
     *
     * @since 2.7.0
     *
     * @param int $courseId Course Id
     * @param string $referer Referer
     * @return string URL
     */
    public static function courseEditLink($courseId, $referer = '')
    {
        $referer = empty($referer) ? $_SERVER['REQUEST_URI'] : $referer;
        $referer = urlencode($referer);

        return $courseId > 0 ? admin_url('admin.php?page=courseLoyaltySuite&view=course&courseId=' . $courseId . '&referer=' . $referer) : '';
    }

    /**
     * Format messages for consumer log
     *
     * @since 2.7.0
     * @internal
     *
     * @param object $log Log details
     * @return string Event text
     */
    public function consumerLog($log)
    {
        $event = '';

        if($log->messageType === 'course-started') {
            $event = __('Course started', 'ls');
        } elseif($log->messageType === 'course-lesson-read') {
            $event = __('Course lesson read', 'ls');
        } elseif($log->messageType === 'course-ended') {
            $event = __('Course ended', 'ls');
        } elseif($log->messageType === 'course-stopped') {
            $event = __('Course stopped', 'ls');
        }

        if($log->messageType === 'course-lesson-read') {
            if(isset($log->note) && $log->note > 0) {
                $event .= ': <a href="' . get_edit_post_link($log->note) . '" target="_blank">' . get_the_title($log->note) . '</a>';
            }
        } else {
            $courses = LSCourseHelper::getCoursesList();
            if(isset($log->note, $courses[$log->note]) && $log->note > 0) {
                $event .= ': <a href="' . self::courseEditLink($log->note) . '" target="_blank">' . $courses[$log->note] . '</a>';
            }
        }

        return $event;
    }

    /**
     * Display information about the courses on the consumer details page
     *
     * @since 2.7.0
     * @internal
     *
     * @param array $tabs A list of consumer tabs
     * @param int $consumerId Consumer ID
     * @return array Modified list of consumer tabs
     */
    public function consumerDetailsFilter($tabs, $consumerId)
    {
        $data = [];
        foreach(LSCourseHelper::getCoursesList() as $courseId => $courseName) {
            if(LSCourseHelper::isConsumerStartedCourse($consumerId, $courseId)) {
                $data[] = (object) [
                    'courseId'   => $courseId,
                    'courseName' => $courseName,
                    'lessons'    => $this->buildConsumerCourse($courseId, $consumerId),
                    'ended'      => LSCourseHelper::isConsumerEndedCourse($consumerId, $courseId)
                ];
            }
        }

        $content = (new LS\Template(__DIR__ . '/views/backend'))
            ->assign('data', $data)
            ->assign('consumerId', $consumerId)
            ->render('admin-consumer-courses.phtml', false);

        $tabs['courses'] = [__('Courses', 'ls'), $content];

        return $tabs;
    }

    /**
     * Process CRON Job
     * @since 2.7.0
     * @internal
     */
    public function cronJobDaily()
    {
        if(self::lock('course')) {

            // send notification email
            if($this->getOption('nextNotificationDate', strtotime('next monday')) >= time()) {

                foreach(LSCourseHelper::getCoursesToSendNotificationEmail() as $course) {

                    $recipients = $course->getLogEmail();
                    $subject = $course->getLogSubject();
                    $body = $course->getLogBody();

                    if(!empty($recipients) && !empty($subject) && !empty($body)) {
                        $this->sendWeeklyNotificationEmail($course, $recipients, $subject, $body);
                    }
                }

                $this->setOption('nextNotificationDate', strtotime('next monday'));
            }

            // unpublish pages depends on course end date
            if($this->getOption('acp')) {
                foreach(LSCourseHelper::getCoursesEndedToday() as $course) {
                    $post = $course->getPost();
                    if($post && $post->post_status == 'publish') {
                        wp_update_post([
                            'ID'          => $course->getCoursePageId(),
                            'post_status' => 'draft'
                        ]);
                    }
                }
            }

            // publish pages depends on course start date
            if($this->getOption('acp')) {

                foreach(LSCourseHelper::getCoursesStartedToday() as $course) {
                    $post = $course->getPost();
                    if($post && $post->post_status == 'draft') {
                        wp_update_post([
                            'ID'          => $course->getCoursePageId(),
                            'post_status' => 'publish'
                        ]);
                    }
                }
            }

            self::unlock('course');
        }
    }

    /**
     * Send out course dashboard figures
     *
     * @ajax
     * @since 2.7.0
     * @internal
     */
    public function courseDashboardFigures()
    {
        if(current_user_can('ls_course')) {
            wp_send_json(LSCourseHelper::getStatistics($this->getParam('courseId', 'id')));
        }

        exit;
    }

    /**
     * Send out course activities figures
     *
     * @ajax
     * @since 2.7.0
     * @internal
     */
    public function courseActivities()
    {
        if(current_user_can('ls_course')) {
            $courseId = $this->getParam('courseId', 'id');
            $startDate = $this->getParam('startDate', 'date');
            $endDate = $this->getParam('endDate', 'date');
            $columns = $this->getParam('columns', 'array');

            wp_send_json(LSCourseHelper::getStats($courseId, $startDate, $endDate, $columns));
        }

        exit;
    }

    /**
     * Add column "Course" to the list of course lessons (overview page)
     *
     * @since 2.7.0
     *
     * @param array $columns A list of columns
     * @return array A list of columns with new column
     */
    public function addColumn($columns)
    {
        $columns['course'] = __('Course', 'ls');

        return $columns;
    }

    /**
     * Show course name for each course lesson (overview page)
     *
     * @since 2.7.0
     *
     * @param string $colName Column name
     * @param int $postId Post ID
     */
    public function addRowForColumn($colName, $postId)
    {
        if($colName == 'course' && $postId > 0) {
            $course = LSCourseHelper::getCourseByLessonPostId($postId);
            if($course) {
                echo '<a href="' . get_edit_post_link($course->getCoursePageId()) . '">' . $course->getCourseName() . '</a>';
            }
        }
    }
}