<?php
class LSConsumerCourseItem
{
    /** @var int */
    protected $courseId = 0;

    /** @var int */
    protected $postId = 0;

    /** @var bool */
    protected $accessible = true;

    /** @var bool */
    protected $read = false;

    /** @var int */
    protected $accessibleOn = 0;

    /** @var bool */
    protected $accessibleSoon = false;

    /** @var int */
    protected $readDate = 0;

    /**
     * LSConsumerCourse constructor.
     *
     * @since 1.1.0
     *
     * @param int $courseId Course Id
     * @param int $postId Post Id
     */
    public function __construct($courseId, $postId)
    {
        $this->courseId = (int) $courseId;
        $this->postId = (int) $postId;
    }

    /**
     * Retrieve post id
     * @since 1.1.0
     * @return int Post Id
     */
    public function getPostId()
    {
        return $this->postId;
    }

    /**
     * Retrieve course id
     * @since 1.1.0
     * @return int Course Id
     */
    public function getCourseId()
    {
        return $this->courseId;
    }

    /**
     * Retrieve scheduled dated
     * @since 1.1.0
     * @return int Timestamp
     */
    public function getScheduledDate()
    {
        return $this->isScheduled() ? $this->accessibleOn : 0;
    }

    /**
     * Retrieve accessible date
     * @since 2.7.0
     * @return int Timestamp
     */
    public function getAccessibleDate()
    {
        return $this->accessibleOn;
    }

    /**
     * Retrieve read date
     * @since 1.3.1
     * @return int Timestamp
     */
    public function getReadDate()
    {
        return $this->readDate;
    }

    /**
     * Mark post as accessible by consumer
     * @since 1.1.0
     * @return self
     */
    public function markAsAccessible()
    {
        $this->accessible = true;

        return $this;
    }

    /**
     * Restrict access to the post by consumer
     * @since 2.0.0
     * @return self
     */
    public function restrictAccess()
    {
        $this->accessible = false;

        return $this;
    }

    /**
     * Mark post as read by current consumer
     *
     * @since 1.1.0
     *
     * @param int $readDate Timestamp; Date when post has been read
     * @return self
     */
    public function markAsRead($readDate = null)
    {
        $this->accessible = true;
        $this->read = true;

        if($readDate !== null) {
            $this->readDate = (int) $readDate;
        }

        return $this;
    }

    /**
     * Set accessible date to the lesson
     *
     * @since 2.5.2
     *
     * @param int $date Timestamp
     * @return self
     */
    public function setAccessible($date = 0)
    {
        if($date > time()) {
            $this->accessibleOn = $date;
            $this->markAsScheduled();
        } elseif($date > 0) {
            $this->accessibleOn = $date;
            $this->markAsAccessible();
        } else {
            $this->markAsAccessible();
        }

        return $this;
    }

    /**
     * Mark post as scheduled/upcoming
     * @since 1.1.0
     * @return self
     */
    public function markAsScheduled()
    {
        $this->accessibleSoon = true;

        return $this;
    }

    /**
     * Determine if the post is accessible by consumer
     * @since 1.1.0
     * @return bool
     */
    public function isAccessible()
    {
        return $this->accessible;
    }

    /**
     * Determine if the post was read by consumer
     * @since 1.1.0
     * @return bool
     */
    public function isRead()
    {
        return $this->read;
    }

    /**
     * Determine if the post was scheduled for consumer
     * @since 1.1.0
     * @return bool
     */
    public function isScheduled()
    {
        return $this->accessibleSoon;
    }
}