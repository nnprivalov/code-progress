<?php
class LSCourseConsumer extends LS\Model
{
    /** @var int */
    protected $courseId;

    /** @var int */
    protected $consumerId;

    /** @var int */
    protected $startDate;

    /** @var int */
    protected $endDate;

    /** @var string[] */
    protected $primaryKeys = [
        'courseId',
        'consumerId'
    ];

    /** @var string[] */
    protected $dbCols = [
        'courseId',
        'consumerId',
        'startDate',
        'endDate'
    ];

    /**
     * Set default data for the class record
     * @since 1.2.0
     * @return self
     */
    public function setDefault()
    {
        $this->courseId =
        $this->consumerId =
        $this->startDate =
        $this->endDate = 0;

        return $this;
    }

    /**
     * Format the data
     *
     * @since 2.0.0
     *
     * @param array $data Data to be formatted
     * @return array Formatted data
     */
    protected function format($data)
    {
        return array_map('intval', $data);
    }

    /**
     * Check if the code is valid
     * @since 2.0.0
     * @return bool True if the code is valid
     */
    public function valid()
    {
        return $this->consumerId > 0 && $this->courseId > 0;
    }

    /**
     * Mark course as ended by consumer
     * @since 1.2.0
     * @return self
     */
    public function markAsEnded()
    {
        $this->endDate = time();

        return $this;
    }

    /**
     * Check if the course has been ended by the consumer
     * @since 2.1.0
     * @return bool True if course has been ended by the consumer
     */
    public function isEnded()
    {
        return $this->endDate > 0;
    }

    /**
     * Retrieve course Id
     * @since 1.2.0
     * @return int Course Id
     */
    public function getCourseId()
    {
        return $this->courseId;
    }

    /**
     * Retrieve consumer Id
     * @since 1.2.0
     * @return int Consumer Id
     */
    public function getConsumerId()
    {
        return $this->consumerId;
    }

    /**
     * Retrieve start date
     * @since 1.3.1
     * @return int Timestamp
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set start date
     *
     * @since 2.6.1
     *
     * @param int $startDate Start date (timestamp)
     * @return self
     */
    public function setStartDate($startDate)
    {
        $this->startDate = (int) $startDate;

        return $this;
    }

    /**
     * Retrieve end date
     * @since 1.3.1
     * @return int Timestamp
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * Retrieve number of read lessons in the course
     *
     * @since 2.2.0
     * @deprecated 05.2019 Legacy
     *
     * @return int Number of lessons
     */
    public function getNumberReadLessons()
    {
        _deprecated_function(__FUNCTION__, '2.8.6');

        return LSCourseHelper::getConsumerReadLessonsInCourse($this->getConsumerId(), $this->getCourseId());
    }

    /**
     * Increase number of read lessons in the course
     *
     * @since 2.2.0
     * @deprecated 05.2019 Legacy
     *
     * @return self
     */
    public function increaseNumberOfReadLessons()
    {
        _deprecated_function(__FUNCTION__, '2.8.6');

        return $this;
    }
}