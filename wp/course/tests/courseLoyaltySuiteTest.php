<?php
/**
 * Created by PhpStorm.
 * User: made
 * Date: 2018-07-18
 * Time: 09:34
 */

define('PHP_UNIT_TEST', true);
define('WP_ADMIN', true);

require_once __DIR__ . '/../../../../../wp-load.php';

use PHPUnit\Framework\TestCase;

class courseLoyaltySuiteTest extends TestCase
{
    /** @var courseLoyaltySuite */
    protected $module;

    public function setUp(): void
    {
        $this->module = LS()->getModule('course');

        global $wpdb;
        $wpdb->query("BEGIN");
    }

    public function tearDown(): void
    {
        global $wpdb;
        $wpdb->query("ROLLBACK");
    }

    /**
     * @param string $name
     * @param string $type
     * @param string $content
     * @return int
     */
    protected function createPost($name, $type, $content = '')
    {
         $result = wp_insert_post([
            'post_name'      => $name,
            'post_status'    => 'publish',
            'post_title'     => $name,
            'post_type'      => $type,
            'post_content'   => $content
        ]);

         return $result instanceof WP_Error ? 0 : $result;
    }

    /**
     * @param LSCourse[] $courses
     * @param LSCourse $course
     * @return bool
     */
    protected function hasCourse($courses, $course)
    {
        foreach($courses as $c) {
            if($c->getCourseId() === $course->getCourseId()) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param LSCourseLesson[] $lessons
     * @param LSCourseLesson $lesson
     * @return bool
     */
    protected function hasCoursePost($lessons, $lesson)
    {
        foreach($lessons as $l) {
            if($l->getCourseId() === $lesson->getCourseId() && $l->getPostId() === $lesson->getPostId()) {
                return true;
            }
        }

        return false;
    }

    public function testCourseClass()
    {
        // COURSE
        $coursePageId = $this->createPost('course1', 'course');

        $this->assertNotEmpty($coursePageId);

        $course = new LSCourse([
            'coursePageId'      => $coursePageId,
            'courseObjective'   => 'CO',
            'courseTargetGroup' => 'TG',
            'trainerName'       => 'TN',
            'trainerOccupation' => 'TO',
            'trainerImage'      => ['id' => 1],
            'startDate'         => strtotime('TODAY'),
            'endDate'           => strtotime('TODAY +30 DAYS'),
            'sendLog'           => true,
            'logEmail'          => 'email@example.com',
            'logSubject'        => 'LS',
            'logBody'           => 'LB',
            'failureMessage'    => 'S**t',
            'defaultInterval'   => 3,
            'minInterval'       => 2,
            'maxInterval'       => 4
        ]);

        $this->assertTrue($course->valid());
        $this->assertSame($coursePageId, $course->getCoursePageId());
        $this->assertSame('CO', $course->getCourseObjective());
        $this->assertSame('TG', $course->getCourseTargetGroup());
        $this->assertSame('TN', $course->getTrainerName());
        $this->assertSame('TO', $course->getTrainerOccupation());
        $this->assertSame(['id' => 1], $course->getTrainerImage());
        $this->assertSame(strtotime('TODAY'), $course->getStartDate());
        $this->assertSame(strtotime('TODAY +30 DAYS'), $course->getEndDate());
        $this->assertTrue($course->getSendLogStatus());
        $this->assertSame('email@example.com', $course->getLogEmail());
        $this->assertSame('LS', $course->getLogSubject());
        $this->assertSame('LB', $course->getLogBody());
        $this->assertSame('S**t', $course->getFailureMessage());
        $this->assertSame(3, $course->getDefaultInterval());
        $this->assertSame(2, $course->getMinInterval());
        $this->assertSame(4, $course->getMaxInterval());
        $this->assertInstanceOf(WP_Post::class, $course->getPost());
        $this->assertNotEmpty($course->getCourseName());

        $course->setMinInterval(1);
        $course->setMaxInterval(3);
        $course->setDefaultInterval(2);

        $this->assertSame(2, $course->getDefaultInterval());
        $this->assertSame(1, $course->getMinInterval());
        $this->assertSame(3, $course->getMaxInterval());

        $courseId = LSCourseHelper::createCourse($course);

        $this->assertNotEmpty($courseId);
        $this->assertSame($courseId, $course->getCourseId());
        $this->assertTrue($course->isActual());

        $success = LSCourseHelper::updateCourse(
            $course->setMaxInterval(5)
        );

        $this->assertTrue($success);

        $course = LSCourseHelper::getCourse($courseId);
        $cce = LSCourseHelper::getCoursesToSendNotificationEmail();

        $this->assertInstanceOf(LSCourse::class, $course);
        $this->assertTrue($this->hasCourse(LSCourseHelper::getCourses(), $course));
        $this->assertTrue($this->hasCourse(LSCourseHelper::getCoursesStartedToday(), $course));
        $this->assertFalse($this->hasCourse(LSCourseHelper::getCoursesEndedToday(), $course));
        $this->assertContains($courseId, array_keys(LSCourseHelper::getCoursesList()));
        $this->assertContains($courseId, array_keys(LSCourseHelper::getCoursesList(true)));
        $this->assertNotEmpty($cce);

        // LESSONS

        $courseLessonPageId1 = $this->createPost('course-lesson-1', 'course-lesson');
        $courseLessonPageId2 = $this->createPost('course-lesson-2', 'course-lesson');
        $courseLessonPageId3 = $this->createPost('course-lesson-3', 'course-lesson');

        $this->assertNotEmpty($courseLessonPageId1);
        $this->assertNotEmpty($courseLessonPageId2);
        $this->assertNotEmpty($courseLessonPageId3);

        $success = LSCourseHelper::updateLessons($courseId, [$courseLessonPageId1, $courseLessonPageId2]);

        $this->assertTrue($success);

        $lesson = new LSCourseLesson([
            'postId'   => $courseLessonPageId3,
            'courseId' => $courseId,
            'order'    => 3
        ]);

        $this->assertSame($courseLessonPageId3, $lesson->getPostId());
        $this->assertSame($courseId, $lesson->getCourseId());
        $this->assertSame(3, $lesson->getOrder());
        $this->assertTrue($lesson->valid());

        $success = LSCourseHelper::createLesson($lesson);

        $this->assertTrue($success);

        $pct = LSCourseHelper::getAllLessons();
        $lesson1 = LSCourseHelper::getLessonByLessonPostId($courseLessonPageId1);

        $this->assertInstanceOf(LSCourseLesson::class, $lesson1);
        $this->assertTrue($this->hasCoursePost($pct, $lesson1));

        $ul = LSCourseHelper::updateLesson($lesson1);
        $pcs = LSCourseHelper::getLessons($courseId);
        $lesson2 = LSCourseHelper::getLessonByLessonPostId($courseLessonPageId2);
        $lesson3 = LSCourseHelper::getLastLessonByCourseId($courseId);
        $pcsIds = LSCourseHelper::getLessonIds($courseId);
        $pcsWId = LSCourseHelper::getAllLessonPostIds();
        $cId = LSCourseHelper::getCourseIdByLessonPostId($courseLessonPageId1);
        $c1 = LSCourseHelper::getCourseByCoursePageId($coursePageId);
        $c2 = LSCourseHelper::getCourseByLessonPostId($courseLessonPageId3);
        $cLId = LSCourseHelper::getCourseLastLessonPostId($courseId);
        $icl = LSCourseHelper::isCourseLesson($lesson1->getPostId());
        $cIdPId = LSCourseHelper::getCourseIdByCoursePageId($coursePageId);

        $this->assertTrue($ul);
        $this->assertNotEmpty($pcsWId);
        $this->assertSame($lesson1->getPostId(), $courseLessonPageId1);
        $this->assertSame($lesson2->getPostId(), $courseLessonPageId2);
        $this->assertTrue($this->hasCoursePost($pcs, $lesson2));
        $this->assertNotEmpty($pcsIds);
        $this->assertSame($cId, $courseId);
        $this->assertNotEmpty($c1);
        $this->assertNotEmpty($c2);
        $this->assertSame($cLId, $lesson3->getPostId());
        $this->assertTrue($icl);
        $this->assertSame($cIdPId, $courseId);

        // CONSUMERS
        $consumer1 = new \LS\Consumer(['email' => 'fake-consumer-t@example.com', 'active' => 1, 'verified' => 1, 'allowEmails' => 1]);
        $consumer2 = new \LS\Consumer(['email' => 'fake-consumer-t2@example.com', 'active' => 1, 'verified' => 1, 'allowEmails' => 1]);

        \LS\ConsumersHelper::createConsumer($consumer1);
        \LS\ConsumersHelper::createConsumer($consumer2);

        $consumerId = $consumer1->getConsumerId();
        $consumerId2 = $consumer2->getConsumerId();

        $this->assertTrue($consumerId > 0);
        $this->assertTrue($consumerId2 > 0);

        $cc = new LSCourseConsumer([
            'courseId'    => $courseId,
            'consumerId'  => $consumerId,
            'startDate'   => strtotime('YESTERDAY'),
            'endDate'     => strtotime('TODAY')
        ]);

        $this->assertSame($courseId, $cc->getCourseId());
        $this->assertSame($consumerId, $cc->getConsumerId());
        $this->assertSame(strtotime('YESTERDAY'), $cc->getStartDate());
        $this->assertSame(strtotime('TODAY'), $cc->getEndDate());
        $this->assertTrue($cc->valid());

        $cc->set('endDate', 0);

        $this->assertSame(0, $cc->getEndDate());

        $cc->markAsEnded();
        $cc->setStartDate(strtotime('TODAY -10 DAYS'));

        $this->assertTrue($cc->isEnded());
        $this->assertSame(strtotime('TODAY -10 DAYS'), $cc->getStartDate());

        $success = LSCourseHelper::createConsumerConnection($cc);

        $this->assertTrue($success);

        $cc = LSCourseHelper::getConsumerConnection($courseId, $consumerId);

        $this->assertNotEmpty($cc);

        $success = LSCourseHelper::updateConsumerConnection($cc->setStartDate(strtotime('TODAY')));
        $success2 = !empty(LSCourseHelper::getConsumerConnections($consumerId, false));
        $success3 = !empty(LSCourseHelper::getConsumerConnections($consumerId, true));
        $success4 = LSCourseHelper::isConsumerStartedCourse($consumerId, $courseId);
        $success5 = LSCourseHelper::isConsumerEndedCourse($consumerId, $courseId);
        $cns = LSCourseHelper::getConsumersConnectionsByCourseId($courseId);
        $cns2 = LSCourseHelper::getCoursesStartedByConsumer($consumerId);
        $cns3 = LSCourseHelper::getCoursesEndedByConsumer($consumerId);

        $this->assertTrue($success);
        $this->assertTrue($success2);
        $this->assertTrue($success3);
        $this->assertTrue($success4);
        $this->assertTrue($success5);
        $this->assertNotEmpty($cns);
        $this->assertNotEmpty($cns2);
        $this->assertNotEmpty($cns3);

        LSCourseHelper::createConsumerConnection(new LSCourseConsumer([
            'courseId'   => $courseId,
            'consumerId' => $consumerId2
        ]));

        $cns = LSCourseHelper::getConsumerConnections($consumerId2);

        $this->assertNotEmpty($cns);

        $cc->set('endDate', 0);
        LSCourseHelper::updateConsumerConnection($cc);

        // ACTIVITIES

        $t = time();
        $activity = new LSCourseActivity([
            'courseId'        => $courseId,
            'consumerId'      => $consumerId,
            'postId'          => $courseLessonPageId1,
            'interval'        => 3,
            'actionDate'      => strtotime('TODAY'),
            'actionTimestamp' => $t
        ]);

        $this->assertTrue($activity->valid());
        $this->assertSame($courseId, $activity->getCourseId());
        $this->assertSame($consumerId, $activity->getConsumerId());
        $this->assertSame($courseLessonPageId1, $activity->getPostId());
        $this->assertSame(3, $activity->getInterval());
        $this->assertSame(strtotime('TODAY'), $activity->getActionDate());
        $this->assertSame($t, $activity->getActionTimestamp());

        $activity->setActionDate(strtotime('YESTERDAY'));
        $activity->setActionTimestamp($t - DAY_IN_SECONDS);

        $this->assertSame(strtotime('YESTERDAY'), $activity->getActionDate());
        $this->assertSame($t - DAY_IN_SECONDS, $activity->getActionTimestamp());

        $activityId = LSCourseHelper::createActivity($activity);
        $activity->setActivityId($activityId);

        $this->assertNotEmpty($activityId);
        $this->assertSame($activityId, $activity->getActivityId());

        $activity->set('interval', 1);
        $success = LSCourseHelper::updateActivity($activity);

        $activity2Id = LSCourseHelper::createActivity(new LSCourseActivity([
            'courseId'        => $courseId,
            'consumerId'      => $consumerId,
            'postId'          => $courseLessonPageId2,
            'interval'        => 1,
            'actionDate'      => strtotime('YESTERDAY'),
            'actionTimestamp' => $t - DAY_IN_SECONDS
        ]));

        $this->assertTrue($success);
        $this->assertTrue($activity2Id > 0);

        $ca = LSCourseHelper::getConsumerActivities($consumerId);
        $ca2 = LSCourseHelper::getConsumerActivitiesInCourse($consumerId, $courseId);
        $ca3 = LSCourseHelper::getActivitiesByPostId($courseLessonPageId1);
        $ca4 = LSCourseHelper::getCourseActivitiesByPostId($courseId, $courseLessonPageId1);
        $ca5 = LSCourseHelper::getCourseActivitiesByPostId($courseId, $courseLessonPageId2);
        $ca6 = LSCourseHelper::getConsumerReadLessonsInCourse($consumerId, $courseId);

        $this->assertCount(2, $ca);
        $this->assertSame($ca, $ca2);
        $this->assertCount(1, $ca3);
        $this->assertSame($ca3, $ca4);
        $this->assertSame(2, $ca6);
        
        $act = LSCourseHelper::getActivitiesForReminder(0, $courseId);

        $this->assertNotEmpty($act);
        $this->assertSame($act, $ca5);

        $progress = LSCourseHelper::calculateConsumerProgress($courseId, $consumerId);

        $this->assertSame(67.0, $progress); // 2 of 3 lessons

        // REMOVING/CLEANING

        $activity->set('postId', 123456789);
        LSCourseHelper::updateActivity($activity);
        LSCourseHelper::cleanUpActivities();
        $ca = LSCourseHelper::getConsumerActivities($consumerId);

        $this->assertCount(1, $ca);

        $success = LSCourseHelper::deleteActivity($ca5[0]);

        $this->assertTrue($success);

        $success = LSCourseHelper::deleteConsumerToCourseConnection($cc);

        $this->assertTrue($success);

        LSCourseHelper::deleteConsumerFromAllCourses($consumerId2);
        $cns = LSCourseHelper::getConsumerConnections($consumerId2);

        $this->assertEmpty($cns);

        LSCourseHelper::disconnectCourseFromAllConsumers($courseId);
        $cns = LSCourseHelper::getConsumerConnections($courseId);

        $this->assertEmpty($cns);

        $success = LSCourseHelper::deleteLesson($lesson3);
        $success2 = LSCourseHelper::deleteLessonByLessonPostId($lesson2->getPostId());
        $success3 = LSCourseHelper::deleteAllLessonsByCourseId($courseId);

        $this->assertTrue($success);
        $this->assertTrue($success2);
        $this->assertTrue($success3);

        LSCourseHelper::updateCourse(
            $course
                ->setStartDate(strtotime('YESTERDAY'))
                ->setEndDate(strtotime('YESTERDAY'))
        );

        $this->assertFalse($course->isActual());
        $this->assertContains($courseId, array_keys(LSCourseHelper::getCoursesList()));
        $this->assertNotContains($courseId, array_keys(LSCourseHelper::getCoursesList(true)));

        $success = LSCourseHelper::deleteCourse($courseId);

        $this->assertTrue($success);
    }
}