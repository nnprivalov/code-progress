jQuery(function($) {

    $('.course-accept').each(function() {

        var ca = $(this),
            ci = ca.find('.course-interval');

        var updValues = function(e, ui) {
            
            ci.find('.course-interval-value').html(ui.value);

            var link = ca.find('.course-link > a');

            link.attr('href', link.attr('href').replace(/(interval=)[^\&]+/, '$1' + ui.value));
        };

        ci.find('.course-interval-slider').slider({
            orientation: "horizontal",
            min: parseInt(ci.attr('data-min'), 10),
            max: parseInt(ci.attr('data-max'), 10),
            value: parseInt(ci.attr('data-value'), 10),
            step: 1,
            range: "min",
            slide: updValues,
            change: updValues
        });

    });
});