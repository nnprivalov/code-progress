jQuery(function($) {

    addDeleteConfirmation('course-delete');
    
    if($('#courseForDash').length) {

        $('#courseForDash').change(function() {

            var dash = $(this),
                courseId = dash.val();

            $('#courseDashData').hide().after('<div class="loading ls-course-spinner"></div>');
            dash.attr('disabled', 'disabled');

            $.post(ajaxurl, {
                    action: 'courseDashboardFigures',
                    courseId: courseId
                }
            ).done(function(response) {

                if(response) {
                    $('#courseDbOverallParticipants').html(response.overallParticipants);
                    $('#courseDbActiveParticipants').html(response.activeParticipants);
                    $('#courseDbFinishedParticipants').html(response.finishedParticipants);
                    $('#courseDbParticipantsWeek').html(response.weeklyNewParticipants);
                    $('#courseDbFinishedParticipantsWeek').html(response.weeklyFinishedParticipants);
                    $('#courseDbLastWeekReads').html(response.readsLastWeek);
                    $('#courseDbParticipantsTW').html(response.twNewParticipants);
                    $('#courseDbFinishedParticipantsTW').html(response.twFinishedParticipants);
                    $('#courseDbThisWeekReads').html(response.readsThisWeek);
                }

                $('.ls-dash .loading').remove();
                $('#courseDashData').show();
                dash.removeAttr('disabled');
            });

        });
    }

    if($('.course-posts').length) {

        var recalculateOrder = function() {
            var i = 1;
            cp.find('li').each(function() {
                $(this).find('.order').html(i++);
            });
        };

        var cp = $('.course-posts');

        cp.sortable({
            items: 'li',
            helper: 'clone',
            update: recalculateOrder
        });

        var posts = [];
        cp.find('li').each(function() {
            posts.push($(this).data('post_id'));
        });

        $('.course-posts .postPicker').on('pp_select', function(e, postId) {
            
            if(postId > 0 && !has(posts, postId)) {

                posts.push(postId);

                var label = cp.find('.postPicker-field').val();

                cp.find('ul').append(
                    '<li class="clearfix">' +
                        '<input type="hidden" name="posts[]" value="' + postId + '" />' +
                        '<div class="title">' + label + '</div>' +
                        '<div class="remove"><a href="#"><span class="dashicons dashicons-dismiss"></span></a></div>' +
                        '<div class="order"></div>' +
                    '</li>'
                );

                recalculateOrder();
            }

            lsPostPickerClean(cp.find('.postPicker'));
        });

        cp.on('click', '.remove a', function() {

            $(this).closest('li').remove();

            posts.splice(posts.indexOf($(this).find('.value').val()), 1);

            recalculateOrder();

            return false;
        });
    }

    // Activity block
    initActivitiesChart(
        '.course-activity',
        '#courseActivitiesMultipleSelect',
        '#statsSD',
        '#statsED',
        'courseActivities',
        {
            courseId: '#courseForActivity'
        }
    );

});