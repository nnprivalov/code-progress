<?php
class LSCourseActivity extends LS\Model
{
    /** @var int */
    protected $activityId;

    /** @var int */
    protected $courseId;

    /** @var int */
    protected $consumerId;

    /** @var int */
    protected $postId;

    /** @var int */
    protected $interval;

    /** @var int */
    protected $actionDate;

    /** @var int */
    protected $actionTimestamp;

    /** @var string[] */
    protected $primaryKeys = [
        'activityId'
    ];

    /** @var string[] */
    protected $dbCols = [
        'activityId',
        'courseId',
        'consumerId',
        'postId',
        'interval',
        'actionDate',
        'actionTimestamp'
    ];

    /**
     * Set default data for the activity
     * @since 1.0.0
     * @return self
     */
    public function setDefault()
    {
        $this->activityId =
        $this->courseId =
        $this->consumerId =
        $this->postId =
        $this->interval =
        $this->actionDate =
        $this->actionTimestamp = 0;

        return $this;
    }

    /**
     * Format the data
     *
     * @since 2.0.0
     *
     * @param array $data Data to be formatted
     * @return array Formatted data
     */
    protected function format($data)
    {
        foreach($data as $key => $value) {
            if(in_array($key, ['activityId', 'courseId', 'consumerId', 'postId', 'interval'])) {
                $data[$key] = (int) $value;
            } elseif(in_array($key, ['actionDate', 'actionTimestamp'])) {
                $data[$key] = max(0, !empty($value) && !is_numeric($value) ? strtotime($value) : (int) $value);
            }
        }

        return $data;
    }

    /**
     * Check if the course activity is valid
     * @since 2.0.0
     * @return bool True if the course activity is valid
     */
    public function valid()
    {
        return $this->courseId > 0 && $this->consumerId > 0 && $this->postId > 0;
    }

    /**
     * Retrieve course activity Id
     * @since 1.3.1
     * @return int Course activity Id
     */
    public function getActivityId()
    {
        return $this->activityId;
    }

    /**
     * Set activity Id
     *
     * @since 2.6.1
     *
     * @param int $activityId Activity Id
     * @return self
     */
    public function setActivityId($activityId)
    {
        $this->activityId = (int) $activityId;

        return $this;
    }

    /**
     * Retrieve course Id
     * @since 1.3.1
     * @return int Course Id
     */
    public function getCourseId()
    {
        return $this->courseId;
    }

    /**
     * Retrieve consumer Id
     * @since 1.3.1
     * @return int Consumer Id
     */
    public function getConsumerId()
    {
        return $this->consumerId;
    }

    /**
     * Retrieve post Id
     * @since 1.3.1
     * @return int Post Id
     */
    public function getPostId()
    {
        return $this->postId;
    }

    /**
     * Retrieve activity interval
     * @since 1.3.1
     * @return int Interval in days
     */
    public function getInterval()
    {
        return $this->interval;
    }

    /**
     * Retrieve activity create date
     * @since 1.3.1
     * @return int Timestamp
     */
    public function getActionDate()
    {
        return $this->actionDate;
    }

    /**
     * Retrieve activity create timestamp
     * @since 2.6.10
     * @return int Timestamp
     */
    public function getActionTimestamp()
    {
        return $this->actionTimestamp;
    }

    /**
     * Set action date
     *
     * @since 2.6.1
     *
     * @param int $actionDate Action date (timestamp)
     * @return self
     */
    public function setActionDate($actionDate)
    {
        $this->actionDate = (int) $actionDate;

        return $this;
    }

    /**
     * Set action timestamp
     *
     * @since 2.6.10
     *
     * @param int $actionTimestamp Action timestamp
     * @return self
     */
    public function setActionTimestamp($actionTimestamp)
    {
        $this->actionTimestamp = (int) $actionTimestamp;

        return $this;
    }

    /**
     * Retrieve record of the object data which can be stored in the database table
     *
     * @since 2.0.0
     *
     * @param bool $withPrimaryKeys True to include primary keys data
     * @return array An array of columns and their values
     */
    public function getRecord($withPrimaryKeys = false)
    {
        $data = [];

        foreach($this->getDbColumns($withPrimaryKeys) as $col) {
            if($col == 'actionDate') {
                $data[$col] = empty($this->actionDate) ? '0000-00-00' : date('Y-m-d', $this->actionDate);
            } else {
                $data[$col] = $this->{$col};
            }
        }

        return $data;
    }
}