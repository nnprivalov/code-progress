<?php
class LSCourseLesson extends LS\Model
{
    /** @var int */
    protected $courseId;

    /** @var int */
    protected $postId;

    /** @var int */
    protected $order;

    /** @var string[] */
    protected $primaryKeys = [
        'courseId',
        'postId'
    ];

    /** @var string[] */
    protected $dbCols = [
        'courseId',
        'postId',
        'order'
    ];

    /**
     * Set default data for the post connection
     * @since 1.0.0
     * @return LSCourseLesson $this Class instance
     */
    public function setDefault()
    {
        $this->courseId =
        $this->postId =
        $this->order = 0;

        return $this;
    }

    /**
     * Format the data
     *
     * @since 2.0.0
     *
     * @param array $data Data to be formatted
     * @return array Formatted data
     */
    protected function format($data)
    {
        return array_map('intval', $data);
    }

    /**
     * Check if the code is valid
     * @since 2.0.0
     * @return bool True if the code is valid
     */
    public function valid()
    {
        return $this->courseId > 0 && $this->postId > 0;
    }

    /**
     * Retrieve post Id
     * @since 1.2.0
     * @return int Post Id
     */
    public function getPostId()
    {
        return $this->postId;
    }

    /**
     * Retrieve course Id
     * @since 1.2.0
     * @return int Course Id
     */
    public function getCourseId()
    {
        return $this->courseId;
    }

    /**
     * Retrieve course order
     * @since 1.3.1
     * @return int Order number
     */
    public function getOrder()
    {
        return $this->order;
    }
}