<?php
class LSCourse extends LS\Model
{
    /** Max consumer course interval between posts */
    const DEFAULT_MAX_INTERVAL = 7;

    /** Default course interval between posts */
    const DEFAULT_INTERVAL = 3;

    /** @var int */
    protected $courseId;

    /** @var int */
    protected $coursePageId;

    /** @var string */
    protected $courseObjective;

    /** @var string */
    protected $courseTargetGroup;

    /** @var string */
    protected $trainerName;

    /** @var string */
    protected $trainerOccupation;

    /** @var string */
    protected $trainerImage;

    /** @var int */
    protected $startDate;

    /** @var int */
    protected $endDate;

    /** @var bool */
    protected $sendLog;

    /** @var string */
    protected $logEmail;

    /** @var string */
    protected $logSubject;

    /** @var string */
    protected $logBody;

    /** @var string */
    protected $failureMessage;

    /** @var int */
    protected $defaultInterval;

    /** @var int */
    protected $minInterval;

    /** @var int */
    protected $maxInterval;

    /** @var string[] */
    protected $primaryKeys = [
        'courseId'
    ];

    /** @var string[] */
    protected $dbCols = [
        'courseId',
        'coursePageId',
        'courseObjective',
        'courseTargetGroup',
        'trainerName',
        'trainerOccupation',
        'trainerImage',
        'startDate',
        'endDate',
        'sendLog',
        'logEmail',
        'logSubject',
        'logBody',
        'failureMessage',
        'defaultInterval',
        'minInterval',
        'maxInterval'
    ];

    /** @var null|WP_Post */
    protected $post;

    /** @var string */
    protected $courseName;

    /**
     * Set default data for the course
     * @since 1.0.0
     * @return self
     */
    public function setDefault()
    {
        $this->courseId =
        $this->startDate =
        $this->endDate =
        $this->coursePageId = 0;
        $this->courseName =
        $this->courseObjective =
        $this->courseTargetGroup =
        $this->trainerName =
        $this->trainerOccupation =
        $this->trainerImage = '';
        $this->sendLog = false;
        $this->post = null;
        $this->logEmail = '';
        $this->logSubject = 'Weekly statistics for course {courseName}';
        $this->logBody = "Course \"<strong>{courseName}</strong>\".<br />\n" .
                         "<br />\n==========================<br /><br />\n" .
                         "Weekly statistics ({weekRange})<br />\n" .
                         "<br />\n==========================<br /><br />\n" .
                         "Number of new participants in the course: <strong>{weeklyNewParticipants}</strong><br />\n" .
                         "Number of participants who finished course: <strong>{weeklyFinishedParticipants}</strong><br />\n" .
                         "<br />Participation in the course:<br />\n" .
                         "{weeklyParticipation}<br />\n" .
                         "<br />\n==========================<br /><br />\n" .
                         "Overall statistics<br />\n" .
                         "<br />\n==========================<br /><br />\n" .
                         "Total participants in the course: <strong>{overallParticipants}</strong><br />\n" .
                         "Active participants in the course: <strong>{activeParticipants}</strong><br />\n" .
                         "Total participants who finished the course: <strong>{finishedParticipants}</strong><br />\n" .
                         "<br />Details:<br />\n" .
                         "{overallParticipation}";
        $this->failureMessage = __('Sorry, you have no access to this post right now.', 'ls');
        $this->defaultInterval = self::DEFAULT_INTERVAL;
        $this->minInterval = 1;
        $this->maxInterval = self::DEFAULT_MAX_INTERVAL;

        return $this;
    }

    /**
     * Format the data
     *
     * @since 2.0.0
     *
     * @param array $data Data to be formatted
     * @return array Formatted data
     */
    protected function format($data)
    {
        foreach($data as $key => $value) {
            if(in_array($key, ['courseId', 'coursePageId', 'defaultInterval', 'minInterval', 'maxInterval'])) {
                $data[$key] = (int) $value;
            } elseif(in_array($key, ['startDate', 'endDate'])) {
                $data[$key] = max(0, !empty($value) && !is_numeric($value) ? strtotime($value) : (int) $value);
            } elseif($key === 'sendLog') {
                $data[$key] = (bool) $value;
            } elseif($key === 'trainerImage') {
                $data[$key] = is_array($value) ? $value : LS\Library::maybeUnserialize($value);
            }
        }

        return $data;
    }

    /**
     * Retrieve course ID
     * @since 1.0.0
     * @return int Course ID
     */
    public function getCourseId()
    {
        return $this->courseId;
    }

    /**
     * Set course Id
     *
     * @since 2.6.1
     *
     * @param int $courseId Course Id
     * @return self
     */
    public function setCourseId($courseId)
    {
        $this->courseId = (int) $courseId;

        return $this;
    }

    /**
     * Retrieve course page Id
     * @since 2.0.0
     * @return string Course page Id
     */
    public function getCoursePageId()
    {
        return $this->coursePageId;
    }

    /**
     * Retrieve link to the course page
     *
     * @since 2.0.0
     *
     * @param false|string $default Default value
     * @return false|string Course link or false if the link not available
     */
    public function getCourseLink($default = '')
    {
        return $this->coursePageId > 0 ? get_permalink($this->coursePageId) : $default;
    }

    /**
     * Retrieve course objective
     * @since 2.0.0
     * @return string Course objective
     */
    public function getCourseObjective()
    {
        return $this->courseObjective;
    }

    /**
     * Retrieve course target group
     * @since 2.5.0
     * @return string Course target group
     */
    public function getCourseTargetGroup()
    {
        return $this->courseTargetGroup;
    }

    /**
     * Retrieve trainer name
     * @since 2.0.0
     * @return string Trainer name
     */
    public function getTrainerName()
    {
        return $this->trainerName;
    }

    /**
     * Retrieve trainer occupation
     * @since 2.0.0
     * @return string Trainer occupation
     */
    public function getTrainerOccupation()
    {
        return $this->trainerOccupation;
    }

    /**
     * Retrieve trainer image
     * @since 2.0.0
     * @return string Trainer image
     */
    public function getTrainerImage()
    {
        return $this->trainerImage;
    }

    /**
     * Retrieve course start date
     * @since 1.3.1
     * @return int Timestamp
     */
    public function getStartDate()
    {
        return $this->startDate; // Can be empty!
    }

    /**
     * Set course start date
     *
     * @since 2.6.1
     *
     * @param string $startDate Course start date
     * @return self
     */
    public function setStartDate($startDate)
    {
        $this->startDate = (int) $startDate;

        return $this;
    }

    /**
     * Retrieve course end date
     * @since 1.3.1
     * @return int Timestamp
     */
    public function getEndDate()
    {
        return $this->endDate; // Can be empty!
    }

    /**
     * Set course end date
     *
     * @since 2.6.1
     *
     * @param string $endDate Course end date
     * @return self
     */
    public function setEndDate($endDate)
    {
        $this->endDate = (int) $endDate;

        return $this;
    }

    /**
     * Retrieve course send log status (enabled/disabled)
     * @since 1.3.1
     * @return bool True if send log is enabled
     */
    public function getSendLogStatus()
    {
        return $this->sendLog;
    }

    /**
     * Retrieve course log email
     * @since 1.3.1
     * @return string Email address
     */
    public function getLogEmail()
    {
        return $this->logEmail;
    }

    /**
     * Retrieve course log subject
     * @since 1.3.1
     * @return string Text
     */
    public function getLogSubject()
    {
        return $this->logSubject;
    }

    /**
     * Retrieve course log body
     * @since 1.3.1
     * @return string Text or HTML
     */
    public function getLogBody()
    {
        return $this->logBody;
    }

    /**
     * Retrieve course failure message
     * @since 1.3.1
     * @return string Text
     */
    public function getFailureMessage()
    {
        return $this->failureMessage;
    }

    /**
     * Retrieve course default interval
     * @since 1.3.1
     * @return int Value in days
     */
    public function getDefaultInterval()
    {
        return $this->defaultInterval;
    }

    /**
     * Set course default interval
     *
     * @since 2.6.1
     *
     * @param int $defaultInterval Default interval
     * @return self
     */
    public function setDefaultInterval($defaultInterval)
    {
        $this->defaultInterval = (int) $defaultInterval;

        return $this;
    }

    /**
     * Retrieve course min interval
     * @since 1.3.1
     * @return int Value in days
     */
    public function getMinInterval()
    {
        return $this->minInterval;
    }

    /**
     * Set course min interval
     *
     * @since 2.6.1
     *
     * @param int $minInterval Minimum interval
     * @return self
     */
    public function setMinInterval($minInterval)
    {
        $this->minInterval = (int) $minInterval;

        return $this;
    }

    /**
     * Retrieve course max interval
     * @since 1.3.1
     * @return int Value in days
     */
    public function getMaxInterval()
    {
        return $this->maxInterval;
    }

    /**
     * Set course max interval
     *
     * @since 2.6.1
     *
     * @param int $maxInterval Maximum interval
     * @return self
     */
    public function setMaxInterval($maxInterval)
    {
        $this->maxInterval = (int) $maxInterval;

        return $this;
    }

    /**
     * Retrieve course post
     * @since 2.1.0
     * @return null|WP_Post
     */
    public function getPost()
    {
        if($this->post === null && $this->getCoursePageId() > 0) {
            $this->post = get_post($this->getCoursePageId());
            $this->post = $this->post instanceof WP_Post ? $this->post : null;
        }

        return $this->post;
    }

    /**
     * Retrieve course name
     * @since 1.3.1
     * @return string Course name
     */
    public function getCourseName()
    {
        if(empty($this->courseName)) {
            $post = $this->getPost();
            $this->courseName = $post ? $post->post_title : '';
        }

        return $this->courseName;
    }

    /**
     * Check if the course is valid
     * @since 2.0.0
     * @return bool True if the course is valid
     */
    public function valid()
    {
        return $this->getCoursePageId() > 0;
    }

    /**
     * Retrieve record of the object data which can be stored in the database table
     *
     * @since 2.0.0
     *
     * @param bool $withPrimaryKeys True to include primary keys data
     * @return array An array of columns and their values
     */
    public function getRecord($withPrimaryKeys = false)
    {
        $data = [];

        foreach($this->getDbColumns($withPrimaryKeys) as $col) {
            if(in_array($col, ['startDate', 'endDate'])) {
                $data[$col] = empty($this->{$col}) ? '0000-00-00' : date('Y-m-d', $this->{$col});
            } elseif($col == 'sendLog') {
                $data[$col] = (int) $this->{$col};
            } elseif($col == 'trainerImage') {
                $data[$col] = maybe_serialize($this->{$col});
            } else {
                $data[$col] = $this->{$col};
            }
        }

        return $data;
    }

    /**
     * Check if course is actual (it works)
     * @since 1.2.0
     * @return bool True if course is actual
     */
    public function isActual()
    {
        $post = $this->getPost();

        return
            $this->courseId > 0
            && $post
            && $post->post_status == 'publish'
            && ($this->startDate == 0 || time() >= $this->startDate)
            && ($this->endDate == 0 || $this->endDate + DAY_IN_SECONDS > time());
    }
}