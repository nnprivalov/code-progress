<?php
class LSCoursesTable extends LS\WP_List_Table
{
    protected $_column_headers;

    public $referer;

    public function __construct()
    {
        parent::__construct([
            'singular' => 'wp_ls_course',
            'plural'   => 'wp_ls_courses',
            'hash'     => '#ls-courses'
        ]);
    }

    /**
     * @return array
     */
    public function get_columns()
    {
        $cols = [
            'cb'         => '<input type="checkbox" />',
            'courseId'   => 'ID',
            'courseName' => __('Course name', 'ls'),
            'active'     => __('Active', 'ls'),
            'startDate'  => __('Start date', 'ls'),
            'endDate'    => __('End date', 'ls')
        ];

        if(current_user_can('ls_course_manage')) {
            $cols['details'] = '';
        }

        return $cols;
    }

    /**
     * @return array
     */
    public function get_sortable_columns()
    {
        return [
            'courseId'   => ['courseId', false],
            'courseName' => ['courseName', true],
            'active'     => ['active', true],
            'startDate'  => ['startDate', false],
            'endDate'    => ['endDate', false]
        ];
    }

    /**
     * @return array
     */
    public function get_bulk_actions()
    {
        $actions = [];

        if(current_user_can('ls_course_manage')) {
            $actions['course-delete'] = __('Delete', 'ls');
        }

        return $actions;
    }

    /**
     * @param object $item
     * @return string
     */
    public function column_cb($item)
    {
        return sprintf('<input type="checkbox" name="courseId[]" value="%s" />', $item->getCourseId());
    }

    public function prepare_items()
    {
        $options = $this->get_table_options();
        $items = LSCourseHelper::getCourses($options['orderBy'], $options['order']);

        $this->set_pagination_args([
            "total_items" => count($items),
            "total_pages" => 1,
            "per_page"    => count($items)
        ]);

        $this->items = $items;
        $this->_column_headers = [$this->get_columns(), [], $this->get_sortable_columns()];
    }

    /**
     * @param LSCourse $item
     * @param string $columnName
     * @return string
     */
    public function column_default($item, $columnName)
    {
        switch($columnName) {
            case 'courseId' :
                return $item->getCourseId();
            case 'courseName' :
                return $item->getCourseName();
            case 'startDate' :
            case 'endDate' :
                return $item->get($columnName) == 0 ? '—' : date('d.m.Y', $item->get($columnName));
            case 'active' :
                return $item->isActual() ? '<span class="ls-sign-plus">+</span>' : ' ';
            case 'details' :
                return '<a href="' . get_edit_post_link($item->getCoursePageId()) . '" class="button">' . __('Details', 'ls') . '</a>';
            default:
                return ' ';
        }
    }
}