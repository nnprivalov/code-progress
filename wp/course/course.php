<?php
/**
 * @module courseLoyaltySuite
 * @title Course
 * @description Course module
 * @version 2.8.9
 * @priority 10
 * @depends consumersLoyaltySuite 3.4.1
 */
class courseLoyaltySuite extends LS\Module
{
    /**
     * @since 2.4.1
     * @return string
     */
    public function getVersion()
    {
        return '2.8.9';
    }

    /**
     * @since 2.4.1
     * @return string
     */
    public function getCapability()
    {
        return 'ls_course';
    }

    /**
     * @since 2.4.1
     * @return array
     */
    public function setDepends()
    {
        return [
            'consumersLoyaltySuite' => '3.4.1'
        ];
    }

    /**
     * @since 1.3.3
     * @return string
     */
    public function getTitle()
    {
        return __('Courses', 'ls');
    }

    /**
     * @since 1.3.3
     * @return string
     */
    public function getDescription()
    {
        return __('Courses module', 'ls');
    }

    /**
     * @since 2.4.0
     * @return array
     */
    public function getDefaultOptions()
    {
        return [
            'acp'                  => true,
            'courseTypeSlug'       => 'course',
            'courseLessonTypeSlug' => 'course-lesson'
        ];
    }

    /**
     * @since 1.0.0
     */
    public function load()
    {
        require_once __DIR__ . '/LSCourse.php';
        require_once __DIR__ . '/LSCourseConsumer.php';
        require_once __DIR__ . '/LSCourseLesson.php';
        require_once __DIR__ . '/LSCourseActivity.php';
        require_once __DIR__ . '/LSConsumerCourseItem.php';
        require_once __DIR__ . '/LSCourseHelper.php';

        // main hooks
        add_action('init', [$this, 'initEarly'], 1);
        add_action('init', [$this, 'init']);
        add_action('wp_enqueue_scripts', [$this, 'printScripts']);
        add_filter('the_content', [$this, 'theContent'], 99);
        add_filter('comments_template', [$this, 'commentsTemplate'], 99);
        add_filter('pre_get_posts', [$this, 'preGetPosts'], 98);
        add_filter('wp_get_nav_menu_items', [$this, 'wpNavMenuObjects'], 100);
        add_filter('pre_get_comments', [$this, 'preGetComments'], 99);

        // connect to consumers
        add_action('ls_consumer_deleted', [$this, 'consumerDeleted']);

        // connect to com.center
        if(LS()->isModuleActive('comCenter', '2.0.0')) {
            add_filter('ls_cc_init', [$this, 'comCenterInit']);
            add_action('ls_course_started', [$this, 'comCenterCourseStarted'], 10, 2);
            add_action('ls_course_lesson_read', [$this, 'comCenterCourseLessonRead'], 10, 3);
            add_action('ls_course_ended', [$this, 'comCenterCourseEnded'], 10, 2);
        }

        // connect to AT
        if(LS()->isModuleActive('at2')) {
            add_action('ls_course_started', [$this, 'atCourseStarted'], 10, 2);
        }
    }

    /**
     * Create shortcodes
     * @since 1.2
     */
    public function initShortcodes()
    {
        add_shortcode('course', [$this, 'courseShortcode']);
        add_shortcode('course-lesson-cockpit', [$this, 'courseLessonCockpitShortcode']);
        add_shortcode('course-start', [$this, 'courseStartShortcode']);
        add_shortcode('course-accept', [$this, 'courseAcceptShortcode']);
        add_shortcode('course-name', [$this, 'courseNameShortcode']);
        add_shortcode('course-progress-bar', [$this, 'courseProgressBarShortcode']);
        add_shortcode('courses-overview', [$this, 'coursesOverviewShortcode']);
    }

    /**
     * Load module scripts
     * @since 1.1.0
     * @internal
     */
    public function printScripts()
    {
        wp_enqueue_style('ls-course', $this->getUrl2Module() . 'css/course.css', [], $this->getVersion());
        wp_enqueue_script('jquery.ui.touch-punch', $this->getUrl2Module() . 'js/jquery.ui.touch-punch.min.js', ['jquery'], '0.2.3', true);
    }

    /**
     * Early init action
     * @since 2.0.0
     * @internal
     */
    public function initEarly()
    {
        register_post_type('course', [
            'labels'              => [
                'name'          => __('Courses', 'ls'),
                'singular_name' => __('Course', 'ls'),
                'add_new_item'  => __('Add new course', 'ls'),
                'edit_item'     => __('Edit course', 'ls'),
                'new_item'      => __('New course', 'ls'),
                'all_items'     => __('All courses', 'ls'),
                'view_item'     => __('View course post', 'ls'),
                'search_items'  => __('Search courses', 'ls'),
                'menu_name'     => __('Courses', 'ls')
            ],
            'public'              => true,
            'publicly_queryable'  => true,
            'show_ui'             => true,
            'show_in_menu'        => true,
            'query_var'           => true,
            'capability_type'     => 'post',
            'hierarchical'        => false,
            'exclude_from_search' => false,
            'rewrite'             => ['slug' => $this->getOption('courseTypeSlug')],
            'supports'            => ['title', 'subtitles', 'editor', 'author', 'thumbnail', 'excerpt', 'comments', 'revisions', 'custom-fields'],
            'taxonomies'          => ['post_tag', 'category']
        ]);

        register_post_type('course-lesson', [
            'labels'              => [
                'name'               => __('Course lessons', 'ls'),
                'singular_name'      => __('Course lesson', 'ls'),
                'add_new_item'       => __('Add new lesson', 'ls'),
                'edit_item'          => __('Edit lesson', 'ls'),
                'new_item'           => __('New lesson', 'ls'),
                'all_items'          => __('All lessons', 'ls'),
                'view_item'          => __('View lesson post', 'ls'),
                'search_items'       => __('Search lessons', 'ls'),
                'not_found'          => __('No lessons found', 'ls'),
                'not_found_in_trash' => __('No lessons in the trash found', 'ls'),
                'menu_name'          => __('Course lessons', 'ls')
            ],
            'public'              => true,
            'publicly_queryable'  => true,
            'show_ui'             => true,
            'show_in_menu'        => true,
            'query_var'           => true,
            'capability_type'     => 'post',
            'hierarchical'        => false,
            'exclude_from_search' => false,
            'rewrite'             => ['slug' => $this->getOption('courseLessonTypeSlug')],
            'supports'            => ['title', 'subtitles', 'editor', 'author', 'thumbnail', 'excerpt', 'comments', 'revisions', 'custom-fields'],
            'acm_content_access'  => false
        ]);

        if(LS()->isModuleActive('at2')) {
            $this->initATActions();
        }
    }

    /**
     * Process actions and requests
     * @since 1.0.0
     * @internal
     */
    public function init()
    {
        if(!$this->hasAction()) {
            return;
        }

        if($this->isAction('course-start')) {

            $consumerId = consumersLoyaltySuite::getAuthorizedConsumerId();
            if($consumerId > 0) {

                $courseId = $this->getParam('id', 'id');
                $course = LSCourseHelper::getCourse($courseId);
                if($course && $course->isActual()) {

                    if(LSCourseHelper::isConsumerStartedCourse($consumerId, $courseId)) {
                        $courseName = $course->getCourseName();
                        $this->setError(sprintf(__('You are already participating in the course "%s"', 'ls'), empty($courseName) ? 'UNKNOWN COURSE' : $courseName));
                    } elseif($this->startCourse($courseId, $consumerId)) {
                        $this->setAlert(__('Course has been successfully started', 'ls'));
                    } else {
                        $this->setError(__('Can not start course', 'ls'));
                        self::logBreakpoint();
                    }

                } else {
                    $this->setError(__('Course is not available at the current time', 'ls'));
                }
            }

            \LS\Library::redirect(remove_query_arg(['action', 'id']));
        }

        if($this->isAction('course-accept')) {

            $consumerId = consumersLoyaltySuite::getAuthorizedConsumerId();
            $courseId = $this->getParam('id', 'id');
            $postId = $this->getParam('post_id', 'id');
            $course = LSCourseHelper::getCourse($courseId);
            $success = false;

            if($course && $course->isActual() && $this->canConsumerMarkPostAsRead($postId, $consumerId, $courseId)) {

                $courseInterval = $this->getParam('interval', 'int');
                $courseInterval = $courseInterval >= $course->getMinInterval() && $courseInterval <= $course->getMaxInterval() ? $courseInterval : LSCourse::DEFAULT_INTERVAL;

                if(($consumerCourse = $this->acceptCourseActivity($courseId, $postId, $courseInterval)) !== false) {

                    if($consumerCourse->isEnded()) {

                        $this->setAlert(__('Done!', 'ls') . ' ' . __('You have successfully finished the course', 'ls'));

                        $coursePage = $course->getCourseLink();

                        if(!empty($coursePage)) {
                            \LS\Library::redirect($coursePage);
                        }

                    } elseif($courseInterval == 0) {
                        $this->setAlert(__('Done!', 'ls') . ' ' . __('You now allowed to enter the next post', 'ls'));
                    } else {
                        $this->setAlert(__('Done!', 'ls') . ' ' . sprintf(__('You will get access to the next post in %d day(s)', 'ls'), $courseInterval));
                    }

                    $success = true;
                }
            }

            if(!$success) {
                $this->setAlert(__('Can not mark post as read', 'ls'));
            }

            \LS\Library::redirect(remove_query_arg(['action', 'id', 'post_id', 'interval']));
        }

        if($this->isAction('course-stop')) {

            $courseId = $this->getParam('courseId', 'id');
            $consumerId = consumersLoyaltySuite::getAuthorizedConsumerId();

            if($courseId > 0 && $consumerId > 0 && $this->stopCourse($courseId, $consumerId)) {
                $this->setAlert(__('Participation in the course has been stopped', 'ls'));
            } else {
                $this->setError(__('Can not stop participation in the course', 'ls'));
                self::logBreakpoint();
            }

            \LS\Library::redirect(remove_query_arg(['action', 'courseId']));
        }

        if($this->isAction('course-restart')) {

            $courseId = $this->getParam('courseId', 'id');
            $consumerId = consumersLoyaltySuite::getAuthorizedConsumerId();

            if($courseId > 0 && $consumerId > 0 && $this->restartCourse($courseId, $consumerId)) {
                $this->setAlert(__('Participation in the course has been restarted', 'ls'));
            } else {
                $this->setError(__('Can not restart participation in the course', 'ls'));
                self::logBreakpoint();
            }

            \LS\Library::redirect(remove_query_arg(['action', 'courseId']));
        }
    }

    /**
     * Consumer deleted action
     *
     * @since 1.0.0
     * @internal
     *
     * @param int $consumerId Consumer ID
     */
    public function consumerDeleted(int $consumerId)
    {
        LSCourseHelper::deleteConsumerFromAllCourses($consumerId);
    }

    /**
     * Retrieve the course item which is accessible by consumer and have not been read
     *
     * @since 2.0.0
     *
     * @param LSCourseConsumer $cc Consumer Challenge
     * @return false|LSConsumerCourseItem Course item or false on failure
     */
    public function getLatestAccessiblePostInConsumerCourse(LSCourseConsumer $cc)
    {
        foreach($this->buildConsumerCourse($cc->getCourseId(), $cc->getConsumerId()) as $item) {
            if($item->isAccessible() && !$item->isRead()) {
                return $item;
            }
        }

        return false;
    }

    /**
     * Start course by consumer
     *
     * @since 1.0.0
     *
     * @param int $courseId Course Id
     * @param int $consumerId Consumer Id
     * @return bool True if course successfully started
     */
    protected function startCourse($courseId, $consumerId)
    {
        $success = false;

        if($courseId > 0 && $consumerId > 0) {

            $consumerCourse = new LSCourseConsumer([
                'courseId'   => $courseId,
                'consumerId' => $consumerId
            ]);

            if(LSCourseHelper::createConsumerConnection($consumerCourse)) {
                \LS\ConsumersHelper::insertLog('course-started', $consumerId, $courseId);
                do_action('ls_course_started', $courseId, $consumerId);
                $success = true;
            }
        }

        return $success;
    }

    /**
     * Accept course activity by consumer
     *
     * @since 1.0.0
     *
     * @param int $courseId Course Id
     * @param int $postId Post Id
     * @param int $courseInterval Activity interval in days
     * @return LSCourseConsumer|false $cc Course connection to the consumer or false on failure
     */
    protected function acceptCourseActivity($courseId, $postId, $courseInterval)
    {
        $consumerId = consumersLoyaltySuite::getAuthorizedConsumerId();

        $activity = new LSCourseActivity([
            'courseId'        => $courseId,
            'consumerId'      => $consumerId,
            'postId'          => $postId,
            'interval'        => $courseInterval,
            'actionDate'      => strtotime('TODAY'),
            'actionTimestamp' => time()
        ]);

        if(LSCourseHelper::createActivity($activity)) {

            $cc = LSCourseHelper::getConsumerConnection($courseId, $consumerId);

            if($cc) {

                $isLastActivity = LSCourseHelper::getCourseLastLessonPostId($courseId) === $postId;

                if($isLastActivity) {

                    $cc->markAsEnded();

                    if(LSCourseHelper::updateConsumerConnection($cc)) {

                        \LS\ConsumersHelper::insertLog('course-lesson-read', $consumerId, $postId, $activity->getActionTimestamp());
                        do_action('ls_course_lesson_read', $courseId, $consumerId, $postId);

                        if($isLastActivity) {
                            \LS\ConsumersHelper::insertLog('course-ended', $consumerId, $courseId, $activity->getActionTimestamp());
                            do_action('ls_course_ended', $courseId, $consumerId);
                        }
                    }
                }
            }

        } else {
            $cc = false;
        }

        return $cc;
    }

    /**
     * Stop participation in the course by consumer
     *
     * @since 2.1.0
     *
     * @param int $courseId Course Id
     * @param int $consumerId Consumer Id
     * @return bool True if course stopped
     */
    protected function stopCourse($courseId, $consumerId)
    {
        if ($cc = LSCourseHelper::getConsumerConnection($courseId, $consumerId)) {
            if (LSCourseHelper::deleteConsumerToCourseConnection($cc)) {
                \LS\ConsumersHelper::insertLog('course-stopped', $consumerId, $courseId);
                return true;
            }
        }

        return false;
    }

    /**
     * Restart participation in the course by consumer
     *
     * @since 2.5.1
     *
     * @param int $courseId Course Id
     * @param int $consumerId Consumer Id
     * @return bool True if course restarted
     */
    protected function restartCourse($courseId, $consumerId)
    {
        return
            $this->stopCourse($courseId, $consumerId)
            && $this->startCourse($courseId, $consumerId);
    }

    /**
     * Handle access to post
     *
     * @since 2.0.0
     * @internal
     *
     * @param string $content Page content
     * @return string Page content
     */
    public function theContent($content)
    {
        $postId = get_the_ID();
        if($postId < 1) {
            return $content;
        }

        if(\LS\Library::isAdminPreview()) {
            return $content;
        }

        if(!$this->hasConsumerAccessToPost($postId)) {
            
            $course = LSCourseHelper::getCourseByLessonPostId($postId);
            $text = $course ? $course->getFailureMessage() : __('Sorry, you have no access to this post right now.', 'ls');

            return apply_filters('course_no_access_text', $text, $course);
        }

        return $content;
    }

    /**
     * Handle access to the comments
     *
     * @since 2.0.0
     * @internal
     *
     * @param string $template Template
     * @return string
     */
    public function commentsTemplate($template)
    {
        if(!\LS\Library::isAdminPreview()) {
            $postId = get_the_ID();
            if($postId > 0 && !$this->hasConsumerAccessToPost($postId)) {
                $template = __DIR__ . '/views/frontend/comments-template.php';
            }
        }

        return $template;
    }

    /**
     * Retrieve a list of protected posts for the guest/consumer
     * Note: if consumer participated in the course -> all the lessons (even non-accessible) will be NOT protected
     *
     * @since 2.0.0
     *
     * @return int[] Posts Id's
     */
    protected function getProtectedPosts()
    {
        $protectedPostsIds = [];
        $consumerCoursesIds = LSCourseHelper::getCoursesStartedByConsumer(
            consumersLoyaltySuite::getAuthorizedConsumerId()
        );

        foreach(LSCourseHelper::getAllLessons() as $lesson) {
            if(!in_array($lesson->getCourseId(), $consumerCoursesIds)) {
                $protectedPostsIds[] = $lesson->getPostId();
            }
        }

        return $protectedPostsIds;
    }

    /**
     * Control access to posts
     *
     * @since 2.0.0
     *
     * @param WP_Query $wpQuery
     * @return WP_Query
     */
    public function preGetPosts(WP_Query $wpQuery)
    {
        if (
            $wpQuery->get('course_stop_processing')
            || $wpQuery->is_singular()
            || \LS\Library::isBackend()
            || \LS\Library::isAdminPreview()
        ) {
            return $wpQuery;
        }

        $protectedPostsIds = $this->getProtectedPosts();

        if(!empty($protectedPostsIds)) {
            $wpQuery->set('post__not_in', array_unique(array_merge(wp_parse_id_list($wpQuery->get('post__not_in', [])), $protectedPostsIds)));
        }

        return $wpQuery;
    }

    /**
     * Remove pages from menus to which consumer don't have access
     *
     * @since 2.0.0
     * @internal
     *
     * @param array $objects An array of menu objects
     * @return array Array of objects to which consumer have access
     */
    public function wpNavMenuObjects($objects)
    {
        if (\LS\Library::isBackend()) {
            return $objects;
        }

        foreach($objects as $key => $object) {
            if($object->type === 'post_type' && !$this->hasConsumerAccessToPost((int) $object->object_id)) {
                unset($objects[$key]);
            }
        }

        return $objects;
    }

    /**
     * Control access to the comments
     * @since 2.0.0
     * @param WP_Comment_Query $wpCommentQuery
     */
    public function preGetComments(WP_Comment_Query $wpCommentQuery)
    {
        if (\LS\Library::isBackend() || \LS\Library::isAdminPreview()) {
            return;
        }

        $protectedPostsIds = $this->getProtectedPosts();

        if(!empty($protectedPostsIds)) {

            if(!isset($wpCommentQuery->query_vars['post__not_in'])) {
                $wpCommentQuery->query_vars['post__not_in'] = [];
            }

            $wpCommentQuery->query_vars['post__not_in'] = array_unique(array_merge(wp_parse_id_list($wpCommentQuery->query_vars['post__not_in']), $protectedPostsIds));
        }
    }

    /**
     * Check if consumer can mark post as read
     *
     * @since 2.3.0
     *
     * @param int $postId Course post Id
     * @param int $consumerId Course Id
     * @param int $courseId Course Id
     * @return bool True if consumer can mark post as read
     */
    protected function canConsumerMarkPostAsRead($postId, $consumerId, $courseId)
    {
        $result = false;

        if($postId > 0 && LSCourseHelper::isConsumerStartedCourse($consumerId, $courseId)) {
            foreach($this->buildConsumerCourse($courseId, $consumerId) as $item) {
                if($item->getPostId() === $postId) {
                    $result = $item->isAccessible() && !$item->isRead();
                }
            }
        }

        return $result;
    }

    /**
     * Check if consumer has access to the post
     *
     * @since 1.0.0
     *
     * @param int $postId Course post Id
     * @param int $consumerId Consumer Id or null to use current authorized consumer
     * @return bool True if consumer has access to the post
     */
    public function hasConsumerAccessToPost($postId, $consumerId = null)
    {
        // check if this is the course post (lesson)
        if(!LSCourseHelper::isCourseLesson($postId)) {
            return true;
        }

        // must be consumer
        $consumerId = $consumerId ?? consumersLoyaltySuite::getAuthorizedConsumerId();
        if($consumerId < 1) {
            return false;
        }

        // retrieve course by the post
        $courseId = LSCourseHelper::getCourseIdByLessonPostId($postId);
        if($courseId < 1) {
            return false;
        }

        // course not started
        if(!LSCourseHelper::isConsumerStartedCourse($consumerId, $courseId)) {
            return false;
        }

        // course ended -> means all posts inside it are accessible
        if(LSCourseHelper::isConsumerEndedCourse($consumerId, $courseId)) {
            return true;
        }

        // check access to post
        foreach($this->buildConsumerCourse($courseId, $consumerId) as $item) {
            if($item->getPostId() === $postId) {
                return $item->isAccessible();
            }
        }

        return false;
    }

    /**
     * Build consumer statement in the course list
     * Note: course must be started to use this function
     *
     * @since 1.0.0
     *
     * @param int $courseId Course Id
     * @param int $consumerId Consumer Id
     * @return LSConsumerCourseItem[]
     */
    public function buildConsumerCourse($courseId, $consumerId = null)
    {
        static $consumerCourses;

        if ($consumerCourses === null) {
            $consumerCourses = [];
        }

        $consumerCourse = [];

        if($courseId > 0) {

            $consumerId = $consumerId ?? consumersLoyaltySuite::getAuthorizedConsumerId();
            $isCurrentConsumer = consumersLoyaltySuite::getAuthorizedConsumerId() == $consumerId;

            if($isCurrentConsumer && isset($consumerCourses[$courseId])) {

                $consumerCourse = $consumerCourses[$courseId];

            } elseif(!empty($lessons = LSCourseHelper::getLessons($courseId))) {

                // get consumer activities in the course
                $consumerActivities = LSCourseHelper::getConsumerActivitiesInCourse($consumerId, $courseId);

                $postsStatuses = LSCourseHelper::getPostsStatuses(array_map(static function(LSCourseLesson $lesson) {
                    return $lesson->getPostId();
                }, $lessons));

                foreach($lessons as $i => $lesson) {

                    if(!isset($postsStatuses[$lesson->getPostId()])) {
                        continue;
                    }

                    $item = new LSConsumerCourseItem($lesson->getCourseId(), $lesson->getPostId());

                    // always restrict access by default (exception: published first post)
                    if($i > 0 || $postsStatuses[$lesson->getPostId()] !== 'publish') {
                        $item->restrictAccess();
                    }

                    // first lesson always should be available if course started
                    if($i == 0 && $postsStatuses[$lesson->getPostId()] === 'publish') {
                        $cc = LSCourseHelper::getConsumerConnection($courseId, $consumerId);
                        $item->setAccessible($cc->getStartDate());
                    }

                    foreach($consumerActivities as $ca) {

                        // check if post was "read"
                        if($ca->getPostId() === $item->getPostId()) {

                            $item
                                ->markAsRead($ca->getActionTimestamp())
                                ->markAsAccessible();

                            // check if previous post was "read"
                        } elseif($i > 0 && $ca->getPostId() === $consumerCourse[$i - 1]->getPostId()) {

                            // set access date to the lesson
                            $item->setAccessible($ca->getActionDate() + $ca->getInterval() * DAY_IN_SECONDS);
                        }
                    }

                    if(\LS\Library::isAdminPreview()) {
                        $item->markAsAccessible();
                    }

                    $consumerCourse[$i] = $item;

                    unset($item);
                }
            }

            if($isCurrentConsumer) {
                $consumerCourses[$courseId] = $consumerCourse;
            }
        }

        return $consumerCourse;
    }

    /**
     * Shortcode [course-start]
     * Display a link to start the course
     *
     * @since 1.0.0
     * @internal
     *
     * @param array $atts Attributes ("id" - course Id, "class" - extra class for the link)
     * @param string $linkText Text inside the link
     * @return string HTML
     */
    public function courseStartShortcode($atts, $linkText)
    {
        if(!consumersLoyaltySuite::isConsumerAuthorized()) {
            return '';
        }

        $courseId = isset($atts['id']) ? (int) $atts['id'] : 0;
        if($courseId < 1) {
            return current_user_can('edit_users') ? 'Admin warning: Shortcode [course-start] --> attribute id" is required' : '';
        }

        $consumerId = consumersLoyaltySuite::getAuthorizedConsumerId();
        $course = LSCourseHelper::getCourse($courseId);

        if(!$course || !$course->valid()) {
            return current_user_can('edit_users') ? 'Admin warning: Shortcode [course-start] --> incorrect course ID' : '';
        }

        if(!$course->isActual()) {
            return current_user_can('edit_users') ? 'Admin warning: Shortcode [course-start] --> course not activated/expired' : __('Course is not available at the current time', 'ls');
        }

        if(LSCourseHelper::isConsumerStartedCourse($consumerId, $courseId)) {
            return current_user_can('edit_users') ? 'Admin warning: Shortcode [course-start] --> consumer already participated' : '';
        }

        $lessons = LSCourseHelper::getLessons($courseId);
        if(empty($lessons)) {
            return current_user_can('edit_users') ? 'Admin warning: Course #' . $courseId . ' has no posts' : '';
        }

        $link = add_query_arg(['action' => 'course-start', 'id' => $courseId]);
        $class = isset($atts['class']) ? 'class="' . esc_attr($atts['class']) . '"' : '';
        $linkText = empty($linkText) ? __('Start course', 'ls') : $linkText;

        return '<a href="' . esc_url($link) . '" ' . $class . '>' . do_shortcode($linkText) . '</a>';
    }

    /**
     * Retrieve a link to mark post as read in course scope
     *
     * @since 2.5.7
     *
     * @param string $class Class name
     * @param string $linkText Text inside the link
     * @return string HTML
     */
    public function getCourseAcceptLink($class = '', $linkText = '')
    {
        $post = get_post();

        if(!($post instanceof WP_Post)) {
            return current_user_can('edit_users') ? 'Admin warning: Shortcode [course-accept] --> incorrect post id' : '';
        }

        $consumerId = consumersLoyaltySuite::getAuthorizedConsumerId();
        if($consumerId < 1) {
            return '';
        }

        $course = LSCourseHelper::getCourseByLessonPostId($post->ID);
        if(!$course || !$course->valid()) {
            return current_user_can('edit_users') ? 'Admin warning: Shortcode [course-accept] --> incorrect course ID' : '';
        }

        if(!$course->isActual()) {
            return current_user_can('edit_users') ? 'Admin warning: Shortcode [course-accept] --> course not activated/expired' : __('Course is not available at the current time', 'ls');
        }

        if(!LSCourseHelper::isConsumerStartedCourse($consumerId, $course->getCourseId())) {
            return current_user_can('edit_users') ? 'Admin warning: Shortcode [course-accept] --> consumer not started course' : '';
        }

        $consumerCourse = $this->buildConsumerCourse($course->getCourseId());
        if(empty($consumerCourse)) {
            return current_user_can('edit_users') ? 'Admin warning: Course with ID ' . $course->getCourseId() . ' has no posts' : '';
        }

        foreach($consumerCourse as $i => $item) {
            if($item->getPostId() === $post->ID) {

                if($item->isAccessible() && !$item->isRead()) {

                    $link = add_query_arg(['action' => 'course-accept', 'id' => $course->getCourseId(), 'post_id' => $post->ID, 'interval' => $course->getDefaultInterval()]);
                    $class = !empty($class) ? ' ' . esc_attr($class) : '';
                    $class = 'class="course-accept-btn button normal-button large inline' . $class . '"';
                    $linkText = empty($linkText) ? __('Ready with this!', 'ls') : $linkText;
                    $link = '<a href="' . esc_url($link) . '" ' . $class . ' data-post-id="' . $post->ID . '" data-course-id="' . $course->getCourseId() . '">' . $linkText . '</a>';

                    // calculate allowed intervals
                    if($course->getEndDate() > 0) {
                        $daysToEnd = ($course->getEndDate() - strtotime('TODAY')) / DAY_IN_SECONDS + 1;
                        $nextPostsCount = count($consumerCourse) - $i - 1;
                        $minDaysToStart = $daysToEnd - $nextPostsCount;
                        if($minDaysToStart < 1) {
                            $course
                                ->setMinInterval(0)
                                ->setMaxInterval(0)
                                ->setDefaultInterval(0);
                        } elseif($course->getMaxInterval() > $minDaysToStart) {
                            $course
                                ->setMinInterval(1)
                                ->setMaxInterval($minDaysToStart)
                                ->setDefaultInterval(1);
                        }
                    }

                    return (new LS\Template(__DIR__ . '/views/frontend'))
                        ->assign('isLast', count($consumerCourse) === $i + 1)
                        ->assign('link', $link)
                        ->assign('course', $course)
                        ->render('course-accept.phtml', false);
                }

                return '';
            }
        }

        return current_user_can('edit_users') ? 'Admin warning: Shortcode [course-accept] --> post not in the course' : '';
    }

    /**
     * Shortcode [course-accept]
     * Display a link to mark post as read in course scope
     *
     * @since 1.0.0
     * @internal
     *
     * @param array $atts Attributes ("class" - extra class for the link, "accessible_text" - custom accessible text)
     * @param string $linkText Text inside the link
     * @return string HTML
     */
    public function courseAcceptShortcode($atts, $linkText = '')
    {
        return $this->getCourseAcceptLink($atts['class'] ?? '', $linkText);
    }

    /**
     * Shortcode [course]
     * Display a list of posts in the course
     *
     * @since 2.5.0
     * @internal
     *
     * @param array $atts Attributes ("id" - course Id)
     * @return string HTML
     */
    public function courseShortcode($atts)
    {
        if(!consumersLoyaltySuite::isConsumerAuthorized()) {
            return __('Please authorize to see the course', 'ls');
        }

        $consumerId = consumersLoyaltySuite::getAuthorizedConsumerId();
        $courseId = isset($atts['id']) ? (int) $atts['id'] : LSCourseHelper::getCourseIdByCoursePageId(get_the_ID());
        if($courseId < 1) {
            return current_user_can('edit_users') ? 'Admin warning: Shortcode [course] --> attribute "id" is required' : '';
        }

        $course = LSCourseHelper::getCourse($courseId);
        if(!$course) {
            return current_user_can('edit_users') ? 'Admin warning: Shortcode [course] --> course #' . $courseId . ' not found' : '';
        }

        $courseStarted = LSCourseHelper::isConsumerStartedCourse($consumerId, $courseId);
        if($courseStarted) {

            $consumerCourse = $this->buildConsumerCourse($courseId, $consumerId);
            if(empty($consumerCourse)) {
                return current_user_can('edit_users') ? 'Admin warning: Course #' . $courseId . ' has no posts' : '';
            }

            if(!$course->isActual()) {
                return current_user_can('edit_users') ? 'Admin warning: Shortcode [course] --> course not activated/expired' : '<p>' . __('Course is not available at the current time', 'ls') . '</p>';
            }

            $latestReadLessonNumber = 0;

            foreach($consumerCourse as $i => $item) {
                if($item->isAccessible() && $item->isRead()) {
                    $latestReadLessonNumber = $i + 1;
                }
            }

        } else {
            $consumerCourse = false;
            $latestReadLessonNumber = 0;
        }

        return (new LS\Template(__DIR__ . '/views/frontend'))
            ->assign('course', $course)
            ->assign('courseStarted', $courseStarted)
            ->assign('consumerCourse', $consumerCourse)
            ->assign('courseEnded', LSCourseHelper::isConsumerEndedCourse($consumerId, $courseId))
            ->assign('courseLessonsCount', count(LSCourseHelper::getLessons($courseId)))
            ->assign('latestReadLessonNumber', $latestReadLessonNumber)
            ->assign('courseNextLesson', $this->getNextCourseLessonByCourseId($courseId, $consumerId))
            ->assign('url', $this->getUrl2Module())
            ->render('course.phtml', false);
    }

    /**
     * Display cockpit for the current course lesson for the current consumer
     * @since 2.5.7
     * @return string HTML
     */
    public function getCourseLessonCockpit()
    {
        return (new LS\Template(__DIR__ . '/views/frontend'))
            ->assign('courseData', $this->prepareLessonForConsumer(get_the_ID()))
            ->render('course-lesson-cockpit.phtml', false);
    }

    /**
     * Shortcode [course-lesson-cockpit]
     * Display cockpit for the current course lesson for the current consumer
     *
     * @since 2.5.0
     * @internal
     *
     * @return string HTML
     */
    public function courseLessonCockpitShortcode()
    {
        return $this->getCourseLessonCockpit();
    }

    /**
     * Shortcode [course-name]
     * Display course name
     *
     * @since 1.0.0
     * @internal
     *
     * @param array $atts Shortcode attributes
     * @return string HTML
     */
    public function courseNameShortcode($atts)
    {
        $courseId = isset($atts['id']) ? (int) $atts['id'] : LSCourseHelper::getCourseIdByCoursePageId(get_the_ID());
        if($courseId < 1) {
            return current_user_can('edit_users') ? 'Admin warning: Shortcode [course-name] --> attribute "id" is required' : '';
        }

        $course = LSCourseHelper::getCourse($courseId);
        if(!$course || !$course->valid()) {
            return current_user_can('edit_users') ? 'Admin warning: Shortcode [course-name] --> can not find course with id ' . $courseId : '';
        }

        return $course->getCourseName();
    }

    /**
     * Shortcode [course-progress-bar]
     * Display challenge progress (bar) for consumer
     *
     * @since 1.2.0
     * @internal
     *
     * @param array $atts Attributes ('id' - Course Id, 'class' - CSS classes)
     * @return string HTML
     */
    public function courseProgressBarShortcode($atts)
    {
        return $this->getCourseProgressBar(
            isset($atts['id']) ? (int) $atts['id'] : LSCourseHelper::getCourseIdByCoursePageId(get_the_ID()),
            null,
            $atts['class'] ?? ''
        );
    }

    /**
     * Shortcode [courses-overview]
     * Display courses overview for consumer
     *
     * @since 2.2.0
     * @internal
     * @return string HTML
     */
    public function coursesOverviewShortcode()
    {
        $html = '';

        $consumerId = consumersLoyaltySuite::getAuthorizedConsumerId();
        if($consumerId > 0) {

            $data = [];

            foreach(LSCourseHelper::getConsumerConnections($consumerId) as $cc) {

                $course = LSCourseHelper::getCourse($cc->getCourseId());

                if($course && $course->isActual()) {
                    $data[$cc->getCourseId()] = [
                        'course'       => $course,
                        'lessonsTotal' => $this->getNumberOfLessonsInTheCourse($cc->getCourseId()),
                        'lessonsRead'  => LSCourseHelper::getConsumerReadLessonsInCourse($consumerId, $cc->getCourseId()),
                        'ended'        => $cc->isEnded(),
                        'nextLesson'   => $this->getNextCourseLessonByCourseId($course->getCourseId(), $consumerId)
                    ];
                }
            }

            $html = (new LS\Template(__DIR__ . '/views/frontend'))
                ->assign('consumerId', $consumerId)
                ->assign('data', $data)
                ->assign('imagesUrl', $this->getUrl2Module() . 'images')
                ->render('courses-overview.phtml', false);
        }

        return $html;
    }

    /**
     * Display course progress bar
     *
     * @since 2.0.4
     *
     * @param int $courseId Course Id
     * @param null|int $consumerId Consumer Id or null to get the current consumer
     * @param string $class Class name
     * @return string HTML
     */
    protected function getCourseProgressBar($courseId, $consumerId = null, $class = '')
    {
        $consumerId = $consumerId ?? consumersLoyaltySuite::getAuthorizedConsumerId();

        $html = '';

        if($consumerId > 0 && $courseId > 0) {

            $progress = LSCourseHelper::calculateConsumerProgress($courseId, $consumerId);

            if(($html = apply_filters('ls-course-progress', '', $progress, $consumerId, $courseId)) == '') {
                $html = (new LS\Template(__DIR__ . '/views/frontend'))
                    ->assign('progress', $progress)
                    ->assign('class', $class)
                    ->render('progress-bar.phtml', false);
            }
        }

        return $html;
    }

    /**
     * Generate a list of conditions for Com.Center
     *
     * @since 1.0.0
     * @internal
     *
     * @param LSConditionalEmailCondition[] $conditions List of conditions
     * @return LSConditionalEmailCondition[] List of conditions
     */
    public function comCenterInit($conditions)
    {
        $conditions['course_started'] = new LSConditionalEmailCondition('course_started', __('Course started by consumer', 'ls'), $this->getTitle(), [
            'allowedPeriods' => [LSConditionalEmail::PERIOD_ON],
            'onceADay'       => false,
            'shortcodes'     => [
                '{courseName}' => [
                    'label' => __('Course name', 'ls'),
                    'value' => static function($consumerId, LSConditionalEmail $ce) {
                        return $ce->getConsumerExtraValue($consumerId, 'courseName', 'COURSE-NAME');
                    }
                ],
                '{courseLink}' => [
                    'label' => __('Link to the course', 'ls'),
                    'value' => static function($consumerId, LSConditionalEmail $ce) {
                        return $ce->getConsumerExtraValue($consumerId, 'courseLink', home_url('/'));
                    }
                ]
            ],
            'defaultMessage' => '<p>Dear [consumer-name], you successfully started the course {courseName}.</p>',
            'customFields'   => [
                'courseId' => static function() {
                    return new LS\Field\Select('courseId', __('Course', 'ls'), LSCourseHelper::getCoursesList(), ['emptyText' => __('All courses', 'ls')]);
                }
            ]
        ]);

        $conditions['course_lesson_read'] = new LSConditionalEmailCondition('course_lesson_read', __('Consumer read the lesson', 'ls'), $this->getTitle(), [
            'allowedPeriods' => [LSConditionalEmail::PERIOD_ON],
            'onceADay'       => false,
            'shortcodes'     => [
                '{courseName}'       => [
                    'label' => __('Course name', 'ls'),
                    'value' => static function($consumerId, LSConditionalEmail $ce) {
                        return $ce->getConsumerExtraValue($consumerId, 'courseName', 'COURSE-NAME');
                    }
                ],
                '{courseLink}' => [
                    'label' => __('Link to the course', 'ls'),
                    'value' => static function($consumerId, LSConditionalEmail $ce) {
                        return $ce->getConsumerExtraValue($consumerId, 'courseLink', home_url('/'));
                    }
                ],
                '{courseLessonName}' => [
                    'label' => __('Course lesson name', 'ls'),
                    'value' => static function($consumerId, LSConditionalEmail $ce) {
                        return $ce->getConsumerExtraValue($consumerId, 'courseLessonName', 'LESSON-NAME');
                    }
                ]
            ],
            'defaultMessage' => '<p>Dear [consumer-name], you successfully read the course lesson {courseLessonName} in the course {courseName}.</p>',
            'customFields'   => [
                'courseId' => static function() {
                    return new LS\Field\Select('courseId', __('Course', 'ls'), LSCourseHelper::getCoursesList(), ['emptyText' => __('All courses', 'ls')]);
                }
            ]
        ]);

        $conditions['course_ended'] = new LSConditionalEmailCondition('course_ended', __('Course ended by consumer', 'ls'), $this->getTitle(), [
            'allowedPeriods' => [LSConditionalEmail::PERIOD_ON],
            'onceADay'       => false,
            'shortcodes'     => [
                '{courseName}' => [
                    'label' => __('Course name', 'ls'),
                    'value' => static function($consumerId, LSConditionalEmail $ce) {
                        return $ce->getConsumerExtraValue($consumerId, 'courseName', 'COURSE-NAME');
                    }
                ],
                '{courseLink}' => [
                    'label' => __('Link to the course', 'ls'),
                    'value' => static function($consumerId, LSConditionalEmail $ce) {
                        return $ce->getConsumerExtraValue($consumerId, 'courseLink', home_url('/'));
                    }
                ]
            ],
            'defaultMessage' => '<p>Dear [consumer-name], you successfully finished the course {courseName}.</p>',
            'customFields'   => [
                'courseId' => static function() {
                    return new LS\Field\Select('courseId', __('Course', 'ls'), LSCourseHelper::getCoursesList(), ['emptyText' => __('All courses', 'ls')]);
                }
            ]
        ]);

        $conditions['course_reminder'] = new LSConditionalEmailCondition('course_reminder', __('Next lesson is available to read [reminder]', 'ls'), $this->getTitle(), [
            'allowedPeriods' => [LSConditionalEmail::PERIOD_AFTER],
            'defaultDays'    => 3,
            'shortcodes'     => [
                '{courseName}' => [
                    'label' => __('Course name', 'ls'),
                    'value' => static function($consumerId, LSConditionalEmail $ce) {
                        return $ce->getConsumerExtraValue($consumerId, 'courseName', 'COURSE-NAME');
                    }
                ],
                '{courseLink}' => [
                    'label' => __('Link to the course', 'ls'),
                    'value' => static function($consumerId, LSConditionalEmail $ce) {
                        return $ce->getConsumerExtraValue($consumerId, 'courseLink', home_url('/'));
                    }
                ],
                '{days}'       => [
                    'label' => __('Days after the last activity in the course', 'ls'),
                    'value' => static function($consumerId, LSConditionalEmail $ce) {
                        return $ce->getConsumerExtraValue($consumerId, 'days', 'N');
                    }
                ]
            ],
            'defaultMessage' => '<p>Dear [consumer-name], you missed the course "{courseName}" {days} days ago.</p>',
            'customFields'   => [
                'courseId' => static function() {
                    return new LS\Field\Select('courseId', __('Course', 'ls'), LSCourseHelper::getCoursesList(), ['emptyText' => __('All courses', 'ls')]);
                }
            ],
            'recipientsCb'   => static function(LSConditionalEmail $ce) {

                $courseId = (int) $ce->getCustomFieldValue('courseId', 0);
                $days = $ce->getDays();
                $consumers =
                $posts = [];

                foreach(LSCourseHelper::getActivitiesForReminder($days, $courseId) as $activity) {

                    if(!isset($posts[$activity->getPostId()])) {
                        $posts[$activity->getPostId()] = [
                            'title' => get_the_title($activity->getPostId()),
                            'link'  => get_permalink($activity->getPostId())
                        ];
                    }

                    $ce
                        ->setConsumerExtraValue($activity->getConsumerId(), 'courseName', $posts[$activity->getPostId()]['title'])
                        ->setConsumerExtraValue($activity->getConsumerId(), 'courseLink', $posts[$activity->getPostId()]['link'])
                        ->setConsumerExtraValue($activity->getConsumerId(), 'days', $days);

                    $consumers[] = $activity->getConsumerId();
                }

                return $consumers;
            }
        ]);

        return $conditions;
    }

    /**
     * Trigger Com.Center email when course started
     *
     * @since 2.1.0
     *
     * @param int $courseId Course Id
     * @param int $consumerId Consumer Id
     */
    public function comCenterCourseStarted($courseId, $consumerId)
    {
        /** @var comCenterLoyaltySuite $comCenterModule */
        $comCenterModule = LS()->getModule('comCenter');
        $comCenterModule->triggerConditionalEmails('course_started', static function(LSConditionalEmail $ce) use ($consumerId, $courseId) {

            $course = LSCourseHelper::getCourse($courseId);
            if(!$course) {
                return false;
            }

            $ce
                ->setConsumerExtraValue($consumerId, 'courseName', $course->getCourseName())
                ->setConsumerExtraValue($consumerId, 'courseLink', $course->getCourseLink());

            return $consumerId;
        });
    }

    /**
     * Trigger Com.Center email when consumer read the course lesson
     *
     * @since 2.1.0
     *
     * @param int $courseId Course Id
     * @param int $consumerId Consumer Id
     * @param int $postId Post Id (lesson)
     */
    public function comCenterCourseLessonRead($courseId, $consumerId, $postId)
    {
        /** @var comCenterLoyaltySuite $comCenterModule */
        $comCenterModule = LS()->getModule('comCenter');
        $comCenterModule->triggerConditionalEmails('course_lesson_read', static function(LSConditionalEmail $ce) use ($consumerId, $courseId, $postId) {

            $course = LSCourseHelper::getCourse($courseId);
            if(!$course) {
                return false;
            }

            $ce
                ->setConsumerExtraValue($consumerId, 'courseName', $course->getCourseName())
                ->setConsumerExtraValue($consumerId, 'courseLink', $course->getCourseLink())
                ->setConsumerExtraValue($consumerId, 'courseLessonName', get_the_title($postId));

            return $consumerId;
        });
    }

    /**
     * Trigger Com.Center email when course ended
     *
     * @since 2.1.0
     *
     * @param int $courseId Course Id
     * @param int $consumerId Consumer Id
     */
    public function comCenterCourseEnded($courseId, $consumerId)
    {
        /** @var comCenterLoyaltySuite $comCenterModule */
        $comCenterModule = LS()->getModule('comCenter');
        $comCenterModule->triggerConditionalEmails('course_ended', static function(LSConditionalEmail $ce) use ($consumerId, $courseId) {

            $course = LSCourseHelper::getCourse($courseId);
            if(!$course) {
                return false;
            }

            $ce
                ->setConsumerExtraValue($consumerId, 'courseName', $course->getCourseName())
                ->setConsumerExtraValue($consumerId, 'courseLink', $course->getCourseLink());

            return $consumerId;
        });
    }

    /**
     * Init AT actions
     * @since 2.2.4
     */
    protected function initATActions()
    {
        ATActions::addAction('course_started', (new ATAction())
            ->setLabels([
                'title'        => __('Start course', 'ls'),
                'openActivity' => __('Open course', 'ls'),
                'doneActivity' => __('Course started', 'ls'),
                'emptyIdent'   => __('Activity "%s": please select course', 'ls')
            ])
            ->allowEmptyLink(false)
            ->setIdentId('identCourseStart')
            ->setReviewFunc(static function($consumerId, $activity) {
                return LSCourseHelper::isConsumerStartedCourse($consumerId, (int) $activity['ident']);
            })
            ->setSettingsFunc(static function($activity) {
                (new LS\Template(__DIR__ . '/views/backend'))
                    ->assign('courses', LSCourseHelper::getCoursesList())
                    ->assign('activity', $activity)
                    ->render('admin-at-course-started-action.phtml');
            })
            ->validateActivity(static function($activity) {

                $course = LSCourseHelper::getCourse((int) $activity['ident']);
                if(!$course || !$course->isActual()) {
                    $activity['future'] = true;
                }

                return $activity;
            })
            ->setConsumerDetailsLogFunc(static function($activity) {
                $course = LSCourseHelper::getCourse((int) $activity['ident']);
                return __('Course started', 'ls') . ' "' . ($course ? $course->getCourseName() : 'UNKNOWN COURSE NAME') . '"';
            })
        );
    }

    /**
     * Mark AT action as performed on course started
     *
     * @since 2.2.4
     * @internal
     *
     * @param int $courseId Course Id
     * @param int $consumerId Consumer Id
     */
    public function atCourseStarted($courseId, $consumerId)
    {
        /** @var at2LoyaltySuite $at */
        $at = LS()->getModule('at2');
        $at->activitiesLogic('course_started', $courseId, $consumerId);
    }

    /**
     * Retrieve a list of posts in the course
     *
     * @since 2.0.0
     *
     * @param LSCourse $course Course object
     * @return LSConsumerCourseItem[]
     */
    public function getCoursePostsForConsumer(LSCourse $course)
    {
        $consumerId = consumersLoyaltySuite::getAuthorizedConsumerId();

        if($consumerId < 1) {
            return [];
        }

        if(!$course || !$course->isActual() || !LSCourseHelper::isConsumerStartedCourse($consumerId, $course->getCourseId())) {
            return [];
        }

        $consumerCourse = $this->buildConsumerCourse($course->getCourseId(), $consumerId);

        if(empty($consumerCourse)) {
            return [];
        }

        return $consumerCourse;
    }

    /**
     * Get the course lesson which is the next to read for the consumer
     *
     * @since 2.3.0
     *
     * @param int $courseId Course Id
     * @param null|int $consumerId Consumer Id or null to use the current logged in consumer
     * @return false|LSConsumerCourseItem LSConsumerCourseItem object of the next course lesson
     */
    public function getNextCourseLessonByCourseId($courseId, $consumerId = null)
    {
        $courseItem = false;

        if(LSCourseHelper::isConsumerStartedCourse($consumerId, $courseId)) {
            foreach($this->buildConsumerCourse($courseId, $consumerId) as $item) {
                if($item->isAccessible() && !$item->isRead()) {
                    $courseItem = $item;
                } elseif($item->isScheduled()) {
                    $courseItem = $item;
                    break;
                }
            }
        }

        return $courseItem;
    }

    /**
     * Retrieve number of lessons in the course
     *
     * @since 2.0.0
     *
     * @param int $courseId Course Id
     * @return int Number of lessons in the course
     */
    public function getNumberOfLessonsInTheCourse($courseId)
    {
        return count(LSCourseHelper::getLessons($courseId));
    }

    /**
     * Retrieve course + complete list of posts (lessons) for the specified course lesson (post ID)
     *
     * @since 2.3.0
     *
     * @param int $postId Post to collect the data
     * @return object|false
     */
    public function prepareLessonForConsumer($postId)
    {
        $course = LSCourseHelper::getCourseByLessonPostId($postId);
        if(!$course) {
            return false;
        }

        $course = LSCourseHelper::getCourse($course->getCourseId());
        $lessons = $this->getCoursePostsForConsumer($course);
        $lessonsCount = count($lessons);
        $currentLesson = false;
        $lessonNumber = 0;
        $readPosts = 0;

        foreach($lessons as $i => $ci) {

            if($ci->getPostId() === $postId) {
                $currentLesson = $ci;
                $lessonNumber = $i + 1;
            }

            if($ci->isRead()) {
                ++$readPosts;
            }
        }

        return (object) [
            'courseId'      => $course->getCourseId(),
            'course'        => $course,
            'lessons'       => $lessons,
            'lessonsCount'  => $lessonsCount,
            'currentLesson' => $currentLesson,
            'lessonNumber'  => $lessonNumber,
            'progress'      => empty($lessons) ? 0 : ceil($readPosts / $lessonsCount * 100)
        ];
    }
}