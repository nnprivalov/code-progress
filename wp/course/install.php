<?php
function courseInstallLoyaltySuite()
{
    $wpdb = LS()->wpdb;

    $tb = $wpdb->get_var(sprintf("SHOW TABLES LIKE '{$wpdb->prefix}ls_course'"));
    if(empty($tb)) {
        $wpdb->query(sprintf("CREATE TABLE {$wpdb->prefix}ls_course (
            `courseId` SMALLINT(5) UNSIGNED NOT NULL AUTO_INCREMENT,
            `coursePageId` INT(9) UNSIGNED NOT NULL,
            `courseObjective` VARCHAR(1000) NOT NULL,
            `courseTargetGroup` VARCHAR(1000) NOT NULL,
            `trainerName` VARCHAR(100) NOT NULL,
            `trainerOccupation` VARCHAR(100) NOT NULL,
            `trainerImage` VARCHAR(1000) NOT NULL,
            `startDate` DATE NULL DEFAULT '0000-00-00',
            `endDate` DATE NULL DEFAULT '0000-00-00',
            `sendLog` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
            `logEmail` VARCHAR(200) NULL DEFAULT NULL,
            `logSubject` VARCHAR(500) NULL DEFAULT NULL,
            `logBody` LONGTEXT NULL,
            `failureMessage` TEXT NULL,
            `defaultInterval` TINYINT(3) UNSIGNED NULL DEFAULT '3',
            `minInterval` TINYINT(3) UNSIGNED NULL DEFAULT '1',
            `maxInterval` TINYINT(3) UNSIGNED NULL DEFAULT '7',
            PRIMARY KEY (`courseId`),
	        INDEX `coursePageId` (`coursePageId`)
        ) %s ENGINE=InnoDB", LS\Library::getCharsetCollate()));
    }

    $tb = $wpdb->get_var(sprintf("SHOW TABLES LIKE '{$wpdb->prefix}ls_course_posts'"));
    if(empty($tb)) {
        $wpdb->query(sprintf("CREATE TABLE {$wpdb->prefix}ls_course_posts (
            `courseId` TINYINT(5) UNSIGNED NOT NULL,
            `postId` BIGINT(20) UNSIGNED NOT NULL,
            `order` MEDIUMINT(5) UNSIGNED NOT NULL,
            PRIMARY KEY (`courseId`, `postId`)
        ) %s ENGINE=InnoDB", LS\Library::getCharsetCollate()));
    }

    $tb = $wpdb->get_var(sprintf("SHOW TABLES LIKE '{$wpdb->prefix}ls_course_activities'"));
    if(empty($tb)) {
        $wpdb->query(sprintf("CREATE TABLE {$wpdb->prefix}ls_course_activities (
            `activityId` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
            `courseId` SMALLINT(5) UNSIGNED NOT NULL,
            `consumerId` INT(10) UNSIGNED NOT NULL,
            `postId` BIGINT(20) UNSIGNED NOT NULL,
            `interval` TINYINT(2) UNSIGNED NOT NULL,
            `actionDate` DATE NOT NULL,
            `actionTimestamp` INT(10) UNSIGNED NOT NULL,
            PRIMARY KEY (`activityId`),
            INDEX `courseId` (`courseId`),
            INDEX `consumerId` (`consumerId`),
            INDEX `postId` (`postId`)
        ) %s ENGINE=InnoDB", LS\Library::getCharsetCollate()));
    }

    $tb = $wpdb->get_var(sprintf("SHOW TABLES LIKE '{$wpdb->prefix}ls_course_consumers'"));
    if(empty($tb)) {
        $wpdb->query(sprintf("CREATE TABLE {$wpdb->prefix}ls_course_consumers (
            `courseId` SMALLINT(5) UNSIGNED NOT NULL,
            `consumerId` INT(10) UNSIGNED NOT NULL,
            `startDate` INT(10) UNSIGNED NULL DEFAULT NULL,
            `endDate` INT(10) UNSIGNED NULL DEFAULT NULL,
            PRIMARY KEY (`courseId`, `consumerId`),
            INDEX `courseId` (`courseId`),
            INDEX `consumerId` (`consumerId`)
        ) %s ENGINE=InnoDB", LS\Library::getCharsetCollate()));
    }
}