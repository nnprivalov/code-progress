<?php
class PersonalChallengeAdmin extends PersonalChallenge implements \LS\ModuleAdminInterface
{
    /**
     * @internal
     */
    public function load()
    {
        parent::load();

        add_action('admin_menu', [$this, 'adminMenu']);
        add_action('admin_init', [$this, 'adminInit']);
        add_action('admin_enqueue_scripts', [$this, 'printScripts']);
        add_action('add_meta_boxes', [$this, 'challengeMetaBox']);
        add_filter('save_post', [$this, 'challengeSaveMetaBox']);
        add_action('delete_post', [$this, 'postDelete']);
        add_filter('ls_consumer_log__pech_accepted', [$this, 'consumerLog']);
        add_filter('ls_consumer_log__pech_canceled', [$this, 'consumerLog']);
        add_filter('ls_consumers_email_log_type', [$this, 'consumersEmailLogType']);
        add_action('ls_ce_dashboard', [$this, 'pechComCenterDashboard'], 40);
        add_action('ls_cron', [$this, 'lsCron'], 25);
        add_action('ls_cron_daily', [$this, 'cronJobDaily']);
    }

    /**
     * @internal
     */
    public function adminMenu()
    {
        add_submenu_page('edit.php?post_type=1x1', 'Challenge Teams', 'Challenge Teams', 'edit_pages', 'pech-teams', [$this, 'teamsPage']);
        add_submenu_page('edit.php?post_type=1x1', 'New Challenge Team', 'New Challenge Team', 'edit_pages', 'pech-add-team', [$this, 'addTeamPage']);
        add_submenu_page(null, 'Edit Challenge Team', 'Edit Challenge Team', 'edit_pages', 'pech-edit-team', [$this, 'editTeamPage']);
        add_submenu_page('edit.php?post_type=1x1', 'Challenge Participants', 'Challenge Participants', 'edit_pages', 'pech-participants', [$this, 'participantsPage']);
    }

    /**
     * @internal
     */
    public function teamsPage()
    {
        include_once __DIR__ . '/tables/teams-statistics-table.php';

        $selectedPostId = $this->getParam('postId', 'id');
        $selectedCompanyId = $this->getParam('companyId', 'id');

        (new LS\Template(__DIR__ . '/views/backend'))
            ->assign('tablePechTeamsStatistics', new pechTeamsStatisticsTableLoyaltySuite())
            ->assign('pechList', PechHelper::getChallengesList())
            ->assign('companiesList', LSCompaniesHelper::getCompaniesList(true, true))
            ->assign('selectedPostId', $selectedPostId)
            ->assign('selectedCompanyId', $selectedCompanyId)
            ->render('admin-teams-statistics.phtml');
    }

    /**
     * @internal
     */
    public function addTeamPage()
    {
        (new LS\Template(__DIR__ . '/views/backend'))
            ->assign('challengesList', PechHelper::getChallengesList())
            ->render('admin-new-team.phtml');
    }

    /**
     * @internal
     */
    public function editTeamPage()
    {
        $teamId = $this->getParam('teamId', 'id');
        $team = PechHelper::getTeam($teamId);

        if(!$team) {
            echo '<p>Team not found</p>';
            return;
        }

        (new LS\Template(__DIR__ . '/views/backend'))
            ->assign('challengesList', PechHelper::getChallengesList())
            ->assign('team', $team)
            ->render('admin-edit-team.phtml');
    }

    /**
     * @internal
     */
    public function participantsPage()
    {
        include_once __DIR__ . '/tables/pcp-statistics-table.php';

        $selectedTeamId = $this->getParam('teamId', 'id');
        $selectedPostId = $this->getParam('postId', 'id');
        $selectedCompanyId = $this->getParam('companyId', 'id');

        (new LS\Template(__DIR__ . '/views/backend'))
            ->assign('tablePechPcpStatistics', new pechPcpStatisticsTableLoyaltySuite())
            ->assign('pechList', PechHelper::getChallengesList())
            ->assign('companiesList', LSCompaniesHelper::getCompaniesList(true, true))
            ->assign('selectedPostId', $selectedPostId)
            ->assign('selectedCompanyId', $selectedCompanyId)
            ->assign('selectedTeamId', $selectedTeamId)
            ->render('admin-pcp-statistics.phtml');
    }

    /**
     * @return \LS\FieldAbstract[]
     */
    private static function getChallengeFields()
    {
        $pushAfter = '<div style="clear: both; margin-left: 200px;">* Use field {recipient-name} to display the author/creator name<br />* Use field {challenge-name} to display challenge name as link to the page</div>';
        $inviteAfter = '<div style="clear: both; margin-left: 200px;">* Use field {recipient-name} to display the author/creator name<br />* Use field {author-name} to display the author/creator name<br />* Use field {challenge-name} to display the challenge name as link to the page<br />* Use field {start-date} to display start date of the challenge<br />* Use field {challenge-link} to display a link to the challenge<br />* Use field {activity-days} to display a number of activity days</div>';
        $eopAfter = '<div style="clear: both; margin-left: 200px;">* Use field {recipient-name} to display the recipient name<br />* Use field {challenge-name} to display the challenge name as link to the page<br />* Use field {rank} to display your rank</div>';

        return [
            'startDate'              => new LS\Field\Date('challengeStartDate', 'Start date'),
            'endDate'                => new LS\Field\Date('challengeEndDate', 'End date'),
            'duration'               => new LS\Field\Value('challengeDuration', 'Duration time', ['minValue' => 1, 'maxValue' => 100, 'after' => 'Tag(e). Gesamte Laufzeit, inkl. Puffer nach Challengeende']),
            'activityDays'           => new LS\Field\Value('challengeActivityDays', 'Activity days', ['minValue' => 0, 'maxValue' => 100, 'after' => 'Tag(e)']),
            'question'               => new LS\Field\Text('challengeQuestion', 'Question', ['maxlength' => 1000]),
            'type'                   => new LS\Field\Select('challengeType', 'Challenge type', PechChallenge::getTypes(), ['showEmpty' => false]),
            'maxValue'               => new \LS\Field\Value('maxValue', 'Max. value'),
            'icon'                   => new LS\Field\Text('challengeIcon', 'Icon'),
            'freeEntrance'           => new LS\Field\Checkbox('freeEntrance', 'Free entrance', ['description' => 'Show all available teams and let consumer to join them']),
            'singlePcp'              => new LS\Field\Checkbox('singlePcp', 'Single participation', ['description' => 'Consumer can join on the one team within the challenge']),
            'consumerCanCreate'      => new LS\Field\Checkbox('consumerCanCreate', 'Consumers can create teams', ['description' => 'Allow consumers to create teams. Otherwise only the logged-in users will be able to create teams.']),
            'homepageChallenge'      => new LS\Field\Checkbox('homepageChallenge', 'Show as Challenge des Monats', ['description' => 'Allow to display this challenge on the homepage']),
            'enablePushIt'           => new LS\Field\Checkbox('challengeEnablePushIt', 'Push It', ['description' => 'Allow to push participants']),
            'pushItMessage'          => new LS\Field\Textarea('challengePushItMessage', 'Push It Message', ['maxlength' => 1000, 'after' => $pushAfter]),
            'invitationEmailSubject' => new LS\Field\Text('invitationEmailSubject', 'E-Mail an den Empfänger - Titel', ['maxlength' => 500]),
            'invitationEmailMessage' => new LS\Field\Textarea('invitationEmailMessage', 'E-Mail an den Empfänger - Inhalt', ['richEditor' => true, 'tinymce' => false, 'after' => $inviteAfter, 'height' => 220]),
            'startEmail'             => new LS\Field\Checkbox('startEmail', 'Send start E-Mail'),
            'startEmailSubject'      => new LS\Field\Text('startEmailSubject', 'Start E-Mail Titel', ['maxlength' => 1000]),
            'startEmailMessage'      => new LS\Field\Textarea('startEmailMessage', 'Start E-Mail Inhalt', ['richEditor' => true, 'tinymce' => false, 'height' => 250, 'after' => $eopAfter]),
            'endingEmail'            => new LS\Field\Checkbox('endingEmail', 'Send ending E-Mail'),
            'endingEmailSubject'     => new LS\Field\Text('endingEmailSubject', 'Ending E-Mail Titel', ['maxlength' => 1000]),
            'endingEmailMessage'     => new LS\Field\Textarea('endingEmailMessage', 'Ending E-Mail Inhalt', ['richEditor' => true, 'tinymce' => false, 'height' => 250, 'after' => $eopAfter])
        ];
    }

    /**
     * @param WP_Post $post
     * @return \LS\FieldAbstract[]
     */
    private function getChallengeMetaFields($post)
    {
        $fields = $this->getChallengeFields();
        $data = (new PechChallenge($post))->getChallengeMetaFieldsData();

        /** @var LS\FieldAbstract $field */
        foreach($fields as $key => $field) {
            $field->setValue($data[$key]);
        }

        return $fields;
    }

    /**
     * @internal
     */
    public function initShortcodes()
    {
        parent::initShortcodes();

        LS()->addShortcodeInfo('personal-challenge', 'Challenge', 'Anzeige des Challenge', self::getName(), ['id' => 'Challenge ID']);
        LS()->addShortcodeInfo('personal-challenge-title', 'Challenge: Title', 'Anzeige des Titels des Challenge', self::getName(), ['id' => 'Challenge ID']);
        LS()->addShortcodeInfo('personal-challenge-period', 'Challenge: Display Challenge work period', '', self::getName());
        LS()->addShortcodeInfo('personal-challenge-days', 'Challenge: Display Challenge activity days', '', self::getName());
        LS()->addShortcodeInfo('personal-challenge-alert-box', 'Challenge: Display homepage challenge', '', self::getName());
        LS()->addShortcodeInfo('personal-challenge-leaderboard', 'Challenge Leaderboard', 'Shows Challenge Leaderboard', self::getName(), ['id' => 'Challenge ID', 'company_based' => '"1" to show a teams from the company of the current consumer', 'limit' => 'Number of TOP-teams to be displayed in the leaderboard']);
        LS()->addShortcodeInfo('personal-challenges-overview', 'Challenge: Display Challenges overview', '', self::getName());
    }

    /**
     * @internal
     */
    public function printScripts()
    {
        wp_enqueue_style('admin-pech', $this->getUrl2Module() . 'css/admin-challenge.css', [], $this->getVersion());
    }

    /**
     * @internal
     */
    public function adminInit()
    {
        if(!$this->hasAction() || !current_user_can('read')) {
            return;
        }

        if($this->isAction('pech-new-team') || $this->isAction('pech-edit-team')) {

            $new = $this->isAction('pech-new-team');
            $data = [
                'challengeId' => $this->getParam('challengeId', 'id'),
                'startDate'   => $this->getParam('startDate', 'date'),
                'teamName'    => $this->getParam('teamName', 'text')
            ];

            $challenge = new PechChallenge($data['challengeId']);
            if(!$challenge) {
                $this->setError('Incorrect challenge selected');
                return;
            }

            if(empty($data['startDate'])) {
                $this->setError('Incorrect challenge start date');
                return;
            }

            if(empty($data['teamName'])) {
                $this->setError('Team name is empty');
                return;
            }

            if($new) {

                $team = new PechTeam([
                    'postId'     => $challenge->getChallengeId(),
                    'startDate'  => $data['startDate'],
                    'teamName'   => $data['teamName'],
                    'createDate' => time()
                ]);

                if(!$team->valid()) {
                    $this->setError('Das Team konnte nicht erstellt werden');
                    return;
                }

                $teamId = PechHelper::createTeam($team);

                $this->setAlert('Team created');
                \LS\Library::redirect(admin_url('edit.php?post_type=1x1&page=pech-teams'));
                LS()->logUserAction(sprintf('Personal Challenge: team created (ID: %s)', $teamId));

            } else {

                $teamId = $this->getParam('teamId', 'id');
                $team = PechHelper::getTeam($teamId);

                if(!$team) {
                    $this->setError('Das Team konnte nicht bearbeitet werden');
                    return;
                }

                $team->setFromArray([
                    'postId'     => $challenge->getChallengeId(),
                    'startDate'  => $data['startDate'],
                    'teamName'   => $data['teamName']
                ]);

                if(!$team->valid()) {
                    $this->setError('Das Team konnte nicht bearbeitet werden');
                    return;
                }

                PechHelper::updateTeam($team);

                $this->setAlert('Team updated');
                \LS\Library::redirect(remove_query_arg('action'));
                LS()->logUserAction(sprintf('Personal Challenge: team updated (ID: %s)', $teamId));
            }
        }

        if($this->isAction('pech-delete-team')) {

            $teamIds = $this->getParam('teamId', 'int_array');
            if(!empty($teamIds)) {

                foreach($teamIds as $teamId) {
                    $team = PechHelper::getTeam($teamId);
                    if($team) {
                        PechHelper::deleteTeam($team);
                    }
                }

                $this->setAlert('Selected records have been deleted');
                LS()->logUserAction(sprintf('Personal Challenge: teams(s) deleted (ID: %s)', implode(',', $teamIds)), 'warning');
            }

            \LS\Library::redirect($this->getReferer());
        }

        if($this->isAction('pech-delete-pcp')) {

            $pcpIds = $this->getParam('pcpId', 'int_array');
            if(!empty($pcpIds)) {

                foreach($pcpIds as $pcpId) {
                    $pcp = PechHelper::getParticipation($pcpId);
                    if($pcp) {
                        PechHelper::deleteParticipation($pcp);
                    }
                }

                $this->setAlert('Selected records have been deleted');
                LS()->logUserAction(sprintf('Personal Challenge: participation(s) deleted (ID: %s)', implode(',', $pcpIds)), 'warning');
            }

            \LS\Library::redirect(remove_query_arg(['action', 'action2', 'pcpId']));
        }

        if($this->isAction('pech-consumer-rank')) {
            $consumerId = $this->getParam('consumerId', 'id');
            $challengeId = $this->getParam('challengeId', 'id');
            $challenge = new PechChallenge($challengeId);
            if($consumerId > 0 && $challenge->valid()) {
                $rank = PechHelper::getParticipantRankInChallenge($consumerId, $challenge);
                $this->setAlert('The rank is ' . $rank->getRank() . ' of ' . $rank->getTotalParticipations());
            }
        }
    }

    /**
     * @internal
     */
    public function challengeMetaBox()
    {
        add_meta_box('pechChallenge', 'Challenge options', [$this, 'challengeMetaBoxContent'], '1x1');
    }

    /**
     * @internal
     * @param WP_Post $post
     */
    public function challengeMetaBoxContent($post)
    {
        (new LS\Template(__DIR__ . '/views/backend'))
            ->assign('fields', $this->getChallengeMetaFields($post))
            ->assign('nonce', wp_create_nonce(basename(__FILE__)))
            ->render('admin-meta-box.phtml');
    }

    /**
     * @param int $postId
     */
    private function sendTestInformEmail($postId)
    {
        $challenge = new PechChallenge($postId);
        if($challenge) {

            $email = $this->getParam('testInformEmail', 'email');
            $consumerId = $this->getParam('testInformConsumerId', 'id');

            $consumer = \LS\ConsumersHelper::getConsumer($consumerId);
            if(!$consumer) {
                $this->setError('Can not find consumer with specified ID');
            } elseif(empty($email)) {
                $this->setError('Bitte E-Mail Adresse eingeben um eine Test E-Mail zu erhalten');
            } else {

                $emailSubject = $challenge->getInvitationEmailSubject();
                $emailMessage = $challenge->getInvitationEmailMessage($consumer->getConsumerId(), time());

                if(!empty($emailMessage) && !empty($emailSubject)) {
                    if (PechHelper::sendComCenterEmail($email, $emailSubject, $emailMessage, $consumer)) {
                        $this->setAlert('E-Mail wurde gesendet');
                    } else {
                        $this->setError('Die E-Mail konnte nicht gesendet werden. Bitte prüfst die eingegebenen Daten.');
                    }
                }
            }
        }
    }

    /**
     * @param int $postId
     */
    private function sendTestStartEmail($postId)
    {
        $challenge = new PechChallenge($postId);
        if($challenge) {

            $email = $this->getParam('testStartEmail', 'email');
            $consumerId = $this->getParam('testStartEmailConsumerId', 'id');

            $consumer = \LS\ConsumersHelper::getConsumer($consumerId);
            if(!$consumer) {
                $this->setError('Can not find consumer with specified ID');
            } elseif(empty($email)) {
                $this->setError('Bitte E-Mail Adresse eingeben um eine Test E-Mail zu erhalten');
            } else {

                $emailSubject = $challenge->getStartEmailSubject();
                $emailMessage = $challenge->getStartEmailMessage($consumerId);

                if (PechHelper::sendComCenterEmail($email, $emailSubject, $emailMessage, $consumer)) {
                    $this->setAlert('E-Mail wurde gesendet');
                } else {
                    $this->setError('Die E-Mail konnte nicht gesendet werden. Bitte prüfst die eingegebenen Daten.');
                }
            }
        }
    }

    /**
     * @param int $postId
     */
    private function sendTestEndingEmail($postId)
    {
        $challenge = new PechChallenge($postId);
        if($challenge) {

            $email = $this->getParam('testEndingEmail', 'email');
            $consumerId = $this->getParam('testEndingEmailConsumerId', 'id');

            $consumer = \LS\ConsumersHelper::getConsumer($consumerId);
            if(!$consumer) {
                $this->setError('Can not find consumer with specified ID');
            } elseif(empty($email)) {
                $this->setError('Bitte E-Mail Adresse eingeben um eine Test E-Mail zu erhalten');
            } else {

                $emailSubject = $challenge->getEndingEmailSubject();
                $emailMessage = $challenge->getEndingEmailMessage($consumerId, new PechRank(1, 99));

                if (PechHelper::sendComCenterEmail($email, $emailSubject, $emailMessage, $consumer)) {
                    $this->setAlert('E-Mail wurde gesendet');
                } else {
                    $this->setError('Die E-Mail konnte nicht gesendet werden. Bitte prüfst die eingegebenen Daten.');
                }
            }
        }
    }

    /**
     * @internal
     * @param int $postId
     */
    public function challengeSaveMetaBox($postId)
    {
        if(defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
            return;
        }

        if(!isset($_POST['pech-challenge-nonce']) || !wp_verify_nonce($_POST['pech-challenge-nonce'], basename(__FILE__))) {
            return;
        }

        $post = get_post($postId);
        $postType = get_post_type_object($post->post_type);
        if(in_array($post->post_status, ['inherit', 'trash']) || !$postType || !current_user_can($postType->cap->edit_post, $postId)) {
            return;
        }

        $challenge = new PechChallenge($postId);

        $data = [];

        foreach($this->getChallengeFields() as $key => $field) {
            $field->setValue($this->getParam($field->getName(), $field->getType()));
            if($field->isValid()) {
                $data[$key] = $field->getValue();
            }
        }

        if($data['activityDays'] > $data['duration']) {
            $data['activityDays'] = $data['duration'];
        }

        // if marked as homepage challenge -> deactivate this mark from all the other challenges
        if($data['homepageChallenge']) {
            foreach(PechHelper::getChallenges() as $ch) {
                if($ch->getChallengeId() !== $postId && $ch->canShowOnHomepage()) {
                    $ch->setChallengeMetaFieldsData(['homepageChallenge' => false]);
                }
            }
        }

        $challenge->setChallengeMetaFieldsData($data);

        wp_cache_delete('pech_challenge_alert_box', 'fitco');

        if(isset($_POST['sendTestInformEmail'])) {
            $this->sendTestInformEmail($postId);
        } elseif(isset($_POST['sendTestStartEmail'])) {
            $this->sendTestStartEmail($postId);
        } elseif(isset($_POST['sendTestEndingEmail'])) {
            $this->sendTestEndingEmail($postId);
        }
    }

    /**
     * @internal
     * @param int $postId
     */
    public function postDelete($postId)
    {
        PechHelper::deleteChallenge($postId);
    }

    /**
     * @internal
     * @param object $log
     * @return string
     */
    public function consumerLog($log)
    {
        $event = '';

        if($log->messageType == 'pech_accepted') {
            $event = "Akzeptierte Challenge";
        } elseif($log->messageType == 'pech_canceled') {
            $event = "Abgesagte Challenge";
        }

        if(isset($log->note)) {
            $teamId = (int) $log->note;
            $team = PechHelper::getTeam($teamId);
            if($team) {
                $event .= ' <a href="' . get_edit_post_link($team->getChallengeId()) . '" target="_blank">' . get_the_title($team->getChallengeId()) . '</a>';
            }
        }

        return $event;
    }

    /**
     * @internal
     * @param string $type
     * @return string
     */
    public function consumersEmailLogType($type)
    {
        switch($type) {
            case 'fitco_pech_start_email' :
                return 'Challenge Start E-Mail';
            case 'fitco_pech_ending_email' :
                return 'Challenge Abschluss E-Mail';
            case 'fitco_pech' :
                return '1-1 Challenge Teilnehmereinladung';
            default:
                return $type;
        }
    }

    /**
     * @internal
     */
    public function pechComCenterDashboard()
    {
        $pushes = PechHelper::countPushes();
        ?>
        <div class="bx size-1">
            <div>
                <span><?=$pushes?></span>
                <label>Push-it Notifications (Antwort offen)</label>
            </div>
        </div>
        <?php
    }

    /**
     * @internal
     */
    public function lsCron()
    {
        if(self::lock('fitco.pech')) {

            $this->startEmailCron();
            $this->endingEmailCron();

            self::unlock('fitco.pech');
        }
    }

    /**
     * @internal
     */
    public function cronJobDaily()
    {
        if(self::lock('fitco.pech')) {
            $this->unpublishChallengeCron();
            self::unlock('fitco.pech');
        }
    }

    private function startEmailCron()
    {
        // send after 06:00
        if(date_i18n('H') >= 6) {

            // send only once a day
            $lastSent = $this->getOption('lastStartEmail', 0);
            if($lastSent == 0 || date('Ymd', $lastSent) != date('Ymd')) {

                foreach(PechHelper::getTeamsToSendStartEmail() as $myTeam) {

                    $subject = $myTeam->getChallenge()->getStartEmailSubject();
                    $message = $myTeam->getChallenge()->getStartEmailMessage($myTeam->getConsumerId());
                    $consumer = $myTeam->getParticipation()->getConsumer();

                    if(!empty($subject) && !empty($message) && $consumer && !empty($consumer->getEmail())) {
                        $sent = PechHelper::sendComCenterEmail($consumer->getEmail(), $subject, $message, $consumer);
                        \LS\ConsumersHelper::saveEmailLog($subject, $myTeam->getConsumerId(), 'fitco_pech_start_email', $sent);
                    }
                }
            }

            $this->setOption('lastStartEmail', time());
        }
    }

    private function endingEmailCron()
    {
        // send after 06:00
        if(date_i18n('H') >= 6) {

            // send only once a day
            $lastSent = $this->getOption('lastEndingEmail', 0);
            if($lastSent == 0 || date('Ymd', $lastSent) != date('Ymd')) {

                foreach(PechHelper::getTeamsToSendEndingEmail() as $myTeam) {

                    $subject = $myTeam->getChallenge()->getEndingEmailSubject();
                    $message = $myTeam->getChallenge()->getEndingEmailMessage($myTeam->getConsumerId(), PechHelper::getParticipantRank($myTeam));
                    $consumer = $myTeam->getParticipation()->getConsumer();

                    if(!empty($subject) && !empty($message) && $consumer && !empty($consumer->getEmail())) {
                        $sent = PechHelper::sendComCenterEmail($consumer->getEmail(), $subject, $message, $consumer);
                        \LS\ConsumersHelper::saveEmailLog($subject, $myTeam->getConsumerId(), 'fitco_pech_ending_email', $sent);
                    }
                }
            }

            $this->setOption('lastEndingEmail', time());
        }
    }

    /**
     * Unpublish pages depends on challenge end date
     */
    private function unpublishChallengeCron()
    {
        foreach(PechHelper::getEndedChallenges() as $challenge) {

            $post = $challenge->getPost();
            if($post && $post->post_status == 'publish') {
                wp_update_post([
                    'ID'          => $challenge->getChallengeId(),
                    'post_status' => 'draft'
                ]);
            }

            foreach(PechHelper::getTeams($challenge->getChallengeId()) as $team) {
                foreach(PechHelper::getTeamParticipations($team) as $pcp) {
                    do_action('pech_ended', $pcp->getConsumerId(), $pcp);
                }
            }
        }
    }
}