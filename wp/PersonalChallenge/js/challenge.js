jQuery(document).ready(function($) {

    var pechOverview = $('.pech-overview-details'),
        pechCreate = $('.pech-create'),
        focusOnOpen = true;

    if(pechOverview.length || pechCreate.length) {

        var stepsXHR,
            recalculateBlock = false,
            alertMsg = true,
            CH_TYPE_BOOL = 2,
            ANSWER_YES = 2;

        var validParticipants = function(form, pechId, teamId, successCallback, failCallback) {

            var errorsBlock = form.find('.pech-participants-error'),
                participants = form.find('.pech-pcp'),
                emailsList = [],
                xhr = false;

            successCallback = successCallback || function() {
            };
            failCallback = failCallback || function() {
            };

            participants
                .removeClass('mistake')
                .find('.error')
                .remove();

            errorsBlock.empty();

            participants.each(function() {

                var field = $(this),
                    email = field.find('.pech-field').val();

                emailsList.push(email);
            });

            if(!emailsList.length) {
                if(teamId > 0) {
                    errorsBlock.append('<p>' + PECH.emailMissed + '</p>');
                    participants.last().addClass('mistake');
                    failCallback();
                } else {
                    successCallback();
                }
            } else {
                xhr = $.post(LS.ajaxurl, {
                    action: 'pechCheckEmails',
                    pechId: pechId,
                    id: teamId,
                    emails: emailsList
                }).always(function(response) {
                    if(response) {
                        if(response.hasOwnProperty('general') && response.general.length) {
                            errorsBlock.append('<p>' + response.general + '</p>');
                            failCallback();
                        } else if(!$.isEmptyObject(response.participants)) {
                            for(var e in response.participants) {
                                participants.each(function() {

                                    var field = $(this),
                                        email = $.trim(field.find('.pech-field').val()),
                                        isMultiple = email.indexOf(',') !== -1 || email.indexOf(';') !== -1;

                                    if(email.indexOf(e) !== -1 && response.participants.hasOwnProperty(e)) {
                                        field
                                            .addClass('mistake')
                                            .append('<p class="error">' + (isMultiple ? e + ' - ' : '') + response.participants[e] + '</p>');
                                    }
                                });
                            }
                            failCallback();
                        } else {
                            successCallback();
                        }
                    }
                });
            }

            return xhr;
        };

        var submitCreateForm = function(form) {

            var teamField = form.find('.pech-team-name'),
                hasTeamName = form.find('.pech-team-switcher').is(':checked'),
                teamName = $.trim(teamField.val()),
                pechId = form.find('input[name="pechId"]').val(),
                teamError = pechCreate.find('.team-error'),
                tnSuccess = !hasTeamName,
                mSuccess = false;

            if(form.attr('checked') === undefined) {

                form.find('.mistake').removeClass('mistake');
                teamError.empty();

                if(hasTeamName && !teamName.length) {
                    teamError.html(PECH.teamNameMissed);
                    teamField.addClass('mistake');
                } else {

                    var btn = form.find('input[type=submit]');
                    btn.hide().after('<div class="pech-loading"></div>');

                    var check = function() {
                        if(tnSuccess && mSuccess) {
                            form.attr('checked', 1).submit();
                        }
                    };

                    var mXhr = validParticipants(form, pechId, 0, function() {
                        mSuccess = true;
                        check();
                    });

                    var tnXhr = false;

                    if(hasTeamName) {
                        if(!teamName.length) {
                            teamError.html(PECH.teamNameMissed);
                            teamField.addClass('mistake');
                        } else {
                            tnXhr = $.post(LS.ajaxurl, {
                                action: 'pechCheckTeam',
                                teamName: teamName
                            }).always(function(response) {
                                if(response && response.length) {
                                    teamError = pechCreate.find('.team-error');
                                    teamError.html(response);
                                    teamField.addClass('mistake');
                                } else {
                                    tnSuccess = true;
                                    check();
                                }
                            });
                        }

                    } else {
                        tnXhr = true;
                    }

                    $.when(mXhr, tnXhr).then(function() {
                        if(form.attr('checked') === undefined) {
                            form.find('.pech-loading').remove();
                            btn.show();
                        }
                    });
                }

                return false;
            }

            return true;
        };

        var onlyUnique = function(value, index, self) {
            return self.indexOf(value) === index;
        };

        var limitPcpsView = function(block) {

            var pcps = block.find('.pech-participations li'),
                totalPcps = pcps.length,
                myPos = pcps.filter('.current').index() + 1,
                visibleKeys = [myPos],
                firstPcpsToDisplay = 3,
                aboveMeToDisplay = 0,
                belowMeToDisplay = 0,
                minPcpsToCollapse = 1 + firstPcpsToDisplay + aboveMeToDisplay + belowMeToDisplay;

            // collapse only if enough participants
            if(minPcpsToCollapse >= totalPcps) {
                return;
            }

            // always add fist {firstPcpsToDisplay} items to the visible list
            for (var i = 1; i <= firstPcpsToDisplay; i++) {
                visibleKeys.push(i);
            }

            // display {aboveMeToDisplay} above me
            if (aboveMeToDisplay > 0) {
                for(i = 1; i <= aboveMeToDisplay; i++) {
                    if(myPos - i > 0) {
                        visibleKeys.push(myPos - i);
                    }
                }
            }

            // display {belowMeToDisplay} above me
            if (belowMeToDisplay > 0) {
                for(i = 1; i <= belowMeToDisplay; i++) {
                    if(myPos + i <= totalPcps) {
                        visibleKeys.push(myPos + i);
                    }
                }
            }

            // don't display --(1) between top and your pos
            if (aboveMeToDisplay > 0 && (myPos - 1) === firstPcpsToDisplay) {
                visibleKeys.push(myPos - 1);
            }

            // don't display --(1) below your pos
            if (belowMeToDisplay > 0 && (myPos + 1) === totalPcps) {
                visibleKeys.push(myPos + 1);
            }

            // don't display --(1) at the bottom
            if (belowMeToDisplay > 0 && has(visibleKeys, totalPcps - 1)) {
                visibleKeys.push(totalPcps);
            }

            visibleKeys = visibleKeys.filter(onlyUnique).sort(function(a,b) {
                return a - b;
            });

            pcps
                .removeClass('last-visible pa-hide')
                .removeAttr('data-gap');

            if(totalPcps >= visibleKeys.length) {
                pcps.each(function(pos) {
                    if(!has(visibleKeys, pos + 1)) {

                        $(this).addClass('pa-hide').slideUp();

                        if(!pcps.eq(pos - 1).hasClass('pa-hide')) {
                            var nextVisiblePos = typeof visibleKeys[pos] === "undefined" ? pcps.length + 1 : visibleKeys[pos];
                            pcps.eq(pos - 1).addClass('last-visible').attr('data-gap', Math.max(1, nextVisiblePos - pos - 1));
                        }

                    } else {
                        $(this).slideDown();
                    }
                });
            }
        };

        var showContent = function(block) {

            limitPcpsView(block);
            initActivityBlock(block);

            block.addClass('open');
            block.find('.details-button').show();
            block.find('.pech-details-data').show();
            block.find('.pech-details-container').hide().slideDown(700);

            // newly added pcp notification
            if(window.location.hash.indexOf('/pcp-added') > 0) {
                block.find('.pech-added-notice').show();
                window.location.hash = window.location.hash.replace('/pcp-added', '');
            }

            if(focusOnOpen) {
                $('body,html').stop(false, false).animate({
                    scrollTop: block.offset().top - 10
                }, 700);
            } else {
                focusOnOpen = true;
            }
        };

        var formatDate = function(date) {

            var day = date.getDate(),
                month = date.getMonth() + 1,
                year = date.getFullYear();

            return ((day > 9 ? '' : '0') + day) + '.' + ((month > 9 ? '' : '0') + month) + '.' + year;
        };

        var isBooleanChallenge = function(block) {
            return block.data('type') === CH_TYPE_BOOL;
        };

        var getCurrentValue = function(block) {
            var val = block.find('.pech-my').val();
            return val === '' ? '' : parseInt(val, 10);
        };

        var setCurrentValue = function(block, val) {
            val = val === '' ? '' : parseInt(val, 10);
            block.find('.pech-my').val(val);
        };

        var moveActivityBlock = function(block, direction) {

            var DAY = 86400 * 1000,
                inner = block.find('.pech-ma-inner'),
                current = block.find('.pech-ma-today'),
                prev = block.find('.pech-ma-prev'),
                next = block.find('.pech-ma-next'),
                todayDate = new Date(current.attr('data-day') * 1000),
                range = [new Date(prev.data('limit') * 1000), new Date(next.data('limit') * 1000)],
                newDate = new Date(todayDate.getTime() + direction * DAY);

            inner.fadeOut('fast', function() {

                setCurrentValue(block, block.find('.pech-step[data-day="' + formatDate(newDate) + '"]').val());

                current
                    .attr('data-day', newDate.getTime() / 1000)
                    .html(formatDate(newDate));

                prev.setVisible(newDate > range[0]);
                next.setVisible(range[1] > newDate);

                updateActivityView(block);

                inner.fadeIn('fast');
            });
        };

        var initActivityBlock = function(block) {
            moveActivityBlock(block, 0);
        };

        var recalculateSteps = function(block) {

            if(stepsXHR) {
                stepsXHR.abort();
            }

            var myBar = block.find('.pech-participations .current .pech-steps-bar'),
                isBoolType = isBooleanChallenge(block),
                teamId = parseInt(block.find('input.pech-team-id').val(), 10),
                aDays = parseInt(block.find('input.pech-days').val(), 10),
                myPoints = 0,
                maxPoints = 0;

            var steps = block.find('.pech-step').map(function() {
                var val = $(this).val();
                val = val === '' ? '' : parseInt(val, 10);
                if(val > 0) {
                    myPoints += isBoolType ? (val === ANSWER_YES ? 1 : 0) : val;
                }
                return val;
            }).get();

            myBar.attr('data-value', myPoints);

            if(isBoolType) {

                maxPoints = aDays;

                myBar.find('.step-value').html(Math.ceil(myPoints / aDays * 100) + '%');

            } else {

                maxPoints = myPoints;

                myBar.find('.step-value').html(myPoints);

                block.find('.pech-steps-bar').each(function() {
                    var pt = parseInt($(this).attr('data-value'), 10);
                    if(pt > maxPoints) {
                        maxPoints = pt;
                    }
                });

                block.find('.pech-participations li:not(.current)').each(function() {
                    $(this).find('.step-width').animate({
                        width: Math.ceil($(this).find('.pech-steps-bar').attr('data-value') / maxPoints * 100) + '%'
                    }, 700);
                });
            }

            var pcps = block.find('.pech-participations'),
                lis = pcps.children('li');

            lis.detach().sort(function(a, b) {

                var aw = $(a).find('.pech-steps-bar').attr('data-value'),
                    bw = $(b).find('.pech-steps-bar').attr('data-value');

                if(aw === bw) {
                    return $(a).hasClass('current') ? -1 : ($(b).hasClass('current') ? 1 : 0);
                }

                return bw - aw;
            });

            pcps.append(lis);

            pcps.find('li').each(function(k) {
                $(this).find('.name span').remove();
                if(k > 2) {
                    $(this).find('.name').prepend('<span>' + (k + 1) + '.</span>');
                }
            });

            limitPcpsView(block);

            myBar.find('.step-width').animate({
                width: Math.ceil(myPoints / maxPoints * 100) + '%'
            }, 700);

            block.find('.pech-my-rank').html(pcps.find('li.current').index() + 1);

            stepsXHR = $.post(LS.ajaxurl, {
                action: 'pechActivities',
                id: teamId,
                steps: steps
            }).always(function(response) {
                if(response.length && alertMsg) {
                    alert(response);
                    alertMsg = false;
                }
            });

            return true;
        };

        var updateActivityView = function(block) {

            var ma = block.find('.pech-ma');

            ma.removeClass('action-bg done-bg');

            if(getCurrentValue(block) !== '') {
                ma.addClass('done-bg');
            } else {
                ma.addClass('action-bg');
            }
        };

        var checkMaxValue = function(block) {

            var input = block.find('.pech-my'),
                maxValue = parseInt(input.data('max'), 10);

            if(maxValue > 0 && input.val() > maxValue) {
                input.val(maxValue);
            }
        };

        var changeCurrentActivity = function(block) {

            var val = getCurrentValue(block),
                my = block.find('.pech-my'),
                title = isBooleanChallenge(block) ? my.find('option:selected').html() : val,
                dayTs = block.find('.pech-ma-today').attr('data-day'),
                day = formatDate(new Date(dayTs * 1000)),
                status = block.find('.pech-step-status[data-day="' + day + '"]'),
                cls = getCurrentValue(block) === '' ? 'action-bg' : 'done-bg';

            status.removeClass('action-bg done-bg').addClass(cls).html(title === '' ? '--' : title);

            block.find('.pech-step[data-day="' + day + '"]').val(val);

            recalculateBlock = block;

            updateActivityView(block);
        };

        var showRestParticipants = function(link) {

            var block = link.closest('.pech-block');
            if(link.hasClass('open')) {
                block.find('.pech-rpc-list').slideUp();
                link.html('anzeigen').removeClass('open');
            } else {
                block.find('.pech-rpc-list').slideDown();
                link.html('ausblenden').addClass('open');
            }

            link.blur();
        };

        setInterval(function() {
            if(recalculateBlock.length) {
                if(recalculateSteps(recalculateBlock)) {
                    recalculateBlock = false;
                }
            }
        }, 500);

        pechOverview.find('.details-button').click(function() {

            var button = $(this),
                block = button.closest('.pech-block');

            if(block.hasClass('open')) {

                var dataBlock = block.find('.pech-details-data'),
                    container = dataBlock.find('.pech-details-container');

                container.slideUp(500, function() {
                    block.removeClass('open');
                    dataBlock.hide();
                });

            } else {
                if(!block.hasClass('loaded')) {

                    button
                        .hide()
                        .parent()
                        .append('<div class="pech-loading"></div>');

                    $.post(LS.ajaxurl, {
                        action: 'pechDetails',
                        id: block.data('id')
                    }).always(function(response) {

                        block
                            .find('.pech-details-container')
                            .html(response.length > 1 ? response : PECH.loadingFailed);

                        block
                            .find('.pech-loading')
                            .remove();

                        block.addClass('loaded');

                        showContent(block);
                    });

                } else {
                    showContent(block);
                }
            }
        });

        $('.pech-block')
            .on('change', '.pech-select-push', function() {
                $('.push-it').show().next('span').remove();
            })
            .on('click', '.push-it', function() {

                var button = $(this),
                    container = button.closest('.pech-block'),
                    loading = container.find('.push-it-loading');

                button.hide();
                loading.css('display', 'inline-block');

                $.post(LS.ajaxurl, {
                    action: 'pechPushIt',
                    id: container.data('id'),
                    cid: container.find('.pech-select-push').val()
                }).always(function(response) {

                    loading.hide();

                    if(response.length) {
                        button.after('<span class="danger-color">' + response + '</span>');
                    } else {
                        button.after('<span class="done-color">' + PECH.pushSent + '</span>');
                    }
                });

                return false;
            })
            .on('click', '.pech-remove', function() {

                var block = $(this).closest('.pech-block');

                $.post(LS.ajaxurl, {
                    action: 'pechHide',
                    id: parseInt(block.data('id'), 10)
                });

                setTimeout(function() {
                    block.parent('li').fadeOut('slow');
                }, 500);

                return false;

            })
            .on('submit', '.participation-nm', function() {

                var form = $(this).closest('form'),
                    pechId = form.closest('.pech-block').data('pech-id'),
                    id = form.closest('.pech-block').data('id');

                if(form.attr('checked') === undefined) {

                    var btn = form.find('input[type=submit]');
                    btn.hide().after('<div class="pech-loading"></div>');

                    validParticipants(form, pechId, id, function() {
                        form.attr('checked', 1).submit();
                    }, function() {
                        form.find('.pech-loading').remove();
                        btn.show();
                    });

                    return false;
                }
            })
            .on('change paste', '.pech-my', function() {
                checkMaxValue($(this).closest('.pech-block'));
            })
            .on('keyup change paste', '.pech-my', function() {
                changeCurrentActivity($(this).closest('.pech-block'));
            })
            .on('click', '.pech-ma-prev', function() {
                moveActivityBlock($(this).closest('.pech-block'), -1);
                return false;
            })
            .on('click', '.pech-ma-next', function() {
                moveActivityBlock($(this).closest('.pech-block'), 1);
                return false;
            })
            .on('click', '.pech-rpc-link', function() {
                showRestParticipants($(this));
                return false;
            });

        var addParticipant = function(participant) {

            var mContainer = participant.closest('.pech-participants');
            if(mContainer.length >= 100) {
                return false;
            }

            var firstParticipant = mContainer.find('.pech-pcp').first(),
                newParticipant = firstParticipant.clone();

            firstParticipant.find('.pech-add').addClass('hide');
            firstParticipant.find('.pech-del').removeClass('hide');

            newParticipant.removeClass('mistake');
            newParticipant.find('.error').remove();
            newParticipant.find('.pech-add').removeClass('hide');
            newParticipant.find('.pech-del').addClass('hide');
            newParticipant.find('input').val('');

            mContainer.prepend(newParticipant);
            mContainer.find('input').first().focus();
        };

        var removeParticipant = function(participant) {
            var mContainer = participant.closest('.pech-participants');
            if(mContainer.find('.pech-pcp').length > 1) {
                participant.remove();
            }
        };

        $('.pech-create, .pech-block')

            .on('click', '.pech-add', function() {

                addParticipant($(this).closest('.pech-pcp'));
                return false;

            })
            .on('click', '.pech-del', function() {

                removeParticipant($(this).closest('.pech-pcp'));
                return false;

            })
            .on('keyup paste', '.pech-participants', function() {

                $(this).closest('.pech-participants').find('.mistake').removeClass('mistake');
                $('.pech-participants-error').empty();
            });

        // hide team name error if team name has been changed
        pechCreate.find('.pech-team-name').on('keyup paste', function() {
            $(this).removeClass('mistake');
            pechCreate.find('.team-error').empty();
        });

        // show/hide team name
        pechCreate.find('.pech-team-switcher').on('change', function() {

            var form = $(this).closest('form'),
                input = form.find('.pech-team-name');

            form.find('.team-error').empty();

            if(input.is(':disabled')) {
                input.removeAttr('disabled');
            } else {
                input.attr('disabled', 'disabled');
            }
        });

        // submit form to create a challenge
        pechCreate.submit(function() {
            return submitCreateForm($(this).find('form'));
        });

        // jump to the challenge by hash
        if(window.location.hash.indexOf('#pech/') === 0) {

            var id = parseInt(window.location.hash.match('#pech\\/(\\d+)(w+)?')[1], 10);
            if(id > 0) {
                var pech = $('.pech-block[data-id=' + id + ']');
                if(pech.length) {

                    $('body,html').stop(false, false).animate({
                        scrollTop: pech.offset().top - 10
                    }, 700);

                    focusOnOpen = false;
                    pech.find('.details-button').click();
                }
            }

            // if only one challenge available = open it
        } else if(pechOverview.find('.pech-block').length === 1) {
            focusOnOpen = false;
            pechOverview.find('.details-button').click();
        }
    }

    // push-it window
    var fpc = $('#fitcoPushContent');
    if(fpc && fpc.length) {

        setTimeout(function() {

            $.fancybox.open(fpc, {
                margin: 10,
                minWidth: 240,
                maxWidth: 500,
                beforeShow: function() {
                    $("body").css({'overflow-y': 'hidden'});
                },
                afterClose: function() {
                    $("body").css({'overflow-y': 'visible'});
                },
                helpers: {
                    overlay: {
                        closeClick: false
                    }
                },
                beforeClose: function() {
                    $.post(LS.ajaxurl, {
                        action: 'pechPushCancel',
                        id: fpc.data('pcp-id')
                    });
                }
            });

            fpc.find('.cancel-action').click(function() {
                $.fancybox.close();
                return false;
            });

        }, 2000);
    }
});