<?php
class PechChallenge
{
    const TYPE_VALUES = 1;
    const TYPE_YES_NO = 2;

    const ANSWER_NO = 1;
    const ANSWER_YES = 2;

    /** @var WP_Post */
    private $post;

    /** @var array */
    private $meta = [];

    /**
     * @param int|WP_Post $post
     */
    public function __construct($post)
    {
        if(!empty($post)) {
            $this->post = $post instanceof WP_Post ? $post : get_post($post);
        }

        if($this->valid()) {
            $this->meta = $this->getChallengeMetaFieldsData();
        }
    }

    /**
     * @return WP_Post
     */
    public function getPost()
    {
        return $this->post;
    }

    /**
     * @return int
     */
    public function getChallengeId()
    {
        return $this->post ? $this->post->ID : 0;
    }

    /**
     * @return string
     */
    public function getChallengeLink()
    {
        return FitcoHelper::getPermalink($this->post, '/');
    }

    /**
     * @return string
     */
    public function getPostTitle()
    {
        return $this->post ? get_the_title($this->post) : '';
    }

    /**
     * @return array
     */
    private static function getFieldsDefaults()
    {
        return [
            'startDate'              => '',
            'endDate'                => '',
            'duration'               => 12,
            'activityDays'           => 10,
            'question'               => 'Trage deine heutiges Ergebnis ein!',
            'type'                   => self::TYPE_VALUES,
            'maxValue'               => 0,
            'icon'                   => 'icomoon-excercise1',
            'freeEntrance'           => false,
            'singlePcp'              => false,
            'consumerCanCreate'      => true,
            'enablePushIt'           => false,
            'pushItMessage'          => 'Hello {recipient-name}, come back to the challenge {challenge-name}.',
            'invitationEmailSubject' => 'Challenge invitation',
            'invitationEmailMessage' => 'Hello {recipient-name},<br />{author-name} invited you to join the Challenge "{challenge-name}" which starts on {start-date}."',
            'startEmail'             => true,
            'startEmailSubject'      => 'Start of Challenge',
            'startEmailMessage'      => 'Hello {recipient-name},<br />Challenge "{challenge-name}" started!',
            'endingEmail'            => true,
            'endingEmailSubject'     => 'End of challenge',
            'endingEmailMessage'     => 'Hello {recipient-name},<br />you finished the participation in challenge "{challenge-name}".',
            'homepageChallenge'      => false
        ];
    }

    /**
     * @return array
     */
    public static function getTypes()
    {
        return [
            self::TYPE_VALUES => 'Values',
            self::TYPE_YES_NO => 'Yes/No'
        ];
    }

    /**
     * @return array
     */
    public static function getIcomoonIcons()
    {
        $classes = get_transient('icomoon_icons');

        if(!is_array($classes) || empty($classes)) {

            $path = get_template_directory() . '/css/icomoon.css';
            if(file_exists($path)) {
                $css = file($path);
                $class = false;
                foreach($css as $line) {
                    if(preg_match('/^\.(icomoon\-(.*)+):before {\r?$/', $line, $matches)) {
                        if(count($matches) > 1) {
                            $class = $matches[1];
                            continue;
                        }
                    } elseif(!empty($class) && preg_match('/^\"((.*)+)\";\r?$/', trim(str_replace('content: ', '', $line)), $matches)) {
                        if(count($matches) > 1) {
                            $classes[$class] = $matches[1];
                        }
                    }

                    $class = false;
                }
            }

            set_transient('icomoon_icons', $classes, DAY_IN_SECONDS);
        }

        return $classes;
    }

    /**
     * @return array
     */
    public function getChallengeMetaFieldsData()
    {
        $result = $this->getFieldsDefaults();

        if($this->valid()) {
            $meta = get_post_meta($this->post->ID, '_challenge', true);
            if(is_array($meta)) {
                $result = array_merge($result, $meta);
            }
        }

        return $result;
    }

    /**
     * @param array $data
     * @return bool
     */
    public function setChallengeMetaFieldsData($data)
    {
        $success = false;

        if($this->valid()) {
            update_post_meta($this->post->ID, '_challenge', array_merge($this->getChallengeMetaFieldsData(), $data));
            $success = true;
        }

        return $success;
    }

    /**
     * @return int
     */
    public function getChallengeType()
    {
        return (int) ($this->meta['type'] ?? 0);
    }

    /**
     * @return int
     */
    public function getMaxValue()
    {
        return (int) ($this->meta['maxValue'] ?? 0);
    }

    /**
     * @return bool
     */
    public function isTypeYesNo()
    {
        return $this->getChallengeType() === self::TYPE_YES_NO;
    }

    /**
     * @return bool
     */
    public function hasChallengeIcon()
    {
        return isset($this->meta['icon']) && !empty($this->meta['icon']);
    }

    /**
     * @return string
     */
    public function getChallengeIcon()
    {
        return $this->hasChallengeIcon() ? $this->meta['icon'] : '';
    }

    /**
     * @return bool
     */
    public function pushEnabled()
    {
        return isset($this->meta['enablePushIt']) && $this->meta['enablePushIt'];
    }

    /**
     * @return bool
     */
    public function isFreeEntrance()
    {
        return isset($this->meta['freeEntrance']) && $this->meta['freeEntrance'];
    }

    /**
     * @return bool
     */
    public function isSingleParticipation()
    {
        return isset($this->meta['singlePcp']) && $this->meta['singlePcp'];
    }

    /**
     * @return bool
     */
    public function canConsumerCreateTeam()
    {
        return isset($this->meta['consumerCanCreate']) && $this->meta['consumerCanCreate'];
    }

    /**
     * @return int
     */
    public function getStartDate()
    {
        return isset($this->meta['startDate']) ? (int) $this->meta['startDate'] : 0;
    }

    /**
     * @return int
     */
    public function getEndDate()
    {
        return isset($this->meta['endDate']) ? (int) $this->meta['endDate'] : 0;
    }

    /**
     * @return int
     */
    public function getDuration()
    {
        return isset($this->meta['duration']) ? (int) $this->meta['duration'] : 0;
    }

    /**
     * @return int
     */
    public function getActivityDays()
    {
        return isset($this->meta['activityDays']) ? (int) $this->meta['activityDays'] : 0;
    }

    /**
     * @return bool
     */
    public function isScheduled()
    {
        return $this->getStartDate() > strtotime('TODAY');
    }

    /**
     * @return bool
     */
    public function isEnded()
    {
        return $this->getEndDate() > 0 && strtotime('TODAY') > $this->getEndDate();
    }

    /**
     * Check if possible to create/participate/join to the challenge
     *
     * @deprecated 03.2019 Use PechHelper::canCreateTeamOn()
     * @TODO Can be removed after 06.2019
     *
     * @param null|int $date Date to create a team
     * @return bool
     */
    public function canCreateTeam($date = null)
    {
        _deprecated_function(__FUNCTION__, '3.5.0', 'PechHelper::canCreateTeamOn()');

        $date = $date ?? strtotime('TODAY');

        // option that allows to create challenges by consumers is disabled
        if (!$this->canConsumerCreateTeam()) {
            return false;
        }

        // can't be in the past
        if (strtotime('TODAY') > $date) {
            return false;
        }

        // not later than 1 month after today
        if ($date > strtotime('TODAY +1 MONTH')) {
            return false;
        }

        // check if it's enough days until the end of challenge
        if ($this->getEndDate() > 0) {
            $teamLastDay = $date + ($this->getDuration() - 1) * DAY_IN_SECONDS;
            if ($teamLastDay > $this->getEndDate()) {
                return false;
            }
        }

        return true;
    }

    /**
     * @return int
     */
    public function getPossibleDaysToParticipate()
    {
        // days until the end of the challenge but not more than 31 days
        $m1 = $this->getEndDate() > 0 ? floor(($this->getEndDate() - strtotime('TODAY')) / DAY_IN_SECONDS) - ($this->getDuration() - 1) : 31;
        
        // days until the same day of the next month
        $m2 = floor((strtotime('TODAY +1 MONTH') - strtotime('TODAY')) / DAY_IN_SECONDS);

        return max(0, min($m1, $m2));
    }

    /**
     * @return string
     */
    public function getQuestion()
    {
        $question = $this->meta['question'] ?? '';
        $question = empty($question) ? 'Aktivität Überblick' : $question;

        return $question;
    }

    /**
     * @return string
     */
    public function getInvitationEmailSubject()
    {
        return $this->meta['invitationEmailSubject'] ?? '';
    }

    /**
     * @param int $recipientConsumerId
     * @param int $startDate
     * @return string
     */
    public function getInvitationEmailMessage($recipientConsumerId, $startDate)
    {
        $message = $this->meta['invitationEmailMessage'] ?? '';
        if(!empty($message)) {

            if(strpos($message, '{recipient-name}') !== false) {
                $message = str_replace('{recipient-name}', consumersLoyaltySuite::getConsumerNiceName($recipientConsumerId), $message);
            }

            if(strpos($message, '{author-name}') !== false) {
                $message = str_replace('{author-name}', consumersLoyaltySuite::getConsumerNiceName(), $message);
            }

            if(strpos($message, '{challenge-name}') !== false) {
                $message = str_replace('{challenge-name}', '<a href="' . $this->getChallengeLink() . '">' . $this->post->post_title . '</a>', $message);
            }

            if(strpos($message, '{challenge-link}') !== false) {
                $message = str_replace('{challenge-link}', $this->getChallengeLink(), $message);
            }

            if(strpos($message, '{start-date}') !== false) {
                $message = str_replace('{start-date}', date('d.m.Y', $startDate), $message);
            }

            if(strpos($message, '{activity-days}') !== false) {
                $message = str_replace('{activity-days}', $this->getActivityDays(), $message);
            }

            $message = apply_filters('ls_pech_invitation_message', $message, $recipientConsumerId);
        }

        return $message;
    }

    /**
     * @param \LS\ConsumerInterface $consumer
     * @return string
     */
    public function getPushItMessage(\LS\ConsumerInterface $consumer)
    {
        $message = $this->meta['pushItMessage'] ?? '';

        if(!empty($message)) {

            if(strpos($message, '{challenge-name}') !== false) {
                $message = str_replace('{challenge-name}', '<a href="' . $this->getChallengeLink() . '">' . $this->post->post_title . '</a>', $message);
            }

            if(strpos($message, '{recipient-name}') !== false) {
                $message = str_replace('{recipient-name}', consumersLoyaltySuite::getConsumerNiceName($consumer), $message);
            }
        }

        return $message;
    }

    /**
     * @return bool
     */
    public function startEmailEnabled()
    {
        return isset($this->meta['startEmail']) && $this->meta['startEmail'];
    }

    /**
     * @return string
     */
    public function getStartEmailSubject()
    {
        return $this->meta['startEmailSubject'] ?? '';
    }

    /**
     * @param int $recipientConsumerId
     * @return string
     */
    public function getStartEmailMessage($recipientConsumerId)
    {
        $message = $this->meta['startEmailMessage'] ?? '';

        if(!empty($message)) {

            if(strpos($message, '{challenge-name}') !== false) {
                $message = str_replace('{challenge-name}', '<a href="' . $this->getChallengeLink() . '">' . $this->post->post_title . '</a>', $message);
            }

            if(strpos($message, '{recipient-name}') !== false) {
                $message = str_replace('{recipient-name}', consumersLoyaltySuite::getConsumerNiceName($recipientConsumerId), $message);
            }

            if(strpos($message, '{activity-days}') !== false) {
                $message = str_replace('{activity-days}', $this->getActivityDays(), $message);
            }

            $message = apply_filters('ls_pech_start_message', $message, $recipientConsumerId);
        }

        return $message;
    }

    /**
     * @return bool
     */
    public function endingEmailEnabled()
    {
        return isset($this->meta['endingEmail']) && $this->meta['endingEmail'];
    }

    /**
     * @return string
     */
    public function getEndingEmailSubject()
    {
        return $this->meta['endingEmailSubject'] ?? '';
    }

    /**
     * @param int $recipientConsumerId
     * @param false|PechRank $rank
     * @return string
     */
    public function getEndingEmailMessage($recipientConsumerId, $rank)
    {
        $message = $this->meta['endingEmailMessage'] ?? '';

        if(!empty($message)) {

            if(strpos($message, '{challenge-name}') !== false) {
                $message = str_replace('{challenge-name}', '<a href="' . $this->getChallengeLink() . '">' . $this->post->post_title . '</a>', $message);
            }

            if(strpos($message, '{recipient-name}') !== false) {
                $message = str_replace('{recipient-name}', consumersLoyaltySuite::getConsumerNiceName($recipientConsumerId), $message);
            }

            if($rank) {
                $message = str_replace('{rank}', $rank->getRank(), $message);
            }

            $message = apply_filters('ls_pech_ending_message', $message, $recipientConsumerId);
        }

        return $message;
    }

    /**
     * @return bool
     */
    public function canShowOnHomepage()
    {
        return isset($this->meta['homepageChallenge']) && $this->meta['homepageChallenge'];
    }

    /**
     * @return string
     */
    public function validate()
    {
        $error = '';

        if(!($this->post instanceof WP_Post)) {
            $error = current_user_can('edit_users') ? 'Incorrect Challenge' : 'Die Vorschau für die Challenge konnte leider nicht erstellt werden';
        }

        return $error;
    }

    /**
     * @return bool
     */
    public function valid()
    {
        $error = $this->validate();

        return empty($error);
    }

    /**
     * @param string &$error
     * @return bool
     */
    public function isActive(&$error = '')
    {
        if(isset($_GET['preview']) && current_user_can('edit_pages')) {
            return true;
        }

        $error = $this->validate();

        if(empty($error)) {
            if($this->post->post_status != 'publish' || empty($this->meta)) {
                $error = 'Zum jetzigen Zeitpunkt ist keine Challenge aktiv/verfügbar.';
            } elseif($this->isScheduled()) {
                if($this->getStartDate() > 0) {
                    $error = sprintf('Die Challenge wird ab %s verfügbar sein.', date('d.m.Y', $this->getStartDate()));
                } else {
                    $error = 'Die Challenge wird in Kürze verfügbar sein';
                }
            } elseif($this->isEnded()) {
                $error = 'Die Challenge ist beendet.';
            }
        }

        return empty($error);
    }
}