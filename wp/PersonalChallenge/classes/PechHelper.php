<?php
class PechHelper
{
    /**
     * @param int $itemsPerPage
     * @param int $page
     * @param string $orderBy
     * @param string $order
     * @return array
     */
    public static function paginateChallenges($itemsPerPage = 20, $page = 1, $orderBy = 'date', $order = 'ASC')
    {
        $query = new WP_Query();

        $posts = $query->query([
            'posts_per_page' => $itemsPerPage > 0 ? $itemsPerPage : -1,
            'offset'         => $itemsPerPage > 0 && $page > 0 ? $itemsPerPage * ($page - 1) : 0,
            'post_type'      => '1x1',
            'order'          => $order == 'ASC' ? 'ASC' : 'DESC',
            'orderby'        => $orderBy,
            'post_status'    => 'any'
        ]);

        $challenges = [];
        foreach($posts as $post) {
            $challenges[] = new PechChallenge($post);
        }

        $result = [
            'rows'         => $challenges,
            'totalCount'   => $query->found_posts,
            'totalPages'   => $itemsPerPage > 0 ? ceil($query->found_posts / $itemsPerPage) : 0,
            'itemsPerPage' => $itemsPerPage
        ];

        wp_reset_postdata();

        return $result;
    }

    /**
     * @return PechChallenge[]
     */
    public static function getChallenges()
    {
        return self::paginateChallenges(0, 0, 'name')['rows'];
    }

    /**
     * @return PechChallenge[]
     */
    public static function getActiveChallenges()
    {
        return array_filter(self::getChallenges(), static function(PechChallenge $challenge) {
            return $challenge->isActive();
        });
    }

    /**
     * Retrieve ended (today) challenges
     * @since 2.1.0
     * @return PechChallenge[]
     */
    public static function getEndedChallenges()
    {
        return array_filter(self::paginateChallenges(0, null, '', '')['rows'], static function(PechChallenge $challenge) {
            return $challenge->getEndDate() > 0 && strtotime('TODAY') > $challenge->getEndDate();
        });
    }

    /**
     * @return array[int]string
     */
    public static function getChallengesList()
    {
        $list = [];

        foreach(self::getChallenges() as $challenge) {
            $list[$challenge->getChallengeId()] = $challenge->getPostTitle();
        }

        return $list;
    }

    /**
     * @return bool|PechChallenge
     */
    public static function getHomepageChallenge()
    {
        $result = false;

        foreach(self::getActiveChallenges() as $challenge) {
            if($challenge->canShowOnHomepage()) {
                $result = $challenge;
                break;
            }
        }

        return $result;
    }

    /**
     * @param PechTeam $team
     * @return int
     */
    public static function createTeam(PechTeam $team)
    {
        if($team->valid()) {

            $team->setCreateDate(time());

            $wpdb = LS()->wpdb;

            if($wpdb->insert($wpdb->prefix . 'ls_fitco_challenge_teams', $team->getRecord())) {
                return $wpdb->insert_id;
            }
        }

        return 0;
    }

    /**
     * @param PechTeam $team
     */
    public static function updateTeam(PechTeam $team)
    {
        if($team->valid()) {
            $wpdb = LS()->wpdb;
            $wpdb->update($wpdb->prefix . 'ls_fitco_challenge_teams', $team->getRecord(), ['teamId' => $team->getTeamId()], null, '%d');
        }
    }

    /**
     * @param PechTeam $team
     */
    public static function deleteTeam(PechTeam $team)
    {
        foreach(self::getTeamParticipations($team) as $pcp) {
            self::deleteParticipation($pcp);
        }

        $wpdb = LS()->wpdb;
        $wpdb->delete($wpdb->prefix . 'ls_fitco_challenge_teams', ['teamId' => $team->getTeamId()], '%d');

        do_action('pech_team_deleted', $team);
    }

    /**
     * @param PechTeam $team
     * @return bool
     */
    public static function hasTeamVisibleParticipants(PechTeam $team)
    {
        $result = false;

        foreach(self::getTeamParticipations($team) as $pcp) {
            if($pcp->isVisible()) {
                $result = true;
                break;
            }
        }

        return $result;
    }

    /**
     * @param PechParticipation $pcp
     * @return int
     */
    public static function createParticipation(PechParticipation $pcp)
    {
        if($pcp->valid()) {
            $wpdb = LS()->wpdb;
            if($wpdb->insert($wpdb->prefix . 'ls_fitco_challenge_participations', $pcp->getRecord())) {
                return $wpdb->insert_id;
            }
        }

        return 0;
    }

    /**
     * @param PechParticipation $pcp
     */
    public static function updateParticipation(PechParticipation $pcp)
    {
        if($pcp->valid()) {
            $wpdb = LS()->wpdb;
            $wpdb->update($wpdb->prefix . 'ls_fitco_challenge_participations', $pcp->getRecord(), ['pcpId' => $pcp->getParticipationId()], null, '%d');
        }
    }

    /**
     * @param int $participationId
     * @return false|PechParticipation
     */
    public static function getParticipation($participationId)
    {
        $result = false;

        if($participationId > 0) {
            $wpdb = LS()->wpdb;
            $result = $wpdb->get_row(sprintf("SELECT * FROM {$wpdb->prefix}ls_fitco_challenge_participations WHERE pcpId = %d", $participationId));
            $result = $result ? new PechParticipation($result) : false;
        }

        return $result;
    }

    /**
     * @param PechParticipation $pcp
     */
    public static function deleteParticipation(PechParticipation $pcp)
    {
        self::deleteParticipationActivities($pcp);

        $wpdb = LS()->wpdb;
        $wpdb->delete($wpdb->prefix . 'ls_fitco_challenge_participations', ['consumerId' => $pcp->getConsumerId(), 'teamId' => $pcp->getTeamId()], '%d');

        do_action('pech_pcp_deleted', $pcp);
    }

    /**
     * Check if consumer has at least one accepted (by himself or by recipient) challenge
     *
     * @param int $consumerId
     * @param null|int $challengeId
     * @param null|int $status
     * @return bool
     */
    private static function hasConsumerParticipation($consumerId, $challengeId = null, $status = null)
    {
        $result = false;

        if($consumerId > 0) {
            $wpdb = LS()->wpdb;
            $join = $challengeId === null ? '' : sprintf("INNER JOIN {$wpdb->prefix}ls_fitco_challenge_teams ci ON ci.teamId = pcp.teamId AND ci.postId = %d", $challengeId);
            $where = $status === null ? '' : sprintf("AND pcp.status = %d", $status);
            $query = sprintf("SELECT EXISTS (
                                 SELECT 1 
                                 FROM {$wpdb->prefix}ls_fitco_challenge_participations pcp
                                 %s
                                 WHERE pcp.consumerId = %d %s
                              )", $join, $consumerId, $where);
            $result = $wpdb->get_var($query) > 0;
        }

        return $result;
    }

    /**
     * @param int $consumerId
     * @param int $teamId
     * @return bool
     */
    public static function isConsumerInTeam($consumerId, $teamId)
    {
        return self::getConsumerParticipationByTeamId($consumerId, $teamId) != false;
    }

    /**
     * @param int $consumerId
     * @param int $challengeId
     * @return bool
     */
    public static function isConsumerAcceptedChallenge($consumerId, $challengeId)
    {
        return self::hasConsumerParticipation($consumerId, $challengeId, PechParticipation::STATUS_ACCEPTED);
    }

    /**
     * @param int $teamId
     * @return false|PechTeam
     */
    public static function getTeam($teamId)
    {
        $result = false;

        if($teamId > 0) {
            $wpdb = LS()->wpdb;
            $result = $wpdb->get_row(sprintf("SELECT * FROM {$wpdb->prefix}ls_fitco_challenge_teams WHERE teamId = %d LIMIT 1", $teamId));
            $result = $result ? new PechTeam($result) : false;
        }

        return $result;
    }

    /**
     * @param int $consumerId
     * @return PechConsumerTeam[]
     */
    public static function getConsumerTeams($consumerId)
    {
        $result = [];

        if($consumerId > 0) {

            $wpdb = LS()->wpdb;
            $query = sprintf("SELECT ci.*, pcp.*
                              FROM {$wpdb->prefix}ls_fitco_challenge_teams ci
                              INNER JOIN {$wpdb->prefix}ls_fitco_challenge_participations pcp ON pcp.teamId = ci.teamId AND pcp.consumerId = %d
                              GROUP BY pcp.pcpId
                              ORDER BY pcp.status ASC, ci.teamId DESC", $consumerId);

            foreach($wpdb->get_results($query) as $record) {
                $result[] = new PechConsumerTeam($record);
            }
        }

        return $result;
    }

    /**
     * @param int $consumerId
     * @return PechConsumerTeam[]
     */
    public static function getConsumerVisibleTeams($consumerId)
    {
        /** @var acmLoyaltySuite $acm */
        $acm = LS()->getModule('acm');

        return array_filter(self::getConsumerTeams($consumerId), static function(PechConsumerTeam $pcc) use ($acm) {
            return $pcc->valid() && $acm->hasAccessToPost($pcc->getChallengeId());
        });
    }

    /**
     * @param int $consumerId
     * @param int $teamId
     * @return false|PechConsumerTeam
     */
    public static function getConsumerTeam($consumerId, $teamId)
    {
        $result = false;

        if($consumerId > 0) {

            $wpdb = LS()->wpdb;
            $query = sprintf("SELECT ci.*, pcp.*
                              FROM {$wpdb->prefix}ls_fitco_challenge_teams ci
                              INNER JOIN {$wpdb->prefix}ls_fitco_challenge_participations pcp ON pcp.teamId = ci.teamId AND pcp.consumerId = %d
                              WHERE ci.teamId = %d", $consumerId, $teamId);
            $result = $wpdb->get_row($query);
            $result = $result ? new PechConsumerTeam($result) : false;

            if(!$result || !$result->valid()) {
                return false;
            }
        }

        return $result;
    }

    /**
     * @param null|int $challengeId
     * @param null|int $companyId
     * @param null|int $status
     * @param null|int $startDate
     * @param null|int $endDate
     * @return int
     */
    public static function getConsumersCount($challengeId = null, $companyId = null, $status = null, $startDate = null, $endDate = null)
    {
        $wpdb = LS()->wpdb;
        $where = [];

        if($challengeId > 0) {
            $where[] = sprintf("p.ID = %d", $challengeId);
        }

        if($companyId > 0) {
            $where[] = sprintf("c.companyId IN (%s)", implode(',', LSCompaniesHelper::includeChildrenCompaniesIds($companyId, true)));
        }

        if($status !== null) {
            $where[] = sprintf("pcp.status = %d", $status);
        }

        if($status !== PechParticipation::STATUS_SENT) {
            if($startDate !== null && $endDate != null) {
                $where[] = $startDate > 0 && $endDate > 0 ? sprintf("pcp.reactionDate BETWEEN %d AND %d", $startDate, $endDate) : '1=0';
            } elseif($startDate !== null) {
                $where[] = $startDate > 0 ? sprintf("pcp.reactionDate >= %d", $startDate) : '1=0';
            } elseif($endDate !== null) {
                $where[] = $endDate > 0 ? sprintf("pcp.reactionDate <= %d", $endDate) : '1=0';
            }
        }

        $where = empty($where) ? '' : ' WHERE ' . implode(' AND ', $where);

        $query = sprintf("SELECT COUNT(DISTINCT pcp.consumerId)
                          FROM {$wpdb->prefix}ls_fitco_challenge_participations pcp
                          INNER JOIN {$wpdb->prefix}ls_consumers c ON c.consumerId = pcp.consumerId AND c.active = 1
                          INNER JOIN {$wpdb->prefix}ls_fitco_challenge_teams ci ON ci.teamId = pcp.teamId
                          INNER JOIN {$wpdb->posts} p ON p.ID = ci.postId
                          %s", $where);

        return (int) $wpdb->get_var($query);
    }

    /**
     * @param int $challengeId
     * @param null|int $companyId
     * @return PechTeam[]
     */
    public static function getTeams($challengeId, $companyId = null)
    {
        $result = [];

        if($challengeId > 0) {

            $wpdb = LS()->wpdb;
            $cmpJoin = $companyId > 0 ? sprintf(" AND c.companyId = %d", $companyId) : '';
            $query = sprintf("SELECT ci.* 
                              FROM {$wpdb->prefix}ls_fitco_challenge_teams ci
                              LEFT JOIN {$wpdb->prefix}ls_fitco_challenge_participations pcp ON pcp.teamId = ci.teamId
                              LEFT JOIN {$wpdb->prefix}ls_consumers c ON c.consumerId = pcp.consumerId AND c.active = 1 %s
                              WHERE ci.postId = %d
                              GROUP BY ci.teamId", $cmpJoin, $challengeId);
            $items = $wpdb->get_results($query);

            if(is_array($items)) {
                foreach($items as $item) {
                    $result[] = new PechTeam($item);
                }
            }

            $result = array_filter($result, static function(PechTeam $pi) {
                return $pi->valid();
            });
        }

        return $result;
    }

    /**
     * @param int $challengeId
     * @param int $companyId
     * @param null|int $startDate
     * @param null|int $endDate
     * @return int
     */
    public static function getTeamsCount($challengeId = 0, $companyId = 0, $startDate = null, $endDate = null)
    {
        $wpdb = LS()->wpdb;
        $where = [];

        if($challengeId > 0) {
            $where[] = sprintf("p.ID = %d", $challengeId);
        }

        if($companyId > 0) {
            $where[] = sprintf("c.companyId IN (%s)", implode(',', LSCompaniesHelper::includeChildrenCompaniesIds($companyId, true)));
        }

        if($startDate !== null && $endDate != null) {
            $where[] = $startDate > 0 && $endDate > 0 ? sprintf("ci.createDate BETWEEN %d AND %d", $startDate, $endDate) : '1=0';
        }

        $where = empty($where) ? '' : ' WHERE ' . implode(' AND ', $where);

        $query = sprintf("SELECT COUNT(DISTINCT ci.teamId)
                          FROM {$wpdb->prefix}ls_fitco_challenge_teams ci
                          INNER JOIN {$wpdb->posts} p ON p.ID = ci.postId
                          LEFT JOIN {$wpdb->prefix}ls_fitco_challenge_participations pcp ON pcp.teamId = ci.teamId
                          LEFT JOIN {$wpdb->prefix}ls_consumers c ON c.consumerId = pcp.consumerId AND c.active = 1
                          %s", $where);

        return (int) $wpdb->get_var($query);
    }

    /**
     * Check if possible to create/participate/join to the challenge
     *
     * @param PechChallenge $challenge
     * @param int $startDate Date to create a team
     * @return bool
     */
    public static function possibleToCreateTeamOn($startDate, PechChallenge $challenge)
    {
        $startDate = $startDate ?? strtotime('TODAY');

        // option that allows to create challenges by consumers is disabled
        if (!$challenge->canConsumerCreateTeam()) {
            return false;
        }

        // can't be in the past
        if (strtotime('TODAY') > $startDate) {
            return false;
        }

        // not later than 1 month after today
        if ($startDate > strtotime('TODAY +1 MONTH')) {
            return false;
        }

        // check if it's enough days until the end of challenge
        if ($challenge->getEndDate() > 0) {
            $teamLastDay = $startDate + ($challenge->getDuration() - 1) * DAY_IN_SECONDS;
            if ($teamLastDay > $challenge->getEndDate()) {
                return false;
            }
        }

        return true;
    }

    /**
     * @param int $consumerId
     * @param PechChallenge $challenge
     * @return bool
     */
    public static function singleParticipationRuleCompleted($consumerId, PechChallenge $challenge)
    {
        if (!$challenge->isSingleParticipation()) {
            return true;
        }

        foreach(self::getConsumerVisibleTeams($consumerId) as $team) {
            if($team->getChallengeId() === $challenge->getChallengeId() && $team->isParticipated() && !$team->getChallengeTeam()->isFinished()) {
                return false;
            }
        }

        return true;
    }

    /**
     * @deprecated 03.2019 Use canConsumerJoinChallenge()
     * @TODO Can be removed after 06.2019
     *
     * @param int $consumerId
     * @param PechChallenge $challenge
     * @return bool
     */
    public static function canConsumerJoinTeam($consumerId, PechChallenge $challenge)
    {
        _deprecated_function(__FUNCTION__, '4.5.0', 'canConsumerJoinChallenge');

        return self::singleParticipationRuleCompleted($consumerId, $challenge);
    }

    /**
     * @param int $consumerId
     * @param PechChallenge $challenge
     * @return bool
     */
    public static function canConsumerCreateTeam($consumerId, PechChallenge $challenge)
    {
        return
            self::possibleToCreateTeamOn(strtotime('TODAY'), $challenge)
            && self::singleParticipationRuleCompleted($consumerId, $challenge);
    }

    /**
     * @param int $consumerId
     * @param null|int $challengeId
     * @return PechParticipation[]
     */
    public static function getConsumerParticipations($consumerId, $challengeId = null)
    {
        $result = [];

        if($consumerId > 0) {

            $wpdb = LS()->wpdb;
            $challengeJoin = $challengeId === null ? '' : sprintf("INNER JOIN {$wpdb->prefix}ls_fitco_challenge_teams t ON t.teamId = pcp.teamId AND t.postId = %d", $challengeId);
            $query = sprintf("SELECT pcp.*
                              FROM {$wpdb->prefix}ls_fitco_challenge_participations pcp
                              %s
                              WHERE pcp.consumerId = %d
                              GROUP BY pcp.pcpId
                              ORDER BY pcp.status ASC, pcp.teamId DESC", $challengeJoin, $consumerId);

            foreach($wpdb->get_results($query) as $pcp) {
                $result[] = new PechParticipation($pcp);
            }

            $result = array_filter($result, static function(PechParticipation $pp) {
                return $pp->valid();
            });
        }

        return $result;
    }

    /**
     * @param int $consumerId
     * @param int $teamId
     * @return PechParticipation
     */
    public static function getConsumerParticipationByTeamId($consumerId, $teamId)
    {
        $result = false;

        if($consumerId > 0 && $teamId > 0) {
            $wpdb = LS()->wpdb;
            $result = $wpdb->get_row(sprintf("SELECT * FROM {$wpdb->prefix}ls_fitco_challenge_participations WHERE consumerId = %d AND teamId = %d LIMIT 1", $consumerId, $teamId));
            $result = $result ? new PechParticipation($result) : false;
        }

        return $result;
    }

    /**
     * @param PechTeam $team
     * @param int[] $statuses
     * @return PechParticipation[]
     */
    public static function getTeamParticipations(PechTeam $team, $statuses = [PechParticipation::STATUS_SENT, PechParticipation::STATUS_ACCEPTED, PechParticipation::STATUS_CANCELED])
    {
        $wpdb = LS()->wpdb;
        $query = sprintf("SELECT pcp.*
                          FROM {$wpdb->prefix}ls_fitco_challenge_participations pcp
                          INNER JOIN {$wpdb->prefix}ls_consumers c ON c.consumerId = pcp.consumerId AND c.active = 1
                          INNER JOIN {$wpdb->prefix}ls_fitco_challenge_teams ci ON ci.teamId = pcp.teamId AND ci.teamId = %d
                          GROUP BY pcp.pcpId
                          ORDER BY pcp.status ASC, pcp.teamId DESC", $team->getTeamId());

        $result = [];

        foreach($wpdb->get_results($query) as $record) {
            $result[] = new PechParticipation($record);
        }

        $result = array_filter($result, static function(PechParticipation $pp) {
            return $pp->valid();
        });

        $result = array_filter($result, static function(PechParticipation $pcp) use ($statuses) {
            return in_array($pcp->getStatus(), $statuses);
        });

        return $result;
    }

    /**
     * @param PechActivity $activity
     * @param string $challengeType
     * @return int
     */
    private static function getPointsFromActivity(PechActivity $activity, $challengeType)
    {
        return
            $challengeType === PechChallenge::TYPE_VALUES
                ? $activity->getData()
                : ($activity->getData() === PechChallenge::ANSWER_YES ? 1 : 0);
    }

    /**
     * @param PechParticipation[] $participations
     * @param PechActivity[] $activities
     * @param string $challengeType
     * @return array[int]int
     */
    public static function getParticipationsPoints($participations, $activities, $challengeType)
    {
        $pcpPoints = [];

        foreach($participations as $participation) {

            if(!$participation->isAccepted()) {
                continue;
            }

            $points = 0;

            foreach($activities as $activity) {
                if($activity->getParticipationId() === $participation->getParticipationId()) {
                    $points += self::getPointsFromActivity($activity, $challengeType);
                }
            }

            $pcpPoints[$participation->getConsumerId()] = $points;
        }

        return $pcpPoints;
    }

    /**
     * @param PechConsumerTeam $pcc
     * @return false|PechRank
     */
    public static function getParticipantRank(PechConsumerTeam $pcc)
    {
        if(!$pcc->isStarted()) {
            return false;
        }

        $participations = self::getTeamParticipations($pcc->getTeam(), [PechParticipation::STATUS_ACCEPTED]);
        $activities = self::getTeamActivities($pcc->getTeam());
        $points = self::getParticipationsPoints($participations, $activities, $pcc->getChallenge()->getChallengeType());

        if(!isset($points[$pcc->getConsumerId()])) {
            return false;
        }

        // trick to sort consumers AND move current consumer higher that opponents with the same points values
        $points[$pcc->getConsumerId()] += 0.1;
        arsort($points);
        $points[$pcc->getConsumerId()] -= 0.1;

        $pos = array_search($pcc->getConsumerId(), array_keys($points));
        if($pos === false) {
            return false;
        }

        return new PechRank($pos + 1, count($participations));
    }

    /**
     * @param PechParticipation $pcp
     * @param PechActivity[] $activities
     */
    public static function updateParticipationActivities(PechParticipation $pcp, $activities)
    {
        self::deleteParticipationActivities($pcp);

        foreach($activities as $activity) {
            self::addActivity($activity);
        }
    }

    /**
     * @param int $consumerId
     * @param null|int $challengeId
     * @return PechActivity[]
     */
    public static function getConsumerActivitiesForToday($consumerId, $challengeId = null)
    {
        $activities = [];

        if($consumerId > 0) {

            $wpdb = LS()->wpdb;
            $chJoin = '';

            if($challengeId !== null) {
                $chJoin = sprintf("INNER JOIN {$wpdb->prefix}ls_fitco_challenge_teams ci ON ci.teamId = pcp.teamId AND ci.postId = %d", $challengeId);
            }

            $query = sprintf("SELECT ca.* 
                              FROM {$wpdb->prefix}ls_fitco_challenge_activities ca
                              INNER JOIN {$wpdb->prefix}ls_fitco_challenge_participations pcp ON pcp.pcpId = ca.pcpId AND pcp.consumerId = %d
                              %s
                              WHERE ca.activityDate = '%s'
                              ORDER BY ca.pcpId", $consumerId, $chJoin, date('Y-m-d'));

            foreach($wpdb->get_results($query) as $result) {
                $activities[] = new PechActivity($result);
            }

            $activities = array_filter($activities, static function(PechActivity $pa) {
                return $pa->valid();
            });
        }

        return $activities;
    }

    /**
     * @param PechTeam $team
     * @return PechActivity[]
     */
    public static function getTeamActivities(PechTeam $team)
    {
        $activities = [];

        if(!$team->isUpcoming()) {

            $wpdb = LS()->wpdb;

            $query = sprintf("SELECT ca.* 
                              FROM {$wpdb->prefix}ls_fitco_challenge_activities ca 
                              INNER JOIN {$wpdb->prefix}ls_fitco_challenge_participations pcp ON pcp.pcpId = ca.pcpId
                              INNER JOIN {$wpdb->prefix}ls_consumers c ON c.consumerId = pcp.consumerId AND c.active = 1
                              INNER JOIN {$wpdb->prefix}ls_fitco_challenge_teams ci ON ci.teamId = pcp.teamId AND ci.teamId = %d
                              GROUP BY ca.pcpId, ca.activityDate
                              ORDER BY ca.pcpId DESC, ca.activityDate ASC", $team->getTeamId());

            foreach($wpdb->get_results($query) as $result) {
                $activities[] = new PechActivity($result);
            }

            $activities = array_filter($activities, static function(PechActivity $pa) {
                return $pa->valid();
            });
        }

        return $activities;
    }

    /**
     * @param PechActivity $activity
     * @return int
     */
    public static function addActivity(PechActivity $activity)
    {
        if($activity->valid()) {
            $wpdb = LS()->wpdb;
            if($wpdb->insert($wpdb->prefix . 'ls_fitco_challenge_activities', $activity->getRecord(true))) {
                return $wpdb->insert_id;
            }
        }

        return 0;
    }

    /**
     * @param PechParticipation $pcp
     */
    public static function deleteParticipationActivities(PechParticipation $pcp)
    {
        $wpdb = LS()->wpdb;
        $wpdb->delete($wpdb->prefix . 'ls_fitco_challenge_activities', ['pcpId' => $pcp->getParticipationId()], '%d');
    }

    /**
     * @param int[] $companyIds
     * @return int
     */
    public static function countPushes($companyIds = [])
    {
        $wpdb = LS()->wpdb;
        $cmpWhere = empty($companyIds) ? '' : sprintf(" AND c.companyId IN (%s)", implode(',', $companyIds));
        $query = sprintf("SELECT pcp.*, ci.*
                          FROM {$wpdb->prefix}ls_fitco_challenge_participations pcp
                          INNER JOIN {$wpdb->prefix}ls_consumers c ON c.consumerId = pcp.consumerId AND c.active = 1 %s
                          INNER JOIN {$wpdb->prefix}ls_fitco_challenge_teams ci ON ci.teamId = pcp.teamId
                          WHERE pcp.push = 1
                          GROUP BY pcp.pcpId", $cmpWhere);

        $pushes = 0;

        foreach($wpdb->get_results($query) as $record) {
            $pcc = new PechConsumerTeam($record);
            if($pcc->valid() && $pcc->hasToPush()) {
                $pushes++;
            }
        }

        return $pushes;
    }

    /**
     * @param int $itemsPerPage
     * @param int $page
     * @param string $orderBy
     * @param string $order
     * @param array $filters
     * @return array
     */
    public static function getParticipantsStatistics($itemsPerPage = 20, $page = 1, $orderBy = '', $order = '', $filters = [])
    {
        $wpdb = LS()->wpdb;
        $order = empty($order) ? 'DESC' : $order;
        $page = isset($page) ? ($page > 0 ? absint($page) : 1) : null;

        if(in_array($orderBy, ['pcpId', 'teamId', 'reactionDate', 'push', 'status'])) {
            $orderBy = 'pcp.' . $orderBy;
        } elseif($orderBy == 'consumerId') {
            $orderBy = 'CONCAT(c.displayName, c.lastName, c.firstName, c.email)';
        } elseif($orderBy == 'postId') {
            $orderBy = 'ci.postId';
        } else {
            $orderBy = 'pcp.teamId';
        }

        $where = [];

        // filter by challenge ID
        if(isset($filters['postId']) && $filters['postId'] > 0) {
            $where[] = sprintf("p.ID = %d", $filters['postId']);
        }

        // filter by company ID
        if(isset($filters['companyId']) && $filters['companyId'] > 0) {
            $where[] = sprintf("c.companyId IN (%s)", implode(',', LSCompaniesHelper::includeChildrenCompaniesIds($filters['companyId'], true)));
        }

        // filter by team ID
        if(isset($filters['teamId']) && $filters['teamId'] > 0) {
            $where[] = sprintf("ci.teamId = %d", $filters['teamId']);
        }

        $where = empty($where) ? '' : 'WHERE ' . implode(" AND ", array_values($where));

        $limit = '';
        if($page > 0 && $itemsPerPage > 0) {
            $limit = sprintf(" LIMIT %d, %d", ($page - 1) * $itemsPerPage, $itemsPerPage);
        }

        $query = sprintf("SELECT SQL_CALC_FOUND_ROWS pcp.*, ci.*
                          FROM {$wpdb->prefix}ls_fitco_challenge_participations pcp
                          INNER JOIN {$wpdb->prefix}ls_fitco_challenge_teams ci ON ci.teamId = pcp.teamId
                          INNER JOIN {$wpdb->posts} p ON p.ID = ci.postId
                          INNER JOIN {$wpdb->prefix}ls_consumers c ON c.consumerId = pcp.consumerId
                          %s
                          GROUP BY pcp.pcpId
                          ORDER BY %s %s %s", $where, $orderBy, $order, $limit);

        $result = $wpdb->get_results($query);
        $totalCount = (int) $wpdb->get_var("SELECT FOUND_ROWS()");
        $itemsPerPage = $itemsPerPage > 0 ? $itemsPerPage : $totalCount;
        $totalPages = $totalCount > 0 && $itemsPerPage > 0 ? ceil($totalCount / $itemsPerPage) : 0;

        $items = [];

        foreach ($result as $record) {
            $items[] = new PechConsumerTeam($record);
        }

        return [
            'rows'         => $items,
            'totalCount'   => $totalCount,
            'totalPages'   => $totalPages,
            'itemsPerPage' => $itemsPerPage
        ];
    }

    /**
     * @param int $itemsPerPage
     * @param int $page
     * @param string $orderBy
     * @param string $order
     * @param array $filters
     * @return array
     */
    public static function getTeamsStatistic($itemsPerPage = 20, $page = 1, $orderBy = '', $order = '', $filters = [])
    {
        $wpdb = LS()->wpdb;
        $order = empty($order) ? 'DESC' : $order;
        $page = isset($page) ? ($page > 0 ? absint($page) : 1) : null;

        if($orderBy == 'pcps') {
            $orderBy = 'pcps';
        } elseif(in_array($orderBy, ['pcpId', 'teamId', 'consumerId', 'reactionDate', 'push'])) {
            $orderBy = 'pcp.' . $orderBy;
        } elseif($orderBy == 'consumer') {
            $orderBy = 'CONCAT(c.displayName, c.lastName, c.firstName, c.email)';
        } elseif(in_array($orderBy, ['createDate', 'startDate'])) {
            $orderBy = 'ci.' . $orderBy;
        } else {
            $orderBy = 'pcp.teamId';
        }

        $where = [];

        // filter by challenge ID
        if(isset($filters['postId']) && $filters['postId'] > 0) {
            $where[] = sprintf("p.ID = %d", $filters['postId']);
        }

        // filter by company ID
        if(isset($filters['companyId']) && $filters['companyId'] > 0) {
            $where[] = sprintf("c.companyId IN (%s)", implode(',', LSCompaniesHelper::includeChildrenCompaniesIds($filters['companyId'], true)));
        }

        $where = empty($where) ? '' : 'WHERE ' . implode(" AND ", array_values($where));

        $limit = '';
        if($page > 0 && $itemsPerPage > 0) {
            $limit = sprintf(" LIMIT %d, %d", ($page - 1) * $itemsPerPage, $itemsPerPage);
        }

        $query = sprintf("SELECT SQL_CALC_FOUND_ROWS ci.*, COUNT(pcp.consumerId) as pcps
                          FROM {$wpdb->prefix}ls_fitco_challenge_teams ci 
                          INNER JOIN {$wpdb->posts} p ON p.ID = ci.postId
                          LEFT JOIN {$wpdb->prefix}ls_fitco_challenge_participations pcp ON pcp.teamId = ci.teamId
                          LEFT JOIN {$wpdb->prefix}ls_consumers c ON c.consumerId = pcp.consumerId AND c.active = 1
                          %s
                          GROUP BY ci.teamId
                          ORDER BY %s %s %s", $where, $orderBy, $order, $limit);

        $items =
        $pcps = [];

        foreach($wpdb->get_results($query) as $record) {
            $items[] = new PechTeam($record);
            $pcps[$record->teamId] = (int) $record->pcps;
        }

        $totalCount = (int) $wpdb->get_var("SELECT FOUND_ROWS()");
        $itemsPerPage = $itemsPerPage > 0 ? $itemsPerPage : $totalCount;
        $totalPages = $totalCount > 0 && $itemsPerPage > 0 ? ceil($totalCount / $itemsPerPage) : 0;

        return [
            'rows'         => $items,
            'pcps'         => $pcps,
            'totalCount'   => $totalCount,
            'totalPages'   => $totalPages,
            'itemsPerPage' => $itemsPerPage
        ];
    }

    /**
     * @return PechConsumerTeam[]
     */
    public static function getTeamsToSendStartEmail()
    {
        $wpdb = LS()->wpdb;
        $result = [];

        foreach(self::getActiveChallenges() as $challenge) {

            if(!$challenge->startEmailEnabled()) {
                continue;
            }

            $query = sprintf("SELECT pcp.*, ci.* 
                              FROM {$wpdb->prefix}ls_fitco_challenge_participations pcp
                              INNER JOIN {$wpdb->prefix}ls_consumers c ON c.consumerId = pcp.consumerId AND c.active = 1
                              INNER JOIN {$wpdb->prefix}ls_fitco_challenge_teams ci ON ci.teamId = pcp.teamId
                              WHERE ci.postId = %d AND '%s' = ci.startDate
                              GROUP BY pcp.pcpId", $challenge->getChallengeId(), date('Y-m-d'));

            foreach($wpdb->get_results($query) as $record) {
                $result[] = new PechConsumerTeam($record);
            }

            $result = array_filter($result, static function(PechConsumerTeam $pcc) {
                return $pcc->valid();
            });
        }

        return $result;
    }

    /**
     * @return PechConsumerTeam[]
     */
    public static function getTeamsToSendEndingEmail()
    {
        $wpdb = LS()->wpdb;
        $result = [];

        foreach(self::getActiveChallenges() as $challenge) {

            if(!$challenge->endingEmailEnabled()) {
                continue;
            }

            $query = sprintf("SELECT pcp.*, ci.* 
                              FROM {$wpdb->prefix}ls_fitco_challenge_participations pcp
                              INNER JOIN {$wpdb->prefix}ls_consumers c ON c.consumerId = pcp.consumerId AND c.active = 1
                              INNER JOIN {$wpdb->prefix}ls_fitco_challenge_teams ci ON ci.teamId = pcp.teamId
                              WHERE ci.postId = %d AND '%s' = DATE(DATE_ADD(ci.startDate, INTERVAL %d DAY))
                              GROUP BY pcp.pcpId", $challenge->getChallengeId(), date('Y-m-d'), $challenge->getDuration());

            foreach($wpdb->get_results($query) as $record) {
                $result[] = new PechConsumerTeam($record);
            }

            $result = array_filter($result, static function(PechConsumerTeam $pcc) {
                return $pcc->valid() && $pcc->isEnded();
            });
        }

        return $result;
    }

    /**
     * @param int $challengeId
     */
    public static function deleteChallenge($challengeId)
    {
        if($challengeId > 0) {

            $wpdb = LS()->wpdb;
            $teams = $wpdb->get_results(sprintf("SELECT * FROM {$wpdb->prefix}ls_fitco_challenge_teams WHERE postId = %d", $challengeId));

            foreach($teams as $team) {
                self::deleteTeam(new PechTeam($team));
            }

            do_action('pech_deleted', $challengeId);
        }
    }

    /**
     * @param PechChallenge $challenge
     * @param int $companyId
     * @return PechTeamRank[]
     */
    public static function getLeaderboard(PechChallenge $challenge, $companyId = 0)
    {
        $cacheName = $companyId > 0 ? 'pech_leaderboard_' . $companyId : 'pech_leaderboard';
        if(($leaderboard = wp_cache_get($cacheName, 'fitco')) === false) {

            $leaderboard = [];

            foreach(self::getTeams($challenge->getChallengeId(), $companyId) as $team) {
                if(!$team->isUpcoming() && !empty($team->getTeamName())) {

                    $points = self::getParticipationsPoints(
                        self::getTeamParticipations($team, [PechParticipation::STATUS_ACCEPTED]),
                        self::getTeamActivities($team),
                        $challenge->getChallengeType()
                    );

                    $leaderboard[] = new PechTeamRank($team, array_sum($points), count($points));
                }
            }

            usort($leaderboard, static function(PechTeamRank $a, PechTeamRank $b) {
                return ($a->getRank() > $b->getRank() ? -1 : ($a->getRank() < $b->getRank() ? 1 : 0));
            });

            wp_cache_add($cacheName, $leaderboard, 'fitco', 60);
        }

        return $leaderboard;
    }

    /**
     * @param string $name
     * @return bool
     */
    public static function isUniqueTeamName($name)
    {
        $unique = false;

        if(!empty($name)) {
            $wpdb = LS()->wpdb;
            $unique = $wpdb->get_var(sprintf("SELECT EXISTS ( SELECT 1 FROM {$wpdb->prefix}ls_fitco_challenge_teams WHERE teamName = '%s' )", esc_sql($name))) == 0;
        }

        return $unique;
    }

    /**
     * @param int $consumerId
     * @param PechChallenge $challenge
     * @return PechRank
     */
    public static function getParticipantRankInChallenge($consumerId, PechChallenge $challenge)
    {
        $wpdb = LS()->wpdb;
        $sumQuery = $challenge->getChallengeType() === PechChallenge::TYPE_VALUES ? "a.data" : "IF(a.data = " . PechChallenge::ANSWER_YES . ", 1, 0)";
        $query = sprintf("SELECT t.consumerId
                            FROM (
                            SELECT pcp.consumerId, SUM(%s) points
                            FROM {$wpdb->prefix}ls_fitco_challenge_activities a
                            INNER JOIN {$wpdb->prefix}ls_fitco_challenge_participations pcp ON pcp.pcpId = a.pcpId
                            INNER JOIN {$wpdb->prefix}ls_fitco_challenge_teams ci ON ci.teamId = pcp.teamId AND ci.postId = %d
                            GROUP BY a.pcpId
                          ) t
                          GROUP BY t.consumerId
                          HAVING MAX(t.points)
                          ORDER BY MAX(t.points)", $sumQuery, $challenge->getChallengeId());
        $scores = array_map('intval', $wpdb->get_col($query));
        $rank = in_array($consumerId, $scores) ? array_search($consumerId, array_reverse($scores)) + 1 : 0;

        return new PechRank($rank, count($scores));
    }

    /**
     * @param string $email
     * @param string $emailSubject
     * @param string $emailMessage
     * @param \LS\ConsumerInterface $consumer
     * @return bool
     */
    public static function sendComCenterEmail(string $email, string $emailSubject, string $emailMessage, \LS\ConsumerInterface $consumer): bool
    {
        /** @var comCenterLoyaltySuite $comCenter */
        $comCenter = LS()->getModule('comCenter');
        $template = $comCenter->getTemplate('fitco-green');
        $body = $comCenter->renderEmailContent($emailSubject, $emailMessage, $template, $consumer->getConsumerId());

        return LS\Mail::mail([
            'to'             => $email,
            'subject'        => $emailSubject,
            'body'           => $body,
            'footer'         => false,
            'embeddedImages' => true
        ]);
    }
}