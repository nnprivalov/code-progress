<?php
class PechRank
{
    /** @var int */
    private $rank = 0;

    /** @var int */
    private $total = 0;

    /**
     * @param int $rank
     * @param int $totalParticipations
     */
    public function __construct($rank, $totalParticipations)
    {
        if($rank > 0 && $totalParticipations >= $rank) {
            $this->rank = (int) $rank;
            $this->total = (int) $totalParticipations;
        }
    }

    /**
     * @return int
     */
    public function getRank()
    {
        return $this->rank;
    }

    /**
     * @return int
     */
    public function getTotalParticipations()
    {
        return $this->total;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getRank() . ' von ' . $this->getTotalParticipations();
    }
}