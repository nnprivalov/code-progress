<?php
namespace LS\BonusPoints;

class BPPechTrigger extends \LS\BPTrigger
{
    /** @var null|array */
    private static $challengesList;

    /**
     * @return string
     */
    public function getName(): string
    {
        return 'pech';
    }

    /**
     * @return string
     */
    public function getLabel(): string
    {
        return __('1:1 Challenge participation', 'ls');
    }

    /**
     * @return array
     */
    private function getChallengesList(): array
    {
        if (self::$challengesList === null) {
            self::$challengesList = \PechHelper::getChallengesList();
        }

        return self::$challengesList;
    }

    /**
     * @return \LS\Field\Select
     */
    public function getIdentifyField()
    {
        return new \LS\Field\Select(
            $this->getIdentifyFieldName(),
            '1:1 Challenge',
            $this->getChallengesList(),
            [
                'emptyText' => 'All',
                'class'     => 'trigger-ident'
            ]
        );
    }
}