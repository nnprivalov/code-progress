<?php
class PechChallengeTeam
{
    /** @var PechChallenge */
    private $challenge;

    /** @var PechTeam */
    private $team;

    /**
     * @param PechChallenge $challenge
     * @param PechTeam $team
     */
    public function __construct(PechChallenge $challenge, PechTeam $team)
    {
        $this->challenge = $challenge;
        $this->team = $team;
    }

    /**
     * @return PechTeam
     */
    public function getTeam()
    {
        return $this->team;
    }

    /**
     * @return int
     */
    public function getTeamId()
    {
        return $this->team->getTeamId();
    }

    /**
     * @return PechChallenge
     */
    public function getChallenge()
    {
        return $this->challenge;
    }

    /**
     * @return int
     */
    public function getChallengeId()
    {
        return $this->challenge->getChallengeId();
    }

    /**
     * @return int
     */
    public function getTeamEndDate()
    {
        return
            $this->challenge->getActivityDays() > 0
                ? $this->team->getStartDate() + ($this->challenge->getActivityDays() - 1) * DAY_IN_SECONDS
                : 0;
    }

    /**
     * @return int
     */
    public function getStartDate()
    {
        return $this->team->getStartDate();
    }

    /**
     * @return bool
     */
    public function isUpcoming()
    {
        return $this->team->isUpcoming();
    }

    /**
     * @return bool
     */
    public function isEnded()
    {
        return time() >= $this->getTeamEndDate() + DAY_IN_SECONDS;
    }

    /**
     * @return int
     */
    private function getFinishDate()
    {
        return
            $this->challenge->getDuration() > 0
                ? $this->team->getStartDate() + ($this->challenge->getDuration() - 1) * DAY_IN_SECONDS
                : 0;
    }

    /**
     * @return bool
     */
    public function isFinished()
    {
        return time() >= $this->getFinishDate() + DAY_IN_SECONDS;
    }

    /**
     * @return int
     */
    public function getDaysToStart()
    {
        return
            $this->team->getStartDate() >= time()
                ? ceil(($this->team->getStartDate() - time()) / DAY_IN_SECONDS)
                : 0;
    }

    /**
     * @return int
     */
    public function getDaysToEnd()
    {
        return
            $this->getTeamEndDate() >= time()
                ? ceil(($this->getTeamEndDate() - time()) / DAY_IN_SECONDS)
                : 0;
    }

    /**
     * @return string
     */
    public function getTeamName()
    {
        return $this->team->getTeamName();
    }
}