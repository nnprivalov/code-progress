<?php
class PechTeam extends LS\Model
{
    /** @var int */
    protected $teamId;

    /** @var int */
    protected $postId;

    /** @var string */
    protected $teamName;

    /** @var int */
    protected $startDate;

    /** @var int */
    protected $createDate;

    /** @var string[] */
    protected $primaryKeys = [
        'teamId'
    ];

    /** @var string[] */
    protected $dbCols = [
        'teamId',
        'postId',
        'teamName',
        'startDate',
        'createDate'
    ];

    /**
     * @return PechTeam $this
     */
    public function setDefault()
    {
        $this->teamId =
        $this->postId =
        $this->startDate = 0;

        $this->teamName = '';

        $this->createDate = time();

        return $this;
    }

    /**
     * @param array $data
     * @return array
     */
    protected function format($data)
    {
        foreach($data as $key => $value) {
            if(in_array($key, ['teamId', 'postId'])) {
                $data[$key] = (int) $value;
            } elseif($key == 'teamName') {
                $data[$key] = trim($value);
            } elseif(in_array($key, ['startDate', 'createDate'])) {
                $data[$key] = max(0, !empty($value) && !is_numeric($value) ? strtotime($value) : (int) $value);
            }
        }

        return $data;
    }

    /**
     * @return int
     */
    public function getTeamId()
    {
        return $this->teamId;
    }

    /**
     * @return int
     */
    public function getChallengeId()
    {
        return $this->postId;
    }

    /**
     * @return string
     */
    public function getTeamName()
    {
        return $this->teamName;
    }

    /**
     * @return int
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * @return bool
     */
    public function isUpcoming()
    {
        return $this->getStartDate() > time();
    }

    /**
     * @return int
     */
    public function getCreateDate()
    {
        return $this->createDate;
    }

    /**
     * @param int $createDate
     * @return self
     */
    public function setCreateDate($createDate)
    {
        $this->createDate = (int) $createDate;

        return $this;
    }

    /**
     * @return bool
     */
    public function valid()
    {
        return $this->getChallengeId() > 0 && $this->getStartDate() > 0;
    }

    /**
     * @param bool $withPrimaryKeys True to include primary keys data
     * @return array An array of columns and their values
     */
    public function getRecord($withPrimaryKeys = false)
    {
        $data = [];

        foreach($this->getDbColumns($withPrimaryKeys) as $col) {
            if($col == 'startDate') {
                $data[$col] = empty($this->{$col}) ? '0000-00-00' : date('Y-m-d', $this->{$col});
            } else {
                $data[$col] = $this->{$col};
            }
        }

        return $data;
    }
}