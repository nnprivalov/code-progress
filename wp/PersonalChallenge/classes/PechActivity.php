<?php
class PechActivity extends LS\Model
{
    /** @var int */
    protected $pcpId;

    /** @var int */
    protected $activityDate;

    /** @var int */
    protected $data;

    /** @var string[] */
    protected $primaryKeys = [
        'pcpId',
        'activityDate'
    ];

    /** @var string[] */
    protected $dbCols = [
        'pcpId',
        'activityDate',
        'data'
    ];

    /**
     * @return PechActivity $this
     */
    public function setDefault()
    {
        $this->pcpId =
        $this->activityDate =
        $this->data = 0;

        return $this;
    }

    /**
     * @param array $data
     * @return array
     */
    protected function format($data)
    {
        foreach($data as $key => $value) {
            if($key === 'pcpId') {
                $data[$key] = (int) $value;
            } elseif($key === 'activityDate') {
                $data[$key] = max(0, !empty($value) && !is_numeric($value) ? strtotime($value) : (int) $value);
            } elseif($key === 'data') {
                $data[$key] = (int) $value;
            }
        }

        return $data;
    }

    /**
     * @return int
     */
    public function getParticipationId()
    {
        return $this->pcpId;
    }

    /**
     * @return string
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @return int
     */
    public function getActivityDate()
    {
        return $this->activityDate;
    }

    /**
     * @return bool
     */
    public function valid()
    {
        return $this->getParticipationId() > 0 && $this->getActivityDate() > 0;
    }

    /**
     * @param bool $withPrimaryKeys True to include primary keys data
     * @return array An array of columns and their values
     */
    public function getRecord($withPrimaryKeys = false)
    {
        $data = [];

        foreach($this->getDbColumns($withPrimaryKeys) as $col) {
            if($col == 'activityDate') {
                $data[$col] = empty($this->{$col}) ? '0000-00-00' : date('Y-m-d', $this->{$col});
            } else {
                $data[$col] = $this->{$col};
            }
        }

        return $data;
    }
}