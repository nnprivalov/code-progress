<?php
class PechTeamRank
{
    /** @var PechTeam */
    private $team;

    /** @var int */
    private $participants;

    /** @var int[] */
    private $points;

    /** @var float */
    private $rank;

    /**
     * @param PechTeam $team
     * @param int $points
     * @param int $participants
     */
    public function __construct(PechTeam $team, $points, $participants)
    {
        $this->team = $team;
        $this->points = $points;
        $this->participants = $participants;
        $this->rank = $this->points > 0 && $this->participants > 0 ? $this->points / $this->participants : 0;
    }

    /**
     * @return PechTeam
     */
    public function getTeam()
    {
        return $this->team;
    }

    /**
     * @return int
     */
    public function getParticipants()
    {
        return $this->participants;
    }

    /**
     * @return int
     */
    public function getPoints()
    {
        return $this->points;
    }

    /**
     * @return float
     */
    public function getRank()
    {
        return $this->rank;
    }
}