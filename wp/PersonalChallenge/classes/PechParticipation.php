<?php
class PechParticipation extends LS\Model
{
    /** @var int */
    protected $pcpId;

    /** @var int */
    protected $teamId;

    /** @var int */
    protected $consumerId;

    /** @var int */
    protected $reactionDate;

    /** @var int */
    protected $status;

    /** @var bool */
    protected $push;

    /** @var bool */
    protected $visible;

    /** @var string[] */
    protected $primaryKeys = [
        'pcpId'
    ];

    /** @var string[] */
    protected $dbCols = [
        'pcpId',
        'teamId',
        'consumerId',
        'status',
        'reactionDate',
        'push',
        'visible'
    ];

    const STATUS_SENT = 0;
    const STATUS_ACCEPTED = 1;
    const STATUS_CANCELED = 2;

    /** @var null|\LS\Consumer */
    protected $consumer;

    /**
     * @return PechParticipation $this
     */
    public function setDefault()
    {
        $this->pcpId =
        $this->teamId =
        $this->consumerId =
        $this->reactionDate = 0;

        $this->status = self::STATUS_SENT;

        $this->push = false;

        $this->visible = true;

        $this->consumer = null;

        return $this;
    }

    /**
     * @param array $data
     * @return array
     */
    protected function format($data)
    {
        foreach($data as $key => $value) {
            if(in_array($key, ['pcpId', 'teamId', 'consumerId'])) {
                $data[$key] = (int) $value;
            } elseif($key == 'status') {
                $data[$key] = in_array($value, [self::STATUS_SENT, self::STATUS_ACCEPTED, self::STATUS_CANCELED]) ? (int) $value : self::STATUS_CANCELED;
            } elseif($key == 'reactionDate') {
                $data[$key] = max(0, !empty($value) && !is_numeric($value) ? strtotime($value) : (int) $value);
            } elseif(in_array($key, ['push', 'visible'])) {
                $data[$key] = (bool) $value;
            }
        }

        return $data;
    }

    /**
     * @return int
     */
    public function getParticipationId()
    {
        return $this->pcpId;
    }

    /**
     * @return int
     */
    public function getTeamId()
    {
        return $this->teamId;
    }

    /**
     * @return int
     */
    public function getConsumerId()
    {
        return $this->consumerId;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return bool
     */
    public function toBeAccepted()
    {
        return $this->getStatus() === self::STATUS_SENT;
    }

    /**
     * @return bool
     */
    public function isAccepted()
    {
        return $this->getStatus() === self::STATUS_ACCEPTED;
    }

    /**
     * @return bool
     */
    public function isCanceled()
    {
        return $this->getStatus() == self::STATUS_CANCELED;
    }

    /**
     * @return self
     */
    public function markAsAccepted()
    {
        $this->status = self::STATUS_ACCEPTED;
        $this->reactionDate = time();

        return $this;
    }

    /**
     * @return self
     */
    public function markAsCanceled()
    {
        $this->status = self::STATUS_CANCELED;
        $this->visible = false;
        $this->reactionDate = time();

        return $this;
    }

    /**
     * @return int
     */
    public function getReactionDate()
    {
        return $this->reactionDate;
    }

    /**
     * @return bool
     */
    public function hasToPush()
    {
        return $this->push;
    }

    /**
     * @return self
     */
    public function push()
    {
        $this->push = true;

        return $this;
    }

    /**
     * @return self
     */
    public function releasePush()
    {
        $this->push = false;

        return $this;
    }

    /**
     * @return bool
     */
    public function isVisible()
    {
        return $this->visible;
    }

    /**
     * @return self
     */
    public function makeVisible()
    {
        $this->visible = true;

        return $this;
    }

    /**
     * @return self
     */
    public function makeInvisible()
    {
        $this->visible = false;

        return $this;
    }

    /**
     * @return bool
     */
    public function valid()
    {
        return $this->getTeamId() > 0 && $this->getConsumerId() > 0;
    }

    /**
     * @param bool $withPrimaryKeys True to include primary keys data
     * @return array An array of columns and their values
     */
    public function getRecord($withPrimaryKeys = false)
    {
        $data = [];

        foreach($this->getDbColumns($withPrimaryKeys) as $col) {
            if(in_array($col, ['push', 'visible'])) {
                $data[$col] = (int) $this->{$col};
            } else {
                $data[$col] = $this->{$col};
            }
        }

        return $data;
    }

    /**
     * @return \LS\Consumer
     */
    public function getConsumer()
    {
        if($this->consumer === null) {
            $this->consumer = consumersLoyaltySuite::getConsumer($this->getConsumerId());
        }

        return $this->consumer;
    }
}