<?php
/**
 * Important: after initialization you must validate result using function "valid()"
 * Timeline:
 * 1. Participation created: $this->getTeam()->getCreateDate()
 * 2. N Days start awaiting: $this->isUpcoming()
 * 2.1. Consumer not accepted participation: $this->confirmationAwaiting()
 * 2.2. Consumer accepted participation: $this->startAwaiting()
 * 3. Active stage: $this->isParticipated()
 * 3.1. Consumer has to add activity today: $this->activityAwaiting()
 * 3.2. Consumer added activity today: $this->hasActivityToday()
 * 4. Active stage ended: $this->isEnded()
 * 4.1. Consumer can add missed data: $this->canAddActivities()
 * 4.2. Consumer can't add missed data: $this->isFinished()
 */
class PechConsumerTeam
{
    /** @var PechParticipation */
    private $participation;

    /** @var PechChallengeTeam */
    private $ct;

    /**
     * @param object $record
     */
    public function __construct($record)
    {
        if(isset($record->consumerId) && $record->consumerId > 0) {

            $this->participation = new PechParticipation($record);
            $team = new PechTeam($record);

            if($team->valid()) {
                $challenge = new PechChallenge($team->getChallengeId());
                if($challenge->valid()) {
                    $this->ct = new PechChallengeTeam($challenge, $team);
                }
            }
        }
    }

    /**
     * @return PechParticipation
     */
    public function getParticipation()
    {
        return $this->participation;
    }

    /**
     * @return PechChallengeTeam
     */
    public function getChallengeTeam()
    {
        return $this->ct;
    }

    /**
     * @return PechTeam
     */
    public function getTeam()
    {
        return $this->ct->getTeam();
    }

    /**
     * @return PechChallenge
     */
    public function getChallenge()
    {
        return $this->ct->getChallenge();
    }

    /**
     * @return int
     */
    public function getConsumerId()
    {
        return $this->participation->getConsumerId();
    }

    /**
     * @return int
     */
    public function getChallengeId()
    {
        return $this->ct->getChallengeId();
    }

    /**
     * @return int
     */
    public function getParticipationId()
    {
        return $this->participation->getParticipationId();
    }

    /**
     * @return int
     */
    public function getTeamId()
    {
        return $this->ct->getTeamId();
    }

    /**
     * @return bool
     */
    public function confirmationAwaiting()
    {
        return $this->participation->toBeAccepted() && !$this->isFinished();
    }

    /**
     * @return bool
     */
    public function startAwaiting()
    {
        return $this->participation->isAccepted() && $this->ct->isUpcoming();
    }

    /**
     * @return bool
     */
    public function isStarted()
    {
        return $this->participation->isAccepted() && !$this->ct->isUpcoming();
    }

    /**
     * @return bool
     */
    public function isParticipated()
    {
        return $this->participation->isAccepted();
    }

    /**
     * @return bool
     */
    public function isEnded()
    {
        return $this->ct->isEnded();
    }

    /**
     * @return bool
     */
    public function canAcceptOrReject()
    {
        return
            $this->participation->toBeAccepted()
            && !$this->participation->isCanceled()
            && !$this->isFinished();
    }

    /**
     * @return bool
     */
    public function canAddActivities()
    {
        return $this->isStarted() && !$this->isFinished();
    }

    /**
     * @return bool
     */
    public function isFinished()
    {
        return $this->ct->isFinished();
    }

    /**
     * @param PechActivity[] $todayActivities
     * @return bool
     */
    private function hasActivityToday($todayActivities)
    {
        foreach($todayActivities as $activity) {
            if($activity->getParticipationId() === $this->getParticipationId()) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param PechActivity[] $todayActivities
     * @return bool
     */
    public function activityAwaiting($todayActivities)
    {
        return !$this->ct->isEnded() && $this->canAddActivities() && !$this->hasActivityToday($todayActivities);
    }

    /**
     * @return bool
     */
    public function canPush()
    {
        return $this->ct->getChallenge()->pushEnabled() && $this->canAddActivities();
    }

    /**
     * @return bool
     */
    public function hasToPush()
    {
        return $this->participation->hasToPush() && $this->canPush();
    }

    /**
     * @return bool
     */
    public function canAddParticipants()
    {
        return $this->participation->isAccepted() && !$this->isEnded();
    }

    /**
     * @return bool
     */
    public function canStopParticipation()
    {
        return $this->participation->isAccepted() && !$this->isFinished();
    }

    /**
     * @return bool
     */
    public function canHide()
    {
        return
            $this->participation->isVisible()
            && $this->participation->isAccepted()
            && $this->isFinished();
    }

    /**
     * @return bool
     */
    public function valid()
    {
        return
            $this->getConsumerId()
            && $this->ct->getChallenge()
            && $this->ct->getChallenge()->isActive()
            && !$this->participation->isCanceled() // shouldn't be canceled
            && !($this->participation->toBeAccepted() && $this->isFinished()) // finished but still not accepted/rejected
            && $this->participation->isVisible(); // not hidden
    }
}