<?php
class pechPcpStatisticsTableLoyaltySuite extends LS\WP_List_Table
{
    /** @var array */
    protected $_column_headers;

    public function __construct()
    {
        parent::__construct([
            'singular' => 'pech-participant',
            'plural'   => 'pech-participants'
        ]);
    }

    /**
     * @return array
     */
    public function get_columns()
    {
        return [
            'cb'           => '<input type="checkbox" />',
            'pcpId'        => 'Participation ID',
            'postId'       => 'Challenge',
            'teamName'     => 'Team name',
            'consumerId'   => 'Participant name',
            'status'       => 'Status',
            'reactionDate' => 'Reaction date'
        ];
    }

    /**
     * @return array
     */
    public function get_sortable_columns()
    {
        return [
            'pcpId'        => ['pcpId', true],
            'postId'       => ['postId', true],
            'teamName'     => ['teamName', true],
            'consumerId'   => ['consumerId', true],
            'status'       => ['status', false],
            'reactionDate' => ['reactionDate', false]
        ];
    }

    /**
     * @param PechConsumerTeam $item
     * @return string
     */
    public function column_cb($item)
    {
        return sprintf('<input type="checkbox" name="pcpId[]" value="%s" />', $item->getParticipationId());
    }

    public function prepare_items()
    {
        $options = $this->get_table_options();
        $postId = LS()->getParam('postId', 'id');
        $companyId = LS()->getParam('companyId', 'id');
        $teamId = LS()->getParam('teamId', 'id');

        $records = PechHelper::getParticipantsStatistics(30, $options['page'], $options['orderBy'], $options['order'], [
            'postId'    => $postId,
            'companyId' => $companyId,
            'teamId'    => $teamId
        ]);

        $this->set_pagination_args([
            "total_items" => $records['totalCount'],
            "total_pages" => $records['totalPages'],
            "per_page"    => $records['itemsPerPage']
        ]);

        $this->items = $records['rows'];
        $this->_column_headers = [$this->get_columns(), [], $this->get_sortable_columns()];
    }

    /**
     * @return array
     */
    public function get_bulk_actions()
    {
        return [
            'pech-delete-pcp' => 'Delete'
        ];
    }

    /**
     * @param PechConsumerTeam $item
     * @param string $columnName
     * @return string
     */
    public function column_default($item, $columnName)
    {
        switch($columnName) {
            case 'pcpId' :
                return $item->getParticipationId();
            case 'postId' :
                return '<a href="' . get_edit_post_link($item->getChallengeId()) . '">' . $item->getChallenge()->getPostTitle() . '</a>';
            case 'teamName' :
                return esc_html($item->getTeam()->getTeamName());
            case 'consumerId' :
                return '<a href="' . consumersAdminLoyaltySuite::consumerEditLink($item->getConsumerId()) . '#ls-pech/pech-statistics-tab' . '">' . esc_html(consumersLoyaltySuite::getConsumerNiceName($item->getConsumerId())) . '</a>';
            case 'reactionDate' :
                return date('d.m.Y H:i', $item->getParticipation()->getReactionDate() + TIMEZONE_DIFF);
            case 'status' :
                switch($item->getParticipation()->getStatus()) {

                    case PechParticipation::STATUS_ACCEPTED :
                        $status = '<span style="color: green;">Accepted</span>';
                        break;

                    case PechParticipation::STATUS_CANCELED :
                        $status = '<span style="color: red;">Canceled</span>';
                        break;

                    case PechParticipation::STATUS_SENT :
                    default :
                        $status = '<span style="color: orange;">Sent</span>';
                        break;
                }
                return $status;
            default:
                return ' ';
        }
    }
}