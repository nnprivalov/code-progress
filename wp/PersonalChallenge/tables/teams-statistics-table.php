<?php
class pechTeamsStatisticsTableLoyaltySuite extends LS\WP_List_Table
{
    /** @var array */
    protected $_column_headers;

    /** @var array[int]int */
    private $pcps = [];

    public function __construct()
    {
        parent::__construct([
            'singular' => 'pech-team',
            'plural'   => 'pech-teams'
        ]);
    }

    /**
     * @return array
     */
    public function get_columns()
    {
        return [
            'cb'         => '<input type="checkbox" />',
            'teamId'     => 'Team ID',
            'postId'     => 'Challenge',
            'teamName'   => 'Team name',
            'createDate' => 'Create date',
            'startDate'  => 'Start date',
            'pcps'       => 'Participants',
            'details'    => ''
        ];
    }

    /**
     * @return array
     */
    public function get_sortable_columns()
    {
        return [
            'teamId'     => ['teamId', true],
            'postId'     => ['postId', true],
            'teamName'   => ['teamName', true],
            'createDate' => ['createDate', true],
            'startDate'  => ['startDate', true],
            'pcps'       => ['pcps', true]
        ];
    }

    /**
     * @param PechTeam $item
     * @return string
     */
    public function column_cb($item)
    {
        return sprintf('<input type="checkbox" name="teamId[]" value="%s" />', $item->getTeamId());
    }

    public function prepare_items()
    {
        $options = $this->get_table_options();
        $postId = LS()->getParam('postId', 'id');
        $companyId = LS()->getParam('companyId', 'id');

        $records = PechHelper::getTeamsStatistic(30, $options['page'], $options['orderBy'], $options['order'], [
            'postId'    => $postId,
            'companyId' => $companyId
        ]);

        $this->set_pagination_args([
            "total_items" => $records['totalCount'],
            "total_pages" => $records['totalPages'],
            "per_page"    => $records['itemsPerPage']
        ]);

        $this->items = $records['rows'];
        $this->pcps = $records['pcps'];
        $this->_column_headers = [$this->get_columns(), [], $this->get_sortable_columns()];
    }

    /**
     * @return array
     */
    public function get_bulk_actions()
    {
        return [
            'pech-delete-team' => 'Delete'
        ];
    }

    /**
     * @param PechTeam $item
     * @param string $columnName
     * @return string
     */
    public function column_default($item, $columnName)
    {
        switch($columnName) {
            case 'teamId' :
                return $item->getTeamId();
            case 'postId' :
                return '<a href="' . get_edit_post_link($item->getChallengeId()) . '">' . get_the_title($item->getChallengeId()) . '</a>';
            case 'teamName' :
                return esc_html($item->getTeamName());
            case 'createDate' :
                return date('d.m.Y H:i', $item->getCreateDate() + TIMEZONE_DIFF);
            case 'startDate' :
                return date('d.m.Y H:i', $item->getStartDate() + TIMEZONE_DIFF);
            case 'pcps' :
                return '<a href="' . admin_url('edit.php?post_type=1x1&page=pech-participants&teamId=' . $item->getTeamId()) . '">' . $this->pcps[$item->getTeamId()] . '</a>';
            case 'details' :
                return '<a href="' . admin_url('edit.php?post_type=1x1&page=pech-edit-team&teamId=' . $item->getTeamId()) . '">Edit</a>';
            default:
                return ' ';
        }
    }
}