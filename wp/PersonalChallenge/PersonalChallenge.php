<?php
class PersonalChallenge extends LS\Module
{
    /** @var PechConsumerTeam[] */
    private $myTeams;

    /**
     * @return string
     */
    public function getVersion()
    {
        return '4.5.7';
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return 'Personal Challenges';
    }

    /**
     * @internal
     */
    public function afterActivate()
    {
        require_once __DIR__ . '/database.php';
        fitcoPersonalChallengeActualize();
    }

    /**
     * @internal
     * @param string $oldVersion
     * @param string $newVersion
     */
    public function afterUpdate($oldVersion, $newVersion)
    {
        require_once __DIR__ . '/database.php';
        fitcoPersonalChallengeActualize($oldVersion);
    }

    /**
     * @internal
     */
    public function onUninstall()
    {
        require_once __DIR__ . '/database.php';
        fitcoPersonalChallengeUninstall();
    }

    public function load()
    {
        require_once __DIR__ . '/classes/PechTeam.php';
        require_once __DIR__ . '/classes/PechParticipation.php';
        require_once __DIR__ . '/classes/PechActivity.php';
        require_once __DIR__ . '/classes/PechChallenge.php';
        require_once __DIR__ . '/classes/PechRank.php';
        require_once __DIR__ . '/classes/PechTeamRank.php';
        require_once __DIR__ . '/classes/PechChallengeTeam.php';
        require_once __DIR__ . '/classes/PechConsumerTeam.php';
        require_once __DIR__ . '/classes/PechHelper.php';

        add_action('init', [$this, 'initEarly'], 1);
        add_action('init', [$this, 'init']);
        add_action('wp_enqueue_scripts', [$this, 'printScripts']);
        add_action('ls_consumer_deleted', [$this, 'consumerDeleted']);
        add_action('get_footer', [$this, 'pushItMessage']);

        $action = $this->getParam('action', 'text');

        if(!empty($action)) {
            add_action('wp_ajax_' . $action, [$this, 'ajaxController']);
            add_action('wp_ajax_nopriv_' . $action, [$this, 'ajaxController']);
        }

        // AT integration
        if(LS()->isModuleActive('at2')) {
            add_action('pech_accepted', [$this, 'atPechAccepted'], 10, 2);
        }

        // Bonus Points integration
        if(LS()->isModuleActive('bonusPoints')) {
            // @TODO Change this after 08.2019
            if (LS()->isModuleActive('bonusPoints', '1.4.0')) {
                add_action('ls_bp_registered_triggers', [$this, 'bpRegisteredTriggers']);
            } else {
                add_action('ls_bp_trigger_types', [$this, 'bpTriggerTypes']);
                add_filter('ls_bp_trigger_idents', [$this, 'bpTriggerIdents']);
            }
            add_action('pech_accepted', [$this, 'bpPechAccepted'], 10, 2);
        }
    }

    public function initShortcodes()
    {
        add_shortcode('personal-challenge', [$this, 'pechShortcode']);
        add_shortcode('personal-challenge-title', [$this, 'challengeTitleShortcode']);
        add_shortcode('personal-challenge-period', [$this, 'periodShortcode']);
        add_shortcode('personal-challenge-days', [$this, 'daysShortcode']);
        add_shortcode('personal-challenge-alert-box', [$this, 'alertBoxShortcode']);
        add_shortcode('personal-challenge-leaderboard', [$this, 'leaderboardShortcode']);
        add_shortcode('personal-challenges-overview', [$this, 'overviewShortcode']);
    }

    /**
     * @internal
     */
    public function initEarly()
    {
        register_post_type('1x1', [
            'labels'              => [
                'name'          => 'Challenges',
                'singular_name' => 'Challenge',
                'add_new_item'  => 'Neuen Challenge hinzufügen',
                'edit_item'     => 'Challenge bearbeiten',
                'new_item'      => 'Neuer Challenge',
                'all_items'     => 'Alle Challenges',
                'view_item'     => 'Challenge anschauen',
                'search_items'  => 'Challenges durchsuchen',
                'menu_name'     => 'Challenges'
            ],
            'public'              => true,
            'publicly_queryable'  => true,
            'show_ui'             => true,
            'show_in_menu'        => true,
            'query_var'           => true,
            'capability_type'     => 'page',
            'hierarchical'        => false,
            'rewrite'             => ['slug' => apply_filters('ls_pech_slug', 'mach-mit')],
            'exclude_from_search' => false,
            'supports'            => ['title', 'subtitles', 'editor', 'thumbnail', 'excerpt', 'custom-fields', 'revisions'],
            'taxonomies'          => ['post_tag', 'category']
        ]);

        if(LS()->isModuleActive('at2')) {
            $this->initPechAT();
        }
    }

    /**
     * @internal
     */
    public function printScripts()
    {
        wp_enqueue_style('pech', $this->getUrl2Module() . 'css/challenge.css', [], $this->getVersion());
        wp_enqueue_script('pech-challenge', $this->getUrl2Module() . 'js/challenge.js', ['jquery', 'fancybox'], $this->getVersion(), true);
        wp_localize_script('ls', 'PECH', [
            'loadingFailed'  => __('Challenge details können nicht geladen werden', 'ls'),
            'emailMissed'    => __('Bitte lade mindestens 1 Teilnehmer zum Teamwettbewerb ein', 'ls'),
            'teamNameMissed' => __('Bitte gib den Namen für das Team ein', 'ls'),
            'pushSent'       => __('Benachrichtigung versendet', 'ls')
        ]);
    }

    /**
     * @internal
     */
    public function init()
    {
        if($this->hasAction()) {
            if($this->isAction('pech-create')) {
                $this->createTeamAction();
            } elseif($this->isAction('pech-add-participants')) {
                $this->addParticipantsAction();
            } elseif($this->isAction('pech-cancel')) {
                $this->cancelTeamAction();
            } elseif($this->isAction('pech-accept')) {
                $this->acceptTeamAction();
            } elseif($this->isAction('pech-refuse')) {
                $this->refuseTeamAction();
            } elseif($this->isAction('pech-push-cancel')) {
                $this->cancelPushAction();
            } elseif($this->isAction('pech-join')) {
                $this->joinTeamAction();
            }
        }
    }

    /**
     * @param int $teamId
     * @param int $challengeId
     * @param int $consumerId
     */
    private function createAcceptedParticipation($teamId, $challengeId, $consumerId)
    {
        $pcp = new PechParticipation([
            'teamId'       => $teamId,
            'consumerId'   => $consumerId,
            'status'       => PechParticipation::STATUS_ACCEPTED,
            'reactionDate' => time(),
            'visible'      => true
        ]);

        if($pcp->valid() && PechHelper::createParticipation($pcp) > 0) {
            do_action('pech_accepted', $challengeId, $consumerId);
            \LS\ConsumersHelper::insertLog('pech_accepted', $consumerId, $teamId);
        } else {
            \LS\Library::logBreakpoint();
        }
    }

    /**
     * @param int $teamId
     * @param int $consumerId
     * @return bool
     */
    private function createPendingParticipation($teamId, $consumerId)
    {
        $success = false;

        $pcp = new PechParticipation([
            'teamId'       => $teamId,
            'consumerId'   => $consumerId,
            'status'       => PechParticipation::STATUS_SENT,
            'reactionDate' => time(),
            'visible'      => true
        ]);

        if($pcp->valid() && PechHelper::createParticipation($pcp) > 0) {
            $success = true;
        } else {
            \LS\Library::logBreakpoint();
        }

        return $success;
    }

    /**
     * @param int $consumerId
     * @param int $teamId
     * @param int $challengeId
     * @return bool
     */
    private function reAcceptParticipation($teamId, $challengeId, $consumerId)
    {
        $pcp = PechHelper::getConsumerParticipationByTeamId($consumerId, $teamId);

        if(!empty($pcp)) {

            PechHelper::updateParticipation(
                $pcp
                    ->markAsAccepted()
                    ->makeVisible()
            );

            do_action('pech_accepted', $challengeId, $consumerId);
            do_action('pech_consumer_action', $consumerId);

            \LS\ConsumersHelper::insertLog('pech_accepted', $consumerId, $teamId);

            return true;
        }

        return false;
    }

    /**
     * @param PechChallenge $challenge
     * @param \LS\ConsumerInterface $consumer
     * @param int $startDate
     * @return bool
     */
    private function sendInvitationEmail(PechChallenge $challenge, \LS\ConsumerInterface $consumer, $startDate)
    {
        $sent = false;
        $emailSubject = $challenge->getInvitationEmailSubject();
        $emailMessage = $challenge->getInvitationEmailMessage($consumer->getConsumerId(), $startDate);

        if(!empty($emailMessage) && !empty($emailSubject) && !empty($consumer->getEmail())) {
            $sent = PechHelper::sendComCenterEmail($consumer->getEmail(), $emailSubject, $emailMessage, $consumer);
            \LS\ConsumersHelper::saveEmailLog($emailSubject, $consumer->getConsumerId(), 'fitco_pech', $sent);
        }

        return $sent;
    }

    private function consumerAccessCheck()
    {
        if(!consumersLoyaltySuite::isConsumerAuthorized()) {
            $this->setError(__('Die Challenge ist leider nur für registrierte Teilnehmer sichtbar.', 'ls'));
            self::logBreakpoint();
            \LS\Library::redirect(remove_query_arg(['action']));
        }
    }

    private function createTeamAction()
    {
        $this->consumerAccessCheck();

        $errors = [];
        $startDate = $this->getParam('startDate', 'date');
        $teamName = trim($this->getParam('teamName', 'text'));
        $participants = $this->getParam('pechParticipants', 'array');
        $currentConsumer = consumersLoyaltySuite::getAuthorizedConsumer();
        $challenge = new PechChallenge($this->getParam('pechId', 'id'));

        if(!$challenge->isActive($e)) {
            $errors[] = $e;
        } elseif(!PechHelper::possibleToCreateTeamOn($startDate, $challenge)) {
            $error = sprintf(__('Die Challenge endet am %s. Aktuell hast du leider nicht mehr genügend Zeit, die Challenge durchzuführen.', 'ls'), date_i18n('d.m.Y', $challenge->getEndDate()));
        } elseif(!PechHelper::singleParticipationRuleCompleted($currentConsumer->getConsumerId(), $challenge)) {
            self::logBreakpoint();
            $error = __('Du bist bereits Mitglied in einem anderen Team', 'ls');
        }

        $participants = self::extractEmails($participants);
        $participations = [];

        if(empty($error)) {

            $hasIncorrectParticipants = false;

            foreach($participants as $email) {
                $consumer = \LS\ConsumersHelper::getConsumerByEmail($email);
                if($consumer && $consumer->active()) {
                    $participations[] = $consumer;
                } else {
                    $hasIncorrectParticipants = true;
                }
            }

            if($hasIncorrectParticipants) {
                $errors[] = __('Du hast leider einen/oder mehrere falsche E-Mail Adressen angegeben. Bitte korrigiere die Eingabe.', 'ls');
            } elseif(in_array($currentConsumer->getEmail(), $participants)) {
                $errors[] = __('Du kannst keine eigene E-Mail-Adresse hinzufügen.', 'ls');
            }
        }

        if(empty($errors)) {

            $team = new PechTeam([
                'postId'     => $challenge->getChallengeId(),
                'startDate'  => $startDate,
                'createTime' => time(),
                'teamName'   => $teamName
            ]);

            if($team->valid() && ($teamId = PechHelper::createTeam($team)) > 0) {

                $this->createAcceptedParticipation($teamId, $challenge->getChallengeId(), $currentConsumer->getConsumerId());

                /** @var \LS\ConsumerInterface $consumer */
                foreach($participations as $consumer) {
                    if($this->createPendingParticipation($teamId, $consumer->getConsumerId())) {
                        $this->sendInvitationEmail($challenge, $consumer, $startDate);
                    }
                }

                do_action('pech_consumer_action', $currentConsumer->getConsumerId());

                $this->setAlert(__('Geschafft! Das Team ist erstellt. Alle Teilnehmer erhalten nun eine Einladung.', 'ls'));
                \LS\Library::redirect($challenge->getChallengeLink() . '#pech/' . $teamId);
            }

            self::logBreakpoint();

            $errors[] = __('Das Team kann leider nicht erstellt werden', 'ls');
        }

        foreach ($errors as $error) {
            $this->setError($error);
        }
    }

    /**
     * Extract array of emails even if the single record is something like:
     * "Bob Smith" <bob@company.com>, joe@company.com, "John Doe"<john@company.com>
     *
     * @param string[] $emails
     * @return string[]
     */
    private static function extractEmails($emails)
    {
        $emailsString = implode(',', array_map('trim', $emails));
        $emails = [];

        if(preg_match_all('/\s*"?([^><,;"]+)"?\s*((?:<[^><,;]+>)?)\s*/', $emailsString, $matches, PREG_SET_ORDER) > 0) {
            foreach($matches as $m) {
                $emails[] = empty($m[2]) ? $m[1] : trim($m[2], '<>');
            }
        }

        return array_unique(array_filter(array_map('trim', $emails)));
    }

    private function addParticipantsAction()
    {
        $this->consumerAccessCheck();

        $myTeam = $this->getMyTeam($this->getParam('id', 'id'));

        if(!$myTeam || !$myTeam->canAddParticipants()) {
            self::logBreakpoint();
            \LS\Library::redirect(remove_query_arg(['action', 'id']));
        }

        $error = '';
        $currentConsumer = consumersLoyaltySuite::getAuthorizedConsumer();
        $newParticipants =
        $existsConsumersIds = [];

        foreach(PechHelper::getTeamParticipations($myTeam->getTeam()) as $pcp) {
            $existsConsumersIds[] = $pcp->getConsumerId();
        }

        $participants = self::extractEmails($this->getParam('pechParticipants', 'array'));

        if(empty($participants)) {
            $error = __('Bitte lade mindestens 1 Teilnehmer zum Teamwettbewerb ein', 'ls');
        } else {

            $hasIncorrectParticipants =
            $hasOldParticipants = false;

            foreach($participants as $email) {
                $consumer = \LS\ConsumersHelper::getConsumerByEmail($email);
                if($consumer && $consumer->active()) {
                    if(!in_array($consumer->getConsumerId(), $existsConsumersIds)) {
                        $newParticipants[] = $consumer;
                    } else {
                        $hasOldParticipants = true;
                    }
                } else {
                    $hasIncorrectParticipants = true;
                }
            }

            if($hasIncorrectParticipants) {
                $error = __('Du hast leider einen/oder mehrere falsche E-Mail Adressen angegeben. Bitte korrigiere die Eingabe.', 'ls');
            } elseif(in_array($currentConsumer->getEmail(), $participants)) {
                $error = __('Du kannst keine eigene E-Mail-Adresse hinzufügen', 'ls');
            } elseif(empty($newParticipants)) {
                if($hasOldParticipants) {
                    $error = __('Der ausgewählte Teilnehmer ist bereits Mitglied in einem anderen Team', 'ls');
                } else {
                    $error = __('Bitte lade mindestens 1 Teilnehmer zum Teamwettbewerb ein', 'ls');
                }
            }
        }

        if(empty($error)) {

            /** @var \LS\ConsumerInterface $consumer */
            foreach($newParticipants as $consumer) {
                if($this->createPendingParticipation($myTeam->getTeamId(), $consumer->getConsumerId())) {
                    $this->sendInvitationEmail($myTeam->getChallenge(), $consumer, $myTeam->getTeam()->getStartDate());
                }
            }

            do_action('pech_consumer_action', $currentConsumer->getConsumerId());

            if(count($newParticipants) === 1) {
                $this->setAlert(sprintf(__('Teilnehmer %s hinzugefügt. Der Teilnehmer erhält eine Einladung.', 'ls'), consumersLoyaltySuite::getConsumerNiceName($newParticipants[0])));
            } else {
                $this->setAlert(__('Teilnehmer hinzugefügt. Alle Teilnehmer erhalten nun eine Einladung.', 'ls'));
            }

            \LS\Library::redirect(remove_query_arg(['action']) . '#pech/' . $myTeam->getTeamId() . '/pcp-added');
        }

        $this->setError($error);
    }

    private function acceptTeamAction()
    {
        $this->consumerAccessCheck();

        $myTeam = $this->getMyTeam($this->getParam('id', 'id'));

        if(!$myTeam || !$myTeam->canAcceptOrReject()) {
            self::logBreakpoint();
            $this->setError(__('Eine Teilnahme ist nicht mehr möglich.', 'ls'));
        } elseif(!PechHelper::singleParticipationRuleCompleted($myTeam->getConsumerId(), $myTeam->getChallenge())) {
            self::logBreakpoint();
            $this->setError(__('Du bist bereits Mitglied in einem anderen Team', 'ls'));
        } else {

            PechHelper::updateParticipation(
                $myTeam->getParticipation()->markAsAccepted()
            );

            // Cancel all the other "open to access" teams
            if($myTeam->getChallenge()->isSingleParticipation()) {

                foreach(PechHelper::getConsumerParticipations($myTeam->getConsumerId(), $myTeam->getChallengeId()) as $pcp) {
                    if($pcp->toBeAccepted()) {
                        $pcp->markAsCanceled();
                        PechHelper::updateParticipation($pcp);
                    }
                }
            }

            do_action('pech_accepted', $myTeam->getChallengeId(), $myTeam->getConsumerId());
            do_action('pech_consumer_action', $myTeam->getConsumerId());

            \LS\ConsumersHelper::insertLog('pech_accepted', $myTeam->getConsumerId(), $myTeam->getTeamId());

            $this->setAlert(__('Du hast die Team-Einladung akzeptiert', 'ls'));

            \LS\Library::redirect(remove_query_arg(['action', 'id']) . '#pech/' . $myTeam->getTeamId());
        }

        \LS\Library::redirect(remove_query_arg(['action', 'id']));
    }

    private function refuseTeamAction()
    {
        $this->consumerAccessCheck();

        $myTeam = $this->getMyTeam($this->getParam('id', 'id'));

        if(!$myTeam || !$myTeam->canAcceptOrReject()) {
            self::logBreakpoint();
            $this->setError(__('Die Teilnahme ist nicht mehr möglich', 'ls'));
        } else {

            PechHelper::updateParticipation($myTeam->getParticipation()->markAsCanceled());

            do_action('pech_canceled', $myTeam);
            do_action('pech_consumer_action', $myTeam->getConsumerId());

            \LS\ConsumersHelper::insertLog('pech_canceled', $myTeam->getConsumerId(), $myTeam->getTeamId());

            $this->setAlert(__('Du hast die Team-Einladung abgesagt', 'ls'));
        }

        \LS\Library::redirect(remove_query_arg(['action', 'id']));
    }

    private function cancelTeamAction()
    {
        $this->consumerAccessCheck();

        $myTeam = $this->getMyTeam($this->getParam('id', 'id'));

        if(!$myTeam || !$myTeam->canStopParticipation()) {
            self::logBreakpoint();
            $this->setError(__('Die Teilnahme kann leider nicht beendet werden', 'ls'));
        } else {

            PechHelper::updateParticipation($myTeam->getParticipation()->markAsCanceled());

            $consumerId = consumersLoyaltySuite::getAuthorizedConsumerId();

            do_action('pech_canceled', $myTeam);
            do_action('pech_consumer_action', $consumerId);

            \LS\ConsumersHelper::insertLog('pech_canceled', $consumerId, $myTeam->getTeamId());

            // if team is invisible to all participants -> delete it
            // but only if this is not free-entrance challenge
            if(!$myTeam->getChallenge()->isFreeEntrance() && !PechHelper::hasTeamVisibleParticipants($myTeam->getTeam())) {
                PechHelper::deleteTeam($myTeam->getTeam());
            }

            $this->setAlert(__('Du hast die Teilnahme beendet', 'ls'));
        }

        \LS\Library::redirect(remove_query_arg(['action', 'id']));
    }

    private function joinTeamAction()
    {
        $this->consumerAccessCheck();

        $team = PechHelper::getTeam($this->getParam('id', 'id'));

        if(!$team) {
            self::logBreakpoint();
            $this->setError(__('Teilnahme ist nicht möglich', 'ls'));
        } else {

            $challenge = new PechChallenge($team->getChallengeId());

            if(!$challenge->isActive() || !$challenge->isFreeEntrance()) {
                $this->setError(__('Teilnahme ist nicht möglich', 'ls'));
            } else {

                $challengeTeam = new PechChallengeTeam($challenge, $team);

                if($challengeTeam->isEnded()) {
                    self::logBreakpoint();
                    $this->setError(__('Teilnahme ist nicht möglich', 'ls'));
                } else {

                    $consumerId = consumersLoyaltySuite::getAuthorizedConsumerId();

                    if(!PechHelper::singleParticipationRuleCompleted($consumerId, $challenge)) {
                        self::logBreakpoint();
                        $this->setError(__('Du bist bereits Mitglied in einem anderen Team', 'ls'));
                    } else {

                        if(!$this->reAcceptParticipation($team->getTeamId(), $team->getChallengeId(), $consumerId)) {
                            $this->createAcceptedParticipation($team->getTeamId(), $challenge->getChallengeId(), $consumerId);
                            do_action('pech_consumer_action', $consumerId);
                        }

                        $this->setAlert(__('Willkommen im Team', 'ls'));
                        \LS\Library::redirect(remove_query_arg(['action', 'id']) . '#pech/' . $team->getTeamId());
                    }
                }
            }
        }

        \LS\Library::redirect(remove_query_arg(['action', 'id']));
    }

    private function cancelPushAction()
    {
        $this->consumerAccessCheck();

        $myTeam = $this->getMyTeam($this->getParam('id', 'id'));

        if(!$myTeam) {
            self::logBreakpoint();
            $this->setError(__('Keine Push-Info an Team-Teilnehmer möglich', 'ls'));
        } else {
            PechHelper::updateParticipation($myTeam->getParticipation()->releasePush());
            do_action('pech_consumer_action', $myTeam->getConsumerId());
        }

        \LS\Library::redirect(remove_query_arg(['action', 'id']));
    }

    /**
     * @internal
     * @param array $atts
     * @return int|string
     */
    public function challengeTitleShortcode($atts)
    {
        $challengeId = isset($atts['id']) ? (int) $atts['id'] : get_the_ID();

        return (new PechChallenge($challengeId))->getPostTitle();
    }

    /**
     * @internal
     * @param array $atts
     * @return string
     */
    public function pechShortcode($atts)
    {
        $challengeId = isset($atts['id']) ? (int) $atts['id'] : get_the_ID();
        $consumerId = consumersLoyaltySuite::getAuthorizedConsumerId();
        $myTeams =
        $myRanks = [];

        if($consumerId < 1) {
            self::logBreakpoint();
            return current_user_can('edit_users') ? 'Admin warning: [personal-challenge] called on public page (ID: ' . get_the_ID() . ')' : '';
        }

        $challenge = new PechChallenge($challengeId);

        if(!$challenge->isActive($error)) {
            return $error;
        }

        foreach($this->getMyTeamsInChallenge($challengeId) as $team) {
            $myTeams[$team->getTeamId()] = $team;
            if($team->isEnded()) {
                $myRanks[$team->getTeamId()] = PechHelper::getParticipantRank($team);
            }
        }

        // create an array of all visible challenge teams
        if($challenge->isFreeEntrance()) {

            $challengeTeams = array_map(static function(PechTeam $team) use ($challenge) {
                return new PechChallengeTeam($challenge, $team);
            }, PechHelper::getTeams($challengeId));

            $myPcpTeams = array_filter($myTeams, static function(PechConsumerTeam $ci) {
                return $ci->isParticipated();
            });

            $challengeTeams = array_filter($challengeTeams, static function(PechChallengeTeam $ci) use ($challenge, $myPcpTeams) {

                // if consumer participated in the challenge - show it
                if(isset($myPcpTeams[$ci->getTeamId()])) {
                    return true;
                }

                // consumer can't join ended teams
                if($ci->isEnded()) {
                    return false;
                }

                // consumer can't join 2nd team if this is single participation challenge
                if($challenge->isSingleParticipation() && !empty($myPcpTeams)) {
                    return false;
                }

                return true;
            });

        } else {
            $challengeTeams = array_map(static function(PechConsumerTeam $ci) use ($challenge) {
                return new PechChallengeTeam($challenge, $ci->getTeam());
            }, $myTeams);
        }

        // release pushes related to the displayed challenge
        /** @var PechConsumerTeam $team */
        foreach ($myTeams as $myTeam) {
            if($myTeam->hasToPush()) {
                PechHelper::updateParticipation(
                    $myTeam->getParticipation()->releasePush()
                );
            }
        }

        return (new LS\Template(__DIR__ . '/views/frontend'))
            ->assign('challenge', $challenge)
            ->assign('challengeTeams', $challengeTeams)
            ->assign('myTeams', $myTeams)
            ->assign('canCreateTeam', PechHelper::canConsumerCreateTeam($consumerId, $challenge))
            ->assign('todayActivities', PechHelper::getConsumerActivitiesForToday($consumerId))
            ->assign('myRanks', $myRanks)
            ->render('challenge.phtml', false);
    }

    /**
     * @internal
     * @param array $atts
     * @return int
     */
    public function periodShortcode($atts)
    {
        $challengeId = isset($atts['id']) ? (int) $atts['id'] : get_the_ID();
        $challenge = new PechChallenge($challengeId);

        return
            $challenge && $challenge->getStartDate() > 0 && $challenge->getEndDate() > 0
                ? date('d.m.Y', $challenge->getStartDate()) . ' - ' . date('d.m.Y', $challenge->getEndDate())
                : '—';
    }

    /**
     * @internal
     * @param array $atts
     * @return int
     */
    public function daysShortcode($atts)
    {
        $challengeId = isset($atts['id']) ? (int) $atts['id'] : get_the_ID();
        $challenge = new PechChallenge($challengeId);

        return $challenge ? $challenge->getActivityDays() : 0;
    }

    /**
     * @internal
     * @return string
     */
    public function alertBoxShortcode()
    {
        if(($content = wp_cache_get('pech_challenge_alert_box', 'fitco')) === false) {

            $challenge = PechHelper::getHomepageChallenge();

            if(!$challenge) {
                $content = '';
            } else {

                /** @var fitcoLoyaltySuite $fitco */
                $fitco = LS()->getModule('fitco');
                $link = $challenge->getChallengeLink();
                $bottom = '';
                $challengesPageId = $fitco->getOption('challengesPageId');

                if($challengesPageId > 0) {
                    $allPostsLink = FitcoHelper::getPermalink($challengesPageId);
                    if(!empty($allPostsLink)) {
                        $bottom = '<a class="homebox-button" href="' . esc_url($allPostsLink) . '">' . __('Checke deine Challenges', 'ls') . '</a>';
                    }
                }

                ob_start();

                FitcoHelper::homeBox($challenge->getPost(), [
                    'contentPos' => 'right',
                    'class'      => 'pech',
                    'label'      => __('Challenge Tipp des Monats', 'ls'),
                    'link'       => $link,
                    'buttonText' => __('Challenge öffnen', 'ls'),
                    'bottom'     => $bottom
                ]);

                $content = ob_get_clean();
            }

            if($challenge) {

                /** @var acmLoyaltySuite $acm */
                $acm = LS()->getModule('acm');

                if(!$acm || !$acm->isPostProtected($challenge->getChallengeId())) {
                    wp_cache_add('pech_challenge_alert_box', $content, 'fitco', HOUR_IN_SECONDS);
                } else {
                    _doing_it_wrong(__FUNCTION__, 'Private 1:1 Challenge marked as homepage challenge.', '3.0.1');
                }
            }
        }

        return $content;
    }

    /**
     * @internal
     * @param $atts
     * @return string
     */
    public function leaderboardShortcode($atts)
    {
        $consumer = consumersLoyaltySuite::getAuthorizedConsumer();

        if(!$consumer) {
            self::logBreakpoint();
            return current_user_can('edit_users') ? 'Admin warning: [personal-challenge-leaderboard] called on public page' : '';
        }

        $challenge = new PechChallenge(isset($atts['id']) ? (int) $atts['id'] : get_the_ID());

        if(!$challenge->isActive()) {
            return __('Da die Challenge noch nicht aktiv ist, kann eine Ergebnisliste noch nicht angezeigt werden!', 'ls');
        }

        $myTeams = array_map(static function(PechConsumerTeam $myTeam) {
            return $myTeam->getTeamId();
        }, $this->getMyTeams());

        $leaderboard = [];
        $companyId = empty($atts['company_based']) ? null : $consumer->getCompanyId();
        $leadersToDisplay = isset($atts['limit']) ? (int) $atts['limit'] : 10;

        foreach(PechHelper::getLeaderboard($challenge, $companyId) as $pos => $lb) {
            if(in_array($lb->getTeam()->getTeamId(), $myTeams)) {
                $leaderboard[$pos] = $lb;
            } elseif($leadersToDisplay > 0 && $pos < $leadersToDisplay) {
                $leaderboard[$pos] = $lb;
            }
        }

        return (new LS\Template(__DIR__ . '/views/frontend'))
            ->assign('leaderboard', $leaderboard)
            ->assign('myTeamsIds', $myTeams)
            ->render('leaderboard.phtml', false);
    }

    /**
     * @internal
     * @return string
     */
    public function overviewShortcode()
    {
        if(!consumersLoyaltySuite::isConsumerAuthorized()) {

            self::logBreakpoint();

            return current_user_can('edit_users') ? 'Admin warning: [personal-challenges-overview] called on public page' : '';
        }

        $data = [];
        $myTeams = $this->getMyTeams();

        if(!empty($myTeams)) {

            $todayActivities = PechHelper::getConsumerActivitiesForToday(consumersLoyaltySuite::getAuthorizedConsumerId());

            foreach($myTeams as $team) {

                $challengeId = $team->getChallengeId();

                if(!isset($data[$challengeId])) {
                    $data[$challengeId] = [
                        'icon'          => $team->getChallenge()->getChallengeIcon(),
                        'challengeName' => $team->getChallenge()->getPostTitle(),
                        'statusText'    => count($myTeams) > 1 ? __('Deine Challenges sind abgeschlossen', 'ls') : __('Deine Challenge ist abgeschlossen', 'ls'),
                        'link'          => $team->getChallenge()->getChallengeLink(),
                        'class'         => 'finished',
                        'level'         => 0 // Level statuses: 0 -> ended, 1 -> upcoming, 2 -> everything okay, 3 -> alert, 4 -> important
                    ];
                }

                if($data[$challengeId]['level'] < 4 && $team->confirmationAwaiting()) {
                    $data[$challengeId]['statusText'] = count($myTeams) > 1 ? __('Du hast eine neue Challengeeinladungen erhalten', 'ls') : __('Du hast eine neue Challengeeinladung erhalten', 'ls');
                    $data[$challengeId]['class'] = 'action';
                    $data[$challengeId]['level'] = 4;
                } elseif($data[$challengeId]['level'] < 3 && $team->activityAwaiting($todayActivities)) {
                    $data[$challengeId]['statusText'] = __('Bitte ergänze die aktuellen Challengedaten', 'ls');
                    $data[$challengeId]['class'] = 'action';
                    $data[$challengeId]['level'] = 3;
                } elseif($data[$challengeId]['level'] < 2 && $team->startAwaiting()) {
                    $data[$challengeId]['statusText'] = count($myTeams) > 1 ? __('Die nächsten Challenges starten in Kürze', 'ls') : sprintf(__('Die nächste Challenge startet am %s', 'ls'), date('d.m.Y', $team->getTeam()->getStartDate()));
                    $data[$challengeId]['class'] = 'done';
                    $data[$challengeId]['level'] = 2;
                } elseif($data[$challengeId]['level'] < 1 && !$team->isEnded()) {
                    $data[$challengeId]['statusText'] = __('Alle aktuellen Challengedaten sind eingetragen. Top! Schaue morgen wieder vorbei', 'ls');
                    $data[$challengeId]['class'] = 'done';
                    $data[$challengeId]['level'] = 1;
                }
            }
        }

        $html = (new LS\Template(__DIR__ . '/views/frontend'))
            ->assign('data', $data)
            ->render('overview.phtml', false);

        return $html;
    }

    /**
     * @internal
     * @ajax
     */
    public function ajaxController()
    {
        $action = $this->getParam('action');

        if($action == 'pechDetails') {
            $this->pechDetailsAjax();
        } elseif($action == 'pechActivities') {
            $this->pechActivitiesAjax();
        } elseif($action == 'pechPushIt') {
            $this->pechPushItAjax();
        } elseif($action == 'pechCheckEmails') {
            $this->pechCheckEmailsAjax();
        } elseif($action == 'pechCheckTeam') {
            $this->pechCheckTeamAjax();
        } elseif($action == 'pechPushCancel') {
            $this->pechPushCancelAjax();
        } elseif($action == 'pechHide') {
            $this->pechHideAjax();
        }
    }

    /**
     * @ajax
     */
    private function pechDetailsAjax()
    {
        if(!consumersLoyaltySuite::isConsumerAuthorized()) {
            self::logBreakpoint();
            exit(__('Teamdetails können nicht geladen werden', 'ls'));
        }

        $team = PechHelper::getTeam($this->getParam('id', 'id'));

        if(!$team) {
            self::logBreakpoint();
            exit(__('Teamdetails können nicht geladen werden', 'ls'));
        }

        $challenge = new PechChallenge($team->getChallengeId());

        if(!$challenge->isActive()) {
            exit(__('Teamdetails können nicht geladen werden', 'ls'));
        }

        $challengeTeam = new PechChallengeTeam($challenge, $team);
        $myTeam = $this->getMyTeam($team->getTeamId());

        // check if free entrance is available in a case that this is not my team
        if(!$myTeam && !$challenge->isFreeEntrance()) {
            self::logBreakpoint();
            exit(__('Teamdetails können nicht geladen werden', 'ls'));
        }

        $participations = PechHelper::getTeamParticipations($team);

        // hide participations from the finished teams if they are not accepted
        $participations = array_filter($participations, static function(PechParticipation $pcp) use ($challengeTeam) {
            return !$challengeTeam->isFinished() || !$pcp->toBeAccepted();
        });

        $activeParticipationsCount = count(array_filter($participations, static function(PechParticipation $pcp) {
            return $pcp->isAccepted();
        }));

        if($myTeam) {

            $activities = PechHelper::getTeamActivities($team);
            $pcpPoints = PechHelper::getParticipationsPoints($participations, $activities, $challenge->getChallengeType());
            $maxPoints = $challenge->isTypeYesNo() ? $challenge->getActivityDays() : (empty($pcpPoints) ? 0 : max($pcpPoints));
            $progresses =
            $myActivitiesData = [];

            foreach($activities as $activity) {
                if($activity->getParticipationId() === $myTeam->getParticipationId()) {
                    $myActivitiesData[$activity->getActivityDate()] = $activity->getData();
                }
            }

            foreach($pcpPoints as $consumerId => $points) {
                $progresses[$consumerId] = $maxPoints > 0 ? min(100, ceil($points / $maxPoints * 100)) : 0;
            }

            if($myTeam->confirmationAwaiting() || $myTeam->startAwaiting()) {

                // if confirmation awaiting -> move to the top of the list
                usort($participations, static function(PechParticipation $a) use ($myTeam) {
                    return $a->getConsumerId() === $myTeam->getConsumerId() ? -1 : 1;
                });

            } else {

                // trick to move current consumer to the top of equal values
                $progresses[$myTeam->getConsumerId()] += 0.1;

                // trick to move not accepted consumers to the bottom
                foreach($participations as $k => $pcp) {
                    if(!$pcp->isAccepted()) {
                        $progresses[$pcp->getConsumerId()] = -1;
                    }
                }

                usort($participations, static function(PechParticipation $a, PechParticipation $b) use ($progresses) {

                    if($progresses[$b->getConsumerId()] === $progresses[$a->getConsumerId()]) {
                        return 0;
                    }

                    return $progresses[$b->getConsumerId()] < $progresses[$a->getConsumerId()] ? -1 : 1;
                });

                $progresses[$myTeam->getConsumerId()] -= 0.1;
            }

            $myRank = PechHelper::getParticipantRank($myTeam);

        } else { // free entrance mode
            $progresses =
            $pcpPoints =
            $myActivitiesData = [];
            $myRank = false;
        }

        (new LS\Template(__DIR__ . '/views/frontend'))
            ->assign('challengeTeam', $challengeTeam)
            ->assign('myTeam', $myTeam)
            ->assign('participations', $participations)
            ->assign('activeParticipationsCount', $activeParticipationsCount)
            ->assign('progresses', $progresses)
            ->assign('pcpPoints', $pcpPoints)
            ->assign('myActivitiesData', $myActivitiesData)
            ->assign('myRank', $myRank)
            ->assign('imagesPath', $this->getUrl2Module() . 'images')
            ->render('details-data.phtml');

        exit;
    }

    /**
     * @ajax
     */
    private function pechActivitiesAjax()
    {
        if(!consumersLoyaltySuite::isConsumerAuthorized()) {
            self::logBreakpoint();
            exit(__('Aktion ist nicht möglich', 'ls'));
        }

        $myTeam = $this->getMyTeam($this->getParam('id', 'id'));

        if(!$myTeam || !$myTeam->canAddActivities()) {
            self::logBreakpoint();
            exit(__('Aktion ist nicht möglich', 'ls'));
        }

        $activities = [];
        $steps = $this->getParam('steps', 'int_array');
        $challenge = $myTeam->getChallenge();
        $maxValue = $challenge->getMaxValue();
        $checkMaxValue = $challenge->getChallengeType() === PechChallenge::TYPE_VALUES && $maxValue > 0;

        for($i = 0; $i < $challenge->getActivityDays(); $i++) {
            $dateFor = $myTeam->getTeam()->getStartDate() + $i * DAY_IN_SECONDS;
            if(time() >= $dateFor && isset($steps[$i]) && $steps[$i] !== '') {

                $value = $steps[$i];

                if($checkMaxValue && $value > $maxValue) {
                    $value = $maxValue;
                }

                $activities[] = new PechActivity([
                    'pcpId'        => $myTeam->getParticipationId(),
                    'activityDate' => $dateFor,
                    'data'         => $value
                ]);
            }
        }

        PechHelper::updateParticipationActivities($myTeam->getParticipation(), $activities);
        exit;
    }

    /**
     * @ajax
     */
    private function pechPushItAjax()
    {
        if(!consumersLoyaltySuite::isConsumerAuthorized()) {
            self::logBreakpoint();
            exit(__('Keine Push-Info an Team-Teilnehmer möglich', 'ls'));
        }

        $myTeam = $this->getMyTeam($this->getParam('id', 'id'));

        if(!$myTeam || !$myTeam->canPush()) {
            self::logBreakpoint();
            exit(__('Keine Push-Info an Team-Teilnehmer möglich', 'ls'));
        }

        $opponentConsumerId = $this->getParam('cid', 'id');
        $opponentParticipation = PechHelper::getConsumerTeam($opponentConsumerId, $myTeam->getTeamId());

        if(!$opponentParticipation) {
            self::logBreakpoint();
            exit(__('Keine Push-Info an Team-Teilnehmer möglich', 'ls'));
        }

        if($opponentParticipation->canPush()) {
            PechHelper::updateParticipation(
                $opponentParticipation->getParticipation()->push()
            );
        }

        exit;
    }

    /**
     * @ajax
     */
    private function pechCheckEmailsAjax()
    {
        if(!consumersLoyaltySuite::isConsumerAuthorized()) {
            self::logBreakpoint();
            exit(__('Aktion ist nicht möglich', 'ls'));
        }

        $currentConsumer = consumersLoyaltySuite::getAuthorizedConsumer();
        $existsConsumersIds = [];
        $challenge = new PechChallenge($this->getParam('pechId', 'id'));
        $errors = [
            'general'      => '',
            'participants' => []
        ];

        if(!$challenge->isActive($e)) {
            $errors['general'] = $e;
        }

        $teamId = $this->getParam('id', 'id');

        if($teamId > 0) {
            $myTeam = $this->getMyTeam($teamId);
            if(!$myTeam || $myTeam->getChallengeId() !== $challenge->getChallengeId() || !$myTeam->canAddParticipants()) {
                $errors['general'] = __('Diese E-mail Adresse ist im System nicht bekannt', 'ls');
            } else {
                foreach(PechHelper::getTeamParticipations($myTeam->getTeam()) as $pcp) {
                    $existsConsumersIds[] = $pcp->getConsumerId();
                }
            }
        }

        if(empty($errors['general'])) {
            foreach(self::extractEmails($this->getParam('emails', 'array')) as $email) {
                if(!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                    $errors['participants'][$email] = __('Falsche E-Mail Adresse', 'ls');
                } elseif($currentConsumer->getEmail() == $email) {
                    $errors['participants'][$email] = __('Du kannst keine eigene E-Mail-Adresse hinzufügen', 'ls');
                } else {
                    $consumer = \LS\ConsumersHelper::getConsumerByEmail($email);
                    if(!$consumer || !$consumer->active()) {
                        $errors['participants'][$email] = __('Nicht im Programm registriert!', 'ls');
                    } elseif(in_array($consumer->getConsumerId(), $existsConsumersIds)) {
                        $errors['participants'][$email] = __('Ist bereits Mitglied in diesem Team', 'ls');
                    } elseif(!PechHelper::singleParticipationRuleCompleted($consumer->getConsumerId(), $challenge)) {
                        $errors['participants'][$email] = __('Ist bereits Mitglied in einem anderen Team', 'ls');
                    }
                }
            }
        }

        wp_send_json($errors);
        exit;
    }

    /**
     * @ajax
     */
    private function pechCheckTeamAjax()
    {
        if(!consumersLoyaltySuite::isConsumerAuthorized()) {
            self::logBreakpoint();
            exit(__('Aktion ist nicht möglich', 'ls'));
        }

        $teamName = $this->getParam('teamName', 'text');

        if(empty($teamName)) {
            $error = __('Bitte gib deinem Team einen Namen', 'ls');
        } elseif(!PechHelper::isUniqueTeamName($teamName)) {
            $error = __('Dieser Name ist leider schon vergeben', 'ls');
        } else {
            $error = '';
        }

        wp_send_json($error);
        exit;
    }

    /**
     * @ajax
     */
    private function pechPushCancelAjax()
    {
        if(!consumersLoyaltySuite::isConsumerAuthorized()) {
            self::logBreakpoint();
            exit(__('Aktion ist nicht möglich', 'ls'));
        }

        $myTeam = $this->getMyTeam($this->getParam('id', 'id'));

        if($myTeam) {

            PechHelper::updateParticipation(
                $myTeam->getParticipation()->releasePush()
            );

            do_action('pech_consumer_action', $myTeam->getConsumerId());
        }

        exit;
    }

    /**
     * @ajax
     */
    private function pechHideAjax()
    {
        if(!consumersLoyaltySuite::isConsumerAuthorized()) {
            self::logBreakpoint();
            exit(__('Aktion ist nicht möglich', 'ls'));
        }

        $myTeam = $this->getMyTeam($this->getParam('id', 'id'));

        if($myTeam && $myTeam->canHide()) {

            PechHelper::updateParticipation(
                $myTeam->getParticipation()->makeInvisible()
            );

            do_action('pech_consumer_action', $myTeam->getConsumerId());
        }

        exit;
    }

    /**
     * @return PechConsumerTeam[]
     */
    public function getMyTeams()
    {
        if($this->myTeams === null) {
            $this->myTeams = PechHelper::getConsumerVisibleTeams(
                consumersLoyaltySuite::getAuthorizedConsumerId()
            );
        }

        return $this->myTeams;
    }

    /**
     * @param int $challengeId
     * @return PechConsumerTeam[]
     */
    private function getMyTeamsInChallenge($challengeId)
    {
        return array_filter($this->getMyTeams(), static function(PechConsumerTeam $myTeam) use ($challengeId) {
            return $myTeam->getChallengeId() === $challengeId;
        });
    }

    /**
     * @param int $teamId
     * @return false|PechConsumerTeam
     */
    public function getMyTeam($teamId)
    {
        $result = false;

        if($teamId > 0) {
            foreach($this->getMyTeams() as $myTeam) {
                if($myTeam->getTeamId() === $teamId) {
                    $result = $myTeam;
                    break;
                }
            }
        }

        return $result;
    }

    /**
     * @return bool
     */
    public function haveCurrentConsumerActivities(): bool
    {
        if(!consumersLoyaltySuite::isConsumerAuthorized()) {
            return false;
        }

        $myTeams = $this->getMyTeams();
        $consumerId = consumersLoyaltySuite::getAuthorizedConsumerId();
        $todayActivities = PechHelper::getConsumerActivitiesForToday($consumerId);

        foreach($myTeams as $team) {
            if($team->confirmationAwaiting() || $team->activityAwaiting($todayActivities)) {
                return true;
            }
        }

        return false;
    }

    /**
     * @internal
     * @param int $consumerId
     */
    public function consumerDeleted(int $consumerId)
    {
        foreach(PechHelper::getConsumerParticipations($consumerId) as $participation) {
            PechHelper::deleteParticipation($participation);
        }
    }

    /**
     * @internal
     */
    public function pushItMessage()
    {
        global $wp_query, $popupMessage;

        if(!apply_filters('ls_pech_show_push_it', true)) {
            return;
        }

        // one popup message per time
        if($popupMessage !== null && $popupMessage) {
            return;
        }

        if(LS\LoyaltySuite::hasErrors()) {
            return;
        }

        if($wp_query && \LS\Library::isFrontendRequest() && !is_404() && ($consumerId = consumersLoyaltySuite::getAuthorizedConsumerId(true)) > 0) {

            if(wp_cache_get('push_' . $consumerId, 'fitco') == false) {

                $pushes = 0;

                foreach($this->getMyTeams() as $myTeam) {

                    if(!$myTeam->hasToPush()) {
                        continue;
                    }

                    // send only the first push
                    if(!$popupMessage) {

                        (new LS\Template(__DIR__ . '/views/frontend'))
                            ->assign('postId', $myTeam->getChallengeId())
                            ->assign('teamId', $myTeam->getTeamId())
                            ->assign('link', $myTeam->getChallenge()->getChallengeLink())
                            ->assign('message', $myTeam->getChallenge()->getPushItMessage(consumersLoyaltySuite::getAuthorizedConsumer()))
                            ->render('push-it.phtml');

                        $popupMessage = true;
                    }

                    $pushes++;
                }

                // cache only for one or zero pop-ups, otherwise have to display new message after the page refresh
                if($pushes < 2) {
                    wp_cache_set('push_' . $consumerId, 'fitco', 60);
                }
            }
        }
    }

    private function initPechAT()
    {
        ATActions::addAction('fitcoPech', (new ATAction())
            ->setLabels([
                'title'        => 'Challenge',
                'emptyIdent'   => 'Activity "%s": please select challenge',
                'openActivity' => 'keine aktive Challenge',
                'doneActivity' => 'Neue Challenge starten'
            ])
            ->allowEmptyLink(false)
            ->setIdentId('identPech')
            ->setSettingsFunc(static function($activity) {
                (new LS\Template(__DIR__ . '/views/backend'))
                    ->assign('challenges', PechHelper::getChallengesList())
                    ->assign('activity', $activity)
                    ->render('admin-at-activity.phtml');
            })
            ->validateActivity(static function($activity) {

                $challenge = new PechChallenge((int) $activity['ident']);
                if(!$challenge || $challenge->isScheduled()) {
                    $activity['future'] = true;
                }

                return $activity;
            })
            ->setReviewFunc(static function($consumerId, $activity) {
                return PechHelper::isConsumerAcceptedChallenge($consumerId, (int) $activity['ident']);
            })
            ->setConsumerDetailsLogFunc(static function($activity) {
                $challenge = new PechChallenge((int) $activity['ident']);

                return 'Challenge participation: ' . ($challenge ? $challenge->getPostTitle() : 'UNKNOWN CHALLENGE NAME');
            })
        );
    }

    /**
     * @internal
     * @param int $postId
     * @param int $consumerId
     */
    public function atPechAccepted($postId, $consumerId)
    {
        /** @var at2LoyaltySuite $at */
        $at = LS()->getModule('at2');
        $at->activitiesLogic('fitcoPech', $postId, $consumerId);
    }

    /**
     * @internal
     * @param \LS\BPTrigger[] $triggers
     * @return \LS\BPTrigger[]
     */
    public function bpRegisteredTriggers($triggers)
    {
        include_once __DIR__ . '/classes/BPPechTrigger.php';

        $triggers[] = new \LS\BonusPoints\BPPechTrigger();

        return $triggers;
    }

    /**
     * @internal
     * @deprecated 05.2019 Legacy
     * @TODO Remove after 08.2019
     *
     * @param array $types
     * @return array
     */
    public function bpTriggerTypes($types)
    {
        $types['pech'] = '1:1 Challenge participation';

        return $types;
    }

    /**
     * @internal
     * @deprecated 05.2019 Legacy
     * @TODO Remove after 08.2019
     *
     * @param \LS\FieldAbstract[] $idents
     * @return \LS\FieldAbstract[]
     */
    public function bpTriggerIdents($idents)
    {
        $idents['pech'] = new LS\Field\Select('triggerIdent[pech]', '1:1 Challenge', PechHelper::getChallengesList(), ['emptyText' => 'All', 'class' => 'trigger-ident']);

        return $idents;
    }

    /**
     * @internal
     * @param int $postId
     * @param int $consumerId
     */
    public function bpPechAccepted($postId, $consumerId)
    {
        /** @var bonusPointsLoyaltySuite $bp */
        $bp = LS()->getModule('bonusPoints');
        $bp->performTrigger('pech', $postId, $consumerId);
    }
}