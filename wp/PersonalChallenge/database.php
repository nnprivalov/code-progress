<?php
/**
 * @param string $oldVersion
 */
function fitcoPersonalChallengeActualize($oldVersion = '')
{
    require_once ABSPATH . 'wp-admin/includes/upgrade.php';

    $wpdb = LS()->wpdb;

    if(empty($oldVersion)) {

        dbDelta(sprintf("CREATE TABLE `{$wpdb->prefix}ls_fitco_challenge_teams` (
            `teamId` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
            `postId` BIGINT(20) UNSIGNED NOT NULL,
	        `teamName` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8mb4_unicode_ci',
            `startDate` DATE NOT NULL,
            `createDate` INT(10) UNSIGNED NOT NULL,
            PRIMARY KEY (`teamId`),
            INDEX `postId` (`postId`)
        ) %s ENGINE=InnoDB", LS\Library::getCharsetCollate()));

        dbDelta(sprintf("CREATE TABLE `{$wpdb->prefix}ls_fitco_challenge_participations` (
            `pcpId` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
            `teamId` INT(10) UNSIGNED NOT NULL,
            `consumerId` INT(10) UNSIGNED NOT NULL,
            `reactionDate` INT(10) UNSIGNED NOT NULL,
            `status` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
            `push` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
            `visible` TINYINT(1) UNSIGNED NOT NULL DEFAULT '1',
            PRIMARY KEY (`pcpId`),
            INDEX `teamId` (`teamId`),
            INDEX `consumerId` (`consumerId`)
        ) %s ENGINE=InnoDB", LS\Library::getCharsetCollate()));

        dbDelta(sprintf("CREATE TABLE `{$wpdb->prefix}ls_fitco_challenge_activities` (
            `pcpId` INT(10) UNSIGNED NOT NULL,
            `activityDate` DATE NOT NULL,
            `data` VARCHAR(200) NOT NULL,
            PRIMARY KEY (`pcpId`, `activityDate`)
        ) %s ENGINE=InnoDB", LS\Library::getCharsetCollate()));

    } elseif(version_compare($oldVersion, '4.0.0', '<')) {
        $wpdb->query("RENAME TABLE `{$wpdb->prefix}ls_fitco_challenge_invitations` TO `{$wpdb->prefix}ls_fitco_challenge_teams`");
        $wpdb->query("ALTER TABLE `{$wpdb->prefix}ls_fitco_challenge_teams` CHANGE COLUMN `invitationId` `teamId` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT FIRST");
        $wpdb->query("ALTER TABLE `{$wpdb->prefix}ls_fitco_challenge_participations` CHANGE COLUMN `invitationId` `teamId` INT(10) UNSIGNED NOT NULL AFTER `pcpId`");
    }
}

function fitcoPersonalChallengeUninstall()
{
    $wpdb = LS()->wpdb;
    $wpdb->query(sprintf("DROP TABLE {$wpdb->prefix}ls_fitco_challenge_activities"));
    $wpdb->query(sprintf("DROP TABLE {$wpdb->prefix}ls_fitco_challenge_participations"));
    $wpdb->query(sprintf("DROP TABLE {$wpdb->prefix}ls_fitco_challenge_teams"));
}