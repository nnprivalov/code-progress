<?php
class tippDerWocheAdminLoyaltySuite extends tippDerWocheLoyaltySuite implements \LS\ModuleAdminInterface
{
    /**
     * @internal
     */
    public function load()
    {
        parent::load();

        add_action('add_meta_boxes', [$this, 'addFitcoBox']);
        add_action('save_post', [$this, 'saveFitcoBox']);
        add_action('ls_ce_dashboard', [$this, 'comCenterCEDashboard'], 15);
        add_action('manage_posts_custom_column', [$this, 'modifyColumn'], 10, 2);
        add_filter('manage_post_posts_columns', [$this, 'addColumn']);
        add_filter('manage_edit-post_sortable_columns', [$this, 'addSorting']);
        add_action('pre_get_posts', [$this, 'sortByColumn'], 1);

        // Consumers integration
        add_filter('ls_consumer_log__tdw_enabled', [$this, 'consumerLog']);
        add_filter('ls_consumer_log__tdw_disabled', [$this, 'consumerLog']);
    }

    /**
     * @internal
     */
    public function initShortcodes()
    {
        parent::initShortcodes();

        LS()->addShortcodeInfo('tipp-der-woche', 'Tipp-der-Woche', '', self::getName(), ['posts_category_id' => 'ID of the posts category for a list of categories']);
        LS()->addShortcodeInfo('tipp-der-woche-form', 'Tipp-der-Woche Form', '', self::getName());
        LS()->addShortcodeInfo('Fitco-Aufgabe', 'Fitco TODO: Zeige den Inhakt des Feldes "Fitco TODO" an', '', self::getName());
        LS()->addShortcodeInfo('Fitco-Aufgabe-recent-post', 'Fitco TODO: Zeige den Inhalt des Feldes "Fitco TODO" für diesen Beitrag an', '', self::getName());
        LS()->addShortcodeInfo('gb-task-box', 'Fitco task box', 'Füge einen Block mit den Optionen (yes/ja - no/nein - maybe/vielleicht) hinzu, um Kundenreaktionen zu erfassen', self::getName(), ['label' => 'Box label']);
    }

    /**
     * @internal
     */
    public function addFitcoBox()
    {
        add_meta_box('fitco-box', 'FitCo in dieser Woche', static function ($post) {

            wp_nonce_field('fitcoBoxNonce', 'fitcoBoxNonce');

            (new LS\Template(__DIR__ . '/views/backend'))
                ->assign('fitcoToDo', get_post_meta($post->ID, '_fitcoToDo', true))
                ->assign('fitcoTDW', get_post_meta($post->ID, '_fitcoTDW', true))
                ->assign('fitcoTDWEmail', get_post_meta($post->ID, '_fitcoTDWEmail', true))
                ->assign('fitcoTDWCustomLink', get_post_meta($post->ID, '_fitcoTDWCustomLink', true))
                ->render('admin-fitco-box.phtml');

        }, 'post');
    }

    /**
     * @internal
     * @param int $postId
     */
    public function saveFitcoBox($postId)
    {
        if(isset($_POST['fitcoBoxNonce']) && wp_verify_nonce($_POST['fitcoBoxNonce'], 'fitcoBoxNonce')) {
            update_post_meta($postId, '_fitcoTDW', $this->getParam('fitcoTDW', 'date'));
            update_post_meta($postId, '_fitcoTDWEmail', $this->getParam('fitcoTDWEmail', 'checkbox'));
            update_post_meta($postId, '_fitcoToDo', addslashes($this->getParam('fitcoToDo', 'html')));
            update_post_meta($postId, '_fitcoTDWCustomLink', $this->getParam('fitcoTDWCustomLink', 'url'));
        }
    }

    /**
     * @internal
     */
    public function comCenterCEDashboard()
    {
        /** @var tippDerWocheLoyaltySuite $tdw */
        $tdw = LS()->getModule('tippDerWoche');
        $emails = $tdw->getSentTDWEmails(null, $this->getMonday(time()));
        ?>
        <div class="bx size-1">
            <div>
                <span><?=$emails?></span>
                <label>Push E-Mails letzte Woche</label>
            </div>
        </div>
        <?php
    }

    /**
     * @param string[] $column
     * @return string[]
     */
    public function addColumn($column)
    {
        return $column + ['tdw' => 'TDW'];
    }

    /**
     * @param string $columnName
     * @param int $postId
     */
    public function modifyColumn($columnName, $postId)
    {
	    if($columnName == 'tdw') {
            $date = $this->getTDWDate($postId);
            echo $date > 0 ? date('d.m.Y', $date) : '—';
        }
    }

    /**
     * @param string[] $columns
     * @return string[]
     */
    public function addSorting($columns)
    {
        $columns['tdw'] = 'tdw';
        
        return $columns;
    }

    /**
     * @param WP_Query $query
     */
    public function sortByColumn($query)
    {
        if($query->is_main_query() && ($orderby = $query->get('orderby')) && $orderby == 'tdw') {
            $query->set('meta_key', '_fitcoTDW');
            $query->set('orderby', 'meta_value');
        }
    }

    /**
     * @internal
     * @param object $log
     * @return string
     */
    public function consumerLog($log)
    {
        $event = '';

        if($log->messageType == 'tdw_enabled') {
            $event = __('Tipp der Woche aktiviert', 'ls');
        } elseif($log->messageType == 'tdw_disabled') {
            $event = __('Tipp der Woche deaktiviert', 'ls');
        }

        return $event;
    }
}