jQuery(document).ready(function($) {
    $('.task-box-buttons').on('click', '.task-box-btn', function(){

        var btn = $(this),
            tbButtons = btn.closest('.task-box-buttons'),
            sel = btn.hasClass('selected');

        tbButtons.find('a').removeClass('selected');

        if(!sel) {
            btn.addClass('selected');
        }

        $.post(LS.ajaxurl, {
            action: 'fitcoTaskBox',
            name: tbButtons.data('name'),
            nonce: tbButtons.data('nonce'),
            option: sel ? '' : btn.data('option')
        });

        return false;
    });
});