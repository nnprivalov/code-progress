jQuery(document).ready(function($) {
    $('.tdw-form .checkbox-switcher').on('click', '.sld', function() {
        $.post(LS.ajaxurl, {
            action: 'tdwChangeStatus',
            checked: $(this).parent().find('input[type=checkbox]').is(':checked') ? 0 : 1
        });
    });
});