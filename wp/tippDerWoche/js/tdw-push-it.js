jQuery(document).ready(function($) {

    var toDoBox = $('#tdwToDo');
    if(!toDoBox.length) {
        return;
    }

    setTimeout(function() {

        $.fancybox.open(toDoBox.first(), {
            wrapCSS: 'tdw',
            margin: 10,
            minWidth: 240,
            maxWidth: 800,
            beforeShow: function() {
                $("body").css({'overflow-y': 'hidden'});
            },
            afterClose: function() {
                $("body").css({'overflow-y': 'visible'});
            },
            helpers: {
                overlay: {
                    closeClick: false
                }
            }
        });

    }, 1500);

});