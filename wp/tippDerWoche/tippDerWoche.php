<?php
class tippDerWocheLoyaltySuite extends LS\Module
{
    const OPTION_UNDEFINED = '';
    const OPTION_YES = 'yes';
    const OPTION_NO = 'no';
    const OPTION_MAYBE = 'maybe';

    const BP_TRIGGER_NAME = 'taskBox';

    const FIELD_PREFIX = 'fitco_tb_';

    /**
     * @return string
     */
    public function getVersion()
    {
        return '1.19.2';
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return 'Tipp der Woche';
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return 'Tipp der Woche + FitCo ToDo + Task Box';
    }

    public function load()
    {
        add_action('wp_footer', [$this, 'wpFooter']);
        add_action('wp_ajax_tdwChangeStatus', [$this, 'tdwChangeStatusAjax']);
        add_action('wp_ajax_nopriv_tdwChangeStatus', [$this, 'tdwChangeStatusAjax']);
        add_action('wp_ajax_fitcoTaskBox', [$this, 'fitcoTaskBox']);
        add_action('wp_ajax_nopriv_fitcoTaskBox', [$this, 'fitcoTaskBox']);
        add_action('delete_post', [$this, 'deletePost']);

        // Consumers integration
        add_filter('consumerDynamicFields', [$this, 'consumerDynamicFields']);
        add_action('ls_consumer_created', [$this, 'consumerCreated']);

        // Com.Center integration
        add_filter('ls_cc_init', [$this, 'comCenterInit']);
        add_filter('ls_cc_templates', [$this, 'comCenterTemplates'], 11);

        // AT2 integration
        if (LS()->isModuleActive('at2')) {
            $this->initAT2Action();
            add_action('ls_fitco_tdw_changed', [$this, 'tdwChangedForAT2'], 10, 3);
        }
    }

    public function initShortcodes()
    {
        add_shortcode('tipp-der-woche', [$this, 'tippDerWocheShortcode']);
        add_shortcode('tipp-der-woche-form', [$this, 'tippDerWocheFormShortcode']);
        add_shortcode('Fitco-Aufgabe', [$this, 'fitcoAufgabeShortcode']);
        add_shortcode('Fitco-Aufgabe-recent-post', [$this, 'fitcoAufgabeRecentPostShortcode']);
        add_shortcode('gb-task-box', [$this, 'gbTaskBoxShortcode']);
    }

    /**
     * @internal
     */
    public function wpFooter()
    {
        if ((is_home() || is_front_page()) && consumersLoyaltySuite::isConsumerAuthorized(true)) {

            $p = $this->getLatestTDWPost();

            if (!empty($p)) {

                $consumer = consumersLoyaltySuite::getAuthorizedConsumer();
                $consumerLatestTdw = $consumer->getDynamicValue('tdwLatestView', 0, true);
                $tdwDate = $this->getTDWDate($p->ID);

                if ($tdwDate > $consumerLatestTdw) {

                    $this->showTDWPushIt($p);

                    \LS\ConsumersHelper::saveDynamicFieldValue(
                        'tdwLatestView',
                        time(),
                        consumersLoyaltySuite::getAuthorizedConsumerId()
                    );
                }
            }
        }
    }

    /**
     * @param int $postId
     * @return string
     */
    public function getFieldName(int $postId): string
    {
        return self::FIELD_PREFIX . $postId;
    }

    /**
     * @param string $fieldName
     * @return int
     */
    public function getPostIdFromFieldName(string $fieldName)
    {
        return (int) str_replace(self::FIELD_PREFIX, '', $fieldName);
    }

    /**
     * @param \LS\ConsumerInterface $consumer
     * @param int $postId
     * @return string
     */
    public function getConsumerAnswerToTDW(\LS\ConsumerInterface $consumer, int $postId): string
    {
        return $consumer->getDynamicValue(
            $this->getFieldName($postId),
            self::OPTION_UNDEFINED
        );
    }

    /**
     * @param \LS\ConsumerInterface $consumer
     * @param int $post
     * @return bool
     */
    public function isConsumerAcceptedTDW(\LS\ConsumerInterface $consumer, int $post): bool
    {
        return $this->getConsumerAnswerToTDW($consumer, $post) === self::OPTION_YES;
    }

    /**
     * @param WP_Post $post
     * @return string
     */
    private function renderTaskBox(WP_Post $post): string
    {
        $name = $this->getFieldName($post->ID);
        $external = get_the_ID() != $post->ID;

        $html = (new LS\Template(__DIR__ . '/views/frontend'))
            ->assign('name', $name)
            ->assign('urlToModule', $this->getUrl2Module())
            ->assign('post', $post)
            ->assign('todoText', $this->getTaskBoxTextToDisplay($post))
            ->assign('buttons', $this->getTaskBoxButtonsToDisplay($post))
            ->assign('external', $external)
            ->render('task-box.phtml', false);

        return $html;
    }

    /**
     * @param WP_Post $post
     * @return string
     */
    public function getTaskBoxTextToDisplay(WP_Post $post)
    {
        return $this->getToDo($post->ID);
    }

    /**
     * @param WP_Post $post
     * @return string
     */
    public function getTaskBoxButtonsToDisplay(WP_Post $post)
    {
        $name = $this->getFieldName($post->ID);
        $consumer = consumersLoyaltySuite::getAuthorizedConsumer();
        $status = $consumer ? $consumer->getDynamicValue($name, self::OPTION_UNDEFINED) : self::OPTION_UNDEFINED;

        $html = (new LS\Template(__DIR__ . '/views/frontend'))
            ->assign('status', $status)
            ->assign('name', $name)
            ->render('task-box-buttons.phtml', false);

        return $html;
    }

    /**
     * @param WP_Post $post
     */
    private function showTDWPushIt(WP_Post $post)
    {
        global $popupMessage;

        // one popup message per page load
        if ($popupMessage !== null && $popupMessage) {
            return;
        }

        echo '<div id="tdwToDo" style="display: none">' . $this->renderTaskBox($post) . '</div>';

        (new \LS\Template(__DIR__ . '/js'))
            ->renderScript('tdw-push-it.js');
    }

    /**
     * @return false|WP_Post
     */
    public function getLatestTDWPost()
    {
        $posts = get_posts([
            'numberposts'    => 1,
            'post_status'    => 'publish',
            'no_found_rows'  => true,
            // get latest TDW by date
            'meta_key'       => '_fitcoTDW',
            'orderby'        => 'meta_value',
            'meta_query'     => [
                // but only in actual range
                [
                    'key'     => '_fitcoTDW',
                    'compare' => 'BETWEEN',
                    'value'   => [0, time()]
                ]
            ]
        ]);

        return empty($posts) ? false : $posts[0];
    }

    /**
     * @param int $from
     * @param int $to
     * @return WP_Post[]
     */
    public function getTDWPosts($from, $to)
    {
        $args = [
            'posts_per_page' => -1,
            'no_found_rows'  => true,
            'post_status'    => 'publish',
            'meta_query'     => [
                [
                    'key'     => '_fitcoTDW',
                    'compare' => 'BETWEEN',
                    'value'   => [$from, $to]
                ]
            ]
        ];

        return get_posts($args);
    }

    /**
     * @return false|WP_Post
     */
    public function getLatestTDWPostForEmail()
    {
        $posts = get_posts([
            'numberposts'   => 1,
            'post_status'   => 'publish',
            'no_found_rows' => true,
            // get latest TDW by date
            'meta_key'      => '_fitcoTDW',
            'orderby'       => 'meta_value',
            'meta_query'    => [
                // but only in actual range
                [
                    'key'     => '_fitcoTDW',
                    'compare' => 'BETWEEN',
                    'value'   => [0, time()]
                ],
                // email checked
                [
                    'key'   => '_fitcoTDWEmail',
                    'value' => 1
                ]
            ]
        ]);

        return empty($posts) ? false : $posts[0];
    }

    /**
     * @param int $postId
     * @return int
     */
    public function getTDWDate($postId)
    {
        return (int) get_post_meta($postId, '_fitcoTDW', true);
    }

    /**
     * @param int $postId
     * @return string
     */
    public function getToDo($postId)
    {
        return trim(do_shortcode(get_post_meta($postId, '_fitcoToDo', true)));
    }

    /**
     * @param WP_Post $post
     * @return string
     */
    public function getLink(WP_Post $post)
    {
        $link = get_post_meta($post->ID, '_fitcoTDWCustomLink', true);

        if (empty($link)) {
            $link = $post->post_status == 'publish' ? FitcoHelper::getPermalink($post) : home_url('/');
        }

        return $link;
    }

    /**
     * @param \LS\ConsumerInterface $consumer
     * @return bool
     */
    protected function isTDWEmailEnabled(\LS\ConsumerInterface $consumer): bool
    {
        return (bool) $consumer->getDynamicValue('tdwEmailEnabled');
    }

    /**
     * @param \LS\ConsumerInterface $consumer
     */
    protected function enableTDWEmail(\LS\ConsumerInterface $consumer)
    {
        if ($this->isTDWEmailEnabled($consumer)) {
            return;
        }

        $consumer->addDynamicValue('tdwEmailEnabled', 1);

        \LS\ConsumersHelper::updateConsumer($consumer);

        \LS\ConsumersHelper::insertLog('tdw_enabled', $consumer->getConsumerId());
    }

    /**
     * @param \LS\ConsumerInterface $consumer
     */
    protected function disableTDWEmail(\LS\ConsumerInterface $consumer)
    {
        if (!$this->isTDWEmailEnabled($consumer)) {
            return;
        }

        $consumer->addDynamicValue('tdwEmailEnabled', 0);

        \LS\ConsumersHelper::updateConsumer($consumer);

        \LS\ConsumersHelper::insertLog('tdw_disabled', $consumer->getConsumerId());
    }

    /**
     * @internal
     * @return string
     */
    public function fitcoAufgabeShortcode()
    {
        return $this->getToDo(get_the_ID());
    }

    /**
     * @internal
     * @return string
     */
    public function fitcoAufgabeRecentPostShortcode()
    {
        $p = $this->getLatestTDWPost();
        $value = '';

        if (!empty($p)) {
            $value = $this->getToDo($p->ID);
        }

        return empty($value) ? '' : '<div class="fitco-aufgabe-recent-post post-id-' . $p->ID . '">' . $value . '</div>';
    }

    /**
     * @internal
     * @return string
     */
    public function tippDerWocheShortcode()
    {
        $p = $this->getLatestTDWPost();
        if (empty($p)) {
            return '';
        }

        $bottom = '<a href="#tdwToDo" class="normal-button large inline fancybox autosize" data-class="tdw" data-max-width="800">To Do</a>' .
                  '<div id="tdwToDo" style="display: none">' . $this->renderTaskBox($p) . '</div>' .
                  '<a href="' . esc_url($this->getLink($p)) . '" class="normal-button large reversed inline mar">Informieren</a>';

        ob_start();

        FitcoHelper::homeBox($p, [
            'class'  => 'tdw',
            'label'  => __('Dein Gesundheits To Do der Woche', 'ls'),
            'link'   => false,
            'bottom' => $bottom
        ]);

        return ob_get_clean();
    }

    /**
     * @internal
     * @return string
     */
    public function tippDerWocheFormShortcode()
    {
        $consumer = consumersLoyaltySuite::getAuthorizedConsumer();

        if (!$consumer) {
            return '';
        }

        return (new LS\Template(__DIR__ . '/views/frontend'))
            ->assign('tdwEmailEnabled', $this->isTDWEmailEnabled($consumer))
            ->render('tipp-der-woche-form.phtml', false);
    }

    /**
     * @internal
     * @ajax
     */
    public function tdwChangeStatusAjax()
    {
        $checked = $this->getParam('checked', 'value', 'POST');
        $consumer = consumersLoyaltySuite::getAuthorizedConsumer();

        if ($consumer) {
            if ($checked) {
                $this->enableTDWEmail($consumer);
            } else {
                $this->disableTDWEmail($consumer);
            }
        }

        exit;
    }

    /**
     * @internal
     * @param \LS\ConsumerInterface $consumer
     */
    public function consumerCreated(\LS\ConsumerInterface $consumer)
    {
        $this->enableTDWEmail($consumer);
    }

    /**
     * @internal
     * @param LSConditionalEmailCondition[] $conditions
     * @return LSConditionalEmailCondition[]
     */
    public function comCenterInit($conditions)
    {
        $tdw = $this;

        $conditions['fitco_tdw'] = new LSConditionalEmailCondition('fitco_tdw', 'Tipp der Woche E-Mail (X Tage nach Veröffentlichung)', 'FitCo', [
            'allowedPeriods' => [LSConditionalEmail::PERIOD_AFTER],
            'template'       => 'fitco-tdw',
            'recipientsCb'   => function (LSConditionalEmail $ce) use ($tdw) {

                $td = new DateTime(date('Y-m-d'));
                $td->modify($ce->getDaysToEvent() . ' days');

                $p = $tdw->getLatestTDWPostForEmail();

                if (empty($p)) {
                    return [];
                }

                $tdwDate = $this->getTDWDate($p->ID);

                if (empty($tdwDate) || $td->format('Ymd') != date('Ymd', $tdwDate)) {
                    return [];
                }

                return \LS\ConsumersHelper::getConsumersIds(true, true, true, [
                    ['r' => 'tdwEmailEnabled', 'c' => 'notEquals', 't' => 0]
                ]);
            }
        ]);

        return $conditions;
    }

    /**
     * @internal
     *
     * @param LSConditionalEmailTemplate[] $templates A list of custom template
     * @return LSConditionalEmailTemplate[] Custom templates
     */
    public function comCenterTemplates($templates = [])
    {
        if (isset($templates['fitco-green'])) {

            /** @var LSConditionalEmailTemplate $template */
            $template = clone $templates['fitco-green'];
            $tdw = $this;

            $template
                ->setName('fitco-tdw')
                ->setLabel('FitCo Tipp-der-Woche')
                ->setPathToHTML(__DIR__ . '/views/email/fitco-weekly.phtml')
                ->setArg('tdw', static function () use ($tdw) {

                    $tdwPost = $tdw->getLatestTDWPost();

                    return (object) [
                        'title'    => $tdwPost ? get_the_title($tdwPost) : '',
                        'subtitle' => $tdwPost ? get_post_meta($tdwPost->ID, '_subtitle', true) : '',
                        'excerpt'  => $tdwPost ? get_the_excerpt($tdwPost) : '',
                        'image'    => $tdwPost && has_post_thumbnail($tdwPost) ? get_the_post_thumbnail_url($tdwPost, 'email') : '',
                        'todo'     => $tdwPost ? $tdw->getToDo($tdwPost->ID) : false,
                        'link'     => $tdwPost ? $tdw->getLink($tdwPost) : ''
                    ];
                });

            $templates['fitco-tdw'] = $template;
        }

        return $templates;
    }

    /**
     * Add extra consumer dynamic fields
     *
     * @internal
     *
     * @param \LS\FieldAbstract[] $fields
     * @return \LS\FieldAbstract[]
     */
    public function consumerDynamicFields($fields)
    {
        $fields['tdwEmailEnabled'] = new LS\Field\Checkbox('tdwEmailEnabled', 'PUSH-email der Woche');

        return $fields;
    }

    /**
     * @param null|int $companyId
     * @return int
     */
    public function getConsumersEnabledTDW($companyId = null): int
    {
        $filters = [
            ['r' => 'tdwEmailEnabled', 'c' => 'checked']
        ];

        if ($companyId !== null) {
            $filters['companyId'] = $companyId;
        }

        return count(
            \LS\ConsumersHelper::getConsumersIds(false, false, false, $filters)
        );
    }

    /**
     * @param null|int $companyId
     * @param int $sentFrom
     * @param int $sentTo
     * @return int
     */
    public function getSentTDWEmails($companyId = null, $sentFrom = 0, $sentTo = 0)
    {
        $wpdb = LS()->wpdb;
        $companyJoin = $companyId === null ? '' : sprintf("INNER JOIN {$wpdb->prefix}ls_consumers c ON c.consumerId = ce.consumerId AND c.companyId = %d", $companyId);
        $sentTo = $sentTo == 0 ? time() : $sentTo;
        $query = sprintf("SELECT COUNT(DISTINCT ce.emailId) 
                          FROM {$wpdb->prefix}ls_consumers_emails_log ce
                          %s
                          INNER JOIN {$wpdb->prefix}ls_cc_conditional_emails cc ON cc.emailSlug = ce.type AND cc.condition = 'fitco_tdw'
                          WHERE ce.sentDate BETWEEN %d AND %d", $companyJoin, $sentFrom, $sentTo);

        return (int) $wpdb->get_var($query);
    }

    /**
     * @param \LS\ConsumerInterface $consumer
     * @param int $postId
     * @param string $newOption
     * @return bool
     */
    private function changeTaskBoxByConsumer(\LS\ConsumerInterface $consumer, int $postId, string $newOption): bool
    {
        $fieldName = $this->getFieldName($postId);
        $totalTasks = $consumer->getDynamicValue('fitco_tb_count', 0, true);
        $oldOption = $consumer->getDynamicValue($fieldName, self::OPTION_UNDEFINED);

        if ($oldOption == self::OPTION_YES && $newOption != self::OPTION_YES) {
            $totalTasks--;
        } elseif ($newOption == self::OPTION_YES && $oldOption != self::OPTION_YES) {
            $totalTasks++;
        }

        $consumer->addDynamicValue($fieldName, $newOption);
        $consumer->addDynamicValue('fitco_tb_count', $totalTasks);

        return \LS\ConsumersHelper::updateConsumer($consumer);
    }

    /**
     * @internal
     * @ajax
     */
    public function fitcoTaskBox()
    {
        $consumer = consumersLoyaltySuite::getAuthorizedConsumer();

        if (!$consumer) {
            exit;
        }

        $name = $this->getParam('name', 'text', 'POST');
        $option = $this->getParam('option', 'text', 'POST');
        $nonce = $this->getParam('nonce', '', 'POST');

        if (
            empty($name)
            || !wp_verify_nonce($nonce, $name)
            || !in_array($option, [self::OPTION_UNDEFINED, self::OPTION_YES, self::OPTION_NO, self::OPTION_MAYBE])
        ) {
            exit;
        }

        $postId = $this->getPostIdFromFieldName($name);

        if (!$this->changeTaskBoxByConsumer($consumer, $postId, $option)) {
            exit;
        }

        do_action('ls_fitco_tdw_changed', $consumer, $postId, $option);

        exit;
    }

    /**
     * @internal
     * @return string
     */
    public function gbTaskBoxShortcode()
    {
        $post = get_post();

        return $post instanceof WP_Post ? $this->renderTaskBox($post) : '';
    }

    /**
     * @internal
     * @param int $postId Post ID
     */
    public function deletePost($postId)
    {
        \LS\ConsumersHelper::deleteDynamicField(
            $this->getFieldName((int) $postId)
        );
    }

    private function initAT2Action()
    {
        $tdw = $this;

        ATActions::addAction('tdw', (new ATAction())
            ->setLabels([
                'title'        => 'Tipp der Woche mit "ja" bestätigt',
                'openActivity' => 'Tipp der Woche umsetzen',
                'doneActivity' => 'Aktiviert'
            ])
            ->allowEmptyLink(false)
            ->setIdentId('identTDWPostId')
            ->setSettingsFunc(static function ($activity) {
                (new LS\Template(__DIR__ . '/views/backend'))
                    ->assign('activity', $activity)
                    ->render('action.tdw.phtml');
            })
            ->validateActivity(static function ($activity) {

                $valid = true;

                if ($activity['ident'] > 0) {
                    $post = get_post($activity['ident']);
                    if (!($post instanceof WP_Post) || $post->post_status !== 'publish') {
                        $valid = false;
                    }
                }

                return $valid;
            })
            ->setReviewFunc(static function ($consumerId, $activity) use ($tdw) {

                $consumer = \LS\ConsumersHelper::getConsumer($consumerId);

                if ($consumer) {
                    return $tdw->isConsumerAcceptedTDW($consumer, (int) $activity['ident']);
                }

                return false;
            })
            ->setConsumerDetailsLogFunc(static function ($activity) {
                return 'Tipp der Woche "' . get_the_title($activity['ident']) . '" mit "ja" bestätigt';
            })
        );
    }

    /**
     * @internal
     * @param \LS\ConsumerInterface $consumer
     * @param int $postId
     * @param string $option
     */
    public function tdwChangedForAT2(\LS\ConsumerInterface $consumer, int $postId, string $option)
    {
        if ($option === self::OPTION_YES) {

            /** @var at2LoyaltySuite $at2 */
            $at2 = LS()->getModule('at2');
            $at2->activitiesLogic('tdw', $postId, $consumer->getConsumerId(), false);
        }
    }
}