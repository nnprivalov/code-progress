<?php
namespace LS;

class ContentCalendarAdmin extends ContentCalendar implements ModuleAdminInterface
{
    /**
     * @internal
     */
    public function load()
    {
        add_action('wp_dashboard_setup', [$this, 'dashboardSetup']);
        add_action('wp_ajax_update_calendar', [$this, 'updateCalendar']);
        add_action('init', [$this, 'addCronEvent']);
    }

    public function addCronEvent()
    {
        wp_clear_scheduled_hook( 'addTimeLastCornJob' );
        wp_schedule_single_event( $this->getNextCron(), 'add_time_last_cron_job', [$this->getNextCron()]);
        add_action('add_time_last_cron_job', [$this, 'lastCronJobDate']);

    }

    public function getNextCron(){
        $defaultCrons = _get_cron_array();
        $crons = [];
        foreach($defaultCrons as $timestamp => $cron){
            if(isset($cron['add_time_last_cron_job'])){
                unset($cron['add_time_last_cron_job']);
            }
            $crons[$timestamp] = $cron;
        }
        $allTimestamps = array_keys($crons);
        $timestamps = array_filter ( $allTimestamps, function ($timestamp){
            if ($timestamp > time()){
                return $timestamp;
            }
        });
        return (int) $timestamps['0'];
    }


    public function lastCronJobDate($timestamp)
    {
        if ( defined( 'DOING_CRON' ) && DOING_CRON ) {
            $this->setOption( '_lastCronJobRun', $timestamp );
        }
    }


    /**
     * @internal
     */
    public function dashboardSetup()
    {
        $this->printScripts();

        wp_add_dashboard_widget('content_calendar_widget', 'Editorial Calendar', [$this, 'renderWidget']);
    }

    /**
     * @param int $companyId
     * @param \LSConditionalEmail $ce
     * @return bool
     */
    private function hasCompanyAccessToComCenterEmail(int $companyId, \LSConditionalEmail $ce): bool
    {
        if ($ce->getRecipientsGroup() === 'cmp') {

            $companiesIds = wp_parse_id_list($ce->getRecipientsIdent());

            foreach ($companiesIds as $cmpId) {
                $companiesIds = array_merge($companiesIds, \LSCompaniesHelper::getChildrenCompaniesIdsTo($cmpId));
            }

            if (!in_array($companyId, $companiesIds)) {
                return false;
            }
        }

        if ($ce->getRecipientsGroup() === 'acm') {

            $groupIds = wp_parse_id_list($ce->getRecipientsIdent());
            $companyGroupIds = \ACMHelper::getGroupIdsConnectedToCompany($companyId);

            if (!array_intersect($companyGroupIds, $groupIds)) {
                return false;
            }
        }

        return true;
    }

    /**
     * @param int $fromTimestamp
     * @param int $toTimestamp
     * @param false|int $companyId
     * @return ContentCalendarEvent[]
     */
    private function collectEvents(int $fromTimestamp, int $toTimestamp, $companyId = false)
    {
        require_once __DIR__ . '/ContentCalendarEvent.php';

        $postTypes = [
            'post',
            'course',
            '1x1',
            'timeline',
            'activity-tracking',
            'recipe',
            'partner',
            'i-box-tip',
            'portfolio',
            'health-tipp',
            'fitco-news',
            'live-stream',
            'course-lesson',
            'page',
            'b2b',
            'company-channel',
            'fit-tipps'
        ];

        $posts = get_posts([
            'post_type'   => $postTypes,
            'numberposts' => -1,
            'post_status' => [
                'future',
                'publish',
                'private'
            ],
            'date_query'  => [
                'after'     => date('Y-m-d', $fromTimestamp),
                'before'    => date('Y-m-d', $toTimestamp),
                'inclusive' => true
            ]
        ]);

        $events = [];
        /** @var \acmLoyaltySuite $acmModule */
        $acmModule = LS()->getModule('acm');

        /*
         * Posts
         */
        foreach ($posts as $post) {

            if ($companyId && !$acmModule->hasCompanyAccessToPost($companyId, $post->ID)) {
                continue;
            }

            $acmGroups = \ACMHelper::getGroupsByPostId($post->ID);
            $extendedProps = ['allDay' => false];
            $backgroundColor = ContentCalendarEvent::EVENT_COLOR_DEFAULT;

            if (!empty($acmGroups)) {

                $groupsList = array_map(static function (\ACMGroup $group) {
                    return $group->getName();
                }, $acmGroups);

                $extendedProps['eventProps'][] = [
                    'label' => 'CA-Gruppen',
                    'list'  => $groupsList
                ];

                $backgroundColor = ContentCalendarEvent::EVENT_COLOR_CA_POST;
            }

            $events[] = new ContentCalendarEvent(
                $this->getPostTypeLabel($post) . ' ' . strip_tags($post->post_title),
                strtotime($post->post_date),
                get_edit_post_link($post->ID, ''),
                $backgroundColor,
                $extendedProps
            );
        }

        /*
         * Com.Center Emails with the condition type “targetDate”
         */
        $emails = \ComCenterHelper::getConditionalEmails(['__condition' => 'targetDate']);
        $extendedProps = [];

        foreach ($emails as $ce) {

            if ($companyId > 0 && !$this->hasCompanyAccessToComCenterEmail($companyId, $ce)) {
                continue;
            }

            $targetDate = $ce->getTargetDate();

            if ($targetDate >= $fromTimestamp && $targetDate <= $toTimestamp) {

                if ($ce->getSendHour() > 0) {
                    $targetDate += $ce->getSendHour(true) * HOUR_IN_SECONDS;
                    $extendedProps['allDay'] = false;
                }

                $events[] = new ContentCalendarEvent(
                    '[Com.Center] ' . $ce->getTitle(),
                    $targetDate,
                    admin_url('admin.php?page=comCenterLoyaltySuite&view=ce&ceId=' . $ce->getEmailId() . '&referer=' . urlencode($_SERVER['REQUEST_URI'])),
                    ContentCalendarEvent::EVENT_COLOR_COM_CENTER,
                    $extendedProps
                );
            }
        }

        /*
         * AT Challenge start and end date
         */
        if (LS()->isModuleActive('at2')) {

            $challenges = \AT2Model::getChallenges();

            foreach ($challenges as $challenge) {

                $dateStart = strtotime($challenge->dateStart);
                $dateEnd = strtotime($challenge->dateEnd);

                if ($dateStart >= $fromTimestamp && $dateStart <= $toTimestamp) {

                    if ($companyId) {
                        $result = $acmModule->hasCompanyAccessToPost($companyId, $challenge->challengePageId);
                        if (!$result) {
                            continue;
                        }
                    }

                    $events[] = new ContentCalendarEvent(
                        '[AT] Beginn ' . $challenge->title,
                        $dateStart
                    );
                }

                if ($dateEnd >= $fromTimestamp && $dateEnd <= $toTimestamp) {

                    if ($companyId) {
                        $result = $acmModule->hasCompanyAccessToPost($companyId, $challenge->challengePageId);
                        if (!$result) {
                            continue;
                        }
                    }

                    $events[] = new ContentCalendarEvent(
                        '[AT] Ende ' . $challenge->title,
                        $dateEnd,
                        $challenge->challengePageId > 0 ? get_edit_post_link($challenge->challengePageId, '') : null,
                        ContentCalendarEvent::EVENT_COLOR_END_DATE
                    );
                }
            }
        }

        /**
         * Tipp der Woche
         */
        if (LS()->isModuleActive('tippDerWoche')) {

            /** @var \tippDerWocheLoyaltySuite $tdwModule */
            $tdwModule = LS()->getModule('tippDerWoche');
            $tdwPosts = $tdwModule->getTDWPosts($fromTimestamp, $toTimestamp);
            $tdwEmails = \ComCenterHelper::getConditionalEmails(['__condition' => 'fitco_tdw']);

            foreach ($tdwPosts as $post) {

                if ($companyId) {
                    $result = $acmModule->hasCompanyAccessToPost($companyId, $post->ID);
                    if (!$result) {
                        continue;
                    }
                }

                $extendedProps = [];
                $tdwDate = $tdwModule->getTDWDate($post->ID);

                $events[] = new ContentCalendarEvent(
                    '[TDW Startseite] ' . strip_tags($post->post_title),
                    $tdwDate,
                    get_edit_post_link($post, '')
                );

                if (get_post_meta($post->ID, '_fitcoTDWEmail', true)) { // @TODO Move this logic to the TDW module
                    foreach ($tdwEmails as $tdwEmail) {

                        if ($companyId > 0 && !$this->hasCompanyAccessToComCenterEmail($companyId, $tdwEmail)) {
                            continue;
                        }

                        $targetDate = new \DateTime();
                        $targetDate->setTimestamp($tdwDate);
                        $targetDate->modify(-$tdwEmail->getDaysToEvent() . ' days');
                        $targetDate = $targetDate->getTimestamp();

                        if ($tdwEmail->getSendHour() > 0) {
                            $extendedProps['allDay'] = false;
                            $targetDate += $tdwEmail->getSendHour(true) * HOUR_IN_SECONDS;
                        }

                        $events[] = new ContentCalendarEvent(
                            '[Com.Center] ' . $tdwEmail->getTitle(),
                            $targetDate,
                            admin_url('admin.php?page=comCenterLoyaltySuite&view=ce&ceId=' . $tdwEmail->getEmailId() . '&referer=' . urlencode($_SERVER['REQUEST_URI'])),
                            ContentCalendarEvent::EVENT_COLOR_COM_CENTER,
                            $extendedProps
                        );
                    }
                }
            }
        }

        /*
         * Access Codes / FitCode
         */
        $filters = [
            [
                'r' => 'endDate',
                'c' => 'fromDate',
                't' => date('Y-m-d', $fromTimestamp)
            ],
            [
                'r' => 'endDate',
                'c' => 'toDate',
                't' => date('Y-m-d', $toTimestamp)
            ]
        ];

        if ($companyId) {
            $filters['companyId'] = $companyId;
        }

        $codes = \ACMHelper::getAccessCodes('', '', '', $filters);

        foreach ($codes as $code) {
            $events[] = new ContentCalendarEvent(
                '[FitCode] Letzter Tag von ' . $code->getCode(),
                $code->getEndDate(),
                \acmAdminLoyaltySuite::codeEditLink($code->getCodeId()),
                ContentCalendarEvent::EVENT_COLOR_END_FITCODE
            );
        }

        if (LS()->isModuleActive('acm', '3.6.2')) {

            try {
                $acEndingData = \ACMHelper::getEndedConsumerAccesses(
                    new \DatePeriod(
                        new \DateTime('-3 months'),
                        new \DateInterval('P1D'),
                        new \DateTime('+3 months')
                    ), $companyId
                );
            } catch (\Exception $e) {
                _doing_it_wrong(__FUNCTION__, 'Wrong DRC data field', '3.5.11');
                $acEndingData = [];
            }

            /** @var \consumersLoyaltySuite $consumersModule */
            $consumersModule = LS()->getModule('consumers');
            $endingDates = [];

            foreach ($acEndingData as $consumerId => $endDate) {

                if (!isset($endingDates[$endDate])) {
                    $endingDates[$endDate] = [];
                }

                $endingDates[$endDate][] = $consumerId;
            }

            foreach ($endingDates as $endDate => $consumers) {

                $title = '';

                foreach ($consumers as $consumerId) {
                    $consumer = $consumersModule->getConsumer($consumerId);
                    if ($consumer) {
                        $title = $consumersModule->getConsumerNiceName($consumer);
                        break;
                    }
                }

                if (count($consumers) > 1) {
                    $title .= sprintf('und %d weitere Teilnehmer', count($consumers) - 1);
                }

                $events[] = new ContentCalendarEvent(
                    $title . ' den Zugang zum Portal verlieren',
                    strtotime($endDate),
                    null,
                    ContentCalendarEvent::EVENT_COLOR_END_FITCODE
                );
            }
        }

        /**
         * Courses
         */
        if (LS()->isModuleActive('course')) {

            $courses = \LSCourseHelper::getCourses();

            foreach ($courses as $course) {

                if ($course->getStartDate() >= $fromTimestamp && $course->getStartDate() <= $toTimestamp) {

                    if ($companyId) {
                        $result = $acmModule->hasCompanyAccessToPost($companyId, $course->getPost()->ID);
                        if (!$result) {
                            continue;
                        }
                    }

                    $events[] = new ContentCalendarEvent(
                        '[eCoach] Beginn ' . strip_tags($course->getPost()->post_title),
                        $course->getStartDate(),
                        get_edit_post_link($course->getPost(), '')
                    );
                }

                if ($course->getEndDate() >= $fromTimestamp && $course->getEndDate() <= $toTimestamp) {

                    if ($companyId) {
                        $result = $acmModule->hasCompanyAccessToPost($companyId, $course->getPost()->ID);
                        if (!$result) {
                            continue;
                        }
                    }

                    $events[] = new ContentCalendarEvent(
                        '[eCoach] Letzter Tag von ' . strip_tags($course->getPost()->post_title),
                        $course->getEndDate(),
                        get_edit_post_link($course->getPost(), ''),
                        ContentCalendarEvent::EVENT_COLOR_END_DATE
                    );
                }
            }
        }

        /**
         * Personal Challenges
         */
        if (LS()->isModuleActive('pech')) {

            $challenges = \PechHelper::getChallenges();

            foreach ($challenges as $challenge) {

                if ($challenge->getStartDate() >= $fromTimestamp && $challenge->getStartDate() <= $toTimestamp) {

                    if ($companyId) {
                        $result = $acmModule->hasCompanyAccessToPost($companyId, $challenge->getPost()->ID);
                        if (!$result) {
                            continue;
                        }
                    }

                    $events[] = new ContentCalendarEvent(
                        '[Challenge] Start of ' . strip_tags($challenge->getPost()->post_title),
                        $challenge->getStartDate(),
                        get_edit_post_link($challenge->getPost(), '')
                    );
                }

                if ($challenge->getEndDate() >= $fromTimestamp && $challenge->getEndDate() <= $toTimestamp) {

                    if ($companyId) {
                        $result = $acmModule->hasCompanyAccessToPost($companyId, $challenge->getPost()->ID);
                        if (!$result) {
                            continue;
                        }
                    }

                    $events[] = new ContentCalendarEvent(
                        '[Challenge] Letzter Tag von ' . strip_tags($challenge->getPost()->post_title),
                        $challenge->getEndDate(),
                        get_edit_post_link($challenge->getPost(), ''),
                        ContentCalendarEvent::EVENT_COLOR_END_DATE
                    );
                }
            }
        }

        /**
         * Timelines
         */
        if (LS()->isModuleActive('timeline')) {

            $timelines = \timelineLoyaltySuite::getTimelines(false);

            foreach ($timelines as $post) {

                if ($companyId) {
                    $result = $acmModule->hasCompanyAccessToPost($companyId, $post->ID);
                    if (!$result) {
                        continue;
                    }
                }

                $eventDate = \timelineLoyaltySuite::getTimelineEventDate($post->ID);

                if ($eventDate >= $fromTimestamp && $eventDate <= $toTimestamp) {
                    $events[] = new ContentCalendarEvent(
                        '[G-Tag] ' . strip_tags($post->post_title),
                        $eventDate,
                        get_edit_post_link($post, ''),
                        ContentCalendarEvent::EVENT_COLOR_END_DATE
                    );
                }
            }
        }

        return $events;
    }

    /**
     * @param \WP_Post $post
     * @return string
     */
    private function getPostTypeLabel(\WP_Post $post): string
    {
        $obj = get_post_type_object($post->post_type);
        $type = $obj ? $obj->labels->name : $post->post_type;

        return '[' . $type . ']';
    }

    /**
     * @param ContentCalendarEvent[] $events
     * @return array
     */
    private function formatEventsToFullCalendarEvents($events)
    {
        $result = [];

        foreach ($events as $event) {
            $result[] = [
                'title'           => $event->getTitle(),
                'allDay'          => $event->isAllDay(),
                'start'           => $event->getStartDate(),
                'url'             => $event->getUrl(),
                'backgroundColor' => $event->getBackgroundColor(),
                'extendedProps'   => $event->getExtendedProps()
            ];
        }

        return $result;
    }

    /**
     * @internal
     */
    public function renderWidget()
    {
        $fromTimestamp = strtotime(date('Y-m-01') . ' -3 month');
        $toTimestamp = strtotime(date('Y-m-01') . '+4 month') - 1;

        $events = $this->collectEvents($fromTimestamp, $toTimestamp);

        wp_enqueue_script('content-calendar', $this->getUrl2Module() . 'js/content-calendar.js', ['jquery'], $this->getVersion(), true);
        wp_localize_script('content-calendar', 'contentCalendar', [
            'events'    => $this->formatEventsToFullCalendarEvents($events),
            'startDate' => date('Y-m-d', $fromTimestamp),
            'endDate'   => date('Y-m-d', $toTimestamp)
        ]);

        ?>
        <div class="calendar-tools-panel">
            <label for="calendar-select-company">Unternehmen</label>
            <select id="calendar-select-company" autocomplete="off">
                <option value="all">Alle</option>
                <?php
                $companies = \LSCompaniesHelper::getCompaniesList(false, true);
                foreach ($companies as $companyId => $companyName) { ?>
                    <option value="<?=$companyId?>"><?=esc_html($companyName)?></option>
                <?php } ?>
            </select>
            <button class="button print-calendar" style="float: right;">Drucken</button>
        </div>
        <hr>
        <div>
            <?php
            $companies = \LSCompaniesHelper::getCompaniesList();
            require_once __DIR__ . '/CompaniesEmailsListTable.php';
            foreach ($companies as $companyId => $companyName) {
                $company = \LSCompaniesHelper::getCompany($companyId);
                (new \CompaniesEmailsListTable($company))->display();
            }

            echo '<hr>';
            global $wpdb;
            $query = "SELECT gr.name
                                    FROM {$wpdb->prefix}ls_consumers c
                                    INNER JOIN {$wpdb->prefix}ls_companies cp
                                    ON c.companyId = cp.companyId
                                    INNER JOIN {$wpdb->prefix}ls_acm_consumers_codes cc
                                    ON c.consumerId = cc.consumerId
                                    INNER JOIN {$wpdb->prefix}ls_acm_code_groups cg
                                    ON cc.codeId = cg.codeId
                                    INNER JOIN {$wpdb->prefix}ls_acm_groups gr
                                    ON cg.groupId = g.groupId
                                    WHERE c.companyId = %d";

            $companies = \LSCompaniesHelper::getCompaniesList(false, true);
            foreach ($companies as $companyId => $companyName) {
                $contentAcessGroups = $wpdb->get_results(sprintf($query,$companyId));
                echo 'Company Name - <b>"' . $companyName . '"</b>use CA-Groups: <br>';
                    foreach($contentAcessGroups as $group){
                        echo ' - ' . $group->name . '<br>';
                    }
            }


            /*$lastCronJob = $this->getOption( '_lastCronJobRun');
            if ((time() - $lastCronJob) > DAY_IN_SECONDS){
                echo 'true';
            } else {
                echo 'false';
            }

            echo '<hr>';
            echo 'number of received bonus points (last 24 hours)<br>';
            $wpdb = LS()->wpdb;
            $countBP = $wpdb->get_var(sprintf("SELECT SUM(points) FROM {$wpdb->prefix}ls_bp_transactions WHERE createDate > %d AND createDate < %d", time() - DAY_IN_SECONDS, time()));
            if($countBP === null){
                $countBP = 0;
            }
            echo $countBP;
            echo '<hr>';
            $query = sprintf("SELECT COUNT(*) 
                        FROM wp_ls_consumers_log 
                        WHERE messageType = 'inactive' AND createDate > %d", time() - DAY_IN_SECONDS);

            $result = $wpdb->get_var($query);

            echo 'Count deactivated actions in last day: '.$result;
            echo '<hr>';

            $query = sprintf("SELECT COUNT(*) 
                        FROM wp_ls_consumers_log cl 
                        INNER JOIN wp_ls_consumers_fields_values fv
                        ON cl.consumerId = fv.consumerId
                        WHERE cl.createDate = fv.value
                        AND cl.messageType = 'inactive'
                        AND fv.fieldName = 'bounceDeactivation' 
                        AND cl.createDate > %d", time() - DAY_IN_SECONDS);
            $result = $wpdb->get_var($query);
            echo 'Number of deactivated consumers because of Bounce Email: '.$result;
            echo '<hr>';
            $query = sprintf("SELECT quits FROM {$wpdb->prefix}ls_consumers_stats WHERE createDate = '%s'", date('Y-m-d', time()));
            $result = (int) $wpdb->get_var($query);
            echo 'Number of deleted consumers last 24 hours: '.$result;
            echo '<hr>';
            $filters = [
                [
                    'r' => 'endDate',
                    'c' => 'fromDate',
                    't' => date('Y-m-d', time() - WEEK_IN_SECONDS)
                ],
                [
                    'r' => 'endDate',
                    'c' => 'toDate',
                    't' => date('Y-m-d', time())
                ]
            ];
            $codes = \ACMHelper::getAccessCodes('', '', '', $filters);
            echo 'Number of expired access codes (last week): ' . count($codes);
            echo '<hr>';

            $consumers = [];
            foreach($codes as $code){
                $query = sprintf("SELECT cc.consumerId
                                    FROM {$wpdb->prefix}ls_acm_codes cd
                                    LEFT JOIN {$wpdb->prefix}ls_acm_consumers_codes cc 
                                    ON cc.codeId = cd.codeId
                                    WHERE cd.code = '%s'", esc_sql($code->getCode()));
                $result = $wpdb->get_results($query);

                foreach ($result as $item){
                    if (in_array($item->consumerId, $consumers)||($item->consumerId === null)) {
                        continue;
                    }
                    $consumers[] = $item->consumerId;
                }
            }
            echo 'Number of consumers wit expired access codes: '.count($consumers);



            echo '<hr>';
            $TDWLastPost = get_posts([
                'numberposts' => 1,
                'post_status' => 'publish',
                'no_found_rows'  => true,
                // get latest TDW by date
                'meta_key'    => '_fitcoTDW',
                'orderby'     => 'meta_value',

            ]);
            $TWDPost = get_post($TDWLastPost['0']);
            echo 'TWD last Post <br>';
            echo 'Title: ' . $TWDPost->post_title . ' Date post: ' . $TWDPost->post_date;
            echo '<hr>';

            echo 'Edenred LOG<br>';

            $dirEdenredModule = dirname((new \ReflectionClass(EdenredAdmin::class))->getFileName());
            $lastImport = $this->getOption('lastImport');
            $logs = EdenredAdmin::getLogs(0, 3);
            $log = [];
            foreach ($logs as $item){
                $log[] = array_filter($item, function($item) use ($lastImport){
                    if ($item['time'] === $lastImport){
                        return $item;
                    }
                });
            }
            $log = array_filter($log, function($value) { return !empty($value) && $value !== ''; });

            (new Template($dirEdenredModule . '/views/backend'))
                ->assign('logs', $log)
                ->render('logs-page.phtml');



            echo '<hr>';

            echo 'lastStatsTrackNew<br>';
            echo $this->getOption('lastStatsTrackNew');
            echo '<hr>';

            echo 'ContentQuality<br>';


            $dirFitcoModule = dirname((new \ReflectionClass(\FitcoHelper::class))->getFileName());

            (new Template($dirFitcoModule . '/views/backend'))
                ->assign('postsToCheckQuality', \FitcoHelper::getPostsToCheckQuality())
                ->assign('ccEmailsToCheckQuality', \FitcoHelper::getComCenterEmailsToCheckQuality())
                ->render('admin-content-quality.phtml');*/
            


            ?>
        </div>
        <div id="ls-content-calendar"></div>
        <?php
    }

    /**
     * @internal
     * @ajax
     */
    public function updateCalendar()
    {
        $companyId = Library::getParam('companyID', 'id');

        $fromTimestamp = strtotime(date('Y-m-01') . ' -3 month');
        $toTimestamp = strtotime(date('Y-m-01') . '+4 month') - 1;

        $events = $this->collectEvents($fromTimestamp, $toTimestamp, $companyId);

        wp_send_json_success(
            $this->formatEventsToFullCalendarEvents($events)
        );
    }

    /**
     * @internal
     */
    public function printScripts()
    {
        wp_enqueue_style('content-calendar-core', $this->getUrl2Module() . 'css/core.css', [], $this->getVersion());
        wp_enqueue_style('content-calendar-daygrid', $this->getUrl2Module() . 'css/daygrid.css', [], $this->getVersion());
        wp_enqueue_style('content-calendar-timegrid', $this->getUrl2Module() . 'css/timegrid.css', [], $this->getVersion());
        wp_enqueue_style('content-calendar-list', $this->getUrl2Module() . 'css/list.css', [], $this->getVersion());
        wp_enqueue_style('content-calendar-style', $this->getUrl2Module() . 'css/style.css', [], $this->getVersion());

        wp_enqueue_script('jquery-print', $this->getUrl2Module() . 'js/print.min.js', ['jquery'], $this->getVersion(), false);
        wp_enqueue_script('content-calendar-core', $this->getUrl2Module() . 'js/core.min.js', ['jquery'], $this->getVersion(), false);
        wp_enqueue_script('content-calendar-daygrid', $this->getUrl2Module() . 'js/daygrid.min.js', ['jquery'], $this->getVersion(), false);
        wp_enqueue_script('content-calendar-timegrid', $this->getUrl2Module() . 'js/timegrid.min.js', ['jquery'], $this->getVersion(), false);
        wp_enqueue_script('content-calendar-list', $this->getUrl2Module() . 'js/list.min.js', ['jquery'], $this->getVersion(), false);
        wp_enqueue_script('content-calendar-interaction', $this->getUrl2Module() . 'js/interaction.min.js', ['jquery'], $this->getVersion(), false);
        wp_enqueue_script('content-calendar-moment', $this->getUrl2Module() . 'js/moment.min.js', ['jquery'], $this->getVersion(), false);
        wp_enqueue_script('content-calendar-moment-timezone', $this->getUrl2Module() . 'js/momenttimezone.js', ['jquery'], $this->getVersion(), false);
        wp_enqueue_script('content-calendar-luxon', $this->getUrl2Module() . 'js/luxon.min.js', ['jquery'], $this->getVersion(), false);
    }
}