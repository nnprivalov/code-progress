<?php
class CompaniesEmailsListTable extends WP_List_Table
{
    /** @var LS\Company $company */
    private $company;


    public function __construct($company)
    {
        parent::__construct([
            'singular' => 'wp_ls_companies_consumer_emails',
            'plural'   => 'wp_ls_companies_consumer_emails',
            'ajax'     => false,
        ]);
        $this->company = $company;

        $this->prepare_items();
    }

    public function prepare_items()
    {
        $options = $this->get_table_options();

        $this->_column_headers = [
            $this->get_columns(),
            [],
            $this->get_sortable_columns(),
        ];

        $items = ComCenterHelper::paginateConditionalEmails(10, $options['page'], $options['filterTerms'], $options['orderBy'], $options['order']);

        $this->set_pagination_args([
            "total_items" => $items['totalCount'],
            "total_pages" => $items['totalPages'],
            "per_page"    => $items['itemsPerPage']
        ]);

        $conditionEmails = $items['rows'];
        /** @var \LSConditionalEmail $ce */
        foreach($conditionEmails as $ce){
            $resipients = 0;
            global $wpdb;
            $query = "SELECT COUNT(c.consumerId)
                        FROM {$wpdb->prefix}ls_consumers c
                        INNER JOIN {$wpdb->prefix}ls_companies cp
                        ON c.companyId = cp.companyId
                        WHERE cp.companyId = %d";



            switch ($ce->getRecipientsGroup()):
                case 'cmp':
                $companiesIds = wp_parse_id_list($ce->getRecipientsIdent());
                foreach ($companiesIds as $cmpId) {
                    $companiesIds = array_merge($companiesIds, \LSCompaniesHelper::getChildrenCompaniesIdsTo($cmpId));
                }
                    if(!in_array($this->company->getCompanyId(), $companiesIds)){
                        $resipients += (int)$wpdb->get_var(sprintf($query, $this->company->getCompanyId()));
                    }
                    break;
                case 'acm':
                $groupIds = wp_parse_id_list($ce->getRecipientsIdent());
                $companyGroupIds = \ACMHelper::getGroupIdsConnectedToCompany($this->company->getCompanyId());
                    $companyId = array_intersect($companyGroupIds, $groupIds);
                    if(!$companyId){
                        continue;
                        $resipients += (int)$wpdb->get_var(sprintf($query, $this->company->getCompanyId()));
                }
                    break;
                case 'all':
                $resipients += (int)$wpdb->get_var(sprintf($query, $this->company->getCompanyId()));
                    break;
            endswitch;
            if ($resipients < 1){
                continue;
            }
            $this->items[] = (object) [
                'ceID'    => $ce->getEmailId(),
                'title'    => $ce->getTitle(),
                'condition'   => $ce->getConditionName(),
                'RecipientsIdent'   => $ce->getRecipientsGroup(),
                'resipient' => $resipients
            ];
        }
    }

    public function get_columns()
    {
        return [
            'ceID'        => __('ceID'),
            'title'        => __('title'),
            'condition'     => __('condition'),
            'RecipientsIdent'     => __('RecipientsIdent'),
            'resipient' => __('resipient')

        ];
    }

    public function get_sortable_columns()
    {
        return [
            'ceID'             => ['ceID', true],
            'title'             => ['title', true],
            'condition'            => ['condition', true],
            'RecipientsIdent'     => ['RecipientsIdent', true],
            'resipient'     => ['resipient', true],

        ];
    }

    public function column_default($item, $colname)
    {
        switch ($colname) {
            case 'ceID':
                return $item->$colname ;
            case 'title':
                return '<a href="#">' . $item->$colname . '</a>';
            case 'condition':
                return '<a href="#">' . $item->$colname . '</a>';
            case 'RecipientsIdent':
                return '<a href="#">' . $item->$colname . '</a>';
            case 'resipient':
                return '<a href="#">' . $item->$colname . '</a>';
            default:
                return print_r($item, true);
        }

    }

    public function display()
    {
        echo '<div>Company name: <b>' . $this->company->getCompanyName() . '</b></div>';
        parent::display();
    }

    protected function display_tablenav($which)
    {
    }
}