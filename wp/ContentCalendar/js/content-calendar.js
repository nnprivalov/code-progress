jQuery(document).ready(function($) {

    var calendarEl = document.getElementById('ls-content-calendar');
    var calendar = new FullCalendar.Calendar(calendarEl, {
        plugins: ['interaction', 'dayGrid', 'timeGrid', 'list', 'moment', 'luxon'],
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'dayGridMonth,timeGridWeek,listMonth'
        },
        /* Locale options */
        //timeZone: 'UTC',
        locale: 'de', // line can be removed.
        buttonText: {
            today: "heute",
            year: "jahr",
            month: "monat",
            week: "woche",
            day: "tag",
            list: "terminübersicht"
        },
        firstDay: 1,
        weekLabel: "KW",
        allDayText: "ganztägig",
        eventLimitText: function(n) {
            return "+ weitere " + n;
        },
        noEventsMessage: "Keine Ereignisse anzuzeigen",
        monthNames: ["Januar", "Februar", "März", "April", "Mai", "Juni", "Juli", "August", "September", "Oktober", "November", "Dezember"],
        monthNamesShort: ["Jan", "Feb", "Mär", "Apr", "Mai", "Jun", "Jul", "Aug", "Sep", "Okt", "Nov", "Dez"],
        dayNames: ["Sonntag", "Montag", "Dienstag", "Mittwoch", "Donnerstag", "Freitag", "Samstag"],
        dayNamesShort: ["So", "Mo", "Di", "Mi", "Do", "Fr", "Sa"],
        /*titleFormat: {
            month: "MMMM YYYY",
            week: "d [MMMM][yyyy]{ - d MMMM yyyy}",
            day: "dddd, d.MMMM yyyy"
        },*/
        /*columnFormat: {
            month: "dddd",
            week: "ddd d.M.",
            day: "dddd d.M."
        },*/
        timeFormat: "H:mm",

        eventLimit: false,
        /*views: {
            month: {
                eventLimit: 2
            }
        },*/
        validRange: {
            start: contentCalendar.startDate,
            end: contentCalendar.endDate
        },

        //events: dynamicEvents,
        height: 500,
        contentHeight: 'auto',
        aspectRatio: 1,
        displayEventTime: false,
        eventRender: function(info) {

            var extendedProps = info.event.extendedProps,
                eventProps = extendedProps.eventProps || [];

            if (eventProps !== undefined && eventProps.length > 0) {
                $.each(eventProps, function(key, value) {
                    var dgLabel = value.label,
                        list = value.list;

                    if (list.length > 0) {
                        dgLabel += ': ' + list[0];
                    }

                    if (list.length > 1) {
                        dgLabel += ' (+' + (list.length - 1) + ') ';
                    }

                    /* For month view */
                    $(info.el).find('.fc-content')
                        .append('<br><span class="fc-title">' + dgLabel + '</span>');

                    /* For list view */
                    $(info.el).find('.fc-list-item-title')
                        .append('<blockquote>' + value.label + list + '</blockquote>');
                });
            }

        }
    });
    editEventsCalendar(calendar, false);
    calendar.render();

    $("select#calendar-select-company").change(function() {
        var companyID = $(this).children("option:selected").val();
        $('div#ls-content-calendar').css({"pointer-events": "none", "opacity": "0.4"});
        if (companyID === 'all') {
            editEventsCalendar(calendar, false);
            jQuery('div#ls-content-calendar').removeAttr('style');
        } else {
            $.ajax({
                type: "POST",
                cache: false,
                url: LS.ajaxurl,
                dataType: 'json',
                data: {
                    action: 'update_calendar',
                    companyID: companyID
                },
                success: function(response) {
                    jQuery('div#ls-content-calendar').removeAttr('style');
                    editEventsCalendar(calendar, response.data);
                }
            });
        }

    });

    $(calendarEl).on('click', '.fc-day-grid-event', function() {
        $(this).attr('target', '_blank');
    });

    var widget = $('#content_calendar_widget');

    widget.find('h2.hndle')
        .css('font-size', '14px')
        .css('padding', '8px 12px')
        .css('margin', '0')
        .css('line-height', '1.4');

    $('.welcome-panel').before(widget.show());

    $('button.print-calendar').on('click', function() {
        jQuery('.fc-view-container').attr('id', 'fc-view-container');
        printJS({
            printable: 'fc-view-container',
            type: 'html',
            style: '.fc-title {color: black!important}',
            targetStyle: ['*'],
            targetStyles: ['*']
        });
    });

});

function editEventsCalendar(calendar, ajaxEvents) {
    var dynamicEvents = null;

    if (ajaxEvents) {
        dynamicEvents = ajaxEvents;
    } else {
        dynamicEvents = contentCalendar.events;
    }

    var events = calendar.getEvents();
    events.forEach(function(item) {
        item.remove();
    });

    dynamicEvents.forEach(function(item) {
        calendar.addEvent(item);
    });

}