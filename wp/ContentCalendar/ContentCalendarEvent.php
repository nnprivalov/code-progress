<?php
namespace LS;

class ContentCalendarEvent
{
    /**
     * EVENT_COLOR_* - declared background colors for events of calendar.
     */
    const EVENT_COLOR_DEFAULT = '#1abc9c';
    const EVENT_COLOR_CA_POST = '#a7c711';
    const EVENT_COLOR_END_DATE = '#E54215';
    const EVENT_COLOR_END_FITCODE = '#DE7E21';
    const EVENT_COLOR_COM_CENTER = '#295077';

    /** @var string */
    private $title;

    /** @var int */
    private $startDate;

    /** @var string */
    private $url;

    /** @var string */
    private $backgroundColor;

    /** @var array */
    private $extendedProps;

    /**
     * ContentCalendarEvent constructor.
     * @param string $title
     * @param int $startTimestamp
     * @param string|null $url
     * @param string $backgroundColor
     * @param array $extendedProps
     */
    public function __construct(
        string $title,
        int $startTimestamp,
        $url = null,
        $backgroundColor = '',
        $extendedProps = []
    ) {
        $this->title = $title;
        $this->startDate = date('Y-m-d H:i:s', $startTimestamp);
        $this->url = $url ?? '';
        $this->backgroundColor = $backgroundColor;
        $this->extendedProps = $extendedProps;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getStartDate(): string
    {
        return $this->startDate;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @return string
     */
    public function getBackgroundColor(): string
    {
        return $this->backgroundColor;
    }

    /**
     * @return array
     */
    public function getExtendedProps()
    {
        return $this->extendedProps;
    }

    /**
     * @return bool
     */
    public function isAllDay(): bool
    {
        $props = $this->getExtendedProps();

        return !isset($props['allDay']) || $props['allDay'];
    }
}