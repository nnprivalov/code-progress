<?php
namespace LS;

class ContentCalendar extends Module
{
    /**
     * @return string
     */
    public function getVersion()
    {
        return '1.5.1';
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return __('Content Calendar', 'ls');
    }
}