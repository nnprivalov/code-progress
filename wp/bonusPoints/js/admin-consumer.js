jQuery(function($){
    $('#btn-remove-consumer-transactions, #btn-remove-transactions').on('click', function(e) {
        if($(this).attr('disabled') === 'disabled'){
            e.preventDefault();
            return;
        }
        if (!confirm(LS.areYouSure)) {
            e.preventDefault();
        }
    });
});