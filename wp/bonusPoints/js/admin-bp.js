jQuery(function($) {

    addDeleteConfirmation('delete-bonus-trigger');
    addDeleteConfirmation('delete-points');

    // Dashboard
    (function() {

        var dashboard = $('.ls-bp-dashboard');
        if(!dashboard) {
            return;
        }

        dashboard.html('<div class="ls-spinner"></div>');

        $.post(ajaxurl, {
            action: 'bpDashboard',
            date: (new Date()).toISOString().split('T')[0]
        }, function(response) {
            $('#dashboard-spinner').remove();
            if(response.length) {
                dashboard.html(response);
            }
        });

    })();

    // Quick History
    (function() {

        var qh = $('.ls-bp-quick-history');
        if(!qh) {
            return;
        }

        qh.html('<div class="ls-spinner"></div>');

        $.post(ajaxurl, {
            action: 'bpQuickHistory',
            date: (new Date()).toISOString().split('T')[0]
        }, function(response) {
            $('#dashboard-spinner').remove();
            if(response.length) {
                qh.html(response);
            }
        });

    })();

    // Overview table
    if($('.wp_ls_bps').length) {

        // Hide columns from overview table if not items found
        $('.wp_ls_bps .no-items').closest('table').find('thead, tfoot').remove();

        // Hide columns from overview table if not selected in multi-select box
        $('#multiple-select option').each(function() {
            if(!$(this).attr('selected')) {
                $('.wp_ls_bps .column-' + $(this).val()).hide();
            }
        });

        // Customize columns
        $('#multiple-select').multipleSelect({
            width: '400px',
            selectAll: false,
            filter: true,
            countSelected: 0,
            onClick: function(view) {

                $('.wp_ls_bps .column-' + view.value).setVisible(view.checked);

                $.post(ajaxurl, {
                        action: 'saveBPColumnsStructure',
                        columns: $('#multiple-select').multipleSelect("getSelects")
                    }
                );
            }
        });
    }

    // Import process
    if($('.bp-import').length) {

        $('#importStep2').submit(function() {

            var isValid = true;

            if(!$('select.col-ident').val().length) {
                $('#bpStep2Errors').append('<div class="error-box">' + lsBpStrings.noIdentify + '</div>');
                isValid = false;
            }

            if(!$('select.col-points').val().length) {
                $('#bpStep2Errors').append('<div class="error-box">' + lsBpStrings.noPoints + '</div>');
                isValid = false;
            }
            return isValid;
        });
    }

    // Bonus Trigger
    if($('.bonus-trigger-form').length) {

        var trigger = $('.bonus-trigger-form select[name="trigger"]');

        var triggerProcess = function() {

            var identName = trigger.val(),
                pointsField = $('input[name="points"]'),
                alertField = $('textarea[name="consumerAlert"]');
            
            $('.trigger-ident').parent().hide();
            $('.field-' + identName).show();

            pointsField.removeAttr('disabled');
            alertField.removeAttr('disabled');

            if (typeof triggersSettings === 'object') {

                var settings = triggersSettings[identName];

                if (settings) {

                    if (settings.disablePoints) {
                        pointsField
                            .removeAttr('checked')
                            .attr('disabled', 'disabled');
                    }
                    
                    if (settings.disableAlert) {
                        alertField
                            .removeAttr('checked')
                            .attr('disabled', 'disabled');
                    }
                }
            }
        };

        trigger.change(triggerProcess);

        triggerProcess();
    }
});