<?php
namespace LS;

class BPConsumerTransactions extends WP_List_Table
{
    protected $_column_headers;

    public $referer;

    /** @var int */
    private $consumerId;

    /** @var BPHelper */
    private $helper;

    /**
     * @param int $consumerId
     */
    public function __construct(int $consumerId)
    {
        if ($consumerId < 1) {
            return;
        }

        /** @var bonusPointsAdmin $bpModule */
        $bpModule = LS()->getModule('bonusPoints');
        $this->consumerId = $consumerId;
        $this->helper = $bpModule->helper();

        parent::__construct([
            'singular' => 'wp_ls_bp_consumer_transaction',
            'plural'   => 'wp_ls_bp_consumer_transactions',
            'hash'     => '#bp'
        ]);
    }

    /**
     * @return array
     */
    public function get_columns()
    {
        return [
            'createDate' => __('Date', 'ls'),
            'message'    => __('Transaction details', 'ls'),
            'points'     => __('Points', 'ls')
        ];
    }

    /**
     * @return array
     */
    public function get_sortable_columns()
    {
        return [
            'createDate' => ['createDate', true],
            'message'    => ['message', false],
            'points'     => ['points', true]
        ];
    }

    public function prepare_items()
    {
        $options = $this->get_table_options();
        $records = $this->helper->getConsumerTransactions($this->consumerId, $options['orderBy'], $options['order']);

        $this->set_pagination_args([
            "total_items" => count($records),
            "total_pages" => 1,
            "per_page"    => 1
        ]);

        $this->items = $records;
        $this->_column_headers = [$this->get_columns(), [], $this->get_sortable_columns()];
    }

    /**
     * @param BPTransaction $item
     * @param $columnName
     * @return string
     */
    public function column_default($item, $columnName)
    {
        switch ($columnName) {
            case 'points' :
                return number_format($item->getPoints(), 0, ',', '.');
            case 'message' :
                return esc_html($item->getMessage());
            case 'createDate' :
                return date('d.m.Y', $item->getCreateDate() + TIMEZONE_DIFF);
            default:
                return ' ';
        }
    }
}