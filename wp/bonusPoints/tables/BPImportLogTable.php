<?php
namespace LS;

class BPImportLogTable extends WP_List_Table
{
    protected $_column_headers;

    public $referer;

    /** @var BPHelper */
    private $helper;

    public function __construct()
    {
        /** @var bonusPointsAdmin $bp */
        $bp = LS()->getModule('bonusPoints');
        $this->helper = $bp->helper();

        parent::__construct([
            'singular' => 'wp_ls_bp_import_log',
            'plural'   => 'wp_ls_bp_import_logs',
            'hash'     => '#ls-log/import-log'
        ]);
    }

    /**
     * @return array
     */
    public function get_columns()
    {
        return [
            'importId'                => 'ID',
            'fileImportName'          => __('File', 'ls'),
            'status'                  => __('Status', 'ls'),
            'recordsProcessed'        => __('Transactions (processed / failed)', 'ls'),
            'associatedWithConsumers' => __('Associated with consumers', 'ls'),
            'pointsTransferred'       => __('Points transferred', 'ls'),
            'importDate'              => __('Date', 'ls')
        ];
    }

    /**
     * @return array
     */
    public function get_sortable_columns()
    {
        return [
            'importId'                => ['importId', false],
            'fileImportName'          => ['fileImportName', true],
            'status'                  => ['status', false],
            'recordsProcessed'        => ['recordsProcessed', false],
            'associatedWithConsumers' => ['associatedWithConsumers', false],
            'pointsTransferred'       => ['pointsTransferred', true],
            'importDate'              => ['importDate', false]
        ];
    }

    public function prepare_items()
    {
        $options = $this->get_table_options();
        $records = $this->helper->paginateImports(20, $options['page'], $options['orderBy'], $options['order']);

        $this->set_pagination_args([
            "total_items" => $records['totalCount'],
            "total_pages" => $records['totalPages'],
            "per_page"    => $records['itemsPerPage']
        ]);

        $this->items = $records['rows'];
        $this->_column_headers = [$this->get_columns(), [], $this->get_sortable_columns()];
    }

    /**
     * @param BPImport $item
     * @param $columnName
     * @return string
     */
    public function column_default($item, $columnName)
    {
        switch ($columnName) {
            case 'recordsProcessed' :
                return $item->getRecordsProcessed() . ' (' . $item->getRecordsSaved() . ' / ' . $item->getRecordsFailed() . ')';
            case 'importId' :
                return $item->getImportId();
            case 'pointsTransferred' :
                return $item->getPointsTransferred();
            case 'associatedWithConsumers' :
                return $item->getAssociatedWithConsumers();
            case 'fileImportName' :
                return '<a href="admin.php?page=bonusPoints&action=bp-get-file&a=import&importId=' . $item->getImportId() . '">' . ls_kses($item->getImportFileName()) . '</a>';
            case 'status' :
                switch ($item->getStatus()) {
                    case BPImport::STATUS_IMPORT_STARTED:
                        return __('Started', 'ls');
                    case BPImport::STATUS_IMPORT_FINISHED:
                        return __('Finished', 'ls');
                    case BPImport::STATUS_IMPORT_FAILED:
                    default:
                        return __('Failed', 'ls');
                }
            case 'importDate' :
                return $item->getImportDate() > 0 ? date('d.m.Y H:i:s', $item->getImportDate() + TIMEZONE_DIFF) : ' ';
            default:
                return ' ';
        }
    }
}