<?php
namespace LS;

class BPTransactionsTable extends WP_List_Table
{
    protected $_column_headers;

    public $referer;

    /** @var string[] */
    private $fieldsToDisplay;

    /** @var BPMethod[] */
    private $sources;

    /** @var BPHelper */
    private $helper;

    public function __construct()
    {
        $this->fieldsToDisplay = $this->getColumnsStructure();

        /** @var bonusPointsAdmin $bp */
        $bp = LS()->getModule('bonusPoints');
        $this->sources = $bp->getTransactionSources();
        $this->helper = $bp->helper();

        parent::__construct([
            'singular' => 'wp_ls_bp',
            'plural'   => 'wp_ls_bps',
            'hash'     => '#ls-bonus'
        ]);
    }

    /**
     * @return string[]
     */
    public function getFieldsToDisplay()
    {
        return is_array($this->fieldsToDisplay) ? $this->fieldsToDisplay : [];
    }

    /**
     * @param string[] $columns
     */
    public static function setColumnsStructure($columns)
    {
        if (get_current_user_id() > 0) {
            update_user_meta(get_current_user_id(), 'ls_bp_fields', empty($columns) ? self::getDefaultColumnsStructure() : array_unique($columns));
        }
    }

    /**
     * @return string[]
     */
    public static function getDefaultColumnsStructure()
    {
        return ['transactionId', 'ident', 'consumer', 'points', 'message', 'createDate'];
    }

    /**
     * @return string[]
     */
    public function getColumnsStructure()
    {
        $meta = get_user_meta(get_current_user_id(), 'ls_bp_fields', true);
        $meta = empty($meta) ? self::getDefaultColumnsStructure() : $meta;

        return $meta;
    }

    /**
     * @return array
     */
    public function get_columns()
    {
        $cols = [
            'cb'            => '<input type="checkbox" />',
            'transactionId' => 'ID',
            'SKU'           => __('SKU (External ID)', 'ls'),
            'ident'         => __('Identifier', 'ls'),
            'consumer'      => __('Consumer', 'ls'),
            'user'          => __('Added/Imported by', 'ls'),
            'message'       => __('Transaction details', 'ls'),
            'points'        => __('Points', 'ls'),
            'source'        => __('Transaction source', 'ls'),
            'createDate'    => __('Date', 'ls'),
            'agingDate'     => __('Aging date', 'ls')
        ];

        if (!current_user_can('ls_bp_settings')) {
            unset($cols['cb']);
        }

        return $cols;
    }

    /**
     * @return array
     */
    public function get_sortable_columns()
    {
        return [
            'transactionId' => ['transactionId', false],
            'SKU'           => ['SKU', false],
            'ident'         => ['ident', true],
            'consumer'      => ['consumer', false],
            'user'          => ['user', true],
            'message'       => ['message', false],
            'points'        => ['points', true],
            'source'        => ['source', false],
            'createDate'    => ['createDate', true],
            'agingDate'     => ['agingDate', true]
        ];
    }

    public function filter_box()
    {
        $this->__filter_box([
            ['transactionId', 'value'],
            ['SKU', 'string'],
            ['ident', 'string'],
            ['consumer', 'string'],
            ['user', 'string'],
            ['message', 'string'],
            ['points', 'value'],
            ['source', 'select', $this->sources],
            ['createDate', 'date'],
            ['agingDate', 'date']
        ], 3);
    }

    public function prepare_items()
    {
        $options = $this->get_table_options();
        $records = $this->helper->paginateTransactions(50, $options['page'], $options['orderBy'], $options['order'], $options['searchTerm'], $options['filterTerms']);

        $this->set_pagination_args([
            'total_items' => $records['totalCount'],
            'total_pages' => $records['totalPages'],
            'per_page'    => $records['itemsPerPage']
        ]);

        $this->items = $records['rows'];
        $this->_column_headers = [$this->get_columns(), [], $this->get_sortable_columns()];
    }

    /**
     * @return array
     */
    public function get_bulk_actions()
    {
        if (!current_user_can('ls_bp_settings')) {
            return [];
        }

        return [
            'delete-points' => __('Delete', 'ls')
        ];
    }

    /**
     * @param BPTransaction $item
     * @return string
     */
    public function column_cb($item)
    {
        return sprintf('<input type="checkbox" name="transactionId[]" value="%s" />', $item->getTransactionId());
    }

    /**
     * @param BPTransaction $item
     * @param $columnName
     * @return string
     */
    public function column_default($item, $columnName)
    {
        switch ($columnName) {
            case 'transactionId' :
                return (int) $item->getTransactionId();
            case 'points' :
                return $item->getPoints() > 0 ? '<span class="green">' . $item->getPoints() . '</span>' : '<span class="red">' . $item->getPoints() . '</span>';
            case 'SKU' :
                return esc_html($item->getSKU());
            case 'ident' :
                return esc_html($item->getIdent());
            case 'message' :
                return esc_html($item->getMessage());
            case 'consumer' :
                return $item->getConsumerId() > 0 ? '<a href="' . \consumersAdminLoyaltySuite::consumerEditLink($item->getConsumerId(), $this->referer) . '#bp">' . \consumersLoyaltySuite::getConsumerNiceName($item->getConsumerId()) . '</a>' : '';
            case 'user' :
                $user = get_userdata($item->getUserId());

                return $user instanceof \WP_User ? '<a href="' . admin_url('user-edit.php?user_id=' . $item->getUserId()) . '">' . esc_html($user->display_name) . '</a>' : ' ';
            case 'source' :
                return isset($this->sources[$item->getSource()]) ? esc_html($this->sources[$item->getSource()]) : $item->getSource();
            case 'createDate' :
                return $item->getCreateDate() > 0 ? date('d.m.Y', $item->getCreateDate() + TIMEZONE_DIFF) : '—';
            case 'agingDate' :
                return $item->getAgingDate() > 0 ? date('d.m.Y', $item->getAgingDate() + TIMEZONE_DIFF) : '—';
            default:
                return ' ';
        }
    }
}