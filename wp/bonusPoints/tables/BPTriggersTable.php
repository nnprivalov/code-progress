<?php
namespace LS;

class BPTriggersTable extends WP_List_Table
{
    protected $_column_headers;

    public $referer;

    /** @var array */
    private $triggerTypes;

    /** @var BPTrigger[] */
    private $triggerStructure;

    /** @var BPHelper */
    private $helper;

    /**
     * BPTriggersTable constructor.
     */
    public function __construct()
    {
        /** @var bonusPointsAdmin $bpModule */
        $bpModule = LS()->getModule('bonusPoints');
        $this->helper = $bpModule->helper();
        $this->triggerTypes = $bpModule->getBonusTriggerTypes();
        $this->triggerStructure = $bpModule->getBonusTriggerStructure(
            $bpModule->getRegisteredTriggers()
        );

        parent::__construct([
            'singular' => 'wp_ls_bonus_trigger',
            'plural'   => 'wp_ls_bonus_triggers',
            'hash'     => '#ls-earnings/BT-tab'
        ]);
    }

    /**
     * @return array
     */
    public function get_columns()
    {
        $cols = [
            'cb'        => '<input type="checkbox" />',
            'triggerId' => 'ID',
            'name'      => __('Name', 'ls'),
            'trigger'   => __('Trigger', 'ls'),
            'points'    => __('Points', 'ls'),
            'active'    => __('Active', 'ls'),
            'edit'      => __('Edit', 'ls')
        ];

        if (!current_user_can('ls_bp_settings')) {
            unset($cols['cb'], $cols['edit']);
        }

        return $cols;
    }

    /**
     * @return array
     */
    public function get_sortable_columns()
    {
        return [
            'triggerId' => ['triggerId', false],
            'name'      => ['name', false],
            'trigger'   => ['trigger', false],
            'points'    => ['points', true],
            'active'    => ['active', true]
        ];
    }

    public function prepare_items()
    {
        $options = $this->get_table_options();
        $records = $this->helper->getTriggerModels($options['orderBy'], $options['order']);

        $this->set_pagination_args([
            "total_items" => count($records),
            "total_pages" => 1,
            "per_page"    => 1
        ]);

        $this->items = $records;
        $this->_column_headers = [$this->get_columns(), [], $this->get_sortable_columns()];
    }

    /**
     * @return array
     */
    public function get_bulk_actions()
    {
        if (!current_user_can('ls_bp_settings')) {
            return [];
        }

        return [
            'delete-bonus-trigger' => __('Delete', 'ls')
        ];
    }

    /**
     * @param BPTriggerModel $item
     * @return string
     */
    public function column_cb($item)
    {
        return sprintf('<input type="checkbox" name="triggerId[]" value="%s" />', $item->getTriggerId());
    }

    /**
     * @param BPTriggerModel $item
     * @param string $columnName
     * @return string
     */
    public function column_default($item, $columnName)
    {
        switch ($columnName) {
            case 'triggerId' :
                return $item->getTriggerId();
            case 'points' :
                return $item->getPoints();
            case 'name' :
                return $item->getName();
            case 'trigger' :

                $triggerName = $item->getTriggerName();

                if (isset($this->triggerTypes[$triggerName])) {

                    $text = $this->triggerTypes[$triggerName];

                    if ($triggerName != 'button' && isset($this->triggerStructure[$triggerName]) && !empty($item->getTriggerIdent())) {

                        /** @var FieldAbstract $field */
                        $field = $this->triggerStructure[$triggerName];
                        $field->setValue($item->getTriggerIdent());

                        $text .= ' (' . $field->getDisplayValue() . ')';
                    }

                } else {
                    $text = '';
                }

                return $text;

            case 'active' :
                return $item->isActive() ? '<span class="ls-sign-plus">+</span>' : '<span class="ls-sign-cross">-</span>';
            case 'edit' :
                return '<a href="admin.php?page=bonusPoints&view=bonus-trigger&triggerId=' . $item->getTriggerId() . '&referer=' . urlencode($this->referer) . '" class="button">' . __('Edit', 'ls') . '</a>';
            default:
                return ' ';
        }
    }
}