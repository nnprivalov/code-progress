<?php
function bpInstallLoyaltySuite()
{
    $wpdb = LS()->wpdb;

    $tb = $wpdb->get_var(sprintf("SHOW TABLES LIKE '{$wpdb->prefix}ls_bp_transactions'"));
    if (empty($tb)) {
        $wpdb->query(sprintf("CREATE TABLE `{$wpdb->prefix}ls_bp_transactions` (
            `transactionId` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
            `ident` VARCHAR(100) NOT NULL,
            `consumerId` INT(10) UNSIGNED NULL DEFAULT NULL,
            `userId` BIGINT(20) UNSIGNED NULL DEFAULT NULL,
            `points` MEDIUMINT(7) NOT NULL,
            `message` VARCHAR(200) NOT NULL,
            `source` VARCHAR(2) NOT NULL,
            `createDate` INT(11) UNSIGNED NOT NULL,
            `agingDate` INT(11) UNSIGNED NOT NULL,
            PRIMARY KEY (`transactionId`),
            INDEX `ident` (`ident`),
            INDEX `consumerId` (`consumerId`),
            INDEX `userId` (`userId`)
        ) %s ENGINE=InnoDB", LS\Library::getCharsetCollate()));
    }

    $tb = $wpdb->get_var(sprintf("SHOW TABLES LIKE '{$wpdb->prefix}ls_bp_imports'"));
    if (empty($tb)) {
        $wpdb->query(sprintf("CREATE TABLE `{$wpdb->prefix}ls_bp_imports` (
        `importId` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
        `fileName` VARCHAR(400) NOT NULL,
        `fileImportName` VARCHAR(400) NOT NULL,
        `importDate` INT(11) UNSIGNED NOT NULL,
        `status` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
        `recordsProcessed` INT(10) UNSIGNED NOT NULL DEFAULT '0',
        `recordsSaved` INT(10) UNSIGNED NOT NULL DEFAULT '0',
        `pointsTransferred` INT(10) UNSIGNED NOT NULL DEFAULT '0',
        `associatedWithConsumers` INT(10) UNSIGNED NOT NULL DEFAULT '0',
        PRIMARY KEY (`importId`)
      ) %s ENGINE=InnoDB", LS\Library::getCharsetCollate()));
    }

    $tb = $wpdb->get_var(sprintf("SHOW TABLES LIKE '{$wpdb->prefix}ls_bp_triggers'"));
    if (empty($tb)) {
        $wpdb->query(sprintf("CREATE TABLE `{$wpdb->prefix}ls_bp_triggers` (
            `triggerId` SMALLINT(5) UNSIGNED NOT NULL AUTO_INCREMENT,
            `companyId` INT(10) UNSIGNED NOT NULL DEFAULT '0',
            `name` VARCHAR(250) NOT NULL,
            `trigger` VARCHAR(100) NOT NULL,
            `triggerIdent` BIGINT(20) UNSIGNED NOT NULL DEFAULT '0',
            `points` INT(10) NOT NULL,
            `message` VARCHAR(500) NOT NULL,
            `consumerAlert` VARCHAR(500) NOT NULL,
            `active` TINYINT(1) UNSIGNED NOT NULL,
            `startDate` DATE NULL DEFAULT NULL,
            `endDate` DATE NULL DEFAULT NULL,
            `createDate` INT(10) UNSIGNED NOT NULL,
            PRIMARY KEY (`triggerId`)
        ) %s ENGINE=InnoDB", LS\Library::getCharsetCollate()));
    }

    $tb = $wpdb->get_var(sprintf("SHOW TABLES LIKE '{$wpdb->prefix}ls_bp_consumers_triggers'"));
    if (empty($tb)) {
        $wpdb->query(sprintf("CREATE TABLE `{$wpdb->prefix}ls_bp_consumers_triggers` (
            `consumerId` INT(10) UNSIGNED NOT NULL,
            `triggerId` SMALLINT(5) UNSIGNED NOT NULL,
            `triggerDate` INT(10) UNSIGNED NOT NULL,
            PRIMARY KEY (`consumerId`, `triggerId`)
        ) %s ENGINE=InnoDB", LS\Library::getCharsetCollate()));
    }

    if (get_option('ls_bonusPointsLoyaltySuite')) {
        include_once __DIR__ . '/update.php';
        (new \LS\bonusPointsUpdate())->switchToNewName();
    }
}