<?php
namespace LS;

class BPMethod
{
    /** @var string */
    protected $name;

    /** @var string */
    protected $label;

    /** @var null|string|callable */
    protected $settingsCB;

    /** @var bool */
    private $active = false;

    /**
     * @param string $name
     * @param string $label
     * @param null|string|callable $settingsCB
     */
    public function __construct(string $name, string $label, $settingsCB = null)
    {
        $this->name = $name;
        $this->label = $label;
        $this->settingsCB = $settingsCB;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getLabel(): string
    {
        return $this->label;
    }

    /**
     * @return bool
     */
    public function hasSettings(): bool
    {
        return $this->settingsCB !== null;
    }

    /**
     * @return string
     */
    public function getSettings(): string
    {
        if ($this->hasSettings()) {

            if (is_callable($this->settingsCB)) {
                return call_user_func($this->settingsCB);
            }

            return (new Template())->render($this->settingsCB, false);
        }

        return '';
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->active;
    }

    /**
     * @return self
     */
    public function setActive(): BPMethod
    {
        $this->active = true;

        return $this;
    }
}