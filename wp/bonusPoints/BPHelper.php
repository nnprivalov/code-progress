<?php

namespace LS;

class BPHelper
{
    /**
     * Delete module tables
     * @since 1.1.0
     */
    public static function deleteTables()
    {
        $wpdb = LS()->wpdb;

        foreach (['ls_bp_imports', 'ls_bp_consumers_triggers', 'ls_bp_triggers', 'ls_bp_transactions'] as $t) {
            $wpdb->query(sprintf("DROP TABLE IF EXISTS {$wpdb->prefix}%s", $t));
        }
    }

    /**
     * Retrieve trigger models by specified conditions and filters
     *
     * @since 1.4.0
     *
     * @param string $orderBy Field to order
     * @param string $order Order direction
     * @param array $filterTerms Filters
     * @return BPTriggerModel[]
     */
    public static function getTriggerModels(string $orderBy = '', string $order = 'DESC', array $filterTerms = []): array
    {
        $wpdb = LS()->wpdb;
        $order = empty($order) ? 'DESC' : $order;

        if (!in_array($orderBy, ['triggerId', 'name', 'trigger', 'triggerIdent', 'points', 'message', 'active', 'createDate'])) {
            $orderBy = 'triggerId';
        }

        $where = [];

        if (!empty($filterTerms)) {

            $filtersWhere = [];

            foreach ($filterTerms as $filter) {

                if (!isset($filter['r'])) {
                    continue;
                }

                $record = '';
                $field = $filter['r'];
                $condition = $filter['c'] ?? '';
                $value = $filter['t'] ?? '';

                if (in_array($field, ['triggerId', 'name', 'trigger', 'triggerIdent', 'points', 'message', 'active', 'createDate'])) {
                    $record = 'bt.' . $field;
                    if ($field == 'createDate') {
                        $value = strtotime($value);
                    }
                }

                if (!empty($record)) {
                    $filterQuery = Library::generateQueryFromFilter($record, $condition, $value);
                    if (!empty($filterQuery)) {
                        $filtersWhere[] = $filterQuery;
                    }
                }
            }

            if (!empty($filtersWhere)) {
                $xor = isset($filterTerms['x']) && in_array($filterTerms['x'], ['AND', 'OR']) ? $filterTerms['x'] : 'AND';
                $where[] = '(' . implode(') ' . $xor . ' (', $filtersWhere) . ')';
            }
        }

        $where = implode(' AND ', $where);
        $where = empty($where) ? '' : ' WHERE ' . $where;

        $query = sprintf("SELECT bt.* 
                          FROM {$wpdb->prefix}ls_bp_triggers bt %s 
                          GROUP BY bt.triggerId 
                          ORDER BY %s %s", $where, $orderBy, $order);

        $result = $wpdb->get_results($query);
        $triggers = [];

        if (is_array($result)) {
            foreach ($result as $item) {
                $triggers[] = new BPTriggerModel($item);
            }
        }

        return $triggers;
    }

    /**
     * Retrieve trigger models by specific type
     *
     * @since 1.4.0
     *
     * @param string $type Trigger type
     * @return BPTriggerModel[]
     */
    public static function getTriggerModelsByType(string $type)
    {
        return self::getTriggerModels('', '', [
            ['r' => 'trigger', 'c' => 'equals', 't' => $type]
        ]);
    }

    /**
     * Retrieve triggers by specific type
     *
     * @since 1.5.0
     *
     * @param string $type Trigger type
     * @return BPTrigger[]
     */
    public static function getTriggersByType(string $type)
    {
        return array_filter(array_map(static function (BPTriggerModel $triggerModel) {
            return self::getTriggerFromModel($triggerModel);
        }, self::getTriggerModelsByType($type)));
    }

    /**
     * Save trigger
     *
     * @since 1.4.0
     *
     * @param BPTriggerModel $model
     * @return bool True if model saved
     */
    public static function saveTriggerModel(BPTriggerModel $model): bool
    {
        $result = false;

        if ($model->valid()) {

            $wpdb = LS()->wpdb;

            if ($model->getTriggerId() > 0) {

                $wpdb->update($wpdb->prefix . 'ls_bp_triggers', $model->getRecord(), ['triggerId' => $model->getTriggerId()], null, '%d');

                $result = true;

                do_action('ls_bonus_trigger_updated', $model);

            } else {

                $model->setCreateDate(time());

                if ($wpdb->insert($wpdb->prefix . 'ls_bp_triggers', $model->getRecord())) {

                    $triggerModelId = (int) $wpdb->insert_id;

                    $model->setFromArray(['triggerId' => $triggerModelId]);

                    do_action('ls_bonus_trigger_added', $model);

                    // set trigger ident for button's
                    if ($model->getTriggerName() == 'button') {

                        $wpdb->update($wpdb->prefix . 'ls_bp_triggers', ['triggerIdent' => $triggerModelId], ['triggerId' => $triggerModelId], null, '%d');

                        $model->setFromArray(['triggerIdent' => $triggerModelId]);
                    }
                }
            }
        }

        return $result;
    }

    /**
     * Delete all transactions
     * @since 1.5.1
     */
    public static function deleteTriggerModels()
    {
        $wpdb = LS()->wpdb;

        if (defined('PHP_UNIT_TEST')) {
            $wpdb->query("DELETE FROM " . $wpdb->prefix . "ls_bp_triggers");
        } else {
            $wpdb->query("TRUNCATE TABLE " . $wpdb->prefix . "ls_bp_triggers");
        }
    }

    /**
     * Delete bonus trigger model
     *
     * @since 1.4.0
     *
     * @param int $triggerModelId Trigger model Id
     * @return bool True if trigger has been removed
     */
    public static function deleteTriggerModel(int $triggerModelId): bool
    {
        $wpdb = LS()->wpdb;

        return (bool) $wpdb->delete($wpdb->prefix . 'ls_bp_triggers', ['triggerId' => $triggerModelId], '%d');
    }

    /**
     * Retrieve trigger model by ID
     *
     * @since 1.4.0
     *
     * @param int $triggerModelId Trigger Id
     * @return false|BPTriggerModel Bonus trigger or false on failure
     */
    public static function getTriggerModel(int $triggerModelId)
    {
        $wpdb = LS()->wpdb;
        $result = $wpdb->get_row(sprintf("SELECT * FROM {$wpdb->prefix}ls_bp_triggers WHERE triggerId = %d LIMIT 1", $triggerModelId));
        $result = empty($result) ? false : new BPTriggerModel($result);

        return $result;
    }

    /**
     * Check if plugin "Gravity Forms" is activated
     * @since 1.5.0
     * @return bool True if GF activated
     */
    private static function isGravityFormsActivated(): bool
    {
        return is_plugin_active('gravityforms/gravityforms.php') && class_exists('GFCommon');
    }

    /**
     * Retrieve registered trigger names grouped by type
     * @since 1.5.0
     * @return string[] Trigger names and their class names
     */
    public static function getRegisteredTriggerNames()
    {
        static $bonusPointsTriggers;

        if ($bonusPointsTriggers === null) {

            $bonusPointsTriggers = [];

            // @deprecated 07.2019 Legacy
            include_once __DIR__ . '/triggers/ButtonTrigger.php';
            $bonusPointsTriggers['button'] = BonusPoints\ButtonTrigger::class;

            include_once __DIR__ . '/triggers/ConsumerVerification.php';
            $bonusPointsTriggers['consumerVerification'] = BonusPoints\ConsumerVerification::class;

            if (LS()->isModuleActive('consumersVisits')) {

                include_once __DIR__ . '/triggers/ConsumersVisits.php';
                include_once __DIR__ . '/triggers/ConsumersVisitsInPeriod.php';

                $bonusPointsTriggers['consumersVisits'] = BonusPoints\ConsumersVisits::class;
                $bonusPointsTriggers['consumersVisitsInPeriod'] = BonusPoints\ConsumersVisitsInPeriod::class;
            }

            // @deprecated 07.2019 Legacy
            if (self::isGravityFormsActivated()) {
                include_once __DIR__ . '/triggers/GFSubmit.php';
                $bonusPointsTriggers['GFSubmit'] = BonusPoints\GFSubmit::class;
            }

            if (LS()->isModuleActive('at2')) {

                include_once __DIR__ . '/triggers/ATJoin.php';
                $bonusPointsTriggers['at2'] = BonusPoints\ATJoin::class;

                include_once __DIR__ . '/triggers/ATActivity.php';
                $bonusPointsTriggers['at2activity'] = BonusPoints\ATActivity::class;
            }

            $bonusPointsTriggers = apply_filters('ls_bp_registered_triggers', $bonusPointsTriggers);

            foreach ($bonusPointsTriggers as $key => $trigger) {
                if ($trigger instanceof BPTriggerInterface) {

                    $bonusPointsTriggers[$trigger->getName()] = get_class($trigger);
                    unset($bonusPointsTriggers[$key]);

                    // @TODO Update usages after 09.2019
                    // @TODO Uncomment after 10.2019
                    //_doing_it_wrong(__FUNCTION__, 'Hook "ls_bp_registered_triggers" should receive class names (strings) instead of objects', '3.5.0');
                }
            }
        }

        return $bonusPointsTriggers;
    }

    /**
     * Retrieve trigger from a trigger model
     *
     * @since 1.5.0
     *
     * @param BPTriggerModel $triggerModel Trigger model
     * @return false|BPTrigger BPTrigger object or false on failure
     */
    public static function getTriggerFromModel(BPTriggerModel $triggerModel)
    {
        $registeredTriggerNames = self::getRegisteredTriggerNames();

        if (!isset($registeredTriggerNames[$triggerModel->getTriggerName()])) {
            return false;
        }

        return new $registeredTriggerNames[$triggerModel->getTriggerName()]($triggerModel);
    }

    /**
     * Retrieve consumer triggers
     *
     * @since 1.1.0
     *
     * @param int $consumerId Consumer Id
     * @return BPConsumerTrigger[]
     */
    public static function getConsumerTriggers(int $consumerId): array
    {
        $wpdb = LS()->wpdb;
        $result = $wpdb->get_results(sprintf("SELECT * FROM {$wpdb->prefix}ls_bp_consumers_triggers where consumerId = %d", $consumerId));

        $triggers = [];

        foreach ($result as $item) {
            $triggers[] = new BPConsumerTrigger($item);
        }

        return $triggers;
    }

    /**
     * Check if consumer had performed trigger
     *
     * @since 1.1.0
     *
     * @param ConsumerInterface $consumer Consumer
     * @param BPTrigger $trigger Trigger
     * @return bool
     */
    public static function isConsumerPerformedTrigger(ConsumerInterface $consumer, BPTrigger $trigger): bool
    {
        foreach (self::getConsumerTriggers($consumer->getConsumerId()) as $consumerTrigger) {
            if ($consumerTrigger->getTriggerId() === $trigger->getModel()->getTriggerId()) {
                return true;
            }
        }

        return false;
    }

    /**
     * Check if possible to add trigger to consumer
     *
     * @since 1.3.1
     *
     * @param BPTrigger $trigger Trigger
     * @param ConsumerInterface $consumer Consumer
     * @return bool
     */
    public static function canAddTriggerToConsumer(BPTrigger $trigger, ConsumerInterface $consumer): bool
    {
        return !self::isConsumerPerformedTrigger($consumer, $trigger);
    }

    /**
     * Check if consumer can perform trigger
     *
     * @since 1.3.0
     *
     * @param ConsumerInterface $consumer Consumer to perform the trigger
     * @param BPTrigger $trigger Trigger to check
     * @return bool True if consumer can perform trigger
     */
    public static function canConsumerPerformTrigger(ConsumerInterface $consumer, BPTrigger $trigger): bool
    {
        $triggerModel = $trigger->getModel();

        if ($triggerModel->getCompanyId() > 0) {

            $companyIds = \LSCompaniesHelper::includeChildrenCompaniesIds(
                $triggerModel->getCompanyId()
            );

            if (!in_array($consumer->getCompanyId(), $companyIds)) {
                return false;
            }
        }

        if (self::isConsumerPerformedTrigger($consumer, $trigger)) {
            return false;
        }

        return true;
    }

    /**
     * Connect trigger to consumer
     *
     * @since 1.1.0
     *
     * @param BPTrigger $trigger Trigger to add
     * @param ConsumerInterface $consumer Consumer
     * @return bool True if success
     */
    public static function addTriggerToConsumer(BPTrigger $trigger, ConsumerInterface $consumer): bool
    {
        $success = false;

        $tcc = new BPConsumerTrigger([
            'triggerId'   => $trigger->getModel()->getTriggerId(),
            'consumerId'  => $consumer->getConsumerId(),
            'triggerDate' => time()
        ]);

        if ($tcc->valid()) {
            $wpdb = LS()->wpdb;
            if ($wpdb->insert($wpdb->prefix . 'ls_bp_consumers_triggers', $tcc->getRecord(true), '%d')) {
                $success = true;
            }
        }

        return $success;
    }

    /**
     * Delete all triggers and transactions connected to the consumer
     * @since 1.1.3
     * @param int $consumerId Consumer Id
     */
    public static function deleteConsumer(int $consumerId)
    {
        $wpdb = LS()->wpdb;
        $wpdb->delete($wpdb->prefix . 'ls_bp_consumers_triggers', ['consumerId' => $consumerId], '%d');

        self::deleteConsumerTransactions($consumerId);
    }

    /**
     * Delete connection between consumer and trigger
     *
     * @since 1.2.5
     *
     * @param int $triggerModelId Trigger Id
     * @param int $consumerId Consumer Id
     * @return bool True if connection deleted
     */
    public static function deleteTriggerFromConsumer(int $triggerModelId, int $consumerId)
    {
        $wpdb = LS()->wpdb;

        return (bool) $wpdb->delete($wpdb->prefix . 'ls_bp_consumers_triggers', ['triggerId' => $triggerModelId, 'consumerId' => $consumerId], '%d');
    }

    /**
     * Retrieve transactions by specified conditions and filters
     *
     * @since 1.1.0
     *
     * @param int $itemsPerPage Number of records to retrieve
     * @param int $page Page number for pagination. To get all records without pagination set this value to null
     * @param string $orderBy Field to order
     * @param string $order Order direction
     * @param string $searchTerm Search query
     * @param array $filterTerms Filters
     * @return array Array(
     *   'rows' - records array,
     *   'totalCount' - total number of found records,
     *   'totalPages' - total number of pages (for pagination),
     *   'itemsPerPage' - number of records per page
     * )
     */
    public static function paginateTransactions(int $itemsPerPage = 20, int $page = 1, string $orderBy = '', string $order = 'DESC', string $searchTerm = '', array $filterTerms = []): array
    {
        $wpdb = LS()->wpdb;
        $order = empty($order) ? 'DESC' : $order;
        $whereConditions = [];

        if (in_array($orderBy, ['transactionId', 'ident', 'consumerId', 'userId', 'points', 'message', 'source', 'createDate', 'agingDate'])) {
            $orderBy = 'tr.' . $orderBy;
        } elseif ($orderBy == 'SKU') {
            $orderBy = 'CONCAT(tr.source, LPAD(tr.transactionId, 8, "0"))';
        } elseif ($orderBy == 'consumer') {
            $orderBy = 'c.displayName, c.lastName, c.firstName, c.email';
        } elseif ($orderBy == 'user') {
            $orderBy = 'u.display_name';
        } else {
            $orderBy = 'tr.createDate DESC, tr.transactionId';
        }

        // special filter "aging"
        if (isset($filterTerms['aging'])) {

            // get aged transactions for today
            $whereConditions[] = sprintf("tr.agingDate BETWEEN %d AND %d", strtotime('TODAY'), strtotime('TOMORROW') - 1);

            // earning only
            $whereConditions[] = sprintf("tr.points > 0");

            // consumer ID must be specified
            $whereConditions[] = sprintf("tr.consumerId > 0");
        }

        if (!empty($searchTerm)) {

            $columns = [
                'tr.ident',
                'tr.message',
                'CONCAT(c.displayName, " ", c.firstName, " ", c.lastName, " ", c.firstName)',
                'u.display_name'
            ];

            $searchConditions = [];
            foreach ($columns as $c) {
                $searchConditions[] = sprintf("%s LIKE '%%%s%%'", $c, esc_sql(str_replace('\\', '\\\\', $searchTerm)));
            }

            if (!empty($searchConditions)) {
                $whereConditions[] = implode(' OR ', $searchConditions);
            }
        }

        if (!empty($filterTerms)) {

            $filtersWhere = [];

            foreach ($filterTerms as $filter) {

                if (!isset($filter['r'])) {
                    continue;
                }

                $record = '';
                $field = $filter['r'];
                $condition = $filter['c'] ?? '';
                $value = $filter['t'] ?? '';

                if ($field == 'source') {
                    $record = 'tr.' . $field;
                    $value = $filter['s_source'];
                } elseif ($field == 'consumer') {
                    $record = 'CONCAT(c.displayName, " ", c.firstName, " ", c.lastName, " ", c.firstName)';
                } elseif ($field == 'user') {
                    $record = 'u.display_name';
                } elseif (in_array($field, ['transactionId', 'ident', 'consumerId', 'userId', 'points', 'message', 'source', 'createDate', 'agingDate'])) {
                    $record = 'tr.' . $field;
                    if (in_array($field, ['createDate', 'agingDate'])) {
                        $value = strtotime($value);
                    }
                } elseif ($field == 'SKU') {
                    $record = 'CONCAT(tr.source, LPAD(tr.transactionId, 8, "0"))';
                }

                if (!empty($record)) {
                    $filterQuery = Library::generateQueryFromFilter($record, $condition, $value);
                    if (!empty($filterQuery)) {
                        $filtersWhere[] = $filterQuery;
                    }
                }
            }

            if (!empty($filtersWhere)) {
                $xor = isset($filterTerms['x']) && in_array($filterTerms['x'], ['AND', 'OR']) ? $filterTerms['x'] : 'AND';
                $whereConditions[] = '(' . implode(') ' . $xor . ' (', $filtersWhere) . ')';
            }
        }

        $limit = '';

        if ($page > 0 && $itemsPerPage > 0) {
            $limit = sprintf(" LIMIT %d, %d", ($page - 1) * $itemsPerPage, $itemsPerPage);
        }

        $where = empty($whereConditions) ? '' : sprintf(" WHERE (%s)", implode(') AND (', $whereConditions));

        $query = sprintf("SELECT SQL_CALC_FOUND_ROWS tr.*
                          FROM {$wpdb->prefix}ls_bp_transactions tr
                          LEFT JOIN {$wpdb->prefix}ls_consumers c ON c.consumerId = tr.consumerId
                          LEFT JOIN {$wpdb->prefix}users u ON u.ID = tr.userId
                          %s
                          GROUP BY tr.transactionId
                          ORDER BY %s %s %s", $where, $orderBy, $order, $limit);
        $result = $wpdb->get_results($query);

        $transactions = [];

        if (is_array($result)) {
            foreach ($result as $item) {
                $transactions[] = new BPTransaction($item);
            }
        }

        $totalCount = (int) $wpdb->get_var("SELECT FOUND_ROWS()");
        $itemsPerPage = $itemsPerPage > 0 ? $itemsPerPage : $totalCount;
        $totalPages = $totalCount > 0 && $itemsPerPage > 0 ? ceil($totalCount / $itemsPerPage) : 0;

        return [
            'rows'         => $transactions,
            'totalCount'   => $totalCount,
            'totalPages'   => $totalPages,
            'itemsPerPage' => $itemsPerPage
        ];
    }

    /**
     * Retrieve single transaction
     *
     * @since 1.1.0
     *
     * @param int $transactionId transactionId
     * @return false|BPTransaction Transaction or false on failure
     */
    public static function getTransaction(int $transactionId)
    {
        $wpdb = LS()->wpdb;
        $result = $wpdb->get_row(sprintf("SELECT * FROM {$wpdb->prefix}ls_bp_transactions WHERE transactionId = %d LIMIT 1", $transactionId));
        $result = empty($result) ? false : new BPTransaction($result);

        return $result;
    }

    /**
     * Save transaction
     *
     * @since 1.1.0
     *
     * @param BPTransaction $transaction
     * @return bool True if updated
     */
    public static function saveTransaction(BPTransaction $transaction): bool
    {
        $success = false;

        if ($transaction->valid()) {

            $wpdb = LS()->wpdb;

            if ($transaction->getTransactionId() > 0) {

                $wpdb->update($wpdb->prefix . 'ls_bp_transactions', $transaction->getRecord(), ['transactionId' => $transaction->getTransactionId()], null, '%d');

                $success = true;

                do_action('ls_bp_transaction_updated', $transaction);

            } elseif ($wpdb->insert($wpdb->prefix . 'ls_bp_transactions', $transaction->getRecord())) {

                $transaction->setFromArray(['transactionId' => (int) $wpdb->insert_id]);

                $success = true;

                do_action('ls_bp_transaction_added', $transaction);
            }
        }

        return $success;
    }

    /**
     * Delete transaction
     *
     * @since 1.2.5
     *
     * @param int $transactionId Transaction Id
     * @return bool True if has been deleted
     */
    public static function deleteTransaction(int $transactionId): bool
    {
        $success = false;
        $wpdb = LS()->wpdb;

        if ($wpdb->delete($wpdb->prefix . 'ls_bp_transactions', ['transactionId' => $transactionId], '%d')) {
            do_action('ls_bp_transaction_deleted', $transactionId);
            $success = true;
        }

        return $success;
    }

    /**
     * Delete all transactions
     * @since 1.4.2
     */
    public static function deleteTransactions()
    {
        $wpdb = LS()->wpdb;

        if (defined('PHP_UNIT_TEST')) {
            $wpdb->query("DELETE FROM " . $wpdb->prefix . "ls_bp_transactions");
        } else {
            $wpdb->query("TRUNCATE TABLE " . $wpdb->prefix . "ls_bp_transactions");
        }

        do_action('ls_bp_transactions_deleted');
    }

    /**
     * Retrieve total value of points for specified consumer
     *
     * @since 1.1.0
     *
     * @param int $consumerId Consumer ID
     * @return int Number of points
     */
    public static function calculateConsumerBalance(int $consumerId): int
    {
        $wpdb = LS()->wpdb;

        return (int) $wpdb->get_var(sprintf("SELECT SUM(points) FROM {$wpdb->prefix}ls_bp_transactions WHERE consumerId = %d", $consumerId));
    }

    /**
     * Check if consumer has specified amount of points
     *
     * @since 1.1.0
     *
     * @param int $consumerId Consumer Id
     * @param int $points Points amount
     * @return bool True if consumer have enough points
     */
    public static function hasConsumerEnoughPoints(int $consumerId, int $points): bool
    {
        return self::calculateConsumerBalance($consumerId) >= $points;
    }

    /**
     * Check if total points > 0
     * If return true => transactions exist
     *
     * @since 1.4.4
     * @return bool
     */
    public static function hasTransactions(): bool
    {
        return self::getTotalPoints() > 0;
    }

    /**
     * Retrieve consumers ids who has transactions
     * @since 1.4.4
     * @return array
     */
    public static function getTransactionsConsumersIds()
    {
        $wpdb = LS()->wpdb;
        $result = $wpdb->get_col("SELECT DISTINCT consumerId FROM {$wpdb->prefix}ls_bp_transactions ORDER BY consumerId");

        return array_map('intval', $result);
    }

    /**
     * Retrieve consumer transactions
     *
     * @since 1.1.0
     *
     * @param int $consumerId ConsumerId
     * @param string $orderBy Field to order
     * @param string $order Order direction
     * @return BPTransaction[]
     */
    public static function getConsumerTransactions(int $consumerId, string $orderBy = '', string $order = 'DESC')
    {
        $order = empty($order) ? 'DESC' : $order;

        if (!in_array($orderBy, ['createDate', 'message', 'points'])) {
            $orderBy = 'createDate ' . $order . ', transactionId';
        }

        $wpdb = LS()->wpdb;
        $query = sprintf("SELECT * FROM {$wpdb->prefix}ls_bp_transactions WHERE consumerId = %d ORDER BY %s %s", $consumerId, $orderBy, $order);
        $result = $wpdb->get_results($query);

        $transactions = [];

        if (is_array($result)) {
            foreach ($result as $item) {
                $transactions[] = new BPTransaction($item);
            }
        }

        return $transactions;
    }

    /**
     * Reset consumer points balance
     *
     * @since 1.4.2
     *
     * @param int $consumerId Consumer Id
     * @return bool True on success
     */
    public static function deleteConsumerTransactions(int $consumerId)
    {
        $result = false;
        $transactions = self::getConsumerTransactions($consumerId);

        foreach ($transactions as $transaction) {
            if (self::deleteTransaction($transaction->getTransactionId())) {
                $result = true;
            }
        }

        return $result;
    }

    /**
     * Retrieve total bonus points
     * @since 1.1.0
     * @return int
     */
    private static function getTotalPoints(): int
    {
        $wpdb = LS()->wpdb;

        return (int) $wpdb->get_var(sprintf("SELECT SUM(points) FROM {$wpdb->prefix}ls_bp_transactions"));
    }

    /**
     * Retrieve received points in specified time range
     *
     * @since 1.1.0
     *
     * @param int $startDate Start timestamp
     * @param null|int $endDate End timestamp
     * @return int
     */
    private static function getReceivedPoints(int $startDate = 0, $endDate = null): int
    {
        if ($endDate === 0) {
            return 0;
        }

        $wpdb = LS()->wpdb;
        $startDate = $startDate < 0 ? 0 : (int) $startDate;
        $endDate = $endDate === null ? current_time('timestamp') : (int) $endDate;
        $query = sprintf("SELECT SUM(points) FROM {$wpdb->prefix}ls_bp_transactions WHERE points >= 0 AND createDate BETWEEN %d AND %d", $startDate, $endDate);
        $result = $wpdb->get_var($query);

        return (int) $result;
    }

    /**
     * Retrieve spent points in specified time range
     *
     * @since 1.1.0
     *
     * @param int $startDate Start timestamp
     * @param null|int $endDate End timestamp
     * @return int
     */
    private static function getSpentPoints(int $startDate = 0, $endDate = null): int
    {
        if ($endDate === 0) {
            return 0;
        }

        $wpdb = LS()->wpdb;
        $startDate = $startDate < 0 ? 0 : (int) $startDate;
        $endDate = $endDate === null ? current_time('timestamp') : (int) $endDate;
        $query = sprintf("SELECT SUM(points) FROM {$wpdb->prefix}ls_bp_transactions WHERE points < 0 AND createDate BETWEEN %d AND %d", $startDate, $endDate);
        $result = $wpdb->get_var($query);

        return abs((int) $result);
    }

    /**
     * Retrieve expired points in specified time range
     *
     * @since 1.1.0
     *
     * @param int $startDate Start timestamp
     * @param null|int $endDate End timestamp
     * @return int
     */
    private static function getExpiredPoints(int $startDate = 0, $endDate = null): int
    {
        if ($endDate === 0) {
            return 0;
        }

        $wpdb = LS()->wpdb;
        $startDate = $startDate < 0 ? 0 : (int) $startDate;
        $endDate = $endDate === null ? current_time('timestamp') : (int) $endDate;
        $query = sprintf("SELECT SUM(points) FROM {$wpdb->prefix}ls_bp_transactions WHERE source = '%s' AND createDate BETWEEN %d AND %d", BPTransaction::SOURCE_AGING, $startDate, $endDate);
        $result = $wpdb->get_var($query);

        return abs((int) $result);
    }

    /**
     * Retrieve dashboard date in specified time range
     *
     * @since 1.1.0
     *
     * @param int $startPeriod Start timestamp
     * @param int $endPeriod End timestamp
     * @return object Dashboard data
     */
    private static function getDashboardForPeriod(int $startPeriod, int $endPeriod)
    {
        return (object) [
            'startDate'      => $startPeriod,
            'endDate'        => $endPeriod,
            'pointsReceived' => self::getReceivedPoints($startPeriod, $endPeriod),
            'pointsSpent'    => self::getSpentPoints($startPeriod, $endPeriod),
            'pointsExpired'  => self::getExpiredPoints($startPeriod, $endPeriod)
        ];
    }

    /**
     * Retrieve dashboard in specified day
     *
     * @since 1.1.0
     *
     * @param null|int $today Day (timestamp)
     * @return object Dashboard data
     */
    public static function getDashboard($today = null)
    {
        $today = $today === null ? strtotime('TODAY', current_time('timestamp')) : (int) $today;

        $result = wp_cache_get('ls_bp_dashboard_' . $today, 'bp');
        if ($result === false) {

            $result = (object) [
                'totalPoints'    => self::getTotalPoints(),
                'pointsReceived' => self::getReceivedPoints(),
                'pointsSpent'    => self::getSpentPoints(),
                'pointsExpired'  => self::getExpiredPoints()
            ];

            wp_cache_add('ls_bp_dashboard_' . $today, $result, 'bp', 60);
        }

        return $result;
    }

    /**
     * Retrieve quick history in specified day
     *
     * @since 1.1.0
     *
     * @param null|int $today Day (timestamp)
     * @return object Dashboard data
     */
    public static function getQuickHistory($today = null)
    {
        $today = $today === null ? strtotime('TODAY', current_time('timestamp')) : (int) $today;

        $result = wp_cache_get('ls_bp_quick_history_' . $today, 'bp');
        if ($result === false) {

            $result = (object) [
                'now'       => $today,
                'today'     => self::getDashboardForPeriod($today, strtotime('TOMORROW', $today) - 1),
                'yesterday' => self::getDashboardForPeriod(strtotime('YESTERDAY', $today), $today - 1),
                '-2 DAYS'   => self::getDashboardForPeriod(strtotime('-2 DAYS', $today), strtotime('-1 DAY', $today) - 1),
                'thisMonth' => self::getDashboardForPeriod(strtotime(date('Y-m-01', $today)), strtotime('first day of next month', $today) - 1),
                'thisYear'  => self::getDashboardForPeriod(strtotime(date('Y-01-01', $today)), strtotime(date('Y-12-31', $today)))
            ];

            wp_cache_add('ls_bp_quick_history_' . $today, $result, 'bp', 60);
        }

        return $result;
    }

    /**
     * Retrieve consumers to send conditional email "You reached X points"...
     * Note: one consumer can receive only one email
     *
     * @since 1.1.0
     *
     * @param int $balance Required consumers balance
     * @param string $emailSlug Conditional email slug to prevent duplicates
     * @return int[] A list of consumers ID's
     */
    public static function getConsumersForBalanceConditionalEmail(int $balance, string $emailSlug): array
    {
        if ($balance < 1 || empty($emailSlug)) {
            return [];
        }

        $wpdb = LS()->wpdb;
        $query = sprintf("SELECT cfv.consumerId
                          FROM {$wpdb->prefix}ls_consumers_fields_values cfv
                          INNER JOIN {$wpdb->prefix}ls_consumers c ON c.consumerId = cfv.consumerId AND c.active = 1 AND c.allowEmails = 1 AND c.verified = 1
                          WHERE cfv.fieldName = 'bpBalance' AND cfv.value >= %d
                          AND NOT EXISTS ( SELECT 1 FROM {$wpdb->prefix}ls_consumers_emails_log WHERE consumerId = cfv.consumerId AND sent = 1 AND type = '%s' )", $balance, esc_sql($emailSlug));
        $result = $wpdb->get_col($query);
        $result = is_array($result) ? $result : [];

        return $result;
    }

    /**
     * Retrieve consumers to send conditional email "Points aged"...
     * @since 1.1.0
     * @return array A list of objects (consumerId, points)
     */
    public static function getConsumersToSendAgedConditionalEmail(): array
    {
        $wpdb = LS()->wpdb;
        $query = sprintf("SELECT bpt.consumerId, bpt.points, bpt.createDate
                          FROM {$wpdb->prefix}ls_bp_transactions bpt
                          INNER JOIN {$wpdb->prefix}ls_consumers c ON c.consumerId = bpt.consumerId AND c.active = 1 AND c.allowEmails = 1 AND c.verified = 1
                          WHERE bpt.source = '%s' AND bpt.createDate >= %d", BPTransaction::SOURCE_AGING, strtotime('TODAY'));
        $result = $wpdb->get_results($query);
        $result = is_array($result) ? $result : [];

        foreach ($result as &$item) {
            $item->consumerId = (int) $item->consumerId;
            $item->points = (int) $item->points;
            $item->createDate = (int) $item->createDate;
        }

        unset($item);

        return $result;
    }

    /**
     * Retrieve a path where plugin store import files
     * @since 1.1.0
     * @return string Path to import filer
     */
    public static function getImportFilesFolder(): string
    {
        return wp_upload_dir()['basedir'] . DIRECTORY_SEPARATOR . 'bonus_points' . DIRECTORY_SEPARATOR . 'import';
    }

    /**
     * Retrieve path to file by name
     *
     * @since 1.1.0
     *
     * @param string $file File name
     * @return string Full path to the file
     */
    public static function getPathToFile(string $file): string
    {
        return self::getImportFilesFolder() . DIRECTORY_SEPARATOR . $file;
    }

    /**
     * Retrieve imports by specified conditions and filters
     *
     * @since 1.1.0
     *
     * @param int $itemsPerPage Number of records to retrieve
     * @param int $page Page number for pagination. To get all records without pagination set this value to null
     * @param string $orderBy Field to order
     * @param string $order Order direction
     * @return array Array(
     *   'rows' - records array,
     *   'totalCount' - total number of found records,
     *   'totalPages' - total number of pages (for pagination),
     *   'itemsPerPage' - number of records per page
     * )
     */
    public static function paginateImports(int $itemsPerPage = 20, int $page = 1, string $orderBy = '', string $order = 'DESC'): array
    {
        $wpdb = LS()->wpdb;
        $order = empty($order) ? 'DESC' : $order;

        if (in_array($orderBy, ['fileImportName', 'status', 'recordsProcessed', 'recordsSaved', 'recordsFailed', 'associatedWithConsumers', 'notAssociatedWithConsumers', 'importDate'])) {
            $orderBy = 'bpi.' . $orderBy;
        } else {
            $orderBy = 'bpi.importId';
        }

        $limit = '';

        if ($page > 0 && $itemsPerPage > 0) {
            $limit = sprintf(" LIMIT %d, %d", ($page - 1) * $itemsPerPage, $itemsPerPage);
        }

        $query = sprintf("SELECT SQL_CALC_FOUND_ROWS bpi.*
                          FROM {$wpdb->prefix}ls_bp_imports bpi
                          WHERE bpi.status != %d
                          GROUP BY bpi.importId
                          ORDER BY %s %s %s", BPImport::STATUS_IMPORT_STARTED, $orderBy, $order, $limit);
        $result = $wpdb->get_results($query);

        $imports = [];

        if (is_array($result)) {
            foreach ($result as $item) {
                $imports[] = new BPImport($item);
            }
        }

        $totalCount = (int) $wpdb->get_var("SELECT FOUND_ROWS()");
        $itemsPerPage = $itemsPerPage > 0 ? $itemsPerPage : $totalCount;
        $totalPages = $totalCount > 0 && $itemsPerPage > 0 ? ceil($totalCount / $itemsPerPage) : 0;

        return [
            'rows'         => $imports,
            'totalCount'   => $totalCount,
            'totalPages'   => $totalPages,
            'itemsPerPage' => $itemsPerPage
        ];
    }

    /**
     * Save import
     *
     * @since 1.1.0
     *
     * @param BPImport $import
     * @return bool
     */
    public static function saveImport(BPImport $import): bool
    {
        $result = false;

        if ($import->valid()) {

            $wpdb = LS()->wpdb;

            if ($import->getImportId() > 0) {

                $wpdb->update($wpdb->prefix . 'ls_bp_imports', $import->getRecord(), ['importId' => $import->getImportId()], null, '%d');
                $result = true;

            } else {
                if ($wpdb->insert($wpdb->prefix . 'ls_bp_imports', $import->getRecord())) {
                    $import->setFromArray(['importId' => (int) $wpdb->insert_id]);
                    $result = true;
                }
            }
        }

        return $result;
    }

    /**
     * Start import process
     *
     * @since 1.1.0
     *
     * @param string $fileImportName File name
     * @param string $pathToFile Path to import file
     * @return int Import ID
     */
    public static function startImport(string $fileImportName, string $pathToFile): int
    {
        $importId = 0;

        if (!empty($pathToFile) && file_exists($pathToFile) && is_readable($pathToFile) && !empty($fileImportName)) {

            // get path to import folder
            $savePath = self::getImportFilesFolder();
            if (!is_dir(realpath($savePath)) && !Library::createPrivateFolder($savePath)) {
                $savePath = false;
            }

            if ($savePath) {

                $fileName = microtime(true) . '.' . self::getExt($fileImportName);

                if (copy($pathToFile, $savePath . DIRECTORY_SEPARATOR . $fileName)) {

                    $import = new BPImport([
                        'fileName'       => $fileName,
                        'fileImportName' => $fileImportName,
                        'importDate'     => time(),
                        'status'         => BPImport::STATUS_IMPORT_STARTED
                    ]);

                    if (self::saveImport($import)) {
                        $importId = $import->getImportId();
                    }
                }
            }
        }

        return $importId;
    }

    /**
     * Delete import information
     *
     * @since 1.2.5
     *
     * @param int $importId Import Id
     * @return bool True if deleted
     */
    public static function deleteImport(int $importId): bool
    {
        $wpdb = LS()->wpdb;

        return $wpdb->delete($wpdb->prefix . 'ls_bp_imports', ['importId' => $importId], '%d');
    }

    /**
     * Retrieve import by ID
     *
     * @since 1.1.0
     *
     * @param int $importId Import Id
     * @return false|BPImport Import object or false on failure
     */
    public static function getImport(int $importId)
    {
        $wpdb = LS()->wpdb;
        $result = $wpdb->get_row(sprintf("SELECT * FROM {$wpdb->prefix}ls_bp_imports WHERE importId = %d LIMIT 1", $importId));
        $result = empty($result) ? false : new BPImport($result);

        return $result;
    }

    /**
     * Get file extension
     *
     * @since 1.1.0
     *
     * @param string $file File name
     * @return string File extension
     */
    public static function getExt(string $file): string
    {
        return substr($file, strrpos($file, '.') + 1);
    }

    /**
     * Retrieve trigger by ID
     *
     * @since 1.1.0
     * @deprecated 05.2019 use getTriggerModel()
     *
     * @param int $triggerModelId Trigger Id
     * @return false|BPTriggerModel Bonus trigger or false on failure
     */
    public static function getTrigger(int $triggerModelId)
    {
        _deprecated_function(__FUNCTION__, '1.4.0', 'getTriggerModel');

        return self::getTriggerModel($triggerModelId);
    }

    /**
     * Retrieve bonus points by specified conditions and filters
     *
     * @since 1.1.0
     * @deprecated 05.2019 Use getTriggerModels()
     *
     * @param bool $activeOnly True to retrieve only active triggers
     * @param string $orderBy Field to order
     * @param string $order Order direction
     * @param array $filterTerms Filters
     * @return BPTriggerModel[]
     */
    public static function getTriggers(bool $activeOnly = true, string $orderBy = '', string $order = 'DESC', array $filterTerms = []): array
    {
        _deprecated_function(__FUNCTION__, '1.4.0', 'getTriggerModels');

        return self::getTriggerModels($orderBy, $order, $filterTerms);
    }
}