<?php
namespace LS;

class BPTriggerModel extends Model
{
    /** @var int */
    protected $triggerId;

    /** @var int */
    protected $companyId;

    /** @var string */
    protected $name;

    /** @var string */
    protected $trigger;

    /** @var int */
    protected $triggerIdent;

    /** @var int */
    protected $points;

    /** @var string */
    protected $message;

    /** @var string */
    protected $consumerAlert;

    /** @var bool */
    protected $active;

    /** @var int */
    protected $startDate;

    /** @var int */
    protected $endDate;

    /** @var int */
    protected $createDate;

    /** @var string[] */
    protected $primaryKeys = [
        'triggerId'
    ];

    /** @var string[] */
    protected $dbCols = [
        'triggerId',
        'companyId',
        'name',
        'trigger',
        'triggerIdent',
        'points',
        'message',
        'consumerAlert',
        'active',
        'startDate',
        'endDate',
        'createDate'
    ];

    /**
     * @param array $data Data to be formatted
     * @return array Formatted data
     */
    protected function format($data)
    {
        foreach ($data as $key => $value) {
            if (in_array($key, ['triggerId', 'companyId', 'triggerIdent', 'points'])) {
                $data[$key] = (int) $value;
            } elseif ($key == 'active') {
                $data[$key] = (bool) $value;
            } elseif (in_array($key, ['message', 'consumerAlert'])) {
                $data[$key] = trim(Library::substr($value, 0, 500));
            } elseif ($key == 'name') {
                $data[$key] = trim(substr($value, 0, 200));
            } elseif ($key == 'trigger') {
                $data[$key] = trim(substr($value, 0, 100));
            } elseif (in_array($key, ['createDate', 'startDate', 'endDate'])) {
                $data[$key] = max(0, !empty($value) && !is_numeric($value) ? strtotime($value) : (int) $value);
            }
        }

        return $data;
    }

    /**
     * @return self
     */
    public function setDefault()
    {
        $this->triggerId =
        $this->companyId =
        $this->triggerIdent =
        $this->points =
        $this->startDate =
        $this->endDate =
        $this->createDate = 0;

        $this->name =
        $this->trigger =
        $this->message =
        $this->consumerAlert = '';

        $this->active = false;

        return $this;
    }

    /**
     * @return bool True if the record is valid
     */
    public function valid(): bool
    {
        return
            !empty($this->getName())
            && !empty($this->getTriggerName())
            && (empty($this->getEndDate()) || empty($this->getStartDate()) || $this->getEndDate() >= $this->getStartDate());
    }

    /**
     * @return int
     */
    public function getTriggerId(): int
    {
        return $this->triggerId;
    }

    /**
     * @param int $triggerId
     * @return self
     */
    public function setId(int $triggerId): BPTriggerModel
    {
        $this->triggerId = (int) $triggerId;

        return $this;
    }

    /**
     * @return int
     */
    public function getCompanyId(): int
    {
        return $this->companyId;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @deprecated 08.2019 Use getTriggerName()
     * @return string
     */
    public function getTrigger(): string
    {
        // @TODO Uncomment after 10.2019
        //_deprecated_function(__FUNCTION__, '1.5.0', 'getTriggerName');

        return $this->trigger;
    }

    /**
     * @return string
     */
    public function getTriggerName(): string
    {
        return $this->trigger;
    }

    /**
     * @return int
     */
    public function getTriggerIdent(): int
    {
        return $this->triggerIdent;
    }

    /**
     * @return int
     */
    public function getPoints(): int
    {
        return $this->points;
    }

    /**
     * @param int $points
     * @return BPTriggerModel
     */
    public function setPoints(int $points): BPTriggerModel
    {
        $this->points = $points;

        return $this;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @return string
     */
    public function getConsumerAlert(): string
    {
        return $this->consumerAlert;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->active;
    }

    /**
     * @return int
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * @return int
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * @return int
     */
    public function getCreateDate(): int
    {
        return $this->createDate;
    }

    /**
     * @param int $timestamp
     * @return self
     */
    public function setCreateDate(int $timestamp): BPTriggerModel
    {
        $this->createDate = $timestamp;

        return $this;
    }

    /**
     * @param bool $withPrimaryKeys True to include primary keys data
     * @return array An array of columns and their values
     */
    public function getRecord($withPrimaryKeys = false)
    {
        $data = [];

        foreach ($this->getDbColumns($withPrimaryKeys) as $col) {
            if ($col == 'active') {
                $data[$col] = (int) $this->{$col};
            } elseif ($col == 'startDate' || $col == 'endDate') {
                $data[$col] = empty($this->{$col}) ? null : date('Y-m-d', $this->{$col});
            } else {
                $data[$col] = $this->{$col};
            }
        }

        return $data;
    }
}