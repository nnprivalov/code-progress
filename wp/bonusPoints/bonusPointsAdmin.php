<?php
namespace LS;

class bonusPointsAdmin extends bonusPoints implements ModuleAdminInterface
{
    /**
     * @since 1.2.1
     * @return bool
     */
    public function hasAdministrationPage(): bool
    {
        return true;
    }

    /**
     * Main function to load models, create actions and filters, init variables
     * @since 1.2.0
     * @internal
     */
    public function load()
    {
        parent::load();

        add_action('admin_enqueue_scripts', [$this, 'printScripts']);
        add_action('admin_init', [$this, 'adminInit']);
        add_action('wp_ajax_bpDashboard', [$this, 'getDashboard']);
        add_action('wp_ajax_bpQuickHistory', [$this, 'getQuickHistory']);
        add_action('wp_ajax_saveBPColumnsStructure', [$this, 'saveColumnsStructure']);
        add_action('ls_cron_daily', [$this, 'cronJobDaily']);

        // Consumers hooks
        add_filter('consumerDetails', [$this, 'consumerDetailsFilter'], 12, 2);
        add_filter('ls_consumer_log__bpBalance', [$this, 'consumerLog']);
        add_filter('ls_consumer_log__bp_status_silver', [$this, 'consumerLog']);
        add_filter('ls_consumer_log__bp_status_silver_removed', [$this, 'consumerLog']);
        add_filter('ls_consumer_log__bp_trigger', [$this, 'consumerLog']);
        add_filter('ls_consumer_log__reset_bonus_points', [$this, 'consumerLog']);

        // AT2 hooks
        if (LS()->isModuleActive('at2')) {
            add_action('ls_at_activity_fields', [$this, 'atActivityFields']);
            add_action('ls_at_save_activity', [$this, 'atSaveActivity'], 10, 2);
            add_action('ls_at_delete_activity', [$this, 'atDeleteActivity']);
        }

        // Rewards hooks
        if (LS()->isModuleActive('rewards')) {
            add_filter('ls_bp_settings_updated', [$this, 'bpRewardsSettingsUpdated']);
        }
    }

    /**
     * Create shortcodes
     * @since 1.2.0
     */
    public function initShortcodes()
    {
        parent::initShortcodes();

        LS()->addShortcodeInfo(
            'bonus-points',
            __('Bonus Points', 'ls'),
            __('Display points statement', 'ls'),
            $this->getName(),
            [
                'ident' => __('Identifier (optional)', 'ls')
            ]
        );

        LS()->addShortcodeInfo(
            'bonus-points-overview',
            __('Bonus Points Overview', 'ls'),
            __('Display transactions overview table with a list of transactions related to the current consumer', 'ls'),
            $this->getName(),
            [
                'ident' => __('Identifier (optional)', 'ls')
            ]
        );

        LS()->addShortcodeInfo(
            'bonus-points-trigger',
            __('Bonus Points trigger', 'ls'),
            __('Display trigger button', 'ls'),
            $this->getName(),
            [
                'id'    => __('Trigger ID (required)', 'ls'),
                'class' => __('Specific class names (optional)', 'ls'),
                'href'  => __('Redirect link (optional)', 'ls')
            ],
            '[bonus-points-trigger id="trigger id"]text[/bonus-points-trigger]'
        );
    }

    /**
     * After module has been installed
     * @since 1.2.0
     * @internal
     */
    public function onInstall()
    {
        require_once __DIR__ . '/install.php';

        bpInstallLoyaltySuite();
    }

    /**
     * After module activation
     * @since 1.2.0
     * @internal
     */
    public function afterActivate()
    {
        $this->addCapsToRole(['ls_bp'], 'contributor');
        $this->addCapsToRole(['ls_bp'], 'author');
        $this->addCapsToRole(['ls_bp'], 'editor');
        $this->addCapsToRole(['ls_bp', 'ls_bp_settings'], 'administrator');
    }

    /**
     * On uninstall module
     * @since 1.2.0
     * @internal
     */
    public function onUninstall()
    {
        $this->helper->deleteTables();
    }

    /**
     * Load module scripts
     * @since 1.2.0
     * @internal
     */
    public function printScripts()
    {
        wp_enqueue_style('ls-bp-admin', $this->getUrl2Module() . 'css/admin-bp.css', [], $this->getVersion());

        if ($this->isCurrentPluginPage()) {
            wp_enqueue_script('ls-bp-admin', $this->getUrl2Module() . 'js/admin-bp.js', ['jquery'], $this->getVersion(), true);
            wp_enqueue_script('ls-bp-admin-consumer', $this->getUrl2Module() . 'js/admin-consumer.js', ['jquery'], $this->getVersion(), true);
            wp_localize_script('ls', 'lsBpStrings', [
                'noIdentify' => __('One of the columns in the import file must be as identifier field', 'ls'),
                'noPoints'   => __('One of the columns in the import file must be as points field', 'ls')
            ]);
        }
    }

    /**
     * Fill the trigger structure with the data from trigger model
     *
     * @since 1.4.0
     *
     * @param FieldAbstract[] $triggerStructure Trigger structure
     * @param BPTriggerModel $triggerModel Trigger model
     */
    private function fillTriggerStructureWithModel($triggerStructure, BPTriggerModel $triggerModel)
    {
        /** @var FieldAbstract $field */
        foreach ($triggerStructure as $fieldName => $field) {

            if ($triggerModel->getTriggerId() < 1 && in_array($field->getName(), ['triggerId', 'groupId', 'createDate'])) {
                continue;
            }

            if ($fieldName == 'button') {
                if ($triggerModel->getTriggerId() > 0) {
                    $field->setValue('[bonus-points-trigger id=' . $triggerModel->getTriggerId() . ']Click me[/bonus-points-trigger]');
                } else {
                    $field->setValue(__('Save trigger to see the shortcode', 'ls'));
                }
            } elseif ($fieldName == $triggerModel->getTriggerName()) {
                $field->setValue($triggerModel->getTriggerIdent());
            } elseif ($triggerModel->get($fieldName) !== null) {
                $field->setValue($triggerModel->get($fieldName));
            }
        }
    }

    /**
     * Render administration menu
     * @since 1.2.0
     * @internal
     */
    public function moduleAdministrationPage()
    {
        include_once __DIR__ . '/tables/BPTransactionsTable.php';
        include_once __DIR__ . '/tables/BPImportLogTable.php';
        include_once __DIR__ . '/tables/BPTriggersTable.php';

        $template = new Template(__DIR__ . '/views/backend');
        $template
            ->assign('options', $this->getOptions())
            ->assign('pointsTable', new BPTransactionsTable())
            ->assign('importLogTable', new BPImportLogTable())
            ->assign('earningMethods', $this->getEarningMethods())
            ->assign('burningMethods', $this->getBurningMethods())
            ->assign('hasTransactions', $this->helper->hasTransactions())
            ->assign('tabs', [
                ['name' => 'ls-bonus', 'title' => __('Bonus transactions', 'ls'), 'view' => 'admin-transactions.phtml'],
                ['name' => 'ls-earnings', 'title' => __('Earning', 'ls'), 'view' => 'admin-earnings.phtml', 'capability' => 'ls_bp_settings'],
                ['name' => 'ls-log', 'title' => 'Log', 'view' => 'admin-log.phtml'],
                ['name' => 'ls-settings', 'title' => __('Settings', 'ls'), 'view' => 'admin-settings.phtml', 'capability' => 'ls_bp_settings']
            ]);

        // bonus trigger
        if (isset($_REQUEST['view']) && $_REQUEST['view'] == 'bonus-trigger') {

            $triggerModelId = $this->getParam('triggerId', 'id');

            if ($triggerModelId > 0) {
                $triggerModel = $this->helper->getTriggerModel($triggerModelId);
                $title = __('Edit bonus trigger', 'ls');
            } else {
                $triggerModel = new BPTriggerModel();
                $title = __('New bonus trigger', 'ls');
            }

            if ($triggerModel) {

                if ($this->isAction('bp-trigger-save')) {
                    $triggerModel->setFromArray(stripslashes_deep($_POST));
                    $idents = $this->getParam('triggerIdent', 'array');
                    $triggerModel->set('triggerIdent', $idents[$triggerModel->getTriggerName()] ?? '');
                }

                $registeredTriggers = $this->getRegisteredTriggers();
                $triggersSettings = [];

                foreach ($registeredTriggers as $regTrigger) {
                    $triggersSettings[$regTrigger->getName()] = $regTrigger->getSettings();
                }

                $bonusTriggerStructure = $this->getBonusTriggerStructure($registeredTriggers);

                $this->fillTriggerStructureWithModel($bonusTriggerStructure, $triggerModel);

                $template
                    ->assign('triggerId', (int) $triggerModelId)
                    ->assign('triggerStructure', $bonusTriggerStructure)
                    ->assign('triggersSettings', $triggersSettings)
                    ->assign('page', ['name' => 'ls-bonus-trigger', 'title' => $title, 'view' => 'admin-bonus-trigger.phtml']);

            } else {
                $template->assign('page', ['name' => 'ls-bonus-trigger', 'title' => $title, 'content' => __('Can not find Access Code', 'ls')]);
            }
        }

        // save settings
        if ($this->isAction('bp-save-settings') && isset($_REQUEST['submit'])) {
            $options = $this->getOptions();
            $options = array_merge($options, stripslashes_deep($_POST));
            $template->assign('options', $options);
        }

        $template->renderAdminPage();
    }

    /**
     * @since 1.2.0
     * @internal
     */
    public function adminInit()
    {
        if (!$this->isCurrentPluginPage() || !$this->hasAction()) {
            return;
        }

        if ($this->isAction('bp-clear-all') && current_user_can('ls_bp')) {

            $consumersIds = $this->helper->getTransactionsConsumersIds();
            $result = [
                'errors'   => [],
                'complete' => []
            ];

            foreach ($consumersIds as $consumerId) {
                if ($this->helper->deleteConsumerTransactions($consumerId)) {
                    $this->updateConsumerBalance($consumerId);
                    $result['complete'][] = $consumerId;
                } else {
                    $result['errors'][] = $consumerId;
                }
            }

            if (empty($result['errors'])) {
                $this->setAlert(sprintf(
                    __('Bonus points for %s consumers have been removed', 'ls'),
                    '<b>' . count($result['complete']) . '</b>'
                ));
            } elseif (empty($result['complete'])) {
                $this->setError(__('Can not reset consumers balance', 'ls'));
            } else {
                $this->setError(sprintf(
                    __('Can not reset balance for %s consumers', 'ls'),
                    '<b>' . count($result['errors']) . '</b>'
                ));
                $this->setAlert(sprintf(
                    __('Bonus points have been reset for %s consumers', 'ls'),
                    '<b>' . count($result['complete']) . '</b>'
                ));
            }

            LS()->logUserAction('Bonus points: deleted all transactions');
            $this->redirect(remove_query_arg('action'));
        }

        if ($this->isAction('reset-bonus-points') && current_user_can('ls_bp')) {

            $consumerId = $this->getParam('consumerId', 'id');

            if ($this->helper->deleteConsumerTransactions($consumerId)) {

                $this->updateConsumerBalance($consumerId);

                ConsumersHelper::insertLog('reset_bonus_points', $consumerId, '', time());
                LS()->logUserAction('Bonus points: reset points for consumer ' . $consumerId);

                $this->setAlert(__('Bonus points have been reset', 'ls'));

            } else {
                $this->setError(__('Can not reset consumer balance'));
            }

            $this->redirect($this->getReferer());
        }

        if ($this->isAction('bp-save-settings') && isset($_REQUEST['submit']) && current_user_can('ls_bp_settings')) {
            if ($this->saveSettings()) {
                $this->setAlert(__('Settings have been saved', 'ls'));
                LS()->logUserAction('Bonus points: settings saved');
                $this->redirect(remove_query_arg('action'));
            }
        }

        if ($this->isAction('bp-import-check-file') && current_user_can('ls_bp_settings') && $this->isEarningMethodActivated(BPTransaction::SOURCE_IMPORT)) {

            if (!isset($_FILES['file']) || empty($_FILES['file']['name']) || $_FILES['file']['error'] > 0) {
                $this->setError(__('Please select file to import', 'ls'));
            } else {

                $ext = $this->helper->getExt($_FILES['file']['name']);
                $currHandler = $this->getImportHandler($_FILES['file']['tmp_name'], $ext);

                if (!$currHandler || empty($ext)) {
                    $this->setError(__('Unknown file type', 'ls'));
                } elseif (($error = $currHandler->validate()) !== '') {
                    $this->setError($error);
                } else {
                    $columns = $currHandler->getColumns();
                    if (empty($columns)) {
                        $this->setError(__('Invalid file content or invalid columns structure', 'ls'));
                    } elseif (!$importId = $this->helper->startImport($_FILES['file']['name'], $_FILES['file']['tmp_name'])) {
                        $this->setError(__('System can not process file', 'ls'));
                    } else {
                        $this->redirect(add_query_arg(['view' => 'bp-import-structure', 'importId' => $importId], remove_query_arg('action')));
                    }
                }
            }

            if ($this->hasErrors()) {
                $this->redirect(add_query_arg('view', 'bp-import', remove_query_arg('action')));
            }
        }

        if ($this->isAction('bp-import-start') && current_user_can('ls_bp_settings') && $this->isEarningMethodActivated(BPTransaction::SOURCE_IMPORT)) {

            $import = $this->importFile($this->getParam('importId', 'id'));
            if ($import) {
                $this->setAlert(__('File has been imported. For detailed information please check Import Log.', 'ls'));
                LS()->logUserAction(sprintf('Bonus points: file imported (ID: %d)', $import->getImportId()));
            } else {
                $this->setError(__('Can not import file', 'ls'));
            }

            $this->redirect(remove_query_arg(['action', 'view', 'importId']));
        }

        if ($this->isAction('bp-get-file') && current_user_can('ls_bp')) {

            $path = $fileName = '';
            if ($this->getParam('a') == 'import') {
                $importId = $this->getParam('importId', 'id');
                $import = $this->helper->getImport($importId);
                if ($import) {
                    $path = $this->helper->getPathToFile($import->getFileName());
                    $fileName = $import->getImportFileName();
                }
            }

            if (!empty($fileName) && file_exists($path)) {
                header("Content-type: text/csv");
                header("Cache-Control: no-store, no-cache");
                header('Content-Disposition: attachment; filename="' . $fileName . '"');
                $fp = fopen("php://output", 'w');
                fprintf($fp, chr(0xEF) . chr(0xBB) . chr(0xBF)); // UTF-8
                fwrite($fp, file_get_contents($path));
                fclose($fp);
            } else {
                echo __('File not exists', 'ls');
            }

            exit;
        }

        if ($this->isAction('bp-get-test-file') && current_user_can('ls_bp')) {

            $type = $this->getParam('type', 'text', 'REQUEST', 'csv');

            if ($type == 'csv') {
                $path = __DIR__ . '/test/test.' . $type;
                $contentType = 'text/csv';
            }

            if (isset($path, $contentType) && file_exists($path)) {
                header("Content-type: " . $contentType);
                header("Cache-Control: no-store, no-cache");
                header('Content-Disposition: attachment; filename="' . basename($path) . '"');
                $fp = fopen("php://output", 'w');
                fprintf($fp, chr(0xEF) . chr(0xBB) . chr(0xBF)); // UTF-8
                fwrite($fp, file_get_contents($path));
                fclose($fp);
            } else {
                echo __('File not exists', 'ls');
            }

            exit;
        }

        if ($this->isAction('bp-manual-add') && current_user_can('ls_bp_settings') && $this->isEarningMethodActivated(BPTransaction::SOURCE_MANUAL)) {

            $data = [
                'consumerId' => $this->getParam('consumerId', 'id'),
                'points'     => $this->getParam('points', 'value'),
                'message'    => $this->getParam('message', 'text'),
                'createDate' => $this->getParam('createDate', 'datetime')
            ];

            foreach ($this->getManualPointsStructure() as $fieldName => $field) {
                if (isset($data[$fieldName])) {
                    $error = $field->setValue($data[$fieldName])->validate();
                    if (!empty($error)) {
                        $this->setError($error);
                    } else {
                        $data[$fieldName] = $field->formatToDatabase();
                    }
                }
            }

            if (!$this->hasErrors()) {

                if ($transactionId = $this->createTransaction(null, $data['consumerId'], $data['points'], $data['message'], $data['createDate'], BPTransaction::SOURCE_MANUAL)) {
                    $this->setAlert(__('Transaction has been added', 'ls'));
                    LS()->logUserAction(sprintf('Bonus points: manual transaction created (ID: %d)', $transactionId));
                    $this->redirect(remove_query_arg('action'));
                }

                $this->setError(__('Can not add transaction', 'ls'));
            }
        }

        if ($this->isAction('bp-trigger-save') && current_user_can('ls_bp_settings')) {

            $triggerModelId = $this->getParam('triggerId', 'id');
            $data = [
                'triggerIdent' => $this->getParam('triggerIdent', 'array')
            ];

            foreach ($this->getBonusTriggerStructure($this->getRegisteredTriggers()) as $fieldName => $field) {

                if (in_array($fieldName, ['triggerId', 'createDate'])) {
                    continue;
                }

                $data[$fieldName] = $this->getParam($fieldName, $field->getType());

                if (isset($data[$fieldName])) {
                    $error = $field->setValue($data[$fieldName])->validate();
                    if (!empty($error)) {
                        $this->setError($error);
                    } else {
                        $data[$fieldName] = $field->formatToDatabase();
                    }
                }
            }

            $data['triggerIdent'] = $data['triggerIdent'][$data['trigger']] ?? '';

            if ($data['startDate'] > 0 && $data['endDate'] > 0 && $data['startDate'] > $data['endDate']) {
                $this->setError(__('End date must not be before the start date', 'ls'));
            }

            if (!$this->hasErrors()) {
                if ($triggerModelId > 0) {

                    $triggerModel = $this->helper->getTriggerModel($triggerModelId);

                    if ($triggerModel) {

                        $triggerModel->setFromArray($data);

                        if ($this->helper->saveTriggerModel($triggerModel)) {
                            $this->setAlert(__('Bonus trigger has been updated', 'ls'));
                            LS()->logUserAction(sprintf('Bonus points: trigger updated (ID: %d)', $triggerModelId));
                            $this->redirect(remove_query_arg('action'));
                        }

                        $this->setError(__('Can not update bonus trigger', 'ls'));
                    } else {
                        $this->setError(__('Can not update bonus trigger', 'ls'));
                    }
                } else {

                    $triggerModel = new BPTriggerModel($data);

                    if ($this->helper->saveTriggerModel($triggerModel)) {
                        $this->setAlert(__('Bonus trigger has been created', 'ls'));
                        LS()->logUserAction(sprintf('Bonus points: trigger created (ID: %d)', $triggerModel->getTriggerId()));
                        $this->redirect($this->getReferer());
                    }

                    $this->setError(__('Can not add bonus trigger', 'ls'));
                }
            }
        }

        if ($this->isAction('delete-bonus-trigger') && isset($_REQUEST['triggerId']) && current_user_can('ls_bp_settings')) {

            $ids = $this->getParam('triggerId', 'int_array');
            $deletedTriggers = 0;

            foreach ($ids as $triggerModelId) {
                $triggerModel = $this->helper->getTriggerModel($triggerModelId);
                if ($triggerModel) {
                    if (!$this->helper->deleteTriggerModel($triggerModelId)) {
                        $this->setError(sprintf(__('Can not delete trigger "%s"', 'ls'), $triggerModel->getName()));
                    } else {
                        $deletedTriggers++;
                    }
                }
            }

            if (!$this->hasErrors()) {
                $this->setAlert(_n('1 trigger has been deleted', '%d triggers have been deleted', $deletedTriggers, 'ls'));
                LS()->logUserAction(sprintf('Bonus points: trigger(s) deleted (ID: %s)', implode(',', $ids)));
            }

            $this->redirect(remove_query_arg(['action', 'action2', 'triggerId']));
        }

        if ($this->isAction('delete-points') && current_user_can('ls_bp')) {

            $ids = $this->getParam('transactionId', 'int_array');
            $remTransactions = 0;

            foreach ($ids as $transactionId) {
                if (!$this->helper->deleteTransaction($transactionId)) {
                    $this->setError(sprintf(__('Can not delete transaction #%d', 'ls'), $transactionId));
                } else {
                    $remTransactions++;
                }
            }

            if (!$this->hasErrors()) {
                $this->setAlert(_n('1 transaction has been deleted', '%d transactions have been deleted', $remTransactions, 'ls'));
                LS()->logUserAction(sprintf('Bonus points: transaction(s) deleted (ID: %s)', implode(',', $ids)));
            }

            $this->redirect(remove_query_arg(['action', 'action2', 'transactionId']));
        }
    }

    /**
     * Get dashboard
     *
     * @ajax
     * @since 1.2.0
     * @internal
     */
    public function getDashboard()
    {
        $date = $this->getParam('date', 'date', 'REQUEST');
        $date = empty($date) ? current_time('timestamp') : $date;

        (new Template(__DIR__ . '/views/backend'))
            ->assign('dashboard', $this->helper->getDashboard($date))
            ->render('admin-dashboard.phtml');

        exit;
    }

    /**
     * Get dashboard
     *
     * @ajax
     * @since 1.2.0
     * @internal
     */
    public function getQuickHistory()
    {
        $date = $this->getParam('date', 'date', 'REQUEST');
        $date = empty($date) ? current_time('timestamp') : $date;

        (new Template(__DIR__ . '/views/backend'))
            ->assign('dashboard', $this->helper->getQuickHistory($date))
            ->render('admin-quick-history.phtml');

        exit;
    }

    /**
     * Create tab with the information about consumer bonus points
     *
     * @since 1.2.0
     * @internal
     *
     * @param array $tabs A list of tabs on the consumer details page
     * @param int $consumerId Consumer ID
     * @return array A list of tabs on the consumer details page
     */
    public function consumerDetailsFilter(array $tabs, int $consumerId): array
    {
        if ($consumerId > 0) {

            require_once __DIR__ . '/tables/BPConsumerTransactions.php';

            wp_enqueue_script('ls-bp-admin-consumer', $this->getUrl2Module() . 'js/admin-consumer.js', ['jquery'], $this->getVersion(), true);

            $content = (new Template(__DIR__ . '/views/backend'))
                ->assign('consumerId', $consumerId)
                ->assign('table', new BPConsumerTransactions($consumerId))
                ->assign('totalPoints', $this->helper->calculateConsumerBalance($consumerId))
                ->render('admin-consumer-points.phtml', false);

            $tabs['bp'] = [__('Bonus Points', 'ls'), $content];
        }

        return $tabs;
    }

    /**
     * Save module settings
     * @since 1.2.0
     * @return bool Returns true if settings have been saved
     */
    private function saveSettings(): bool
    {
        $options = [
            'ptSingular'           => $this->getParam('ptSingular', 'text'),
            'ptPlural'             => $this->getParam('ptPlural', 'text'),
            'ptCurrency'           => $this->getParam('ptCurrency', 'float'),
            'activeEarningMethods' => array_keys($this->getParam('earningMethods', 'array')),
            'activeBurningMethods' => array_keys($this->getParam('burningMethods', 'array')),
            'agingEnabled'         => $this->getParam('agingEnabled', 'checkbox'),
            'agingDays'            => $this->getParam('agingDays', 'int'),
            'agingMessage'         => $this->getParam('agingMessage', 'text'),
            'statusEnabled'        => $this->getParam('statusEnabled', 'checkbox'),
            'statusPoints'         => $this->getParam('statusPoints', 'int'),
            'statusLifetime'       => $this->getParam('statusLifetime', 'int')
        ];

        do_action('ls_bp_settings_updated', $options);

        if (!$this->hasErrors()) {

            $this->setOptions(
                array_merge($this->getOptions(), $options)
            );

            return true;
        }

        return false;
    }

    /**
     * Retrieve a list of possible triggers types
     * @since 1.2.0
     * @return array
     */
    public function getBonusTriggerTypes(): array
    {
        $types = [];

        foreach ($this->getRegisteredTriggers() as $trigger) {
            $types[$trigger->getName()] = $trigger->getLabel();
        }

        return $types;
    }

    /**
     * Retrieve Bonus Triggers record structure
     *
     * @since 1.2.0
     *
     * @param BPTrigger[] $triggers Registered triggers
     * @return FieldAbstract[] Trigger structure
     */
    public function getBonusTriggerStructure($triggers): array
    {
        $fields = [
            'triggerId'   => new Field\Value('triggerId', 'ID'),
            'description' => new \LS\Field\Label('description'),
            'name'        => new Field\Text('name', __('Name', 'ls'), ['maxlength' => 255, 'required' => true]),
            'trigger'     => new Field\Select('trigger', __('Trigger', 'ls'), $this->getBonusTriggerTypes(), ['required' => true])
        ];

        foreach ($triggers as $trigger) {
            $field = $trigger->getIdentifyField();
            if ($field !== null) {
                $fields[$trigger->getName()] = $field;
            }
        }

        $fields = array_merge($fields, [
            'points'        => new Field\Value('points', __('Points', 'ls')),
            'message'       => new Field\Textarea('message', __('Transaction details', 'ls'), ['required' => true, 'maxlength' => 200]),
            'companyId'     => new Field\Select('companyId', __('Company', 'ls'), \LSCompaniesHelper::getCompaniesList(), ['emptyText' => __('Any', 'ls')]),
            'consumerAlert' => new Field\Textarea('consumerAlert', __('Message to consumer on perform', 'ls')),
            'startDate'     => new Field\Date('startDate', __('Start date', 'ls')),
            'endDate'       => new Field\Date('endDate', __('End date', 'ls')),
            'active'        => new Field\Checkbox('active', __('Active', 'ls'))
        ]);

        return $fields;
    }

    /**
     * Retrieve manual points transaction structure
     * @since 1.2.0
     * @return FieldAbstract[]
     */
    public function getManualPointsStructure(): array
    {
        return [
            'consumerId' => new Field\Value('consumerId', __('Consumer Id', 'ls')),
            'points'     => new Field\Value('points', __('Points', 'ls'), ['required' => true]),
            'message'    => new Field\Textarea('message', __('Transaction details', 'ls'), ['required' => true, 'maxlength' => 200]),
            'createDate' => new Field\Datetime('createDate', __('Create date', 'ls'), [], time())
        ];
    }

    /**
     * Retrieve import handler
     *
     * @since 1.2.0
     *
     * @param string $pathToFile Path to file
     * @param string $ext File extension
     * @return false|BPImportHandler
     */
    private function getImportHandler(string $pathToFile, string $ext)
    {
        if (empty($pathToFile) || empty($ext) || !is_file($pathToFile) || !is_readable($pathToFile)) {
            return false;
        }

        require_once __DIR__ . '/BPImportHandlerCSV.php';

        $handlers = [
            new BPImportHandlerCSV()
        ];

        /** @var BPImportHandler $handler */
        foreach ($handlers as $handler) {
            if (in_array($ext, $handler->getExtensions())) {
                return $handler->read($pathToFile);
            }
        }

        return false;
    }

    /**
     * Retrieve import file columns and sample records
     *
     * @since 1.2.0
     *
     * @param int $importId Import ID
     * @return array File columns and sample records
     */
    private function getImportFile(int $importId): array
    {
        $result = [];
        $import = $this->helper->getImport($importId);

        if ($import) {

            $file = $this->helper->getPathToFile($import->getFileName());
            if ($file && file_exists($file)) {

                $handler = $this->getImportHandler($file, $import->getExt());
                if ($handler && $handler->validate() == '') {

                    $records = $handler->getRecords();

                    $result = [
                        'importId'     => $importId,
                        'columns'      => $handler->getColumns(),
                        'records'      => array_slice($records, 0, 5),
                        'totalRecords' => count($records)
                    ];
                }
            }
        }

        return $result;
    }

    /**
     * Start import process
     *
     * @since 1.2.0
     *
     * @param int $importId Import Id
     * @return false|BPImport Import object or false on failure
     */
    private function importFile(int $importId)
    {
        $imported = false;
        $import = $this->helper->getImport($importId);

        if ($import) {

            $colsAssociations = [
                'ident'            => $this->getParam('ident', 'int'),
                'identType'        => $this->getParam('identType', 'text'),
                'points'           => $this->getParam('points', 'int'),
                'message'          => $this->getParam('message', 'int'),
                'createDate'       => $this->getParam('createDate', 'int'),
                'createDateFormat' => $this->getParam('createDateFormat', 'text')
            ];

            $file = $this->helper->getPathToFile($import->getFileName());
            if ($file && file_exists($file)) {

                $handler = $this->getImportHandler($file, $import->getExt());
                if ($handler) {

                    $result = [
                        'recordsProcessed'        => 0,
                        'recordsSaved'            => 0,
                        'pointsTransferred'       => 0,
                        'associatedWithConsumers' => 0
                    ];

                    if (!empty($colsAssociations) && isset($colsAssociations['ident'], $colsAssociations['points'])) {

                        $consAsc = [];

                        foreach ($handler->getRecords() as $key => $record) {

                            $identValue = $record[$colsAssociations['ident']];
                            $identType = $colsAssociations['identType'];
                            $points = (int) $record[$colsAssociations['points']];
                            $message = isset($colsAssociations['message']) ? $record[$colsAssociations['message']] : '';
                            $createDate = isset($colsAssociations['createDate']) ? $record[$colsAssociations['createDate']] : null;
                            $createDateFormat = $colsAssociations['createDateFormat'];

                            $result['recordsProcessed']++;

                            $result['pointsTransferred'] += $points;

                            $consumerId = 0;
                            if (isset($consAsc[$identValue])) {
                                $consumerId = (int) $consAsc[$identValue];
                            } elseif ($identType == 'id') {
                                $consumer = ConsumersHelper::getConsumer((int) $identValue);
                                $consumerId = $consumer ? (int) $identValue : 0;
                            } elseif ($identType == 'login') {
                                $consumer = ConsumersHelper::getConsumerByLogin($identValue);
                                $consumerId = $consumer ? $consumer->getConsumerId() : 0;
                            } elseif ($identType == 'email') {
                                $consumer = ConsumersHelper::getConsumerByEmail($identValue);
                                $consumerId = $consumer ? $consumer->getConsumerId() : 0;
                            } elseif ($identType == 'other') {
                                $consumerId = apply_filters('ls_bp_ident_to_consumer_id', $identValue);
                            }

                            if ($consumerId > 0) {
                                $consAsc[$identValue] = $consumerId;
                                $result['associatedWithConsumers']++;
                            }

                            if ($createDate !== null) {
                                if ($createDateFormat == 'timestamp') {
                                    $createDate = (int) $createDate;
                                } elseif ($createDateFormat == 'other') {
                                    if (has_filter('ls_bp_format_create_date', $createDate)) {
                                        $createDate = apply_filters('ls_bp_format_create_date', $createDate);
                                    } else {
                                        $createDate = strtotime($createDate);
                                    }
                                } else {
                                    $createDate = \DateTime::createFromFormat($createDateFormat, $createDate)->getTimestamp();
                                }
                            } else {
                                $createDate = time();
                            }

                            if (!empty($identValue) && $this->createTransaction($identValue, $consumerId, $points, $message, $createDate, BPTransaction::SOURCE_IMPORT)) {
                                $result['recordsSaved']++;
                            }
                        }

                        foreach (array_unique(array_values($consAsc)) as $consumerId) {
                            if ($consumerId > 0) {
                                $this->updateConsumerBalance($consumerId);
                            }
                        }
                    }

                    $import->setFromArray([
                        'status'                  => BPImport::STATUS_IMPORT_FINISHED,
                        'recordsProcessed'        => $result['recordsProcessed'],
                        'recordsSaved'            => $result['recordsSaved'],
                        'pointsTransferred'       => $result['pointsTransferred'],
                        'associatedWithConsumers' => $result['associatedWithConsumers']
                    ]);

                    if ($this->helper->saveImport($import)) {
                        $imported = true;
                    }
                }
            }

            if (!$imported) {
                $this->helper->saveImport(
                    $import->setStatus(BPImport::STATUS_IMPORT_FAILED)
                );
            }
        }

        return $imported ? $import : false;
    }

    /**
     * Save columns structure for the points table
     *
     * @ajax
     * @since 1.2.0
     * @internal
     */
    public function saveColumnsStructure()
    {
        require_once __DIR__ . '/tables/BPTriggersTable.php';

        BPTransactionsTable::setColumnsStructure($this->getParam('columns', 'array'));

        exit;
    }

    /**
     * Render page to import transactions
     *
     * @since 1.2.0
     * @internal
     *
     * @return string
     */
    public function renderImportTransactions(): string
    {
        return (new Template(__DIR__ . '/views/backend'))
            ->assign('importFile', $this->getImportFile($this->getParam('importId', 'id')))
            ->render('admin-transactions-import.phtml', false);
    }

    /**
     * Render page to manually add transactions
     *
     * @since 1.4.0
     * @internal
     *
     * @return string
     */
    public function renderManualTransactions(): string
    {
        return (new Template(__DIR__ . '/views/backend'))
            ->assign('structure', $this->getManualPointsStructure())
            ->render('admin-transactions-manual.phtml', false);
    }

    /**
     * Retrieve an array of earning methods
     * @since 1.2.0
     * @return BPMethod[] Array of earning methods
     */
    public function getEarningMethods(): array
    {
        static $bpEarningMethods;

        if ($bpEarningMethods === null) {

            $methods = apply_filters('ls_bp_earning_methods', [
                new BPMethod(BPTransaction::SOURCE_TRIGGER, __('Bonus trigger', 'ls'), __DIR__ . '/views/backend/admin-transactions-bt.phtml'),
                new BPMethod(BPTransaction::SOURCE_IMPORT, __('Import via interface', 'ls'), [$this, 'renderImportTransactions']),
                new BPMethod(BPTransaction::SOURCE_MANUAL, __('Manual adding', 'ls'), [$this, 'renderManualTransactions'])
            ]);

            $activeEarningMethods = $this->getActiveEarningMethods();

            /** @var BPMethod $method */
            foreach ($methods as $method) {
                if (in_array($method->getName(), $activeEarningMethods)) {
                    $method->setActive();
                }
            }

            $bpEarningMethods = $methods;
        }

        return $bpEarningMethods;
    }

    /**
     * Retrieve an array of burning methods
     * @since 1.2.0
     * @return BPMethod[] Array of burning methods
     */
    public function getBurningMethods(): array
    {
        static $bpBurningMethods;

        if ($bpBurningMethods === null) {

            $methods = apply_filters('ls_bp_burning_methods', [
                new BPMethod(BPTransaction::SOURCE_MANUAL, __('Manual burning', 'ls'), __DIR__ . '/views/backend/admin-transactions-manual.phtml')
            ]);

            $burningActiveMethods = $this->getActiveBurningMethods();

            /** @var BPMethod $method */
            foreach ($methods as $method) {
                if (in_array($method->getName(), $burningActiveMethods)) {
                    $method->setActive();
                }
            }

            $bpBurningMethods = $methods;
        }

        return $bpBurningMethods;
    }

    /**
     * Retrieve all possible transaction sources
     * @since 1.2.0
     * @return BPMethod[] A list of transactions sources
     */
    public function getTransactionSources(): array
    {
        $sources = [];

        /** @var BPMethod $method */
        foreach (array_merge($this->getEarningMethods(), $this->getBurningMethods()) as $method) {
            $sources[$method->getName()] = $method->getLabel();
        }

        $sources[BPTransaction::SOURCE_AGING] = __('Aging', 'ls');

        return $sources;
    }

    /**
     * Retrieve triggers by AT activity Id
     * @param int $activityId Activity Id
     * @return false|BPTriggerModel
     */
    private function getTriggerModelByATActivityId(int $activityId)
    {
        $triggerModels = $this->helper->getTriggerModels('', '', [
            ['r' => 'trigger', 'c' => 'equals', 't' => 'at2activity'],
            ['r' => 'triggerIdent', 'c' => 'equals', 't' => $activityId]
        ]);

        return $triggerModels ? $triggerModels[0] : false;
    }

    /**
     * Add points field to the AT settings
     * @since 1.2.0
     * @param null|array $activity AT activity
     */
    public function atActivityFields($activity)
    {
        if ($activity) {
            if (isset($activity['bonusPoints'])) {
                $points = (int) $activity['bonusPoints'];
            } else {
                $triggerModel = $this->getTriggerModelByATActivityId((int) $activity['activityId']);
                $points = $triggerModel ? $triggerModel->getPoints() : 0;
            }
        } else {
            $points = 0;
        }
        ?>
        <fieldset>
            <legend><?=__('Points', 'ls')?></legend>
            <input type="text" class="bonus-points" data-column="bonusPoints" maxlength="5" value="<?=$points?>"/>
        </fieldset>
        <?php
    }

    /**
     * AT activity saved - update points
     * @param int $activityId Activity Id
     * @param array $activityData Activity data
     */
    public function atSaveActivity($activityId, $activityData)
    {
        if (isset($activityData['bonusPoints'])) {

            $points = (int) $activityData['bonusPoints'];
            $triggerModel = $this->getTriggerModelByATActivityId((int) $activityId);

            if ($points === 0) {
                if ($triggerModel) {
                    $this->helper->deleteTriggerModel($triggerModel->getTriggerId());
                }
            } else {

                if (!$triggerModel) {
                    $triggerModel = new BPTriggerModel([
                        'name'         => 'Activity Tracking - ' . $activityData['title'],
                        'trigger'      => 'at2activity',
                        'triggerIdent' => $activityId,
                        'message'      => 'Activity Tracking - ' . $activityData['title'],
                        'active'       => true
                    ]);
                }

                $this->helper->saveTriggerModel(
                    $triggerModel->setPoints($points)
                );
            }
        }
    }

    /**
     * Delete trigger related to the deleted AT activity
     * @since 1.2.0
     * @param int $activityId Activity Id
     */
    public function atDeleteActivity($activityId)
    {
        $triggerModel = $this->getTriggerModelByATActivityId((int) $activityId);

        if ($triggerModel) {
            $this->helper->deleteTriggerModel($triggerModel->getTriggerId());
        }
    }

    /**
     * Format message for consumer log
     *
     * @since 1.2.0
     * @internal
     *
     * @param object $log Log details
     * @return string Event text
     */
    public function consumerLog($log): string
    {
        $event = '';

        if ($log->messageType == 'bpBalance') {
            $event = sprintf(__('Balance changed to %d', 'ls'), $log->note);
        } elseif ($log->messageType == 'bp_status_silver') {
            $event = __('Status changed to "Silver"', 'ls');
        } elseif ($log->messageType == 'bp_status_silver_removed') {
            $event = __('Status "Silver" removed', 'ls');
        } elseif ($log->messageType == 'bp_trigger') {
            $event = __('Bonus Points trigger', 'ls');
            if (isset($log->note) && $log->note > 0) {
                $triggerModel = $this->helper->getTriggerModel((int) $log->note);
                if ($triggerModel) {
                    $event .= ' (' . $triggerModel->getName() . ')';
                }
            }
        } elseif ($log->messageType == 'reset_bonus_points') {
            $event = sprintf(__('Bonus Points balance reset', 'ls'));
        }

        return $event;
    }

    /**
     * Set rewards currency to Bonus Points when activation Rewards as burning method
     *
     * @since 1.2.0
     * @internal
     *
     * @param array $settings Bonus points settings
     */
    public function bpRewardsSettingsUpdated(array $settings)
    {
        if (isset($settings['activeBurningMethods']) && in_array('rewards', $settings['activeBurningMethods'])) {
            $this->setOption('currency', 'BP');
        }
    }

    /**
     * Cron Job
     * @since 1.2.4
     * @internal
     */
    public function cronJobDaily()
    {
        if ($this->lock('bp')) {

            $this->checkConsumersAging();
            $this->checkConsumersStatus();

            $this->unlock('bp');
        }
    }

    /**
     * Control aging process
     * @since 1.2.3
     */
    private function checkConsumersAging()
    {
        if (!$this->isAgingEnabled()) {
            return;
        }

        // be sure that it runs only once a day
        if (!$this->isLastAgingCheckWasToday()) {

            $cp = [];

            // get expired transactions
            /** @var BPTransaction $transaction */
            foreach ($this->helper->paginateTransactions(0, 0, '', '', '', ['aging'])['rows'] as $transaction) {

                if (!isset($cp[$transaction->getConsumerId()])) {
                    $cp[$transaction->getConsumerId()] = 0;
                }

                $cp[$transaction->getConsumerId()] -= $transaction->getPoints();
            }

            foreach ($cp as $consumerId => $points) {
                $this->createTransaction(null, $consumerId, $points, $this->getAgingMessage(), strtotime('TODAY'), BPTransaction::SOURCE_AGING);
            }

            $this->markAgingAsChecked();
        }
    }

    /**
     * Control status process
     * @since 1.2.3
     */
    private function checkConsumersStatus()
    {
        if ($this->isSilverStatusEnabled()) {

            $points = $this->getMinPointsToHaveSilverStatus();
            $lifetime = $this->getSilverStatusLifetime();

            if ($points > 0 && $lifetime > 0) {

                // get consumers that should receive silver status
                $consumers = ConsumersHelper::getConsumers([
                    ['r' => 'bpBalance', 'c' => 'great', 't' => $points - 1],
                    ['r' => 'bpStatusSilver', 'c' => 'empty']
                ]);

                $consumers = array_filter($consumers, static function (ConsumerInterface $consumer) {
                    return $consumer->active();
                });

                // give silver status
                foreach ($consumers as $consumer) {

                    ConsumersHelper::updateConsumer(
                        $consumer->addDynamicValue('bpStatusSilver', 1)
                    );

                    ConsumersHelper::insertLog('bp_status_silver', $consumer->getConsumerId());
                }

                // get consumers that should lose silver status
                $consumers = ConsumersHelper::getConsumers([
                    ['r' => 'bpBalance', 'c' => 'less', 't' => $points],
                    ['r' => 'bpStatusSilver', 'c' => 'equals', 't' => 1],
                    ['r' => 'bpStatusSilverOver', 'c' => 'empty']
                ]);

                // mark today as the last day with silver status
                foreach ($consumers as $consumer) {
                    ConsumersHelper::updateConsumer(
                        $consumer->addDynamicValue('bpStatusSilverOver', strtotime('TODAY'))
                    );
                }

                // get consumers which have a "end mark" but should keep the silver status because of points value
                $consumersIds = ConsumersHelper::getConsumersIds(true, false, false, [
                    ['r' => 'bpBalance', 'c' => 'great', 't' => $points - 1],
                    ['r' => 'bpStatusSilverOver', 'c' => 'notEmpty']
                ]);

                // remove "end mark"
                foreach ($consumersIds as $consumerId) {
                    ConsumersHelper::deleteDynamicFieldValue('bpStatusSilverOver', $consumerId);
                }

                // get consumers to remove silver status
                $consumersIds = ConsumersHelper::getConsumersIds(true, false, false, [
                    ['r' => 'bpStatusSilverOver', 'c' => 'beforeDate', 't' => date('Y-m-d', strtotime('TODAY') - $lifetime * DAY_IN_SECONDS)]
                ]);

                // remove silver status
                foreach ($consumersIds as $consumerId) {
                    ConsumersHelper::deleteDynamicFieldValue('bpStatusSilver', $consumerId);
                    ConsumersHelper::deleteDynamicFieldValue('bpStatusSilverOver', $consumerId);
                    ConsumersHelper::insertLog('bp_status_silver_removed', $consumerId);
                }
            }
        }
    }
}