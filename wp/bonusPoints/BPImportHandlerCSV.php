<?php
namespace LS;

class BPImportHandlerCSV extends BPImportHandler
{
    /** @var string Handler name */
    protected $name = 'CSV';

    /** @var string[] Handler possible file names */
    protected $extensions = ['csv'];

    /** @var string[] List of possible CSV delimiters */
    private $possibleDelimiters = [';', ',', '\t', '|'];

    /** @var string Current delimiter */
    private $delimiter;

    /**
     * @return string[]
     */
    protected function __getColumns(): array
    {
        $columns = [];

        if (is_file($this->pathToFile)) {

            $fh = fopen($this->pathToFile, 'r');

            if ($fh) {

                $keys = '';

                for ($i = 0; $i < 3; $i++) {
                    $keys = fread($fh, 4096);
                    if (!empty($keys)) {

                        // remove BOM
                        $bom = pack('H*', 'EFBBBF');
                        $keys = preg_replace("/^$bom/", '', $keys);

                        $keys = explode(PHP_EOL, $keys)[0];
                        break;
                    }
                }

                if (!empty($keys)) {
                    foreach ($this->possibleDelimiters as $del) {
                        $parts = str_getcsv($keys, $del);
                        if (count($parts) > 1) {
                            $columns = array_filter(array_map('trim', $parts));
                            $this->delimiter = $del;
                            break;
                        }
                    }
                }

                fclose($fh);
            }
        }

        return $columns;
    }

    /**
     * @return string[]
     */
    protected function __getRecords(): array
    {
        $records = [];
        $columns = $this->getColumns();

        if (!empty($columns) && !empty($this->delimiter)) {
            $csv = file($this->pathToFile, FILE_SKIP_EMPTY_LINES);
            foreach ($csv as $k => $record) {
                if ($k > 0 && !empty($record)) {
                    $records[] = str_getcsv($record, $this->delimiter);
                }
            }
        }

        return $records;
    }
}