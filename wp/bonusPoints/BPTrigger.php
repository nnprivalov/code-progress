<?php
namespace LS;

/**
 * Class BPTrigger
 * @since 1.4.0
 * @package LS
 */
abstract class BPTrigger implements BPTriggerInterface
{
    /** @var BPTriggerModel */
    protected $triggerModel;

    /**
     * BPTrigger constructor.
     * @param null|BPTrigger $triggerModel
     */
    public function __construct($triggerModel = null)
    {
        if (!($triggerModel instanceof BPTriggerModel)) {
            $triggerModel = new BPTriggerModel();
            // @TODO Update usages after 09.2019
            // @TODO Uncomment after 10.2019
            //_doing_it_wrong(__CLASS__, 'BPTrigger should receive BPTriggerModel', '1.5.0');
        }

        $this->triggerModel = $triggerModel;
    }

    /**
     * Retrieve trigger model
     * @since 1.5.0
     * @return BPTriggerModel Trigger model
     */
    public function getModel()
    {
        return $this->triggerModel;
    }

    /**
     * Retrieve identify field
     * @since 1.4.0
     * @return null|FieldAbstract
     */
    public function getIdentifyField()
    {
        return null;
    }

    /**
     * Retrieve name of identify field
     * @since 1.4.0
     * @return string Identify field name
     */
    final public function getIdentifyFieldName(): string
    {
        return 'triggerIdent[' . $this->getName() . ']';
    }

    /**
     * Retrieve trigger settings
     * @since 1.4.0
     * @return array Settings (name => value)
     */
    final public function getSettings(): array
    {
        return [
            'disablePoints' => $this->disablePoints(),
            'disableAlert'  => $this->disableAlert()
        ];
    }

    /**
     * Check if points field must be disabled
     * @return bool
     */
    public function disablePoints(): bool
    {
        return false;
    }

    /**
     * Check if alert field must be disabled
     * @return bool
     */
    public function disableAlert(): bool
    {
        return false;
    }

    /**
     * Check if model is actual
     * @since 1.5.0
     * @return bool True if trigger model is actual
     */
    public function isActual(): bool
    {
        if (!$this->triggerModel->isActive()) {
            return false;
        }

        if ($this->triggerModel->getStartDate() > 0 && $this->triggerModel->getStartDate() > time()) {
            return false;
        }

        if ($this->triggerModel->getEndDate() > 0 && time() > $this->triggerModel->getEndDate() + DAY_IN_SECONDS - 1) {
            return false;
        }

        return true;
    }

    /**
     * Check if provided ident is valid for the trigger
     *
     * @since 1.5.0
     *
     * @param int $actualTriggerValue Ident provided from a system (consumer data, etc.)
     * @return bool True if provided ident is valid for the trigger
     */
    public function isIdentValueValid(int $actualTriggerValue = 0): bool
    {
        if (empty($this->triggerModel->getTriggerIdent())) {
            return true;
        }

        return $this->triggerModel->getTriggerIdent() === $actualTriggerValue;
    }
}