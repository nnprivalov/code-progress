<?php
namespace LS\BonusPoints;

class TDWConsumerAccept extends \LS\BPTrigger
{
    /**
     * Retrieve trigger name
     * @since 1.4.0
     * @return string Trigger name
     */
    public function getName(): string
    {
        return 'TDWConsumerAccept';
    }

    /**
     * Retrieve trigger label
     * @since 1.4.0
     * @return string
     */
    public function getLabel(): string
    {
        return __('Tip der woche - consumer accept', 'ls');
    }
}