<?php
namespace LS\BonusPoints;

class ConsumersVisits extends \LS\BPTrigger
{
    /**
     * Retrieve trigger name
     * @since 1.4.0
     * @return string Trigger name
     */
    public function getName(): string
    {
        return 'consumersVisits';
    }

    /**
     * Retrieve trigger label
     * @since 1.4.0
     * @return string
     */
    public function getLabel(): string
    {
        return __('Daily consumer visits', 'ls');
    }

    /**
     * @since 1.4.0
     * @return \LS\Field\Value
     */
    public function getIdentifyField()
    {
        return new \LS\Field\Value(
            $this->getIdentifyFieldName(),
            __('Days', 'ls'),
            ['class' => 'trigger-ident'],
            3
        );
    }

    /**
     * Check if trigger can be performed corresponding to provided identify
     *
     * @since 1.5.0
     *
     * @param int $actualTriggerValue Ident provided from a system (consumer data, etc.)
     * @return bool True if trigger can be performed
     */
    public function isIdentValueValid(int $actualTriggerValue = 0): bool
    {
        return $actualTriggerValue >= $this->triggerModel->getTriggerIdent();
    }
}