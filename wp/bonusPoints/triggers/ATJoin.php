<?php
namespace LS\BonusPoints;

class ATJoin extends \LS\BPTrigger
{
    /** @var null|array */
    private static $challengesList;

    /**
     * Retrieve trigger name
     * @since 1.4.0
     * @return string Trigger name
     */
    public function getName(): string
    {
        return 'at2';
    }

    /**
     * Retrieve trigger label
     * @since 1.4.0
     * @return string
     */
    public function getLabel(): string
    {
        return __('Activity Tracking - join challenge', 'ls');
    }

    /**
     * Retrieve a list of challenges
     * @since 1.4.0
     * @return array Challenges (id => label)
     */
    private function getChallengesList(): array
    {
        if (self::$challengesList === null) {
            self::$challengesList = \AT2Model::getChallengesList();
        }

        return self::$challengesList;
    }

    /**
     * @since 1.4.0
     * @return \LS\Field\Select
     */
    public function getIdentifyField()
    {
        return new \LS\Field\Select(
            $this->getIdentifyFieldName(),
            __('Activity Tracking', 'ls'),
            $this->getChallengesList(),
            [
                'emptyText' => __('Any', 'ls'),
                'class'     => 'trigger-ident'
            ]
        );
    }
}