<?php
namespace LS\BonusPoints;

class ATActivity extends \LS\BPTrigger
{
    /** @var null|array */
    private static $activitiesList;

    /**
     * Retrieve trigger name
     * @since 1.4.0
     * @return string Trigger name
     */
    public function getName(): string
    {
        return 'at2activity';
    }

    /**
     * Retrieve trigger label
     * @since 1.4.0
     * @return string
     */
    public function getLabel(): string
    {
        return __('Activity Tracking - perform activity', 'ls');
    }

    /**
     * Retrieve a list of activities
     * @since 1.4.0
     * @return array Activities (id => label)
     */
    private function getActivitiesList(): array
    {
        if (self::$activitiesList === null) {

            self::$activitiesList = [];

            foreach (\AT2Model::getChallengesList() as $challengeId => $challengeTitle) {
                foreach (\AT2Model::getChallengeActivities($challengeId) as $activity) {
                    self::$activitiesList[$activity['activityId']] = $challengeTitle . ' - ' . $activity['title'];
                }
            }
        }

        return self::$activitiesList;
    }

    /**
     * @return \LS\Field\Select
     */
    public function getIdentifyField()
    {
        return new \LS\Field\Select(
            $this->getIdentifyFieldName(),
            __('Activity Tracking', 'ls'),
            $this->getActivitiesList(),
            [
                'showEmpty' => false,
                'class'     => 'trigger-ident'
            ]
        );
    }
}