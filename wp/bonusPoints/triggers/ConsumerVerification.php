<?php
namespace LS\BonusPoints;

class ConsumerVerification extends \LS\BPTrigger
{
    /**
     * Retrieve trigger name
     * @since 1.4.0
     * @return string Trigger name
     */
    public function getName(): string
    {
        return 'consumerVerification';
    }

    /**
     * Retrieve trigger label
     * @since 1.4.0
     * @return string
     */
    public function getLabel(): string
    {
        return __('Consumer verified account', 'ls');
    }
}