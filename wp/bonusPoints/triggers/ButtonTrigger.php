<?php
namespace LS\BonusPoints;

class ButtonTrigger extends \LS\BPTrigger
{
    /**
     * Retrieve trigger name
     * @since 1.4.0
     * @return string Trigger name
     */
    public function getName(): string
    {
        return 'button';
    }

    /**
     * Retrieve trigger label
     * @since 1.4.0
     * @return string
     */
    public function getLabel(): string
    {
        return __('Trigger the button', 'ls') . ' [DEPRECATED]';
    }

    /**
     * @since 1.4.0
     * @return \LS\Field\Label
     */
    public function getIdentifyField()
    {
        return new \LS\Field\Label('Shortcode', ['class' => 'trigger-ident']);
    }
}