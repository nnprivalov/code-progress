<?php
namespace LS\BonusPoints;

class GFSubmit extends \LS\BPTrigger
{
    /** @var null|array */
    private static $formsList;

    /**
     * Retrieve trigger name
     * @since 1.4.0
     * @return string Trigger name
     */
    public function getName(): string
    {
        return 'GFSubmit';
    }

    /**
     * Retrieve trigger label
     * @since 1.4.0
     * @return string
     */
    public function getLabel(): string
    {
        return __('Submit Gravity Forms form', 'ls');
    }

    /**
     * Retrieve a list of Gravity Forms
     * @since 1.4.0
     * @return array List of forms (id => label)
     */
    private function getFormsList(): array
    {
        if (self::$formsList === null) {

            self::$formsList = [];

            foreach (\RGFormsModel::get_forms(true) as $form) {
                self::$formsList[$form->id] = $form->title;
            }
        }

        return self::$formsList;
    }

    /**
     * @since 1.4.0
     * @return \LS\Field\Select
     */
    public function getIdentifyField()
    {
        return new \LS\Field\Select(
            $this->getIdentifyFieldName(),
            __('Gravity Form', 'ls'),
            $this->getFormsList(),
            [
                'emptyText' => __('Any', 'ls'),
                'class'     => 'trigger-ident'
            ]
        );
    }
}