<?php
namespace LS\BonusPoints;

class ConsumersVisitsInPeriod extends ConsumersVisits
{
    /**
     * Retrieve trigger name
     * @since 1.5.0
     * @return string Trigger name
     */
    public function getName(): string
    {
        return 'consumersVisitsInPeriod';
    }

    /**
     * Retrieve trigger label
     * @since 1.5.0
     * @return string
     */
    public function getLabel(): string
    {
        return __('Daily consumer visits between start and end dates', 'ls');
    }
}