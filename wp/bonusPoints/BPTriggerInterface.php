<?php
namespace LS;

/**
 * Interface BPTriggerInterface
 * @since 1.4.0
 * @package LS
 */
interface BPTriggerInterface
{
    /**
     * Retrieve trigger name
     * @since 1.4.0
     * @return string Trigger name
     */
    public function getName(): string;

    /**
     * Retrieve trigger label
     * @since 1.4.0
     * @return string Trigger label
     */
    public function getLabel(): string;

    /**
     * Retrieve identify field
     * @since 1.4.0
     * @return null|FieldAbstract
     */
    public function getIdentifyField();

    /**
     * Retrieve trigger settings
     * @since 1.4.0
     * @return array Settings (name => value)
     */
    public function getSettings(): array;

    /**
     * Check if provided ident is valid for the trigger
     *
     * @since 1.5.0
     *
     * @param int $actualTriggerValue Ident provided from a system (consumer data, etc.)
     * @return bool True if provided ident is valid for the trigger
     */
    public function isIdentValueValid(int $actualTriggerValue = 0): bool;
}