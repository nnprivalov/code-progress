<?php

require_once __DIR__ . '/bonusPointsTestCase.php';

class bonusPointsLoyaltySuiteTestHelper extends bonusPointsTestCase
{
    public function testSaveTriggerModel()
    {
        $triggerModel = $this->saveTriggerModel([
            'name'      => 'test',
            'trigger'   => 'unknown',
            'startDate' => strtotime('YESTERDAY'),
            'endDate'   => strtotime('TOMORROW')
        ]);

        $triggerModelSame = $this->helper()->getTriggerModel($triggerModel->getTriggerId());

        $this->assertInstanceOf(
            \LS\BPTriggerModel::class,
            $triggerModelSame
        );

        $this->assertSame(
            $triggerModel->getTriggerId(),
            $triggerModelSame->getTriggerId()
        );

        $this->assertNotEmpty(
            $triggerModelSame->getCreateDate()
        );

        $this->assertTrue(
            $triggerModel->valid()
        );

        $triggerModel->setFromArray(['name' => 'test2']);

        $this->assertTrue(
            $this->helper()->saveTriggerModel($triggerModel)
        );

        $triggerModelSame = $this->helper()->getTriggerModel($triggerModel->getTriggerId());

        $this->assertInstanceOf(
            \LS\BPTriggerModel::class,
            $triggerModelSame
        );

        $this->assertSame(
            $triggerModel->getTriggerId(),
            $triggerModelSame->getTriggerId()
        );

        $triggerModel = $this->saveTriggerModel(['trigger' => 'button']);
        $triggerModelSame = $this->helper()->getTriggerModel($triggerModel->getTriggerId());

        $this->assertSame(
            $triggerModel->getTriggerId(),
            $triggerModelSame->getTriggerIdent()
        );

        $triggerModel = $this->saveTriggerModel(['trigger' => 'consumersVisits']);
        $triggerModelSame = $this->helper()->getTriggerModel($triggerModel->getTriggerId());

        $this->assertSame(
            0,
            $triggerModelSame->getTriggerIdent()
        );
    }

    public function testGetTriggerModels()
    {
        $models[] = $this->saveTriggerModel();
        $models[] = $this->saveTriggerModel();
        $models[] = $this->saveTriggerModel();

        $modelsIds = array_map(static function (\LS\BPTriggerModel $model) {
            return $model->getTriggerId();
        }, $models);

        $triggerModels = $this->helper()->getTriggerModels();

        $this->assertNotEmpty($triggerModels);

        $this->assertInstanceOf(
            \LS\BPTriggerModel::class,
            array_values($triggerModels)[0]
        );

        $triggerModelsIds = array_map(static function (\LS\BPTriggerModel $model) {
            return $model->getTriggerId();
        }, $triggerModels);

        foreach ($modelsIds as $modelId) {
            $this->assertContains($modelId, $triggerModelsIds);
        }
    }

    public function testGetTriggerModelsByType()
    {
        $triggerModelsType_1 = $this->helper()->getTriggerModelsByType('type_1');
        $triggerModelsType_2 = $this->helper()->getTriggerModelsByType('type_2');

        $this->assertEmpty($triggerModelsType_1);
        $this->assertEmpty($triggerModelsType_2);

        $models[] = $this->saveTriggerModel(['trigger' => 'type_1']);
        $models[] = $this->saveTriggerModel(['trigger' => 'type_2']);
        $models[] = $this->saveTriggerModel(['trigger' => 'type_2']);

        $triggerModelsType_1 = $this->helper()->getTriggerModelsByType('type_1');

        $this->assertCount(1, $triggerModelsType_1);

        $triggerModelsType_2 = $this->helper()->getTriggerModelsByType('type_2');

        $this->assertCount(2, $triggerModelsType_2);

        foreach ($triggerModelsType_1 as $triggerModel) {
            $this->assertSame(
                'type_1',
                $triggerModel->getTriggerName()
            );
        }

        foreach ($triggerModelsType_2 as $triggerModel) {
            $this->assertSame(
                'type_2',
                $triggerModel->getTriggerName()
            );
        }
    }

    public function testGetRegisteredTriggerNames()
    {
        $this->assertNotEmpty(
            $this->helper()->getRegisteredTriggerNames()
        );
    }

    public function testGetTriggerFromModel()
    {
        $this->createTrigger();
    }

    public function testGetTriggersByType()
    {
        $this->saveTriggerModel(['trigger' => 'button']);

        $this->assertNotEmpty(
            $this->helper()->getTriggersByType('button')
        );
    }

    public function testGetTriggerModel()
    {
        $triggerModel = $this->saveTriggerModel();
        $triggerModelSame = $this->helper()->getTriggerModel($triggerModel->getTriggerId());

        $this->assertInstanceOf(
            \LS\BPTriggerModel::class,
            $triggerModelSame
        );

        $this->assertSame(
            $triggerModel->getTriggerId(),
            $triggerModelSame->getTriggerId()
        );
    }

    public function testDeleteTriggerModel()
    {
        $triggerModel = $this->saveTriggerModel();
        $triggerModelSame = $this->helper()->getTriggerModel($triggerModel->getTriggerId());

        $this->assertSame(
            $triggerModel->getTriggerId(),
            $triggerModelSame->getTriggerId()
        );

        $this->assertTrue(
            $this->helper()->deleteTriggerModel($triggerModel->getTriggerId())
        );

        $this->assertFalse(
            $this->helper()->getTriggerModel($triggerModel->getTriggerId())
        );
    }

    public function testGetConsumerTriggers()
    {
        $consumer = $this->createConsumer();

        $this->assertEmpty(
            $this->helper()->getConsumerTriggers($consumer->getConsumerId())
        );

        $trigger = $this->createTrigger();

        $this->assertTrue(
            $this->helper()->addTriggerToConsumer($trigger, $consumer)
        );

        $consumerTriggers = $this->helper()->getConsumerTriggers($consumer->getConsumerId());

        $this->assertCount(
            1,
            $consumerTriggers
        );

        $trigger = $this->createTrigger();

        $this->assertTrue(
            $this->helper()->addTriggerToConsumer($trigger, $consumer)
        );

        $consumerTriggers = $this->helper()->getConsumerTriggers($consumer->getConsumerId());

        $this->assertCount(
            2,
            $consumerTriggers
        );
    }

    public function testIsConsumerPerformedTrigger()
    {
        $consumer_1 = $this->createConsumer();
        $consumer_2 = $this->createConsumer();

        $this->assertEmpty(
            $this->helper()->getConsumerTriggers($consumer_1->getConsumerId())
        );
        $this->assertEmpty(
            $this->helper()->getConsumerTriggers($consumer_2->getConsumerId())
        );

        $trigger_1 = $this->createTrigger();
        $trigger_2 = $this->createTrigger();

        $this->assertTrue(
            $this->helper()->addTriggerToConsumer($trigger_1, $consumer_1)
        );
        $this->assertTrue(
            $this->helper()->addTriggerToConsumer($trigger_2, $consumer_2)
        );

        $this->assertFalse(
            $this->helper()->isConsumerPerformedTrigger($consumer_1, $trigger_2)
        );

        $this->assertFalse(
            $this->helper()->isConsumerPerformedTrigger($consumer_2, $trigger_1)
        );

        $this->assertTrue(
            $this->helper()->isConsumerPerformedTrigger($consumer_1, $trigger_1)
        );

        $this->assertTrue(
            $this->helper()->isConsumerPerformedTrigger($consumer_2, $trigger_2)
        );
    }

    public function testCanAddTriggerToConsumer()
    {
        $consumer_1 = $this->createConsumer();
        $consumer_2 = $this->createConsumer();

        $this->assertEmpty(
            $this->helper()->getConsumerTriggers($consumer_1->getConsumerId())
        );
        $this->assertEmpty(
            $this->helper()->getConsumerTriggers($consumer_2->getConsumerId())
        );

        $trigger_1 = $this->createTrigger();
        $trigger_2 = $this->createTrigger();

        $this->assertTrue(
            $this->helper()->canAddTriggerToConsumer($trigger_1, $consumer_1)
        );

        $this->assertTrue(
            $this->helper()->addTriggerToConsumer($trigger_1, $consumer_1)
        );

        $this->assertFalse(
            $this->helper()->canAddTriggerToConsumer($trigger_1, $consumer_1)
        );

        $this->assertTrue(
            $this->helper()->canAddTriggerToConsumer($trigger_2, $consumer_1)
        );

        $this->assertTrue(
            $this->helper()->addTriggerToConsumer($trigger_2, $consumer_1)
        );

        $this->assertFalse(
            $this->helper()->canAddTriggerToConsumer($trigger_2, $consumer_1)
        );

        $this->assertTrue(
            $this->helper()->canAddTriggerToConsumer($trigger_1, $consumer_2)
        );

        $this->assertTrue(
            $this->helper()->addTriggerToConsumer($trigger_1, $consumer_2)
        );

        $this->assertFalse(
            $this->helper()->canAddTriggerToConsumer($trigger_1, $consumer_2)
        );

        $this->assertTrue(
            $this->helper()->canAddTriggerToConsumer($trigger_2, $consumer_2)
        );

        $this->assertTrue(
            $this->helper()->addTriggerToConsumer($trigger_2, $consumer_2)
        );

        $this->assertFalse(
            $this->helper()->canAddTriggerToConsumer($trigger_2, $consumer_2)
        );

        $this->assertCount(
            2,
            $this->helper()->getConsumerTriggers($consumer_1->getConsumerId())
        );

        $this->assertCount(
            2,
            $this->helper()->getConsumerTriggers($consumer_2->getConsumerId())
        );
    }

    public function testAddTriggerToConsumer()
    {
        $consumer = $this->createConsumer();

        $this->assertEmpty(
            $this->helper()->getConsumerTriggers($consumer->getConsumerId())
        );

        $trigger = $this->createTrigger();

        $this->assertTrue(
            $this->helper()->addTriggerToConsumer($trigger, $consumer)
        );

        $consumerTriggers = $this->helper()->getConsumerTriggers($consumer->getConsumerId());

        $this->assertCount(
            1,
            $consumerTriggers
        );

        $consumerTriggerModel = array_pop($consumerTriggers);

        $this->assertInstanceOf(
            \LS\BPConsumerTrigger::class,
            $consumerTriggerModel
        );

        $this->assertSame(
            $consumer->getConsumerId(),
            $consumerTriggerModel->getConsumerId()
        );

        $this->assertSame(
            $trigger->getModel()->getTriggerId(),
            $consumerTriggerModel->getTriggerId()
        );
    }

    public function testCanConsumerPerformTrigger()
    {
        $consumer = $this->createConsumer();
        $trigger = $this->createTrigger(['active' => 0]);

        $this->assertTrue(
            $this->helper()->canConsumerPerformTrigger($consumer, $trigger)
        );

        $companyId = random_int(8888, 9999);
        $trigger = $this->createTrigger(['companyId' => $companyId]);

        $this->assertFalse(
            $this->helper()->canConsumerPerformTrigger($consumer, $trigger)
        );

        $consumer->setCompanyId($companyId);

        $this->assertTrue(
            $this->helper()->canConsumerPerformTrigger($consumer, $trigger)
        );

        $trigger = $this->createTrigger();

        $this->assertTrue(
            $this->helper()->canConsumerPerformTrigger($consumer, $trigger)
        );

        $this->assertTrue(
            $this->helper()->addTriggerToConsumer($trigger, $consumer)
        );

        $this->assertFalse(
            $this->helper()->canConsumerPerformTrigger($consumer, $trigger)
        );
    }

    public function testDeleteConsumer()
    {
        $consumer = $this->createConsumer();

        $this->assertEmpty(
            $this->helper()->getConsumerTriggers($consumer->getConsumerId())
        );

        $trigger = $this->createTrigger();
        $triggerModel = $trigger->getModel();

        $this->assertTrue(
            $this->helper()->addTriggerToConsumer($trigger, $consumer)
        );

        $consumerTriggers = $this->helper()->getConsumerTriggers($consumer->getConsumerId());

        $this->assertCount(
            1,
            $consumerTriggers
        );

        $this->saveTransaction([
            'consumerId' => $consumer->getConsumerId(),
            'ident'      => $triggerModel->getTriggerId(),
            'points'     => $triggerModel->getPoints(),
            'source'     => \LS\BPTransaction::SOURCE_TRIGGER
        ]);

        $points = random_int(11, 19);

        $this->saveTransaction([
            'consumerId' => $consumer->getConsumerId(),
            'points'     => $points
        ]);

        $consumer->addDynamicValue(
            'bpBalance',
            $this->helper()->calculateConsumerBalance($consumer->getConsumerId())
        );

        $this->assertTrue(
            \LS\ConsumersHelper::updateConsumer($consumer)
        );

        $consumer = \LS\ConsumersHelper::getConsumer($consumer->getConsumerId());

        /** @var \LS\bonusPoints $module */
        $module = $this->module();

        $this->assertCount(
            2,
            $this->helper()->getConsumerTransactions($consumer->getConsumerId())
        );

        $this->assertSame(
            $points + $triggerModel->getPoints(),
            $module->getConsumerBalance($consumer)
        );

        $this->helper()->deleteConsumer($consumer->getConsumerId());

        $consumer->addDynamicValue(
            'bpBalance',
            $this->helper()->calculateConsumerBalance($consumer->getConsumerId())
        );

        $this->assertTrue(
            \LS\ConsumersHelper::updateConsumer($consumer)
        );

        $consumer = \LS\ConsumersHelper::getConsumer($consumer->getConsumerId());

        $this->assertEmpty(
            $this->helper()->getConsumerTriggers($consumer->getConsumerId())
        );

        $this->assertEmpty(
            $this->helper()->getConsumerTransactions($consumer->getConsumerId())
        );

        $this->assertSame(
            0,
            $module->getConsumerBalance($consumer)
        );
    }

    public function testDeleteTriggerFromConsumer()
    {
        $consumer = $this->createConsumer();
        $trigger = $this->createTrigger();
        $triggerModel = $trigger->getModel();

        $this->assertFalse(
            $this->helper()->deleteTriggerFromConsumer($triggerModel->getTriggerId(), $consumer->getConsumerId())
        );

        $this->assertTrue(
            $this->helper()->addTriggerToConsumer($trigger, $consumer)
        );

        $this->assertCount(
            1,
            $this->helper()->getConsumerTriggers($consumer->getConsumerId())
        );

        $this->assertTrue(
            $this->helper()->deleteTriggerFromConsumer($triggerModel->getTriggerId(), $consumer->getConsumerId())
        );

        $this->assertEmpty(
            $this->helper()->getConsumerTriggers($consumer->getConsumerId())
        );
    }

    public function testPaginateTransactions()
    {
        $existingTransactions = $this->helper()->paginateTransactions(0, 0);

        $this->saveTransaction();
        $this->saveTransaction();
        $this->saveTransaction();
        $this->saveTransaction();

        $transactions = $this->helper()->paginateTransactions(0, 0);

        $this->assertCount(
            4 + count($existingTransactions['rows']),
            $transactions['rows']
        );

        $this->assertSame(
            4 + $existingTransactions['totalCount'],
            $transactions['totalCount']
        );
    }

    public function testGetTransaction()
    {
        $transaction = $this->saveTransaction();
        $transactionSame = $this->helper()->getTransaction($transaction->getTransactionId());

        $this->assertInstanceOf(
            \LS\BPTransaction::class,
            $transactionSame
        );

        $this->assertSame(
            $transaction->getTransactionId(),
            $transactionSame->getTransactionId()
        );
    }

    public function testSaveTransaction()
    {
        $transaction = $this->saveTransaction();
        $transactionSame = $this->helper()->getTransaction($transaction->getTransactionId());

        $this->assertInstanceOf(
            \LS\BPTransaction::class,
            $transactionSame
        );

        $this->assertSame(
            $transaction->getTransactionId(),
            $transactionSame->getTransactionId()
        );

        $consumer = $this->createConsumer();

        $this->assertEmpty(
            $this->helper()->getConsumerTransactions($consumer->getConsumerId())
        );

        $transaction = $this->saveTransaction([
            'consumerId' => $consumer->getConsumerId()
        ]);

        $consumerTransactions = $this->helper()->getConsumerTransactions($consumer->getConsumerId());

        $this->assertCount(
            1,
            $consumerTransactions
        );

        $consumerTransaction = array_pop($consumerTransactions);

        $this->assertSame(
            $consumer->getConsumerId(),
            $consumerTransaction->getConsumerId()
        );

        $this->assertSame(
            $transaction->getTransactionId(),
            $consumerTransaction->getTransactionId()
        );
    }

    public function testDeleteTransaction()
    {
        $consumer = $this->createConsumer();

        $this->assertEmpty(
            $this->helper()->getConsumerTransactions($consumer->getConsumerId())
        );

        $transaction = $this->saveTransaction([
            'consumerId' => $consumer->getConsumerId()
        ]);

        $this->assertCount(
            1,
            $this->helper()->getConsumerTransactions($consumer->getConsumerId())
        );

        $this->assertTrue(
            $this->helper()->deleteTransaction($transaction->getTransactionId())
        );

        $this->assertEmpty(
            $this->helper()->getConsumerTransactions($consumer->getConsumerId())
        );

        $this->assertFalse(
            $this->helper()->deleteTransaction($transaction->getTransactionId())
        );
    }

    public function testDeleteTransactions()
    {
        $this->helper()->deleteTransactions();

        $transactions = $this->helper()->paginateTransactions(0, 0);

        $this->assertEmpty(
            $transactions['rows']
        );

        $this->assertSame(
            0,
            $transactions['totalCount']
        );

        $this->saveTransaction();
        $this->saveTransaction();
        $this->saveTransaction();
        $this->saveTransaction();

        $transactions = $this->helper()->paginateTransactions(0, 0);

        $this->assertCount(
            4,
            $transactions['rows']
        );

        $this->assertSame(
            4,
            $transactions['totalCount']
        );

        $this->helper()->deleteTransactions();

        $transactions = $this->helper()->paginateTransactions(0, 0);

        $this->assertEmpty(
            $transactions['rows']
        );

        $this->assertSame(
            0,
            $transactions['totalCount']
        );
    }

    public function testDeleteTriggerModels()
    {
        $this->helper()->deleteTriggerModels();

        $this->assertEmpty(
            $this->helper()->getTriggerModels()
        );

        $this->createTrigger();
        $this->createTrigger();
        $this->createTrigger();

        $this->assertCount(
            3,
            $this->helper()->getTriggerModels()
        );

        $this->helper()->deleteTriggerModels();

        $this->assertEmpty(
            $this->helper()->getTriggerModels()
        );
    }

    public function testCalculateConsumerBalance()
    {
        $consumer = $this->createConsumer();

        $this->assertSame(
            0,
            $this->helper()->calculateConsumerBalance($consumer->getConsumerId())
        );

        $points = [
            random_int(1, 10),
            random_int(11, 20),
            random_int(21, 30)
        ];

        $this->saveTransaction([
            'consumerId' => $consumer->getConsumerId(),
            'points'     => $points[0]
        ]);

        $this->assertSame(
            $points[0],
            $this->helper()->calculateConsumerBalance($consumer->getConsumerId())
        );

        $this->saveTransaction([
            'consumerId' => $consumer->getConsumerId(),
            'points'     => $points[1]
        ]);

        $this->assertSame(
            $points[0] + $points[1],
            $this->helper()->calculateConsumerBalance($consumer->getConsumerId())
        );

        $this->saveTransaction([
            'consumerId' => $consumer->getConsumerId(),
            'points'     => $points[2]
        ]);

        $this->assertSame(
            array_sum($points),
            $this->helper()->calculateConsumerBalance($consumer->getConsumerId())
        );
    }

    public function testHasConsumerEnoughPoints()
    {
        $consumer = $this->createConsumer();

        $pointsEnough = random_int(11, 20);

        $this->assertFalse(
            $this->helper()->hasConsumerEnoughPoints($consumer->getConsumerId(), $pointsEnough)
        );

        $this->saveTransaction([
            'consumerId' => $consumer->getConsumerId(),
            'points'     => random_int(1, 10)
        ]);

        $this->assertGreaterThan(
            0,
            $this->helper()->calculateConsumerBalance($consumer->getConsumerId())
        );

        $this->assertFalse(
            $this->helper()->hasConsumerEnoughPoints($consumer->getConsumerId(), $pointsEnough)
        );

        $this->saveTransaction([
            'consumerId' => $consumer->getConsumerId(),
            'points'     => random_int(21, 30)
        ]);

        $this->assertTrue(
            $this->helper()->hasConsumerEnoughPoints($consumer->getConsumerId(), $pointsEnough)
        );
    }

    public function testHasTransactions()
    {
        $this->helper()->deleteTransactions();

        $this->assertFalse(
            $this->helper()->hasTransactions()
        );

        $this->saveTransaction(['points' => 0]);

        $this->assertFalse(
            $this->helper()->hasTransactions()
        );

        $this->saveTransaction(['points' => random_int(1, 99)]);

        $this->assertTrue(
            $this->helper()->hasTransactions()
        );
    }

    public function testGetTransactionsConsumersIds()
    {
        $consumer = $this->createConsumer();

        $this->assertNotContains(
            $consumer->getConsumerId(),
            $this->helper()->getTransactionsConsumersIds()
        );

        $this->saveTransaction([
            'consumerId' => $consumer->getConsumerId()
        ]);

        $this->assertContains(
            $consumer->getConsumerId(),
            $this->helper()->getTransactionsConsumersIds()
        );
    }

    public function testGetConsumerTransactions()
    {
        $consumer = $this->createConsumer();

        $this->assertEmpty(
            $this->helper()->getConsumerTransactions($consumer->getConsumerId())
        );

        $transaction = $this->saveTransaction([
            'consumerId' => $consumer->getConsumerId()
        ]);

        $consumerTransactions = $this->helper()->getConsumerTransactions($consumer->getConsumerId());

        $this->assertCount(
            1,
            $consumerTransactions
        );

        $consumerTransaction = array_pop($consumerTransactions);

        $this->assertSame(
            $transaction->getTransactionId(),
            $consumerTransaction->getTransactionId()
        );

        $this->saveTransaction([
            'consumerId' => $consumer->getConsumerId()
        ]);

        $this->assertCount(
            2,
            $this->helper()->getConsumerTransactions($consumer->getConsumerId())
        );
    }

    public function testDeleteConsumerTransactions()
    {
        $consumer = $this->createConsumer();

        $this->assertEmpty(
            $this->helper()->getConsumerTransactions($consumer->getConsumerId())
        );

        $this->assertFalse(
            $this->helper()->deleteConsumerTransactions($consumer->getConsumerId())
        );

        $this->saveTransaction([
            'consumerId' => $consumer->getConsumerId()
        ]);

        $this->saveTransaction([
            'consumerId' => $consumer->getConsumerId()
        ]);

        $this->assertCount(
            2,
            $this->helper()->getConsumerTransactions($consumer->getConsumerId())
        );

        $this->assertTrue(
            $this->helper()->deleteConsumerTransactions($consumer->getConsumerId())
        );

        $this->assertEmpty(
            $this->helper()->getConsumerTransactions($consumer->getConsumerId())
        );
    }

    public function testGetTotalPoints()
    {
        $this->helper()->deleteTransactions();

        $this->assertSame(
            0,
            $this->invokeMethod(\LS\BPHelper::class, 'getTotalPoints')
        );

        $points = [
            random_int(1, 10),
            random_int(11, 20),
            random_int(21, 30)
        ];

        $this->saveTransaction(['points' => $points[0]]);

        $this->assertSame(
            $points[0],
            $this->invokeMethod(\LS\BPHelper::class, 'getTotalPoints')
        );

        $this->saveTransaction(['points' => $points[1]]);
        $this->saveTransaction(['points' => $points[2]]);

        $this->assertSame(
            array_sum($points),
            $this->invokeMethod(\LS\BPHelper::class, 'getTotalPoints')
        );
    }

    public function testGetReceivedPoints()
    {
        $points = [
            random_int(1, 10),
            random_int(11, 20),
            random_int(21, 30)
        ];

        $this->assertSame(
            0,
            $this->invokeMethod(
                \LS\BPHelper::class,
                'getReceivedPoints',
                [strtotime('+1 Days'), strtotime('+4 Days')]
            )
        );

        $this->saveTransaction([
            'points'     => $points[0],
            'createDate' => strtotime('+2 Days')
        ]);

        $this->saveTransaction([
            'points'     => $points[1],
            'createDate' => strtotime('+3 Days')
        ]);

        $this->saveTransaction([
            'points'     => $points[2],
            'createDate' => strtotime('+4 Days')
        ]);

        $this->assertSame(
            array_sum($points),
            $this->invokeMethod(
                \LS\BPHelper::class,
                'getReceivedPoints',
                [strtotime('+1 Days'), strtotime('+4 Days')]
            )
        );

        $this->assertSame(
            $points[0] + $points[1],
            $this->invokeMethod(
                \LS\BPHelper::class,
                'getReceivedPoints',
                [strtotime('+1 Days'), strtotime('+3 Days')]
            )
        );

        $this->assertSame(
            $points[1] + $points[2],
            $this->invokeMethod(
                \LS\BPHelper::class,
                'getReceivedPoints',
                [strtotime('+3 Days'), strtotime('+4 Days')]
            )
        );

        $this->assertSame(
            $points[2],
            $this->invokeMethod(
                \LS\BPHelper::class,
                'getReceivedPoints',
                [strtotime('+4 Days'), strtotime('+4 Days')]
            )
        );
    }

    public function testGetSpentPoints()
    {
        $points = [
            random_int(-10, -1),
            random_int(11, 20),
            random_int(-30, -21)
        ];

        $this->assertSame(
            0,
            $this->invokeMethod(
                \LS\BPHelper::class,
                'getSpentPoints',
                [strtotime('+1 Days'), strtotime('+4 Days')]
            )
        );

        $this->saveTransaction([
            'points'     => $points[0],
            'createDate' => strtotime('+2 Days')
        ]);

        $this->saveTransaction([
            'points'     => $points[1],
            'createDate' => strtotime('+3 Days')
        ]);

        $this->saveTransaction([
            'points'     => $points[2],
            'createDate' => strtotime('+4 Days')
        ]);

        $this->assertSame(
            abs($points[0] + $points[2]),
            $this->invokeMethod(
                \LS\BPHelper::class,
                'getSpentPoints',
                [strtotime('+1 Days'), strtotime('+4 Days')]
            )
        );

        $this->assertSame(
            abs($points[0]),
            $this->invokeMethod(
                \LS\BPHelper::class,
                'getSpentPoints',
                [strtotime('+2 Days'), strtotime('+3 Days')]
            )
        );

        $this->assertSame(
            abs($points[2]),
            $this->invokeMethod(
                \LS\BPHelper::class,
                'getSpentPoints',
                [strtotime('+3 Days'), strtotime('+4 Days')]
            )
        );

        $this->assertSame(
            abs($points[2]),
            $this->invokeMethod(
                \LS\BPHelper::class,
                'getSpentPoints',
                [strtotime('+4 Days'), strtotime('+4 Days')]
            )
        );
    }

    public function testGetExpiredPoints()
    {
        $this->helper()->deleteTransactions();

        $this->assertSame(
            0,
            $this->invokeMethod(
                \LS\BPHelper::class,
                'getExpiredPoints',
                [strtotime('-4 Days'), strtotime('+4 Days')]
            )
        );

        $points = [
            random_int(1, 10),
            random_int(-11, -1),
            random_int(21, 30)
        ];

        $this->saveTransaction([
            'points' => $points[0]
        ]);

        $this->assertSame(
            0,
            $this->invokeMethod(
                \LS\BPHelper::class,
                'getExpiredPoints',
                [strtotime('-4 Days'), strtotime('+4 Days')]
            )
        );

        $this->saveTransaction([
            'points' => $points[0],
            'source' => \LS\BPTransaction::SOURCE_AGING
        ]);

        $this->assertSame(
            $points[0],
            $this->invokeMethod(
                \LS\BPHelper::class,
                'getExpiredPoints',
                [strtotime('-4 Days'), strtotime('+4 Days')]
            )
        );

        $this->saveTransaction([
            'points' => $points[1],
            'source' => \LS\BPTransaction::SOURCE_AGING
        ]);

        $this->saveTransaction([
            'points' => $points[1],
            'source' => \LS\BPTransaction::SOURCE_IMPORT
        ]);

        $this->saveTransaction([
            'points'     => $points[1],
            'source'     => \LS\BPTransaction::SOURCE_AGING,
            'createDate' => strtotime('+7 Days')
        ]);

        $this->saveTransaction([
            'points' => $points[2],
            'source' => \LS\BPTransaction::SOURCE_AGING
        ]);

        $this->saveTransaction([
            'points' => $points[2],
            'source' => \LS\BPTransaction::SOURCE_MANUAL
        ]);

        $this->assertSame(
            abs(array_sum($points)),
            $this->invokeMethod(
                \LS\BPHelper::class,
                'getExpiredPoints',
                [strtotime('-4 Days'), strtotime('+4 Days')]
            )
        );
    }

    public function testGetDashboardForPeriod()
    {
        $this->helper()->deleteTransactions();

        $startDate = strtotime('-4 Days');
        $endDate = strtotime('+4 Days');

        $dashboard = $this->invokeMethod(
            \LS\BPHelper::class,
            'getDashboardForPeriod',
            [$startDate, $endDate]
        );

        $this->assertSame(
            $startDate,
            $dashboard->startDate
        );

        $this->assertSame(
            $endDate,
            $dashboard->endDate
        );

        $this->assertSame(
            0,
            $dashboard->pointsReceived
        );

        $this->assertSame(
            0,
            $dashboard->pointsSpent
        );

        $this->assertSame(
            0,
            $dashboard->pointsExpired
        );

        $points = [
            random_int(1, 10),
            random_int(-11, -1),
            random_int(21, 30)
        ];

        $this->saveTransaction([
            'points' => $points[0]
        ]);

        $this->saveTransaction([
            'points' => $points[1]
        ]);

        $this->saveTransaction([
            'points' => $points[1],
            'source' => \LS\BPTransaction::SOURCE_AGING
        ]);

        $this->saveTransaction([
            'points' => $points[2]
        ]);

        $this->saveTransaction([
            'points' => $points[2],
            'source' => \LS\BPTransaction::SOURCE_AGING
        ]);

        $dashboard = $this->invokeMethod(
            \LS\BPHelper::class,
            'getDashboardForPeriod',
            [$startDate, $endDate]
        );

        $this->assertSame(
            2 * $points[2] + $points[0],
            $dashboard->pointsReceived
        );

        $this->assertSame(
            abs(2 * $points[1]),
            $dashboard->pointsSpent
        );

        $this->assertSame(
            abs($points[1] + $points[2]),
            $dashboard->pointsExpired
        );
    }

    public function testGetDashboard()
    {
        $this->helper()->deleteTransactions();

        $dayDate = strtotime('+2 Days');

        $dashboard = $this->helper()->getDashboard($dayDate);

        $this->assertSame(
            0,
            $dashboard->totalPoints
        );

        $this->assertSame(
            0,
            $dashboard->pointsReceived
        );

        $this->assertSame(
            0,
            $dashboard->pointsSpent
        );

        $this->assertSame(
            0,
            $dashboard->pointsExpired
        );

        $points = [
            random_int(1, 10),
            random_int(-11, -1),
            random_int(21, 30)
        ];

        $this->saveTransaction([
            'points' => $points[0]
        ]);

        $dashboard = $this->helper()->getDashboard($dayDate);

        $this->assertSame(
            0,
            $dashboard->totalPoints
        );

        $this->saveTransaction([
            'points'     => $points[2],
            'createDate' => strtotime('+1 Day')
        ]);

        $this->saveTransaction([
            'points'     => $points[2],
            'createDate' => strtotime('-1 Day')
        ]);

        $dashboard = $this->helper()->getDashboard();

        $this->assertSame(
            2 * $points[2] + $points[0],
            $dashboard->totalPoints
        );

        // Added transaction for get expiredPoints in dashboard, but dashboard return 0 points.
        $this->saveTransaction([
            'points'     => $points[0],
            'source'     => \LS\BPTransaction::SOURCE_AGING,
            'createDate' => strtotime('-3 Days'),
            'agingDate'  => strtotime('-2 Days')
        ]);

        $this->saveTransaction([
            'points' => $points[1]
        ]);

        $dashboard = $this->helper()->getDashboard();

        //var_dump($points);
        //var_dump($dashboard);

        $this->assertSame(
            (2 * $points[2]) + $points[0],
            $dashboard->totalPoints
        );

        //TODO: spentPoints, expiredPoints
    }

    public function testGetQuickHistory()
    {
        $this->helper()->deleteTransactions();

        $points = [
            random_int(1, 10),
            random_int(-11, -1),
            random_int(21, 30)
        ];

        $time = strtotime(date('2019-07-29'));

        $this->saveTransaction([
            'points'     => $points[0],
            'createDate' => $time
        ]);

        $this->saveTransaction([
            'points'     => $points[2],
            'createDate' => strtotime('-1 day', $time)
        ]);

        $this->saveTransaction([
            'points'     => $points[1],
            'createDate' => strtotime('-2 days', $time)
        ]);

        $this->saveTransaction([
            'points'     => $points[0],
            'source'     => \LS\BPTransaction::SOURCE_AGING,
            'createDate' => strtotime('-3 Days', $time),
            'agingDate'  => strtotime('-2 Days', $time)
        ]);

        $this->saveTransaction([
            'points'     => 2 * $points[0],
            'createDate' => strtotime('-11 days', $time)
        ]);

        $this->saveTransaction([
            'points'     => 2 * $points[2],
            'createDate' => strtotime('+8 days', $time)
        ]);

        $this->saveTransaction([
            'points'     => 5 * $points[0],
            'createDate' => strtotime('-1 year 5 days', $time)
        ]);

        $this->saveTransaction([
            'points'     => 4 * $points[0],
            'createDate' => strtotime(date('Y-12-31')) - 1
        ]);

        $this->saveTransaction([
            'points'     => 3 * $points[2],
            'createDate' => strtotime('+5 month', $time)
        ]);

        $history = $this->helper()->getQuickHistory(strtotime('TODAY', $time));

        //var_dump($points);
        //var_dump($history);

        $this->assertSame(
            $points[0],
            $history->today->pointsReceived
        );

        $this->assertSame(
            0,
            $history->today->pointsSpent
        );

        $this->assertSame(
            0,
            $history->today->pointsExpired
        );

        $this->assertSame(
            $points[2],
            $history->yesterday->pointsReceived
        );

        $this->assertSame(
            0,
            $history->yesterday->pointsSpent
        );

        $this->assertSame(
            0,
            $history->yesterday->pointsExpired
        );

        $this->assertSame(
            0,
            $history->{'-2 DAYS'}->pointsReceived
        );

        $this->assertSame(
            abs($points[1]),
            $history->{'-2 DAYS'}->pointsSpent
        );

        $this->assertSame(
            0,
            $history->{'-2 DAYS'}->pointsExpired
        );

        $this->assertSame(
            4 * $points[0] + $points[2],
            $history->thisMonth->pointsReceived
        );

        $this->assertSame(
            abs($points[1]),
            $history->thisMonth->pointsSpent
        );

        $this->assertSame(
            $points[0],
            $history->thisMonth->pointsExpired
        );

        $this->assertSame(
            (6 * $points[2]) + (8 * $points[0]),
            $history->thisYear->pointsReceived
        );

        $this->assertSame(
            abs($points[1]),
            $history->thisYear->pointsSpent
        );

        $this->assertSame(
            $points[0],
            $history->thisYear->pointsExpired
        );
    }

    public function testGetConsumersForBalanceConditionalEmail()
    {
        $this->helper()->deleteTransactions();

        $consumer = $this->createConsumer([
            'verified'         => true,
            'verificationDate' => time(),
            'allowEmails'      => true
        ]);

        $module = $this->module();

        $points = random_int(1111111, 9999999);

        $this->assertEmpty(
            $this->helper()->getConsumersForBalanceConditionalEmail($points, 'bp-test-email-slug')
        );

        $this->saveTransaction([
            'consumerId' => $consumer->getConsumerId(),
            'points'     => $points
        ]);

        $module->updateConsumerBalance($consumer->getConsumerId());

        $consumer = \LS\ConsumersHelper::getConsumer($consumer->getConsumerId());

        $consumers = $this->helper()->getConsumersForBalanceConditionalEmail($points, 'bp-test-email-slug');

        $this->assertCount(
            1,
            $consumers
        );

        $consumerSameId = array_pop($consumers);

        $consumerSame = \LS\ConsumersHelper::getConsumer($consumerSameId);

        $this->assertSame(
            $consumer->getConsumerId(),
            $consumerSame->getConsumerId()
        );

        $this->assertSame(
            $module->getConsumerBalance($consumer),
            $module->getConsumerBalance($consumerSame)
        );


        $this->assertCount(
            1,
            $this->helper()->getConsumersForBalanceConditionalEmail($points, 'bp-test-email-slug')
        );

        $this->assertTrue(
            \LS\ConsumersHelper::saveEmailLog(
                'Reached X points',
                $consumer->getConsumerId(),
                'bp-test-email-slug',
                true
            )
        );

        $this->assertEmpty(
            $this->helper()->getConsumersForBalanceConditionalEmail($points, 'bp-test-email-slug')
        );

        \LS\ConsumersHelper::deleteConsumer($consumer);
    }

    public function testGetConsumersToSendAgedConditionalEmail()
    {
        $this->helper()->deleteTransactions();

        $this->assertEmpty(
            $this->helper()->getConsumersToSendAgedConditionalEmail()
        );

        $consumer = $this->createConsumer([
            'verified'    => 1,
            'allowEmails' => 1
        ]);

        $points = random_int(91, 99);

        $transaction = $this->saveTransaction([
            'consumerId' => $consumer->getConsumerId(),
            'points'     => $points,
            'source'     => \LS\BPTransaction::SOURCE_AGING,
            'createDate' => strtotime('+2 days')
        ]);

        $this->saveTransaction([
            'consumerId' => $consumer->getConsumerId(),
            'points'     => $points,
            'source'     => \LS\BPTransaction::SOURCE_AGING,
            'createDate' => strtotime('-3 days')
        ]);

        $this->saveTransaction([
            'consumerId' => $consumer->getConsumerId(),
            'points'     => $points,
            'source'     => \LS\BPTransaction::SOURCE_MANUAL,
            'createDate' => strtotime('TODAY')
        ]);

        $consumersToSend = $this->helper()->getConsumersToSendAgedConditionalEmail();

        $this->assertCount(
            1,
            $consumersToSend
        );

        $consumerToSend = array_pop($consumersToSend);

        $this->assertSame(
            $transaction->getConsumerId(),
            $consumerToSend->consumerId
        );

        $this->assertSame(
            $transaction->getPoints(),
            $consumerToSend->points
        );

        $this->assertSame(
            $transaction->getCreateDate(),
            $consumerToSend->createDate
        );

        \LS\ConsumersHelper::deleteConsumer($consumer);
    }

    public function testGetImportFilesFolder()
    {
        $this->assertNotEmpty(
            $this->helper()->getImportFilesFolder()
        );

        //TODO: save new import & check files using 'getImportFilesFolder'
    }

    public function testGetPathToFile()
    {
        $import = $this->saveImport();

        $fileName = $import->getFileName();
        $filePath = $this->helper()->getPathToFile($fileName);

        $this->assertNotEmpty(
            $filePath
        );

        $this->assertFileNotExists(
            $filePath
        );

        $importFileName = 'test.csv';
        $importFilePath = __DIR__ . DIRECTORY_SEPARATOR . $importFileName;

        $importId = $this->helper()->startImport($importFileName, $importFilePath);

        $this->assertGreaterThan(
            0,
            $importId
        );

        $import = $this->helper()->getImport($importId);

        $this->assertSame(
            $importFileName,
            $import->getImportFileName()
        );

        $fileName = $import->getFileName();
        $filePath = $this->helper()->getPathToFile($fileName);

        $this->assertNotEmpty(
            $filePath
        );

        $this->assertFileExists(
            $filePath
        );

        $this->assertTrue(
            unlink($filePath)
        );
    }

    public function testPaginateImports()
    {
        //TODO
        $this->assertTrue(true);
    }

    public function testSaveImport()
    {
        $existingImports = $this->helper()->paginateImports(0, 0);

        $this->saveImport();

        $imports = $this->helper()->paginateImports(0, 0);

        $this->assertCount(
            $existingImports['totalCount'],
            $imports['rows']
        );

        $import = $this->saveImport([
            'status' => \LS\BPImport::STATUS_IMPORT_FINISHED
        ]);

        $imports = $this->helper()->paginateImports(0, 0);

        $this->assertCount(
            $existingImports['totalCount'] + 1,
            $imports['rows']
        );

        $importSame = $this->helper()->getImport($import->getImportId());

        $this->assertSame(
            $import->getImportId(),
            $importSame->getImportId()
        );
    }

    public function testStartImport()
    {
        $importFileName = 'test.csv';
        $importFilePath = __DIR__ . DIRECTORY_SEPARATOR . $importFileName;

        $this->assertFileExists($importFilePath);

        $this->assertNotEmpty(
            file_get_contents($importFilePath)
        );

        $importId = $this->helper()->startImport($importFileName, $importFilePath);

        $this->assertGreaterThan(
            0,
            $importId
        );

        $import = $this->helper()->getImport($importId);

        $this->assertSame(
            $importId,
            $import->getImportId()
        );

        $this->assertSame(
            (bool) \LS\BPImport::STATUS_IMPORT_STARTED,
            $import->getStatus()
        );

        $fileName = $import->getFileName();

        $this->assertNotEmpty(
            $fileName
        );

        $filePath = $this->helper()->getPathToFile($fileName);

        $this->assertFileExists(
            $filePath
        );

        $this->assertNotEmpty(
            file_get_contents($filePath)
        );

        $this->assertFileEquals(
            $importFilePath,
            $filePath
        );

        $this->assertTrue(
            unlink($filePath)
        );
    }

    public function testDeleteImport()
    {
        $existingImports = $this->helper()->paginateImports(0, 0);

        $import = $this->saveImport([
            'status' => \LS\BPImport::STATUS_IMPORT_FINISHED
        ]);

        $imports = $this->helper()->paginateImports(0, 0);

        $this->assertCount(
            $existingImports['totalCount'] + 1,
            $imports['rows']
        );

        $this->assertTrue(
            $this->helper()->deleteImport($import->getImportId())
        );

        $this->assertFalse(
            $this->helper()->getImport($import->getImportId())
        );
    }

    public function testGetImport()
    {
        $existingImports = $this->helper()->paginateImports(0, 0);

        $import = $this->saveImport([
            'status' => \LS\BPImport::STATUS_IMPORT_FINISHED
        ]);

        $imports = $this->helper()->paginateImports(0, 0);

        $this->assertCount(
            $existingImports['totalCount'] + 1,
            $imports['rows']
        );

        $importSame = $this->helper()->getImport($import->getImportId());

        $this->assertSame(
            $import->getImportId(),
            $importSame->getImportId()
        );
    }

    public function testGetExt()
    {
        $importFileName = 'test.csv';

        $this->assertSame(
            'csv',
            $this->helper()->getExt($importFileName)
        );
    }

}