<?php

require_once __DIR__ . '/bonusPointsLoyaltySuiteTestCase.php';

class bonusPointsLoyaltySuiteTestHelper extends bonusPointsLoyaltySuiteTestCase
{
    public function testSaveTriggerModel()
    {
        $this->assertSame(
            0,
            \LS\BPHelper::saveTriggerModel(new \LS\BPTriggerModel())
        );

        $triggerModel = $this->saveTriggerModel();
        $triggerModelSame = \LS\BPHelper::getTriggerModel($triggerModel->getTriggerId());

        $this->assertInstanceOf(
            \LS\BPTriggerModel::class,
            $triggerModelSame
        );

        $this->assertSame(
            $triggerModel->getTriggerId(),
            $triggerModelSame->getTriggerId()
        );

        $this->assertNotEmpty($triggerModel->getName());

        $this->assertSame(
            $triggerModel->getName(),
            $triggerModelSame->getName()
        );
    }

    public function testGetTriggerModels()
    {
        $models[] = $this->saveTriggerModel();
        $models[] = $this->saveTriggerModel();
        $models[] = $this->saveTriggerModel();

        $modelsIds = array_map(function(\LS\BPTriggerModel $model){
            return $model->getTriggerId();
        }, $models);

        $triggerModels = \LS\BPHelper::getTriggerModels();

        $this->assertNotEmpty($triggerModels);

        $this->assertContainsOnlyInstancesOf(
            \LS\BPTriggerModel::class,
            $triggerModels
        );

        $triggerModelsIds = array_map(function(\LS\BPTriggerModel $model){
            return $model->getTriggerId();
        }, $triggerModels);

        foreach($modelsIds as $modelId){
            $this->assertContains($modelId, $triggerModelsIds);
        }

        //todo: filters...
    }

    public function testGetTriggerModelsByType()
    {
        $triggerModelsType_1 = \LS\BPHelper::getTriggerModelsByType('type_1');
        $triggerModelsType_2 = \LS\BPHelper::getTriggerModelsByType('type_2');

        $this->assertEmpty($triggerModelsType_1);
        $this->assertEmpty($triggerModelsType_2);

        $models[] = $this->saveTriggerModel(['trigger'=>'type_1']);
        $models[] = $this->saveTriggerModel(['trigger'=>'type_2']);
        $models[] = $this->saveTriggerModel(['trigger'=>'type_2']);

        $triggerModelsType_1 = \LS\BPHelper::getTriggerModelsByType('type_1');

        $this->assertCount(1, $triggerModelsType_1);

        $triggerModelsType_2 = \LS\BPHelper::getTriggerModelsByType('type_2');

        $this->assertCount(2, $triggerModelsType_2);

        foreach($triggerModelsType_1 as $triggerModel){
            $this->assertSame(
                'type_1',
                $triggerModel->getTrigger()
            );
        }

        foreach($triggerModelsType_2 as $triggerModel){
            $this->assertSame(
                'type_2',
                $triggerModel->getTrigger()
            );
        }
    }

    public function testGetTriggerModel()
    {
        $triggerModel = $this->saveTriggerModel();
        $triggerModelSame = \LS\BPHelper::getTriggerModel($triggerModel->getTriggerId());

        $this->assertInstanceOf(
            \LS\BPTriggerModel::class,
            $triggerModelSame
        );

        $this->assertSame(
            $triggerModel->getTriggerId(),
            $triggerModelSame->getTriggerId()
        );
    }

    public function testDeleteTriggerModel()
    {
        $triggerModel = $this->saveTriggerModel();
        $triggerModelSame = \LS\BPHelper::getTriggerModel($triggerModel->getTriggerId());

        $this->assertSame(
            $triggerModel->getTriggerId(),
            $triggerModelSame->getTriggerId()
        );

        $this->assertTrue(
            \LS\BPHelper::deleteTriggerModel($triggerModel->getTriggerId())
        );

        $this->assertFalse(
            \LS\BPHelper::getTriggerModel($triggerModel->getTriggerId())
        );
    }

    public function testGetConsumerTriggers()
    {
        $consumer = $this->createConsumer();

        $this->assertEmpty(
            \LS\BPHelper::getConsumerTriggers($consumer->getConsumerId())
        );

        $triggerModel = $this->saveTriggerModel();

        $this->assertTrue(
            \LS\BPHelper::addTriggerToConsumer($triggerModel->getTriggerId(), $consumer->getConsumerId())
        );

        $consumerTriggers = \LS\BPHelper::getConsumerTriggers($consumer->getConsumerId());

        $this->assertCount(
            1,
            $consumerTriggers
        );

        $triggerModel = $this->saveTriggerModel();

        $this->assertTrue(
            \LS\BPHelper::addTriggerToConsumer($triggerModel->getTriggerId(), $consumer->getConsumerId())
        );

        $consumerTriggers = \LS\BPHelper::getConsumerTriggers($consumer->getConsumerId());

        $this->assertCount(
            2,
            $consumerTriggers
        );
    }

    public function testIsConsumerPerformedTrigger()
    {
        $consumer_1 = $this->createConsumer();
        $consumer_2 = $this->createConsumer();

        $this->assertEmpty(
            \LS\BPHelper::getConsumerTriggers($consumer_1->getConsumerId())
        );
        $this->assertEmpty(
            \LS\BPHelper::getConsumerTriggers($consumer_2->getConsumerId())
        );

        $triggerModel_1 = $this->saveTriggerModel();
        $triggerModel_2 = $this->saveTriggerModel();

        $this->assertTrue(
            \LS\BPHelper::addTriggerToConsumer($triggerModel_1->getTriggerId(), $consumer_1->getConsumerId())
        );
        $this->assertTrue(
            \LS\BPHelper::addTriggerToConsumer($triggerModel_2->getTriggerId(), $consumer_2->getConsumerId())
        );

        $this->assertFalse(
            \LS\BPHelper::isConsumerPerformedTrigger($consumer_1->getConsumerId(), $triggerModel_2->getTriggerId())
        );

        $this->assertFalse(
            \LS\BPHelper::isConsumerPerformedTrigger($consumer_2->getConsumerId(), $triggerModel_1->getTriggerId())
        );

        $this->assertTrue(
            \LS\BPHelper::isConsumerPerformedTrigger($consumer_1->getConsumerId(), $triggerModel_1->getTriggerId())
        );

        $this->assertTrue(
            \LS\BPHelper::isConsumerPerformedTrigger($consumer_2->getConsumerId(), $triggerModel_2->getTriggerId())
        );
    }

    public function testCanAddTriggerToConsumer()
    {
        $consumer_1 = $this->createConsumer();
        $consumer_2 = $this->createConsumer();

        $this->assertEmpty(
            \LS\BPHelper::getConsumerTriggers($consumer_1->getConsumerId())
        );
        $this->assertEmpty(
            \LS\BPHelper::getConsumerTriggers($consumer_2->getConsumerId())
        );

        $triggerModel_1 = $this->saveTriggerModel();
        $triggerModel_2 = $this->saveTriggerModel();

        $this->assertTrue(
            \LS\BPHelper::canAddTriggerToConsumer($triggerModel_1->getTriggerId(), $consumer_1->getConsumerId())
        );

        $this->assertTrue(
            \LS\BPHelper::addTriggerToConsumer($triggerModel_1->getTriggerId(), $consumer_1->getConsumerId())
        );

        $this->assertFalse(
            \LS\BPHelper::canAddTriggerToConsumer($triggerModel_1->getTriggerId(), $consumer_1->getConsumerId())
        );

        $this->assertTrue(
            \LS\BPHelper::canAddTriggerToConsumer($triggerModel_2->getTriggerId(), $consumer_1->getConsumerId())
        );

        $this->assertTrue(
            \LS\BPHelper::addTriggerToConsumer($triggerModel_2->getTriggerId(), $consumer_1->getConsumerId())
        );

        $this->assertFalse(
            \LS\BPHelper::canAddTriggerToConsumer($triggerModel_2->getTriggerId(), $consumer_1->getConsumerId())
        );

        $this->assertTrue(
            \LS\BPHelper::canAddTriggerToConsumer($triggerModel_1->getTriggerId(), $consumer_2->getConsumerId())
        );

        $this->assertTrue(
            \LS\BPHelper::addTriggerToConsumer($triggerModel_1->getTriggerId(), $consumer_2->getConsumerId())
        );

        $this->assertFalse(
            \LS\BPHelper::canAddTriggerToConsumer($triggerModel_1->getTriggerId(), $consumer_2->getConsumerId())
        );

        $this->assertTrue(
            \LS\BPHelper::canAddTriggerToConsumer($triggerModel_2->getTriggerId(), $consumer_2->getConsumerId())
        );

        $this->assertTrue(
            \LS\BPHelper::addTriggerToConsumer($triggerModel_2->getTriggerId(), $consumer_2->getConsumerId())
        );

        $this->assertFalse(
            \LS\BPHelper::canAddTriggerToConsumer($triggerModel_2->getTriggerId(), $consumer_2->getConsumerId())
        );

        $this->assertCount(
            2,
            \LS\BPHelper::getConsumerTriggers($consumer_1->getConsumerId())
        );

        $this->assertCount(
            2,
            \LS\BPHelper::getConsumerTriggers($consumer_2->getConsumerId())
        );
    }

    public function testCanConsumerPerformTrigger()
    {
        $consumer = $this->createConsumer();

        $triggerModel = $this->saveTriggerModel(['active'=>0]);

        $this->assertFalse(
            \LS\BPHelper::canConsumerPerformTrigger($consumer, $triggerModel)
        );

        $triggerModel = $this->saveTriggerModel(['active'=>1]);

        $this->assertTrue(
            \LS\BPHelper::canConsumerPerformTrigger($consumer, $triggerModel)
        );

        $triggerModel = $this->saveTriggerModel(['startDate'=>strtotime('+1 Day')]);

        $this->assertFalse(
            \LS\BPHelper::canConsumerPerformTrigger($consumer, $triggerModel)
        );

        $triggerModel = $this->saveTriggerModel(['startDate'=>time()]);

        $this->assertTrue(
            \LS\BPHelper::canConsumerPerformTrigger($consumer, $triggerModel)
        );

        $triggerModel = $this->saveTriggerModel(['endDate'=>strtotime('-1 Day')]);

        $this->assertFalse(
            \LS\BPHelper::canConsumerPerformTrigger($consumer, $triggerModel)
        );

        $triggerModel = $this->saveTriggerModel(['endDate'=>strtotime('+1 Day')]);

        $this->assertTrue(
            \LS\BPHelper::canConsumerPerformTrigger($consumer, $triggerModel)
        );

        $companyId = random_int(8888, 9999);

        $triggerModel = $this->saveTriggerModel(['companyId'=>$companyId]);

        $this->assertFalse(
            \LS\BPHelper::canConsumerPerformTrigger($consumer, $triggerModel)
        );

        $consumer->setCompanyId($companyId);

        $this->assertTrue(
            \LS\BPHelper::canConsumerPerformTrigger($consumer, $triggerModel)
        );

        $triggerModel = $this->saveTriggerModel();

        $this->assertTrue(
            \LS\BPHelper::canConsumerPerformTrigger($consumer, $triggerModel)
        );

        $this->assertTrue(
            \LS\BPHelper::addTriggerToConsumer($triggerModel->getTriggerId(), $consumer->getConsumerId())
        );

        $this->assertFalse(
            \LS\BPHelper::canConsumerPerformTrigger($consumer, $triggerModel)
        );

        $triggerModel = $this->saveTriggerModel(['multiple'=>1]);

        $this->assertTrue(
            \LS\BPHelper::addTriggerToConsumer($triggerModel->getTriggerId(), $consumer->getConsumerId())
        );

        $this->assertTrue(
            \LS\BPHelper::canConsumerPerformTrigger($consumer, $triggerModel)
        );

        $consumer = $this->createConsumer();
        $triggerModel = $this->saveTriggerModel();

        $this->assertTrue(
            \LS\BPHelper::canConsumerPerformTrigger($consumer, $triggerModel)
        );
    }

    public function testAddTriggerToConsumer()
    {
        $consumer = $this->createConsumer();

        $this->assertEmpty(
            \LS\BPHelper::getConsumerTriggers($consumer->getConsumerId())
        );

        $triggerModel = $this->saveTriggerModel();

        $this->assertTrue(
            \LS\BPHelper::addTriggerToConsumer($triggerModel->getTriggerId(), $consumer->getConsumerId())
        );

        $consumerTriggers = \LS\BPHelper::getConsumerTriggers($consumer->getConsumerId());

        $this->assertCount(
            1,
            $consumerTriggers
        );

        $consumerTriggerModel = array_pop($consumerTriggers);

        $this->assertInstanceOf(
            \LS\BPConsumerTrigger::class,
            $consumerTriggerModel
        );

        $this->assertSame(
            $consumer->getConsumerId(),
            $consumerTriggerModel->getConsumerId()
        );

        $this->assertSame(
            $triggerModel->getTriggerId(),
            $consumerTriggerModel->getTriggerId()
        );
    }

    public function testDeleteConsumer()
    {
        $consumer = $this->createConsumer();

        $this->assertEmpty(
            \LS\BPHelper::getConsumerTriggers($consumer->getConsumerId())
        );

        $triggerModel = $this->saveTriggerModel();

        $this->assertTrue(
            \LS\BPHelper::addTriggerToConsumer($triggerModel->getTriggerId(), $consumer->getConsumerId())
        );

        $consumerTriggers = \LS\BPHelper::getConsumerTriggers($consumer->getConsumerId());

        $this->assertCount(
            1,
            $consumerTriggers
        );

        $this->saveTransaction([
            'consumerId' => $consumer->getConsumerId(),
            'ident' => $triggerModel->getTriggerId(),
            'points' => $triggerModel->getPoints(),
            'source' => \LS\BPTransaction::SOURCE_TRIGGER
        ]);

        $points = random_int(11, 19);

        $this->saveTransaction([
            'consumerId' => $consumer->getConsumerId(),
            'points' => $points
        ]);

        $consumer->addDynamicValue(
            'bpBalance',
            \LS\BPHelper::calculateConsumerBalance($consumer->getConsumerId())
        );

        $this->assertTrue(
            \LS\ConsumersHelper::updateConsumer($consumer)
        );

        $consumer = \LS\ConsumersHelper::getConsumer($consumer->getConsumerId());

        /** @var \LS\bonusPoints $module */
        $module = $this->module();

        $this->assertCount(
            2,
            \LS\BPHelper::getConsumerTransactions($consumer->getConsumerId())
        );

        $this->assertSame(
            $points + $triggerModel->getPoints(),
            $module->getConsumerBalance($consumer)
        );

        \LS\BPHelper::deleteConsumer($consumer->getConsumerId());

        $consumer->addDynamicValue(
            'bpBalance',
            \LS\BPHelper::calculateConsumerBalance($consumer->getConsumerId())
        );

        $this->assertTrue(
            \LS\ConsumersHelper::updateConsumer($consumer)
        );

        $consumer = \LS\ConsumersHelper::getConsumer($consumer->getConsumerId());

        $this->assertEmpty(
            \LS\BPHelper::getConsumerTriggers($consumer->getConsumerId())
        );

        $this->assertEmpty(
            \LS\BPHelper::getConsumerTransactions($consumer->getConsumerId())
        );

        $this->assertSame(
            0,
            $module->getConsumerBalance($consumer)
        );
    }

    public function testDeleteTriggerFromConsumer()
    {
        $consumer = $this->createConsumer();

        $triggerModel = $this->saveTriggerModel();

        $this->assertFalse(
            \LS\BPHelper::deleteTriggerFromConsumer($triggerModel->getTriggerId(), $consumer->getConsumerId())
        );

        $this->assertTrue(
            \LS\BPHelper::addTriggerToConsumer($triggerModel->getTriggerId(), $consumer->getConsumerId())
        );

        $this->assertCount(
            1,
            \LS\BPHelper::getConsumerTriggers($consumer->getConsumerId())
        );

        $this->assertTrue(
            \LS\BPHelper::deleteTriggerFromConsumer($triggerModel->getTriggerId(), $consumer->getConsumerId())
        );

        $this->assertEmpty(
            \LS\BPHelper::getConsumerTriggers($consumer->getConsumerId())
        );
    }

    public function testPaginateTransactions()
    {
        $existingTransactions = \LS\BPHelper::paginateTransactions(0,0);

        $this->saveTransaction();
        $this->saveTransaction();
        $this->saveTransaction();
        $this->saveTransaction();

        $transactions = \LS\BPHelper::paginateTransactions(0,0);

        $this->assertCount(
            4 + count($existingTransactions['rows']),
            $transactions['rows']
        );

        $this->assertSame(
            4 + $existingTransactions['totalCount'],
            $transactions['totalCount']
        );

        //TODO: test 'paginateTransactions' using 'searchTerms' & 'filterTerms'... etc...
    }

    public function testGetTransaction()
    {
        $transaction = $this->saveTransaction();

        $transactionSame = \LS\BPHelper::getTransaction($transaction->getTransactionId());

        $this->assertInstanceOf(
            \LS\BPTransaction::class,
            $transactionSame
        );

        $this->assertSame(
            $transaction->getTransactionId(),
            $transactionSame->getTransactionId()
        );
    }

    public function testSaveTransaction()
    {
        $transaction = $this->saveTransaction();

        $transactionSame = \LS\BPHelper::getTransaction($transaction->getTransactionId());

        $this->assertInstanceOf(
            \LS\BPTransaction::class,
            $transactionSame
        );

        $this->assertSame(
            $transaction->getTransactionId(),
            $transactionSame->getTransactionId()
        );

        $consumer = $this->createConsumer();

        $this->assertEmpty(
            \LS\BPHelper::getConsumerTransactions($consumer->getConsumerId())
        );

        $transaction = $this->saveTransaction([
            'consumerId'=>$consumer->getConsumerId()
        ]);

        $consumerTransactions = \LS\BPHelper::getConsumerTransactions($consumer->getConsumerId());

        $this->assertCount(
            1,
            $consumerTransactions
        );

        $consumerTransaction = array_pop($consumerTransactions);

        $this->assertSame(
            $consumer->getConsumerId(),
            $consumerTransaction->getConsumerId()
        );

        $this->assertSame(
            $transaction->getTransactionId(),
            $consumerTransaction->getTransactionId()
        );
    }

    public function testDeleteTransaction()
    {
        $consumer = $this->createConsumer();

        $this->assertEmpty(
            \LS\BPHelper::getConsumerTransactions($consumer->getConsumerId())
        );

        $transaction = $this->saveTransaction([
            'consumerId' => $consumer->getConsumerId()
        ]);

        $this->assertCount(
            1,
            \LS\BPHelper::getConsumerTransactions($consumer->getConsumerId())
        );

        $this->assertTrue(
            \LS\BPHelper::deleteTransaction($transaction->getTransactionId())
        );

        $this->assertEmpty(
            \LS\BPHelper::getConsumerTransactions($consumer->getConsumerId())
        );

        $this->assertFalse(
            \LS\BPHelper::deleteTransaction($transaction->getTransactionId())
        );
    }

    public function testDeleteTransactions()
    {
        \LS\BPHelper::deleteTransactions();

        $transactions = \LS\BPHelper::paginateTransactions(0,0);

        $this->assertEmpty(
            $transactions['rows']
        );

        $this->assertSame(
            0,
            $transactions['totalCount']
        );

        $this->saveTransaction();
        $this->saveTransaction();
        $this->saveTransaction();
        $this->saveTransaction();

        $transactions = \LS\BPHelper::paginateTransactions(0,0);

        $this->assertCount(
            4,
            $transactions['rows']
        );

        $this->assertSame(
            4,
            $transactions['totalCount']
        );

        \LS\BPHelper::deleteTransactions();

        $transactions = \LS\BPHelper::paginateTransactions(0,0);

        $this->assertEmpty(
            $transactions['rows']
        );

        $this->assertSame(
            0,
            $transactions['totalCount']
        );
    }

    public function testCalculateConsumerBalance()
    {
        $consumer = $this->createConsumer();

        $this->assertSame(
            0,
            \LS\BPHelper::calculateConsumerBalance($consumer->getConsumerId())
        );

        $points = [
            random_int(1, 10),
            random_int(11, 20),
            random_int(21, 30)
        ];

        $this->saveTransaction([
            'consumerId' => $consumer->getConsumerId(),
            'points' => $points[0]
        ]);

        $this->assertSame(
            $points[0],
            \LS\BPHelper::calculateConsumerBalance($consumer->getConsumerId())
        );

        $this->saveTransaction([
            'consumerId' => $consumer->getConsumerId(),
            'points' => $points[1]
        ]);

        $this->assertSame(
            $points[0] + $points[1],
            \LS\BPHelper::calculateConsumerBalance($consumer->getConsumerId())
        );

        $this->saveTransaction([
            'consumerId' => $consumer->getConsumerId(),
            'points' => $points[2]
        ]);

        $this->assertSame(
            array_sum($points),
            \LS\BPHelper::calculateConsumerBalance($consumer->getConsumerId())
        );
    }

    public function testHasConsumerEnoughPoints()
    {
        $consumer = $this->createConsumer();

        $pointsEnough = random_int(11, 20);

        $this->assertFalse(
            \LS\BPHelper::hasConsumerEnoughPoints($consumer->getConsumerId(), $pointsEnough)
        );

        $this->saveTransaction([
            'consumerId' => $consumer->getConsumerId(),
            'points' => random_int(1, 10)
        ]);

        $this->assertGreaterThan(
            0,
            \LS\BPHelper::calculateConsumerBalance($consumer->getConsumerId())
        );

        $this->assertFalse(
            \LS\BPHelper::hasConsumerEnoughPoints($consumer->getConsumerId(), $pointsEnough)
        );

        $this->saveTransaction([
            'consumerId' => $consumer->getConsumerId(),
            'points' => random_int(21, 30)
        ]);

        $this->assertTrue(
            \LS\BPHelper::hasConsumerEnoughPoints($consumer->getConsumerId(), $pointsEnough)
        );
    }

    public function testHasTransactions()
    {
        \LS\BPHelper::deleteTransactions();

        $this->assertFalse(
            \LS\BPHelper::hasTransactions()
        );

        $this->saveTransaction(['points'=>0]);

        $this->assertFalse(
            \LS\BPHelper::hasTransactions()
        );

        $this->saveTransaction(['points'=>random_int(1, 99)]);

        $this->assertTrue(
            \LS\BPHelper::hasTransactions()
        );
    }

    public function testGetTransactionsConsumersIds()
    {
        $consumer = $this->createConsumer();

        $this->assertFalse(
            in_array($consumer->getConsumerId(), \LS\BPHelper::getTransactionsConsumersIds())
        );

        $this->saveTransaction([
            'consumerId' => $consumer->getConsumerId()
        ]);

        $this->assertTrue(
            in_array($consumer->getConsumerId(), \LS\BPHelper::getTransactionsConsumersIds())
        );
    }

    public function testGetConsumerTransactions()
    {
        $consumer = $this->createConsumer();

        $this->assertEmpty(
            \LS\BPHelper::getConsumerTransactions($consumer->getConsumerId())
        );

        $transaction = $this->saveTransaction([
            'consumerId' => $consumer->getConsumerId()
        ]);

        $consumerTransactions = \LS\BPHelper::getConsumerTransactions($consumer->getConsumerId());

        $this->assertCount(
            1,
            $consumerTransactions
        );

        $consumerTransaction = array_pop($consumerTransactions);

        $this->assertSame(
            $transaction->getTransactionId(),
            $consumerTransaction->getTransactionId()
        );

        $this->saveTransaction([
            'consumerId' => $consumer->getConsumerId()
        ]);

        $this->assertCount(
            2,
            \LS\BPHelper::getConsumerTransactions($consumer->getConsumerId())
        );
    }

    public function testDeleteConsumerTransactions()
    {
        $consumer = $this->createConsumer();

        $this->assertEmpty(
            \LS\BPHelper::getConsumerTransactions($consumer->getConsumerId())
        );

        $this->assertFalse(
            \LS\BPHelper::deleteConsumerTransactions($consumer->getConsumerId())
        );

        $this->saveTransaction([
            'consumerId' => $consumer->getConsumerId()
        ]);

        $this->saveTransaction([
            'consumerId' => $consumer->getConsumerId()
        ]);

        $this->assertCount(
            2,
            \LS\BPHelper::getConsumerTransactions($consumer->getConsumerId())
        );

        $this->assertTrue(
            \LS\BPHelper::deleteConsumerTransactions($consumer->getConsumerId())
        );

        $this->assertEmpty(
            \LS\BPHelper::getConsumerTransactions($consumer->getConsumerId())
        );
    }

    public function testGetTotalPoints()
    {
        \LS\BPHelper::deleteTransactions();

        $this->assertSame(
            0,
            $this->invokeMethod(\LS\BPHelper::class, 'getTotalPoints')
        );

        $points = [
            random_int(1, 10),
            random_int(11, 20),
            random_int(21, 30)
        ];

        $this->saveTransaction(['points'=>$points[0]]);

        $this->assertSame(
            $points[0],
            $this->invokeMethod(\LS\BPHelper::class, 'getTotalPoints')
        );

        $this->saveTransaction(['points'=>$points[1]]);
        $this->saveTransaction(['points'=>$points[2]]);

        $this->assertSame(
            array_sum($points),
            $this->invokeMethod(\LS\BPHelper::class, 'getTotalPoints')
        );
    }

    public function testGetReceivedPoints()
    {
        $points = [
            random_int(1, 10),
            random_int(11, 20),
            random_int(21, 30)
        ];

        $this->assertSame(
            0,
            $this->invokeMethod(
                \LS\BPHelper::class,
                'getReceivedPoints',
                [strtotime('+1 Days'), strtotime('+4 Days')]
            )
        );

        $this->saveTransaction([
            'points' => $points[0],
            'createDate' => strtotime('+2 Days')
        ]);

        $this->saveTransaction([
            'points' => $points[1],
            'createDate' => strtotime('+3 Days')
        ]);

        $this->saveTransaction([
            'points' => $points[2],
            'createDate' => strtotime('+4 Days')
        ]);

        $this->assertSame(
            array_sum($points),
            $this->invokeMethod(
                \LS\BPHelper::class,
                'getReceivedPoints',
                [strtotime('+1 Days'), strtotime('+4 Days')]
            )
        );

        $this->assertSame(
            $points[0] + $points[1],
            $this->invokeMethod(
                \LS\BPHelper::class,
                'getReceivedPoints',
                [strtotime('+1 Days'), strtotime('+3 Days')]
            )
        );

        $this->assertSame(
            $points[1] + $points[2],
            $this->invokeMethod(
                \LS\BPHelper::class,
                'getReceivedPoints',
                [strtotime('+3 Days'), strtotime('+4 Days')]
            )
        );

        $this->assertSame(
            $points[2],
            $this->invokeMethod(
                \LS\BPHelper::class,
                'getReceivedPoints',
                [strtotime('+4 Days'), strtotime('+4 Days')]
            )
        );
    }

    public function testGetSpentPoints()
    {
        $points = [
            random_int(-10, -1),
            random_int(11, 20),
            random_int(-30, -21)
        ];

        $this->assertSame(
            0,
            $this->invokeMethod(
                \LS\BPHelper::class,
                'getSpentPoints',
                [strtotime('+1 Days'), strtotime('+4 Days')]
            )
        );

        $this->saveTransaction([
            'points' => $points[0],
            'createDate' => strtotime('+2 Days')
        ]);

        $this->saveTransaction([
            'points' => $points[1],
            'createDate' => strtotime('+3 Days')
        ]);

        $this->saveTransaction([
            'points' => $points[2],
            'createDate' => strtotime('+4 Days')
        ]);

        $this->assertSame(
            abs($points[0] + $points[2]),
            $this->invokeMethod(
                \LS\BPHelper::class,
                'getSpentPoints',
                [strtotime('+1 Days'), strtotime('+4 Days')]
            )
        );

        $this->assertSame(
            abs($points[0]),
            $this->invokeMethod(
                \LS\BPHelper::class,
                'getSpentPoints',
                [strtotime('+2 Days'), strtotime('+3 Days')]
            )
        );

        $this->assertSame(
            abs($points[2]),
            $this->invokeMethod(
                \LS\BPHelper::class,
                'getSpentPoints',
                [strtotime('+3 Days'), strtotime('+4 Days')]
            )
        );

        $this->assertSame(
            abs($points[2]),
            $this->invokeMethod(
                \LS\BPHelper::class,
                'getSpentPoints',
                [strtotime('+4 Days'), strtotime('+4 Days')]
            )
        );
    }

    public function testGetExpiredPoints()
    {
        \LS\BPHelper::deleteTransactions();

        $this->assertSame(
            0,
            $this->invokeMethod(
                \LS\BPHelper::class,
                'getExpiredPoints',
                [strtotime('-4 Days'), strtotime('+4 Days')]
            )
        );

        $points = [
            random_int(1, 10),
            random_int(-11, -1),
            random_int(21, 30)
        ];

        $this->saveTransaction([
            'points' => $points[0]
        ]);

        $this->assertSame(
            0,
            $this->invokeMethod(
                \LS\BPHelper::class,
                'getExpiredPoints',
                [strtotime('-4 Days'), strtotime('+4 Days')]
            )
        );

        $this->saveTransaction([
            'points' => $points[0],
            'source' => \LS\BPTransaction::SOURCE_AGING
        ]);

        $this->assertSame(
            $points[0],
            $this->invokeMethod(
                \LS\BPHelper::class,
                'getExpiredPoints',
                [strtotime('-4 Days'), strtotime('+4 Days')]
            )
        );

        $this->saveTransaction([
            'points' => $points[1],
            'source' => \LS\BPTransaction::SOURCE_AGING
        ]);

        $this->saveTransaction([
            'points' => $points[1],
            'source' => \LS\BPTransaction::SOURCE_IMPORT
        ]);

        $this->saveTransaction([
            'points' => $points[1],
            'source' => \LS\BPTransaction::SOURCE_AGING,
            'createDate' => strtotime('+7 Days')
        ]);

        $this->saveTransaction([
            'points' => $points[2],
            'source' => \LS\BPTransaction::SOURCE_AGING
        ]);

        $this->saveTransaction([
            'points' => $points[2],
            'source' => \LS\BPTransaction::SOURCE_MANUAL
        ]);

        $this->assertSame(
            abs(array_sum($points)),
            $this->invokeMethod(
                \LS\BPHelper::class,
                'getExpiredPoints',
                [strtotime('-4 Days'), strtotime('+4 Days')]
            )
        );
    }

    public function testGetDashboardForPeriod()
    {
        \LS\BPHelper::deleteTransactions();

        $startDate = strtotime('-4 Days');
        $endDate = strtotime('+4 Days');

        $dashboard = $this->invokeMethod(
            \LS\BPHelper::class,
            'getDashboardForPeriod',
            [$startDate, $endDate]
        );

        $this->assertSame(
            $startDate,
            $dashboard->startDate
        );

        $this->assertSame(
            $endDate,
            $dashboard->endDate
        );

        $this->assertSame(
            0,
            $dashboard->pointsReceived
        );

        $this->assertSame(
            0,
            $dashboard->pointsSpent
        );

        $this->assertSame(
            0,
            $dashboard->pointsExpired
        );

        $points = [
            random_int(1, 10),
            random_int(-11, -1),
            random_int(21, 30)
        ];

        $this->saveTransaction([
            'points' => $points[0]
        ]);

        $this->saveTransaction([
            'points' => $points[1]
        ]);

        $this->saveTransaction([
            'points' => $points[1],
            'source' => \LS\BPTransaction::SOURCE_AGING
        ]);

        $this->saveTransaction([
            'points' => $points[2]
        ]);

        $this->saveTransaction([
            'points' => $points[2],
            'source' => \LS\BPTransaction::SOURCE_AGING
        ]);

        $dashboard = $this->invokeMethod(
            \LS\BPHelper::class,
            'getDashboardForPeriod',
            [$startDate, $endDate]
        );

        $this->assertSame(
            2 * $points[2] + $points[0],
            $dashboard->pointsReceived
        );

        $this->assertSame(
            abs(2 * $points[1]),
            $dashboard->pointsSpent
        );

        $this->assertSame(
            abs($points[1] + $points[2]),
            $dashboard->pointsExpired
        );
    }

    public function testGetDashboard()
    {
        \LS\BPHelper::deleteTransactions();

        $dayDate = strtotime('+2 Days');

        $dashboard = \LS\BPHelper::getDashboard($dayDate);

        $this->assertSame(
            0,
            $dashboard->totalPoints
        );

        $this->assertSame(
            0,
            $dashboard->pointsReceived
        );

        $this->assertSame(
            0,
            $dashboard->pointsSpent
        );

        $this->assertSame(
            0,
            $dashboard->pointsExpired
        );

        $points = [
            random_int(1, 10),
            random_int(-11, -1),
            random_int(21, 30)
        ];

        $this->saveTransaction([
            'points' => $points[0]
        ]);

        $dashboard = \LS\BPHelper::getDashboard($dayDate);

        $this->assertSame(
            0,
            $dashboard->totalPoints
        );

        $this->saveTransaction([
            'points' => $points[2],
            'createDate' => strtotime('+1 Day')
        ]);

        $this->saveTransaction([
            'points' => $points[2],
            'createDate' => strtotime('-1 Day')
        ]);

        $dashboard = \LS\BPHelper::getDashboard();

        $this->assertSame(
            2 * $points[2] + $points[0],
            $dashboard->totalPoints
        );

        // Added transaction for get expiredPoints in dashboard, but dashboard return 0 points.
        $this->saveTransaction([
            'points' => $points[0],
            'source' => \LS\BPTransaction::SOURCE_AGING,
            'createDate' => strtotime('-3 Days'),
            'agingDate' => strtotime('-2 Days')
        ]);

        $this->saveTransaction([
            'points' => $points[1]
        ]);

        $dashboard = \LS\BPHelper::getDashboard();

        //var_dump($points);
        //var_dump($dashboard);

        $this->assertSame(
            (2 * $points[2]) + $points[0],
            $dashboard->totalPoints
        );

        //TODO: spentPoints, expiredPoints
    }

    public function testGetQuickHistory()
    {
        \LS\BPHelper::deleteTransactions();

        $points = [
            random_int(1, 10),
            random_int(-11, -1),
            random_int(21, 30)
        ];

        $time = strtotime(date('2019-07-29'));

        $this->saveTransaction([
            'points' => $points[0],
            'createDate' => $time
        ]);

        $this->saveTransaction([
            'points' => $points[2],
            'createDate' => strtotime('-1 day', $time)
        ]);

        $this->saveTransaction([
            'points' => $points[1],
            'createDate' => strtotime('-2 days', $time)
        ]);

        $this->saveTransaction([
            'points' => $points[0],
            'source' => \LS\BPTransaction::SOURCE_AGING,
            'createDate' => strtotime('-3 Days', $time),
            'agingDate' => strtotime('-2 Days', $time)
        ]);

        $this->saveTransaction([
            'points' => 2 * $points[0],
            'createDate' => strtotime('-11 days', $time)
        ]);

        $this->saveTransaction([
            'points' => 2 * $points[2],
            'createDate' => strtotime('+8 days', $time)
        ]);

        $this->saveTransaction([
            'points' => 5 * $points[0],
            'createDate' => strtotime('-1 year 5 days', $time)
        ]);

        $this->saveTransaction([
            'points' => 4 * $points[0],
            'createDate' => strtotime(date('Y-12-31')) - 1
        ]);

        $this->saveTransaction([
            'points' => 3 * $points[2],
            'createDate' => strtotime('+5 month', $time)
        ]);

        $history = \LS\BPHelper::getQuickHistory(strtotime('TODAY', $time));

        //var_dump($points);
        //var_dump($history);

        $this->assertSame(
            $points[0],
            $history->today->pointsReceived
        );

        $this->assertSame(
            0,
            $history->today->pointsSpent
        );

        $this->assertSame(
            0,
            $history->today->pointsExpired
        );

        $this->assertSame(
            $points[2],
            $history->yesterday->pointsReceived
        );

        $this->assertSame(
            0,
            $history->yesterday->pointsSpent
        );

        $this->assertSame(
            0,
            $history->yesterday->pointsExpired
        );

        $this->assertSame(
            0,
            $history->{'-2 DAYS'}->pointsReceived
        );

        $this->assertSame(
            abs($points[1]),
            $history->{'-2 DAYS'}->pointsSpent
        );

        $this->assertSame(
            0,
            $history->{'-2 DAYS'}->pointsExpired
        );

        $this->assertSame(
            4 * $points[0] + $points[2],
            $history->thisMonth->pointsReceived
        );

        $this->assertSame(
            abs($points[1]),
            $history->thisMonth->pointsSpent
        );

        $this->assertSame(
            $points[0],
            $history->thisMonth->pointsExpired
        );

        $this->assertSame(
            (6 * $points[2]) + (8 * $points[0]),
            $history->thisYear->pointsReceived
        );

        $this->assertSame(
            abs($points[1]),
            $history->thisYear->pointsSpent
        );

        $this->assertSame(
            $points[0],
            $history->thisYear->pointsExpired
        );
    }

    public function testGetConsumersForBalanceConditionalEmail()
    {
        \LS\BPHelper::deleteTransactions();

        $consumer = $this->createConsumer([
            'verified' => true,
            'verificationDate' => time(),
            'allowEmails' => true
        ]);

        $module = $this->module();

        $points = random_int(1111111, 9999999);

        $this->assertEmpty(
            \LS\BPHelper::getConsumersForBalanceConditionalEmail($points, 'bp-test-email-slug')
        );

        $this->saveTransaction([
            'consumerId' => $consumer->getConsumerId(),
            'points' => $points
        ]);

        $module->updateConsumerBalance($consumer->getConsumerId());

        $consumer = \LS\ConsumersHelper::getConsumer($consumer->getConsumerId());

        $consumers = \LS\BPHelper::getConsumersForBalanceConditionalEmail($points, 'bp-test-email-slug');

        $this->assertCount(
            1,
            $consumers
        );

        $consumerSameId = array_pop($consumers);

        $consumerSame = \LS\ConsumersHelper::getConsumer($consumerSameId);

        $this->assertSame(
            $consumer->getConsumerId(),
            $consumerSame->getConsumerId()
        );

        $this->assertSame(
            $module->getConsumerBalance($consumer),
            $module->getConsumerBalance($consumerSame)
        );


        $this->assertCount(
            1,
            \LS\BPHelper::getConsumersForBalanceConditionalEmail($points, 'bp-test-email-slug')
        );

        $this->assertTrue(
            \LS\ConsumersHelper::saveEmailLog(
                'Reached X points',
                $consumer->getConsumerId(),
                'bp-test-email-slug',
                true
            )
        );

        $this->assertEmpty(
            \LS\BPHelper::getConsumersForBalanceConditionalEmail($points, 'bp-test-email-slug')
        );

        \LS\ConsumersHelper::deleteConsumer($consumer);
    }

    public function testGetConsumersToSendAgedConditionalEmail()
    {
        \LS\BPHelper::deleteTransactions();

        $this->assertEmpty(
            \LS\BPHelper::getConsumersToSendAgedConditionalEmail()
        );

        $consumer = $this->createConsumer([
            'verified' => 1,
            'allowEmails' => 1
        ]);

        $points = random_int(91, 99);

        $transaction = $this->saveTransaction([
            'consumerId' => $consumer->getConsumerId(),
            'points' => $points,
            'source' => \LS\BPTransaction::SOURCE_AGING,
            'createDate' => strtotime('+2 days')
        ]);

        $this->saveTransaction([
            'consumerId' => $consumer->getConsumerId(),
            'points' => $points,
            'source' => \LS\BPTransaction::SOURCE_AGING,
            'createDate' => strtotime('-3 days')
        ]);

        $this->saveTransaction([
            'consumerId' => $consumer->getConsumerId(),
            'points' => $points,
            'source' => \LS\BPTransaction::SOURCE_MANUAL,
            'createDate' => strtotime('TODAY')
        ]);

        $consumersToSend = \LS\BPHelper::getConsumersToSendAgedConditionalEmail();

        $this->assertCount(
            1,
            $consumersToSend
        );

        $consumerToSend = array_pop($consumersToSend);

        $this->assertSame(
            $transaction->getConsumerId(),
            $consumerToSend->consumerId
        );

        $this->assertSame(
            $transaction->getPoints(),
            $consumerToSend->points
        );

        $this->assertSame(
            $transaction->getCreateDate(),
            $consumerToSend->createDate
        );

        \LS\ConsumersHelper::deleteConsumer($consumer);
    }

    public function testGetImportFilesFolder()
    {
        $this->assertNotEmpty(
            \LS\BPHelper::getImportFilesFolder()
        );

        //TODO: save new import & check files using 'getImportFilesFolder'
    }

    public function testGetPathToFile()
    {
        $import = $this->saveImport();

        $fileName = $import->getFileName();
        $filePath = \LS\BPHelper::getPathToFile($fileName);

        $this->assertNotEmpty(
            $filePath
        );

        $this->assertFileNotExists(
            $filePath
        );

        $importFileName = 'test.csv';
        $importFilePath = __DIR__ . DIRECTORY_SEPARATOR . $importFileName;

        $importId = \LS\BPHelper::startImport($importFileName, $importFilePath);

        $this->assertGreaterThan(
            0,
            $importId
        );

        $import = \LS\BPHelper::getImport($importId);

        $this->assertSame(
            $importFileName,
            $import->getImportFileName()
        );

        $fileName = $import->getFileName();
        $filePath = \LS\BPHelper::getPathToFile($fileName);

        $this->assertNotEmpty(
            $filePath
        );

        $this->assertFileExists(
            $filePath
        );

        $this->assertTrue(
            unlink($filePath)
        );
    }

    public function testPaginateImports()
    {
        //TODO
        $this->assertTrue(true);
    }

    public function testSaveImport()
    {
        $existingImports = \LS\BPHelper::paginateImports(0,0);

        $this->saveImport();

        $imports = \LS\BPHelper::paginateImports(0,0);

        $this->assertCount(
            $existingImports['totalCount'],
            $imports['rows']
        );

        $import = $this->saveImport([
            'status' => \LS\BPImport::STATUS_IMPORT_FINISHED
        ]);

        $imports = \LS\BPHelper::paginateImports(0,0);

        $this->assertCount(
            $existingImports['totalCount'] + 1,
            $imports['rows']
        );

        $importSame = \LS\BPHelper::getImport($import->getImportId());

        $this->assertSame(
            $import->getImportId(),
            $importSame->getImportId()
        );
    }

    public function testStartImport()
    {
        $importFileName = 'test.csv';
        $importFilePath = __DIR__ . DIRECTORY_SEPARATOR . $importFileName;

        $this->assertTrue(
            file_exists($importFilePath)
        );

        $this->assertNotEmpty(
            file_get_contents($importFilePath)
        );

        $importId = \LS\BPHelper::startImport($importFileName, $importFilePath);

        $this->assertGreaterThan(
            0,
            $importId
        );

        $import = \LS\BPHelper::getImport($importId);

        $this->assertSame(
            $importId,
            $import->getImportId()
        );

        $this->assertSame(
            (bool)\LS\BPImport::STATUS_IMPORT_STARTED,
            $import->getStatus()
        );

        $fileName = $import->getFileName();

        $this->assertNotEmpty(
            $fileName
        );

        $filePath = \LS\BPHelper::getPathToFile($fileName);

        $this->assertFileExists(
            $filePath
        );

        $this->assertNotEmpty(
            file_get_contents($filePath)
        );

        $this->assertFileEquals(
            $importFilePath,
            $filePath
        );

        $this->assertTrue(
            unlink($filePath)
        );
    }

    public function testDeleteImport()
    {
        $existingImports = \LS\BPHelper::paginateImports(0,0);

        $import = $this->saveImport([
            'status' => \LS\BPImport::STATUS_IMPORT_FINISHED
        ]);

        $imports = \LS\BPHelper::paginateImports(0,0);

        $this->assertCount(
            $existingImports['totalCount'] + 1,
            $imports['rows']
        );

        $this->assertTrue(
            \LS\BPHelper::deleteImport($import->getImportId())
        );

        $this->assertFalse(
            \LS\BPHelper::getImport($import->getImportId())
        );
    }

    public function testGetImport()
    {
        $existingImports = \LS\BPHelper::paginateImports(0,0);

        $import = $this->saveImport([
            'status' => \LS\BPImport::STATUS_IMPORT_FINISHED
        ]);

        $imports = \LS\BPHelper::paginateImports(0,0);

        $this->assertCount(
            $existingImports['totalCount'] + 1,
            $imports['rows']
        );

        $importSame = \LS\BPHelper::getImport($import->getImportId());

        $this->assertSame(
            $import->getImportId(),
            $importSame->getImportId()
        );
    }

    public function testGetExt()
    {
        $importFileName = 'test.csv';

        $this->assertSame(
            'csv',
            \LS\BPHelper::getExt($importFileName)
        );
    }

}