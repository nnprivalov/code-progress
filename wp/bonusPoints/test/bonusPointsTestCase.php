<?php
define('PHP_UNIT_TEST', true);

require_once __DIR__ . '/../../../../../wp-load.php';

use PHPUnit\Framework\TestCase;

class bonusPointsTestCase extends TestCase
{
    public function setUp(): void
    {
        global $wpdb;
        $wpdb->query("BEGIN");

        \LS\Library::cleanMessages();
    }

    public function tearDown(): void
    {
        global $wpdb;
        $wpdb->query("ROLLBACK");

        \LS\Library::cleanMessages();
    }

    /**
     * @return \LS\bonusPointsAdmin
     */
    public function module()
    {
        return LS()->getModule('bonusPoints');
    }

    /**
     * @return \LS\BPHelper
     */
    public function helper()
    {
        return $this->module()->helper();
    }

    /**
     * @return string
     */
    protected function randString()
    {
        return md5(uniqid(random_int(0, 100), true));
    }

    /**
     * @throws Exception
     * @param array $params
     * @return \LS\BPTriggerModel
     */
    public function saveTriggerModel($params = [])
    {
        $data = [
            'name'          => $this->randString(),
            'trigger'       => 'consumersVisits',
            'triggerIdent'  => 0,
            'points'        => random_int(1, 20),
            'message'       => $this->randString(),
            'consumerAlert' => $this->randString(),
            'active'        => 1,
            'createDate'    => time()
        ];

        $data = array_merge($data, $params);
        $triggerModel = new \LS\BPTriggerModel($data);

        $this->helper()->saveTriggerModel($triggerModel);

        $this->assertNotEmpty(
            $triggerModel->getTriggerId()
        );

        return $triggerModel;
    }

    /**
     * @throws Exception
     * @param array $params
     * @return \LS\BPTrigger
     */
    public function createTrigger($params = [])
    {
        $triggerModel = $this->saveTriggerModel($params);
        $trigger = $this->helper()->getTriggerFromModel($triggerModel);

        $this->assertNotFalse($trigger);

        return $trigger;
    }

    /**
     * @param array $params
     * @return \LS\Consumer
     */
    public function createConsumer($params = [])
    {
        $data = [
            'active'   => 1,
            'verified' => 1
        ];

        $data = array_merge($data, $params);
        $consumer = new \LS\Consumer($data);

        $this->assertNotEmpty(
            \LS\ConsumersHelper::createConsumer($consumer)
        );

        return $consumer;
    }

    /**
     * @param array $params
     * @return \LS\BPTransaction
     */
    public function saveTransaction($params = [])
    {
        $data = [
            'source'     => \LS\BPTransaction::SOURCE_MANUAL,
            'createDate' => time()
        ];

        $data = array_merge($data, $params);
        $transaction = new \LS\BPTransaction($data);

        $this->assertTrue(
            $this->helper()->saveTransaction($transaction)
        );

        return $transaction;
    }

    /**
     * @param array $params
     * @return \LS\BPImport
     */
    public function saveImport($params = [])
    {
        $data = [
            'importDate' => time(),
            'fileName'   => $this->randString() . '.csv'
        ];

        $data = array_merge($data, $params);
        $import = new \LS\BPImport($data);

        $this->assertTrue(
            $this->helper()->saveImport($import)
        );

        $filePath = $this->helper()->getPathToFile($import->getFileName());

        $this->assertFileNotExists(
            $filePath
        );

        return $import;
    }

    /**
     * @throws ReflectionException
     * @param $methodName
     * @param array $params
     * @param $className
     * @return mixed
     */
    public function invokeMethod($className, $methodName, $params = [])
    {
        $reflection = new ReflectionClass($className);
        $method = $reflection->getMethod($methodName);
        $method->setAccessible(true);

        return $method->invokeArgs(new $className(), $params);
    }

    /**
     * @throws ReflectionException
     * @param $methodName
     * @param array $params
     * @param $object
     * @return mixed
     */
    public function invokeObjectMethod($object, $methodName, $params = [])
    {
        $reflection = new ReflectionObject($object);
        $method = $reflection->getMethod($methodName);
        $method->setAccessible(true);

        return $method->invokeArgs($object, $params);
    }

    public function testSkip()
    {
        $this->assertTrue(true);
    }
}

if (!class_exists('CurrentScreenFix')) {

    class CurrentScreenFix
    {
        static $inAdmin = false;

        public static function setAsAdmin()
        {
            self::$inAdmin = true;
        }

        public static function setAsFront()
        {
            self::$inAdmin = false;
        }

        public function in_admin()
        {
            return self::$inAdmin;
        }
    }

    $GLOBALS['current_screen'] = new CurrentScreenFix();
}