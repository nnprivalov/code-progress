<?php

require_once __DIR__ . '/bonusPointsLoyaltySuiteTestCase.php';

class bonusPointsLoyaltySuiteTestBonusPoints extends bonusPointsLoyaltySuiteTestCase
{

    public function testSubmitBonusTriggerButton()
    {
        $module = $this->module();

        $consumer = $this->createConsumer();

        $this->assertEmpty(
            \LS\BPHelper::getConsumerTriggers($consumer->getConsumerId())
        );

        $this->assertEmpty(
            \LS\BPHelper::getConsumerTransactions($consumer->getConsumerId())
        );

        $trigger = $this->saveTriggerModel([
            'trigger' => 'button'
        ]);

        $this->invokeObjectMethod(
            $module,
            'submitBonusTriggerButton',
            [$consumer, $trigger->getTriggerId()]
        );

        $this->assertNotEmpty(
            \LS\BPHelper::getConsumerTriggers($consumer->getConsumerId())
        );

        $this->assertNotEmpty(
            \LS\BPHelper::getConsumerTransactions($consumer->getConsumerId())
        );

        \LS\ConsumersHelper::deleteConsumer($consumer);
    }

    public function testGetConsumerBalance()
    {
        $module = $this->module();

        $consumer = $this->createConsumer();

        $this->assertSame(
            0,
            $module->getConsumerBalance($consumer)
        );

        $points = random_int(1, 20);

        $transactionId = $module->createTransaction(
            $this->randString(),
            $consumer->getConsumerId(),
            $points,
            $this->randString(),
            time(),
            \LS\BPTransaction::SOURCE_MANUAL
        );

        $consumer = \LS\ConsumersHelper::getConsumer($consumer->getConsumerId());

        $this->assertSame(
            $points,
            $module->getConsumerBalance($consumer)
        );

        \LS\ConsumersHelper::deleteConsumer($consumer);
    }

    public function testUpdateConsumerBalance()
    {
        $module = $this->module();

        $consumer = $this->createConsumer();

        $this->assertSame(
            0,
            $module->getConsumerBalance($consumer)
        );

        $points = random_int(1, 20);

        $transaction = $this->saveTransaction([
            'consumerId' => $consumer->getConsumerId(),
            'points' => $points
        ]);

        $consumer = \LS\ConsumersHelper::getConsumer($consumer->getConsumerId());

        $this->assertSame(
            0,
            $module->getConsumerBalance($consumer)
        );

        $module->updateConsumerBalance($consumer->getConsumerId());

        $this->assertSame(
            0,
            $module->getConsumerBalance($consumer)
        );

        $consumer = \LS\ConsumersHelper::getConsumer($consumer->getConsumerId());

        $this->assertSame(
            $points,
            $module->getConsumerBalance($consumer)
        );

        \LS\ConsumersHelper::deleteConsumer($consumer);
    }

    public function testCalculateAgingDateTimestamp()
    {
        $module = $this->module();

        $createDate = time();

        $module->options()->setOption('agingDays', 7);

        $this->assertGreaterThan(
            0,
            $this->invokeObjectMethod(
                $module,
                'calculateAgingDateTimestamp',
                [$createDate]
            )
        );

        $this->assertSame(
            0,
            $this->invokeObjectMethod(
                $module,
                'calculateAgingDateTimestamp',
                [strtotime('-8 Days', $createDate)]
            )
        );
    }

    public function testCreateTransaction()
    {
        $module = $this->module();

        $consumer = $this->createConsumer();

        \LS\BPHelper::deleteTransactions();

        $this->assertEmpty(
            \LS\BPHelper::getConsumerTransactions($consumer->getConsumerId())
        );

        $points = random_int(88, 99);

        $message = $this->randString();

        $transactionId = $module->createTransaction(
            null,
            $consumer->getConsumerId(),
            $points,
            $message,
            time(),
            \LS\BPTransaction::SOURCE_MANUAL,
            null
        );

        $this->assertNotEmpty(
            \LS\BPHelper::getConsumerTransactions($consumer->getConsumerId())
        );

        /** @var \LS\BPTransaction $transaction */
        $transaction = array_pop(
            \LS\BPHelper::getConsumerTransactions($consumer->getConsumerId())
        );

        $this->assertSame(
            $transactionId,
            $transaction->getTransactionId()
        );

        $this->assertSame(
            $message,
            $transaction->getMessage()
        );

        \LS\ConsumersHelper::deleteConsumer($consumer);
    }

    public function testPerformSingleTrigger()
    {
        $module = $this->module();

        $trigger = $this->saveTriggerModel();

        $consumer = $this->createConsumer();

        $module->updateConsumerBalance($consumer->getConsumerId());

        $consumer = \LS\ConsumersHelper::getConsumer($consumer->getConsumerId());

        $this->assertSame(
            0,
            $module->getConsumerBalance($consumer)
        );

        $this->assertTrue(
            $this->invokeObjectMethod(
                $module,
                'performSingleTrigger',
                [$trigger, $consumer]
            )
        );

        $module->updateConsumerBalance($consumer->getConsumerId());

        $consumer = \LS\ConsumersHelper::getConsumer($consumer->getConsumerId());

        $this->assertSame(
            $trigger->getPoints(),
            $module->getConsumerBalance($consumer)
        );

        $this->assertTrue(
            \LS\BPHelper::isConsumerPerformedTrigger(
                $consumer->getConsumerId(),
                $trigger->getTriggerId()
            )
        );

        \LS\ConsumersHelper::deleteConsumer($consumer);
    }

    public function testPerformTriggers()
    {
        $module = $this->module();

        $points = [
            random_int(11,19),
            random_int(21,29),
            random_int(31,39),
            random_int(41,49)
        ];

        $triggerType = $this->randString();
        $triggerType_2 = $this->randString();

        $this->assertNotSame(
            $triggerType,
            $triggerType_2
        );

        $trigger_1 = $this->saveTriggerModel([
            'trigger' => $triggerType,
            'points' => $points[0]
        ]);

        $trigger_2 = $this->saveTriggerModel([
            'trigger' => $triggerType,
            'points' => $points[1]
        ]);

        $trigger_3 = $this->saveTriggerModel([
            'trigger' => $triggerType,
            'points' => $points[2]
        ]);

        $trigger_4 = $this->saveTriggerModel([
            'trigger' => $triggerType_2,
            'points' => $points[3]
        ]);

        $consumer = $this->createConsumer();

        $module->updateConsumerBalance($consumer->getConsumerId());

        $consumer = \LS\ConsumersHelper::getConsumer($consumer->getConsumerId());

        $this->assertSame(
            0,
            $module->getConsumerBalance($consumer)
        );

        $this->assertFalse(
            \LS\BPHelper::isConsumerPerformedTrigger($consumer->getConsumerId(), $trigger_1->getTriggerId())
        );

        $this->assertFalse(
            \LS\BPHelper::isConsumerPerformedTrigger($consumer->getConsumerId(), $trigger_2->getTriggerId())
        );

        $this->assertFalse(
            \LS\BPHelper::isConsumerPerformedTrigger($consumer->getConsumerId(), $trigger_3->getTriggerId())
        );

        $module->performTriggers($triggerType, $consumer);

        $this->assertTrue(
            \LS\BPHelper::isConsumerPerformedTrigger($consumer->getConsumerId(), $trigger_1->getTriggerId())
        );

        $this->assertTrue(
            \LS\BPHelper::isConsumerPerformedTrigger($consumer->getConsumerId(), $trigger_2->getTriggerId())
        );

        $this->assertTrue(
            \LS\BPHelper::isConsumerPerformedTrigger($consumer->getConsumerId(), $trigger_3->getTriggerId())
        );

        $module->updateConsumerBalance($consumer->getConsumerId());

        $consumer = \LS\ConsumersHelper::getConsumer($consumer->getConsumerId());

        $expectedConsumerPoints = $trigger_1->getPoints() + $trigger_2->getPoints() + $trigger_3->getPoints();

        $this->assertSame(
            $expectedConsumerPoints,
            $module->getConsumerBalance($consumer)
        );

        $consumer_2 = $this->createConsumer();

        $this->assertFalse(
            \LS\BPHelper::isConsumerPerformedTrigger($consumer_2->getConsumerId(), $trigger_4->getTriggerId())
        );

        $module->performTriggers($triggerType_2, $consumer_2);

        $this->assertTrue(
            \LS\BPHelper::isConsumerPerformedTrigger($consumer_2->getConsumerId(), $trigger_4->getTriggerId())
        );

        $module->updateConsumerBalance($consumer_2->getConsumerId());

        $consumer_2 = \LS\ConsumersHelper::getConsumer($consumer_2->getConsumerId());

        $this->assertSame(
            $trigger_4->getPoints(),
            $module->getConsumerBalance($consumer_2)
        );

        \LS\ConsumersHelper::deleteConsumer($consumer);
        \LS\ConsumersHelper::deleteConsumer($consumer_2);
    }

    public function testConsumerVisitSaved()
    {
        $module = $this->module();

        $consumer = $this->createConsumer();

        $module->consumerVisitSaved($consumer);

        $module->updateConsumerBalance($consumer->getConsumerId());

        $consumer = \LS\ConsumersHelper::getConsumer($consumer->getConsumerId());

        $this->assertSame(
            0,
            $module->getConsumerBalance($consumer)
        );

        $triggerIdent = 0;
        $triggerType = 'consumersVisits';
        $points = [
            random_int(11,19),
            random_int(21,29),
            random_int(31,39),
            random_int(41,49)
        ];

        $trigger_1 = $this->saveTriggerModel([
            'triggerIdent' => $triggerIdent,
            'trigger' => $triggerType,
            'multiple' => true,
            'points' => $points[0]
        ]);

        $trigger_2 = $this->saveTriggerModel([
            'triggerIdent' => $triggerIdent,
            'trigger' => $triggerType,
            'points' => $points[1]
        ]);

        $trigger_3 = $this->saveTriggerModel([
            'triggerIdent' => $triggerIdent + 999,
            'trigger' => $triggerType,
            'points' => $points[2]
        ]);

        $trigger_4 = $this->saveTriggerModel([
            'points' => $points[3]
        ]);

        $module->consumerVisitSaved($consumer);

        $module->updateConsumerBalance($consumer->getConsumerId());

        $consumer = \LS\ConsumersHelper::getConsumer($consumer->getConsumerId());

        $this->assertSame(
            $trigger_1->getPoints() + $trigger_2->getPoints(),
            $module->getConsumerBalance($consumer)
        );

        do_action('ls_consumer_visited', $consumer);

        $module->updateConsumerBalance($consumer->getConsumerId());

        $consumer = \LS\ConsumersHelper::getConsumer($consumer->getConsumerId());

        $this->assertSame(
            2 * $trigger_1->getPoints() + $trigger_2->getPoints(),
            $module->getConsumerBalance($consumer)
        );

        \LS\ConsumersHelper::deleteConsumer($consumer);
    }

    public function testGform_after_submission()
    {
        $module = $this->module();

        $consumer = $this->createConsumer();

        $this->assertTrue(
            consumersLoyaltySuite::authorizeConsumer(
                $consumer->getConsumerId(),
                false,
                null,
                true
            )
        );

        $triggerIdent = random_int(101, 199);
        $triggerType = 'GFSubmit';
        $points = [
            random_int(11,19),
            random_int(21,29),
            random_int(31,39),
            random_int(41,49)
        ];

        $module->updateConsumerBalance($consumer->getConsumerId());

        $consumer = \LS\ConsumersHelper::getConsumer($consumer->getConsumerId());

        $this->assertSame(
            0,
            $module->getConsumerBalance($consumer)
        );

        $trigger_1 = $this->saveTriggerModel([
            'triggerIdent' => $triggerIdent,
            'trigger' => $triggerType,
            'points' => $points[0]
        ]);

        $trigger_2 = $this->saveTriggerModel([
            'triggerIdent' => $triggerIdent,
            'trigger' => $triggerType,
            'points' => $points[1]
        ]);

        $trigger_3 = $this->saveTriggerModel([
            'triggerIdent' => $triggerIdent,
            'points' => $points[2]
        ]);

        $trigger_4 = $this->saveTriggerModel([
            'trigger' => $triggerType,
            'points' => $points[3]
        ]);

        $form = [
            'id' => $triggerIdent
        ];

        do_action('gform_after_submission', null, $form);

        $module->updateConsumerBalance($consumer->getConsumerId());

        $consumer = \LS\ConsumersHelper::getConsumer($consumer->getConsumerId());

        $this->assertSame(
            $trigger_1->getPoints() + $trigger_2->getPoints() + $trigger_4->getPoints(),
            $module->getConsumerBalance($consumer)
        );

        \LS\ConsumersHelper::deleteConsumer($consumer);
    }

    public function testAtChallengeStarted()
    {
        $module = $this->module();

        $consumer = $this->createConsumer();

        // challenge not exist. Performing triggers testing only
        $challengeId = random_int(300, 400);

        $triggerIdent = $challengeId;
        $triggerType = 'at2';
        $points = [
            random_int(11,19),
            random_int(21,29),
            random_int(31,39),
            random_int(41,49)
        ];

        $trigger_1 = $this->saveTriggerModel([
            'triggerIdent' => $triggerIdent,
            'trigger' => $triggerType,
            'points' => $points[0]
        ]);

        $trigger_2 = $this->saveTriggerModel([
            'triggerIdent' => $triggerIdent,
            'trigger' => $triggerType,
            'points' => $points[1]
        ]);

        $trigger_3 = $this->saveTriggerModel([
            'triggerIdent' => $triggerIdent,
            'points' => $points[2]
        ]);

        $trigger_4 = $this->saveTriggerModel([
            'trigger' => $triggerType,
            'points' => $points[3]
        ]);

        $module->updateConsumerBalance($consumer->getConsumerId());

        $consumer = \LS\ConsumersHelper::getConsumer($consumer->getConsumerId());

        $this->assertSame(
            0,
            $module->getConsumerBalance($consumer)
        );

        do_action('ls_at_start_challenge', $challengeId, $consumer->getConsumerId());

        $module->updateConsumerBalance($consumer->getConsumerId());

        $consumer = \LS\ConsumersHelper::getConsumer($consumer->getConsumerId());

        $this->assertSame(
            $trigger_1->getPoints() + $trigger_2->getPoints() + $trigger_4->getPoints(),
            $module->getConsumerBalance($consumer)
        );

        \LS\ConsumersHelper::deleteConsumer($consumer);
    }

    public function testAtConsumerMadeActivity()
    {
        $module = $this->module();

        $consumer = $this->createConsumer();

        // activity not exist. Performing triggers testing only
        $activityId = random_int(300, 400);

        $triggerIdent = $activityId;
        $triggerType = 'at2activity';
        $points = [
            random_int(11,19),
            random_int(21,29),
            random_int(31,39),
            random_int(41,49)
        ];

        $trigger_1 = $this->saveTriggerModel([
            'triggerIdent' => $triggerIdent,
            'trigger' => $triggerType,
            'points' => $points[0]
        ]);

        $trigger_2 = $this->saveTriggerModel([
            'triggerIdent' => $triggerIdent,
            'trigger' => $triggerType,
            'points' => $points[1]
        ]);

        $trigger_3 = $this->saveTriggerModel([
            'triggerIdent' => $triggerIdent,
            'points' => $points[2]
        ]);

        $trigger_4 = $this->saveTriggerModel([
            'trigger' => $triggerType,
            'points' => $points[3]
        ]);

        $module->updateConsumerBalance($consumer->getConsumerId());

        $consumer = \LS\ConsumersHelper::getConsumer($consumer->getConsumerId());

        $this->assertSame(
            0,
            $module->getConsumerBalance($consumer)
        );

        do_action('ls_at_consumer_made_activity', $consumer->getConsumerId(), $activityId);

        $module->updateConsumerBalance($consumer->getConsumerId());

        $consumer = \LS\ConsumersHelper::getConsumer($consumer->getConsumerId());

        $this->assertSame(
            $trigger_1->getPoints() + $trigger_2->getPoints() + $trigger_4->getPoints(),
            $module->getConsumerBalance($consumer)
        );

        \LS\ConsumersHelper::deleteConsumer($consumer);
    }

    public function testConsumerVerified()
    {
        $module = $this->module();

        $consumer = $this->createConsumer();

        $triggerType = 'consumerVerification';
        $points = [
            random_int(11,19),
            random_int(21,29),
            random_int(31,39),
            random_int(41,49)
        ];

        $trigger_1 = $this->saveTriggerModel([
            'trigger' => $triggerType,
            'points' => $points[0]
        ]);

        $trigger_2 = $this->saveTriggerModel([
            'trigger' => $triggerType,
            'points' => $points[1]
        ]);

        $trigger_3 = $this->saveTriggerModel([
            'points' => $points[2]
        ]);

        $trigger_4 = $this->saveTriggerModel([
            'trigger' => $triggerType,
            'points' => $points[3]
        ]);

        $module->updateConsumerBalance($consumer->getConsumerId());

        $consumer = \LS\ConsumersHelper::getConsumer($consumer->getConsumerId());

        $this->assertSame(
            0,
            $module->getConsumerBalance($consumer)
        );

        do_action('ls_consumer_verified', $consumer);

        $module->updateConsumerBalance($consumer->getConsumerId());

        $consumer = \LS\ConsumersHelper::getConsumer($consumer->getConsumerId());

        $this->assertSame(
            $trigger_1->getPoints() + $trigger_2->getPoints() + $trigger_4->getPoints(),
            $module->getConsumerBalance($consumer)
        );

        do_action('ls_consumer_verified', $consumer);
        do_action('ls_consumer_verified', $consumer);
        do_action('ls_consumer_verified', $consumer);
        do_action('ls_consumer_verified', $consumer);

        $module->updateConsumerBalance($consumer->getConsumerId());

        $consumer = \LS\ConsumersHelper::getConsumer($consumer->getConsumerId());

        $this->assertSame(
            $trigger_1->getPoints() + $trigger_2->getPoints() + $trigger_4->getPoints(),
            $module->getConsumerBalance($consumer)
        );

        \LS\ConsumersHelper::deleteConsumer($consumer);
    }

    public function testConsumerDeleted()
    {
        $module = $this->module();

        $consumer = $this->createConsumer();

        \LS\BPHelper::deleteTransactions();

        $this->assertEmpty(
            \LS\BPHelper::getConsumerTransactions($consumer->getConsumerId())
        );

        $points = random_int(50, 75);

        $transactionId = $module->createTransaction(
            null,
            $consumer->getConsumerId(),
            $points,
            $this->randString(),
            time(),
            \LS\BPTransaction::SOURCE_MANUAL,
            null
        );

        $this->assertNotEmpty(
            \LS\BPHelper::getConsumerTransactions($consumer->getConsumerId())
        );

        $transaction = \LS\BPHelper::getTransaction($transactionId);

        $module->updateConsumerBalance($consumer->getConsumerId());

        $consumer = \LS\ConsumersHelper::getConsumer($consumer->getConsumerId());

        $this->assertSame(
            $transaction->getPoints(),
            $module->getConsumerBalance($consumer)
        );

        $triggerType = $this->randString();

        $trigger = $this->saveTriggerModel([
            'points' => $points,
            'trigger' => $triggerType
        ]);

        $module->performTriggers($triggerType, $consumer);

        $module->updateConsumerBalance($consumer->getConsumerId());

        $consumer = \LS\ConsumersHelper::getConsumer($consumer->getConsumerId());

        $this->assertSame(
            $transaction->getPoints() + $trigger->getPoints(),
            $module->getConsumerBalance($consumer)
        );

        do_action('ls_consumer_deleted', $consumer->getConsumerId());

        $module->updateConsumerBalance($consumer->getConsumerId());

        $consumer = \LS\ConsumersHelper::getConsumer($consumer->getConsumerId());

        $this->assertEmpty(
            \LS\BPHelper::getConsumerTransactions($consumer->getConsumerId())
        );

        $this->assertSame(
            0,
            $module->getConsumerBalance($consumer)
        );

        \LS\ConsumersHelper::deleteConsumer($consumer);
    }

    public function testComCenterInit()
    {
        $this->assertTrue(
            LS()->isModuleActive('comCenter')
        );

        $conditions = apply_filters('ls_cc_init', []);

        $this->assertNotEmpty(
            $conditions
        );

        $this->assertNotNull(
            $conditions['bp_balance']
        );

        $this->assertNotNull(
            $conditions['bp_aging']
        );

        $this->assertInstanceOf(
            LSConditionalEmailCondition::class,
            $conditions['bp_balance']
        );

        $this->assertInstanceOf(
            LSConditionalEmailCondition::class,
            $conditions['bp_aging']
        );
    }

    public function testBpRewardsCurrencies()
    {
        $this->assertTrue(
            LS()->isModuleActive('rewards')
        );

        $currencies = apply_filters('ls_rewards_currencies', LSRewardsModel::getCurrencies());

        $this->assertNotEmpty(
            $currencies['BP']
        );
    }

    public function testBpRewardsBurningMethods()
    {
        $this->assertTrue(
            LS()->isModuleActive('rewards')
        );

        $module = $this->module();

        $burningMethods = $module->getBurningMethods();

        /** @var \LS\BPMethod[] $methods */
        $methods = apply_filters('ls_bp_burning_methods', $burningMethods);

        $this->assertCount(
            count($burningMethods) + 1,
            $methods
        );

        $rewardMethod = array_pop($methods);

        $this->assertInstanceOf(
            \LS\BPMethod::class,
            $rewardMethod
        );

        $this->assertSame(
            $rewardMethod->getName(),
            \LS\BPTransaction::SOURCE_REWARD
        );

        $this->assertSame(
            $rewardMethod->getLabel(),
            __('Rewards', 'ls')
        );
    }

    public function testBpRewardsBalance()
    {
        $this->assertTrue(
            LS()->isModuleActive('rewards')
        );

        $currencies = apply_filters('ls_rewards_currencies', LSRewardsModel::getCurrencies());

        $points = random_int(31, 39);

        foreach($currencies as $currency){
            $balance = apply_filters('ls_rewards_balance', $points, $currency);

            $this->assertSame(
                $points,
                $balance
            );
        }
    }

    public function testBpRewardsIncentiveOrdered()
    {
        $this->assertTrue(
            LS()->isModuleActive('rewards')
        );

        $module = $this->module();

        $consumer = $this->createConsumer();

        $this->assertSame(
            0,
            $module->getConsumerBalance($consumer)
        );

        $burningMethods = $module->getBurningMethods();

        $methods = apply_filters('ls_bp_burning_methods', $burningMethods);

        $this->assertCount(
            count($burningMethods) + 1,
            $methods
        );

        $activeBurningMethods = $module->options()->getActiveBurningMethods();

        $optionsChanged = false;

        if(!in_array(\LS\BPTransaction::SOURCE_REWARD, $activeBurningMethods)){
            array_push($activeBurningMethods, \LS\BPTransaction::SOURCE_REWARD);
            $optionsChanged = true;
        }

        $this->assertNotEmpty(
            $activeBurningMethods
        );

        $module->options()->setOption('activeBurningMethods', $activeBurningMethods);

        $orderData = [
            'salesPrice' => random_int(4, 7),
            'incentiveId' => $this->randString(),
            'consumerId' => $consumer->getConsumerId(),
            'incentiveName' => $this->randString()
        ];

        do_action('ls_incentive_ordered', $orderData);

        $consumer = \LS\ConsumersHelper::getConsumer($consumer->getConsumerId());

        // Consumer has [-7..-4] bonus points.
        $this->assertLessThan(
            0,
            $module->getConsumerBalance($consumer)
        );

        $consumerTransactions = \LS\BPHelper::getConsumerTransactions($consumer->getConsumerId());

        $this->assertCount(
            1,
            $consumerTransactions
        );

        $transaction = array_pop($consumerTransactions);

        $this->assertInstanceOf(
            \LS\BPTransaction::class,
            $transaction
        );

        $this->assertSame(
            $orderData['salesPrice'],
            abs($transaction->getPoints())
        );

        $this->assertSame(
            $orderData['incentiveId'],
            $transaction->getIdent()
        );

        if($optionsChanged){
            array_pop($activeBurningMethods);
            $module->options()->setOption('activeBurningMethods', $activeBurningMethods);
        }

        \LS\ConsumersHelper::deleteConsumer($consumer);
    }
}