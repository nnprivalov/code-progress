<?php

define('PHP_UNIT_TEST', true);
define('WP_ADMIN', true);

require_once __DIR__ . '/../../../../../wp-load.php';

use PHPUnit\Framework\TestCase;

class bonusPointsLoyaltySuiteTestCase extends TestCase
{
    public function setUp(): void
    {
        global $wpdb;
        $wpdb->query("BEGIN");

        \LS\Library::cleanMessages();
    }

    public function tearDown(): void
    {
        global $wpdb;
        $wpdb->query("ROLLBACK");

        \LS\Library::cleanMessages();
    }

    public function module()
    {
        /** @var \LS\bonusPointsAdmin $module */
        $module = LS()->getModule('bonusPoints');

        return $module;
    }

    /**
     * @return string
     */
    protected function randString()
    {
        return md5(uniqid(random_int(0, 100), true));
    }

    public function saveTriggerModel($params=[])
    {
        $data = [
            'name'  => $this->randString(),
            'trigger' => $this->randString(),
            'triggerIdent' => 0,
            'points' => random_int(1, 20),
            'message' => $this->randString(),
            'consumerAlert' => $this->randString(),
            'active' => 1,
            'createDate' => time()
        ];

        if(!empty($params)){
            foreach($params as $param => $value){
                $data[$param] = $value;
            }
        }

        $triggerModel = new \LS\BPTriggerModel($data);

        $triggerId = \LS\BPHelper::saveTriggerModel($triggerModel);

        $this->assertGreaterThan(
            0,
            $triggerId
        );

        $triggerModel->setId($triggerId);

        $this->assertSame(
            $triggerId,
            $triggerModel->getTriggerId()
        );

        return $triggerModel;
    }

    public function createConsumer($params=[])
    {
        $data = [
            'active' => 1
        ];

        if(!empty($params)){
            foreach($params as $param => $value){
                $data[$param] = $value;
            }
        }

        $consumer = new \LS\Consumer($data);

        $this->assertGreaterThan(
            0,
            \LS\ConsumersHelper::createConsumer($consumer)
        );

        $this->assertNotEmpty($consumer->getConsumerId());

        return $consumer;
    }

    public function saveTransaction($params=[])
    {
        $data = [
            'source' => \LS\BPTransaction::SOURCE_MANUAL,
            'createDate' => time()
        ];

        if(!empty($params)){
            foreach($params as $param => $value){
                $data[$param] = $value;
            }
        }

        $transaction = new \LS\BPTransaction($data);

        $transactionId = \LS\BPHelper::saveTransaction($transaction);

        $this->assertGreaterThan(
            0,
            $transactionId
        );

        $transaction->set('transactionId', $transactionId);

        return $transaction;
    }

    public function saveImport($params=[])
    {
        $data = [
            'importDate' => time(),
            'fileName' => $this->randString() . '.csv'
        ];

        if(!empty($params)){
            foreach($params as $param => $value){
                $data[$param] = $value;
            }
        }

        $import = new \LS\BPImport($data);

        $importId = \LS\BPHelper::saveImport($import);


        $this->assertGreaterThan(
            0,
            $importId
        );

        $filePath = \LS\BPHelper::getPathToFile($import->getFileName());

        $this->assertFileNotExists(
            $filePath
        );

        $import->set('importId', $importId);

        $this->assertSame(
            $import->getImportId(),
            $importId
        );

        return $import;
    }

    public function invokeMethod($className, $methodName, $params=[])
    {
        $reflection = new \ReflectionClass($className);
        $method = $reflection->getMethod($methodName);
        $method->setAccessible(true);

        return $method->invokeArgs(new $className(), $params);
    }

    public function invokeObjectMethod($object, $methodName, $params=[])
    {
        $reflection = new \ReflectionObject($object);
        $method = $reflection->getMethod($methodName);
        $method->setAccessible(true);

        return $method->invokeArgs($object, $params);
    }

}