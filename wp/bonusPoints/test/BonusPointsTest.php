<?php

require_once __DIR__ . '/bonusPointsTestCase.php';

class bonusPointsLoyaltySuiteTestBonusPoints extends bonusPointsTestCase
{
    public function setUp(): void
    {
        parent::setUp();

        $this->helper()->deleteTriggerModels();
        $this->helper()->deleteTransactions();
    }

    public function testHelper()
    {
        $this->assertInstanceOf(
            \LS\BPHelper::class,
            $this->helper()
        );
    }

    public function testSubmitBonusTriggerButton()
    {
        $module = $this->module();
        $consumer = $this->createConsumer();

        $this->assertEmpty(
            $this->helper()->getConsumerTriggers($consumer->getConsumerId())
        );

        $this->assertEmpty(
            $this->helper()->getConsumerTransactions($consumer->getConsumerId())
        );

        $trigger = $this->saveTriggerModel([
            'trigger' => 'button'
        ]);

        $this->invokeObjectMethod(
            $module,
            'submitBonusTriggerButton',
            [$consumer, $trigger->getTriggerId()]
        );

        $this->assertNotEmpty(
            $this->helper()->getConsumerTriggers($consumer->getConsumerId())
        );

        $this->assertNotEmpty(
            $this->helper()->getConsumerTransactions($consumer->getConsumerId())
        );
    }

    public function testGetConsumerBalance()
    {
        $module = $this->module();
        $consumer = $this->createConsumer();

        $this->assertSame(
            0,
            $module->getConsumerBalance($consumer)
        );

        $points = random_int(1, 20);

        $module->createTransaction(
            $this->randString(),
            $consumer->getConsumerId(),
            $points,
            $this->randString(),
            time(),
            \LS\BPTransaction::SOURCE_MANUAL
        );

        $consumer = \LS\ConsumersHelper::getConsumer($consumer->getConsumerId());

        $this->assertSame(
            $points,
            $module->getConsumerBalance($consumer)
        );
    }

    public function testUpdateConsumerBalance()
    {
        $module = $this->module();
        $consumer = $this->createConsumer();
        $points = random_int(1, 20);

        $this->assertSame(
            0,
            $module->getConsumerBalance($consumer)
        );

        $this->saveTransaction([
            'consumerId' => $consumer->getConsumerId(),
            'points'     => $points
        ]);

        $module->updateConsumerBalance($consumer->getConsumerId());

        $consumer = \LS\ConsumersHelper::getConsumer($consumer->getConsumerId());

        $this->assertSame(
            $points,
            $module->getConsumerBalance($consumer)
        );
    }

    public function testCalculateAgingDateTimestamp()
    {
        $module = $this->module();
        $createDate = time();

        $module->setOption('agingDays', 7);

        $this->assertGreaterThan(
            0,
            $this->invokeObjectMethod(
                $module,
                'calculateAgingDateTimestamp',
                [$createDate]
            )
        );

        $this->assertSame(
            0,
            $this->invokeObjectMethod(
                $module,
                'calculateAgingDateTimestamp',
                [strtotime('-8 Days', $createDate)]
            )
        );
    }

    public function testCreateTransaction()
    {
        $module = $this->module();
        $consumer = $this->createConsumer();

        $this->assertEmpty(
            $this->helper()->getConsumerTransactions($consumer->getConsumerId())
        );

        $points = random_int(88, 99);

        $message = $this->randString();

        $transactionId = $module->createTransaction(
            null,
            $consumer->getConsumerId(),
            $points,
            $message,
            time(),
            \LS\BPTransaction::SOURCE_MANUAL,
            null
        );

        $consumerTransactions = $this->helper()->getConsumerTransactions($consumer->getConsumerId());

        $this->assertNotEmpty($consumerTransactions);

        /** @var \LS\BPTransaction $transaction */
        $transaction = array_pop($consumerTransactions);

        $this->assertSame(
            $transactionId,
            $transaction->getTransactionId()
        );

        $this->assertSame(
            $message,
            $transaction->getMessage()
        );
    }

    public function testPerformSingleTrigger()
    {
        $module = $this->module();
        $consumer = $this->createConsumer();
        $trigger = $this->createTrigger(['active' => 0]);

        $this->assertFalse(
            $this->module()->performSingleTrigger($trigger, $consumer)
        );

        $trigger = $this->createTrigger(['startDate' => strtotime('TOMORROW')]);

        $this->assertFalse(
            $this->module()->performSingleTrigger($trigger, $consumer)
        );

        $trigger = $this->createTrigger(['endDate' => strtotime('YESTERDAY')]);

        $this->assertFalse(
            $this->module()->performSingleTrigger($trigger, $consumer)
        );

        $trigger = $this->createTrigger([
            'startDate' => strtotime('TODAY'),
            'endDate'   => strtotime('TODAY')
        ]);

        $this->assertTrue(
            $this->module()->performSingleTrigger($trigger, $consumer)
        );

        $triggerIdent = time();
        $trigger = $this->createTrigger(['triggerIdent' => $triggerIdent]);

        $this->assertFalse(
            $this->module()->performSingleTrigger($trigger, $consumer)
        );

        $this->assertTrue(
            $this->module()->performSingleTrigger($trigger, $consumer, $triggerIdent)
        );

        $points = 100;
        $consumer = $this->createConsumer();
        $trigger = $this->createTrigger();

        $this->assertTrue(
            $this->module()->performSingleTrigger($trigger, $consumer, 0, $points)
        );

        $transactions = $this->helper()->getConsumerTransactions($consumer->getConsumerId());
        $lastTransaction = end($transactions);

        $this->assertSame(
            $points,
            $lastTransaction->getPoints()
        );

        $this->assertFalse(
            $this->module()->performSingleTrigger($trigger, $consumer, 0, 0)
        );

        $points = 100;
        $consumer = $this->createConsumer();
        $trigger = $this->createTrigger(['points' => $points]);

        $this->assertTrue(
            $this->module()->performSingleTrigger($trigger, $consumer)
        );

        $transactions = $this->helper()->getConsumerTransactions($consumer->getConsumerId());
        $lastTransaction = end($transactions);

        $this->assertSame(
            $points,
            $lastTransaction->getPoints()
        );

        $consumer = $this->createConsumer();
        $module->updateConsumerBalance($consumer->getConsumerId());

        $consumer = \LS\ConsumersHelper::getConsumer($consumer->getConsumerId());

        $this->assertEmpty(
            $module->getConsumerBalance($consumer)
        );

        $this->assertTrue(
            $this->module()->performSingleTrigger($trigger, $consumer)
        );

        $module->updateConsumerBalance($consumer->getConsumerId());

        $consumer = \LS\ConsumersHelper::getConsumer($consumer->getConsumerId());

        $this->assertSame(
            $trigger->getModel()->getPoints(),
            $module->getConsumerBalance($consumer)
        );

        $this->assertTrue(
            $this->helper()->isConsumerPerformedTrigger($consumer, $trigger)
        );
    }

    public function testPerformTriggers()
    {
        $module = $this->module();

        $points = [
            random_int(11, 19),
            random_int(21, 29),
            random_int(31, 39),
            random_int(41, 49)
        ];

        $trigger_1 = $this->createTrigger([
            'trigger'      => 'consumersVisits',
            'triggerIdent' => 1,
            'points'       => $points[0]
        ]);

        $trigger_2 = $this->createTrigger([
            'trigger'      => 'consumersVisits',
            'triggerIdent' => 2,
            'points'       => $points[1]
        ]);

        $trigger_3 = $this->createTrigger([
            'trigger'      => 'consumersVisits',
            'triggerIdent' => 3,
            'points'       => $points[2]
        ]);

        $trigger_4 = $this->createTrigger([
            'trigger'      => 'consumersVisits',
            'triggerIdent' => 4,
            'points'       => $points[3]
        ]);

        $consumer = $this->createConsumer();

        $module->updateConsumerBalance($consumer->getConsumerId());

        $consumer = \LS\ConsumersHelper::getConsumer($consumer->getConsumerId());

        $this->assertSame(
            0,
            $module->getConsumerBalance($consumer)
        );

        $this->assertSame(
            3,
            $module->performTriggers('consumersVisits', $consumer, 3)
        );

        $this->assertTrue(
            $this->helper()->isConsumerPerformedTrigger($consumer, $trigger_1)
        );

        $this->assertTrue(
            $this->helper()->isConsumerPerformedTrigger($consumer, $trigger_2)
        );

        $this->assertTrue(
            $this->helper()->isConsumerPerformedTrigger($consumer, $trigger_3)
        );

        $this->assertFalse(
            $this->helper()->isConsumerPerformedTrigger($consumer, $trigger_4)
        );

        $module->updateConsumerBalance($consumer->getConsumerId());

        $consumer = \LS\ConsumersHelper::getConsumer($consumer->getConsumerId());

        $expectedConsumerPoints =
            $trigger_1->getModel()->getPoints()
            + $trigger_2->getModel()->getPoints()
            + $trigger_3->getModel()->getPoints();

        $this->assertSame(
            $expectedConsumerPoints,
            $module->getConsumerBalance($consumer)
        );

        $consumer = $this->createConsumer();
        $trigger = $this->createTrigger(['trigger' => 'button']);

        $this->assertFalse(
            $this->helper()->isConsumerPerformedTrigger($consumer, $trigger)
        );

        $this->assertEmpty(
            $module->performTriggers('button', $consumer)
        );

        $this->assertSame(
            $trigger->getModel()->getTriggerId(),
            $trigger->getModel()->getTriggerIdent()
        );

        $this->assertSame(
            1,
            $module->performTriggers('button', $consumer, $trigger->getModel()->getTriggerIdent())
        );

        $this->assertTrue(
            $this->helper()->isConsumerPerformedTrigger($consumer, $trigger)
        );
    }

    public function testConsumerVisitSaved()
    {
        if (!LS()->isModuleActive('consumersVisits')) {
            $this->assertTrue(true);
            return;
        }

        $module = $this->module();
        $consumer = $this->createConsumer();

        $triggerType = 'consumersVisits';
        $points = [
            random_int(11, 19),
            random_int(21, 29)
        ];

        $trigger_1 = $this->saveTriggerModel([
            'triggerIdent' => 1,
            'trigger'      => $triggerType,
            'points'       => $points[0]
        ]);

        $trigger_2 = $this->saveTriggerModel([
            'triggerIdent' => 2,
            'trigger'      => $triggerType,
            'points'       => $points[1]
        ]);

        $this->assertSame(
            0,
            $module->getConsumerBalance($consumer)
        );

        consumersVisits::saveConsumerVisit($consumer, strtotime('TODAY'));

        $consumer = \LS\ConsumersHelper::getConsumer($consumer->getConsumerId());

        $this->assertSame(
            $trigger_1->getPoints(),
            $module->getConsumerBalance($consumer)
        );

        consumersVisits::saveConsumerVisit($consumer, strtotime('TODAY -1 DAY'));

        $consumer = \LS\ConsumersHelper::getConsumer($consumer->getConsumerId());

        $this->assertSame(
            $trigger_1->getPoints() + $trigger_2->getPoints(),
            $module->getConsumerBalance($consumer)
        );

        consumersVisits::saveConsumerVisit($consumer, strtotime('TODAY -2 DAYS'));

        $consumer = \LS\ConsumersHelper::getConsumer($consumer->getConsumerId());

        $this->assertSame(
            $trigger_1->getPoints() + $trigger_2->getPoints(),
            $module->getConsumerBalance($consumer)
        );
    }

    public function testGform_after_submission()
    {
        if (!class_exists('RGFormsModel')) {
            $this->assertTrue(true);
            return;
        }

        $module = $this->module();
        $consumer = $this->createConsumer();

        $triggerIdent = random_int(101, 199);
        $triggerType = 'GFSubmit';
        $points = [
            random_int(11, 19),
            random_int(21, 29),
            random_int(31, 39)
        ];

        $consumer = \LS\ConsumersHelper::getConsumer($consumer->getConsumerId());

        $this->assertTrue(
            consumersLoyaltySuite::authorizeConsumer(
                $consumer->getConsumerId(),
                false,
                null,
                true
            )
        );

        $this->assertSame(
            0,
            $module->getConsumerBalance($consumer)
        );

        $trigger_1 = $this->saveTriggerModel([
            'triggerIdent' => $triggerIdent,
            'trigger'      => $triggerType,
            'points'       => $points[0]
        ]);

        $trigger_2 = $this->saveTriggerModel([
            'triggerIdent' => $triggerIdent,
            'trigger'      => $triggerType,
            'points'       => $points[1]
        ]);

        $trigger_3 = $this->saveTriggerModel([
            'trigger' => $triggerType,
            'points'  => $points[3]
        ]);

        $form = [
            'id' => $triggerIdent
        ];

        do_action('gform_after_submission', null, $form);

        $consumer = \LS\ConsumersHelper::getConsumer($consumer->getConsumerId());

        $this->assertSame(
            $trigger_1->getPoints() + $trigger_2->getPoints() + $trigger_3->getPoints(),
            $module->getConsumerBalance($consumer)
        );
    }

    public function testAtChallengeStarted()
    {
        $module = $this->module();
        $consumer = $this->createConsumer();

        // challenge not exist. Performing triggers testing only
        $challengeId = random_int(300, 400);

        $triggerIdent = $challengeId;
        $triggerType = 'at2';
        $points = [
            random_int(11, 19),
            random_int(21, 29),
            random_int(31, 39),
            random_int(41, 49)
        ];

        $trigger_1 = $this->saveTriggerModel([
            'triggerIdent' => $triggerIdent,
            'trigger'      => $triggerType,
            'points'       => $points[0]
        ]);

        $trigger_2 = $this->saveTriggerModel([
            'triggerIdent' => $triggerIdent,
            'trigger'      => $triggerType,
            'points'       => $points[1]
        ]);

        $trigger_3 = $this->saveTriggerModel([
            'trigger' => $triggerType,
            'points'  => $points[3]
        ]);

        $module->updateConsumerBalance($consumer->getConsumerId());

        $consumer = \LS\ConsumersHelper::getConsumer($consumer->getConsumerId());

        $this->assertSame(
            0,
            $module->getConsumerBalance($consumer)
        );

        do_action('ls_at_start_challenge', $challengeId, $consumer->getConsumerId());

        $module->updateConsumerBalance($consumer->getConsumerId());

        $consumer = \LS\ConsumersHelper::getConsumer($consumer->getConsumerId());

        $this->assertSame(
            $trigger_1->getPoints() + $trigger_2->getPoints() + $trigger_3->getPoints(),
            $module->getConsumerBalance($consumer)
        );
    }

    public function testAtConsumerMadeActivity()
    {
        $module = $this->module();
        $consumer = $this->createConsumer();

        // activity not exist. Performing triggers testing only
        $activityId = random_int(300, 400);

        $triggerIdent = $activityId;
        $triggerType = 'at2activity';
        $points = [
            random_int(11, 19),
            random_int(21, 29),
            random_int(31, 39),
            random_int(41, 49)
        ];

        $trigger_1 = $this->saveTriggerModel([
            'triggerIdent' => $triggerIdent,
            'trigger'      => $triggerType,
            'points'       => $points[0]
        ]);

        $trigger_2 = $this->saveTriggerModel([
            'triggerIdent' => $triggerIdent,
            'trigger'      => $triggerType,
            'points'       => $points[1]
        ]);

        $trigger_3 = $this->saveTriggerModel([
            'trigger' => $triggerType,
            'points'  => $points[3]
        ]);

        $module->updateConsumerBalance($consumer->getConsumerId());

        $consumer = \LS\ConsumersHelper::getConsumer($consumer->getConsumerId());

        $this->assertSame(
            0,
            $module->getConsumerBalance($consumer)
        );

        do_action('ls_at_consumer_made_activity', $consumer->getConsumerId(), $activityId);

        $module->updateConsumerBalance($consumer->getConsumerId());

        $consumer = \LS\ConsumersHelper::getConsumer($consumer->getConsumerId());

        $this->assertSame(
            $trigger_1->getPoints() + $trigger_2->getPoints() + $trigger_3->getPoints(),
            $module->getConsumerBalance($consumer)
        );
    }

    public function testConsumerVerified()
    {
        $module = $this->module();
        $consumer = $this->createConsumer();
        $triggerType = 'consumerVerification';
        $points = [
            random_int(11, 19),
            random_int(21, 29),
            random_int(31, 39),
            random_int(41, 49)
        ];

        $trigger_1 = $this->saveTriggerModel([
            'trigger' => $triggerType,
            'points'  => $points[0]
        ]);

        $trigger_2 = $this->saveTriggerModel([
            'trigger' => $triggerType,
            'points'  => $points[1]
        ]);

        $trigger_3 = $this->saveTriggerModel([
            'trigger' => $triggerType,
            'points'  => $points[3]
        ]);

        $module->updateConsumerBalance($consumer->getConsumerId());

        $consumer = \LS\ConsumersHelper::getConsumer($consumer->getConsumerId());

        $this->assertSame(
            0,
            $module->getConsumerBalance($consumer)
        );

        do_action('ls_consumer_verified', $consumer);

        $module->updateConsumerBalance($consumer->getConsumerId());

        $consumer = \LS\ConsumersHelper::getConsumer($consumer->getConsumerId());

        $this->assertSame(
            $trigger_1->getPoints() + $trigger_2->getPoints() + $trigger_3->getPoints(),
            $module->getConsumerBalance($consumer)
        );

        do_action('ls_consumer_verified', $consumer);
        do_action('ls_consumer_verified', $consumer);
        do_action('ls_consumer_verified', $consumer);
        do_action('ls_consumer_verified', $consumer);

        $module->updateConsumerBalance($consumer->getConsumerId());

        $consumer = \LS\ConsumersHelper::getConsumer($consumer->getConsumerId());

        $this->assertSame(
            $trigger_1->getPoints() + $trigger_2->getPoints() + $trigger_3->getPoints(),
            $module->getConsumerBalance($consumer)
        );
    }

    public function testConsumerDeleted()
    {
        $module = $this->module();
        $consumer = $this->createConsumer();

        $module->createTransaction(
            null,
            $consumer->getConsumerId(),
            random_int(50, 75),
            $this->randString(),
            time(),
            \LS\BPTransaction::SOURCE_MANUAL,
            null
        );

        $this->assertNotEmpty(
            $this->helper()->getConsumerTransactions($consumer->getConsumerId())
        );

        $this->module()->consumerDeleted($consumer->getConsumerId());

        $this->assertEmpty(
            $this->helper()->getConsumerTransactions($consumer->getConsumerId())
        );
    }

    public function testComCenterInit()
    {
        $this->assertTrue(
            LS()->isModuleActive('comCenter')
        );

        $conditions = apply_filters('ls_cc_init', []);

        $this->assertNotEmpty(
            $conditions
        );

        $this->assertNotNull(
            $conditions['bp_balance']
        );

        $this->assertNotNull(
            $conditions['bp_aging']
        );

        $this->assertInstanceOf(
            LSConditionalEmailCondition::class,
            $conditions['bp_balance']
        );

        $this->assertInstanceOf(
            LSConditionalEmailCondition::class,
            $conditions['bp_aging']
        );
    }

    public function testBpRewardsCurrencies()
    {
        if (!LS()->isModuleActive('rewards')) {
            $this->assertTrue(true);
            return;
        }

        $currencies = apply_filters('ls_rewards_currencies', LSRewardsModel::getCurrencies());

        $this->assertNotEmpty($currencies['BP']);
    }

    public function testBpRewardsBurningMethods()
    {
        if (!LS()->isModuleActive('rewards')) {
            $this->assertTrue(true);
            return;
        }

        $module = $this->module();
        $burningMethods = $module->getBurningMethods();

        /** @var \LS\BPMethod[] $methods */
        $methods = apply_filters('ls_bp_burning_methods', $burningMethods);

        $this->assertCount(
            count($burningMethods) + 1,
            $methods
        );

        $rewardMethod = array_pop($methods);

        $this->assertInstanceOf(
            \LS\BPMethod::class,
            $rewardMethod
        );

        $this->assertSame(
            $rewardMethod->getName(),
            \LS\BPTransaction::SOURCE_REWARD
        );

        $this->assertSame(
            $rewardMethod->getLabel(),
            __('Rewards', 'ls')
        );
    }

    public function testBpRewardsBalance()
    {
        if (!LS()->isModuleActive('rewards')) {
            $this->assertTrue(true);
            return;
        }

        $currencies = apply_filters('ls_rewards_currencies', LSRewardsModel::getCurrencies());
        $points = random_int(31, 39);

        foreach ($currencies as $currency) {

            $balance = apply_filters('ls_rewards_balance', $points, $currency);

            $this->assertSame(
                $points,
                $balance
            );
        }
    }

    public function testBpRewardsIncentiveOrdered()
    {
        if (!LS()->isModuleActive('rewards')) {
            $this->assertTrue(true);
            return;
        }

        $module = $this->module();
        $consumer = $this->createConsumer();

        $this->assertSame(
            0,
            $module->getConsumerBalance($consumer)
        );

        $burningMethods = $module->getBurningMethods();
        $methods = apply_filters('ls_bp_burning_methods', $burningMethods);

        $this->assertCount(
            count($burningMethods) + 1,
            $methods
        );

        $activeBurningMethods = $module->getActiveBurningMethods();

        $optionsChanged = false;

        if (!in_array(\LS\BPTransaction::SOURCE_REWARD, $activeBurningMethods)) {
            $activeBurningMethods[] = \LS\BPTransaction::SOURCE_REWARD;
            $optionsChanged = true;
        }

        $this->assertNotEmpty(
            $activeBurningMethods
        );

        $module->setOption('activeBurningMethods', $activeBurningMethods);

        $orderData = [
            'salesPrice'    => random_int(4, 7),
            'incentiveId'   => $this->randString(),
            'consumerId'    => $consumer->getConsumerId(),
            'incentiveName' => $this->randString()
        ];

        do_action('ls_incentive_ordered', $orderData);

        $consumer = \LS\ConsumersHelper::getConsumer($consumer->getConsumerId());

        // Consumer has [-7..-4] bonus points.
        $this->assertLessThan(
            0,
            $module->getConsumerBalance($consumer)
        );

        $consumerTransactions = $this->helper()->getConsumerTransactions($consumer->getConsumerId());

        $this->assertCount(
            1,
            $consumerTransactions
        );

        $transaction = array_pop($consumerTransactions);

        $this->assertInstanceOf(
            \LS\BPTransaction::class,
            $transaction
        );

        $this->assertSame(
            $orderData['salesPrice'],
            abs($transaction->getPoints())
        );

        $this->assertSame(
            $orderData['incentiveId'],
            $transaction->getIdent()
        );

        if ($optionsChanged) {
            array_pop($activeBurningMethods);
            $module->setOption('activeBurningMethods', $activeBurningMethods);
        }
    }
}