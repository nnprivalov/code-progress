<?php
namespace LS;

require_once __DIR__ . '/BPOptions.php';

/**
 * @module bonusPoints
 * @title Bonus Points
 * @description Bonus Points module
 * @version 1.5.0
 * @priority 14
 * @depends consumersLoyaltySuite 3.5.5
 */
class bonusPoints extends Module
{
    use BPOptions;

    /** @var \consumersLoyaltySuite */
    protected $consumersModule;

    /** @var BPHelper */
    protected $helper;

    /**
     * @since 1.0.12
     * @return string
     */
    public function getVersion(): string
    {
        return '1.5.0';
    }

    /**
     * @since 1.0.12
     * @return array
     */
    public function setDepends(): array
    {
        return [
            'consumersLoyaltySuite' => '3.5.5'
        ];
    }

    /**
     * Retrieve module title
     * @since 1.0.4
     * @return string Module title
     */
    public function getTitle(): string
    {
        return __('Bonus Points', 'ls');
    }

    /**
     * Retrieve module description
     * @since 1.0.4
     * @return string Module description
     */
    public function getDescription(): string
    {
        return __('Bonus Points module', 'ls');
    }

    /**
     * @since 1.0.12
     * @return string
     */
    public function getCapability(): string
    {
        return 'ls_bp';
    }

    /**
     * Get helper
     * @since 1.5.0
     * @return BPHelper
     */
    public function helper(): BPHelper
    {
        return $this->helper;
    }

    /**
     * Main function to load models, create actions and filters, init variables
     * @since 1.0.0
     */
    public function load()
    {
        require_once __DIR__ . '/BPTriggerModel.php';
        require_once __DIR__ . '/BPTriggerInterface.php';
        require_once __DIR__ . '/BPTrigger.php';
        require_once __DIR__ . '/BPConsumerTrigger.php';
        require_once __DIR__ . '/BPTransaction.php';
        require_once __DIR__ . '/BPImport.php';
        require_once __DIR__ . '/BPMethod.php';
        require_once __DIR__ . '/BPImportHandler.php';
        require_once __DIR__ . '/BPHelper.php';

        $this->consumersModule = LS()->getModule('consumers');
        $this->helper = new BPHelper();

        add_action('init', [$this, 'init']);

        // Connect to consumers
        add_filter('consumerDynamicFields', [$this, 'consumerDynamicFields']);
        add_filter('consumerFixedFieldsList', [$this, 'consumerFixedFieldsList'], 10, 2);
        add_action('ls_consumer_verified', [$this, 'consumerVerified']);
        add_action('ls_consumer_deleted', [$this, 'consumerDeleted']);

        // Connect to consumers visits
        add_action('ls_consumer_visited', [$this, 'consumerVisitSaved']);

        // Connect to Gravity Forms
        add_action("gform_after_submission", [$this, 'gform_after_submission'], 10, 2);

        // Connect to Activity Tracking
        add_action('ls_at_start_challenge', [$this, 'atChallengeStarted'], 10, 2);
        add_action('ls_at_consumer_made_activity', [$this, 'atConsumerMadeActivity'], 10, 2);

        // Connect to Com.Center
        add_filter('ls_cc_init', [$this, 'comCenterInit']);

        // Connect to Rewards
        if (LS()->isModuleActive('rewards')) {
            add_filter('ls_rewards_currencies', [$this, 'bpRewardsCurrencies']);
            add_filter('ls_bp_burning_methods', [$this, 'bpRewardsBurningMethods']);
            add_filter('ls_rewards_balance', [$this, 'bpRewardsBalance'], 10, 2);
            add_action('ls_incentive_ordered', [$this, 'bpRewardsIncentiveOrdered']);
        }
    }

    /**
     * Create shortcodes
     * @since 1.0.0
     */
    public function initShortcodes()
    {
        add_shortcode('bonus-points', [$this, 'bonusPointsShortcode']);
        add_shortcode('bonus-points-overview', [$this, 'bonusPointsOverviewShortcode']);
        add_shortcode('bonus-points-trigger', [$this, 'bonusPointsTriggerShortcode']);
    }

    /**
     * @since 1.1.0
     * @internal
     */
    public function init()
    {
        if (!$this->hasAction()) {
            return;
        }

        // Submit button in [bonus-points-trigger]
        if ($this->isAction('bp-trigger')) {

            $nonce = $this->getParam('nonce', 'text');

            if ($this->consumersModule->isConsumerAuthorized() && wp_verify_nonce($nonce, 'bp-trigger')) {
                $this->submitBonusTriggerButton(
                    $this->consumersModule->getAuthorizedConsumer(),
                    $this->getParam('id', 'id')
                );
            }

            $this->redirect(remove_query_arg(['action', 'id', 'nonce']));
        }
    }

    /**
     * Shortcode [bonus-points]
     * Display total number of bonus points
     *
     * @since 1.0.0
     * @internal
     *
     * @param array $atts Attributes
     * @return int Points value
     */
    public function bonusPointsShortcode($atts): int
    {
        $consumer = $this->consumersModule->getConsumer(
            $atts['consumer_id'] ?? null
        );

        if (!$consumer) {
            return 0;
        }

        return $this->getConsumerBalance($consumer);
    }

    /**
     * Shortcode [bonus-points-overview]
     * Display transactions overview table
     *
     * @since 1.0.0
     * @internal
     *
     * @param array $atts Attributes
     * @return string HTML
     */
    public function bonusPointsOverviewShortcode($atts): string
    {
        $consumerId = isset($atts['consumer_id'])
            ? (int) $atts['consumer_id']
            : $this->consumersModule->getAuthorizedConsumerId();

        return (new Template(__DIR__ . '/views/frontend'))
            ->assign('label', $this->getPluralPointsLabel())
            ->assign('transactions', $this->helper->getConsumerTransactions($consumerId))
            ->render('consumer-transactions-overview.phtml', false);
    }

    /**
     * Shortcode [bonus-points-trigger]
     * Display button to perform the trigger
     *
     * @since 1.1.0
     * @internal
     *
     * @param array $atts Attributes
     * @param string $content Button text
     * @return string HTML
     */
    public function bonusPointsTriggerShortcode($atts, $content): string
    {
        $consumer = $this->consumersModule->getAuthorizedConsumer();
        $triggerId = isset($atts['id']) ? (int) $atts['id'] : 0;
        $triggerModel = $this->helper->getTriggerModel($triggerId);
        $trigger = $triggerModel ? $this->helper->getTriggerFromModel($triggerModel) : false;

        if (!$trigger) {
            return current_user_can('edit_users') ? 'Admin warning: shortcode [bonus-points-trigger] -> "id" is not specified or not found' : '';
        }

        if (empty($content)) {
            return current_user_can('edit_users') ? 'Admin warning: shortcode [bonus-points-trigger] -> button text is empty' : '';
        }

        if (!$trigger->isActual()) {
            return '';
        }

        if (!$this->helper->canConsumerPerformTrigger($consumer, $trigger)) {
            return '';
        }

        $class = isset($atts['class']) ? esc_attr($atts['class']) : '';
        $class .= ' button normal-button large';

        $href = $atts['href'] ?? false;
        $href = wp_nonce_url(add_query_arg(['action' => 'bp-trigger', 'id' => $triggerId], $href), 'bp-trigger', 'nonce');

        return '<a href="' . esc_url($href) . '" class="' . trim($class) . '">' . $content . '</a>';
    }

    /**
     * Append module fields to consumer fields
     *
     * @since 1.0.0
     * @internal
     *
     * @param FieldAbstract[] $fields A list of consumer dynamic fields
     * @return FieldAbstract[] A list of consumer dynamic fields
     */
    public function consumerDynamicFields(array $fields): array
    {
        $fields['bpBalance'] = new Field\Value('bpBalance', __('BP: Balance', 'ls'));
        $fields['bpStatusSilver'] = new Field\Checkbox('bpStatusSilver', __('BP: Silver Status', 'ls'));
        $fields['bpStatusSilverOver'] = new Field\Date('bpStatusSilverOver', __('BP: Silver Status ends', 'ls'));

        return $fields;
    }

    /**
     * Always display this field for registration and consumer-add forms
     *
     * @since 2.0.0
     * @internal
     *
     * @param FieldAbstract[] $fields A list of active consumer fields
     * @param string $formName Form name
     * @return FieldAbstract[] A list of active consumer fields
     */
    public function consumerFixedFieldsList(array $fields, string $formName): array
    {
        if (in_array($formName, ['adminAdd', 'adminEdit'])) {
            $fields = array_merge($fields, ['bpBalance', 'bpStatusSilver', 'bpStatusSilverOver']);
        }

        return $fields;
    }

    /**
     * Submit trigger-button by consumer
     *
     * @since 1.4.0
     *
     * @param ConsumerInterface $consumer Consumer who submitted the button
     * @param int $triggerId Trigger Id
     */
    private function submitBonusTriggerButton(ConsumerInterface $consumer, int $triggerId)
    {
        $trigger = $this->helper->getTriggerModel($triggerId);

        if ($trigger && $trigger->getTriggerName() == 'button') {
            $this->performTriggers(
                $trigger->getTriggerName(),
                $consumer,
                $trigger->getTriggerId()
            );
        }
    }

    /**
     * Retrieve consumer balance
     *
     * @since 1.1.0
     *
     * @param ConsumerInterface $consumer
     * @return int
     */
    public function getConsumerBalance($consumer): int
    {
        if (!($consumer instanceof ConsumerInterface)) {
            _doing_it_wrong(__FUNCTION__, '1st parameter must be instance of ConsumerInterface', '1.4.2');
            $consumer = \consumersLoyaltySuite::getAuthorizedConsumer();
        }

        return $consumer->getDynamicValue('bpBalance', 0, true);
    }

    /**
     * Check if consumer status is silver
     *
     * @since 1.1.2
     *
     * @param ConsumerInterface $consumer
     * @return bool
     */
    public function isSilverStatus(ConsumerInterface $consumer): bool
    {
        return (bool) $consumer->getDynamicValue('bpStatusSilver', false);
    }

    /**
     * Check if earning method is activated
     *
     * @since 1.0.0
     *
     * @param string $method Earning method name
     * @return bool True if earning method is activated
     */
    public function isEarningMethodActivated(string $method): bool
    {
        return in_array($method, $this->getActiveEarningMethods());
    }

    /**
     * Check if burning method is activated
     *
     * @since 1.0.0
     *
     * @param string $method Burning method name
     * @return bool True if burning method is activated
     */
    public function isBurningMethodActivated(string $method): bool
    {
        return in_array($method, $this->getActiveBurningMethods());
    }

    /**
     * Update consumer balance on transaction has been added/updated/deleted
     * @since 1.0.0
     * @param int $consumerId Consumer ID
     */
    public function updateConsumerBalance(int $consumerId)
    {
        $consumer = $this->consumersModule->getConsumer($consumerId);

        if (!$consumer) {
            return;
        }

        $newBalance = $this->helper->calculateConsumerBalance($consumerId);

        ConsumersHelper::updateConsumer(
            $consumer->addDynamicValue('bpBalance', $newBalance)
        );

        ConsumersHelper::insertLog('bpBalance', $consumerId);

        do_action('ls_bp_balance_updated', $consumerId);
    }

    /**
     * Calculate aging date timestamp
     *
     * @since 1.4.3
     *
     * @param int $createDate Transaction create date as timestamp
     * @return int Timestamp of the aging date
     */
    private function calculateAgingDateTimestamp(int $createDate): int
    {
        $agingDate = $createDate + $this->getAgingDays() * DAY_IN_SECONDS;

        if (time() > $agingDate) {
            $agingDate = 0;
        }

        return $agingDate;
    }

    /**
     * Retrieve registered triggers grouped by type
     * @since 1.5.0
     * @return BPTrigger[] Triggers
     */
    public function getRegisteredTriggers()
    {
        $emptyTriggerModel = new BPTriggerModel();

        return array_map(static function (string $triggerName) use ($emptyTriggerModel) {
            return new $triggerName($emptyTriggerModel);
        }, $this->helper->getRegisteredTriggerNames());
    }

    /**
     * Add transaction
     *
     * @since 1.0.0
     *
     * @param null|string $ident Identifier
     * @param int $consumerId Consumer ID
     * @param int $points Points value
     * @param string $message Message
     * @param int $createDate Create date
     * @param string $source Transaction source
     * @param int|null $agingDate Aging date or null to use default value
     * @return int transactionId
     */
    public function createTransaction($ident, int $consumerId, int $points, string $message, int $createDate, string $source, $agingDate = null): int
    {
        if ($createDate < 1 || empty($source)) {
            return 0;
        }

        if ($points > 0 && !$this->isEarningMethodActivated($source)) {
            return 0;
        }

        if ($points < 0 && !$this->isBurningMethodActivated($source)) {
            return 0;
        }

        if ($agingDate === null) {
            $agingDate = $this->isAgingEnabled()
                ? $this->calculateAgingDateTimestamp($createDate)
                : 0;
        }

        $transaction = new BPTransaction([
            'ident'      => $ident,
            'consumerId' => $consumerId,
            'userId'     => get_current_user_id(),
            'points'     => $points,
            'message'    => $message,
            'createDate' => $createDate,
            'source'     => $source,
            'agingDate'  => $agingDate
        ]);

        $success = $this->helper->saveTransaction($transaction);

        if ($success && $consumerId > 0) {
            $this->updateConsumerBalance($consumerId);
        }

        return $transaction->getTransactionId();
    }

    /**
     * Perform single trigger
     *
     * @since 1.4.0
     *
     * @param BPTrigger $trigger Trigger
     * @param ConsumerInterface $consumer Consumer to perform the trigger
     * @param int $actualTriggerValue Ident provided from a system (consumer data, etc.)
     * @param null|int $points Number of points to retrieve or null to get value saved to the trigger
     * @return bool True if performed
     */
    public function performSingleTrigger(BPTrigger $trigger, ConsumerInterface $consumer, int $actualTriggerValue = 0, $points = null): bool
    {
        if (!$trigger->isActual()) {
            return false;
        }

        if (!$trigger->isIdentValueValid($actualTriggerValue)) {
            return false;
        }

        if (!$this->helper->canConsumerPerformTrigger($consumer, $trigger)) {
            return false;
        }

        $triggerModel = $trigger->getModel();
        $triggerId = $triggerModel->getTriggerId();
        $consumerId = $consumer->getConsumerId();

        if ($points === null) {
            $points = $triggerModel->getPoints();
        }

        if ($points < 1) {
            _doing_it_wrong(__FUNCTION__, 'Invalid amount of points for trigger ' . $triggerModel->getTriggerId(), '1.5.0');
            return false;
        }

        if ($this->helper->canAddTriggerToConsumer($trigger, $consumer)) {
            if (!$this->helper->addTriggerToConsumer($trigger, $consumer)) {
                return false;
            }
        }

        $transactionId = $this->createTransaction(
            $triggerId,
            $consumerId,
            $points,
            $triggerModel->getMessage(),
            time(),
            BPTransaction::SOURCE_TRIGGER
        );

        if (!$transactionId) {
            return false;
        }

        ConsumersHelper::insertLog('bp_trigger', $consumerId, $triggerId);

        if ($consumerId === $this->consumersModule->getAuthorizedConsumerId()) {
            $this->setAlert(
                $triggerModel->getConsumerAlert()
            );
        }

        return true;
    }

    /**
     * Perform bonus trigger
     *
     * @since 1.0.0
     *
     * @param string $triggerType Bonus trigger type
     * @param ConsumerInterface $consumer Consumer to perform the trigger
     * @param int $actualTriggerValue Ident provided from a system (consumer data, etc.)
     * @param null|int $points Number of points to retrieve or null to get value saved to the trigger
     * @return int Number of success performs
     */
    public function performTriggers(string $triggerType, ConsumerInterface $consumer, int $actualTriggerValue = 0, $points = null): int
    {
        $performs = 0;

        foreach ($this->helper->getTriggersByType($triggerType) as $trigger) {
            $performs += $this->performSingleTrigger($trigger, $consumer, $actualTriggerValue, $points);
        }

        return $performs;
    }

    /**
     * Consumer visit saved - perform visits triggers
     *
     * @since 1.3.0
     * @internal
     *
     * @param ConsumerInterface $consumer
     */
    public function consumerVisitSaved(ConsumerInterface $consumer)
    {
        $this->performTriggers(
            'consumersVisits',
            $consumer,
            \consumersVisits::countConsumerVisitedDays($consumer->getConsumerId())
        );

        foreach ($this->helper->getTriggersByType('consumersVisitsInPeriod') as $trigger) {

            $visits = \consumersVisits::countConsumerVisitedDays(
                $consumer->getConsumerId(),
                $trigger->getModel()->getStartDate(),
                $trigger->getModel()->getEndDate()
            );

            $this->performTriggers($trigger->getName(), $consumer, $visits);
        }
    }

    /**
     * Gravity Form submitted -> perform bonus triggers
     *
     * @since 1.0.0
     * @internal
     *
     * @param array $entry Not used
     * @param array $form Form
     */
    public function gform_after_submission($entry, $form)
    {
        if ($this->consumersModule->isConsumerAuthorized()) {
            $this->performTriggers(
                'GFSubmit',
                $this->consumersModule->getAuthorizedConsumer(),
                (int) $form['id']
            );
        }
    }

    /**
     * Consumer started AT challenge -> perform bonus triggers
     *
     * @since 1.1.0
     * @internal
     *
     * @param int $challengeId Challenge Id
     * @param int $consumerId Consumer Id
     */
    public function atChallengeStarted(int $challengeId, int $consumerId)
    {
        $this->performTriggers(
            'at2',
            $this->consumersModule->getConsumer($consumerId),
            $challengeId
        );
    }

    /**
     * Perform trigger related to the made AT activity
     * @since 1.2.0
     * @param int $consumerId Consumer Id
     * @param int $activityId Activity Id
     */
    public function atConsumerMadeActivity(int $consumerId, int $activityId)
    {
        $this->performTriggers(
            'at2activity',
            $this->consumersModule->getConsumer($consumerId),
            $activityId
        );
    }

    /**
     * Consumer verified account -> perform bonus trigger
     *
     * @since 1.0.0
     * @internal
     *
     * @param int|ConsumerInterface $consumer
     */
    public function consumerVerified($consumer)
    {
        // @TODO Switch to ConsumerInterface after the hook will deliver it
        if (!($consumer instanceof ConsumerInterface)) {
            $consumer = $this->consumersModule->getConsumer($consumer);
        }

        if ($consumer && $consumer->active()) {
            $this->performTriggers('consumerVerification', $consumer);
        }
    }

    /**
     * Delete consumer hook
     *
     * @since 1.1.3
     * @internal
     *
     * @param int $consumerId Consumer Id
     */
    public function consumerDeleted(int $consumerId)
    {
        $this->helper->deleteConsumer($consumerId);
    }

    /**
     * Generate a list of conditions for Com.Center
     *
     * @since 2.7.0
     * @internal
     *
     * @param \LSConditionalEmailCondition[] $conditions List of conditions
     * @return \LSConditionalEmailCondition[] List of conditions
     */
    public function comCenterInit(array $conditions): array
    {
        $bp = $this;

        $bpBalanceField = static function () use ($bp) {
            return new Field\Text(
                'bpBalance',
                __('Points', 'ls'),
                [
                    'maxlength' => 5,
                    'after'     => $bp->getPluralPointsLabel(),
                    'style'     => 'width:70px'
                ]
            );
        };

        $helper = $this->helper;

        // Reach X Bonus Points
        $conditions['bp_balance'] = new \LSConditionalEmailCondition('bp_balance', __('Reach X points', 'ls'), $this->getTitle(), [
            'allowedPeriods' => [\LSConditionalEmail::PERIOD_ON],
            'showTimeField'  => true,
            'customFields'   => [
                'bpBalance' => $bpBalanceField
            ],
            'recipientsCb'   => static function (\LSConditionalEmail $ce) use ($helper) {

                $result = [];
                $points = (int) $ce->getCustomFieldValue('bpBalance', 0);

                if ($points > 0) {
                    $result = $helper->getConsumersForBalanceConditionalEmail($points, $ce->getEmailSlug());
                }

                return $result;
            }
        ]);

        // Aging points
        $conditions['bp_aging'] = new \LSConditionalEmailCondition('bp_aging', __('Points aged', 'ls'), $this->getTitle(), [
            'allowedPeriods' => [\LSConditionalEmail::PERIOD_ON],
            'showTimeField'  => true,
            'shortcodes'     => [
                '{aged-points}' => [
                    'label' => __('Points amount', 'ls'),
                    'value' => static function ($consumerId, \LSConditionalEmail $ce) {
                        return $ce->getConsumerExtraValue($consumerId, 'agedPoints', 100);
                    }
                ],
                '{date}'        => [
                    'label' => __('Aging date', 'ls'),
                    'value' => static function ($consumerId, \LSConditionalEmail $ce) {
                        return $ce->getConsumerExtraValue($consumerId, 'date', date_i18n('d.m.Y'));
                    }
                ]
            ],
            'recipientsCb'   => static function (\LSConditionalEmail $ce) use ($helper, $bp) {

                $ids = [];

                if ($bp->isLastAgingCheckWasToday()) {
                    foreach ($helper->getConsumersToSendAgedConditionalEmail() as $item) {

                        $ce
                            ->setConsumerExtraValue($item->consumerId, 'agedPoints', abs($item->points))
                            ->setConsumerExtraValue($item->consumerId, 'date', date('d.m.Y', $item->createDate + TIMEZONE_DIFF));

                        $ids[] = $item->consumerId;
                    }
                }

                return $ids;
            }
        ]);

        return $conditions;
    }

    /**
     * Retrieve BP Points label
     *
     * @since 1.0.0
     * @deprecated 06.2019 Legacy
     *
     * @param bool $plural True to retrieve plural label
     * @return string Label
     */
    public function getLabel(bool $plural = true): string
    {
        _doing_it_wrong(__FUNCTION__, 'deprecated', '1.4.3');

        return $plural
            ? $this->getPluralPointsLabel()
            : $this->getSingularPointsLabel();
    }

    /**
     * Add Bonus Points as possible currency
     *
     * @since 1.0.7
     * @internal
     *
     * @param string[] $currencies Currencies
     * @return string[] Currencies
     */
    public function bpRewardsCurrencies(array $currencies): array
    {
        return array_merge([
            'BP' => $this->getPluralPointsLabel()
        ], $currencies);
    }

    /**
     * Register Rewards as burning method for Bonus Points
     *
     * @since 1.0.7
     * @internal
     *
     * @param BPMethod[] $methods Burning methods
     * @return BPMethod[] Burning methods
     */
    public function bpRewardsBurningMethods(array $methods): array
    {
        $methods[] = new BPMethod(
            BPTransaction::SOURCE_REWARD,
            __('Rewards', 'ls')
        );

        return $methods;
    }

    /**
     * Customize rewards balance for the consumer
     *
     * @since 1.0.0
     * @internal
     *
     * @param int $balance Reward balance
     * @param string $currency Currency
     * @return int Reward balance
     */
    public function bpRewardsBalance($balance, string $currency): int
    {
        if ($currency === 'BP' && $this->consumersModule->isConsumerAuthorized()) {
            $balance = $this->getConsumerBalance(
                $this->consumersModule->getAuthorizedConsumer()
            );
        }

        return (int) $balance;
    }

    /**
     * Withdraw bonus points on incentive ordered
     *
     * @since 1.0.7
     * @internal
     *
     * @param array $orderData Order info
     */
    public function bpRewardsIncentiveOrdered(array $orderData)
    {
        if ($this->getCurrency() == 'BP' && $orderData['salesPrice'] > 0) {
            $this->createTransaction(
                $orderData['incentiveId'],
                $orderData['consumerId'],
                -$orderData['salesPrice'],
                sprintf(__('Reward %s ordered', 'ls'), $orderData['incentiveName']),
                time(),
                BPTransaction::SOURCE_REWARD
            );
        }
    }

    /**
     * Check if consumer can perform trigger
     *
     * @since 1.1.0
     * @deprecated 04.2019 Use BPHelper->canConsumerPerformTrigger()
     *
     * @param BPTriggerModel $triggerModel Trigger to check
     * @param int $consumerId Consumer Id
     * @return bool True if consumer can perform trigger
     */
    public function isPossibleToPerformTrigger(BPTriggerModel $triggerModel, int $consumerId): bool
    {
        _deprecated_function(__FUNCTION__, '1.3.1', 'BPHelper->canConsumerPerformTrigger()');

        $trigger = $this->helper->getTriggerFromModel($triggerModel);

        if (!$trigger || !$trigger->isActual()) {
            return false;
        }

        return $this->helper->canConsumerPerformTrigger(
            $this->consumersModule->getConsumer($consumerId),
            $trigger
        );
    }

    /**
     * Perform bonus trigger
     *
     * @since 1.0.0
     * @deprecated 05.2019 Use performTriggers()
     *
     * @param string $triggerType Bonus trigger type
     * @param int $actualIdent Value to identify trigger (optional)
     * @param null|int $consumerId Consumer Id (optional)
     * @param null|callable $identValidationCallback Function to validate the ident
     */
    public function performTrigger(string $triggerType, int $actualIdent = 0, $consumerId = null, $identValidationCallback = null)
    {
        _deprecated_function(__FUNCTION__, '1.4.0', 'performTriggers');

        if ($identValidationCallback !== null) {
            _doing_it_wrong(__FUNCTION__, '4th param in performTrigger() deprecated', '1.4.0');
            return;
        }

        $this->performTriggers(
            $triggerType,
            $this->consumersModule->getConsumer($consumerId),
            $actualIdent
        );
    }

    /**
     * @since 3.0.0
     * @param string $name
     * @param array $args
     * @return mixed
     */
    public static function __callStatic($name, $args)
    {
        /** @var bonusPoints $module */
        $module = LS()->getModule('bonusPoints');

        // @deprecated 05.2019 Legacy
        if (in_array($name, ['getConsumerBalance', 'isSilverStatus', 'getLabel'])) {
            _deprecated_function(__FUNCTION__, '1.4.0');
            return $module->{$name}($args[0]);
        }

        throw new \Exception('Wrong argument ' . $name);
    }
}