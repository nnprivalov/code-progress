<?php
namespace LS;

class BPConsumerTrigger extends Model
{
    /** @var int */
    protected $triggerId;

    /** @var int */
    protected $consumerId;

    /** @var int */
    protected $triggerDate;

    /** @var string[] */
    protected $primaryKeys = [
        'triggerId',
        'consumerId'
    ];

    /** @var string[] */
    protected $dbCols = [
        'triggerId',
        'consumerId',
        'triggerDate'
    ];

    /**
     * @return self
     */
    public function setDefault()
    {
        $this->triggerId =
        $this->consumerId =
        $this->triggerDate = 0;

        return $this;
    }

    /**
     * @param array $data
     * @return array
     */
    protected function format($data)
    {
        return array_map('intval', $data);
    }

    /**
     * @return bool
     */
    public function valid()
    {
        return
            $this->triggerId > 0
            && $this->consumerId > 0
            && $this->triggerDate > 0;
    }

    /**
     * @return int
     */
    public function getConsumerId(): int
    {
        return $this->consumerId;
    }

    /**
     * @return int
     */
    public function getTriggerId(): int
    {
        return $this->triggerId;
    }

    /**
     * @return int
     */
    public function getTriggerDate(): int
    {
        return $this->triggerDate;
    }
}