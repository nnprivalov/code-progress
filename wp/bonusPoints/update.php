<?php
namespace LS;

class bonusPointsUpdate implements UpdateInterface
{
    public function switchToNewName()
    {
        $wpdb = LS()->wpdb;

        $wpdb->query("ALTER TABLE `{$wpdb->prefix}ls_bp_triggers`
                        ADD COLUMN `startDate` DATE NULL AFTER `active`,
                        ADD COLUMN `endDate` DATE NULL AFTER `startDate`");

        $wpdb->query("ALTER TABLE `{$wpdb->prefix}ls_bp_triggers`
                        ADD COLUMN `companyId` INT(10) UNSIGNED NOT NULL DEFAULT '0' AFTER `triggerId`");

        if (get_option('ls_bonusPointsLoyaltySuite')) {
            delete_option('ls_bonusPoints');
            $wpdb->update($wpdb->options, ['option_name' => 'ls_bonusPoints'], ['option_name' => 'ls_bonusPointsLoyaltySuite']);
        }
    }

    /**
     * @param Module $module
     * @param string $oldVersion
     * @param string $newVersion
     */
    public function update(Module $module, string $oldVersion, string $newVersion)
    {
        $wpdb = LS()->wpdb;

        // 1.1.0
        if (version_compare($oldVersion, '1.1.0', '<')) {

            $wpdb->query(sprintf("UPDATE {$wpdb->prefix}ls_bp_transactions SET source = 'MT' WHERE source = 'manual'"));
            $wpdb->query(sprintf("UPDATE {$wpdb->prefix}ls_bp_transactions SET source = 'AT' WHERE source = 'at'"));
            $wpdb->query(sprintf("UPDATE {$wpdb->prefix}ls_bp_transactions SET source = 'BT' WHERE source = 'bt'"));
            $wpdb->query(sprintf("UPDATE {$wpdb->prefix}ls_bp_transactions SET source = 'RW' WHERE source = 'rewards'"));
            $wpdb->query(sprintf("UPDATE {$wpdb->prefix}ls_bp_transactions SET source = 'IM' WHERE source = 'import'"));
            $wpdb->query(sprintf("UPDATE {$wpdb->prefix}ls_bp_transactions SET source = 'AG' WHERE source = 'aging'"));

            $wpdb->query(sprintf("ALTER TABLE `{$wpdb->prefix}ls_bp_transactions`
                          CHANGE COLUMN `source` `source` VARCHAR(2) NOT NULL AFTER `message`,
                          DROP COLUMN `EID`,
                          DROP INDEX `EID`"));

            $wpdb->query(sprintf("ALTER TABLE `{$wpdb->prefix}ls_bp_transactions`
                          CHANGE COLUMN `TRID` `transactionId` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT FIRST,
	                      CHANGE COLUMN `message` `message` VARCHAR(200) NOT NULL AFTER `points`,
	                      CHANGE COLUMN `ident` `ident` VARCHAR(100) NOT NULL AFTER `transactionId`"));

            $wpdb->query(sprintf("ALTER TABLE `{$wpdb->prefix}ls_bp_triggers` 
                          CHANGE COLUMN `BTID` `triggerId` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT FIRST,
                          ADD COLUMN `consumerAlert` VARCHAR(500) NOT NULL AFTER `message`,
	                      DROP COLUMN `used`,
	                      ADD COLUMN `multiple` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' AFTER `consumerAlert`"));

            $wpdb->query(sprintf("CREATE TABLE `{$wpdb->prefix}ls_bp_consumers_triggers` (
            `consumerId` INT(10) UNSIGNED NOT NULL,
            `triggerId` SMALLINT(5) UNSIGNED NOT NULL,
            `triggerDate` INT(10) UNSIGNED NOT NULL,
            PRIMARY KEY (`consumerId`, `triggerId`)
        ) %s ENGINE=InnoDB", Library::getCharsetCollate()));

            $wpdb->query(sprintf("UPDATE {$wpdb->prefix}ls_consumers_log SET `messageType` = 'bp_transaction' WHERE `messageType` = 'bp-transaction'"));

            $wpdb->delete($wpdb->prefix . 'bp_transaction', ['source' => 'BT']);
        }

        // 1.2.0
        if (version_compare($oldVersion, '1.2.0', '<')) {
            $wpdb->query("ALTER TABLE `{$wpdb->prefix}ls_bp_triggers` CHANGE COLUMN `triggerIdent` `triggerIdent` BIGINT(20) UNSIGNED NOT null DEFAULT '0' AFTER `trigger`");
        }

        // 1.4.0
        if (version_compare($oldVersion, '1.4.0', '<')) {
            $this->switchToNewName();
        }

        // 1.4.3
        if (version_compare($oldVersion, '1.4.3', '<')) {
            $module->deleteOption('statusMessage');
            $module->deleteOption('statusLabel');
        }

        // 1.5.0
        if (version_compare($oldVersion, '1.5.0', '<')) {
            $wpdb->query("ALTER TABLE `{$wpdb->prefix}ls_bp_triggers` DROP COLUMN `multiple`");
        }
    }
}