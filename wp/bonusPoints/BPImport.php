<?php
namespace LS;

class BPImport extends Model
{
    const STATUS_IMPORT_STARTED = 0;
    const STATUS_IMPORT_FINISHED = 1;
    const STATUS_IMPORT_FAILED = 2;

    /** @var int */
    protected $importId;

    /** @var string */
    protected $fileName;

    /** @var string */
    protected $fileImportName;

    /** @var int */
    protected $importDate;

    /** @var bool */
    protected $status;

    /** @var int */
    protected $recordsProcessed;

    /** @var int */
    protected $recordsSaved;

    /** @var int */
    protected $pointsTransferred;

    /** @var int */
    protected $associatedWithConsumers;

    /** @var string[] */
    protected $primaryKeys = [
        'importId'
    ];

    /** @var string[] */
    protected $dbCols = [
        'importId',
        'fileName',
        'fileImportName',
        'importDate',
        'status',
        'recordsProcessed',
        'recordsSaved',
        'pointsTransferred',
        'associatedWithConsumers'
    ];

    /**
     * @param array $data Data to be formatted
     * @return array Formatted data
     */
    protected function format($data)
    {
        foreach ($data as $key => $value) {
            if ($key == 'importDate') {
                $data[$key] = max(0, !empty($value) && !is_numeric($value) ? strtotime($value) : (int) $value);
            } elseif (in_array($key, ['importId', 'recordsProcessed', 'recordsSaved', 'pointsTransferred', 'associatedWithConsumers'])) {
                $data[$key] = (int) $value;
            } elseif ($key == 'status') {
                $data[$key] = (bool) $value;
            } elseif (in_array($key, ['fileName', 'fileImportName'])) {
                $data[$key] = substr($value, 0, 400);
            }
        }

        return $data;
    }

    /**
     * @return self
     */
    public function setDefault()
    {
        $this->importId =
        $this->importDate =
        $this->recordsProcessed =
        $this->recordsSaved =
        $this->pointsTransferred =
        $this->associatedWithConsumers = 0;

        $this->fileName =
        $this->fileImportName = '';

        $this->status = false;

        return $this;
    }

    /**
     * @return bool True if the record is valid
     */
    public function valid(): bool
    {
        return !empty($this->getImportDate()) && !empty($this->getFileName());
    }

    /**
     * @return int
     */
    public function getImportId(): int
    {
        return $this->importId;
    }

    /**
     * @return string
     */
    public function getFileName(): string
    {
        return $this->fileName;
    }

    /**
     * @return string
     */
    public function getImportFileName(): string
    {
        return $this->fileImportName;
    }

    /**
     * @return int
     */
    public function getRecordsProcessed(): int
    {
        return $this->recordsProcessed;
    }

    /**
     * @return int
     */
    public function getRecordsSaved(): int
    {
        return $this->recordsSaved;
    }

    /**
     * @return int
     */
    public function getRecordsFailed(): int
    {
        return $this->getRecordsProcessed() - $this->getRecordsSaved();
    }

    /**
     * @return int
     */
    public function getPointsTransferred(): int
    {
        return $this->pointsTransferred;
    }

    /**
     * @return int
     */
    public function getAssociatedWithConsumers(): int
    {
        return $this->associatedWithConsumers;
    }

    /**
     * @return int
     */
    public function getNotAssociatedWithConsumers(): int
    {
        return $this->getRecordsProcessed() - $this->getAssociatedWithConsumers();
    }

    /**
     * @return bool
     */
    public function getStatus(): bool
    {
        return $this->status;
    }

    /**
     * @param int $status
     * @return self
     */
    public function setStatus(int $status): BPImport
    {
        if (in_array($status, [self::STATUS_IMPORT_STARTED, self::STATUS_IMPORT_FINISHED, self::STATUS_IMPORT_FAILED])) {
            $this->status = $status;
        }

        return $this;
    }

    /**
     * @return int
     */
    public function getImportDate(): int
    {
        return $this->importDate;
    }

    /**
     * Get file extension
     * @return string File extension
     */
    public function getExt(): string
    {
        return substr($this->getImportFileName(), strrpos($this->getImportFileName(), '.') + 1);
    }

    /**
     * @param bool $withPrimaryKeys True to include primary keys data
     * @return array An array of columns and their values
     */
    public function getRecord($withPrimaryKeys = false)
    {
        $data = [];

        foreach ($this->getDbColumns($withPrimaryKeys) as $col) {
            if ($col == 'status') {
                $data[$col] = (int) $this->{$col};
            } else {
                $data[$col] = $this->{$col};
            }
        }

        return $data;
    }
}