<?php
namespace LS;

class BPTransaction extends Model
{
    const SOURCE_MANUAL = 'MT';
    const SOURCE_IMPORT = 'IM';
    const SOURCE_TRIGGER = 'BT';
    const SOURCE_REWARD = 'RW';
    const SOURCE_AGING = 'AG';

    /** @var int */
    protected $transactionId;

    /** @var string */
    protected $ident;

    /** @var int */
    protected $consumerId;

    /** @var int */
    protected $userId;

    /** @var int */
    protected $points;

    /** @var string */
    protected $message;

    /** @var string */
    protected $source;

    /** @var int */
    protected $createDate;

    /** @var int */
    protected $agingDate;

    /** @var string[] */
    protected $primaryKeys = [
        'transactionId'
    ];

    /** @var string[] */
    protected $dbCols = [
        'transactionId',
        'ident',
        'consumerId',
        'userId',
        'points',
        'message',
        'source',
        'createDate',
        'agingDate'
    ];

    /**
     * @param array $data Data to be formatted
     * @return array Formatted data
     */
    protected function format($data)
    {
        foreach ($data as $key => $value) {
            if (in_array($key, ['createDate', 'agingDate'])) {
                $data[$key] = max(0, !empty($value) && !is_numeric($value) ? strtotime($value) : (int) $value);
            } elseif (in_array($key, ['transactionId', 'consumerId', 'userId', 'points'])) {
                $data[$key] = (int) $value;
            } elseif ($key == 'ident' && $value === null) {
                $data[$key] = '';
            } elseif ($key == 'ident') {
                $data[$key] = trim(substr($value, 0, 100));
            } elseif ($key == 'message') {
                $data[$key] = trim(Library::substr($value, 0, 100));
            } elseif ($key == 'source') {
                $data[$key] = substr($value, 0, 2);
            }
        }

        return $data;
    }

    /**
     * @return self
     */
    public function setDefault()
    {
        $this->transactionId =
        $this->consumerId =
        $this->userId =
        $this->points =
        $this->createDate =
        $this->agingDate = 0;

        $this->ident =
        $this->message =
        $this->source = '';

        return $this;
    }

    /**
     * @return bool True if the record is valid
     */
    public function valid(): bool
    {
        return !empty($this->getSource());
    }

    /**
     * @return int
     */
    public function getTransactionId(): int
    {
        return $this->transactionId;
    }

    /**
     * @return string
     */
    public function getIdent(): string
    {
        return $this->ident;
    }

    /**
     * @return int
     */
    public function getConsumerId(): int
    {
        return $this->consumerId;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @return int
     */
    public function getPoints(): int
    {
        return $this->points;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @return string
     */
    public function getSource(): string
    {
        return $this->source;
    }

    /**
     * @return int
     */
    public function getCreateDate(): int
    {
        return $this->createDate;
    }

    /**
     * @return int
     */
    public function getAgingDate(): int
    {
        return $this->agingDate;
    }

    /**
     * External Id
     * @return string
     */
    public function getSKU(): string
    {
        return $this->getSource() . str_pad($this->getTransactionId(), 8, '0', STR_PAD_LEFT);
    }
}