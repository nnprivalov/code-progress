<?php
namespace LS;

abstract class BPImportHandler
{
    /** @var string Handler name */
    protected $name = '';

    /** @var string[] Possible file extensions for this handler */
    protected $extensions = [];

    /** @var string Path to file */
    protected $pathToFile = '';

    /** @var string[] File columns */
    protected $columns = [];

    /** @var string[] File records */
    protected $records = [];

    /** @var bool True if file was read */
    protected $wasRead = false;

    /**
     * @return string
     */
    final public function getName(): string
    {
        return $this->name;
    }

    /**
     * Retrieve extensions of the file to import
     * @return string[] An array of extension strings
     */
    final public function getExtensions(): array
    {
        return $this->extensions;
    }

    /**
     * Validate import file
     * @return string Error message or empty on success
     */
    final public function validate(): string
    {
        if ($this->isEmpty()) {
            return __('File is empty', 'ls');
        }

        return $this->__validate();
    }

    /**
     * Abstract function to validate file in child class
     * @return string
     */
    protected function __validate(): string
    {
        return '';
    }

    /**
     * Check if file to import is empty
     * @return bool True if file to import is empty
     */
    final public function isEmpty(): bool
    {
        return !$this->wasRead || filesize($this->pathToFile) == 0;
    }

    /**
     * Retrieve columns of the file to import
     * @return string[] File columns
     */
    final public function getColumns(): array
    {
        if (!$this->wasRead) {
            return [];
        }

        return empty($this->columns) ? $this->__getColumns() : $this->columns;
    }

    /**
     * Abstract function to get file columns in child class
     * @return string[]
     */
    abstract protected function __getColumns(): array;

    /**
     * Retrieve records of the file to import
     * @return string[] File records
     */
    final public function getRecords(): array
    {
        if (!$this->wasRead) {
            return [];
        }

        if (empty($this->records)) {
            $this->records = $this->__getRecords();
        }

        return $this->records;
    }

    /**
     * Abstract function to get file records in child class
     * @return string[]
     */
    abstract protected function __getRecords(): array;

    /**
     * Read file
     *
     * @param string $file Path to file
     * @return false|self Object handler or false on failure
     */
    final public function read(string $file)
    {
        if (!is_file($file) || !is_readable($file)) {
            return false;
        }

        $this->pathToFile = $file;
        $this->wasRead = true;

        return $this;
    }

    /**
     * Retrieve a list of possible identifier statements
     * @return array
     */
    public static function getIdentifierStatements(): array
    {
        return [
            'id'    => __('Consumer Id', 'ls'),
            'login' => __('Consumer login', 'ls'),
            'email' => __('Consumer email', 'ls'),
            'other' => __('Other', 'ls')
        ];
    }

    /**
     * Retrieve a list of possible create date statements
     * @return array
     */
    public static function getCreateDateStatements(): array
    {
        return [
            'timestamp'   => __('UNIX Timestamp', 'ls'),
            'd.m.Y'       => date_i18n('d.m.Y'),
            'd.m.Y H:i:s' => date_i18n('d.m.Y H:i:s'),
            'Y-m-d'       => date_i18n('Y-m-d'),
            'Y-m-d H:i:s' => date_i18n('Y-m-d H:i:s'),
            'd/m/Y'       => date_i18n('d/m/Y'),
            'd/m/Y H:i:s' => date_i18n('d/m/Y H:i:s'),
            'other'       => __('Other', 'ls')
        ];
    }
}