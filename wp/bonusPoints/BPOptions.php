<?php
namespace LS;

trait BPOptions
{
    /**
     * @param string $key Option name
     * @param mixed $default
     * @return mixed
     */
    abstract public function getOption($key, $default = null);

    /**
     * @param string $key
     * @param mixed $value
     */
    abstract public function setOption($key, $value);

    /**
     * Retrieve module default options
     * @since 1.5.2
     * @return array
     */
    public function getDefaultOptions(): array
    {
        return [
            'ptSingular'           => __('Point', 'ls'),
            'ptPlural'             => __('Points', 'ls'),
            'ptCurrency'           => 0.01,
            'activeEarningMethods' => [],
            'activeBurningMethods' => [],
            'agingEnabled'         => false,
            'agingDays'            => 0,
            'agingMessage'         => 'Transaction aged',
            'statusEnabled'        => false,
            'statusPoints'         => 1000,
            'statusLifetime'       => 100
        ];
    }

    /**
     * Retrieve a list of active earning methods
     * @since 1.5.2
     * @return array
     */
    public function getActiveEarningMethods(): array
    {
        return (array) $this->getOption('activeEarningMethods');
    }

    /**
     * Retrieve a list of active burning methods
     * @since 1.5.2
     * @return array
     */
    public function getActiveBurningMethods(): array
    {
        return (array) $this->getOption('activeBurningMethods');
    }

    /**
     * Check if aging is enabled
     * @since 1.5.2
     * @return bool
     */
    public function isAgingEnabled(): bool
    {
        return (bool) $this->getOption('agingEnabled');
    }

    /**
     * Retrieve a number of days to mark a transaction as aged
     * @since 1.5.2
     * @return int
     */
    public function getAgingDays(): int
    {
        return (int) $this->getOption('agingDays');
    }

    /**
     * Retrieve aging message
     * @since 1.5.2
     * @return string
     */
    public function getAgingMessage(): string
    {
        return (string) $this->getOption('agingMessage');
    }

    /**
     * Retrieve a plural label to be added after the points value
     * @since 1.5.2
     * @return string
     */
    public function getPluralPointsLabel(): string
    {
        return (string) $this->getOption('ptPlural', 'BP');
    }

    /**
     * Retrieve a singular label to be added after the points value
     * @since 1.5.2
     * @return string
     */
    public function getSingularPointsLabel(): string
    {
        return (string) $this->getOption('ptSingular', 'BP');
    }

    /**
     * Retrieve a date of the last aging in Y-m-d format
     * @since 1.5.2
     * @return string
     */
    public function getLastAgingCheckDate(): string
    {
        return (string) $this->getOption('agingCheck');
    }

    /**
     * Check if last aging check date was made today
     * @since 1.5.2
     * @return bool
     */
    public function isLastAgingCheckWasToday(): bool
    {
        return $this->getLastAgingCheckDate() === date('Ymd');
    }

    /**
     * Mark aging as processed - set last aging date to now
     * @since 1.5.2
     */
    public function markAgingAsChecked()
    {
        $this->setOption('agingCheck', date('Ymd'));
    }

    /**
     * Retrieve currency
     * @since 1.5.2
     * @return string
     */
    public function getCurrency(): string
    {
        return (string) $this->getOption('currency', 'BP');
    }

    /**
     * Check if silver status feature enabled
     * @since 1.5.2
     * @return bool
     */
    public function isSilverStatusEnabled(): bool
    {
        return (bool) $this->getOption('statusEnabled');
    }

    /**
     * Retrieve a minimum number of points to have the silver status
     * @since 1.5.2
     * @return int
     */
    public function getMinPointsToHaveSilverStatus(): int
    {
        return (int) $this->getOption('statusPoints');
    }

    /**
     * Retrieve a number of days to provide the silver status
     * @since 1.5.2
     * @return int
     */
    public function getSilverStatusLifetime(): int
    {
        return (int) $this->getOption('statusLifetime');
    }
}