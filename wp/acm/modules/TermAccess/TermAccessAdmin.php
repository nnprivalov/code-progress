<?php
namespace LS;

class TermAccessAdmin extends TermAccess implements ModuleAdminInterface
{
    /**
     * Main function to load models, create actions and filters, init variables
     * @since 1.0.0
     * @internal
     */
    public function load()
    {
        parent::load();

        add_filter('ls_acm_group_details_page', [$this, 'groupDetailsPage'], 10, 2);
        add_action('admin_init', [$this, 'adminInit']);
        add_action('ls_acm_group_duplicated', [$this, 'groupDuplicated'], 10, 2);
        add_action('ls_acm_group_connections_deleted', [$this, 'groupConnectionsDeleted']);
        add_action('ls_acm_all_group_connections_deleted', [$this, 'allGroupConnectionsDeleted']);
        add_action('admin_enqueue_scripts', [$this, 'adminScripts']);

        // clean caches
        add_action('ls_acm_group_connected_to_term', [$this, 'resetCache']);
        add_action('ls_acm_group_disconnected_from_term', [$this, 'resetCache']);
    }

    /**
     * @since 1.0.0
     * @internal
     */
    public function adminScripts()
    {
        wp_enqueue_script('ls-acm-admin-ext', $this->getUrl2Module() . 'js/term-access.js', ['jquery'], $this->getVersion(), true);
    }

    /**
     * @since 1.0.0
     * @internal
     *
     * @param array $tabs
     * @param \ACMGroup $group
     * @return array
     */
    public function groupDetailsPage(array $tabs, \ACMGroup $group)
    {
        if ($group->getGroupId() > 0) {

            include_once __DIR__ . '/tables/TermAccessTable.php';

            $termsContent = (new Template(__DIR__ . '/views/backend'))
                ->assign('group', $group)
                ->assign('terms', $this->helper->getProtectedTerms($group->getGroupId()))
                ->render('admin-group-details-terms.phtml', false);

            $tabs = array_merge($tabs, [
                'acm-group-terms' => [__('Protected terms', 'ls'), $termsContent]
            ]);
        }

        return $tabs;
    }

    /**
     * Retrieve taxonomies that can be used with the Access Codes
     * @since 1.0.0
     * @return array Taxonomies
     */
    protected function getAvailableTaxonomies()
    {
        $taxonomies = [];

        foreach ($this->acm->getAvailablePostTypes() as $postType) {
            foreach (get_object_taxonomies($postType->name, 'objects') as $taxonomy) {
                if ($taxonomy->hierarchical) {
                    $taxonomies[$taxonomy->name] = $taxonomy;
                    $taxonomies[$taxonomy->name]->postTypesLabel = ' (' . implode(', ', $taxonomy->object_type) . ')';
                }
            }
        }

        return apply_filters('ls_acm_available_taxonomies', $taxonomies);
    }

    /**
     * Process admin actions and requests
     * @since 1.0.0
     * @internal
     */
    public function adminInit()
    {
        add_action('wp_ajax_saveACMTerms', [$this, 'saveACMTerms']);

        foreach ($this->getAvailableTaxonomies() as $taxonomy) {
            add_filter('manage_edit-' . $taxonomy->name . '_columns', [$this, 'addCategoryColumn']);
            add_filter('manage_' . $taxonomy->name . '_custom_column', [$this, 'modifyCategoryColumn'], 10, 3);
        }

        if ($this->isAction('acm-exclude-term') && current_user_can('ls_acm_manage')) {
            $this->excludedTermFromGroupAction();
        }
    }

    /**
     * Action to exclude term from a group
     * @since 1.0.0
     */
    private function excludedTermFromGroupAction()
    {
        $termId = $this->getParam('term_id', 'id');
        $groupId = $this->getParam('group_id', 'id');

        if ($this->helper->disconnectGroupFromTerm($groupId, $termId)) {
            $this->setAlert(__('Term has been removed from the CA-Group', 'ls'));
            $this->redirect(remove_query_arg(['action', 'term_id', 'group_id']) . '#acm-group-terms');
        }
    }

    /**
     * Save connections between term and CA-Group
     *
     * @since 1.0.0
     * @internal
     * @ajax
     */
    public function saveACMTerms()
    {
        if (isset($_POST['termId']) && current_user_can('ls_acm_manage')) {

            $termId = $this->getParam('termId', 'id');
            $groups = $this->getParam('groups', 'int_array');

            $this->helper->disconnectTermFromGroups($termId);

            foreach ($groups as $groupId) {
                $this->helper->connectGroupToTerm($groupId, $termId);
            }

            LS()->logUserAction('ACM: term groups updated (ID: ' . $termId . ')');
        }

        exit;
    }

    /**
     * Add column on the Categories page
     *
     * @since 1.0.0
     * @internal
     *
     * @param array $column A list of columns
     * @return array A list of columns with new column
     */
    public function addCategoryColumn($column)
    {
        return array_merge(
            array_slice($column, 0, 2, true),
            ['acmGroupId' => __('CA-Group', 'ls')],
            array_slice($column, 2, null, true)
        );
    }

    /**
     * Set cell content on the Categories page
     *
     * @since 1.0.0
     * @internal
     *
     * @param null $null Not used here
     * @param string $columnName Column name
     * @param int $termId Term Id
     */
    public function modifyCategoryColumn($null, $columnName, $termId)
    {
        if ($columnName !== 'acmGroupId') {
            return;
        }

        $groups = $this->acmHelper->getGroupsList(true);

        if (empty($groups)) {
            echo '—';
        } else {

            echo '<select name="acm_term[' . (int) $termId . ']" data-term-id="' . (int) $termId . '" class="acm-multiple-select" multiple="multiple" autocomplete="off">';

            $termGroupsIds = $this->helper->getGroupIdsByTermId($termId);

            foreach ($groups as $groupId => $groupName) {
                echo '<option value="' . (int) $groupId . '" ' . selected(in_array($groupId, $termGroupsIds), true, false) . '>' . $groupName . '</option>';
            }

            echo '</select>';
        }
    }

    /**
     * Add column "CA-Group" to the posts overview page
     *
     * @since 1.0.0
     * @internal
     *
     * @param array $columns A list of table columns
     * @return array A list of table columns
     */
    public function addColumn($columns)
    {
        $columns['acmGroups'] = __('CA-Group', 'ls');

        return $columns;
    }

    /**
     * Fill out the CA-Group on the posts overview page
     *
     * @since 1.0.0
     * @internal
     *
     * @param string $colName Column name
     * @param int $postId Post Id
     */
    public function addRowForColumn($colName, $postId)
    {
        if ($colName == 'acmGroups' && $postId > 0) {

            $result = [];

            foreach ($this->acmHelper->getGroupsByPostId($postId) as $group) {
                $result[] = '<a href="' . \acmAdminLoyaltySuite::contentAccessEditLink($group->getGroupId()) . '" ' . ($group->isActive() ? '' : 'style="color: red;"') . '>' . $group->getName() . '</a>';
            }

            echo empty($result) ? ' ' : implode(', ', $result);
        }
    }

    /**
     * CA-Group duplicated
     *
     * @since 1.0.0
     * @internal
     *
     * @param int $oldGroupId Old Group Id
     * @param int $newGroupId New Group Id
     */
    public function groupDuplicated(int $oldGroupId, int $newGroupId)
    {
        foreach ($this->helper->getProtectedTermsIds($oldGroupId) as $termId) {
            $this->helper->connectGroupToTerm($newGroupId, $termId);
        }
    }

    /**
     * CA-Group connections deleted
     * @since 1.0.0
     * @param int $groupId CA-Group Id
     */
    public function groupConnectionsDeleted(int $groupId)
    {
        $this->helper->deleteGroupConnections($groupId);
    }

    /**
     * All CA-Group connections deleted
     * @since 1.0.0
     */
    public function allGroupConnectionsDeleted()
    {
        $this->helper->deleteAllGroupConnections();
    }
}