<?php
namespace LS;

class TermAccess extends Module
{
    /** @var \WP_Term[] */
    private $protectedTerms;

    /** @var \WP_Term[] */
    private $consumerProtectedTerms;

    /** @var \acmLoyaltySuite */
    protected $acm;

    /** @var TermAccessHelper */
    protected $helper;

    /** @var \ACMHelper */
    protected $acmHelper;

    /** @var \consumersLoyaltySuite */
    protected $consumersModule;

    /**
     * @return string
     */
    public function getVersion()
    {
        return '1.0.0';
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return 'Categories/Taxonomies support';
    }

    /**
     * @since 1.0.0
     * @internal
     */
    public function load()
    {
        require_once __DIR__ . '/TermAccessHelper.php';

        $this->acm = $this->getParent();
        $this->helper = new TermAccessHelper();
        $this->acmHelper = $this->acm->helper();
        $this->consumersModule = LS()->getModule('consumers');

        add_action('wp', [$this, 'wp']);
        add_filter('get_terms', [$this, 'getTerms'], 99, 3);
        add_filter('wp_get_nav_menu_items', [$this, 'wpNavMenuObjects'], 100);
        add_filter('ls_acm_pre_get_posts', [$this, 'preGetPosts'], 98, 3);
        add_filter('ls_acm_has_access_to_post', [$this, 'hasAccessToPost'], 10, 3);
    }

    /**
     * Retrieve helper
     * @since 1.0.0
     * @return TermAccessHelper
     */
    public function helper(): TermAccessHelper
    {
        return $this->helper;
    }

    /**
     * Reset a list of pre-saved protect post Id's
     * @internal
     * @since 1.0.0
     */
    public function resetCache()
    {
        $this->protectedTerms = null;
        $this->consumerProtectedTerms = null;
    }

    /**
     * Retrieve an array of ac-protect terms Id's
     *
     * @since 1.0.0
     * @internal
     *
     * @param string $taxonomy Taxonomy name
     * @return int[] An array of terms Id's
     */
    private function getProtectedTerms($taxonomy)
    {
        if ($this->protectedTerms === null) {
            $this->protectedTerms = $this->helper->getProtectedTerms();
        }

        $ids = [];

        foreach ($this->protectedTerms as $term) {
            if ($term->taxonomy == $taxonomy) {
                $ids[] = (int) $term->term_id;
            }
        }

        return $ids;
    }

    /**
     * Determine if term is ac-protected
     *
     * @since 1.0.0
     *
     * @param int $termId Term Id
     * @param string $taxonomy Taxonomy name
     * @return bool True if term is ac-protected
     */
    public function isTermProtected($termId, $taxonomy)
    {
        return in_array($termId, $this->getProtectedTerms($taxonomy));
    }

    /**
     * Retrieve an array of ac-protected terms Id's for the consumer
     *
     * @since 2.0.0
     * @internal
     *
     * @param int $consumerId Consumer Id
     * @param string $taxonomy Taxonomy name
     * @return int[] An array of terms Id's
     */
    private function getProtectedTermsForConsumer($consumerId, $taxonomy)
    {
        if ($consumerId < 1) {
            $ids = $this->getProtectedTerms($taxonomy);
        } else {

            $terms = [];
            $isCurrentConsumer = $consumerId === $this->consumersModule->getAuthorizedConsumerId();

            if (!$isCurrentConsumer || $this->consumerProtectedTerms === null) {

                $terms = $this->helper->getProtectedTermsForConsumer(
                    $consumerId,
                    $this->acmHelper->getConsumerGroupsIds($consumerId, true)
                );

                if ($isCurrentConsumer) {
                    $this->consumerProtectedTerms = $terms;
                }

            } elseif ($isCurrentConsumer) {
                $terms = $this->consumerProtectedTerms;
            }

            $ids = [];

            foreach ($terms as $term) {
                if ($taxonomy === $term->taxonomy) {
                    $ids[] = (int) $term->term_id;
                }
            }
        }

        return $ids;
    }

    /**
     * Retrieve term parents
     *
     * @since 1.0.0
     *
     * @param int[] $terms An array of term's ID's
     * @return int[] An array of term's ID's
     */
    private function getTermParents($terms)
    {
        foreach ($terms as $termId) {

            $parent = get_term($termId)->parent;

            if ($parent > 0) {
                $terms = array_merge($terms, $this->getTermParents([$parent]));
            }
        }

        return array_unique($terms);
    }

    /**
     * Retrieve current post Id even if current page is attachment
     * @since 1.0.0
     * @return int Current post Id
     */
    private function getCurrentPostId()
    {
        return is_attachment() && get_post() instanceof \WP_Post
            ? get_post()->post_parent
            : get_the_ID();
    }

    /**
     * Determine if guest/consumer has access to term/terms
     *
     * @since 1.0.0
     *
     * @param null|array|int $term Term Id or array with a list of term id's or null to get all related to the current page terms
     * @param string $taxonomy Taxonomy name
     * @param null|int $consumerId Consumer Id (optional)
     * @param bool $checkParents Check parent terms for the access. Default: false (no)
     * @return bool True if user has access to term/terms
     */
    public function hasAccessToTerm($term, $taxonomy, $consumerId = null, $checkParents = false)
    {
        if (empty($taxonomy)) {
            return false;
        }

        $protectedTerms = $this->getProtectedTermsForConsumer($consumerId, $taxonomy);
        if (empty($protectedTerms)) {
            return true;
        }

        $terms = [];

        if ($term === null) {
            $terms = get_the_terms($this->getCurrentPostId(), $taxonomy);
            if (!is_array($terms)) {
                return false;
            }
            $terms = array_keys($terms);
        } elseif (is_array($term)) {
            $terms = $term;
        } elseif ($term > 0) {
            $terms = [$term];
        }

        if ($checkParents) {
            $terms = $this->getTermParents($terms);
        }

        // get only ac-protected terms
        foreach ($terms as $key => $termId) {
            if (!$this->isTermProtected($termId, $taxonomy)) {
                unset($terms[$key]);
            }
        }

        if (empty($terms)) {
            return true;
        }

        if ($consumerId === null) {
            $consumerId = $this->consumersModule->getAuthorizedConsumerId();
        }

        // guests can't have access to the ac-protected term
        if ($consumerId < 1) {
            return false;
        }

        // if at least one term are not protected to the consumer -> grant access
        $access = false;
        foreach ($terms as $termId) {
            if (!in_array($termId, $protectedTerms)) {
                $access = true;
            }
        }

        return $access;
    }

    /**
     * Control access to posts
     *
     * @since 1.0.0
     * @internal
     *
     * @param \WP_Query $wpQuery
     * @param array $postTypes Post types in use
     * @param int $consumerId Consume Id
     * @return \WP_Query
     */
    public function preGetPosts(\WP_Query $wpQuery, array $postTypes, int $consumerId)
    {
        $processedTaxonomies = [];

        foreach ($postTypes as $postType) {

            foreach (get_object_taxonomies($postType) as $taxonomy) {

                if (in_array($taxonomy, $processedTaxonomies)) {
                    continue;
                }

                $protectedTerms = $consumerId > 0
                    ? $this->getProtectedTermsForConsumer($consumerId, $taxonomy)
                    : $this->getProtectedTerms($taxonomy);

                if (empty($protectedTerms)) {
                    continue;
                }

                $q = [
                    'relation' => 'OR',
                    [
                        'taxonomy'         => $taxonomy,
                        'field'            => 'id',
                        'terms'            => $protectedTerms,
                        'operator'         => 'NOT IN',
                        'include_children' => false
                    ]
                ];

                $allowedTerms = get_terms(['taxonomy' => $taxonomy, 'fields' => 'ids']);

                if (!empty($allowedTerms)) {
                    $q[] = [
                        'taxonomy'         => $taxonomy,
                        'field'            => 'id',
                        'terms'            => $allowedTerms,
                        'include_children' => false
                    ];
                }

                $taxes = $wpQuery->get('tax_query', []);
                $taxes[] = $q;
                $wpQuery->set('tax_query', $taxes);

                $processedTaxonomies[] = $taxonomy;
            }
        }

        return $wpQuery;
    }

    /**
     * Control access to categories
     *
     * @since 1.0.0
     * @internal
     *
     * @param \WP_Term[] $terms Terms
     * @param string[] $taxonomies Taxonomies
     * @param array $queryVars Query vars
     * @return array Filtered terms
     */
    public function getTerms($terms, $taxonomies, $queryVars)
    {
        if (
            $this->acm->isAccessLogicDisabled()
            || empty($taxonomies)
            || (isset($queryVars['ac_stop_processing']) && $queryVars['ac_stop_processing'])
            || $this->isBackend()
            || $this->isAdminPreview()
            || wp_doing_cron()
        ) {
            return $terms;
        }

        foreach ($terms as $key => $term) {

            $termId = $term->term_id ?? (int) $term;
            $taxonomy = $term->taxonomy ?? $taxonomies[0];

            if (!$this->hasAccessToTerm($termId, $taxonomy)) {
                unset($terms[$key]);
            }
        }

        return $terms;
    }

    /**
     * Remove pages from menus to which consumer don't have access
     *
     * @since 1.0.0
     * @internal
     *
     * @param array $objects An array of menu objects
     * @return array Array of objects to which consumer have access
     */
    public function wpNavMenuObjects($objects)
    {
        if ($this->isBackend()) {
            return $objects;
        }

        foreach ($objects as $key => $object) {
            if ($object->type == 'taxonomy') {
                if (!$this->hasAccessToTerm((int) $object->object_id, $object->object)) {
                    unset($objects[$key]);
                }
            }
        }

        return $objects;
    }

    /**
     * Handle access to the current page
     * Earliest hook to reliably get $post global variable
     *
     * @since 1.0.0
     * @internal
     * @global \wp_query $wp_query
     */
    public function wp()
    {
        if (is_404() || is_search() || $this->isBackendRequest() || is_singular()) {
            return;
        }

        /** WP_Query $wp_query */
        global $wp_query;

        if ($wp_query !== null) {

            $toCheck = false;
            $term =
            $taxonomy = '';

            if (is_category() && !empty($wp_query->query_vars['cat'])) {
                $term = $wp_query->query_vars['cat'];
                $taxonomy = 'category';
                $toCheck = true;
            } elseif (is_tax() && !empty($wp_query->query_vars['term']) && !empty($wp_query->query_vars['taxonomy'])) {
                $term = get_term_by('slug', $wp_query->query_vars['term'], $wp_query->query_vars['taxonomy']);
                if ($term) {
                    $term = $term->term_id;
                    $taxonomy = $wp_query->query_vars['taxonomy'];
                    $toCheck = true;
                }
            }

            if ($toCheck && !$this->hasAccessToTerm($term, $taxonomy)) {
                $this->acm->noAccessHandling();
            }
        }
    }

    /**
     * Check if current guest/consumer has access to the post
     * Note: function can be applied to the guest
     *
     * @since 1.0.0
     *
     * @param bool $access True if consumer/guest has access to the post
     * @param int $postId Post Id
     * @param null|int $consumerId Consumer Id (optional)
     * @return bool True if current guest/consumer has access to the post
     */
    public function hasAccessToPost(bool $access, int $postId, $consumerId = null): bool
    {
        if (!$access) {
            return false;
        }

        $taxonomies = get_post_taxonomies($postId);
        $terms = wp_get_object_terms($postId, $taxonomies, [
            'fields'             => 'ids',
            'ac_stop_processing' => true
        ]);

        foreach ($taxonomies as $taxonomy) {
            if (!$this->hasAccessToTerm($terms, $taxonomy, $consumerId)) {
                return false;
            }
        }

        return true;
    }
}