<?php
class TermAccessTable extends LS\WP_List_Table
{
    /** @var array */
    protected $_column_headers;

    /** @var int */
    private $groupId;

    /** @var WP_Term[] */
    private $terms;

    /**
     * @param int $groupId
     * @param WP_Term[] $terms
     */
    public function __construct($groupId, $terms)
    {
        parent::__construct([
            'singular' => 'wp_ls_acm_term',
            'plural'   => 'wp_ls_acm_terms'
        ]);

        $this->groupId = $groupId;
        $this->terms = $terms;
    }

    /**
     * @return array
     */
    public function get_columns()
    {
        return [
            'termId' => 'ID',
            'title'  => __('Title', 'ls'),
            'remove' => ''
        ];
    }

    public function prepare_items()
    {
        $list = [];

        foreach ($this->terms as $term) {
            $list[] = (object) [
                'termId' => $term->term_id,
                'title'  => $term->name
            ];
        }

        $this->items = $list;
        $this->_column_headers = [$this->get_columns(), [], $this->get_sortable_columns()];
    }

    /**
     * @param $which
     */
    public function display_tablenav($which)
    {
        echo $which == 'top' ? '' : '<br />';
    }

    /**
     * @param object $item
     * @param string $columnName
     * @return string
     */
    public function column_default($item, $columnName)
    {
        switch ($columnName) {
            case 'termId' :
                return (int) $item->termId;
            case 'title' :
                $link = get_edit_term_link((int) $item->termId);

                return empty($item->title) || empty($link) ? ' ' : '<a href="' . esc_url($link) . '" target="_blank">' . esc_html($item->title) . '</a>';
            case 'remove' :
                return '<a href="' . add_query_arg(['action' => 'acm-exclude-term', 'term_id' => $item->termId, 'group_id' => $this->groupId]) . '" class="button">' . __('Remove', 'ls') . '</a>';
            default:
                return ' ';
        }
    }
}