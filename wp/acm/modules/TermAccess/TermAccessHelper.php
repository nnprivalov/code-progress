<?php
namespace LS;

class TermAccessHelper
{
    /**
     * Retrieve ac-protected terms
     *
     * @since 1.0.0
     *
     * @param null|int $groupId Group Id or null to fetch connections from the all groups
     * @return \WP_Term[]
     */
    public function getProtectedTerms($groupId = null)
    {
        $args = [
            'number'             => 0,
            'meta_key'           => '_acm_group_id',
            'orderby'            => 'ID',
            'order'              => 'ASC',
            'ac_stop_processing' => true
        ];

        if ($groupId !== null) {
            $args['meta_value'] = $groupId;
        }

        return get_terms($args);
    }

    /**
     * Retrieve an array of terms Id's to which consumer DON'T have access
     *
     * @since 1.0.0
     *
     * @param int $consumerId Consumer Id
     * @param array $allowedGroups Allowed CA-Group Id's
     * @return int[] Terms Id's
     */
    public function getProtectedTermsForConsumer(int $consumerId, array $allowedGroups)
    {
        $terms = [];

        if (empty($allowedGroups)) {
            $terms = $this->getProtectedTerms();
        } else {

            $wpdb = LS()->wpdb;
            $query = sprintf("SELECT t.*, tt.*
                              FROM {$wpdb->prefix}terms t
                              INNER JOIN {$wpdb->prefix}termmeta tm ON tm.term_id = t.term_id AND tm.meta_key = '_acm_group_id'
                              INNER JOIN {$wpdb->prefix}term_taxonomy tt ON tt.term_id = t.term_id
                              WHERE tm.term_id NOT IN (
                                  SELECT term_id FROM {$wpdb->prefix}termmeta WHERE meta_key = '_acm_group_id' AND meta_value IN (%s)
                              )
                              GROUP BY t.term_id", implode(',', $allowedGroups));
            $result = $wpdb->get_results($query);

            if (is_array($result)) {
                foreach ($result as $item) {
                    $terms[] = new \WP_Term($item);
                }
            }
        }

        return apply_filters('ls_acm_consumer_protected_terms', $terms, $consumerId);
    }

    /**
     * Disconnect group from the term
     *
     * @since 1.0.0
     *
     * @param int $groupId Group Id
     * @param int $termId Term Id
     * @return bool True if group has been disconnected from the term
     */
    public function disconnectGroupFromTerm(int $groupId, int $termId): bool
    {
        if ($groupId < 1 || $termId < 1) {
            return false;
        }

        if (!delete_term_meta($termId, '_acm_group_id', $groupId)) {
            return false;
        }

        do_action('ls_acm_group_disconnected_from_term', $groupId, $termId);

        return true;
    }

    /**
     * Connect groups to the term
     *
     * @since 1.0.0
     *
     * @param int $termId Term Id
     * @param int[] $newGroupsIds Groups Id's connected to the term
     */
    public function updateTermConnections(int $termId, $newGroupsIds)
    {
        if ($termId > 0) {

            $oldConnectionIds = $this->getGroupIdsByTermId($termId);

            foreach (array_diff($oldConnectionIds, $newGroupsIds) as $groupId) {
                $this->disconnectGroupFromTerm($groupId, $termId);
            }

            foreach (array_diff($newGroupsIds, $oldConnectionIds) as $groupId) {
                $this->connectGroupToTerm($groupId, $termId);
            }
        }
    }

    /**
     * Delete connections between term and ACM groups
     * @since 1.0.0
     * @param int $termId Term Id
     */
    public function disconnectTermFromGroups(int $termId)
    {
        delete_term_meta($termId, '_acm_group_id');
    }

    /**
     * Connect group to the term
     *
     * @since 1.0.0
     *
     * @param int $groupId Group Id
     * @param int $termId Term id
     * @return bool True if group has been connected to the term
     */
    public function connectGroupToTerm(int $groupId, int $termId): bool
    {
        if ($groupId < 1 || $termId < 1) {
            return false;
        }

        if (!add_term_meta($termId, '_acm_group_id', $groupId)) {
            return false;
        }

        do_action('ls_acm_group_connected_to_term', $groupId, $termId);

        return true;
    }

    /**
     * Retrieve ac-protected term ID's
     *
     * @since 1.0.0
     *
     * @param null|int $groupId Group Id or null to fetch connections from the all groups
     * @return int[] Term Id's
     */
    public function getProtectedTermsIds($groupId = null)
    {
        return array_map(static function (\WP_Term $t) {
            return (int) $t->term_id;
        }, $this->getProtectedTerms($groupId));
    }

    /**
     * Retrieve a list of AC ID's connected to specified term Id
     *
     * @since 1.0.0
     *
     * @param int $termId Term Id
     * @return int[] Group Id's
     */
    public function getGroupIdsByTermId(int $termId)
    {
        return array_map('intval', get_term_meta($termId, '_acm_group_id'));
    }

    /**
     * Delete connections between taxonomies and ACM group
     *
     * @since 1.0.0
     *
     * @param int $groupId Group Id
     * @return bool True if group connections deleted
     */
    public function deleteGroupConnections(int $groupId): bool
    {
        if ($groupId < 1) {
            return false;
        }

        $wpdb = LS()->wpdb;
        $wpdb->delete($wpdb->termmeta, ['meta_key' => '_acm_group_id', 'meta_value' => $groupId]);

        return true;
    }

    /**
     * Delete all connections between posts/taxonomies and ACM group
     * @since 1.0.0
     */
    public function deleteAllGroupConnections()
    {
        $wpdb = LS()->wpdb;
        $wpdb->delete($wpdb->termmeta, ['meta_key' => '_acm_group_id']);
    }
}