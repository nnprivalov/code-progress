<?php
class ACMCode extends LS\Model
{
    /** @var int */
    protected $codeId;

    /** @var string */
    protected $code;

    /** @var int */
    protected $companyId;

    /** @var bool */
    protected $active;

    /** @var int */
    protected $limit;

    /** @var int */
    protected $lifetime;

    /** @var bool */
    protected $assignCompany;

    /** @var string */
    protected $note;

    /** @var int */
    protected $createDate;

    /** @var int */
    protected $startDate;

    /** @var int */
    protected $endDate;

    /** @var string */
    protected $emailPattern;

    /** @var bool */
    protected $drcActive;

    /** @var int */
    protected $drcDays;

    /** @var string[] */
    protected $primaryKeys = [
        'codeId'
    ];

    /** @var string[] */
    protected $dbCols = [
        'codeId',
        'code',
        'companyId',
        'active',
        'limit',
        'lifetime',
        'assignCompany',
        'note',
        'createDate',
        'startDate',
        'endDate',
        'emailPattern',
        'drcActive',
        'drcDays'
    ];

    /** @var int */
    protected $usages;

    /**
     * Set default data
     * @since 2.0.0
     * @return self
     */
    public function setDefault()
    {
        $this->codeId =
        $this->companyId =
        $this->lifetime =
        $this->createDate =
        $this->startDate =
        $this->endDate =
        $this->drcDays =
        $this->usages = 0;

        $this->limit = 1;

        $this->active =
        $this->assignCompany = true;

        $this->drcActive = false;

        $this->code =
        $this->note =
        $this->emailPattern = '';

        return $this;
    }

    /**
     * Format the data
     *
     * @since 2.0.0
     *
     * @param array $data Data to be formatted
     * @return array Formatted data
     */
    protected function format($data)
    {
        foreach ($data as $key => $value) {
            if (in_array($key, ['codeId', 'companyId', 'limit', 'lifetime', 'drcDays', 'usages'])) {
                $data[$key] = (int) $value;
            } elseif (in_array($key, ['active', 'drcActive', 'assignCompany'])) {
                $data[$key] = (bool) $value;
            } elseif (in_array($key, ['createDate', 'startDate', 'endDate'])) {
                $data[$key] = max(0, !empty($value) && !is_numeric($value) ? strtotime($value) : (int) $value);
            }
        }

        return $data;
    }

    /**
     * Check if the code is valid
     * @since 2.0.0
     * @return bool True if the code is valid
     */
    public function valid()
    {
        return !empty($this->getCode())
               && $this->getStartDate() > 0
               && $this->getEndDate() > 0
               && $this->getEndDate() >= $this->getStartDate();
    }

    /**
     * Check if the code has the limit
     * @since 2.0.0
     * @return bool True if code has the limit
     */
    public function hasLimit()
    {
        return $this->getLimit() > 0;
    }

    /**
     * Check if the limit of usages has been reached
     * @since 2.0.0
     * @return bool True if limit has been reached
     */
    public function isLimitReached()
    {
        return $this->hasLimit() && $this->getUsages() >= $this->getLimit();
    }

    /**
     * Retrieve code Id
     * @since 2.0.0
     * @return int Code Id
     */
    public function getCodeId()
    {
        return $this->codeId;
    }

    /**
     * Set code Id
     *
     * @since 3.1.0
     * @param int $codeId Code Id
     *
     * @return self
     */
    public function setCodeId($codeId)
    {
        $this->codeId = (int) $codeId;

        return $this;
    }

    /**
     * Retrieve company Id
     * @since 3.0.0
     * @return int Company Id
     */
    public function getCompanyId()
    {
        return $this->companyId;
    }

    /**
     * Set company Id
     *
     * @since 3.1.0
     * @param int $companyId Company Id
     *
     * @return self
     */
    public function setCompanyId($companyId)
    {
        $this->companyId = (int) $companyId;

        return $this;
    }

    /**
     * Retrieve code
     * @since 2.0.0
     * @return string Code value
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set code
     *
     * @since 3.1.0
     * @param string $code Code
     *
     * @return self
     */
    public function setCode($code)
    {
        $this->code = trim($code);

        return $this;
    }

    /**
     * Retrieve limit value
     * @since 2.0.0
     * @return int Limit value
     */
    public function getLimit()
    {
        return $this->limit;
    }

    /**
     * Set code usages limit value
     *
     * @since 3.1.0
     * @param int $limit Limit
     *
     * @return self
     */
    public function setLimit($limit)
    {
        $this->limit = (int) $limit;

        return $this;
    }

    /**
     * Retrieve lifetime
     * @since 2.0.0
     * @return int Lifetime in days
     */
    public function getLifetime()
    {
        return $this->lifetime;
    }

    /**
     * Set code lifetime
     *
     * @since 3.1.0
     * @param int $lifetime Lifetime in days
     *
     * @return self
     */
    public function setLifetime($lifetime)
    {
        $this->lifetime = (int) $lifetime;

        return $this;
    }

    /**
     * Check if the code has lifetime
     * @since 2.0.0
     * @return bool True if the code has lifetime
     */
    public function hasLifetime()
    {
        return $this->getLifetime() > 0;
    }

    /**
     * Check if possible to automatically assign company to the consumer who entered the code of this group
     * @since 3.0.0
     * @return bool
     */
    public function canAssignCompany()
    {
        return $this->assignCompany && $this->getCompanyId() > 0;
    }

    /**
     * Check option "Assign company"
     *
     * @since 3.0.0
     *
     * @param bool $assign True to mark option as checked
     * @return self
     */
    public function setAssignCompany($assign)
    {
        $this->assignCompany = (bool) $assign;

        return $this;
    }

    /**
     * Retrieve note
     * @since 2.0.0
     * @return string Note
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * Set note
     *
     * @since 3.1.0
     * @param string $note Note
     *
     * @return self
     */
    public function setNote($note)
    {
        $this->note = trim($note);

        return $this;
    }

    /**
     * Retrieve create date
     * @since 2.0.0
     * @return int Timestamp
     */
    public function getCreateDate()
    {
        return $this->createDate;
    }

    /**
     * Set create date
     *
     * @since 3.1.0
     * @param int $createDate Create date
     *
     * @return self
     */
    public function setCreateDate($createDate)
    {
        $this->createDate = (int) $createDate;

        return $this;
    }

    /**
     * Retrieve start date
     * @since 3.0.0
     * @return int Timestamp
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set code start date
     *
     * @since 3.1.0
     * @param int $startDate Start date
     *
     * @return self
     */
    public function setStartDate($startDate)
    {
        $this->startDate = (int) $startDate;

        return $this;
    }

    /**
     * Retrieve end date
     * @since 3.0.0
     * @return int Timestamp
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * Set code end date
     *
     * @since 3.1.0
     * @param int $endDate End date
     *
     * @return self
     */
    public function setEndDate($endDate)
    {
        $this->endDate = (int) $endDate;

        return $this;
    }

    /**
     * Check if the code is active
     * @since 2.0.0
     * @return bool True if the code is active
     */
    public function isActive()
    {
        return $this->active;
    }

    /**
     * Mark code as active
     * @since 3.0.0
     * @return self
     */
    public function markAsActive()
    {
        $this->active = true;

        return $this;
    }

    /**
     * Mark code as inactive
     * @since 3.0.0
     * @return self
     */
    public function markAsInactive()
    {
        $this->active = false;

        return $this;
    }

    /**
     * Check if the feature to check email pattern is active
     * @since 3.0.0
     * @return bool True if email pattern feature is active
     */
    public function isEmailPatternActive()
    {
        return !empty($this->getEmailPattern());
    }

    /**
     * Retrieve email pattern
     * @since 3.0.0
     * @return string Email pattern
     */
    public function getEmailPattern()
    {
        return $this->emailPattern;
    }

    /**
     * Set email pattern
     *
     * @since 3.1.0
     * @param string $pattern Email pattern
     *
     * @return self
     */
    public function setEmailPattern($pattern)
    {
        $this->emailPattern = $pattern;

        return $this;
    }

    /**
     * Match email with the email pattern
     *
     * @since 3.0.0
     *
     * @param string $email Email to match
     * @return bool True if email matched with pattern
     */
    public function matchEmail($email)
    {
        $pattern = $this->getEmailPattern();

        if (empty($email) || empty($pattern)) {
            return false;
        }

        $patterns = array_map('trim', explode(',', $pattern));

        if (in_array($email, $patterns)) {
            return true;
        }

        foreach ($patterns as $p) {

            $p = preg_replace('~([\[^$.|?*+()\\\])~', '\\\$0', $p); // format string to regular expression
            $p = '#^' . str_replace('\*', '.+', $p) . '$#i'; // replace * with accessible range of chars

            if (preg_match($p, $email)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Check if the feature to deactivate consumers is active
     * @since 3.0.0
     * @return bool True if DRC is active
     */
    public function isDRCActive()
    {
        return $this->drcActive;
    }

    /**
     * Activate feature to deactivate consumers
     * @since 3.1.0
     * @return self
     */
    public function markDRCAsActive()
    {
        $this->drcActive = true;

        return $this;
    }

    /**
     * Deactivate feature to deactivate consumers
     * @since 3.1.0
     * @return self
     */
    public function markDRCAsInactive()
    {
        $this->drcActive = false;

        return $this;
    }

    /**
     * Retrieve DRC number of days
     * @since 3.0.0
     * @return int DRC days
     */
    public function getDRCDays()
    {
        return $this->drcDays;
    }

    /**
     * Set DRC number of days
     *
     * @since 3.1.0
     * @param int $days DRC days
     *
     * @return self
     */
    public function setDRCDays($days)
    {
        $this->drcDays = (int) $days;

        return $this;
    }

    /**
     * Retrieve number of code usages
     * @since 2.0.0
     * @return int|null Amount of usages or null if amount is unknown
     */
    public function getUsages()
    {
        return $this->usages;
    }

    /**
     * Set number of code usages
     *
     * @since 3.6.1
     *
     * @param int $usages Number of code usages
     * @return $this
     */
    public function setUsages(int $usages)
    {
        $this->usages = $usages;

        return $this;
    }

    /**
     * Check if code has been expired
     * @since 3.0.0
     * @return bool True if code has been expired
     */
    public function isExpired()
    {
        return time() > ($this->getEndDate() + DAY_IN_SECONDS - 1);
    }

    /**
     * Check if code will be active in the future
     * @since 3.0.0
     * @return bool True if code will be active in the future
     */
    public function isUpcoming()
    {
        return $this->getStartDate() > time();
    }

    /**
     * Check if code is actual
     * @since 3.0.0
     * @return bool True if code is actual
     */
    public function isActual()
    {
        return $this->isActive() && !$this->isExpired() && !$this->isUpcoming();
    }

    /**
     * Check if code can be entered
     * @since 3.0.0
     * @return bool True if code can be used
     */
    public function canBeUsed()
    {
        return $this->isActual() && !$this->isLimitReached();
    }

    /**
     * Retrieve record of the object data which can be stored in the database table
     *
     * @since 2.0.0
     *
     * @param bool $withPrimaryKeys True to include primary keys data
     * @return array An array of columns and their values
     */
    public function getRecord($withPrimaryKeys = false)
    {
        $data = [];

        foreach ($this->getDbColumns($withPrimaryKeys) as $col) {
            if (in_array($col, ['active', 'drcActive', 'assignCompany'])) {
                $data[$col] = (int) $this->{$col};
            } elseif ($col == 'startDate' || $col == 'endDate') {
                $data[$col] = date('Y-m-d', $this->{$col});
            } else {
                $data[$col] = $this->{$col};
            }
        }

        return $data;
    }
}