jQuery(document).ready(function($) {

    // Taxonomies views -> add ACM column
    (function() {

        if(!$('.wp-list-table .column-acmGroupId').length) {
            return;
        }

        var xhr = false;

        var renderMS = function() {

            $('select.acm-multiple-select:not(.data-rendered)').each(function() {

                var termId = $(this).data('term-id'),
                    _this = $(this).multipleSelect({
                    selectAll: false,
                    countSelected: 0,
                    width: '100%',
                    placeholder: 'Select',
                    onOpen: function() {
                        $(_this).closest('table').find('.column-acmGroupId').width('400px');
                    },
                    onClose: function() {
                        $(_this).closest('table').find('.column-acmGroupId').width('auto');
                    },
                    onClick: function() {

                        if(xhr) {
                            xhr.abort();
                        }

                        xhr = $.post(ajaxurl, {
                                action: 'saveACMTerms',
                                termId: termId,
                                groups: $(_this).multipleSelect('getSelects')
                            }
                        );
                    }
                });

                _this.attr('data-rendered', 1);
            });
        };

        renderMS();

        $('#addtag .button, .inline-edit-row .save').click(function() {
            setTimeout(function() {
                renderMS();
            }, 2000);
        });

    })();
});