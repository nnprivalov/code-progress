jQuery(document).ready(function($) {

    addDeleteConfirmation('delete-acm-groups');
    addDeleteConfirmation('delete-acm-codes');

    // Activity block
    initActivitiesChart(
        '.acm-activity',
        '#acmActivitiesMultipleSelect',
        '#statsSD',
        '#statsED',
        'acmActivities',
        {
            groupId: '#groupForActivity'
        }
    );

    // Codes table
    if($('#ls-codes').length) {

        $('.acm-code-desc').click(function() {

            var f = $(this).hide().parent().append(
                '<div class="acm-code-edit">' +
                '<input type="text" maxlength="400" />' +
                '<a href="#"><span class="disket"></span></a>' +
                '</div>'
            );

            var text = $(this).text();

            text = text === '+' ? '' : text;

            f.find('input').val(text);

            return false;
        });

        $('.wp_ls_acm_codes').on('click', '.acm-code-edit a', function() {

            var bx = $(this).closest('td'),
                codeId = bx.find('a.acm-code-desc').data('id'),
                note = bx.find('input').val();

            $.post(ajaxurl, {
                    action: 'acmChangeNote',
                    codeId: codeId,
                    note: note
                }
            );

            bx.find('.acm-code-edit').remove();
            bx.find('.acm-code-desc').html(note).removeClass('acm-tm').show();

            return false;

        }).on('mouseover', 'td', function() {

            var note = $(this).closest('tr').find('.acm-code-desc');
            if(note.text() === ' ') {
                note.addClass('acm-tm').html('+');
            }

        }).on('mouseout', 'td', function() {

            var note = $(this).closest('tr').find('.acm-code-desc');
            if(note.hasClass('acm-tm')) {
                note.removeClass('acm-tm').html(' ');
            }
        });
    }

    var acmSwitcher = function(cbField, vField) {

        var val = vField.val() || '0';

        if(val.length && val !== '0') {
            cbField.attr('checked', 'checked');
        }

        if(!cbField.is(':checked')) {
            vField.attr('disabled', 'disabled');
        }

        cbField.change(function() {
            if($(this).is(':checked')) {
                vField.removeAttr('disabled');
            } else {
                vField.attr('disabled', 'disabled');
            }
        });
    };

    // Access Code form extras
    if($('.acm-code-fields').length) {
        acmSwitcher($('input[name="hasLimit"]'), $('input[name="limit"]'));
        acmSwitcher($('input[name="hasLifetime"]'), $('input[name="lifetime"]'));
        acmSwitcher($('input[name="hasEP"]'), $('input[name="emailPattern"]'));
        acmSwitcher($('input[name="drcActive"]'), $('input[name="drcDays"]'));
    }

    // Codes prolongation
    var codes = $('.prolongation-codes #codes');
    if(codes.length) {
        codes.multipleSelect({
            width: '600px',
            filter: true,
            onCheckAll: function() {
                $('#codesValues').val($('#codes').multipleSelect("getSelects"));
            },
            onClick: function() {
                $('#codesValues').val($('#codes').multipleSelect("getSelects"));
            }
        });
    }
});