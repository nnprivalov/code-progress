<?php
class ACMConsumerCodeConnection extends LS\Model
{
    /** @var int */
    protected $ccId;

    /** @var int */
    protected $consumerId;

    /** @var int */
    protected $codeId;

    /** @var int */
    protected $enterDate;

    /** @var int */
    protected $endDate;

    /** @var string[] */
    protected $primaryKeys = [
        'ccId'
    ];

    /** @var string[] */
    protected $dbCols = [
        'ccId',
        'consumerId',
        'codeId',
        'enterDate',
        'endDate'
    ];

    /**
     * Format the data
     *
     * @since 2.0.0
     *
     * @param array $data Data to be formatted
     * @return array Formatted data
     */
    protected function format($data)
    {
        foreach ($data as $key => $value) {
            if ($key == 'enterDate' || $key == 'endDate') {
                $data[$key] = max(0, !empty($value) && !is_numeric($value) ? strtotime($value) : (int) $value);
            } else {
                $data[$key] = (int) $value;
            }
        }

        return $data;
    }

    /**
     * Set default data
     * @since 2.0.0
     * @return self
     */
    public function setDefault()
    {
        $this->ccId =
        $this->codeId =
        $this->consumerId =
        $this->enterDate =
        $this->endDate = 0;

        return $this;
    }

    /**
     * Check if the record is valid
     * @since 2.0.0
     * @return bool True if the record is valid
     */
    public function valid()
    {
        return $this->getConsumerId() > 0 && $this->getCodeId() > 0 && $this->getEndDate() > 0;
    }

    /**
     * Retrieve record Id
     * @since 3.2.0
     * @return int Record Id
     */
    public function getId()
    {
        return $this->ccId;
    }

    /**
     * Retrieve consumer Id
     * @since 2.0.0
     * @return int Consumer Id
     */
    public function getConsumerId()
    {
        return $this->consumerId;
    }

    /**
     * Retrieve code Id
     * @since 2.0.0
     * @return int Code Id
     */
    public function getCodeId()
    {
        return $this->codeId;
    }

    /**
     * Retrieve enter date
     * @since 2.0.0
     * @return int Timestamp
     */
    public function getEnterDate()
    {
        return $this->enterDate;
    }

    /**
     * Retrieve end date
     * @since 3.0.0
     * @return int Timestamp
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * Retrieve last second when the code works for the consumer
     * @since 3.0.0
     * @return int Timestamp
     */
    public function getLastValidTimestamp()
    {
        return $this->getEndDate() + DAY_IN_SECONDS;
    }

    /**
     * Check if code connection is expired
     * @since 3.0.0
     * @return bool True if code connection is expired
     */
    public function isExpired()
    {
        return time() >= $this->getLastValidTimestamp();
    }

    /**
     * Check if code connection is actual
     * @since 3.0.0
     * @return bool True if code connection is actual
     */
    public function isActual()
    {
        return !$this->isExpired();
    }

    /**
     * Retrieve record of the object data which can be stored in the database table
     *
     * @since 3.0.0
     *
     * @param bool $withPrimaryKeys True to include primary keys data
     * @return array An array of columns and their values
     */
    public function getRecord($withPrimaryKeys = false)
    {
        $data = [];

        foreach ($this->getDbColumns($withPrimaryKeys) as $col) {
            if ($col == 'endDate') {
                $data[$col] = date('Y-m-d', $this->{$col});
            } else {
                $data[$col] = $this->{$col};
            }
        }

        return $data;
    }
}