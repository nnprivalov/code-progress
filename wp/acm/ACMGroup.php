<?php
class ACMGroup extends LS\Model
{
    /** @var int */
    protected $groupId;

    /** @var int */
    protected $parentGroupId;

    /** @var string */
    protected $name;

    /** @var string */
    protected $description;

    /** @var bool */
    protected $active;

    /** @var int */
    protected $createDate;

    protected $primaryKeys = [
        'groupId'
    ];

    /** @var string[] */
    protected $dbCols = [
        'groupId',
        'parentGroupId',
        'name',
        'description',
        'active',
        'active',
        'createDate'
    ];

    /**
     * Set default data
     * @since 2.0.0
     * @return self
     */
    public function setDefault()
    {
        $this->groupId =
        $this->parentGroupId =
        $this->createDate = 0;

        $this->active = true;

        $this->name =
        $this->description = '';

        return $this;
    }

    /**
     * Format the data
     *
     * @since 2.0.0
     *
     * @param array $data Data to be formatted
     * @return array Formatted data
     */
    protected function format($data)
    {
        foreach ($data as $key => $value) {
            if (in_array($key, ['groupId', 'parentGroupId'])) {
                $data[$key] = (int) $value;
            } elseif ($key == 'createDate') {
                $data[$key] = max(0, !empty($value) && !is_numeric($value) ? strtotime($value) : (int) $value);
            } elseif ($key == 'active') {
                $data[$key] = (bool) $value;
            }
        }

        return $data;
    }

    /**
     * Check if the group is valid
     * @since 2.0.0
     * @return bool True if the group is valid
     */
    public function valid()
    {
        return !empty($this->getName());
    }

    /**
     * Retrieve group Id
     * @since 2.0.0
     * @return int Group Id
     */
    public function getGroupId()
    {
        return $this->groupId;
    }

    /**
     * Set group Id
     *
     * @since 3.1.0
     *
     * @param int $groupId Group Id
     * @return self
     */
    public function setGroupId($groupId)
    {
        $this->groupId = (int) $groupId;

        return $this;
    }

    /**
     * Retrieve parent group Id
     * @since 2.0.0
     * @return int Group Id
     */
    public function getParentGroupId()
    {
        return $this->parentGroupId;
    }

    /**
     * Set parent group Id
     *
     * @since 3.0.0
     *
     * @param int $groupId Group Id
     * @return self
     */
    public function setParentGroupId($groupId)
    {
        $this->parentGroupId = (int) $groupId;

        return $this;
    }

    /**
     * Retrieve group name
     * @since 2.0.0
     * @return string Group name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set group name
     *
     * @since 3.1.0
     *
     * @param string $name Group name
     * @return self
     */
    public function setName($name)
    {
        $this->name = trim($name);

        return $this;
    }

    /**
     * Retrieve description
     * @since 2.0.0
     * @return string Description
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set group description
     *
     * @since 3.1.0
     *
     * @param string $desc Group description
     * @return self
     */
    public function setDescription($desc)
    {
        $this->description = trim($desc);

        return $this;
    }

    /**
     * Check if group is active
     * @since 2.0.0
     * @return bool True if group is active
     */
    public function isActive()
    {
        return $this->active;
    }

    /**
     * Mark group as active
     * @since 2.4.12
     * @return self
     */
    public function markAsActive()
    {
        $this->active = true;

        return $this;
    }

    /**
     * Mark group as inactive
     * @since 2.4.12
     * @return self
     */
    public function markAsInactive()
    {
        $this->active = false;

        return $this;
    }

    /**
     * Retrieve create date
     * @since 2.0.0
     * @return int Create date
     */
    public function getCreateDate()
    {
        return $this->createDate;
    }

    /**
     * Set group create date
     *
     * @since 3.1.0
     *
     * @param int $createDate Create date
     * @return self
     */
    public function setCreateDate($createDate)
    {
        $this->createDate = (int) $createDate;

        return $this;
    }

    /**
     * Retrieve record of the object data which can be stored in the database table
     *
     * @since 2.0.0
     *
     * @param bool $withPrimaryKeys True to include primary keys data
     * @return array An array of columns and their values
     */
    public function getRecord($withPrimaryKeys = false)
    {
        $data = [];

        foreach ($this->getDbColumns($withPrimaryKeys) as $col) {
            if ($col == 'active') {
                $data[$col] = (int) $this->{$col};
            } else {
                $data[$col] = $this->{$col};
            }
        }

        return $data;
    }
}