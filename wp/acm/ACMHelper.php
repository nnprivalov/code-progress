<?php
class ACMHelper
{
    /** @var ACMGroup[] */
    private static $consumerGroups = [];

    private static function cleanCaches()
    {
        wp_cache_delete('groups_list', 'acm');
        wp_cache_delete('groups_list_hierarchical', 'acm');
    }

    /**
     * ##################  GROUPS  ########################
     */

    /**
     * Create new group
     *
     * @since 2.0.0
     *
     * @param ACMGroup $group Group object
     * @return int New AC-Group Id
     */
    public static function createGroup(ACMGroup $group): int
    {
        if (!$group->valid()) {
            return 0;
        }

        if ($group->getCreateDate() == 0) {
            $group->setCreateDate(time());
        }

        $wpdb = LS()->wpdb;

        if (!$wpdb->insert($wpdb->prefix . 'ls_acm_groups', $group->getRecord())) {
            throw new Exception('Can not create group');
        }

        $group->setGroupId((int) $wpdb->insert_id);

        do_action('ls_acm_group_created', $group);

        self::cleanCaches();

        return $group->getGroupId();
    }

    /**
     * Update group
     *
     * @since 2.0.0
     *
     * @param ACMGroup $group Group object
     * @return bool True if updated or false on failure
     */
    public static function updateGroup(ACMGroup $group): bool
    {
        if (!$group->valid() || $group->getGroupId() < 1) {
            return false;
        }

        \LS\TransactionManager::start();

        try {

            $wpdb = LS()->wpdb;
            $wpdb->update($wpdb->prefix . 'ls_acm_groups', $group->getRecord(), ['groupId' => $group->getGroupId()], null, '%d');

            if (!$group->isActive()) {
                self::deactivateAccessCodes(
                    self::getAccessCodesByGroupId($group->getGroupId())
                );
            }

            \LS\TransactionManager::commit();

            $success = true;

        } catch (Exception $e) {
            $success = false;
            \LS\TransactionManager::rollback();
            _doing_it_wrong(__FUNCTION__, $e->getMessage(), '3.5.0');
        }

        if ($success) {
            do_action('ls_acm_group_updated', $group);
        }

        self::cleanCaches();

        return $success;
    }

    /**
     * Delete group
     *
     * @since 2.0.0
     *
     * @param int $groupId Group Id
     * @return bool True if record has been deleted
     */
    public static function deleteGroup($groupId): bool
    {
        if ($groupId < 1) {
            return false;
        }

        \LS\TransactionManager::start();

        try {

            foreach (self::getChildrenGroupsIdsTo($groupId) as $childGroupId) {
                self::deleteGroup($childGroupId);
            }

            foreach (self::getConsumersConnectedToGroup($groupId) as $consumerId) {
                if (!self::disconnectConsumerFromGroup($consumerId, $groupId)) {
                    throw new Exception('Can not disconnect consumer from group');
                }
            }

            if (!self::deleteGroupConnections($groupId)) {
                throw new Exception('Can not delete group connections');
            }

            foreach (self::getAccessCodesByGroupId($groupId) as $code) {

                if (!self::disconnectAccessCodeFromGroup($code->getCodeId(), $groupId)) {
                    throw new Exception('Can not disconnect group for the access code');
                }

                if (!self::updateAccessCode($code)) {
                    throw new Exception('Can not update access code');
                }
            }

            $wpdb = LS()->wpdb;

            if ($wpdb->delete($wpdb->prefix . 'ls_acm_groups', ['groupId' => $groupId], '%d') === false) {
                throw new Exception('Can not delete group');
            }

            \LS\TransactionManager::commit();

            $success = true;

        } catch (Exception $e) {
            $success = false;
            \LS\TransactionManager::rollback();
            _doing_it_wrong(__FUNCTION__, $e->getMessage(), '3.5.0');
        }

        if ($success) {
            do_action('ls_acm_group_deleted', $groupId);
        }

        self::cleanCaches();

        return $success;
    }

    /**
     * Retrieve paginated data of groups by specified conditions and filters
     *
     * @since 2.0.0
     *
     * @param int $itemsPerPage Number of groups to retrieve
     * @param int $page Page number for pagination. To get all groups without pagination set this value to null
     * @param string $orderBy Field to order
     * @param string $order Order direction
     * @param string $searchTerm Search query
     * @param array $filters Filters
     * @return array Array(
     *   'rows' - array of ACMHierarchicalGroup's,
     *   'totalCount' - total number of found groups,
     *   'totalPages' - total number of pages (for pagination),
     *   'itemsPerPage' - number of groups per page
     * )
     */
    public static function paginateGroups($itemsPerPage = 20, $page = 1, $orderBy = '', $order = 'ASC', $searchTerm = '', $filters = []): array
    {
        // @TODO Can be changed to type hinting after 09.2019
        $itemsPerPage = \LS\Library::toInt($itemsPerPage);
        $page = \LS\Library::toInt($page);

        $wpdb = LS()->wpdb;
        $selectExtra = '';
        $joins =
        $whereConditions = [];
        $order = empty($order) ? 'ASC' : $order;

        if (in_array($orderBy, ['groupId', 'name', 'description', 'createDate', 'active'])) {
            $orderBy = 'g.' . $orderBy;
        } elseif ($orderBy == 'consumers') {
            $selectExtra .= ', COUNT(cg.consumerId) consumers';
            $joins['cg'] = sprintf("LEFT JOIN {$wpdb->prefix}ls_acm_consumers_groups cg ON cg.groupId = g.groupId");
            $orderBy = 'consumers';
        } else {
            $orderBy = 'g.name';
        }

        if (!empty($searchTerm)) {

            $columns = [
                'g.name',
                'g.description'
            ];

            $searchConditions = [];
            foreach ($columns as $c) {
                $searchConditions[] = sprintf("%s LIKE '%%%s%%'", $c, esc_sql(str_replace('\\', '\\\\', $searchTerm)));
            }

            if (!empty($searchConditions)) {
                $whereConditions[] = implode(' OR ', $searchConditions);
            }
        }

        if (!empty($filters)) {

            $filtersWhere = [];

            foreach ($filters as $filter) {

                if (!isset($filter['r'])) {
                    continue;
                }

                $field = $filter['r'];
                $condition = $filter['c'] ?? '';
                $value = $filter['t'] ?? '';

                if (in_array($field, ['groupId', 'name', 'description', 'createDate', 'active'])) {
                    $record = 'g.' . $field;
                } else {
                    $record = '';
                }

                if (!empty($record)) {
                    $filterQuery = LS\Library::generateQueryFromFilter($record, $condition, $value);
                    if (!empty($filterQuery)) {
                        $filtersWhere[] = $filterQuery;
                    }
                }
            }

            if (!empty($filtersWhere)) {
                $xor = isset($filters['x']) && in_array($filters['x'], ['AND', 'OR']) ? $filters['x'] : 'AND';
                $whereConditions[] = '(' . implode(') ' . $xor . ' (', $filtersWhere) . ')';
            }
        }

        $where = empty($whereConditions) ? '' : sprintf(" WHERE (%s)", implode(') AND (', $whereConditions));
        $joins = implode(' ', $joins);

        $query = sprintf("SELECT g.groupId, g.parentGroupId %s
                          FROM {$wpdb->prefix}ls_acm_groups g
                          %s %s
                          GROUP BY g.groupId
                          ORDER BY %s %s", $selectExtra, $joins, $where, $orderBy, $order);

        $groupsRelations = $wpdb->get_results($query, OBJECT_K);
        $groupsRelations = is_array($groupsRelations) ? $groupsRelations : [];

        $totalCount = count($groupsRelations);
        $itemsPerPage = $itemsPerPage > 0 ? $itemsPerPage : $totalCount;
        $totalPages = $totalCount > 0 && $itemsPerPage > 0 ? ceil($totalCount / $itemsPerPage) : 0;

        $records = [];

        if ($totalCount > 0) {

            $groupByParent = static function ($relations, $groupId = 0) use (&$groupByParent) {

                $groups = [];

                foreach ($relations as $item) {
                    if ($item->parentGroupId == $groupId) {
                        $groups = array_merge($groups, [(int) $item->groupId], $groupByParent($relations, $item->groupId));
                    }
                }

                return $groups;
            };

            // group groups by hierarchy
            $groupsIds = $groupByParent($groupsRelations);

            // add groups without parents in the result stack
            foreach ($groupsRelations as $relation) {
                if (!in_array($relation->groupId, $groupsIds)) {
                    $groupsIds[] = $relation->groupId;
                }
            }

            if ($page > 0 && $itemsPerPage > 0) {
                $groupsIds = array_slice($groupsIds, ($page - 1) * $itemsPerPage, $itemsPerPage);
            }

            if (!empty($groupsIds)) {

                $query = sprintf("SELECT g.*
                                  FROM {$wpdb->prefix}ls_acm_groups g 
                                  WHERE g.groupId IN (%s) 
                                  ORDER BY FIELD(g.groupId, %s)", implode(',', $groupsIds), implode(',', $groupsIds));
                $groups = $wpdb->get_results($query);
                $groups = is_array($groups) ? $groups : [];

                $getGroupLevel = static function ($groupId, $level = 0) use (&$getGroupLevel, $groupsRelations) {

                    // groups without parents always should have level 0
                    if (!isset($groupsRelations[$groupId])) {
                        return 0;
                    }

                    return $groupsRelations[$groupId]->parentGroupId > 0 ? $getGroupLevel($groupsRelations[$groupId]->parentGroupId, $level + 1) : $level;
                };

                foreach ($groups as $key => $record) {
                    $records[] = new ACMHierarchicalGroup($record, $getGroupLevel($record->groupId));
                }
            }
        }

        return [
            'rows'         => $records,
            'totalCount'   => $totalCount,
            'totalPages'   => $totalPages,
            'itemsPerPage' => $itemsPerPage
        ];
    }

    /**
     * Retrieve groups by specified conditions and filters
     *
     * @since 2.0.0
     *
     * @param string $orderBy Field to order
     * @param string $order Order direction
     * @param string $searchTerm Search query
     * @param array $filters Filters
     * @return ACMHierarchicalGroup[]
     */
    public static function getGroups($orderBy = '', $order = '', $searchTerm = '', $filters = [])
    {
        return self::paginateGroups(0, 0, $orderBy, $order, $searchTerm, $filters)['rows'];
    }

    /**
     * Retrieve groups by specified group Id's
     *
     * @since 2.0.0
     *
     * @param int[] $ids An array of group Id's
     * @return ACMGroup[]
     */
    public static function getGroupsByIds($ids)
    {
        $wpdb = LS()->wpdb;
        $groups = [];

        foreach (array_chunk($ids, 50) as $idsPack) {

            $query = sprintf("SELECT g.* 
                              FROM {$wpdb->prefix}ls_acm_groups g 
                              WHERE g.groupId IN (%s) 
                              GROUP BY g.groupId", implode(',', $idsPack));

            $result = $wpdb->get_results($query);

            if (is_array($result)) {
                foreach ($result as $record) {
                    $groups[] = new ACMGroup($record);
                }
            }
        }

        return $groups;
    }

    /**
     * Retrieve a list of groups
     *
     * @since 2.0.0
     *
     * @param bool $hierarchical True to retrieve in hierarchical view (parents-children)
     * @return array[int]string An array of groups as key pairs: group_id => group_name
     */
    public static function getGroupsList($hierarchical = false)
    {
        // @TODO Can be changed to type hinting after 09.2019
        $hierarchical = \LS\Library::toBool($hierarchical);

        $cacheName = 'groups_list' . ($hierarchical ? '_hierarchical' : '');
        $result = wp_cache_get($cacheName, 'acm');

        if ($result === false) {

            $result = [];
            foreach (self::getGroups() as $group) {
                $result[$group->getGroupId()] = ($hierarchical ? str_repeat('&#8212; ', $group->getLevel()) : '') . $group->getName();
            }

            wp_cache_add($cacheName, $result, 'acm', HOUR_IN_SECONDS);
        }

        return $result;
    }

    /**
     * Retrieve group by Id
     *
     * @since 2.0.0
     *
     * @param int $groupId Group Id
     * @return false|ACMGroup Group object or false on failure
     */
    public static function getGroup($groupId)
    {
        // @TODO Can be changed to type hinting after 09.2019
        $groupId = \LS\Library::toInt($groupId);

        $group = false;

        if ($groupId > 0) {
            $wpdb = LS()->wpdb;
            $group = $wpdb->get_row(sprintf("SELECT g.* FROM {$wpdb->prefix}ls_acm_groups g WHERE g.groupId = %d LIMIT 1", $groupId));
            $group = empty($group) ? false : new ACMGroup($group);
        }

        return $group;
    }

    /**
     * Retrieve ID's of the child groups
     *
     * @since 3.0.5
     *
     * @param int $groupId Parent group Id
     * @param bool $activeOnly True to get active groups only
     * @param int[] $ids Internal param / not in use
     * @return int[] Group Id's
     */
    private static function getChildrenGroupsIdsToInternal(int $groupId, bool $activeOnly = false, $ids = [])
    {
        if ($groupId > 0) {

            $wpdb = LS()->wpdb;
            $activeOnly = $activeOnly ? sprintf("AND active = 1 AND '%s' BETWEEN startDate AND endDate", date('Y-m-d')) : '';
            $result = $wpdb->get_col(sprintf("SELECT groupId FROM {$wpdb->prefix}ls_acm_groups WHERE parentGroupId = %d %s", $groupId, $activeOnly));
            $result = is_array($result) ? array_map('intval', $result) : [];

            foreach ($result as $newId) {
                $ids = array_merge(
                    [$newId],
                    self::getChildrenGroupsIdsToInternal($newId, $activeOnly, $ids)
                );
            }
        }

        return array_unique($ids);
    }

    /**
     * Retrieve ID's of the child groups
     *
     * @since 2.4.3
     *
     * @param int $groupId Parent group Id
     * @param bool $activeOnly True to get active groups only
     * @return int[] Group Id's
     */
    public static function getChildrenGroupsIdsTo($groupId, $activeOnly = false)
    {
        // @TODO Can be changed to type hinting after 09.2019
        $groupId = \LS\Library::toInt($groupId);
        $activeOnly = \LS\Library::toBool($activeOnly);

        return self::getChildrenGroupsIdsToInternal($groupId, $activeOnly);
    }

    /**
     * Retrieve ID's of the children groups together with parent group Id
     *
     * @since 2.4.3
     *
     * @param int $groupId Parent group Id
     * @param bool $activeOnly True to get active groups only
     * @return int[] Group Id's
     */
    public static function includeChildrenGroupsIds($groupId, $activeOnly = false)
    {
        // @TODO Can be changed to type hinting after 09.2019
        $groupId = \LS\Library::toInt($groupId);
        $activeOnly = \LS\Library::toBool($activeOnly);

        return $groupId > 0 ? array_merge([$groupId], self::getChildrenGroupsIdsTo($groupId, $activeOnly)) : [];
    }

    /**
     * ##################  ACCESS CODES  ########################
     */

    /**
     * Create Access Code
     *
     * @since 2.0.0
     *
     * @param ACMCode $code Code object to be created
     * @param int[] $groupIds CA-Group Id's to connect to the AC
     * @return int Created code Id
     */
    public static function createAccessCode(ACMCode $code, $groupIds = []): int
    {
        if (!$code->valid()) {
            return 0;
        }

        if ($code->getCreateDate() == 0) {
            $code->setCreateDate(time());
        }

        \LS\TransactionManager::start();

        try {

            $wpdb = LS()->wpdb;

            if (!$wpdb->insert($wpdb->prefix . 'ls_acm_codes', $code->getRecord())) {
                throw new Exception('Can not create code');
            }

            $code->setCodeId((int) $wpdb->insert_id);

            foreach ($groupIds as $groupId) {
                if (!self::hasAccessCodeConnectionToGroup($code->getCodeId(), $groupId)) {
                    if (!self::connectAccessCodeToGroup($code->getCodeId(), $groupId)) {
                        throw new Exception(sprintf('Can not connect code (%d) to group (%d)', $code->getCode(), $groupId));
                    }
                }
            }

            \LS\TransactionManager::commit();

            $success = true;

        } catch (Exception $e) {
            $success = false;
            \LS\TransactionManager::rollback();
            _doing_it_wrong(__FUNCTION__, $e->getMessage(), '3.5.0');
        }

        if ($success) {
            do_action('ls_acm_code_created', $code);
        }

        return $success ? $code->getCodeId() : 0;
    }

    /**
     * Update Access Code
     *
     * @since 2.0.0
     *
     * @param ACMCode $code Code object to be updated
     * @param null|int[] $groupIds CA-Group Id's to connect to the AC or leave null to keep the old CA-Group connections
     * @return bool True if updated or false on failure
     */
    public static function updateAccessCode(ACMCode $code, $groupIds = null): bool
    {
        $codeId = $code->getCodeId();

        if (!$code->valid() || $codeId < 1) {
            return false;
        }

        \LS\TransactionManager::start();

        try {

            $wpdb = LS()->wpdb;
            $wpdb->update($wpdb->prefix . 'ls_acm_codes', $code->getRecord(), ['codeId' => $codeId], null, '%d');

            if ($groupIds != null) {

                $currentGroupIds = self::getAccessCodeGroupIds($codeId);
                $removedGroupIds = array_diff($currentGroupIds, $groupIds);
                $newGroupIds = array_diff($groupIds, $currentGroupIds);

                foreach ($removedGroupIds as $groupId) {
                    if (!self::disconnectAccessCodeFromGroup($codeId, $groupId)) {
                        throw new Exception('Can not disconnect CA-Group ID: ' . $groupId . ' from access code ' . $code->getCode());
                    }
                }

                foreach ($newGroupIds as $groupId) {
                    if (!self::connectAccessCodeToGroup($codeId, $groupId)) {
                        throw new Exception('Can not connect CA-Group ID: ' . $groupId . ' to access code ' . $code->getCode());
                    }
                }

                foreach (self::getConsumersIdsConnectedToAccessCode($codeId) as $consumerId) {

                    foreach ($removedGroupIds as $groupId) {
                        self::disconnectConsumerFromGroup($consumerId, $groupId);
                    }

                    foreach ($newGroupIds as $groupId) {
                        self::connectConsumerToGroup($consumerId, $groupId);
                    }
                }
            }

            \LS\TransactionManager::commit();

            $success = true;

        } catch (Exception $e) {
            $success = false;
            \LS\TransactionManager::rollback();
            _doing_it_wrong(__FUNCTION__, $e->getMessage(), '3.5.0');
        }

        if ($success) {
            do_action('ls_acm_code_updated', $code);
        }

        return $success;
    }

    /**
     * Delete Access Code
     *
     * @since 2.0.0
     *
     * @param int $codeId Code Id
     * @return bool True if Access Code has been deleted
     */
    public static function deleteAccessCode($codeId): bool
    {
        // @TODO Can be changed to type hinting after 09.2019
        $codeId = \LS\Library::toInt($codeId);

        if ($codeId < 1) {
            return false;
        }

        \LS\TransactionManager::start();

        try {

            if (!self::disconnectConsumersFromAccessCode($codeId)) {
                throw new Exception('Can not disconnect consumers from the code');
            }

            if (!self::disconnectAccessCodeFromAllGroups($codeId)) {
                throw new Exception('Can not disconnect groups from the code');
            }

            $wpdb = LS()->wpdb;

            if ($wpdb->delete($wpdb->prefix . 'ls_acm_codes', ['codeId' => $codeId], '%d') === false) {
                throw new Exception('Can not delete the code');
            }

            \LS\TransactionManager::commit();

            $success = true;

        } catch (Exception $e) {
            $success = false;
            \LS\TransactionManager::rollback();
            _doing_it_wrong(__FUNCTION__, $e->getMessage(), '3.5.0');
        }

        if ($success) {
            do_action('ls_acm_code_deleted', $codeId);
        }

        return $success;
    }

    /**
     * Retrieve paginated data of Access Codes by specified conditions and filters
     *
     * @since 2.0.0
     *
     * @param int $itemsPerPage Number of ACs to retrieve
     * @param int $page Page number for pagination. To get all ACs without pagination set this value to null
     * @param string $orderBy Field to order
     * @param string $order Order direction
     * @param string $searchTerm Search query
     * @param array $filters Filters
     * @return array Array(
     *   'rows' - array of ACMCode objects,
     *   'totalCount' - total number of found codes,
     *   'totalPages' - total number of pages (for pagination),
     *   'itemsPerPage' - number of ACs per page
     * )
     */
    public static function paginateAccessCodes($itemsPerPage = 20, $page = 1, $orderBy = 'codeId', $order = 'DESC', $searchTerm = '', $filters = []): array
    {
        // @TODO Can be changed to type hinting after 09.2019
        $itemsPerPage = \LS\Library::toInt($itemsPerPage);
        $page = \LS\Library::toInt($page);

        $wpdb = LS()->wpdb;
        $order = empty($order) ? 'DESC' : $order;
        $whereConditions = [];
        $joins = [];

        if (in_array($orderBy, ['code', 'lifetime', 'createDate', 'startDate', 'endDate', 'active', 'drcActive', 'drcDays'])) {
            $orderBy = 'cd.' . $orderBy;
        } elseif ($orderBy == 'groupId') {
            $joins[] = "LEFT JOIN {$wpdb->prefix}ls_acm_groups g ON g.groupId = cdg.groupId";
            $orderBy = 'g.name';
        } elseif ($orderBy == 'companyId') {
            $joins[] = "LEFT JOIN {$wpdb->prefix}ls_companies _cmp ON _cmp.companyId = cd.companyId";
            $orderBy = '_cmp.companyName';
        } elseif ($orderBy == 'used') {
            $orderBy = 'usages';
        } elseif ($orderBy == 'note') {
            $orderBy = "cd.note = '', cd.note";
        } elseif ($orderBy == 'limit') {
            $orderBy = "cd.limit = 0 " . $order . ", cd.limit";
        } else {
            $orderBy = 'cd.codeId';
        }

        if (!empty($searchTerm)) {

            $columns = [
                'cd.code',
                'cd.note'
            ];

            $searchConditions = [];
            foreach ($columns as $c) {
                $searchConditions[] = sprintf("%s LIKE '%%%s%%'", $c, esc_sql(str_replace('\\', '\\\\', $searchTerm)));
            }

            if (!empty($searchConditions)) {
                $whereConditions[] = implode(' OR ', $searchConditions);
            }
        }

        if (isset($filters['groupId'])) {
            $filters[] = [
                'r'         => 'groupId',
                'c'         => 'equals',
                's_groupId' => $filters['groupId']
            ];
        }

        if (isset($filters['companyId'])) {
            $filters[] = [
                'r'           => 'companyId',
                'c'           => 'equals',
                's_companyId' => $filters['companyId']
            ];
        }

        if (!empty($filters)) {

            $filtersWhere = [];

            foreach ($filters as $filter) {

                if (!isset($filter['r'])) {
                    continue;
                }

                $field = $filter['r'];
                $condition = $filter['c'] ?? '';
                $value = $filter['t'] ?? '';

                if (in_array($field, ['codeId', 'code', 'limit', 'lifetime', 'note', 'createDate', 'startDate', 'endDate', 'active', 'drcActive', 'drcDays'])) {
                    $record = 'cd.' . $field;
                    if ($field == 'createDate') {
                        $value = strtotime($value);
                    }
                } elseif ($field == 'companyId') {
                    $record = 'cd.companyId';
                    $value = $filter['s_companyId'] ?? 0;
                    $value = is_array($value) ? $value : (int) $value;
                    if (!is_array($value) && (!isset($filters['hierarchical']) || $filters['hierarchical'])) {
                        $value = LSCompaniesHelper::includeChildrenCompaniesIds($value);
                    }
                } elseif ($field == 'groupId') {
                    $record = 'cdg.groupId';
                    $value = $filter['s_groupId'] ?? 0;
                    $value = is_array($value) ? $value : (int) $value;
                    if (!is_array($value) && (!isset($filters['hierarchical']) || $filters['hierarchical'])) {
                        $value = self::includeChildrenGroupsIds((int) $value);
                    }
                } elseif ($field == 'used') {
                    $record = 'cc.codeId';
                } else {
                    $record = '';
                }

                if (!empty($record)) {
                    $filterQuery = LS\Library::generateQueryFromFilter($record, $condition, $value);
                    if (!empty($filterQuery)) {
                        $filtersWhere[] = $filterQuery;
                    }
                }
            }

            if (!empty($filtersWhere)) {
                $xor = isset($filters['x']) && in_array($filters['x'], ['AND', 'OR']) ? $filters['x'] : 'AND';
                $whereConditions[] = '(' . implode(') ' . $xor . ' (', $filtersWhere) . ')';
            }
        }

        $limit = '';

        if ($page > 0 && $itemsPerPage > 0) {
            $limit = sprintf(" LIMIT %d, %d", ($page - 1) * $itemsPerPage, $itemsPerPage);
        }

        $where = empty($whereConditions) ? '' : sprintf(" WHERE (%s)", implode(') AND (', $whereConditions));
        $joins = implode(' ', array_unique($joins));

        $query = sprintf("SELECT SQL_CALC_FOUND_ROWS cd.*, COUNT(DISTINCT cc.consumerId) usages
                          FROM {$wpdb->prefix}ls_acm_codes cd
                          LEFT JOIN {$wpdb->prefix}ls_acm_code_groups cdg ON cdg.codeId = cd.codeId
                          LEFT JOIN {$wpdb->prefix}ls_acm_consumers_codes cc ON cc.codeId = cd.codeId
                          %s %s
                          GROUP BY cd.codeId
                          ORDER BY %s %s %s", $joins, $where, $orderBy, $order, $limit);

        $result = $wpdb->get_results($query);
        $records = [];

        if (is_array($result)) {
            foreach ($result as $record) {
                $records[] = new ACMCode($record);
            }
        }

        $totalCount = (int) $wpdb->get_var("SELECT FOUND_ROWS()");
        $itemsPerPage = $itemsPerPage > 0 ? $itemsPerPage : $totalCount;
        $totalPages = $totalCount > 0 && $itemsPerPage > 0 ? ceil($totalCount / $itemsPerPage) : 0;

        return [
            'rows'         => $records,
            'totalCount'   => $totalCount,
            'totalPages'   => $totalPages,
            'itemsPerPage' => $itemsPerPage
        ];
    }

    /**
     * Retrieve Access Codes by specified conditions and filters
     *
     * @since 2.0.0
     *
     * @param string $orderBy Field to order
     * @param string $order Order direction
     * @param string $searchTerm Search query
     * @param array $filters Filters
     * @return ACMCode[]
     */
    public static function getAccessCodes($orderBy = 'codeId', $order = 'DESC', $searchTerm = '', $filters = [])
    {
        return self::paginateAccessCodes(0, 0, $orderBy, $order, $searchTerm, $filters)['rows'];
    }

    /**
     * Retrieve Access Code by code Id
     *
     * @since 2.0.0
     *
     * @param int $codeId Code Id
     * @return false|ACMCode Code object or false on failure
     */
    public static function getAccessCode($codeId)
    {
        // @TODO Can be changed to type hinting after 09.2019
        $codeId = \LS\Library::toInt($codeId);

        $result = false;

        if ($codeId > 0) {
            $wpdb = LS()->wpdb;
            $query = sprintf("SELECT cd.*, COUNT(DISTINCT cc.consumerId) usages
                              FROM {$wpdb->prefix}ls_acm_codes cd
                              LEFT JOIN {$wpdb->prefix}ls_acm_consumers_codes cc ON cc.codeId = cd.codeId
                              WHERE cd.codeId = %d
                              GROUP BY cd.codeId
                              LIMIT 1", $codeId);
            $result = $wpdb->get_row($query);
            $result = empty($result) ? false : new ACMCode($result);
        }

        return $result;
    }

    /**
     * Retrieve Access Code by specified code
     *
     * @since 2.0.0
     *
     * @param string $code Code
     * @return false|ACMCode Code object or false on failure
     */
    public static function getAccessCodeByCode($code)
    {
        $result = false;
        $code = trim($code);

        if (!empty($code)) {
            $wpdb = LS()->wpdb;
            $query = sprintf("SELECT cd.*, COUNT(DISTINCT cc.consumerId) usages
                              FROM {$wpdb->prefix}ls_acm_codes cd
                              LEFT JOIN {$wpdb->prefix}ls_acm_consumers_codes cc ON cc.codeId = cd.codeId
                              WHERE cd.code = '%s'
                              GROUP BY cd.codeId
                              LIMIT 1", esc_sql($code));
            $result = $wpdb->get_row($query);
            $result = empty($result) ? false : new ACMCode($result);
        }

        return $result;
    }

    /**
     * Retrieve Access Codes by specified codes Id's
     *
     * @since 2.0.0
     *
     * @param int[] $ids Access Codes Id's
     * @param bool $activeOnly True to retrieve only active codes only
     * @return ACMCode[]
     */
    public static function getAccessCodesByIds($ids, $activeOnly = false)
    {
        // @TODO Can be changed to type hinting after 09.2019
        $activeOnly = \LS\Library::toBool($activeOnly);

        $wpdb = LS()->wpdb;
        $codes = [];

        foreach (array_chunk($ids, 50) as $idsPack) {

            $query = sprintf("SELECT cd.*, COUNT(DISTINCT cc.consumerId) usages
                              FROM {$wpdb->prefix}ls_acm_codes cd
                              LEFT JOIN {$wpdb->prefix}ls_acm_consumers_codes cc ON cc.codeId = cd.codeId
                              WHERE cd.codeId IN (%s)
                              GROUP BY cd.codeId", implode(',', $idsPack));

            $result = $wpdb->get_results($query);

            if (is_array($result)) {
                foreach ($result as $record) {
                    $codes[] = new ACMCode($record);
                }
            }
        }

        if ($activeOnly) {

            /** @var ACMCode $code */
            foreach ($codes as $key => $code) {
                if (!$code->isActive()) {
                    unset($codes[$key]);
                }
            }
        }

        return $codes;
    }

    /**
     * Retrieve Access Codes by group Id
     *
     * @since 2.5.0
     *
     * @param int $groupId CA-Group Id
     * @param bool $incChildrenGroups True to include access codes from children groups
     * @return ACMCode[]
     */
    public static function getAccessCodesByGroupId($groupId, bool $incChildrenGroups = true)
    {
        // @TODO Can be changed to type hinting after 09.2019
        $groupId = \LS\Library::toInt($groupId);

        return $groupId > 0 ? self::getAccessCodes('', '', '', ['groupId' => $groupId, 'hierarchical' => $incChildrenGroups]) : [];
    }

    /**
     * Retrieve Access Codes by company Id
     *
     * @since 3.0.0
     *
     * @param int $companyId Company Id
     * @param bool $incChildrenCompanies True to include access codes from children companies
     * @return ACMCode[]
     */
    public static function getAccessCodesByCompanyId($companyId, bool $incChildrenCompanies = true)
    {
        // @TODO Can be changed to type hinting after 09.2019
        $companyId = \LS\Library::toInt($companyId);

        return $companyId > 0 ? self::getAccessCodes('', '', '', ['companyId' => $companyId, 'hierarchical' => $incChildrenCompanies]) : [];
    }

    /**
     * Connection access code to group
     *
     * @since 3.5.0
     *
     * @param int $codeId Code Id
     * @param int $groupId CA-Group Id
     * @return bool True if connected
     */
    public static function connectAccessCodeToGroup(int $codeId, int $groupId): bool
    {
        $success = false;

        if ($codeId > 0 && $groupId > 0) {
            $wpdb = LS()->wpdb;
            $success = (bool) $wpdb->insert($wpdb->prefix . 'ls_acm_code_groups', ['codeId' => $codeId, 'groupId' => $groupId], '%d');
        }

        return $success;
    }

    /**
     * Check if access code connect to the group
     *
     * @since 3.5.0
     *
     * @param int $codeId Code Id
     * @param int $groupId Group Id
     * @return bool True if connected
     */
    public static function hasAccessCodeConnectionToGroup(int $codeId, int $groupId): bool
    {
        $wpdb = LS()->wpdb;

        return (bool) $wpdb->get_var(sprintf("SELECT EXISTS (SELECT 1 FROM {$wpdb->prefix}ls_acm_code_groups WHERE codeId = %d AND groupId = %d)", $codeId, $groupId));
    }

    /**
     * Disconnection access code to group
     *
     * @since 3.5.0
     *
     * @param int $codeId Code Id
     * @param int $groupId CA-Group Id
     * @return bool True if disconnected
     */
    public static function disconnectAccessCodeFromGroup(int $codeId, int $groupId): bool
    {
        $success = false;

        if ($codeId > 0 && $groupId > 0) {
            $wpdb = LS()->wpdb;
            $success = $wpdb->delete($wpdb->prefix . 'ls_acm_code_groups', ['codeId' => $codeId, 'groupId' => $groupId], '%d') !== false;
        }

        return $success;
    }

    /**
     * Disconnection access code to group
     *
     * @since 3.5.0
     *
     * @param int $codeId Code Id
     * @return bool True if disconnected
     */
    public static function disconnectAccessCodeFromAllGroups(int $codeId): bool
    {
        $success = false;

        if ($codeId > 0) {
            $wpdb = LS()->wpdb;
            $success = $wpdb->delete($wpdb->prefix . 'ls_acm_code_groups', ['codeId' => $codeId], '%d') !== false;
        }

        return $success;
    }

    /**
     * Retrieve AC-Group Id's connected to specified access code Id
     *
     * @since 3.5.0
     *
     * @param int $codeId Code Id
     * @return int[] AC-Group Id's
     */
    public static function getAccessCodeGroupIds(int $codeId)
    {
        $wpdb = LS()->wpdb;
        $groupIds = $wpdb->get_col(sprintf("SELECT groupId FROM {$wpdb->prefix}ls_acm_code_groups WHERE codeId = %d", $codeId));
        $groupIds = is_array($groupIds) ? array_map('intval', $groupIds) : [];

        return $groupIds;
    }

    /**
     * Bulk codes activation by access codes
     *
     * @since 3.4.0
     *
     * @param ACMCode[] $codes Access codes
     * @return bool True if all access codes have been activated
     */
    public static function activateAccessCodes($codes): bool
    {
        $success = true;

        foreach ($codes as $code) {
            if ($code->isActive()) {
                $success &= self::updateAccessCode(
                    $code->markAsActive()
                );
            }
        }

        return $success;
    }

    /**
     * Bulk action to deactivate a multiple access codes
     *
     * @since 3.4.0
     *
     * @param ACMCode[] $codes Access codes
     * @return bool True if all access codes have been deactivated
     */
    public static function deactivateAccessCodes($codes): bool
    {
        $success = true;

        foreach ($codes as $code) {
            if ($code->isActive()) {
                $success &= self::updateAccessCode(
                    $code->markAsInactive()
                );
            }
        }

        return $success;
    }

    /**
     * ################## CONSUMER TO ACCESS CODE CONNECTIONS ####################
     */

    /**
     * Create connection between consumer and Access Code
     *
     * @since 2.0.0
     *
     * @param \LS\ConsumerInterface $consumer Consumer
     * @param ACMCode $code Code object
     * @return bool True if connection has been created
     */
    public static function connectConsumerToAccessCode($consumer, ACMCode $code): bool
    {
        // @deprecated 06.2019 Always must be ConsumerInterface
        if (!($consumer instanceof \LS\ConsumerInterface)) {
            $consumerId = (int) $consumer;
            $consumer = consumersLoyaltySuite::getConsumer($consumerId);
            // @TODO Uncomment after 09.2019
            //_doing_it_wrong(__FUNCTION__, '$consumer must be instance of ConsumerInterface', '3.6.0');
        } else {
            $consumerId = $consumer->getConsumerId();
        }

        if ($consumerId < 1 || !$code->canBeUsed()) {
            return false;
        }

        $endDate =
            $code->hasLifetime()
                ? min(strtotime('TODAY') + $code->getLifetime() * DAY_IN_SECONDS, $code->getEndDate())
                : $code->getEndDate();

        $cc = new ACMConsumerCodeConnection([
            'consumerId' => $consumerId,
            'codeId'     => $code->getCodeId(),
            'enterDate'  => time(),
            'endDate'    => $endDate
        ]);

        \LS\TransactionManager::start();

        try {

            if (!$cc->valid()) {
                throw new Exception('Invalid connection between consumer and code');
            }

            if (!self::addConsumerCodeConnection($cc)) {
                throw new Exception('Can not connect consumer to the code');
            }

            foreach (self::getAccessCodeGroupIds($code->getCodeId()) as $groupId) {
                self::connectConsumerToGroup($consumerId, $groupId);
            }

            \LS\TransactionManager::commit();

            $success = true;

        } catch (Exception $e) {
            $success = false;
            \LS\TransactionManager::rollback();
            _doing_it_wrong(__FUNCTION__, $e->getMessage(), '3.5.0');
        }

        if ($success) {

            do_action('ls_acm_consumer_connected', $consumer, $code);

            \LS\ConsumersHelper::insertLog('access_code', $consumerId, $code->getCode());
        }

        return $success;
    }

    /**
     * Save connection between consumer and Access Code
     *
     * @since 3.0.0
     *
     * @param ACMConsumerCodeConnection $cc Consumer connection
     * @return int Connection ID
     */
    public static function addConsumerCodeConnection(ACMConsumerCodeConnection $cc): int
    {
        $ccId = 0;

        if ($cc->valid()) {

            $wpdb = LS()->wpdb;

            if ($wpdb->insert($wpdb->prefix . 'ls_acm_consumers_codes', $cc->getRecord())) {
                $ccId = (int) $wpdb->insert_id;
            }

            if ($ccId > 0) {
                do_action('ls_acm_consumer_code_added', $ccId);
            }
        }

        return $ccId;
    }

    /**
     * Delete consumer to Access Code connection
     *
     * @since 3.0.0
     *
     * @param ACMConsumerCodeConnection $cc Consumer connection
     * @return bool True if connection deleted
     */
    public static function deleteConsumerCodeConnection(ACMConsumerCodeConnection $cc): bool
    {
        $wpdb = LS()->wpdb;
        $success = $wpdb->delete($wpdb->prefix . 'ls_acm_consumers_codes', ['ccId' => $cc->getId()], '%d') !== false;

        if ($success) {
            do_action('ls_acm_consumer_code_disconnected', $cc->getConsumerId(), $cc->getCodeId());
        }

        return $success;
    }

    /**
     * Retrieve connections between consumer and Access Codes by consumer Id
     *
     * @since 2.0.0
     *
     * @param int $consumerId Consumer Id
     * @param bool $actualOnly True to retrieve actual connections only
     * @return ACMConsumerCodeConnection[]
     */
    public static function getConsumerCodesConnections($consumerId, $actualOnly = false)
    {
        // @TODO Can be changed to type hinting after 09.2019
        $consumerId = \LS\Library::toInt($consumerId);
        $actualOnly = \LS\Library::toBool($actualOnly);

        $result = [];

        if ($consumerId > 0) {

            $wpdb = LS()->wpdb;
            $query = sprintf("SELECT * FROM {$wpdb->prefix}ls_acm_consumers_codes WHERE consumerId = %d ORDER BY endDate DESC", $consumerId);
            $connections = $wpdb->get_results($query);

            if (is_array($connections)) {
                foreach ($connections as $connection) {
                    $result[] = new ACMConsumerCodeConnection($connection);
                }
            }

            if ($actualOnly) {
                $result = array_values(array_filter($result, static function ($cc) {
                    /** @var ACMConsumerCodeConnection|false $cc */
                    return $cc && $cc->isActual();
                }));
            }
        }

        return $result;
    }

    /**
     * Retrieve latest connections between consumer and Access Codes by consumer Id
     *
     * @since 3.2.0
     *
     * @param int $consumerId Consumer Id
     * @param bool $actualOnly True to retrieve actual connections only
     * @return ACMConsumerCodeConnection[]
     */
    public static function getLatestConsumerCodesConnections($consumerId, $actualOnly = false)
    {
        // @TODO Can be changed to type hinting after 09.2019
        $consumerId = \LS\Library::toInt($consumerId);
        $actualOnly = \LS\Library::toBool($actualOnly);

        $result = [];

        foreach (self::getConsumerCodesConnections($consumerId, $actualOnly) as $cc) {

            $codeId = $cc->getCodeId();

            /** @var ACMConsumerCodeConnection $latestConnection */
            $latestConnection = $result[$codeId] ?? false;

            if (!isset($result[$codeId]) || ($latestConnection && $cc->getEndDate() > $latestConnection->getEndDate())) {
                $result[$codeId] = $cc;
            }
        }

        return array_values($result);
    }

    /**
     * Retrieve single connection between consumer and Access Code by consumer Id
     *
     * @since 3.0.0
     *
     * @param int $ccId Consumer Code Id
     * @return false|ACMConsumerCodeConnection Consumer connection object or false on failure
     */
    public static function getConsumerCodeConnection($ccId)
    {
        // @TODO Can be changed to type hinting after 09.2019
        $ccId = \LS\Library::toInt($ccId);

        if ($ccId < 1) {
            return false;
        }

        $wpdb = LS()->wpdb;
        $result = $wpdb->get_row(sprintf("SELECT * FROM {$wpdb->prefix}ls_acm_consumers_codes WHERE ccId = %d", $ccId));
        $result = empty($result) ? false : new ACMConsumerCodeConnection($result);

        return $result;
    }

    /**
     * Calculate consumer codes expiration date
     * If consumer has more that one Access Code -> the latest one will be used
     *
     * @since 2.0.0
     *
     * @param int $consumerId Consumer Id
     * @return int Timestamp
     */
    public static function calculateConsumerCodesExpirationDate($consumerId): int
    {
        // @TODO Can be changed to type hinting after 09.2019
        $consumerId = \LS\Library::toInt($consumerId);

        $latestDate = 0;

        foreach (self::getLatestConsumerCodesConnections($consumerId, true) as $cc) {
            $latestDate = max($latestDate, $cc->getEndDate());
        }

        return $latestDate;
    }

    /**
     * Retrieve all connections between consumer and Access Code by code Id
     *
     * @since 2.0.0
     *
     * @param int $codeId Access Code Id
     * @return ACMConsumerCodeConnection[]
     */
    public static function getConsumerConnectionsToAccessCode($codeId)
    {
        // @TODO Can be changed to type hinting after 09.2019
        $codeId = \LS\Library::toInt($codeId);

        $result = [];

        if ($codeId > 0) {

            $wpdb = LS()->wpdb;

            // MUST be ordered by end date for [consumer-access-codes]
            $query = sprintf("SELECT * FROM {$wpdb->prefix}ls_acm_consumers_codes WHERE codeId = %d ORDER BY endDate DESC", $codeId);
            $data = $wpdb->get_results($query);

            if (is_array($data)) {
                foreach ($data as $record) {
                    $result[] = new ACMConsumerCodeConnection($record);
                }
            }
        }

        return $result;
    }

    /**
     * Retrieve latest connections between consumer and Access Code by code Id
     *
     * @since 2.0.0
     *
     * @param int $codeId Access Code Id
     * @return ACMConsumerCodeConnection[]
     */
    public static function getLatestConsumerConnectionsToAccessCode($codeId)
    {
        // @TODO Can be changed to type hinting after 09.2019
        $codeId = \LS\Library::toInt($codeId);

        $result = [];

        foreach (self::getConsumerConnectionsToAccessCode($codeId) as $cc) {

            $consumerId = $cc->getConsumerId();

            /** @var ACMConsumerCodeConnection $latestConnection */
            $latestConnection = $result[$consumerId] ?? false;

            if (!$latestConnection || $cc->getEndDate() > $latestConnection->getEndDate()) {
                $result[$consumerId] = $cc;
            }
        }

        return $result;
    }

    /**
     * Retrieve all consumers ID's connected to specified Access Code
     *
     * @since 3.2.0
     *
     * @param int $codeId Access Code Id
     * @return int[] A list of consumers ID's
     */
    public static function getConsumersIdsConnectedToAccessCode($codeId)
    {
        // @TODO Can be changed to type hinting after 09.2019
        $codeId = \LS\Library::toInt($codeId);

        return array_unique(array_map(static function (ACMConsumerCodeConnection $cc) {
            return $cc->getConsumerId();
        }, self::getConsumerConnectionsToAccessCode($codeId)));
    }

    /**
     * Retrieve consumer Access Codes
     *
     * @since 2.0.0
     *
     * @param int $consumerId Consumer Id
     * @param bool $actualOnly True to retrieve only actual (not expired, valid) codes
     * @return ACMCode[]
     */
    public static function getConsumerAccessCodes($consumerId, $actualOnly = false)
    {
        // @TODO Can be changed to type hinting after 09.2019
        $consumerId = \LS\Library::toInt($consumerId);
        $actualOnly = \LS\Library::toBool($actualOnly);

        $codes = [];

        foreach (self::getLatestConsumerCodesConnections($consumerId, $actualOnly) as $cc) {
            if (!isset($codes[$cc->getCodeId()])) {
                $code = self::getAccessCode($cc->getCodeId());
                if ($code) {
                    $codes[$cc->getCodeId()] = $code;
                }
            }
        }

        if ($actualOnly) {
            $codes = array_filter($codes, static function (ACMCode $code) {
                return $code->isActual();
            });
        }

        return array_values($codes);
    }

    /**
     * Retrieve Access Codes Id's connected to specified consumer
     *
     * @since 2.0.0
     *
     * @param int $consumerId Consumer Id
     * @param bool $actualOnly True to retrieve only actual (not expired, valid) codes Id's
     * @return int[] A list of Access Codes Id's
     */
    public static function getConsumerAccessCodesIds($consumerId, $actualOnly = false)
    {
        // @TODO Can be changed to type hinting after 09.2019
        $consumerId = \LS\Library::toInt($consumerId);
        $actualOnly = \LS\Library::toBool($actualOnly);

        $ids = [];

        foreach (self::getConsumerAccessCodes($consumerId, $actualOnly) as $code) {
            $ids[] = $code->getCodeId();
        }

        return $ids;
    }

    /**
     * Check if specified consumer has at least one entered code
     *
     * @since 2.0.0
     *
     * @param int $consumerId Consumer Id
     * @param bool $activeOnly True to check active codes only
     * @return bool True if consumer has active codes
     */
    public static function hasConsumerCodes($consumerId, $activeOnly = false): bool
    {
        // @TODO Can be changed to type hinting after 09.2019
        $consumerId = \LS\Library::toInt($consumerId);
        $activeOnly = \LS\Library::toBool($activeOnly);

        return count(self::getConsumerAccessCodes($consumerId, $activeOnly)) > 0;
    }

    /**
     * Check if specified consumer connected to specified Access Code by name
     *
     * @since 2.0.0
     *
     * @param int $consumerId Consumer Id
     * @param string $codeToCheck Access Code value
     * @return bool True if consumer connected to Access Code
     */
    public static function hasConsumerCode($consumerId, $codeToCheck): bool
    {
        // @TODO Can be changed to type hinting after 09.2019
        $consumerId = \LS\Library::toInt($consumerId);

        foreach (self::getConsumerAccessCodes($consumerId) as $code) {
            if ($code->getCode() === $codeToCheck) {
                return true;
            }
        }

        return false;
    }

    /**
     * Check if consumer has code by code Id
     *
     * @since 2.0.0
     *
     * @param int $consumerId Consumer Id
     * @param int $codeId Code Id
     * @return bool True if consumer has code Id
     */
    public static function hasConsumerCodeByCodeId($consumerId, $codeId): bool
    {
        // @TODO Can be changed to type hinting after 09.2019
        $consumerId = \LS\Library::toInt($consumerId);
        $codeId = \LS\Library::toInt($codeId);

        foreach (self::getConsumerAccessCodesIds($consumerId) as $id) {
            if ($id === $codeId) {
                return true;
            }
        }

        return false;
    }

    /**
     * Delete connection between consumer and Access Code by consumer Id and code Id
     *
     * @since 2.0.0
     *
     * @param int $consumerId Consumer Id
     * @param int $codeId Code Id
     * @return bool True if connection has been deleted
     */
    public static function disconnectConsumerFromAccessCode($consumerId, $codeId): bool
    {
        // @TODO Can be changed to type hinting after 09.2019
        $consumerId = \LS\Library::toInt($consumerId);
        $codeId = \LS\Library::toInt($codeId);

        if ($consumerId < 1 || $codeId < 1) {
            return false;
        }

        $code = self::getAccessCode($codeId);

        if (!$code) {
            return false;
        }

        $wpdb = LS()->wpdb;
        if ($wpdb->delete($wpdb->prefix . 'ls_acm_consumers_codes', ['consumerId' => $consumerId, 'codeId' => $codeId], '%d') === false) {
            return false;
        }

        \LS\ConsumersHelper::insertLog('access_code_removed', $consumerId, $code->getCode());

        do_action('ls_acm_consumer_code_disconnected', $consumerId, $code);

        return true;
    }

    /**
     * Delete connection between consumer and Access Code by consumer Id and code Id
     *
     * @since 2.0.0
     * @param int $consumerId Consumer Id
     *
     * @return bool True if all connections deleted
     */
    public static function disconnectConsumerFromAccessCodes($consumerId): bool
    {
        // @TODO Can be changed to type hinting after 09.2019
        $consumerId = \LS\Library::toInt($consumerId);

        $success = true;

        foreach (self::getConsumerAccessCodes($consumerId) as $code) {
            $success &= self::disconnectConsumerFromAccessCode($consumerId, $code->getCodeId());
        }

        return $success;
    }

    /**
     * Delete connection between consumers and Access Code by code Id
     *
     * @since 2.0.0
     * @param int $codeId Access Code Id
     *
     * @return bool True if all connections deleted
     */
    public static function disconnectConsumersFromAccessCode($codeId): bool
    {
        // @TODO Can be changed to type hinting after 09.2019
        $codeId = \LS\Library::toInt($codeId);

        $success = true;

        foreach (self::getConsumersIdsConnectedToAccessCode($codeId) as $consumerId) {
            $success &= self::disconnectConsumerFromAccessCode($consumerId, $codeId);
        }

        return $success;
    }

    /**
     * ##################  CONSUMER TO GROUP CONNECTIONS #####################
     */

    /**
     * Retrieve connections between consumer and content groups
     *
     * @since 3.0.0
     *
     * @param int $consumerId Consumer Id
     * @return ACMConsumerGroupConnection[]
     */
    public static function getConsumerGroupsConnections($consumerId)
    {
        // @TODO Can be changed to type hinting after 09.2019
        $consumerId = \LS\Library::toInt($consumerId);

        $result = [];

        if ($consumerId > 0) {

            $wpdb = LS()->wpdb;
            $records = $wpdb->get_results(sprintf("SELECT * FROM {$wpdb->prefix}ls_acm_consumers_groups WHERE consumerId = %d ORDER BY groupId ASC, groupEnterDate DESC", $consumerId));

            if (is_array($records)) {
                foreach ($records as $record) {
                    $result[] = new ACMConsumerGroupConnection($record);
                }
            }
        }

        return $result;
    }

    /**
     * Retrieve group id's that are connected to the consumer
     *
     * @since 3.0.0
     *
     * @param int $consumerId Consumer Id
     * @return int[] Group Id's
     */
    public static function getConsumerGroupsConnectionsIds($consumerId)
    {
        // @TODO Can be changed to type hinting after 09.2019
        $consumerId = \LS\Library::toInt($consumerId);

        $ids = [];

        foreach (self::getConsumerGroupsConnections($consumerId) as $cg) {
            $ids[] = $cg->getGroupId();
        }

        return $ids;
    }

    /**
     * Retrieve consumer groups
     *
     * @since 2.0.0
     *
     * @param int $consumerId Consumer Id
     * @param bool $activeOnly True to retrieve only active groups
     * @param bool $parents True to include parent groups
     * @return ACMGroup[]
     */
    public static function getConsumerGroups($consumerId, $activeOnly = false, $parents = true)
    {
        // @TODO Can be changed to type hinting after 09.2019
        $consumerId = \LS\Library::toInt($consumerId);
        $activeOnly = \LS\Library::toBool($activeOnly);
        $parents = \LS\Library::toBool($parents);

        if ($consumerId < 1) {
            return [];
        }

        if ($parents) {

            if (!isset(self::$consumerGroups[$consumerId])) {

                $getGroups = static function ($consumerGroupsIds, $groups = []) use (&$getGroups) {

                    $parentIds = [];

                    foreach (self::getGroupsByIds($consumerGroupsIds) as $group) {
                        $groups[$group->getGroupId()] = $group;
                        if ($group->getParentGroupId() > 0) {
                            $parentIds[] = $group->getParentGroupId();
                        }
                    }

                    $parentIds = array_values(array_filter(array_unique($parentIds), static function ($id) use ($groups) {
                        return !isset($groups[$id]);
                    }));

                    if (!empty($parentIds)) {
                        $groups = $getGroups($parentIds, $groups);
                    }

                    return $groups;
                };

                $groups = array_values($getGroups(self::getConsumerGroupsConnectionsIds($consumerId)));

                self::$consumerGroups[$consumerId] = $groups;

            } else {
                $groups = self::$consumerGroups[$consumerId];
            }

        } else {
            $groups = self::getGroupsByIds(self::getConsumerGroupsConnectionsIds($consumerId));
        }

        if ($activeOnly) {
            $groups = array_values(array_filter($groups, static function ($group) {
                /** @var ACMGroup|false $group */
                return $group && $group->isActive();
            }));
        }

        return $groups;
    }

    /**
     * Retrieve groups Id's connected to specified consumer
     *
     * @since 2.0.0
     *
     * @param int $consumerId Consumer Id
     * @param bool $activeOnly True to retrieve only active groups Id's
     * @param bool $parents True to include parent groups id's
     * @return int[] A list of groups Id's
     */
    public static function getConsumerGroupsIds($consumerId, $activeOnly = false, $parents = true)
    {
        // @TODO Can be changed to type hinting after 09.2019
        $consumerId = \LS\Library::toInt($consumerId);
        $activeOnly = \LS\Library::toBool($activeOnly);
        $parents = \LS\Library::toBool($parents);

        return array_map(static function (ACMGroup $group) {
            return $group->getGroupId();
        }, self::getConsumerGroups($consumerId, $activeOnly, $parents));
    }

    /**
     * Check if the consumer has connection to specific group
     *
     * @since 2.0.0
     *
     * @param int $consumerId Consumer Id
     * @param int $groupId Group Id
     * @param bool $activeOnly True to check only active connections
     * @return bool True if consumer has connection to specific group
     */
    public static function hasConsumerConnectionToGroup($consumerId, $groupId, $activeOnly = false): bool
    {
        // @TODO Can be changed to type hinting after 09.2019
        $consumerId = \LS\Library::toInt($consumerId);
        $groupId = \LS\Library::toInt($groupId);
        $activeOnly = \LS\Library::toBool($activeOnly);

        return in_array($groupId, self::getConsumerGroupsIds($consumerId, $activeOnly));
    }

    /**
     * Connect consumer to group
     *
     * @since 3.0.0
     *
     * @param int $consumerId Consumer Id
     * @param int $groupId Group Id
     * @return bool True if connected
     */
    public static function connectConsumerToGroup($consumerId, $groupId): bool
    {
        // @TODO Can be changed to type hinting after 09.2019
        $consumerId = \LS\Library::toInt($consumerId);
        $groupId = \LS\Library::toInt($groupId);

        if ($consumerId < 1 || $groupId < 1) {
            return false;
        }

        if (self::hasConsumerConnectionToGroup($consumerId, $groupId)) {
            return false;
        }

        $cg = new ACMConsumerGroupConnection([
            'consumerId'     => $consumerId,
            'groupId'        => $groupId,
            'groupEnterDate' => time()
        ]);

        if (!$cg->valid()) {
            return false;
        }

        $wpdb = LS()->wpdb;

        if (!$wpdb->insert($wpdb->prefix . 'ls_acm_consumers_groups', $cg->getRecord(true))) {
            return false;
        }

        unset(self::$consumerGroups[$consumerId]);

        \LS\ConsumersHelper::insertLog('access_group', $consumerId, $groupId);

        do_action('ls_acm_consumer_group_connected', $consumerId, $groupId);

        return true;
    }

    /**
     * Disconnect consumer from group
     *
     * @since 2.0.0
     *
     * @param int $consumerId Consumer Id
     * @param int $groupId Group Id
     * @return bool True if connection has been deleted
     */
    public static function disconnectConsumerFromGroup($consumerId, $groupId): bool
    {
        // @TODO Can be changed to type hinting after 09.2019
        $consumerId = \LS\Library::toInt($consumerId);
        $groupId = \LS\Library::toInt($groupId);

        if ($consumerId < 1 || $groupId < 1) {
            return false;
        }

        $wpdb = LS()->wpdb;

        if ($wpdb->delete($wpdb->prefix . 'ls_acm_consumers_groups', ['consumerId' => $consumerId, 'groupId' => $groupId], '%d') === false) {
            return false;
        }

        unset(self::$consumerGroups[$consumerId]);

        \LS\ConsumersHelper::insertLog('access_group_removed', $consumerId, $groupId);

        do_action('ls_acm_consumer_group_disconnected', $consumerId, $groupId);

        return true;
    }

    /**
     * Disconnect consumer from all groups
     *
     * @since 3.0.0
     * @param int $consumerId Consumer Id
     *
     * @return bool True if all connections deleted
     */
    public static function disconnectConsumerFromGroups($consumerId): bool
    {
        // @TODO Can be changed to type hinting after 09.2019
        $consumerId = \LS\Library::toInt($consumerId);

        $success = true;

        foreach (self::getConsumerGroupsConnections($consumerId) as $cg) {
            $success &= self::disconnectConsumerFromGroup($consumerId, $cg->getGroupId());
        }

        return $success;
    }

    /**
     * Retrieve a list of consumers ID's related to specified group
     *
     * @since 2.0.0
     *
     * @param int $groupId Group Id
     * @param bool $includeChildren True to include children groups
     * @param null $dep Moved as 2nd parameter
     * @return int[] Consumer Id's
     */
    public static function getConsumersConnectedToGroup($groupId, $includeChildren = true, $dep = null)
    {
        // @TODO Can be changed to type hinting after 09.2019
        $groupId = \LS\Library::toInt($groupId);

        if ($groupId < 1) {
            return [];
        }

        // @deprecated 04.2019 Moved as 2nd parameter
        if ($dep !== null) {
            $includeChildren = (bool) $dep;
            _deprecated_function(__FUNCTION__, '3.5.10');
        } else {
            $includeChildren = \LS\Library::toBool($includeChildren);
        }

        $wpdb = LS()->wpdb;
        $groupIds = $includeChildren ? self::includeChildrenGroupsIds($groupId) : [$groupId];
        $query = sprintf("SELECT cg.consumerId FROM {$wpdb->prefix}ls_acm_consumers_groups cg WHERE cg.groupId IN (%s)", implode(',', $groupIds));
        $consumers = $wpdb->get_col($query);
        $consumers = array_unique(array_map('intval', $consumers));

        return $consumers;
    }

    /**
     * Retrieve a number of usages of access groups
     *
     * @since 3.0.0
     *
     * @param int $groupId Group Id
     * @param bool $includeChildren True to include children groups
     * @return int Number of usages
     */
    public static function countGroupUsages($groupId, bool $includeChildren = true): int
    {
        // @TODO Can be changed to type hinting after 09.2019
        $groupId = \LS\Library::toInt($groupId);
        $includeChildren = \LS\Library::toBool($includeChildren);

        return count(self::getConsumersConnectedToGroup($groupId, $includeChildren));
    }

    /**
     * Retrieve number of access codes connected to the group
     *
     * @since 3.4.0
     *
     * @param int $groupId Group Id
     * @param bool $includeChildren True to include children groups
     * @return int Number of codes
     */
    public static function countCodesInGroup($groupId, bool $includeChildren = true): int
    {
        // @TODO Can be changed to type hinting after 09.2019
        $groupId = \LS\Library::toInt($groupId);
        $includeChildren = \LS\Library::toBool($includeChildren);

        return count(self::getAccessCodesByGroupId($groupId, $includeChildren));
    }

    /**
     * ##################  GROUP TO POST/TERM CONNECTIONS  ########################
     */

    /**
     * Retrieve ac-protected posts
     *
     * @since 2.0.0
     *
     * @param null|int|array $groupIds Array of group Id's, group Id or null to fetch connections from the all groups
     * @return array[int]string Post Id's and their types
     */
    public static function getProtectedPostItems($groupIds = null)
    {
        $wpdb = LS()->wpdb;
        $groupWhere = '';

        if ($groupIds !== null) {

            if (is_numeric($groupIds)) {
                $groupIds = [$groupIds];
            }

            $groupWhere = empty($groupIds) ? 'AND 1=0' : sprintf(" AND pm.meta_value IN (%s)", implode(',', $groupIds));
        }

        $query = sprintf("SELECT p.ID, p.post_type
                          FROM {$wpdb->posts} p
                          INNER JOIN {$wpdb->postmeta} pm ON p.ID = pm.post_id 
                          WHERE pm.meta_key = '_acm_group_id'
                            AND p.post_status != 'trash' 
                            AND p.post_status != 'auto-draft'
                            %s
                          GROUP BY p.ID 
                          ORDER BY p.ID ASC", $groupWhere);

        $items = [];

        foreach ($wpdb->get_results($query) as $record) {
            $items[(int) $record->ID] = $record->post_type;
        }

        return $items;
    }

    /**
     * Retrieve ac-protected post ID's
     *
     * @since 3.4.0
     *
     * @param null|int|array $groupIds Group Id, Array of groups or null to fetch connections from the all groups
     * @return int[] Post Id's
     */
    public static function getProtectedPostIds($groupIds = null)
    {
        return array_keys(self::getProtectedPostItems($groupIds));
    }

    /**
     * Retrieve ac-protected terms
     *
     * @since 2.0.0
     * @deprecated 08.2019 See TermAccessHelper
     *
     * @param null|int $groupId Group Id or null to fetch connections from the all groups
     * @return WP_Term[]
     */
    public static function getProtectedTerms($groupId = null)
    {
        // @TODO Uncomment after 10.2019
        //_deprecated_function(__FUNCTION__, '3.7.0', 'See TermAccessHelper');

        // @TODO Can be changed to type hinting after 09.2019
        $groupId = \LS\Library::toInt($groupId, true);

        $args = [
            'number'             => 0,
            'meta_key'           => '_acm_group_id',
            'orderby'            => 'ID',
            'order'              => 'ASC',
            'ac_stop_processing' => true
        ];

        if ($groupId !== null) {
            $args['meta_value'] = $groupId;
        }

        return get_terms($args);
    }

    /**
     * Retrieve ac-protected term ID's
     *
     * @since 3.4.0
     * @deprecated 08.2019 See TermAccessHelper
     *
     * @param null|int $groupId Group Id or null to fetch connections from the all groups
     * @return int[] Term Id's
     */
    public static function getProtectedTermsIds($groupId = null)
    {
        // @TODO Uncomment after 10.2019
        //_deprecated_function(__FUNCTION__, '3.7.0', 'See TermAccessHelper');

        // @TODO Can be changed to type hinting after 09.2019
        $groupId = \LS\Library::toInt($groupId, true);

        return array_map(static function (WP_Term $t) {
            return (int) $t->term_id;
        }, self::getProtectedTerms($groupId));
    }

    /**
     * Retrieve a list of group Id's connected to specified post Id
     *
     * @since 2.0.0
     *
     * @param int $postId Post Id
     * @return int[] Group Id's
     */
    public static function getGroupIdsByPostId($postId)
    {
        // @TODO Can be changed to type hinting after 09.2019
        $postId = \LS\Library::toInt($postId);

        return array_map('intval', get_post_meta($postId, '_acm_group_id'));
    }

    /**
     * Retrieve groups connected to post
     *
     * @since 2.0.0
     *
     * @param int $postId Post Id
     * @return ACMGroup[]
     */
    public static function getGroupsByPostId($postId)
    {
        // @TODO Can be changed to type hinting after 09.2019
        $postId = \LS\Library::toInt($postId);

        return self::getGroupsByIds(self::getGroupIdsByPostId($postId));
    }

    /**
     * Retrieve a list of AC ID's connected to specified term Id
     *
     * @since 2.0.0
     * @deprecated 07.2019 See TermAccessHelper
     *
     * @param int $termId Term Id
     * @return int[] Group Id's
     */
    public static function getGroupIdsByTermId($termId)
    {
        // @TODO Update usages after 09.2019
        // @TODO Uncomment after 10.2019
        //_deprecated_function(__FUNCTION__, '3.7.0', 'See TermAccessHelper');

        // @TODO Can be changed to type hinting after 09.2019
        $termId = \LS\Library::toInt($termId);

        return array_map('intval', get_term_meta($termId, '_acm_group_id'));
    }

    /**
     * Retrieve groups connected to term
     *
     * @since 2.0.0
     * @deprecated 07.2019 Legacy
     *
     * @param int $termId Term Id
     * @return ACMGroup[]
     */
    public static function getGroupsByTermId($termId)
    {
        _deprecated_function(__FUNCTION__, '3.6.1');

        // @TODO Can be changed to type hinting after 09.2019
        $termId = \LS\Library::toInt($termId);

        return self::getGroupsByIds(self::getGroupIdsByTermId($termId));
    }

    /**
     * Delete connections between post and ACM groups
     * @since 2.0.0
     * @param int $postId Post Id
     */
    public static function disconnectPostFromGroups($postId)
    {
        // @TODO Can be changed to type hinting after 09.2019
        $postId = \LS\Library::toInt($postId);

        delete_post_meta($postId, '_acm_group_id');
    }

    /**
     * Delete connections between term and ACM groups
     *
     * @since 2.0.0
     * @deprecated 08.2019 See TermAccessHelper
     *
     * @param int $termId Term Id
     */
    public static function disconnectTermFromGroups($termId)
    {
        // @TODO Uncomment after 10.2019
        //_deprecated_function(__FUNCTION__, '3.7.0', 'See TermAccessHelper');

        // @TODO Can be changed to type hinting after 09.2019
        $termId = \LS\Library::toInt($termId);

        delete_term_meta($termId, '_acm_group_id');
    }

    /**
     * Delete connections between posts and ACM group
     *
     * @since 2.0.0
     *
     * @param int $groupId Group Id
     * @param bool $skipPosts True to skip connections to the posts
     * @param bool $skipTerms True to skip connections to the terms
     * @return bool True if group connections deleted
     */
    public static function deleteGroupConnections($groupId, $skipPosts = null, $skipTerms = null): bool
    {
        // @deprecated 07.2019 Legacy arguments
        if ($skipPosts !== null || $skipTerms !== null) {
            _deprecated_argument(__FUNCTION__, '3.6.1', '2nd & 3rd params deprecated');
        }

        // @TODO Can be changed to type hinting after 09.2019
        $groupId = \LS\Library::toInt($groupId);

        if ($groupId < 1) {
            return false;
        }

        $wpdb = LS()->wpdb;

        if (!$skipPosts) {
            $wpdb->delete($wpdb->postmeta, ['meta_key' => '_acm_group_id', 'meta_value' => $groupId]);
        }

        do_action('ls_acm_group_connections_deleted', $groupId);

        return true;
    }

    /**
     * Delete all connections between posts and ACM group
     * @since 2.0.0
     */
    public static function deleteAllGroupConnections()
    {
        $wpdb = LS()->wpdb;
        $wpdb->delete($wpdb->postmeta, ['meta_key' => '_acm_group_id']);

        do_action('ls_acm_all_group_connections_deleted');
    }

    /**
     * Connect group to the post
     *
     * @since 2.0.0
     *
     * @param int $groupId Group Id
     * @param int $postId Post Id
     * @return bool True if group has been connected to the post
     */
    public static function connectGroupToPost($groupId, $postId): bool
    {
        // @TODO Can be changed to type hinting after 09.2019
        $groupId = \LS\Library::toInt($groupId);
        $postId = \LS\Library::toInt($postId);

        if ($groupId < 1 || $postId < 1) {
            return false;
        }

        if (!add_post_meta($postId, '_acm_group_id', $groupId)) {
            return false;
        }

        do_action('ls_acm_group_connected_to_post', $groupId, $postId);

        return true;
    }

    /**
     * Disconnect group from the post
     *
     * @since 3.3.0
     *
     * @param int $groupId Group Id
     * @param int $postId Post Id
     * @return bool True if group has been disconnected from the post
     */
    public static function disconnectGroupFromPost($groupId, $postId): bool
    {
        // @TODO Can be changed to type hinting after 09.2019
        $groupId = \LS\Library::toInt($groupId);
        $postId = \LS\Library::toInt($postId);

        if ($groupId < 1 || $postId < 1) {
            return false;
        }

        if (!delete_post_meta($postId, '_acm_group_id', $groupId)) {
            return false;
        }

        do_action('ls_acm_group_disconnected_from_post', $groupId, $postId);

        return true;
    }

    /**
     * Connect group to the term
     *
     * @since 2.0.0
     * @deprecated 08.2019 See TermAccessHelper
     *
     * @param int $groupId Group Id
     * @param int $termId Term id
     * @return bool True if group has been connected to the term
     */
    public static function connectGroupToTerm($groupId, $termId): bool
    {
        // @TODO Uncomment after 10.2019
        //_deprecated_function(__FUNCTION__, '3.7.0', 'See TermAccessHelper');

        // @TODO Can be changed to type hinting after 09.2019
        $groupId = \LS\Library::toInt($groupId);
        $termId = \LS\Library::toInt($termId);

        if ($groupId < 1 || $termId < 1) {
            return false;
        }

        if (!add_term_meta($termId, '_acm_group_id', $groupId)) {
            return false;
        }

        do_action('ls_acm_group_connected_to_term', $groupId, $termId);

        return true;
    }

    /**
     * Disconnect group from the term
     *
     * @since 3.3.0
     * @deprecated 08.2019 See TermAccessHelper
     *
     * @param int $groupId Group Id
     * @param int $termId Term Id
     * @return bool True if group has been disconnected from the term
     */
    public static function disconnectGroupFromTerm($groupId, $termId): bool
    {
        // @TODO Uncomment after 10.2019
        //_deprecated_function(__FUNCTION__, '3.7.0', 'See TermAccessHelper');

        // @TODO Can be changed to type hinting after 09.2019
        $groupId = \LS\Library::toInt($groupId);
        $termId = \LS\Library::toInt($termId);

        if ($groupId < 1 || $termId < 1) {
            return false;
        }

        if (!delete_term_meta($termId, '_acm_group_id', $groupId)) {
            return false;
        }

        do_action('ls_acm_group_disconnected_from_term', $groupId, $termId);

        return true;
    }

    /**
     * Connect groups to the post
     *
     * @since 2.0.0
     *
     * @param int $postId Post Id
     * @param int[] $newGroupsIds Groups Id's connected to the post
     */
    public static function updatePostConnections($postId, $newGroupsIds)
    {
        // @TODO Can be changed to type hinting after 09.2019
        $postId = \LS\Library::toInt($postId);

        if ($postId > 0) {

            $oldConnectionIds = self::getGroupIdsByPostId($postId);

            foreach (array_diff($oldConnectionIds, $newGroupsIds) as $groupId) {
                self::disconnectGroupFromPost($groupId, $postId);
            }

            foreach (array_diff($newGroupsIds, $oldConnectionIds) as $groupId) {
                self::connectGroupToPost($groupId, $postId);
            }
        }
    }

    /**
     * Connect groups to the term
     *
     * @since 2.0.0
     * @deprecated 08.2019 See TermAccessHelper
     *
     * @param int $termId Term Id
     * @param int[] $newGroupsIds Groups Id's connected to the term
     */
    public static function updateTermConnections($termId, $newGroupsIds)
    {
        // @TODO Uncomment after 10.2019
        //_deprecated_function(__FUNCTION__, '3.7.0', 'See TermAccessHelper');

        // @TODO Can be changed to type hinting after 09.2019
        $termId = \LS\Library::toInt($termId);

        if ($termId > 0) {

            $oldConnectionIds = self::getGroupIdsByTermId($termId);

            foreach (array_diff($oldConnectionIds, $newGroupsIds) as $groupId) {
                self::disconnectGroupFromTerm($groupId, $termId);
            }

            foreach (array_diff($newGroupsIds, $oldConnectionIds) as $groupId) {
                self::connectGroupToTerm($groupId, $termId);
            }
        }
    }

    /**
     * ##################  CONSUMER TO POSTS CONNECTION  #############
     */

    /**
     * Retrieve an array of posts Id's to which consumer DON'T have access
     *
     * @since 2.0.0
     *
     * @param int $consumerId Consumer Id
     * @return array[int]string Post Id's and their post types
     */
    public static function getProtectedPostItemsToConsumer($consumerId)
    {
        // @TODO Can be changed to type hinting after 09.2019
        $consumerId = \LS\Library::toInt($consumerId);

        $items = [];

        $allowedGroups = self::getConsumerGroupsIds($consumerId, true);
        if (empty($allowedGroups)) {
            $items = self::getProtectedPostItems();
        } else {

            $wpdb = LS()->wpdb;
            $query = sprintf("SELECT p.ID, p.post_type
                              FROM {$wpdb->posts} p
                              INNER JOIN {$wpdb->postmeta} pm ON pm.post_id = p.ID AND (
                                  pm.meta_key = '_acm_group_id' AND pm.post_id NOT IN (
                                      SELECT post_id 
                                      FROM {$wpdb->postmeta}
                                      WHERE meta_key = '_acm_group_id' AND meta_value IN (%s)
                                  )
                                )
                              GROUP BY p.ID", implode(',', $allowedGroups));

            foreach ($wpdb->get_results($query) as $record) {
                $items[(int) $record->ID] = $record->post_type;
            }
        }

        return $items;
    }

    /**
     * Retrieve an array of terms Id's to which consumer DON'T have access
     *
     * @since 2.0.0
     * @deprecated 08.2019 See TermAccessHelper
     *
     * @param int $consumerId Consumer Id
     * @return int[] Terms Id's
     */
    public static function getProtectedTermsForConsumer($consumerId)
    {
        // @TODO Uncomment after 10.2019
        //_deprecated_function(__FUNCTION__, '3.7.0', 'See TermAccessHelper');

        // @TODO Can be changed to type hinting after 09.2019
        $consumerId = \LS\Library::toInt($consumerId);

        $terms = [];

        $allowedGroups = self::getConsumerGroupsIds($consumerId, true);
        if (empty($allowedGroups)) {
            $terms = self::getProtectedTerms();
        } else {

            $wpdb = LS()->wpdb;
            $query = sprintf("SELECT t.*, tt.*
                              FROM {$wpdb->prefix}terms t
                              INNER JOIN {$wpdb->prefix}termmeta tm ON tm.term_id = t.term_id AND tm.meta_key = '_acm_group_id'
                              INNER JOIN {$wpdb->prefix}term_taxonomy tt ON tt.term_id = t.term_id
                              WHERE tm.term_id NOT IN (
                                  SELECT term_id FROM {$wpdb->prefix}termmeta WHERE meta_key = '_acm_group_id' AND meta_value IN (%s)
                              )
                              GROUP BY t.term_id", implode(',', $allowedGroups));
            $result = $wpdb->get_results($query);

            if (is_array($result)) {
                foreach ($result as $item) {
                    $terms[] = new WP_Term($item);
                }
            }
        }

        return apply_filters('ls_acm_consumer_protected_terms', $terms, $consumerId);
    }

    /**
     * ##################  MISC  ###########################
     */

    /**
     * Retrieve group Id's connected to a specified company
     *
     * @since 3.5.11
     *
     * @param int $companyId Company Id
     * @return int[] A list of group ID's
     */
    public static function getGroupIdsConnectedToCompany(int $companyId)
    {
        $groups = [];

        foreach (self::getAccessCodesByCompanyId($companyId) as $code) {
            $groups = array_merge($groups, self::getAccessCodeGroupIds($code->getCodeId()));
        }

        $groups = array_values(array_unique($groups));

        return $groups;
    }

    /**
     * Retrieve secure key to get access to specified post without having any Access Code
     *
     * @since 2.3.3
     *
     * @param int $postId Post Id
     * @param bool $booCreateIfNotAvailable True to create a key if not available
     * @return string Secure key
     */
    public static function getSecureKey($postId, $booCreateIfNotAvailable = true): string
    {
        // @TODO Can be changed to type hinting after 09.2019
        $postId = \LS\Library::toInt($postId);
        $booCreateIfNotAvailable = \LS\Library::toBool($booCreateIfNotAvailable);

        $key = '';

        if ($postId > 0) {
            $key = get_post_meta($postId, '_secureLink', true);
            if (empty($key) && $booCreateIfNotAvailable) {
                $key = md5('ac_' . $postId);
                update_post_meta($postId, '_secureLink', $key);
            }
        }

        return $key;
    }

    /**
     * Retrieve URL to the secure key to get access to specified post without having any Access Code
     *
     * @since 2.3.3
     *
     * @param int $postId Post Id
     * @param bool $booCreateIfNotAvailable True to create a key if not available
     * @return string URL
     */
    public static function getSecureURL($postId, $booCreateIfNotAvailable = true): string
    {
        // @TODO Can be changed to type hinting after 09.2019
        $postId = \LS\Library::toInt($postId);
        $booCreateIfNotAvailable = \LS\Library::toBool($booCreateIfNotAvailable);

        $key = self::getSecureKey($postId, $booCreateIfNotAvailable);

        return empty($key) ? '' : add_query_arg(['token' => $key], get_permalink($postId));
    }

    /**
     * Delete module tables
     * @since 2.0.0
     */
    public static function deleteTables()
    {
        $wpdb = LS()->wpdb;

        foreach (['ls_acm_consumers_codes', 'ls_acm_consumers_groups', 'ls_acm_codes', 'ls_acm_groups'] as $t) {
            $wpdb->query(sprintf("DROP TABLE IF EXISTS %s", $wpdb->prefix . $t));
        }
    }

    /**
     * Retrieve all ac-protected posts in a specified group
     *
     * @since 3.6.1
     *
     * @param int $groupId Group Id
     * @return WP_Post[] Posts
     */
    public static function getProtectedPostsInGroup(int $groupId): array
    {
        return array_map(static function (int $postId) {
            return get_post($postId);
        }, self::getProtectedPostIds($groupId));
    }

    /**
     * Retrieve all ac-protected posts to specified group
     *
     * @since 2.0.0
     * @deprecated 07.2019 Use getProtectedPostsInGroup()
     *
     * @param int $groupId Group Id
     * @return array Posts grouped by post types ['post type name' => [Post A, Post B, ...], ...]
     */
    public static function getPostsConnectedToGroup($groupId): array
    {
        _deprecated_function(__FUNCTION__, '3.6.1');

        // @TODO Can be changed to type hinting after 09.2019
        $groupId = \LS\Library::toInt($groupId);

        $result = [];

        foreach (self::getProtectedPostItems($groupId) as $postId => $postType) {

            if (!isset($result[$postType])) {
                $result[$postType] = [];
            }

            $result[$postType][] = get_post($postId);
        }

        return $result;
    }

    /**
     * Retrieve all ac-protected terms to specified group
     *
     * @since 2.0.0
     * @deprecated 07.2019 Legacy
     *
     * @param int $groupId Group Id
     * @return array Terms grouped by taxonomies ['taxonomy name' => [Term A, Term B, ...], ...]
     */
    public static function getTermsConnectedToGroup($groupId): array
    {
        _deprecated_function(__FUNCTION__, '3.6.1');

        // @TODO Can be changed to type hinting after 09.2019
        $groupId = \LS\Library::toInt($groupId);

        $result = [];

        foreach (self::getProtectedTerms($groupId) as $term) {

            if (!isset($result[$term->taxonomy])) {
                $result[$term->taxonomy] = [];
            }

            $result[$term->taxonomy][] = $term;
        }

        return $result;
    }

    /**
     * Retrieve consumers and codes which ends
     *
     * @since 3.5.4
     *
     * @param int $daysToEnd Number of days before/after AC ends (0 = today).
     * @return object[]
     */
    public static function getAccessEndings($daysToEnd)
    {
        // @TODO Can be changed to type hinting after 09.2019
        $daysToEnd = \LS\Library::toInt($daysToEnd);

        $wpdb = LS()->wpdb;
        $endDate = strtotime('YESTERDAY') + $daysToEnd * DAY_IN_SECONDS; // yesterday because maxDate is the last day and end day is 1 day after
        $query = sprintf("SELECT cc.consumerId, cd.codeId, cd.code, cd.companyId, MAX(cc.endDate) maxDate
                          FROM {$wpdb->prefix}ls_acm_consumers_codes cc
                          INNER JOIN {$wpdb->prefix}ls_acm_codes cd ON cd.codeId = cc.codeId AND cd.active = 1
                          INNER JOIN {$wpdb->prefix}ls_consumers as c ON c.consumerId = cc.consumerId AND c.active = 1 AND c.verified = 1 AND c.allowEmails = 1
                          GROUP BY cc.codeId, cc.consumerId
                          HAVING maxDate = '%s'", date('Y-m-d', $endDate));
        $result = $wpdb->get_results($query);
        $result = is_array($result) ? $result : [];

        $codes =
        $companies = [];

        foreach ($result as $record) {

            if (!isset($codes[$record->codeId])) {

                if (!isset($companies[$record->companyId])) {

                    $company = LSCompaniesHelper::getCompany((int) $record->companyId, true);
                    if (!$company) {
                        continue;
                    }

                    $companies[$record->companyId] = $company->getCompanyName(true);
                }

                $codes[$record->codeId] = (object) [
                    'recipients'  => [],
                    'code'        => $record->code,
                    'companyName' => $companies[$record->companyId]
                ];
            }

            $codes[$record->codeId]->recipients[] = (int) $record->consumerId;
        }

        return $codes;
    }

    /**
     * Retrieve a list of consumers which don't have active Access Code
     * @since 2.0.0
     * @return int[] Consumers Id's
     */
    public static function getConsumersWithoutActiveCodes()
    {
        $ids = [];

        foreach (\LS\ConsumersHelper::getConsumersIds(true) as $consumerId) {
            $ids[$consumerId] = self::hasConsumerCodes($consumerId, true);
        }

        return array_keys($ids, false);
    }

    /**
     * Retrieve consumer Ids who lose access codes connection(s) and dates when they lose access
     *
     * @since 3.6.2
     *
     * @param null|DatePeriod $dateRange Date range or null to take results only to the current day
     * @param null|int $companyId Company identifier or null to take results only for the current company
     * @return array Associative array of consumer Id's as keys and end dates as values
     */
    public static function getEndedConsumerAccesses($dateRange = null, $companyId = null): array
    {
        $wpdb = LS()->wpdb;

        if ($dateRange === null) {
            $from = $to = date('Y-m-d');
        } elseif ($dateRange instanceof DatePeriod && $dateRange->getEndDate() !== null) {
            $from = $dateRange->getStartDate()->format('Y-m-d');
            $to = $dateRange->getEndDate()->format('Y-m-d');
        } else {
            _doing_it_wrong(__FUNCTION__, 'Wrong incoming argument', '3.6.2');
            return [];
        }

        $cmpWhere = $companyId ? sprintf(" AND cd.companyId = %d", $companyId) : '';

        $query = sprintf("SELECT DISTINCT cc.consumerId, MAX(cc.endDate) endDate
                          FROM {$wpdb->prefix}ls_acm_consumers_codes cc
                          INNER JOIN {$wpdb->prefix}ls_acm_codes cd ON cd.codeId = cc.codeId 
                          WHERE cd.active = 1 AND cd.drcActive = 1 %s
                          GROUP BY cc.consumerId
                          HAVING endDate BETWEEN '%s' AND '%s'", $cmpWhere, $from, $to);

        $result = $wpdb->get_results($query, ARRAY_A);
        $result = is_array($result) ? $result : [];
        $result = array_column($result, 'endDate', 'consumerId');

        return $result;
    }

    /**
     * Retrieve consumers which have to be removed because of DRC feature
     * If both incoming parameters are null - the current day will be in use
     *
     * @since 2.0.0
     *
     * @param null|DatePeriod Date range or null to take results only to the current day
     * @return array Associative array of consumer Id's and their end dates
     */
    public static function getDRCConsumersToRemove($dateRange = null)
    {
        $result = [];
        $wpdb = LS()->wpdb;

        if ($dateRange === null) {
            $from = $to = date('Y-m-d');
        } elseif ($dateRange instanceof DatePeriod && $dateRange->getEndDate() !== null) {
            $from = $dateRange->getStartDate()->format('Y-m-d');
            $to = $dateRange->getEndDate()->format('Y-m-d');
        } else {
            _doing_it_wrong(__FUNCTION__, 'Wrong incoming argument', '3.5.11');
            $from = $to = '2000-01-01';
        }

        $query = sprintf("SELECT DISTINCT cc.consumerId, cd.drcDays, MAX(cc.endDate) maxDate
                          FROM {$wpdb->prefix}ls_acm_consumers_codes cc
                          INNER JOIN {$wpdb->prefix}ls_acm_codes cd ON cd.codeId = cc.codeId 
                          WHERE cd.active = 1 AND cd.drcActive = 1
                          GROUP BY cc.consumerId
                          HAVING DATE_ADD(maxDate, INTERVAL cd.drcDays DAY) BETWEEN '%s' AND '%s'", $from, $to);

        $drcData = $wpdb->get_results($query);

        foreach ($drcData as $data) {

            $consumerId = (int) $data->consumerId;

            if (!isset($result[$consumerId]) && !self::hasConsumerCodes($consumerId, true)) {
                $result[$consumerId] = date('Y-m-d', strtotime($data->maxDate . ' +' . $data->drcDays . ' DAYS'));
            }
        }

        return $result;
    }

    /**
     * Retrieve activities stats
     *
     * @since 2.0.0
     *
     * @param int $groupId CA-Group Id
     * @param int $startDate Start date
     * @param int $endDate End date
     * @param string[] $columns Columns to fetch
     * @return array Results
     */
    public static function getStats($groupId, $startDate = 0, $endDate = 0, $columns = []): array
    {
        // @TODO Can be changed to type hinting after 09.2019
        $groupId = \LS\Library::toInt($groupId);
        $startDate = \LS\Library::toInt($startDate);
        $endDate = \LS\Library::toInt($endDate);

        // validate
        if ($startDate < 1 || $startDate > $endDate || $endDate >= strtotime('TOMORROW')) {
            $startDate = strtotime('TODAY -1 MONTH');
            $endDate = strtotime('TODAY');
        }

        // round to start of the day
        $startDate = strtotime('TODAY', $startDate);
        $endDate = strtotime('TODAY', $endDate);

        $sd = $startDate;
        $ed = $endDate;
        $columns = array_values(array_unique(array_intersect($columns, ['codesCreated', 'codesUsed', 'codesExpired'])));
        $monthInterval = strtotime(date('Y-m-d', $endDate) . ' -1 YEAR') >= $startDate; // switch view to months
        $weekInterval = !$monthInterval && strtotime(date('Y-m-d', $endDate) . '-3 MONTH') >= $startDate; // switch view to weeks

        // format start date depends on the range
        if ($monthInterval) {
            $startDate = strtotime(date('Y-m-01', $startDate)); // round to start of the month
        } elseif ($weekInterval) {
            $startDate = LS\Library::getMonday($startDate); // round to start of the week
        }

        // prepare group condition depends on the range
        if ($monthInterval) {
            $groupBy = static function ($field) {
                $field = $field == 'eventDate' ? $field : sprintf("FROM_UNIXTIME(%s)", $field);

                return sprintf("CONCAT(YEAR(%s), MONTH(%s))", $field, $field); // take every 1th day of the month
            };
        } elseif ($weekInterval) {
            $groupBy = static function ($field) {
                $field = $field == 'eventDate' ? $field : sprintf("FROM_UNIXTIME(%s)", $field);

                return sprintf("CONCAT(YEAR(%s), WEEKOFYEAR(%s))", $field, $field); // take every Monday
            };
        } else {
            $groupBy = static function ($field) {
                $field = $field == 'eventDate' ? $field : sprintf("FROM_UNIXTIME(%s)", $field);

                return sprintf("DATE(%s)", $field); // take every Monday
            };
        }

        $data = $labels = [];
        $wpdb = LS()->wpdb;

        foreach ($columns as $key => $column) {
            $columns[$key] = esc_sql($column);
        }

        if (!empty($columns)) {

            $dbData = [];

            $arrayColumn = static function ($data) {

                $result = [];
                foreach ($data as $row) {
                    $result[$row->eventDate] = $row->items;
                }

                return $result;
            };

            $groupIdJoin =
                $groupId > 0
                    ? sprintf("INNER JOIN {$wpdb->prefix}ls_acm_code_groups cdg ON cdg.codeId = cd.codeId AND cdg.groupId = %d", $groupId)
                    : '';

            if (in_array('codesCreated', $columns)) {
                $query = sprintf("SELECT COUNT(DISTINCT cd.codeId) items, DATE(FROM_UNIXTIME(cd.createDate)) eventDate
                                  FROM {$wpdb->prefix}ls_acm_codes cd
                                  %s
                                  WHERE cd.createDate BETWEEN %d AND %d
                                  GROUP BY %s", $groupIdJoin, $startDate, $endDate + DAY_IN_SECONDS - 1, $groupBy('cd.createDate'));
                $dbData['codesCreated'] = $arrayColumn($wpdb->get_results($query));
            }

            if (in_array('codesUsed', $columns)) {
                $query = sprintf("SELECT COUNT(DISTINCT cc.codeId) items, DATE(FROM_UNIXTIME(cc.enterDate)) eventDate
                                  FROM {$wpdb->prefix}ls_acm_consumers_codes cc
                                  INNER JOIN {$wpdb->prefix}ls_acm_codes as cd ON cd.codeId = cc.codeId
                                  %s
                                  WHERE cc.enterDate BETWEEN %d AND %d
                                  GROUP BY %s", $groupIdJoin, $startDate, $endDate + DAY_IN_SECONDS - 1, $groupBy('cc.enterDate'));
                $dbData['codesUsed'] = $arrayColumn($wpdb->get_results($query));
            }

            if (in_array('codesExpired', $columns)) {
                $query = sprintf("SELECT COUNT(DISTINCT cc.codeId) items, cc.endDate eventDate
                                  FROM {$wpdb->prefix}ls_acm_consumers_codes cc
                                  INNER JOIN {$wpdb->prefix}ls_acm_codes as cd ON cd.codeId = cc.codeId
                                  %s
                                  WHERE cc.endDate BETWEEN '%s' AND '%s'
                                  GROUP BY %s", $groupIdJoin, date('Y-m-d', $startDate), date('Y-m-d', $endDate + DAY_IN_SECONDS - 1), $groupBy('cc.enterDate'));
                $dbData['codesExpired'] = $arrayColumn($wpdb->get_results($query));
            }

            foreach ($columns as $colNum => $column) {

                $data[$column] = [];

                for ($day = $startDate; $day <= $endDate; $day += DAY_IN_SECONDS) {

                    // get only Mondays for weekly presentation
                    if ($weekInterval && date('N', $day) != 1) {
                        continue;
                    }

                    // get first days of each year for month presentation
                    if ($monthInterval && date('j', $day) != 1) {
                        continue;
                    }

                    // default value
                    $data[$column][date('Y-m-d', $day)] = 0;

                    // create label
                    if ($colNum === 0) {
                        if ($monthInterval) {
                            $labels[] = date('m.Y', $day);
                        } elseif ($weekInterval) {
                            $labels[] = LS\Library::getShortDatesRange($day, min(time(), $day + WEEK_IN_SECONDS - 1));
                        } else {
                            $labels[] = date('d.m.Y', $day);
                        }
                    }
                }
            }

            // fill data with daily values
            foreach ($columns as $column) {
                if (isset($dbData[$column])) {
                    foreach ($dbData[$column] as $eventDate => $itemsCount) {
                        if ($monthInterval) {
                            $data[$column][date('Y-m-01', strtotime($eventDate))] = $itemsCount;
                        } elseif ($weekInterval) {
                            $data[$column][date('Y-m-d', LS\Library::getMonday(strtotime($eventDate)))] = $itemsCount;
                        } else {
                            $data[$column][$eventDate] = $itemsCount;
                        }
                    }
                }
            }

            // skip keys
            foreach ($data as $n => $col) {
                $data[$n] = array_values($col);
            }

            // custom view for a single chart line
            if (count($labels) === 1) {

                $labels[] = $labels[0];

                foreach ($columns as $column) {
                    $data[$column][] = $data[$column][0];
                }
            }
        }

        return [
            'startDate' => date('Y-m-d', $sd),
            'endDate'   => date('Y-m-d', $ed),
            'columns'   => $columns,
            'labels'    => $labels,
            'data'      => $data
        ];
    }

    /**
     * Duplicate group
     *
     * @since 2.4.0
     *
     * @param int $oldGroupId Group Id to be duplicated
     * @return int New group Id
     */
    public static function duplicateGroupByGroupId($oldGroupId): int
    {
        // @TODO Can be changed to type hinting after 09.2019
        $oldGroupId = \LS\Library::toInt($oldGroupId);

        $newGroupId = 0;

        $group = self::getGroup($oldGroupId);
        if ($group) {

            $group
                ->setName('Copy of ' . $group->getName())
                ->setCreateDate(time());

            if (self::createGroup($group)) {

                $newGroupId = $group->getGroupId();

                foreach (self::getProtectedPostIds($oldGroupId) as $postId) {
                    self::connectGroupToPost($newGroupId, $postId);
                }

                do_action('ls_acm_group_duplicated', $oldGroupId, $newGroupId);
            }
        }

        return $newGroupId;
    }

    /**
     * Retrieve Access Codes that ends
     *
     * @since 2.0.0
     * @deprecated 04.2019 use getAccessEndings()
     *
     * @param int $days Number of days before/after AC ends (0 = today).
     * @return object[]
     */
    public static function getCodeEndsToSend($days)
    {
        _deprecated_function(__FUNCTION__, '3.5.4', 'getCodesThatEnds');

        return self::getAccessEndings((int) $days);
    }
}