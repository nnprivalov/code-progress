<?php
define('PHP_UNIT_TEST', true);

require_once __DIR__ . '/../../../../../wp-load.php';

use PHPUnit\Framework\TestCase;

/**
 * @codeCoverageIgnore
 */
class acmLoyaltySuiteTestCase extends TestCase
{
    public function setUp(): void
    {
        CurrentScreenFix::setAsFront();

        global $wpdb;
        $wpdb->query("BEGIN");

        \LS\Library::cleanMessages();
    }

    public function tearDown(): void
    {
        global $wpdb;
        $wpdb->query("ROLLBACK");

        \LS\Library::cleanMessages();
    }

    public function testIgnore()
    {
        $this->assertTrue(true);
    }

    /**
     * @return acmLoyaltySuite|\LS\Module
     */
    protected function module()
    {
        return LS()->getModule('acm');
    }

    /**
     * @throws Exception
     * @return string
     */
    protected function randString()
    {
        return md5(uniqid(random_int(0, 100), true));
    }

    /**
     * @param int $parentCompanyId
     * @return bool|\LS\Company
     */
    protected function createCompany(int $parentCompanyId = 0)
    {
        $company = new \LS\Company([
            'active'          => true,
            'parentCompanyId' => $parentCompanyId,
            'companyName'     => $this->randString()
        ]);

        $this->assertTrue(
            LSCompaniesHelper::saveCompany($company)
        );

        $this->assertNotEmpty($company->getCompanyId());

        return $company;
    }

    /**
     * @return false|\LS\Consumer
     */
    protected function createConsumer()
    {
        $company = $this->createCompany();

        $consumer = new \LS\Consumer([
            'companyId' => $company->getCompanyId()
        ]);

        $this->assertNotEmpty(
            \LS\ConsumersHelper::createConsumer($consumer)
        );

        $this->assertNotEmpty($consumer->getConsumerId());

        return $consumer;
    }

    /**
     * @return WP_Post
     */
    protected function createPost()
    {
        $postId = wp_insert_post([
            'post_title'   => $this->randString(),
            'post_content' => 'test',
            'post_status'  => 'publish',
            'post_type'    => 'post'
        ]);

        $post = get_post($postId);

        $this->assertInstanceOf(WP_Post::class, $post);

        return $post;
    }

    /**
     * @param array $data
     * @return ACMGroup
     */
    protected function createGroup($data = [])
    {
        $data = array_merge([
            'name' => $this->randString()
        ], $data);

        $group = new ACMGroup($data);
        $groupId = ACMHelper::createGroup($group);

        $this->assertNotEmpty($groupId);
        $this->assertSame($groupId, $group->getGroupId());
        $this->assertInstanceOf(ACMGroup::class, $group);

        return $group;
    }

    /**
     * @param WP_Post $post
     * @param ACMGroup $group
     */
    protected function protectPost(WP_Post $post, ACMGroup $group)
    {
        $this->assertTrue(
            ACMHelper::connectGroupToPost($group->getGroupId(), $post->ID)
        );
    }

    /**
     * @param \LS\ConsumerInterface $consumer
     * @param ACMGroup $group
     */
    protected function allowConsumerAccessToGroup(\LS\ConsumerInterface $consumer, ACMGroup $group)
    {
        $this->assertTrue(
            ACMHelper::connectConsumerToGroup($consumer->getConsumerId(), $group->getGroupId())
        );
    }

    /**
     * @param array $data
     * @return ACMCode
     */
    protected function createAccessCode($data = [])
    {
        $data = array_merge(
            [
                'code'      => $this->randString(),
                'startDate' => time(),
                'endDate'   => time() + 3600,
                'limit'     => 0
            ], $data
        );

        $code = new ACMCode($data);

        $this->assertNotEmpty(
            ACMHelper::createAccessCode($code)
        );

        $this->assertNotEmpty($code->getCodeId());

        return $code;
    }

    /**
     * @param \LS\ConsumerInterface $consumer
     */
    protected function loginConsumer(\LS\ConsumerInterface $consumer)
    {
        /** @var consumersLoyaltySuite $consumersModule */
        $consumersModule = LS()->getModule('consumers');

        $this->assertTrue(
            $consumersModule->authorizeConsumer($consumer->getConsumerId(), false, null, true)
        );
    }

    protected function logoutConsumer()
    {
        /** @var consumersLoyaltySuite $consumersModule */
        $consumersModule = LS()->getModule('consumers');

        $this->assertTrue(
            $consumersModule->logoutConsumer()
        );
    }

    /**
     * @param WP_Post $post
     * @return WP_Comment
     */
    protected function addComment(WP_Post $post)
    {
        $commentId = wp_insert_comment([
            'comment_content' => 'test',
            'comment_post_ID' => $post->ID
        ]);

        $comment = get_comment($commentId);

        $this->assertInstanceOf(WP_Comment::class, $comment);
        $this->assertSame($post->ID, (int) $comment->comment_post_ID);

        return $comment;
    }

    /**
     * @param \LS\ConsumerInterface $consumer
     * @param ACMCode $code
     */
    protected function connectConsumerToAccessCode(\LS\ConsumerInterface $consumer, ACMCode $code)
    {
        $this->assertTrue(
            ACMHelper::connectConsumerToAccessCode($consumer, $code)
        );
    }

    /**
     * @param ACMCode $code
     */
    protected function updateAccessCode(ACMCode $code)
    {
        $this->assertTrue(
            ACMHelper::updateAccessCode($code)
        );
    }

    /**
     * @param ACMGroup $group
     * @param ACMCode $code
     */
    protected function connectGroupToAccessCode(ACMGroup $group, ACMCode $code)
    {
        $this->assertTrue(
            ACMHelper::connectAccessCodeToGroup($code->getCodeId(), $group->getGroupId())
        );
    }

    /**
     * @param \LS\ConsumerInterface $consumer
     * @param ACMGroup $group
     * @return bool
     */
    protected function hasConsumerGroup(\LS\ConsumerInterface $consumer, ACMGroup $group): bool
    {
        return ACMHelper::hasConsumerConnectionToGroup($consumer->getConsumerId(), $group->getGroupId());
    }

    /**
     * @param int $consumerId
     * @return \LS\ConsumerInterface
     */
    protected function getConsumer(int $consumerId): \LS\ConsumerInterface
    {
        $consumer = \LS\ConsumersHelper::getConsumer($consumerId);

        $this->assertInstanceOf(\LS\ConsumerInterface::class, $consumer);

        return $consumer;
    }

    /**
     * @param \LS\ConsumerInterface $consumer
     * @param ACMGroup $group
     */
    protected function connectConsumerToGroup(\LS\ConsumerInterface $consumer, ACMGroup $group)
    {
        $this->assertTrue(
            ACMHelper::connectConsumerToGroup($consumer->getConsumerId(), $group->getGroupId())
        );
    }
}

if (!class_exists('CurrentScreenFix')) {

    class CurrentScreenFix
    {
        static $inAdmin = false;

        public static function setAsAdmin()
        {
            self::$inAdmin = true;
        }

        public static function setAsFront()
        {
            self::$inAdmin = false;
        }

        public function in_admin()
        {
            return self::$inAdmin;
        }
    }

    $GLOBALS['current_screen'] = new CurrentScreenFix();
}