<?php
/**
 * Created by PhpStorm.
 * User: made
 * Date: 2018-07-18
 * Time: 09:34
 */

define('PHP_UNIT_TEST', true);
define('WP_ADMIN', true);

require_once __DIR__ . '/../../../../../wp-load.php';

use PHPUnit\Framework\TestCase;

class acmLoyaltySuiteTestAccessCodes extends TestCase
{
    /** @var acmLoyaltySuite */
    protected $module;

    public function setUp(): void
    {
        $this->module = LS()->getModule('acm');

        global $wpdb;
        $wpdb->query("BEGIN");
    }

    public function tearDown(): void
    {
        global $wpdb;
        $wpdb->query("ROLLBACK");
    }

    /**
     * @param int $parentCompanyId
     * @return false|\LS\Company
     */
    public function createTestCompany(int $parentCompanyId = 0)
    {
        $company = new \LS\Company([
            'active'          => true,
            'parentCompanyId' => $parentCompanyId
        ]);

        if(!LSCompaniesHelper::saveCompany($company)) {
            $company = false;
        }

        return $company;
    }

    public function testPortalAccess()
    {
        $code1 = new ACMCode([
            'codeId'    => 9999,
            'code'      => '1234567890',
            'startDate' => time(),
            'endDate'   => time() + 100,
            'limit'     => 0
        ]);

        $this->assertTrue($code1->valid());
        $this->assertFalse($code1->hasLimit());
        $this->assertFalse($code1->hasLifetime());
        $this->assertFalse($code1->isLimitReached());

        ACMHelper::createAccessCode($code1, [5555]);

        $this->assertNotEmpty($code1->getCodeId());

        $company1 = $this->createTestCompany();

        $this->assertNotFalse($company1);

        $updated = ACMHelper::updateAccessCode(
            $code1->setCompanyId($company1->getCompanyId())
        );

        $this->assertTrue($updated);
        $this->assertSame($company1->getCompanyId(), $code1->getCompanyId());

        $code1->setLimit(10);
        $this->assertTrue($code1->hasLimit());

        $code1->setLifetime(time());
        $this->assertTrue($code1->hasLifetime());

        $code1->set('usages', 10);
        $this->assertSame(10, $code1->getUsages());
        $this->assertTrue($code1->isLimitReached());
        $code1->set('usages', 0);

        $this->assertSame('1234567890', $code1->getCode());
        $this->assertTrue($code1->getStartDate() > 0);
        $this->assertTrue($code1->getEndDate() >= $code1->getStartDate());

        $this->assertContains(5555, ACMHelper::getAccessCodeGroupIds($code1->getCodeId()));

        $success = ACMHelper::connectAccessCodeToGroup($code1->getCodeId(), 7777);

        $this->assertTrue($success);
        $this->assertTrue(ACMHelper::hasAccessCodeConnectionToGroup($code1->getCodeId(), 7777));
        $this->assertContains(7777, ACMHelper::getAccessCodeGroupIds($code1->getCodeId()));

        ACMHelper::disconnectAccessCodeFromGroup($code1->getCodeId(), 7778);

        $this->assertTrue(ACMHelper::hasAccessCodeConnectionToGroup($code1->getCodeId(), 7777));
        $this->assertContains(7777, ACMHelper::getAccessCodeGroupIds($code1->getCodeId()));

        $success = ACMHelper::disconnectAccessCodeFromGroup($code1->getCodeId(), 7777);

        $this->assertTrue($success);
        $this->assertFalse(ACMHelper::hasAccessCodeConnectionToGroup($code1->getCodeId(), 7777));
        $this->assertNotContains(7777, ACMHelper::getAccessCodeGroupIds($code1->getCodeId()));
        $this->assertCount(1, ACMHelper::getAccessCodeGroupIds($code1->getCodeId()));

        ACMHelper::connectAccessCodeToGroup($code1->getCodeId(), 8888);
        ACMHelper::connectAccessCodeToGroup($code1->getCodeId(), 9999);

        $this->assertCount(3, ACMHelper::getAccessCodeGroupIds($code1->getCodeId()));

        ACMHelper::disconnectAccessCodeFromAllGroups($code1->getCodeId());

        $this->assertEmpty(ACMHelper::getAccessCodeGroupIds($code1->getCodeId()));

        $code2 = new ACMCode([
            'codeId'    => 7575,
            'code'      => '1234567891',
            'startDate' => time(),
            'endDate'   => time() + 100,
            'limit'     => 0
        ]);

        $code2Id = ACMHelper::createAccessCode($code2, [6969, 7575]);

        $this->assertNotEmpty($code2Id);
        $this->assertContains(6969, ACMHelper::getAccessCodeGroupIds($code2->getCodeId()));
        $this->assertContains(7575, ACMHelper::getAccessCodeGroupIds($code2->getCodeId()));

        $code3 = ACMHelper::getAccessCode($code2->getCodeId());

        $this->assertContains(6969, ACMHelper::getAccessCodeGroupIds($code3->getCodeId()));

        $company2 = $this->createTestCompany();
        $code1->setCompanyId($company2->getCompanyId());
        $this->assertSame($company2->getCompanyId(), $code1->getCompanyId());

        $code1->setCode(' 123123 ');
        $this->assertSame('123123', $code1->getCode());

        $code1->setLimit('05');
        $this->assertSame(5, $code1->getLimit());

        $code1->setLimit(strtotime('TOMORROW'));
        $this->assertSame(strtotime('TOMORROW'), $code1->getLimit());

        $this->assertTrue($code1->canAssignCompany());
        $code1->setAssignCompany(false);
        $this->assertFalse($code1->canAssignCompany());

        $code1->setNote(' Тупа мавпочка ');
        $this->assertSame('Тупа мавпочка', $code1->getNote());

        $code1->setCreateDate(strtotime('TODAY'));
        $this->assertSame(strtotime('TODAY'), $code1->getCreateDate());

        $code1->setStartDate(strtotime('TODAY'));
        $this->assertSame(strtotime('TODAY'), $code1->getStartDate());

        $code1->setEndDate(strtotime('TODAY'));
        $this->assertSame(strtotime('TODAY'), $code1->getEndDate());

        $this->assertTrue($code1->isActive());
        $code1->markAsInactive();
        $this->assertFalse($code1->isActive());
        $code1->markAsActive();
        $this->assertTrue($code1->isActive());

        $this->assertFalse($code1->isEmailPatternActive());
        $code1->setEmailPattern('*@gmail.com');
        $this->assertTrue($code1->isEmailPatternActive());
        $this->assertTrue($code1->matchEmail('test@gmail.com'));
        $code1->setEmailPattern('*@gesundheit-bewegt.com');
        $this->assertTrue($code1->matchEmail('ap@gesundheit-bewegt.com'));
        $this->assertFalse($code1->matchEmail('test@gmail.net'));
        $code1->setEmailPattern('*@d-e.f.com');
        $this->assertTrue($code1->matchEmail('a.b-c@d-e.f.com'));

        $this->assertFalse($code1->isDRCActive());
        $code1->markDRCAsActive();
        $this->assertTrue($code1->isDRCActive());
        $code1->markDRCAsInactive();
        $this->assertFalse($code1->isDRCActive());

        $code1->setDRCDays(5);
        $this->assertSame(5, $code1->getDRCDays());

        $this->assertTrue($code1->isActual());
        $this->assertTrue($code1->canBeUsed());

        $this->assertFalse($code1->isExpired());
        $code1->setEndDate(time() - 1);
        $this->assertFalse($code1->isExpired());
        $code1->setEndDate(time() - DAY_IN_SECONDS);
        $this->assertTrue($code1->isExpired());

        $this->assertFalse($code1->isUpcoming());
        $code1->setStartDate(time() + 100);
        $this->assertTrue($code1->isUpcoming());

        $this->assertFalse($code1->isActual());
        $this->assertFalse($code1->canBeUsed());

        $groupId = ACMHelper::createGroup(new ACMGroup([
            'name'          => 'Parent group',
            'parentGroupId' => 0,
            'description'   => 'desc'
        ]));

        $group2Id = ACMHelper::createGroup(new ACMGroup([
            'name'          => 'Child group',
            'parentGroupId' => $groupId,
            'description'   => 'desc'
        ]));

        $this->assertNotEmpty($groupId);
        $this->assertNotEmpty($group2Id);

        $company3 = $this->createTestCompany($company2->getCompanyId());

        $code1->setFromArray([
            'code'      => '1234567890',
            'groupId'   => $group2Id,
            'companyId' => $company3->getCompanyId(),
            'active'    => true,
            'startDate' => 1,
            'endDate'   => time() + 1000 * DAY_IN_SECONDS,
            'limit'     => 100,
            'usages'    => 0
        ]);

        $this->assertTrue(ACMHelper::updateAccessCode($code1));
        $this->assertNotEmpty(ACMHelper::getAccessCodeByCode('1234567890'));

        $ids = static function ($ids) {
            return array_map(static function (ACMCode $c) {
                return $c->getCodeId();
            }, $ids);
        };

        $this->assertContains($code1->getCodeId(), $ids(ACMHelper::getAccessCodes()));
        $this->assertContains($code1->getCodeId(), $ids(ACMHelper::getAccessCodesByIds([$code1->getCodeId()], true)));
        $this->assertContains($code1->getCodeId(), $ids(ACMHelper::getAccessCodesByGroupId($groupId)));
        $this->assertContains($code1->getCodeId(), $ids(ACMHelper::getAccessCodesByGroupId($group2Id)));
        $this->assertNotContains($code1->getCodeId(), $ids(ACMHelper::getAccessCodesByGroupId($groupId, false)));
        $this->assertContains($code1->getCodeId(), $ids(ACMHelper::getAccessCodesByCompanyId($company3->getCompanyId())));
        $this->assertContains($code1->getCodeId(), $ids(ACMHelper::getAccessCodesByCompanyId($company2->getCompanyId())));
        $this->assertNotContains($code1->getCodeId(), $ids(ACMHelper::getAccessCodesByCompanyId($company2->getCompanyId(), false)));

        $consumer = new \LS\Consumer([
            'active' => true
        ]);

        \LS\ConsumersHelper::createConsumer($consumer);
        $consumerId = $consumer->getConsumerId();

        $this->assertTrue($consumerId > 0);

        $code1->setLifetime(0);
        $this->assertFalse($code1->hasLifetime());
        $d1 = $code1->getEndDate();

        $this->assertTrue(ACMHelper::connectConsumerToAccessCode($consumer, $code1));
        $this->assertTrue(ACMHelper::connectConsumerToAccessCode($consumer, $code1));

        $code1->setEndDate($code1->getEndDate() + DAY_IN_SECONDS);
        $d2 = $code1->getEndDate();

        $this->assertNotSame($d1, $d2);

        $ccs = ACMHelper::getConsumerCodesConnections($consumerId, true);

        $this->assertTrue(count($ccs) > 1);
        $cc = false;
        foreach ($ccs as $cc1) {
            if ($cc1->getCodeId() === $code1->getCodeId()) {
                $cc = $cc1;
                break;
            }
        }
        $this->assertNotEmpty($cc);

        $latestCcs = ACMHelper::getLatestConsumerCodesConnections($consumerId, true);
        $this->assertNotEmpty($latestCcs);

        $latestCc = false;
        foreach ($latestCcs as $cc1) {
            if ($cc1->getCodeId() === $code1->getCodeId()) {
                $latestCc = $cc1;
                break;
            }
        }

        $this->assertSame($cc->getEndDate(), $latestCc->getEndDate());

        $cc = ACMHelper::getConsumerCodeConnection($cc->getId());
        $this->assertInstanceOf(ACMConsumerCodeConnection::class, $cc);

        $this->assertSame($latestCc->getEndDate(), ACMHelper::calculateConsumerCodesExpirationDate($consumerId));

        $found = false;
        foreach (ACMHelper::getConsumerConnectionsToAccessCode($code1->getCodeId()) as $cc1) {
            if ($cc1->getId() === $latestCc->getId()) {
                $found = true;
            }
        }
        $this->assertTrue($found);

        $found = false;
        foreach (ACMHelper::getLatestConsumerConnectionsToAccessCode($code1->getCodeId()) as $cc1) {
            if ($cc1->getId() === $latestCc->getId()) {
                $found = true;
            }
        }
        $this->assertTrue($found);

        $this->assertContains($consumerId, ACMHelper::getConsumersIdsConnectedToAccessCode($code1->getCodeId()));

        $found = false;
        foreach (ACMHelper::getConsumerAccessCodes($consumerId, true) as $cd) {
            if ($cd->getCodeId() === $code1->getCodeId()) {
                $found = true;
            }
        }
        $this->assertTrue($found);

        $this->assertSame(1, ACMHelper::countCodesInGroup($groupId));

        $this->assertContains($code1->getCodeId(), ACMHelper::getConsumerAccessCodesIds($consumerId));

        $this->assertTrue(ACMHelper::hasConsumerCodes($consumerId, true));

        $this->assertTrue(ACMHelper::hasConsumerCode($consumerId, $code1->getCode()));

        $this->assertTrue(ACMHelper::deleteConsumerCodeConnection($latestCc));

        $this->assertTrue(ACMHelper::hasConsumerCodeByCodeId($consumerId, $code1->getCodeId()));
        $this->assertTrue(ACMHelper::disconnectConsumerFromAccessCode($consumerId, $code1->getCodeId()));
        $this->assertFalse(ACMHelper::hasConsumerCodeByCodeId($consumerId, $code1->getCodeId()));

        ACMHelper::connectConsumerToGroup($consumerId, $groupId);

        $this->assertTrue(ACMHelper::connectConsumerToAccessCode($consumer, $code1));

        ACMHelper::disconnectConsumerFromAccessCodes($consumerId);

        $this->assertFalse(ACMHelper::hasConsumerCodes($consumerId));

        $this->assertTrue(ACMHelper::deleteAccessCode($code1->getCodeId()));
        $this->assertEmpty(ACMHelper::getAccessCode($code1->getCodeId()));
    }
}