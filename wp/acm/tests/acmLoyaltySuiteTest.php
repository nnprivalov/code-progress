<?php
require_once __DIR__ . '/acmLoyaltySuiteTestCase.php';

class acmLoyaltySuiteTest extends acmLoyaltySuiteTestCase
{
    public function setUp(): void
    {
        parent::setUp();

        CurrentScreenFix::setAsFront();
    }

    public function test__addConsumerCodeAction()
    {
        $code = $this->createAccessCode();
        $consumer = $this->createConsumer();

        $this->module()->addConsumerCodeAction($consumer, $code->getCode());

        $this->assertFalse(\LS\Library::hasErrors());
        $this->assertTrue(\LS\Library::hasAlerts());
    }

    public function test__getProtectedPostIds()
    {
        $post = $this->createPost();
        $post2 = $this->createPost();
        $group = $this->createGroup();

        $protectedPostIds = $this->module()->getProtectedPostIds();

        $this->assertNotContains($post->ID, $protectedPostIds);
        $this->assertNotContains($post2->ID, $protectedPostIds);

        $this->protectPost($post, $group);

        $protectedPostIds = $this->module()->getProtectedPostIds();

        $this->assertContains($post->ID, $protectedPostIds);
        $this->assertNotContains($post2->ID, $protectedPostIds);
    }

    public function test__isPostProtected()
    {
        $post = $this->createPost();
        $group = $this->createGroup();

        $this->assertFalse($this->module()->isPostProtected($post->ID));

        $this->protectPost($post, $group);

        $this->assertTrue($this->module()->isPostProtected($post->ID));
    }

    public function test__getProtectedPostIdsToConsumer()
    {
        $consumer = $this->createConsumer();
        $post = $this->createPost();
        $group = $this->createGroup();

        $this->protectPost($post, $group);

        $this->assertContains($post->ID, $this->module()->getProtectedPostIdsToConsumer($consumer->getConsumerId()));

        $this->allowConsumerAccessToGroup($consumer, $group);

        $this->assertNotContains($post->ID, $this->module()->getProtectedPostIdsToConsumer($consumer->getConsumerId()));
    }

    public function test__hasAccessToPost()
    {
        $module = $this->module();
        $post = $this->createPost();
        $group = $this->createGroup();
        $consumer = $this->createConsumer();
        $post2 = $this->createPost();
        $group2 = $this->createGroup();

        $this->assertTrue($module->hasAccessToPost($post->ID, $consumer->getConsumerId()));
        $this->assertTrue($module->hasAccessToPost($post2->ID, $consumer->getConsumerId()));

        $this->protectPost($post, $group);
        $this->protectPost($post2, $group2);
        $this->allowConsumerAccessToGroup($consumer, $group2);

        $this->assertFalse($module->hasAccessToPost($post->ID, $consumer->getConsumerId()));
        $this->assertTrue($module->hasAccessToPost($post2->ID, $consumer->getConsumerId()));

        $_REQUEST['token'] = ACMHelper::getSecureKey($post->ID);

        $this->assertTrue($module->hasAccessToPost($post->ID, $consumer->getConsumerId()));
    }

    public function test__preGetPosts()
    {
        $post = $this->createPost();
        $group = $this->createGroup();
        $consumer = $this->createConsumer();

        $posts = get_posts([
            'post__in' => [$post->ID],
            'fields'   => 'ids'
        ]);

        $this->assertContains($post->ID, $posts);

        $this->protectPost($post, $group);

        $posts = get_posts([
            'post__in' => [$post->ID],
            'fields'   => 'ids'
        ]);

        $this->assertNotContains($post->ID, $posts);

        $this->allowConsumerAccessToGroup($consumer, $group);

        $posts = get_posts([
            'post__in' => [$post->ID],
            'fields'   => 'ids'
        ]);

        $this->assertNotContains($post->ID, $posts);

        $this->loginConsumer($consumer);

        $posts = get_posts([
            'post__in' => [$post->ID],
            'fields'   => 'ids'
        ]);

        $this->logoutConsumer();

        $this->assertContains($post->ID, $posts);
    }

    public function test__wpNavMenuObjects()
    {
        $post = $this->createPost();
        $post2 = $this->createPost();
        $group = $this->createGroup();

        $post->type = 'post_type';
        $post->object_id = $post->ID;

        $post2->type = 'post_type';
        $post2->object_id = $post2->ID;

        $this->protectPost($post2, $group);

        $posts = $this->module()->wpNavMenuObjects([$post, $post2]);

        $this->assertContains($post, $posts);
        $this->assertNotContains($post2, $posts);
    }

    public function test__preGetComments()
    {
        $post = $this->createPost();
        $group = $this->createGroup();
        $comment = $this->addComment($post);

        $comments = get_comments([
            'post_id' => $post->ID,
            'fields'  => 'ids'
        ]);

        $this->assertContains($comment->comment_ID, $comments);

        $this->protectPost($post, $group);

        $comments = get_comments([
            'post_id' => $post->ID,
            'fields'  => 'ids'
        ]);

        $this->assertNotContains($comment->comment_ID, $comments);
    }

    public function test__validateAccessCodeToSubmit()
    {
        $code = $this->createAccessCode();
        $consumer = $this->createConsumer();

        $this->assertEmpty(
            $this->module()->validateAccessCodeToSubmit($code->getCode())
        );

        $this->assertEmpty(
            $this->module()->validateAccessCodeToSubmit($code)
        );

        $this->assertSame(
            $this->module()->getOption('wrongCodeErrorMsg'),
            $this->module()->validateAccessCodeToSubmit(false)
        );

        $codeTest = (clone $code)
            ->setLimit(1)
            ->setUsages(1);

        $this->assertSame(
            $this->module()->getOption('codeUsedErrorMsg'),
            $this->module()->validateAccessCodeToSubmit($codeTest)
        );

        $codeTest = (clone $code)
            ->setStartDate(strtotime('TOMORROW'));

        $this->assertSame(
            $this->module()->getOption('codeNotStartedErrorMsg'),
            $this->module()->validateAccessCodeToSubmit($codeTest)
        );

        $codeTest = (clone $code)
            ->setEndDate(strtotime('YESTERDAY'));

        $this->assertSame(
            $this->module()->getOption('codeIsEndedErrorMsg'),
            $this->module()->validateAccessCodeToSubmit($codeTest)
        );

        $codeTest = (clone $code)
            ->markAsInactive();

        $this->assertSame(
            $this->module()->getOption('wrongCodeErrorMsg'),
            $this->module()->validateAccessCodeToSubmit($codeTest)
        );

        $codeTest = (clone $code)
            ->setEmailPattern('*@example.com');

        $this->assertEmpty(
            $this->module()->validateAccessCodeToSubmit($codeTest, 'test@example.com')
        );

        $this->assertSame(
            $this->module()->getOption('wrongCodePatternErrorMsg'),
            $this->module()->validateAccessCodeToSubmit($codeTest, 'test@fake.com')
        );

        $this->assertEmpty(
            $this->module()->validateAccessCodeToSubmit($codeTest, null, $consumer->getConsumerId())
        );

        $this->connectConsumerToAccessCode($consumer, $code);

        $this->assertSame(
            $this->module()->getOption('codeUsedErrorMsg'),
            $this->module()->validateAccessCodeToSubmit($codeTest, null, $consumer->getConsumerId())
        );
    }

    public function test__consumerSaveValidation()
    {
        $code = $this->createAccessCode();
        $consumer = $this->createConsumer();

        $data = [
            'email'       => 'test@example.com',
            'actionsCode' => $code->getCode()
        ];

        $this->assertEmpty(
            $this->module()->consumerSaveValidation([], $data, $consumer->getConsumerId())
        );

        $code->setEmailPattern('*@fake.com');
        $this->updateAccessCode($code);

        $this->assertNotEmpty(
            $this->module()->consumerSaveValidation([], $data, $consumer->getConsumerId())
        );

        $this->connectConsumerToAccessCode($consumer, $code);

        $data = [
            'email' => 'test@example.com'
        ];

        $this->assertContains(
            $this->module()->getOption('wrongCodePatternErrorMsg'),
            $this->module()->consumerSaveValidation([], $data, $consumer->getConsumerId())
        );
    }

    public function test__consumerSave()
    {
        $code = $this->createAccessCode();
        $consumer = $this->createConsumer();

        $consumer->addDynamicValue('actionsCode', $code->getCode());

        $this->module()->consumerCreated($consumer);

        $this->assertTrue(
            ACMHelper::hasConsumerCode($consumer->getConsumerId(), $code->getCode())
        );
    }

    public function test__consumerBeforeDeleted()
    {
        $code = $this->createAccessCode();
        $group = $this->createGroup();
        $consumer = $this->createConsumer();

        $this->connectConsumerToAccessCode($consumer, $code);
        $this->allowConsumerAccessToGroup($consumer, $group);

        $this->module()->consumerBeforeDeleted($consumer);

        $this->assertFalse(ACMHelper::hasConsumerCode($consumer->getConsumerId(), $code->getCode()));
        $this->assertFalse($this->hasConsumerGroup($consumer, $group));
    }

    public function test__checkConsumerLogin()
    {
        $code = $this->createAccessCode();
        $consumer = $this->createConsumer();

        $this->module()->setOption('acRequired', true);

        $this->assertFalse(
            $this->module()->checkConsumerLogin(true, $consumer->getConsumerId())
        );

        $this->connectConsumerToAccessCode($consumer, $code);

        $this->assertTrue(
            $this->module()->checkConsumerLogin(true, $consumer->getConsumerId())
        );
    }

    public function test__checkAuthorizationAccess()
    {
        $post = $this->createPost();

        $this->assertFalse(
            $this->module()->checkAuthorizationAccess(false, $post->ID)
        );

        $_REQUEST['token'] = ACMHelper::getSecureKey($post->ID);

        $this->assertTrue(
            $this->module()->checkAuthorizationAccess(false, $post->ID)
        );
    }

    public function test__assignCompanyToConsumer()
    {
        $code = $this->createAccessCode();
        $consumer = $this->createConsumer();
        $company2 = $this->createCompany();

        $this->assertNotSame($company2->getCompanyId(), $consumer->getCompanyId());

        $code
            ->setCompanyId($company2->getCompanyId())
            ->setAssignCompany(true);

        $this->module()->assignCompanyToConsumer($consumer, $code);

        $consumer = $this->getConsumer($consumer->getConsumerId());

        $this->assertSame($company2->getCompanyId(), $consumer->getCompanyId());
    }

    public function test__hasCompanyAccessToPost()
    {
        $company = $this->createCompany();
        $post = $this->createPost();
        $group = $this->createGroup();
        $code = $this->createAccessCode();

        $this->assertTrue(
            $this->module()->hasCompanyAccessToPost($company->getCompanyId(), $post->ID)
        );

        $this->protectPost($post, $group);

        $this->assertFalse(
            $this->module()->hasCompanyAccessToPost($company->getCompanyId(), $post->ID)
        );

        $code->setCompanyId($company->getCompanyId());
        $this->updateAccessCode($code);
        $this->connectGroupToAccessCode($group, $code);

        $this->assertTrue(
            $this->module()->hasCompanyAccessToPost($company->getCompanyId(), $post->ID)
        );
    }
}