<?php
/**
 * Created by PhpStorm.
 * User: made
 * Date: 2018-07-18
 * Time: 09:34
 */

define('PHP_UNIT_TEST', true);
define('WP_ADMIN', true);

require_once __DIR__ . '/../../../../../wp-load.php';

use PHPUnit\Framework\TestCase;

class acmLoyaltySuiteTestTerms extends TestCase
{
    /** @var acmLoyaltySuite */
    protected $module;

    public function setUp(): void
    {
        $this->module = LS()->getModule('acm');

        global $wpdb;
        $wpdb->query("BEGIN");
    }

    public function tearDown(): void
    {
        global $wpdb;
        $wpdb->query("ROLLBACK");
    }

    public function testTerms()
    {
        $data = [
            'name'          => 'Test 1',
            'parentGroupId' => 0,
            'description'   => 'desc'
        ];

        $groupId1 = ACMHelper::createGroup(new ACMGroup($data));

        $termId = get_terms()[0]->term_id;

        $this->assertNotEmpty($termId);
        $this->assertTrue(ACMHelper::connectGroupToTerm($groupId1, $termId));
        $this->assertNotEmpty(ACMHelper::getProtectedTerms($groupId1));
        $this->assertContains($termId, ACMHelper::getProtectedTermsIds($groupId1));
        $this->assertCount(count(ACMHelper::getProtectedTerms($groupId1)), ACMHelper::getProtectedTermsIds($groupId1));
        $this->assertContains($groupId1, ACMHelper::getGroupIdsByTermId($termId));
    }
}