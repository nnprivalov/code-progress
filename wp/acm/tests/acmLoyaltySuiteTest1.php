<?php
/**
 * Created by PhpStorm.
 * User: made
 * Date: 2018-07-18
 * Time: 09:34
 */

define('PHPUNITTEST', true);
define('WP_ADMIN', true);

require_once '../../../../../wp-load.php';

use PHPUnit\Framework\TestCase;

class acmLoyaltySuiteTest1 extends TestCase
{
    /** @var acmLoyaltySuite */
    protected $module;

    public function setUp(): void
    {
        define('PHP_UNIT_TEST', true);

        $this->module = LS()->getModule('acm');

        global $wpdb;
        $wpdb->query("BEGIN");
    }

    public function tearDown(): void
    {
        global $wpdb;
        $wpdb->query("ROLLBACK");
    }

    public function testContentAccess()
    {
        $data = [
            'name'          => 'Test 1',
            'parentGroupId' => 0,
            'description'   => 'desc'
        ];

        $groupId1 = ACMHelper::createGroup(new ACMGroup($data));

        $this->assertNotEmpty($groupId1);

        $data = [
            'name'   => 'Test',
            'active' => false
        ];

        $groupId2 = ACMHelper::createGroup(new ACMGroup($data));
        $this->assertNotEmpty($groupId2);

        $group1 = ACMHelper::getGroup($groupId1);

        $this->assertInstanceOf(ACMGroup::class, $group1);
        $this->assertEquals($group1->getName(), 'Test 1');
        $this->assertEmpty($group1->getParentGroupId());
        $this->assertEquals($group1->getDescription(), 'desc');
        $this->assertTrue($group1->isActive());

        $group2 = ACMHelper::getGroup($groupId2);

        $this->assertInstanceOf(ACMGroup::class, $group2);
        $this->assertEquals($group2->getName(), 'Test');
        $this->assertFalse($group2->isActive());
        $this->assertEmpty($group2->getParentGroupId());

        $group2
            ->setDescription('desc 2')
            ->setCreateDate(1000)
            ->setName('Test 2')
            ->setParentGroupId($groupId1)
            ->markAsActive();

        $this->assertTrue(ACMHelper::updateGroup($group2));

        $group2 = ACMHelper::getGroup($groupId2);
        $this->assertEquals($group2->getName(), 'Test 2');
        $this->assertEquals($group2->getCreateDate(), 1000);
        $this->assertEquals($group2->getParentGroupId(), $groupId1);
        $this->assertEquals($group2->getDescription(), 'desc 2');
        $this->assertTrue($group2->isActive());

        $groupsIds = array_map(function(ACMGroup $g) {
            return $g->getGroupId();
        }, ACMHelper::getGroups());

        $this->assertContains($groupId1, $groupsIds);
        $this->assertContains($groupId2, $groupsIds);

        $this->assertCount(2, ACMHelper::getGroupsByIds([$groupId1, $groupId2]));

        $this->assertArrayHasKey($groupId1, ACMHelper::getGroupsList());
        $this->assertArrayHasKey($groupId2, ACMHelper::getGroupsList(true));

        $this->assertContains($groupId2, ACMHelper::getChildrenGroupsIdsTo($groupId1));
        $this->assertContains($groupId1, ACMHelper::includeChildrenGroupsIds($groupId1));

        $postId = get_posts()[0]->ID;

        $this->assertNotEmpty($postId);
        $this->assertTrue(ACMHelper::connectGroupToPost($groupId1, $postId));
        $this->assertContains($postId, ACMHelper::getProtectedPostIds($groupId1));
        $this->assertContains($groupId1, ACMHelper::getGroupIdsByPostId($postId));

        $termId = get_terms()[0]->term_id;

        $this->assertNotEmpty($termId);
        $this->assertTrue(ACMHelper::connectGroupToTerm($groupId1, $termId));
        $this->assertNotEmpty(ACMHelper::getProtectedTerms($groupId1));
        $this->assertContains($termId, ACMHelper::getProtectedTermsIds($groupId1));
        $this->assertCount(count(ACMHelper::getProtectedTerms($groupId1)), ACMHelper::getProtectedTermsIds($groupId1));
        $this->assertContains($groupId1, ACMHelper::getGroupIdsByTermId($termId));

        ACMHelper::connectGroupToPost($groupId2, $postId);
        ACMHelper::disconnectGroupFromPost($groupId2, $postId);

        $this->assertEmpty(ACMHelper::getPostsConnectedToGroup($groupId2));

        ACMHelper::connectGroupToTerm($groupId2, $termId);
        ACMHelper::disconnectGroupFromTerm($groupId2, $termId);

        $this->assertEmpty(ACMHelper::getTermsConnectedToGroup($groupId2));

        ACMHelper::updatePostConnections($postId, [$groupId1, $groupId2]);

        $this->assertCount(2, ACMHelper::getGroupsByPostId($postId));

        ACMHelper::updateTermConnections($termId, [$groupId1, $groupId2]);
        $this->assertCount(2, ACMHelper::getGroupsByTermId($termId));

        $consumerId = consumersAdminLoyaltySuite::getConsumerIdToTestEmails();

        $this->assertNotEmpty($consumerId);
        $this->assertNotEmpty(ACMHelper::connectConsumerToGroup($consumerId, $groupId1));

        $this->assertTrue(ACMHelper::hasConsumerConnectionToGroup($consumerId, $groupId1));
        $this->assertTrue(ACMHelper::hasConsumerConnectionToGroup($consumerId, $groupId1, true));
        $this->assertFalse(ACMHelper::hasConsumerConnectionToGroup($consumerId, $groupId2));

        $this->assertNotEmpty(ACMHelper::getConsumerGroupsConnections($consumerId));
        $this->assertContains($groupId1, ACMHelper::getConsumerGroupsConnectionsIds($consumerId));
        $this->assertContains($groupId1, ACMHelper::getConsumerGroupsIds($consumerId, true, false));
        $this->assertNotContains($groupId2, ACMHelper::getConsumerGroupsIds($consumerId, true, true));

        $this->assertContains($consumerId, ACMHelper::getConsumersConnectedToGroup($groupId1));

        $this->assertEquals(1, ACMHelper::countGroupUsages($groupId1));
        $this->assertEquals(0, ACMHelper::countGroupUsages($groupId2));

        $this->assertTrue(ACMHelper::disconnectConsumerFromGroup($consumerId, $groupId1));
        $this->assertNotContains($groupId1, ACMHelper::getConsumerGroupsIds($consumerId));

        $this->assertTrue(ACMHelper::deleteGroup($groupId1));
        $this->assertTrue(ACMHelper::deleteGroup($groupId2));

        $this->assertEmpty(ACMHelper::getGroup($groupId1));
        $this->assertEmpty(ACMHelper::getGroup($groupId2));

        $this->assertEmpty(ACMHelper::getPostsConnectedToGroup($groupId1));
        $this->assertEmpty(ACMHelper::getPostsConnectedToGroup($groupId2));

        $this->assertEmpty(ACMHelper::getTermsConnectedToGroup($groupId1));
        $this->assertEmpty(ACMHelper::getTermsConnectedToGroup($groupId2));
    }
}