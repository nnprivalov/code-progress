<?php
require_once __DIR__ . '/acmLoyaltySuiteTestCase.php';

class acmLoyaltySuiteTestHelper extends acmLoyaltySuiteTestCase
{
    public function test__createGroup()
    {
        $group = $this->createGroup();
        $groupSame = ACMHelper::getGroup($group->getGroupId());

        $this->assertSame($group->getGroupId(), $groupSame->getGroupId());
        $this->assertInstanceOf(ACMGroup::class, $groupSame);
        $this->assertNotEmpty($groupSame->getCreateDate());
    }

    public function test__updateGroup()
    {
        $group = new ACMGroup([
            'name' => md5(random_int(0, 1000) . microtime())
        ]);

        $this->assertFalse(
            ACMHelper::updateGroup($group)
        );

        ACMHelper::createGroup($group);

        $this->assertNotEmpty($group->getGroupId());

        $this->assertTrue(
            ACMHelper::updateGroup($group)
        );

        $code = $this->createAccessCode();
        $this->connectGroupToAccessCode($group, $code);

        $this->assertTrue($code->isActive());
        $this->assertTrue($group->isActive());

        $group->markAsInactive();

        ACMHelper::updateGroup($group);

        $code = ACMHelper::getAccessCode(
            $code->getCodeId()
        );

        $this->assertFalse($code->isActive());
    }

    public function test__deleteGroup()
    {
        $group = $this->createGroup();

        $childGroup = $this->createGroup();
        $childGroup->setParentGroupId(
            $group->getGroupId()
        );

        $consumer = $this->createConsumer();
        $this->connectConsumerToGroup($consumer, $group);

        $post = $this->createPost();
        $this->protectPost($post, $group);

        $code = $this->createAccessCode();
        $this->connectGroupToAccessCode($group, $code);

        $this->assertTrue(
            ACMHelper::updateGroup($childGroup)
        );

        $this->assertNotEmpty(
            ACMHelper::getGroup(
                $childGroup->getGroupId()
            )
        );

        $this->assertTrue(
            $this->hasConsumerGroup($consumer, $group)
        );

        $this->assertNotEmpty(
            ACMHelper::getProtectedPostsInGroup(
                $group->getGroupId()
            )
        );

        $this->assertNotEmpty(
            ACMHelper::getAccessCodesByGroupId($group->getGroupId())
        );

        $this->assertTrue(
            ACMHelper::deleteGroup($group->getGroupId())
        );

        $this->assertEmpty(
            ACMHelper::getGroup($childGroup->getGroupId())
        );

        $this->assertFalse(
            $this->hasConsumerGroup($consumer, $group)
        );

        $this->assertEmpty(
            ACMHelper::getProtectedPostsInGroup($group->getGroupId())
        );

        $this->assertEmpty(
            ACMHelper::getAccessCodesByGroupId($group->getGroupId())
        );
    }

    public function test__paginateGroups()
    {
        $createdGroups = [];

        for ($i=0; $i<30; $i++) {
            $createdGroups[] = $this->createGroup();
        }

        /** @var ACMGroup $group */
        $group = end($createdGroups);

        $result = ACMHelper::paginateGroups(0);

        $this->assertGreaterThanOrEqual(30, $result['rows']);
        $this->assertGreaterThanOrEqual(30, $result['totalCount']);
        $this->assertGreaterThanOrEqual(1, $result['totalPages']);

        $result = ACMHelper::paginateGroups(10, 2);

        $this->assertGreaterThanOrEqual(10, $result['rows']);
        $this->assertGreaterThanOrEqual(30, $result['totalCount']);
        $this->assertGreaterThanOrEqual(3, $result['totalPages']);
        $this->assertGreaterThanOrEqual(10, $result['itemsPerPage']);

        $result = ACMHelper::paginateGroups(0, 0, '', '', $group->getName());

        /** @var ACMGroup $result */
        $result = $result['rows'][0];

        $this->assertSame($group->getGroupId(), $result->getGroupId());

        $result = ACMHelper::paginateGroups(0, 0, '', '', '', [
            [
                'r' => 'groupId',
                'c' => 'equals',
                't' => $group->getGroupId()
            ],
            [
                'r' => 'name',
                'c' => 'equals',
                't' => $group->getName()
            ],
            [
                'r' => 'createDate',
                'c' => 'equals',
                't' => $group->getCreateDate()
            ],
            [
                'r' => 'active',
                'c' => 'checked'
            ]
        ]);

        /** @var ACMGroup $result */
        $result = $result['rows'][0];

        $this->assertSame($group->getGroupId(), $result->getGroupId());

        $childGroup = $this->createGroup([
            'parentGroupId' => $group->getGroupId()
        ]);

        $result = ACMHelper::paginateGroups(0, 0, '', '', '', [
            [
                'r' => 'groupId',
                'c' => 'in',
                't' => [$group->getGroupId(), $childGroup->getGroupId()]
            ]
        ]);

        $this->assertCount(2, $result['rows']);

        /** @var ACMHierarchicalGroup $row */
        foreach ($result['rows'] as $row) {

            $this->assertInstanceOf(ACMHierarchicalGroup::class, $row);

            if ($row->getGroupId() === $group->getGroupId()) {
                $this->assertSame(0, $row->getLevel());
            } else {
                $this->assertSame(1, $row->getLevel());
            }
        }
    }

    public function test__getGroups()
    {
        $group = $this->createGroup();
        $result = ACMHelper::getGroups('', '', $group->getName());

        $this->assertCount(1, $result);

        /** @var ACMGroup $result */
        $result = end($result);

        $this->assertSame($group->getGroupId(), $result->getGroupId());
    }

    // @TODO Next tests
}