<?php
/**
 * Created by PhpStorm.
 * User: made
 * Date: 2018-07-18
 * Time: 09:34
 */

define('PHP_UNIT_TEST', true);
define('WP_ADMIN', true);

require_once __DIR__ . '/../../../../../wp-load.php';

use PHPUnit\Framework\TestCase;

class acmLoyaltySuiteTestGroups extends TestCase
{
    /** @var acmLoyaltySuite */
    protected $module;

    public function setUp(): void
    {
        $this->module = LS()->getModule('acm');

        global $wpdb;
        $wpdb->query("BEGIN");
    }

    public function tearDown(): void
    {
        global $wpdb;
        $wpdb->query("ROLLBACK");
    }

    /**
     * @return false|\LS\Consumer
     */
    private function createConsumer()
    {
        $consumer = new \LS\Consumer([
            'active'   => true,
            'verified' => true
        ]);

        if (!\LS\ConsumersHelper::createConsumer($consumer)) {
            $consumer = false;
        }

        return $consumer;
    }

    public function testContentAccess()
    {
        $data = [
            'name'          => 'Test 1',
            'parentGroupId' => 0,
            'description'   => 'desc'
        ];

        $groupId1 = ACMHelper::createGroup(new ACMGroup($data));

        $this->assertNotEmpty($groupId1);

        $data = [
            'name'   => 'Test',
            'active' => false
        ];

        $groupId2 = ACMHelper::createGroup(new ACMGroup($data));
        $this->assertNotEmpty($groupId2);

        $group1 = ACMHelper::getGroup($groupId1);

        $this->assertInstanceOf(ACMGroup::class, $group1);
        $this->assertSame($group1->getName(), 'Test 1');
        $this->assertEmpty($group1->getParentGroupId());
        $this->assertSame($group1->getDescription(), 'desc');
        $this->assertTrue($group1->isActive());

        $group2 = ACMHelper::getGroup($groupId2);

        $this->assertInstanceOf(ACMGroup::class, $group2);
        $this->assertSame($group2->getName(), 'Test');
        $this->assertFalse($group2->isActive());
        $this->assertEmpty($group2->getParentGroupId());

        $group2
            ->setDescription('desc 2')
            ->setCreateDate(1000)
            ->setName('Test 2')
            ->setParentGroupId($groupId1)
            ->markAsActive();

        $this->assertTrue(ACMHelper::updateGroup($group2));

        $group2 = ACMHelper::getGroup($groupId2);
        $this->assertSame($group2->getName(), 'Test 2');
        $this->assertSame($group2->getCreateDate(), 1000);
        $this->assertSame($group2->getParentGroupId(), $groupId1);
        $this->assertSame($group2->getDescription(), 'desc 2');
        $this->assertTrue($group2->isActive());

        $groupsIds = array_map(static function (ACMGroup $g) {
            return $g->getGroupId();
        }, ACMHelper::getGroups());

        $this->assertContains($groupId1, $groupsIds);
        $this->assertContains($groupId2, $groupsIds);

        $this->assertCount(2, ACMHelper::getGroupsByIds([$groupId1, $groupId2]));

        $this->assertArrayHasKey($groupId1, ACMHelper::getGroupsList());
        $this->assertArrayHasKey($groupId2, ACMHelper::getGroupsList(true));

        $this->assertContains($groupId2, ACMHelper::getChildrenGroupsIdsTo($groupId1));
        $this->assertContains($groupId1, ACMHelper::includeChildrenGroupsIds($groupId1));

        $postId = get_posts()[0]->ID;

        $this->assertNotEmpty($postId);
        $this->assertTrue(ACMHelper::connectGroupToPost($groupId1, $postId));
        $this->assertContains($postId, ACMHelper::getProtectedPostIds($groupId1));
        $this->assertContains($groupId1, ACMHelper::getGroupIdsByPostId($postId));
        $this->assertArrayHasKey($postId, ACMHelper::getProtectedPostItems());
        $this->assertArrayHasKey($postId, ACMHelper::getProtectedPostItems($groupId1));
        $this->assertArrayHasKey($postId, ACMHelper::getProtectedPostItems((string) $groupId1));
        $this->assertArrayHasKey($postId, ACMHelper::getProtectedPostItems([$groupId1]));

        ACMHelper::connectGroupToPost($groupId2, $postId);
        ACMHelper::disconnectGroupFromPost($groupId2, $postId);

        $this->assertNotContains($postId, ACMHelper::getProtectedPostsInGroup($groupId2));

        ACMHelper::updatePostConnections($postId, [$groupId1, $groupId2]);

        $this->assertCount(2, ACMHelper::getGroupsByPostId($postId));

        $consumer = $this->createConsumer();

        $this->assertNotEmpty($consumer->getConsumerId());
        $this->assertNotEmpty(ACMHelper::connectConsumerToGroup($consumer->getConsumerId(), $groupId1));

        $this->assertTrue(ACMHelper::hasConsumerConnectionToGroup($consumer->getConsumerId(), $groupId1));
        $this->assertTrue(ACMHelper::hasConsumerConnectionToGroup($consumer->getConsumerId(), $groupId1, true));
        $this->assertFalse(ACMHelper::hasConsumerConnectionToGroup($consumer->getConsumerId(), $groupId2));

        $this->assertNotEmpty(ACMHelper::getConsumerGroupsConnections($consumer->getConsumerId()));
        $this->assertContains($groupId1, ACMHelper::getConsumerGroupsConnectionsIds($consumer->getConsumerId()));
        $this->assertContains($groupId1, ACMHelper::getConsumerGroupsIds($consumer->getConsumerId(), true, false));
        $this->assertNotContains($groupId2, ACMHelper::getConsumerGroupsIds($consumer->getConsumerId(), true, true));

        $data = [
            'name'   => 'Test',
            'active' => false
        ];

        $groupId3 = ACMHelper::createGroup(new ACMGroup($data));
        $group1->setParentGroupId($groupId3);
        ACMHelper::updateGroup($group1);

        $this->assertContains($consumer->getConsumerId(), ACMHelper::getConsumersConnectedToGroup($groupId1));
        $this->assertContains($consumer->getConsumerId(), ACMHelper::getConsumersConnectedToGroup($groupId3));
        $this->assertNotContains($consumer->getConsumerId(), ACMHelper::getConsumersConnectedToGroup($groupId3, false));

        $this->assertSame(1, ACMHelper::countGroupUsages($groupId1));
        $this->assertSame(0, ACMHelper::countGroupUsages($groupId2));
        $this->assertSame(1, ACMHelper::countGroupUsages($groupId3));
        $this->assertSame(0, ACMHelper::countGroupUsages($groupId3, false));

        $this->assertTrue(ACMHelper::disconnectConsumerFromGroup($consumer->getConsumerId(), $groupId1));
        $this->assertNotContains($groupId1, ACMHelper::getConsumerGroupsIds($consumer->getConsumerId()));

        $this->assertTrue(ACMHelper::deleteGroup($groupId1));

        $this->assertFalse(ACMHelper::getGroup($groupId1));
        $this->assertFalse(ACMHelper::getGroup($groupId2));

        $this->assertEmpty(ACMHelper::getProtectedPostsInGroup($groupId1));
        $this->assertEmpty(ACMHelper::getProtectedPostsInGroup($groupId2));
    }
}