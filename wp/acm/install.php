<?php
function acmInstallLoyaltySuite()
{
    $wpdb = LS()->wpdb;

    $tb = $wpdb->get_var(sprintf("SHOW TABLES LIKE '{$wpdb->prefix}ls_acm_groups'"));
    if (empty($tb)) {
        $wpdb->query(sprintf("CREATE TABLE `{$wpdb->prefix}ls_acm_groups` (
                            `groupId` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
                            `parentGroupId` INT(10) UNSIGNED NOT NULL DEFAULT '0',
                            `name` VARCHAR(400) NOT NULL,
                            `description` TEXT NOT NULL,
                            `active` TINYINT(1) UNSIGNED NOT NULL DEFAULT '1',
                            `createDate` INT(10) UNSIGNED NOT NULL,
                            PRIMARY KEY (`groupId`),
	                        INDEX `parentGroupId` (`parentGroupId`)
                        ) %s ENGINE=InnoDB", LS\Library::getCharsetCollate()));
    }

    $tb = $wpdb->get_var(sprintf("SHOW TABLES LIKE '{$wpdb->prefix}ls_acm_codes'"));
    if (empty($tb)) {
        $wpdb->query(sprintf("CREATE TABLE `{$wpdb->prefix}ls_acm_codes` (
                            `codeId` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
                            `code` VARCHAR(150) NOT NULL,
                            `companyId` INT(10) UNSIGNED NOT NULL,
                            `limit` INT(9) UNSIGNED NOT NULL DEFAULT '1',
                            `lifetime` SMALLINT(5) UNSIGNED NOT NULL DEFAULT '0',
                            `assignCompany` TINYINT(1) UNSIGNED NOT NULL DEFAULT '1',
                            `note` VARCHAR(400) NOT NULL,
                            `createDate` INT(10) UNSIGNED NOT NULL,
                            `startDate` DATE NOT NULL,
                            `endDate` DATE NOT NULL,
                            `active` TINYINT(1) UNSIGNED NOT NULL DEFAULT '1',
                            `emailPattern` VARCHAR(200) NOT NULL,
                            `drcActive` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
                            `drcDays` SMALLINT(3) UNSIGNED NOT NULL DEFAULT '0',
                            PRIMARY KEY (`codeId`),
                            UNIQUE INDEX `codeUnique` (`code`),
                            INDEX `codeId` (`codeId`),
                            INDEX `groupId` (`groupId`),
                            INDEX `companyId` (`companyId`)
                        ) %s ENGINE=InnoDB", LS\Library::getCharsetCollate()));
    }

    $tb = $wpdb->get_var(sprintf("SHOW TABLES LIKE '{$wpdb->prefix}ls_acm_consumers_groups'"));
    if (empty($tb)) {
        $wpdb->query(sprintf("CREATE TABLE `{$wpdb->prefix}ls_acm_consumers_groups` (
                            `consumerId` INT(10) UNSIGNED NOT NULL,
                            `groupId` INT(10) UNSIGNED NOT NULL,
                            `groupEnterDate` INT(10) UNSIGNED NOT NULL,
                            PRIMARY KEY (`consumerId`, `groupId`)
                        ) %s ENGINE=InnoDB", LS\Library::getCharsetCollate()));
    }

    $tb = $wpdb->get_var(sprintf("SHOW TABLES LIKE '{$wpdb->prefix}ls_acm_consumers_codes'"));
    if (empty($tb)) {
        $wpdb->query(sprintf("CREATE TABLE `{$wpdb->prefix}ls_acm_consumers_codes` (
                            `ccId` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
                            `consumerId` INT(10) UNSIGNED NOT NULL,
                            `codeId` BIGINT(20) UNSIGNED NOT NULL,
                            `enterDate` INT(10) UNSIGNED NOT NULL,
                            `endDate` DATE NOT NULL,
                            PRIMARY KEY (`ccId`),
                            INDEX `consumerId` (`consumerId`),
	                        INDEX `codeId` (`codeId`)
                        ) %s ENGINE=InnoDB", LS\Library::getCharsetCollate()));
    }

    $tb = $wpdb->get_var(sprintf("SHOW TABLES LIKE '{$wpdb->prefix}ls_acm_code_groups'"));
    if (empty($tb)) {
        $wpdb->query(sprintf("CREATE TABLE `{$wpdb->prefix}ls_acm_code_groups` (
                            `codeId` INT(11) UNSIGNED NOT NULL,
                            `groupId` INT(10) UNSIGNED NOT NULL,
                            PRIMARY KEY (`codeId`, `groupId`)
                        ) %s ENGINE=InnoDB", LS\Library::getCharsetCollate()));
    }
}