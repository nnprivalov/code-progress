<?php
class ACMConsumerGroupConnection extends LS\Model
{
    /** @var int */
    protected $consumerId;

    /** @var int */
    protected $groupId;

    /** @var int */
    protected $groupEnterDate;

    /** @var string[] */
    protected $primaryKeys = [
        'consumerId',
        'groupId'
    ];

    /** @var string[] */
    protected $dbCols = [
        'consumerId',
        'groupId',
        'groupEnterDate'
    ];

    /**
     * Format the data
     *
     * @since 3.0.0
     *
     * @param array $data Data to be formatted
     * @return array Formatted data
     */
    protected function format($data)
    {
        foreach ($data as $key => $value) {
            if ($key == 'groupEnterDate') {
                $data[$key] = max(0, !empty($value) && !is_numeric($value) ? strtotime($value) : (int) $value);
            } else {
                $data[$key] = (int) $value;
            }
        }

        return $data;
    }

    /**
     * Set default data
     * @since 3.0.0
     * @return self
     */
    public function setDefault()
    {
        $this->consumerId =
        $this->groupId =
        $this->groupEnterDate = 0;

        return $this;
    }

    /**
     * Check if the record is valid
     * @since 3.0.0
     * @return bool True if the record is valid
     */
    public function valid()
    {
        return $this->getConsumerId() > 0 && $this->getGroupId() > 0;
    }

    /**
     * Retrieve consumer Id
     * @since 3.0.0
     * @return int Consumer Id
     */
    public function getConsumerId()
    {
        return $this->consumerId;
    }

    /**
     * Retrieve group Id
     * @since 3.0.0
     * @return int Group Id
     */
    public function getGroupId()
    {
        return $this->groupId;
    }

    /**
     * Set group Id
     *
     * @since 3.0.0
     *
     * @param int $groupId Group Id
     * @return self
     */
    public function setGroupId($groupId)
    {
        $this->groupId = $groupId;

        return $this;
    }

    /**
     * Retrieve enter date
     * @since 3.0.0
     * @return int Timestamp
     */
    public function getGroupEnterDate()
    {
        return $this->groupEnterDate;
    }
}