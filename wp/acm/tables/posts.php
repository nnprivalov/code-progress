<?php
class acmPostsTable extends LS\WP_List_Table
{
    /** @var array */
    protected $_column_headers;

    /** @var int */
    private $groupId;

    /** @var WP_Post[] */
    private $posts;

    /**
     * @param int $groupId
     * @param WP_Post[] $posts
     */
    public function __construct($groupId, $posts)
    {
        parent::__construct([
            'singular' => 'wp_ls_acm_post',
            'plural'   => 'wp_ls_acm_posts'
        ]);

        $this->groupId = $groupId;
        $this->posts = $posts;
    }

    /**
     * @return array
     */
    public function get_columns()
    {
        return [
            'postId' => 'ID',
            'title'  => __('Title', 'ls'),
            'remove' => ''
        ];
    }

    public function prepare_items()
    {
        $list = [];

        foreach ($this->posts as $post) {
            $list[] = (object) [
                'postId' => $post->ID,
                'title'  => $post->post_title
            ];
        }

        $this->items = $list;
        $this->_column_headers = [$this->get_columns(), [], $this->get_sortable_columns()];
    }

    /**
     * @param $which
     */
    public function display_tablenav($which)
    {
        echo $which == 'top' ? '' : '<br />';
    }

    /**
     * @param object $item
     * @param string $columnName
     * @return string
     */
    public function column_default($item, $columnName)
    {
        switch ($columnName) {
            case 'postId' :
                return (int) $item->postId;
            case 'title' :
                $link = get_edit_post_link((int) $item->postId);
                return empty($item->title) || empty($link) ? ' ' : '<a href="' . esc_url($link) . '" target="_blank">' . esc_html($item->title) . '</a>';
            case 'remove' :
                return '<a href="' . add_query_arg(['action' => 'acm-exclude-post', 'post_id' => $item->postId, 'group_id' => $this->groupId]) . '" class="button">' . __('Remove', 'ls') . '</a>';
            default:
                return ' ';
        }
    }
}