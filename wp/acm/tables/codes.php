<?php
class acmCodesTable extends LS\WP_List_Table
{
    /** @var array */
    protected $_column_headers;

    /** @var string */
    public $referer;

    /** @var ACMGroup[] */
    protected $groups = [];

    /** @var array[int]string */
    private $companiesList;

    public function __construct()
    {
        parent::__construct([
            'singular' => 'wp_ls_acm_code',
            'plural'   => 'wp_ls_acm_codes'
        ]);

        $this->companiesList = LSCompaniesHelper::getCompaniesList(false, false);
    }

    /**
     * @return array
     */
    public function get_columns()
    {
        $cols = [
            'cb'         => '<input type="checkbox" />',
            'codeId'     => 'ID',
            'code'       => __('Access Code', 'ls'),
            'companyId'  => __('Company', 'ls'),
            'groupId'    => __('CA-Group', 'ls'),
            'note'       => __('Note', 'ls'),
            'limit'      => __('Limit', 'ls'),
            'used'       => __('Used', 'ls'),
            'startDate'  => __('Start date', 'ls'),
            'endDate'    => __('End date', 'ls'),
            'createDate' => __('Created', 'ls'),
            'active'     => __('Activated', 'ls')
        ];

        if (current_user_can('ls_acm_manage')) {
            $cols['details'] = '';
        }

        return $cols;
    }

    /**
     * @return array
     */
    public function get_sortable_columns()
    {
        return [
            'codeId'     => ['codeId', false],
            'companyId'  => ['companyId', false],
            'code'       => ['code', false],
            'groupId'    => ['groupId', false],
            'note'       => ['note', false],
            'limit'      => ['limit', true],
            'used'       => ['used', false],
            'active'     => ['active', true],
            'createDate' => ['createDate', true],
            'startDate'  => ['startDate', true],
            'endDate'    => ['endDate', true]
        ];
    }

    public function filter_box()
    {
        $this->__filter_box([
            ['codeId', 'value'],
            ['companyId', 'select', LSCompaniesHelper::getCompaniesList(true, true)],
            ['code', 'string'],
            ['groupId', 'select', ACMHelper::getGroupsList(true)],
            ['note', 'string'],
            ['limit', 'value'],
            ['used', 'boolean'],
            ['active', 'boolean'],
            ['createDate', 'date'],
            ['startDate', 'date'],
            ['endDate', 'date']
        ], 3);
    }

    public function prepare_items()
    {
        $options = $this->get_table_options();
        $items = ACMHelper::paginateAccessCodes(20, $options['page'], $options['orderBy'], $options['order'], $options['searchTerm'], $options['filterTerms']);

        $this->set_pagination_args([
            "total_items" => $items['totalCount'],
            "total_pages" => $items['totalPages'],
            "per_page"    => $items['itemsPerPage']
        ]);

        $this->items = $items['rows'];
        $this->_column_headers = [$this->get_columns(), [], $this->get_sortable_columns()];
    }

    /**
     * @return array
     */
    public function get_bulk_actions()
    {
        if (current_user_can('ls_acm_manage')) {
            $actions = [
                'activate-acm-codes'   => __('Activate', 'ls'),
                'deactivate-acm-codes' => __('Deactivate', 'ls'),
                'delete-acm-codes'     => __('Delete', 'ls')
            ];
        } else {
            $actions = [];
        }

        return $actions;
    }

    /**
     * @param ACMCode $code
     * @return string
     */
    public function column_cb($code)
    {
        return sprintf('<input type="checkbox" name="codeId[]" value="%d" />', $code->getCodeId());
    }

    /**
     * @param ACMCode $code
     * @param string $columnName
     * @return string
     */
    public function column_default($code, $columnName)
    {
        switch ($columnName) {
            case 'codeId' :
                return $code->getCodeId();
            case 'companyId' :
                if ($code->getCompanyId() > 0 && isset($this->companiesList[$code->getCompanyId()])) {
                    return '<a href="' . companiesAdminLoyaltySuite::companyEditLink($code->getCompanyId(), $this->referer) . '">' . $this->companiesList[$code->getCompanyId()] . '</a>';
                }

                return '—';
            case 'code' :
                $styles = [];
                if (!$code->isActive()) {
                    $styles[] = 'color: #FB300B;';
                } elseif ($code->getUsages() > 0) {
                    $styles[] = 'color: #a7c711;';
                }

                return '<a href="' . acmAdminLoyaltySuite::codeEditLink($code->getCodeId(), $this->referer) . '" ' . (empty($styles) ? '' : ' style="' . implode('', $styles) . '"') . '>' . esc_html($code->getCode()) . '</a>';
            case 'groupId' :
                $groups = [];
                foreach (ACMHelper::getAccessCodeGroupIds($code->getCodeId()) as $groupId) {

                    if (!isset($this->groups[$groupId])) {
                        $this->groups[$groupId] = ACMHelper::getGroup($groupId);
                    }

                    $group = $this->groups[$groupId];
                    if ($group instanceof ACMGroup) {
                        $group = current_user_can('ls_acm_manage') ? '<a href="' . acmAdminLoyaltySuite::contentAccessEditLink($group->getGroupId(), $this->referer) . '"><span' . ($group->isActive() ? '' : ' style="color: #FB300B"') . '>' . $group->getName() . '<span></a>' : $group->getName();
                    } else {
                        $group = $groupId;
                    }

                    $groups[] = $group;
                }

                return empty($groups) ? '—' : implode('<br />', $groups);
            case 'note' :
                return '<a href="#" class="acm-code-desc" data-id="' . $code->getCodeId() . '">' . ($code->getNote() == '' ? ' ' : esc_html($code->getNote())) . '</a>';
            case 'limit' :
                return $code->hasLimit() ? ($code->isLimitReached() ? '<span style="color: red; font-weight: 700;">' . $code->getLimit() . '</span>' : $code->getLimit()) : '&#8734;';
            case 'used' :
                return '<a href="' . admin_url('admin.php?page=consumersLoyaltySuite&table=consumersOverviewTableLoyaltySuite&f[0][r]=acmCode&f[0][c]=equals&f[0][t]=' . urlencode($code->getCode()) . '&referer=' . urlencode($this->referer)) . '">' . $code->getUsages() . '</a>';
            case 'active' :
                return $code->isActive() ? '<span class="ls-sign-plus">+</span>' : '<span class="ls-sign-cross">-</span>';
            case 'createDate' :
                return date('d.m.Y', $code->getCreateDate() + TIMEZONE_DIFF);
            case 'startDate' :
                $value = $code->getStartDate() > 0 ? date('d.m.Y', $code->getStartDate()) : 0;

                return empty($value) ? ' ' : ($code->isUpcoming() ? '<span style="color:red">' . $value . '</span>' : $value);
            case 'endDate' :
                $value = $code->getEndDate() > 0 ? date('d.m.Y', $code->getEndDate()) : 0;

                return empty($value) ? ' ' : ($code->isExpired() ? '<span style="color:red">' . $value . '</span>' : $value);
            case 'details' :
                return '<a href="' . acmAdminLoyaltySuite::codeEditLink($code->getCodeId(), $this->referer) . '" class="button">' . __('Details', 'ls') . '</a>';
            default:
                return ' ';
        }
    }
}