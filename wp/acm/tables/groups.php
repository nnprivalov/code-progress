<?php
class acmGroupsTable extends LS\WP_List_Table
{
    /** @var array */
    protected $_column_headers;

    /** @var string */
    public $referer;

    public function __construct()
    {
        parent::__construct([
            'singular' => 'wp_ls_acm_group',
            'plural'   => 'wp_ls_acm_groups',
            'hash'     => '#ls-groups'
        ]);
    }

    /**
     * @return array
     */
    public function get_columns()
    {
        $cols = [
            'cb'        => '<input type="checkbox" />',
            'groupId'   => 'ID',
            'name'      => __('Name', 'ls'),
            'codes'     => __('Access Codes', 'ls'),
            'consumers' => __('Consumers', 'ls'),
            'active'    => __('Active', 'ls')
        ];

        if (current_user_can('ls_acm_manage')) {
            $cols['details'] = '';
        }

        return $cols;
    }

    /**
     * @return array
     */
    public function get_sortable_columns()
    {
        return [
            'groupId'   => ['groupId', false],
            'name'      => ['name', false],
            'consumers' => ['consumers', true],
            'active'    => ['active', true]
        ];
    }

    public function filter_box()
    {
        $this->__filter_box([
            ['groupId', 'value'],
            ['name', 'string'],
            ['active', 'boolean']
        ], 3);
    }

    public function prepare_items()
    {
        $options = $this->get_table_options();
        $items = ACMHelper::paginateGroups(20, $options['page'], $options['orderBy'], $options['order'], $options['searchTerm'], $options['filterTerms']);

        $this->set_pagination_args([
            "total_items" => $items['totalCount'],
            "total_pages" => $items['totalPages'],
            "per_page"    => $items['itemsPerPage']
        ]);

        $this->items = $items['rows'];
        $this->_column_headers = [$this->get_columns(), [], $this->get_sortable_columns()];
    }

    /**
     * @return array
     */
    public function get_bulk_actions()
    {
        if (current_user_can('ls_acm_manage')) {
            $actions = [
                'duplicate-acm-groups' => __('Duplicate', 'ls'),
                'delete-acm-groups'    => __('Delete', 'ls')
            ];
        } else {
            $actions = [];
        }

        return $actions;
    }

    /**
     * @param ACMHierarchicalGroup $hGroup
     * @return string
     */
    public function column_cb($hGroup)
    {
        return sprintf('<input type="checkbox" name="groupId[]" value="%d" />', $hGroup->getGroupId());
    }

    /**
     * @param ACMHierarchicalGroup $hGroup
     * @param string $columnName
     * @return string
     */
    public function column_default($hGroup, $columnName)
    {
        switch ($columnName) {
            case 'groupId' :
                return $hGroup->getGroupId();
            case 'name' :
                $editLink = acmAdminLoyaltySuite::contentAccessEditLink($hGroup->getGroupId(), $this->referer);
                $text = str_repeat('&#8212; ', $hGroup->getLevel()) . $hGroup->getName();
                $text = current_user_can('ls_acm_manage') ? '<strong><a href="' . $editLink . '" ' . ($hGroup->isActive() ? '' : 'style="color:red"') . '>' . $text . '</a></strong>' : $text;
                $desc = $hGroup->getDescription();
                if (!empty($desc)) {
                    $text .= '<br />' . str_repeat('&nbsp;&nbsp;&nbsp;&nbsp;', $hGroup->getLevel()) . $desc;
                }

                return $text;
            case 'codes' :
                $codes = ACMHelper::countCodesInGroup($hGroup->getGroupId());
                return '<a href="' . admin_url('admin.php?page=acmLoyaltySuite&table=acmCodesTable&f[0][r]=groupId&f[0][c]=equals&f[0][s_groupId]=' . $hGroup->getGroupId() . '#ls-codes') . '">' . $codes . '</a>';
            case 'consumers' :
                $consumers = ACMHelper::countGroupUsages($hGroup->getGroupId());
                return '<a href="' . admin_url('admin.php?page=admin.php&page=consumersLoyaltySuite&table=consumersOverviewTableLoyaltySuite&f[0][r]=acmGroupId&f[0][c]=equals&f[0][s_acmGroupId]=' . $hGroup->getGroupId() . '#ls-codes') . '">' . $consumers . '</a>';
            case 'description' :
                return $hGroup->getDescription();
            case 'active' :
                return $hGroup->isActive() ? '<span class="ls-sign-plus">+</span>' : '<span class="ls-sign-cross">-</span>';
            case 'details' :
                return '<a href="' . acmAdminLoyaltySuite::contentAccessEditLink($hGroup->getGroupId(), $this->referer) . '" class="button">' . __('Details', 'ls') . '</a>';
            default:
                return ' ';
        }
    }
}