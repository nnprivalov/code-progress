<?php
class ACMHierarchicalGroup extends ACMGroup
{
    /** @var int */
    protected $level;

    /**
     * @since 3.0.0
     * @param null|array|object $thing
     * @param int $level
     */
    public function __construct($thing, $level)
    {
        parent::__construct($thing);

        $this->level = (int) $level;
    }

    /**
     * Retrieve group level in the hierarchy
     * @since 3.0.0
     * @return int Group level
     */
    public function getLevel()
    {
        return $this->level;
    }
}