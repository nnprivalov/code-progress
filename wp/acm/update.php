<?php
class acmLoyaltySuiteUpdate implements \LS\UpdateInterface
{
    /**
     * @param \LS\Module $module
     * @param string $oldVersion
     * @param string $newVersion
     */
    public function update(\LS\Module $module, string $oldVersion, string $newVersion)
    {
        $wpdb = LS()->wpdb;

        // 2.1.0
        if (version_compare($oldVersion, '2.1.0', '<')) {

            $wpdb->query(sprintf("ALTER TABLE `{$wpdb->prefix}ls_acm_consumers`
                ADD COLUMN `groupId` INT(10) UNSIGNED NOT null AFTER `codeId`,
                ADD INDEX `groupId` (`groupId`)"));

            $wpdb->query(sprintf("UPDATE {$wpdb->prefix}ls_acm_consumers as c SET c.groupId = (
                SELECT groupId FROM {$wpdb->prefix}ls_acm_codes WHERE codeId = c.codeId
                )"));
        }

        // 2.2.0
        if (version_compare($oldVersion, '2.2.0', '<')) {
            $module->deleteOption('categoriesAccess');
        }

        // 2.3.0
        if (version_compare($oldVersion, '2.3.0', '<')) {
            $wpdb->query(sprintf("UPDATE {$wpdb->postmeta} SET meta_key = '_acm_group_id' WHERE meta_key = 'acm_group_id'"));
            $wpdb->query(sprintf("UPDATE {$wpdb->termmeta} SET meta_key = '_acm_group_id' WHERE meta_key = 'acm_group_id'"));
        }

        // 2.4.0
        if (version_compare($oldVersion, '2.4.0', '<')) {
            $wpdb->query(sprintf("ALTER TABLE `{$wpdb->prefix}ls_acm_groups`
                ADD COLUMN `parentGroupId` INT(10) UNSIGNED NOT NULL DEFAULT '0' AFTER `groupId`,
                ADD INDEX `parentGroupId` (`parentGroupId`)"));
        }

        // 2.4.9
        if (version_compare($oldVersion, '2.4.9', '<')) {
            get_role('author')->add_cap('ls_acm_manage');
            get_role('editor')->add_cap('ls_acm_manage');
            get_role('administrator')->add_cap('ls_acm_manage');
        }

        // 2.5.0
        if (version_compare($oldVersion, '2.5.0', '<')) {
            $wpdb->query(sprintf("ALTER TABLE `{$wpdb->prefix}ls_acm_groups` ADD COLUMN `assignCompany` TINYINT(1) UNSIGNED NOT NULL DEFAULT '1' AFTER `createDate`"));
            if ($module->getOption('autoAssignCompany', true)) {
                $wpdb->query(sprintf("UPDATE {$wpdb->prefix}ls_acm_groups SET assignCompany = 1 WHERE companyId > 0"));
            }
            $module->deleteOption('autoAssignCompany');
        }

        // 3.0.0
        if (version_compare($oldVersion, '3.0.0', '<')) {

            $wpdb->query(sprintf("ALTER TABLE `{$wpdb->prefix}ls_acm_codes`
                                  ADD COLUMN `startDate` DATE NOT NULL AFTER `createDate`,
                                  ADD COLUMN `endDate` DATE NOT NULL AFTER `startDate`,
                                  ADD COLUMN `emailPattern` VARCHAR(200) NOT NULL AFTER `active`,
                                  ADD COLUMN `drcActive` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' AFTER `emailPattern`,
                                  ADD COLUMN `drcDays` SMALLINT(3) UNSIGNED NOT NULL DEFAULT '0' AFTER `drcActive`"));

            $wpdb->query(sprintf("UPDATE {$wpdb->prefix}ls_acm_codes cd
                                  INNER JOIN {$wpdb->prefix}ls_acm_groups g ON g.groupId = cd.groupId
                                  SET cd.startDate = IF(cd.createDate > UNIX_TIMESTAMP(g.startDate), FROM_UNIXTIME(cd.createDate), g.startDate),
                                      cd.endDate = g.endDate,
                                      cd.emailPattern = g.emailPattern, 
                                      cd.drcActive = g.drcActive, 
                                      cd.drcDays = g.drcDays"));

            $wpdb->query(sprintf("ALTER TABLE `{$wpdb->prefix}ls_acm_consumers` ADD COLUMN `endDate` DATE NOT NULL AFTER `enterDate`"));

            $wpdb->query(sprintf("RENAME TABLE `{$wpdb->prefix}ls_acm_consumers` TO `{$wpdb->prefix}ls_acm_consumers_codes`"));

            $wpdb->query(sprintf("UPDATE {$wpdb->prefix}ls_acm_consumers_codes c
                                  INNER JOIN {$wpdb->prefix}ls_acm_codes cd ON cd.codeId = c.codeId
                                  SET c.endDate = IF(cd.lifetime > 0, LEAST(DATE_ADD(FROM_UNIXTIME(c.enterDate), INTERVAL + cd.lifetime DAY), cd.endDate), cd.endDate)"));

            $wpdb->query(sprintf("CREATE TABLE `{$wpdb->prefix}ls_acm_consumers_groups` (
                                    `consumerId` INT(10) UNSIGNED NOT NULL,
                                    `groupId` INT(10) UNSIGNED NOT NULL,
                                    `groupEnterDate` INT(10) UNSIGNED NOT NULL,
                                    PRIMARY KEY (`consumerId`, `groupId`)
                                ) %s ENGINE=InnoDB", LS\Library::getCharsetCollate()));

            $wpdb->query(sprintf("INSERT IGNORE INTO {$wpdb->prefix}ls_acm_consumers_groups SELECT consumerId, groupId, enterDate FROM {$wpdb->prefix}ls_acm_consumers_codes"));

            $wpdb->query(sprintf("ALTER TABLE `{$wpdb->prefix}ls_acm_codes` ADD COLUMN `companyId` INT(10) UNSIGNED NOT NULL AFTER `groupId`, ADD INDEX `companyId` (`companyId`)"));
            $wpdb->query(sprintf("ALTER TABLE `{$wpdb->prefix}ls_acm_codes` ADD COLUMN `assignCompany` TINYINT(1) UNSIGNED NOT NULL DEFAULT '1' AFTER `lifetime`"));

            $wpdb->query(sprintf("UPDATE {$wpdb->prefix}ls_acm_codes cd
                                  INNER JOIN {$wpdb->prefix}ls_acm_groups g ON g.groupId = cd.groupId
                                  SET cd.companyId = g.companyId, cd.assignCompany = g.assignCompany"));
        }

        // 3.0.7
        if (version_compare($oldVersion, '3.0.7', '<')) {
            $wpdb->query(sprintf("ALTER TABLE `{$wpdb->prefix}ls_acm_consumers_codes` DROP COLUMN `groupId`"));
            $wpdb->query(sprintf("ALTER TABLE `{$wpdb->prefix}ls_acm_groups` DROP COLUMN `companyId`, DROP COLUMN `startDate`, DROP COLUMN `endDate`, DROP COLUMN `drcActive`, DROP COLUMN `drcDays`, DROP COLUMN `emailPattern`, DROP COLUMN `assignCompany`"));
        }

        // 3.2.0
        if (version_compare($oldVersion, '3.2.0', '<')) {

            $wpdb->update($wpdb->prefix . 'ls_consumers_log', ['messageType' => 'company_assigned'], ['messageType' => 'companyAssigned']);

            $wpdb->query(sprintf("ALTER TABLE `{$wpdb->prefix}ls_acm_consumers_codes`
                ADD COLUMN `ccId` INT(10) UNSIGNED NOT null AUTO_INCREMENT FIRST,
                DROP PRIMARY KEY,
                ADD PRIMARY KEY(`ccId`),
                ADD INDEX `consumerId` (`consumerId`),
                ADD INDEX `codeId` (`codeId`)"));
        }

        // 3.3.0
        if (version_compare($oldVersion, '3.3.0', '<')) {
            $module->deleteOption('availablePostTypes');
            $module->deleteOption('availableTaxonomies');
        }

        // 3.3.1
        if (version_compare($oldVersion, '3.3.1', '<')) {
            $module->deleteOption('redirectGuestPageId');
            $module->deleteOption('redirectConsumerPageId');
        }

        // 3.5.0
        if (version_compare($oldVersion, '3.5.0', '<')) {
            $wpdb->query("ALTER TABLE `{$wpdb->prefix}ls_acm_consumers_codes` CHANGE COLUMN `codeId` `codeId` INT(11) UNSIGNED NOT NULL AFTER `consumerId`");
            $wpdb->query(sprintf("CREATE TABLE `{$wpdb->prefix}ls_acm_code_groups` (
                                    `codeId` INT(11) UNSIGNED NOT NULL,
                                    `groupId` INT(10) UNSIGNED NOT NULL,
                                    PRIMARY KEY (`codeId`, `groupId`)
                                ) %s ENGINE=InnoDB", LS\Library::getCharsetCollate()));
            $wpdb->query("INSERT INTO {$wpdb->prefix}ls_acm_code_groups (codeId, groupId) SELECT codeId, groupId FROM {$wpdb->prefix}ls_acm_codes WHERE groupId > 0");
        }

        // 3.5.10
        if (version_compare($oldVersion, '3.5.10', '<')) {
            $module->deleteOption('redirectGuest');
            $module->deleteOption('redirectGuestPage');
        }

        // 3.7.0
        if (version_compare($oldVersion, '3.7.0', '<')) {
            $wpdb->query("ALTER TABLE `{$wpdb->prefix}ls_acm_codes` DROP COLUMN `groupId`");
            $module->deleteOption('taxonomiesAccess');
        }
    }
}