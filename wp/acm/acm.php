<?php
/**
 * @module acmLoyaltySuite
 * @title Access Management
 * @description Module to handle access to the frontend pages for guests and consumers via Access Codes that provides by the companies
 * @version 3.7.0
 * @priority 6
 * @depends consumersLoyaltySuite 3.5.5, companiesLoyaltySuite 2.6.0
 */
class acmLoyaltySuite extends LS\Module
{
    /** @var bool */
    private $acLogicDisabled = false;

    /** @var int[] */
    private $protectedPostItems;

    /** @var int[] */
    private $consumerProtectedPostItems;

    /** @var ACMHelper */
    protected $helper;

    /** @var consumersLoyaltySuite */
    protected $consumersModule;

    /**
     * @since 2.4.11
     * @return string
     */
    public function getVersion()
    {
        return '3.7.0';
    }

    /**
     * @since 2.4.11
     * @return string
     */
    public function getCapability()
    {
        return 'ls_acm';
    }

    /**
     * @since 2.4.11
     * @return array
     */
    public function setDepends()
    {
        return [
            'consumersLoyaltySuite' => '3.5.5',
            'companiesLoyaltySuite' => '2.6.0'
        ];
    }

    /**
     * @since 2.0.0
     * @return string
     */
    public function getTitle()
    {
        return __('Access Management', 'ls');
    }

    /**
     * @since 2.0.0
     * @return string
     */
    public function getDescription()
    {
        return __('Module to handle access to the frontend pages for guests and consumers via Access Codes that provides by the companies.', 'ls');
    }

    /**
     * Retrieve module default options
     * @since 2.0.0
     * @return array An array of options and their values
     */
    public function getDefaultOptions()
    {
        return [
            'acRequired'               => true,
            'redirectConsumer'         => 'consumersLogin',
            'redirectConsumerPage'     => 0,
            'wrongCodeErrorMsg'        => 'You have entered wrong Access Code',
            'wrongCodePatternErrorMsg' => 'Entered email does not match to the portal requirements',
            'inactiveCodeErrorMsg'     => 'Your Access Code inactive. Please contact the portal administrator',
            'codeUsedErrorMsg'         => 'Entered Access Code has already been used',
            'codeNotStartedErrorMsg'   => 'Entered Access Code is not available for now',
            'codeIsEndedErrorMsg'      => 'Entered Access Code has out of date',
            'noAccessErrorMsg'         => 'Page not found or you do not have access to view this page',
            'privateAreaMessage'       => 'Dear [consumer-name], this area is protected.',
            'deactivationCheck'        => true
        ];
    }

    /**
     * Main function to load models, create actions and filters, init variables
     * @since 2.0.0
     */
    public function load()
    {
        require_once __DIR__ . '/ACMGroup.php';
        require_once __DIR__ . '/ACMCode.php';
        require_once __DIR__ . '/ACMConsumerGroupConnection.php';
        require_once __DIR__ . '/ACMConsumerCodeConnection.php';
        require_once __DIR__ . '/ACMHierarchicalGroup.php';
        require_once __DIR__ . '/ACMHelper.php';

        $this->helper = new ACMHelper();
        $this->consumersModule = LS()->getModule('consumers');

        // module hooks
        add_action('init', [$this, 'init']);

        // logic implementation
        add_action('wp', [$this, 'wp']);
        add_filter('pre_get_posts', [$this, 'preGetPosts'], 98);
        add_filter('wp_get_nav_menu_items', [$this, 'wpNavMenuObjects'], 100);
        add_filter('pre_get_comments', [$this, 'preGetComments'], 99);
        add_action('ls_acm_consumer_connected', [$this, 'assignCompanyToConsumer'], 10, 2);

        // clean caches
        add_action('ls_acm_group_connected_to_post', [$this, 'resetProtectedItemsCache']);
        add_action('ls_acm_group_disconnected_from_post', [$this, 'resetProtectedItemsCache']);
        add_action('ls_acm_consumer_group_connected', [$this, 'resetProtectedItemsCache']);
        add_action('ls_acm_consumer_group_disconnected', [$this, 'resetProtectedItemsCache']);

        // connect AC to consumers
        add_filter('consumerDynamicFields', [$this, 'consumerDynamicFields']);
        add_filter('ls_consumers_custom_filter_acmGroupId', [$this, 'consumerFilterGroupId']);
        add_filter('ls_consumers_custom_filter_acmCode', [$this, 'consumerFilterCode']);
        add_filter('ls_consumer_before_save', [$this, 'consumerSaveValidation'], 10, 3);
        add_action('ls_consumer_created', [$this, 'consumerCreated']);
        add_action('ls_consumer_before_deleted', [$this, 'consumerBeforeDeleted']);
        add_filter('ls_consumer_allow_login', [$this, 'checkConsumerLogin'], 10, 2);
        add_filter('ls_consumer_allow_access', [$this, 'checkAuthorizationAccess'], 90, 2);

        // Com.Center integration
        add_filter('ls_cc_init', [$this, 'comCenterInit']);
        add_action('ls_acm_consumer_connected', [$this, 'accessCodeConnected'], 10, 2);
        add_filter('ls_cc_filters', [$this, 'initRecipients']);

        // Relevanssi
        add_filter('relevanssi_where', [$this, 'relevanssiWhere']);
    }

    /**
     * Create shortcodes
     * @since 2.0.0
     */
    public function initShortcodes()
    {
        add_shortcode('access-code-form', [$this, 'accessCodeFormShortcode']);
        add_shortcode('consumer-access-codes', [$this, 'consumersAccessCodesShortcode']);
        add_shortcode('ac-private', [$this, 'acPrivateShortcode']);
        add_shortcode('ac-days-left', [$this, 'acDaysLeftShortcode']);
        add_shortcode('ac-end-date', [$this, 'acEndDateShortcode']);
    }

    /**
     * Retrieve ACM module helper
     * @since 3.5.12
     * @return ACMHelper
     */
    public function helper(): ACMHelper
    {
        return $this->helper;
    }

    /**
     * Check if access logic at the moment is disabled
     * @since 3.6.1
     * @return bool True if access logic disabled
     */
    public function isAccessLogicDisabled(): bool
    {
        return $this->acLogicDisabled;
    }

    /**
     * Retrieve excluded posts id's
     * @since 2.0.0
     * @return int[] Excluded posts Id's
     */
    protected function getExcludedPostsIds()
    {
        static $acmExcludedPosts;

        if ($acmExcludedPosts === null) {
            $this->acLogicDisabled = true;
            $acmExcludedPosts = apply_filters('ls_acm_excluded_posts', $this->consumersModule->getExcludedPosts());
            $this->acLogicDisabled = false;
        }

        return $acmExcludedPosts;
    }

    /**
     * Retrieve an array of all ac-protect post Id's
     *
     * @since 3.4.0
     * @internal
     * @TODO Change to private after 09.2019
     *
     * @param null|string $postType Post type or null string to determine a group of posts to retrieve
     * @return int[] An array of protected post Id's
     */
    public function getProtectedPostIds($postType = null)
    {
        // @deprecated 07.2019 Legacy
        if ($postType !== null) {
            _deprecated_argument(__FUNCTION__, '3.6.1', '$postType deprecated');
        }

        if ($this->protectedPostItems === null) {

            $items = $this->helper->getProtectedPostItems();

            if (!empty($items)) {
                $items = array_diff_key($items, array_flip($this->getExcludedPostsIds()));
            }

            $this->protectedPostItems = $items;
        }

        $ids = [];

        // @deprecated 07.2019 Legacy
        foreach ($this->protectedPostItems as $postId => $pt) {
            if ($postType === null || $postType === $pt) {
                $ids[] = $postId;
            }
        }

        return $ids;
    }

    /**
     * Reset a list of pre-saved protect post Id's
     * @since 3.5.3
     * @internal
     */
    public function resetProtectedItemsCache()
    {
        $this->protectedPostItems = null;
        $this->consumerProtectedPostItems = null;
    }

    /**
     * Determine if post is ac-protected
     *
     * @since 2.0.0
     *
     * @param int $postId Post Id
     * @return bool True if post is ac-protected
     */
    public function isPostProtected($postId)
    {
        return in_array($postId, $this->getProtectedPostIds());
    }

    /**
     * Retrieve an array of ac-protected posts Id's for the consumer
     *
     * @since 3.4.0
     * @internal
     * @TODO Change to private after 09.2019
     *
     * @param int $consumerId Consumer Id
     * @param null|string $postType Post type or null string to determine a group of posts to retrieve
     * @return int[] An array of posts Id's
     */
    public function getProtectedPostIdsToConsumer($consumerId, $postType = null)
    {
        // @deprecated 07.2019 Legacy
        if ($postType !== null) {
            _deprecated_argument(__FUNCTION__, '3.6.1', '$postType deprecated');
        }

        if ($consumerId < 1) {
            $ids = $this->getProtectedPostIds($postType);
        } else {

            $items = [];
            $isCurrentConsumer = $consumerId === $this->consumersModule->getAuthorizedConsumerId();

            if (!$isCurrentConsumer || $this->consumerProtectedPostItems === null) {

                $items = $this->helper->getProtectedPostItemsToConsumer($consumerId);

                if (!empty($items)) {
                    $items = array_diff_key($items, array_flip($this->getExcludedPostsIds()));
                }

                if ($isCurrentConsumer) {
                    $this->consumerProtectedPostItems = $items;
                }

            } elseif ($isCurrentConsumer) {
                $items = $this->consumerProtectedPostItems;
            }

            $ids = [];

            // @deprecated 07.2019 Legacy
            foreach ($items as $postId => $pt) {
                if ($postType === null || $postType === $pt) {
                    $ids[] = $postId;
                }
            }
        }

        return $ids;
    }

    /**
     * Check if post can be accessible due to valid secure link
     *
     * @since 3.4.0
     *
     * @param int $postId Post Id to check access
     * @return bool True if post can be accessible due to valid secure link
     */
    private function hasAccessBySecureLink($postId)
    {
        return
            isset($_REQUEST['token'])
            && !empty($_REQUEST['token'])
            && hash_equals($_REQUEST['token'], get_post_meta($postId, '_secureLink', true));
    }

    /**
     * Check if current guest/consumer has access to the post
     * Note: function can be applied to the guest
     *
     * @since 2.0.0
     *
     * @param int $postId Post Id
     * @param null|int $consumerId Consumer Id (optional)
     * @return bool True if current guest/consumer has access to the post
     */
    public function hasAccessToPost($postId, $consumerId = null)
    {
        if ($postId < 1) {
            return false;
        }

        if ($this->hasAccessBySecureLink($postId)) {
            return true;
        }

        if ($consumerId === null) {
            $consumerId = $this->consumersModule->getAuthorizedConsumerId();
        }

        if ($this->isPostProtected($postId)) {

            if ($consumerId < 1) {
                return false;
            }

            if (in_array($postId, $this->getProtectedPostIdsToConsumer($consumerId))) {
                return false;
            }
        }

        return apply_filters('ls_acm_has_access_to_post', true, $postId, $consumerId);
    }

    /**
     * Determine if term is ac-protected
     *
     * @since 2.0.0
     * @deprecated 08.2019 See TermAccess
     *
     * @param int $termId Term Id
     * @param string $taxonomy Taxonomy name
     * @return bool True if term is ac-protected
     */
    public function isTermProtected($termId, $taxonomy)
    {
        if (LS()->isModuleActive('TermAccess')) {
            /** @var \LS\TermAccess $ta */
            $ta = LS()->getModule('TermAccess');
            return $ta->isTermProtected($termId, $taxonomy);
        }

        // @TODO Uncomment after 10.2019
        //_deprecated_function(__FUNCTION__, '3.7.0', 'TermAccess::hasAccessToTerm');

        return false;
    }

    /**
     * Determine if guest/consumer has access to term/terms
     *
     * @since 2.0.0
     * @deprecated 08.2019 Legacy Use TermAccess->hasAccessToTerm()
     *
     * @param null|array|int $term Term Id or array with a list of term id's or null to get all related to the current page terms
     * @param string $taxonomy Taxonomy name
     * @param null|int $consumerId Consumer Id (optional)
     * @param bool $checkParents Check parent terms for the access. Default: false (no)
     * @return bool True if user has access to term/terms
     */
    public function hasAccessToTerm($term, $taxonomy, $consumerId = null, $checkParents = false)
    {
        if (LS()->isModuleActive('TermAccess')) {
            /** @var \LS\TermAccess $ta */
            $ta = LS()->getModule('TermAccess');
            return $ta->hasAccessToTerm($term, $taxonomy, $consumerId, $checkParents);
        }

        // @TODO Update usages after 09.2019
        // @TODO Uncomment after 10.2019
        //_deprecated_function(__FUNCTION__, '3.7.0', 'TermAccess::hasAccessToTerm');

        return true;
    }

    /**
     * Control access to posts
     *
     * @since 2.0.0
     * @internal
     *
     * @param WP_Query $wpQuery
     * @return WP_Query
     */
    public function preGetPosts(WP_Query $wpQuery)
    {
        if (
            $this->acLogicDisabled
            || $wpQuery->get('ac_stop_processing', false)
            || $wpQuery->is_singular()
            || $this->isBackend()
            || $this->isAdminPreview()
            || wp_doing_cron()
        ) {
            return $wpQuery;
        }

        $postTypes = $wpQuery->get('post_type');
        $consumerId = $this->consumersModule->getAuthorizedConsumerId();

        if (empty($postTypes) && $wpQuery->queried_object !== null && isset($wpQuery->queried_object->post_type)) {
            $postTypes = $wpQuery->queried_object->post_type;
        }

        if ($postTypes === 'any') {
            $postTypes = array_keys($this->getPublicQueryablePostTypes());
        }

        if (!is_array($postTypes)) {
            $postTypes = [$postTypes];
        }

        $accessibleGroupIds = $consumerId > 0 ? $this->helper->getConsumerGroupsIds($consumerId, true) : [];

        if (empty($accessibleGroupIds)) {
            $acQuery = [
                [
                    'key'     => '_acm_group_id',
                    'compare' => 'NOT EXISTS'
                ]
            ];
        } else {
            $acQuery = [
                'relation' => 'OR',
                [
                    'key'     => '_acm_group_id',
                    'compare' => 'IN',
                    'value'   => $accessibleGroupIds
                ],
                [
                    'key'     => '_acm_group_id',
                    'compare' => 'NOT EXISTS'
                ]
            ];
        }

        $metaQuery = $wpQuery->get('meta_query');
        $metaQuery = empty($metaQuery)
            ? $acQuery
            : [
                'relation' => 'AND',
                $metaQuery,
                $acQuery
            ];

        $wpQuery->set('meta_query', $metaQuery);
        
        return apply_filters('ls_acm_pre_get_posts', $wpQuery, $postTypes, $consumerId);
    }

    /**
     * Remove pages from menus to which consumer don't have access
     *
     * @since 2.0.0
     * @internal
     *
     * @param array $objects An array of menu objects
     * @return array Array of objects to which consumer have access
     */
    public function wpNavMenuObjects($objects)
    {
        if ($this->isBackend()) {
            return $objects;
        }

        foreach ($objects as $key => $object) {
            if ($object->type == 'post_type') {
                if (!$this->hasAccessToPost((int) $object->object_id)) {
                    unset($objects[$key]);
                }
            }
        }

        return $objects;
    }

    /**
     * Control access to the comments
     *
     * @since 2.0.0
     * @internal
     *
     * @param WP_Comment_Query $wpCommentQuery
     */
    public function preGetComments(WP_Comment_Query $wpCommentQuery)
    {
        if ($this->isBackend() || $this->isAdminPreview()) {
            return;
        }

        $protectedPosts = $this->getProtectedPostIdsToConsumer(
            $this->consumersModule->getAuthorizedConsumerId()
        );

        if (!empty($protectedPosts)) {

            if (!isset($wpCommentQuery->query_vars['post__not_in'])) {
                $wpCommentQuery->query_vars['post__not_in'] = [];
            }

            $wpCommentQuery->query_vars['post__not_in'] = array_unique(
                array_merge(
                    wp_parse_id_list($wpCommentQuery->query_vars['post__not_in']),
                    $protectedPosts
                )
            );
        }
    }

    /**
     * Retrieve current post Id even if current page is attachment
     * @since 2.0.0
     * @return int Current post Id
     */
    private function getCurrentPostId()
    {
        return is_attachment() && get_post() instanceof WP_Post
            ? get_post()->post_parent
            : get_the_ID();
    }

    /**
     * Handle invalid access
     * @since 3.5.14
     */
    public function noAccessHandling()
    {
        if ($this->isAdminPreview()) {
            $this->setNotification(__('You don\'t have access to this page but you can see it because you are logged as WP-User with editor rights.'));
            return;
        }

        $url = $this->getRedirectPage();

        if ($this->consumersModule->isConsumerAuthorized()) {
            $this->setError($this->getOption('noAccessErrorMsg', ''));
            $this->redirect($url);
        } elseif (empty($url)) {
            $this->setError($this->getOption('noAccessErrorMsg', ''));
        } else {

            $url = empty($_SERVER['REQUEST_URI']) || $_SERVER['REQUEST_URI'] == '/'
                ? $url
                : add_query_arg(['_wp_http_referer' => urlencode($_SERVER['REQUEST_URI'])], $url);

            $this->redirect($url);
        }
    }

    /**
     * Handle access to the current page
     * Earliest hook to reliably get $post global variable
     *
     * @since 2.0.0
     * @internal
     * @global wp_query $wp_query
     */
    public function wp()
    {
        if (
            !is_singular()
            || is_404()
            || is_search()
            || $this->isBackendRequest()
        ) {
            return;
        }

        $postId = $this->getCurrentPostId();

        if ($postId < 1) {
            return;
        }

        if (!$this->hasAccessToPost($postId)) {
            $this->noAccessHandling();
        }
    }

    /**
     * Shortcode [access-code-form]
     * Display form to submit new Access Code
     * Note: for the authorized consumers only
     *
     * @since 2.0.0
     * @internal
     *
     * @return string HTML-form
     */
    public function accessCodeFormShortcode()
    {
        $html = '';

        if ($this->consumersModule->isConsumerAuthorized()) {
            $html = (new LS\Template(__DIR__ . '/views/frontend'))
                ->assign('nonce', wp_create_nonce('acm-code'))
                ->render('actions-code-form.phtml', false);
        }

        return $html;
    }

    /**
     * Shortcode [consumer-access-codes]
     * Display a table with a list of consumer Access Codes
     *
     * @since 2.0.0
     * @internal
     *
     * @return string HTML-block
     */
    public function consumersAccessCodesShortcode()
    {
        $consumerId = $this->consumersModule->getAuthorizedConsumerId();
        $codes = [];

        foreach ($this->helper->getLatestConsumerCodesConnections($consumerId) as $cc) {
            $code = $this->helper->getAccessCode($cc->getCodeId())->getCode();
            if (!isset($codes[$code])) {
                $codes[$code] = $cc;
            }
        }

        return (new LS\Template(__DIR__ . '/views/frontend'))
            ->assign('codes', $codes)
            ->render('consumer-actions-codes.phtml', false);
    }

    /**
     * Shortcode [ac-private]
     * Display text inside shortcode only if consumer has Access Code
     *
     * @since 2.0.0
     * @internal
     * @sample [ac-private] private text [/ac-private]
     *
     * @param array $atts Attributes ('group_id' - allow access only to consumer who has Access Code(s) from the specified group Id [optional])
     * @param string $content Content to be display if consumer has Access Code
     * @return string Private content or empty string if no access
     */
    public function acPrivateShortcode($atts, $content)
    {
        $booAccessDenied = false;
        $consumerId = $this->consumersModule->getAuthorizedConsumerId();

        if ($consumerId > 0) {

            if (!isset($atts['group_id'])) {

                // if no code provided -> check if consumer has at least one ACTIVE code connection
                if (!$this->helper->hasConsumerCodes($consumerId, true)) {
                    $booAccessDenied = true;
                }

            } else {

                // otherwise check if consumer has access to specified AC
                $groupId = (int) $atts['group_id'];
                if ($groupId > 0 && !$this->helper->hasConsumerConnectionToGroup($consumerId, $groupId, true)) {
                    $booAccessDenied = true;
                } else {
                    $booAccessDenied = false;
                }
            }

            if (isset($atts['visibility']) && $atts['visibility'] == 'invisible') {
                $booAccessDenied = !$booAccessDenied;
            }

        } else {
            $booAccessDenied = true;
        }

        if ($booAccessDenied) {
            $privateMessage = $this->getOption('privateAreaMessage', '');
            $content = '<div class="ls-ac-private">' . do_shortcode($privateMessage) . '</div>';
        }

        return $content;
    }

    /**
     * Calculate the days till the end of all Access Codes connected to the current consumer
     *
     * @since 2.0.0
     * @internal
     *
     * @return int Days till the end of Access Code
     */
    public function acDaysLeftShortcode()
    {
        $expirationDate = $this->helper->calculateConsumerCodesExpirationDate(
            $this->consumersModule->getAuthorizedConsumerId()
        );

        return ceil(($expirationDate - time()) / DAY_IN_SECONDS) + 1;
    }

    /**
     * Calculate the date when the all connected to the current consumer codes will be expired
     *
     * @since 2.0.0
     * @internal
     *
     * @return string End date of Access Code
     */
    public function acEndDateShortcode()
    {
        if (!$this->consumersModule->isConsumerAuthorized()) {
            return '—';
        }

        $expirationDate = $this->helper->calculateConsumerCodesExpirationDate(
            $this->consumersModule->getAuthorizedConsumerId()
        );

        return date_i18n(get_option('date_format'), $expirationDate);
    }

    /**
     * Process actions and requests
     * @since 2.0.0
     * @internal
     */
    public function init()
    {
        if ($this->isFrontendRequest()) {

            $this->checkConsumerCodes();

            if ($this->hasAction()) {
                $this->actionsController();
            }
        }
    }

    /**
     * Process consumer actions on the frontend
     * @since 3.5.0
     */
    private function actionsController()
    {
        // Submit AC by consumer
        if ($this->isAction('acm-add-consumer-code')) {

            $nonce = $this->getParam('nonce', 'text', 'POST');

            if (wp_verify_nonce($nonce, 'acm-code')) {
                $this->addConsumerCodeAction(
                    $this->consumersModule->getAuthorizedConsumer(),
                    $this->getParam('code', 'text', 'POST')
                );
            }

            $this->redirect(remove_query_arg(['action', 'nonce']));
        }
    }

    /**
     * Add consumer code action
     *
     * @since 3.5.2
     * @internal
     *
     * @param false|\LS\ConsumerInterface $consumer Consumer to connect
     * @param string $code Access code
     */
    public function addConsumerCodeAction($consumer, string $code)
    {
        if (!$consumer) {
            $this->setError(__('Can not check your Access Code. Please try again later.', 'ls'));
            return;
        }

        $code = $this->helper->getAccessCodeByCode($code);
        $error = $this->validateAccessCodeToSubmit($code, $consumer->getEmail(), $consumer->getConsumerId());

        if (!empty($error)) {
            $this->setError($error);
            return;
        }

        if (!$this->helper->connectConsumerToAccessCode($consumer, $code)) {
            $this->setError(__('Can not check your Access Code. Please try again later.', 'ls'));
            $this->logBreakpoint();
            return;
        }

        $this->setAlert(__('Access Code has been entered', 'ls'));
    }

    /**
     * Check consumer access to the portal
     * Note: module check access once every 1 hour and in a case that consumer has no active codes it makes logout process
     * @since 3.0.0
     */
    private function checkConsumerCodes()
    {
        if ($this->getOption('acRequired', true)) {

            $consumer = $this->consumersModule->getAuthorizedConsumer();

            if ($consumer && (!isset($_SESSION['acm_check']) || time() > $_SESSION['acm_check'] + 3600)) {
                if ($this->helper->hasConsumerCodes($consumer->getConsumerId(), true)) {
                    $_SESSION['acm_check'] = time();
                } else {
                    unset($_SESSION['acm_check']);
                    $this->consumersModule->logoutConsumer();
                }
            }
        }
    }

    /**
     * Extend consumer dynamic fields
     *
     * @since 2.0.0
     * @internal
     *
     * @param \LS\FieldAbstract[] $fields
     * @return \LS\FieldAbstract[]
     */
    public function consumerDynamicFields($fields)
    {
        $fields['actionsCode'] = new LS\Field\Text(
            'actionsCode',
            __('Access Code', 'ls'),
            [
                'required'       => true,
                'maxlength'      => 150,
                'allowShortcode' => false,
                'overview'       => false
            ]
        );

        return $fields;
    }

    /**
     * Change query to retrieve a list of consumers with filter 'acmGroupId'
     *
     * @since 2.1.0
     * @internal
     *
     * @param array $filter Filter query
     * @return array Array ['filterQuery' => filter query, 'joins' => an array of extra joins]
     */
    public function consumerFilterGroupId($filter)
    {
        $wpdb = LS()->wpdb;
        $joins = ['_cg' => sprintf("LEFT JOIN {$wpdb->prefix}ls_acm_consumers_groups as _cg ON _cg.consumerId = c.consumerId")];
        $condition = $filter['c'] ?? '';
        $value = $filter['s_acmGroupId'] ?? 0;
        $value = is_array($value) ? $value : $this->helper->includeChildrenGroupsIds((int) $value);

        if ($condition == 'notEquals') {
            if (is_array($value) && !empty($value)) {
                $filterQuery = sprintf("NOT EXISTS ( SELECT 1 FROM {$wpdb->prefix}ls_acm_consumers_groups WHERE groupId IN (%s) AND consumerId = _cg.consumerId )", implode(',', $value));
            } else {
                $filterQuery = '1=0';
            }
        } else {
            $filterQuery = $this->generateQueryFromFilter('_cg.groupId', $condition, $value);
        }

        return ['filterQuery' => $filterQuery, 'joins' => $joins];
    }

    /**
     * Change query to retrieve a list of consumers with filter 'acmCode'
     *
     * @since 2.1.0
     * @internal
     *
     * @param array $filter Filter query
     * @return array Array ['filterQuery' => filter query, 'joins' => an array of extra joins]
     */
    public function consumerFilterCode($filter)
    {
        $wpdb = LS()->wpdb;
        $joins = [
            '_cc' => sprintf("LEFT JOIN {$wpdb->prefix}ls_acm_consumers_codes _cc ON _cc.consumerId = c.consumerId"),
            '_cd' => sprintf("LEFT JOIN {$wpdb->prefix}ls_acm_codes _cd ON _cd.codeId = _cc.codeId")
        ];

        $value = $filter['t'] ?? null;
        $condition = $filter['c'] ?? '';

        if ($condition == 'notContains' || $condition == 'notEquals') {
            $filterQuery = sprintf("NOT EXISTS (
                SELECT 1 
                FROM {$wpdb->prefix}ls_acm_consumers_codes _cc1
                INNER JOIN {$wpdb->prefix}ls_acm_codes _cd1 ON _cd1.codeId = _cc1.codeId AND _cd1.code %s
                WHERE _cc1.consumerId = _cc.consumerId
             )", $condition == 'notContains' ? sprintf("LIKE '%%%s%%'", esc_sql($value)) : sprintf("= '%s'", esc_sql($value)));
        } else {
            $filterQuery = $this->generateQueryFromFilter('_cd.code', $condition, $value);
        }

        return ['filterQuery' => $filterQuery, 'joins' => $joins];
    }

    /**
     * Validate Access Code for the possible errors when submitting
     *
     * @since 2.0.0
     *
     * @param string|ACMCode $codeToCheck Access Code to validate
     * @param null||string $email Related email to check for email pattern or null to skip email-pattern check
     * @param null|int $consumerId Consumer Id (optional)
     * @return string Error message or empty string on success
     */
    public function validateAccessCodeToSubmit($codeToCheck, $email = null, $consumerId = null)
    {
        $error = '';
        $code = $codeToCheck instanceof ACMCode
            ? $codeToCheck
            : $this->helper->getAccessCodeByCode($codeToCheck);

        if (!$code) {
            $error = $this->getOption('wrongCodeErrorMsg', '');
        } elseif (!$code->canBeUsed()) {
            if ($code->isLimitReached()) {
                $error = $this->getOption('codeUsedErrorMsg', '');
            } elseif ($code->isUpcoming()) {
                $error = $this->getOption('codeNotStartedErrorMsg', '');
            } elseif ($code->isExpired()) {
                $error = $this->getOption('codeIsEndedErrorMsg', '');
            } else {
                $error = $this->getOption('wrongCodeErrorMsg', '');
            }
        } elseif ($email !== null && $code->isEmailPatternActive() && !$code->matchEmail($email)) {
            $error = $this->getOption('wrongCodePatternErrorMsg', '');
        } elseif ($consumerId !== null && $this->helper->hasConsumerCode((int) $consumerId, $code->getCode())) {
            $error = $this->getOption('codeUsedErrorMsg', '');
        }

        return $error;
    }

    /**
     * Validate Access Code when saving consumer
     *
     * @since 2.0.0
     * @internal
     *
     * @param string[] $errors Errors list
     * @param array $data Data to be saved
     * @param int $consumerId Consumer Id
     * @return string[] Errors list
     */
    public function consumerSaveValidation($errors, $data, $consumerId)
    {
        if (isset($data['actionsCode'])) {
            $error = $this->validateAccessCodeToSubmit($data['actionsCode'], $data['email'] ?? null);
            if (!empty($error)) {
                $errors[] = $error;
            }
        } elseif (isset($data['email']) && $consumerId > 0) {
            foreach ($this->helper->getConsumerAccessCodes($consumerId, true) as $code) {
                if ($code->isEmailPatternActive() && !$code->matchEmail($data['email'])) {
                    $errors[] = $this->getOption('wrongCodePatternErrorMsg', '');
                }
            }
        }

        return $errors;
    }

    /**
     * Connect Access Code to consumer when consumer has been created
     *
     * @since 3.6.3
     * @internal
     *
     * @param \LS\Consumer $consumer Consumer to save
     */
    public function consumerCreated(\LS\Consumer $consumer)
    {
        if (!$consumer->hasDynamicValue('actionsCode')) {
            return;
        }

        $code = $this->helper->getAccessCodeByCode(
            $consumer->getDynamicValue('actionsCode')
        );

        if ($code && $code->canBeUsed()) {
            $this->helper->connectConsumerToAccessCode($consumer, $code);
        }

        // remove AC code from cfv as it will be saved there with the all other dynamic fields
        $consumer->removeDynamicField('actionsCode');
        $this->consumersModule->helper()->deleteDynamicField('actionsCode');
    }

    /**
     * Remove connection between consumer and Access Code if consumer has been removed
     *
     * @since 3.7.0
     * @internal
     *
     * @param \LS\ConsumerInterface $consumer Consumer to be removed
     */
    public function consumerBeforeDeleted(\LS\ConsumerInterface $consumer)
    {
        $this->helper->disconnectConsumerFromAccessCodes($consumer->getConsumerId());
        $this->helper->disconnectConsumerFromGroups($consumer->getConsumerId());
    }

    /**
     * Deny to login if consumer don't have at least one active Access Code
     *
     * @since 2.0.0
     * @internal
     *
     * @param bool $access Determine to deny access or not
     * @param int $consumerId Consumer Id
     * @return bool Returns false if consumer don't have Access Codes. Otherwise return the default value
     */
    public function checkConsumerLogin($access, $consumerId)
    {
        if ($access && $this->getOption('acRequired', true)) {
            $access = $this->helper->hasConsumerCodes($consumerId, true);
            if (!$access) {
                $this->setError($this->getOption('inactiveCodeErrorMsg', ''));
            }
        }

        return $access;
    }

    /**
     * Allow to access to the page by the secure link even if this is guest
     *
     * @since 2.4.1
     * @internal
     *
     * @param bool $access True if access granted
     * @param int $postId Post to check the access
     * @return bool True if access granted
     */
    public function checkAuthorizationAccess($access, $postId)
    {
        return $access || $this->hasAccessBySecureLink($postId);
    }

    /**
     * Retrieve URL to the page to be redirected if this is guest and it don't have access
     * @since 2.0.0
     * @return string URL
     */
    private function getRedirectGuestPage()
    {
        $url = $this->consumersModule->getLoginPageLink();
        $url = empty($url) ? home_url('/404') : $url;

        return $url;
    }

    /**
     * Retrieve Id to the page to be redirected if this is consumer and it don't have access
     * @since 3.5.6
     * @return int Page Id
     */
    private function getRedirectConsumerPageId()
    {
        $id = 0;
        $redirectGuest = $this->getOption('redirectConsumer', 'consumersLogin');

        if ($redirectGuest == 'consumersLogin') {
            $id = $this->consumersModule->getLoginPageId();
        } elseif ($redirectGuest == 'custom') {
            $id = (int) $this->getOption('redirectConsumerPage', 0);
        }

        return $id;
    }

    /**
     * Retrieve URL to the page to be redirected if this is consumer and it don't have access
     * @since 2.0.0
     * @return string URL
     */
    private function getRedirectConsumerPage()
    {
        $pageId = $this->getRedirectConsumerPageId();
        $url = $pageId > 0 ? get_permalink($pageId) : home_url('/404');

        if ($_SERVER['REQUEST_URI'] != '/') {
            $url = add_query_arg(['_wp_http_referer' => urlencode($_SERVER['REQUEST_URI'])], $url);
        }

        return $url;
    }

    /**
     * Retrieve URL to the page to be redirected if the guest/consumer don't have access to post
     * @since 2.1.2
     * @return string URL
     */
    private function getRedirectPage()
    {
        return
            $this->consumersModule->isConsumerAuthorized()
                ? $this->getRedirectConsumerPage()
                : $this->getRedirectGuestPage();
    }

    /**
     * Retrieve post types that can be used with the Access Codes
     * @since 2.0.0
     * @return object[] Post types
     */
    public function getAvailablePostTypes()
    {
        $postTypes = get_post_types(['_builtin' => false], 'object');

        $postTypes['post'] = get_post_type_object('post');
        $postTypes['page'] = get_post_type_object('page');

        /** @var object $postType */
        foreach ($postTypes as $key => $postType) {
            if (property_exists($postType, 'acm_content_access') && !$postType->acm_content_access) {
                unset($postTypes[$key]);
            }
        }

        ksort($postTypes);

        return apply_filters('ls_acm_available_post_types', $postTypes);
    }

    /**
     * Retrieve a list of public queryable post types
     * @since 3.5.2
     * @return array A list of post type names
     */
    public function getPublicQueryablePostTypes()
    {
        return array_filter($this->getAvailablePostTypes(), static function ($pt) {
            /** @var WP_Post_Type $pt */
            return !property_exists($pt, 'publicly_queryable') || $pt->publicly_queryable;
        });
    }

    /**
     * Generate a list of conditions for Com.Center
     *
     * @since 2.0.0
     * @internal
     *
     * @param LSConditionalEmailCondition[] $conditions List of conditions
     * @return LSConditionalEmailCondition[] List of conditions
     */
    public function comCenterInit($conditions)
    {
        $acmHelper = $this->helper;

        $conditions['consumer_ac_added'] = new LSConditionalEmailCondition('consumer_ac_added', __('Access Code connected to consumer', 'ls'), $this->getTitle(), [
            'onceADay'       => false,
            'allowedPeriods' => [LSConditionalEmail::PERIOD_ON],
            'shortcodes'     => [
                '{code}' => [
                    'label' => __('Access Code', 'ls'),
                    'value' => static function ($consumerId, LSConditionalEmail $ce) {
                        return $ce->getConsumerExtraValue($consumerId, 'code', 'test-code-12345');
                    }
                ]
            ],
            'defaultMessage' => '<p>Access Code <b>{code}</b> has been added to your account.</p>'
        ]);

        $conditions['consumer_ac_prolongation'] = new LSConditionalEmailCondition('consumer_ac_prolongation', __('Access Code prolongation', 'ls'), $this->getTitle(), [
            'onceADay'       => false,
            'allowedPeriods' => [LSConditionalEmail::PERIOD_ON],
            'shortcodes'     => [
                '{code}'         => [
                    'label' => __('Access Code', 'ls'),
                    'value' => static function ($consumerId, LSConditionalEmail $ce) {
                        return $ce->getConsumerExtraValue($consumerId, 'code', 'test-code-12345');
                    }
                ],
                '{new-end-date}' => [
                    'label' => __('New Access Code ends date', 'ls'),
                    'value' => static function ($consumerId, LSConditionalEmail $ce) {
                        return $ce->getConsumerExtraValue($consumerId, 'newEndDate', date_i18n(get_option('date_format')));
                    }
                ]
            ],
            'defaultMessage' => '<p>Access Code <b>{code}</b> has been prolonged to <b>{new-end-date}</b></p>'
        ]);

        $conditions['ac_code_ends'] = new LSConditionalEmailCondition('ac_code_ends', __('Access Code ends', 'ls'), $this->getTitle(), [
            'shortcodes'     => [
                '{code}'        => [
                    'label' => __('Access Code', 'ls'),
                    'value' => static function ($consumerId, LSConditionalEmail $ce) {
                        return $ce->getConsumerExtraValue($consumerId, 'code', 'test-code-12345');
                    }
                ],
                '{companyName}' => [
                    'label' => __('Access Code company name', 'ls'),
                    'value' => static function ($consumerId, LSConditionalEmail $ce) {
                        return $ce->getConsumerExtraValue($consumerId, 'companyName', 'COMPANY-XYZ');
                    }
                ]
            ],
            'defaultMessage' => '<p>Dear [consumer-name], your Access Code {code} provided by the company {companyName} will be ended soon</p>',
            'recipientsCb'   => static function (LSConditionalEmail $ce) use ($acmHelper) {

                $ids = [];

                foreach ($acmHelper->getAccessEndings($ce->getDaysToEvent()) as $data) {
                    foreach ($data->recipients as $consumerId) {

                        $ce
                            ->setConsumerExtraValue($consumerId, 'code', $data->code)
                            ->setConsumerExtraValue($consumerId, 'companyName', $data->companyName);

                        $ids[] = $consumerId;
                    }
                }

                return $ids;
            }
        ]);

        return $conditions;
    }

    /**
     * Send email on Access Code connected to consumer
     *
     * @since 2.0.0
     * @internal
     *
     * @param \LS\ConsumerInterface $consumer Consumer
     * @param ACMCode $code
     */
    public function accessCodeConnected(\LS\ConsumerInterface $consumer, ACMCode $code)
    {
        /** @var comCenterLoyaltySuite $comCenterModule */
        $comCenterModule = LS()->getModule('comCenter');
        $comCenterModule->triggerConditionalEmails('consumer_ac_added', static function (LSConditionalEmail $ce) use ($consumer, $code) {

            // Check status here because consumer MIGHT be not verified (e.g. code added during the registration process)
            if (!$consumer->canReceiveNewsletters()) {
                return 0;
            }

            $ce->setConsumerExtraValue($consumer->getConsumerId(), 'code', $code->getCode());

            return $consumer->getConsumerId();
        });
    }

    /**
     * Init recipients conditions
     *
     * @since 2.0.0
     * @internal
     *
     * @param LSConditionalEmailFilter[] $recipients Array of recipients conditions
     * @return LSConditionalEmailFilter[] Updated array of recipients conditions
     */
    public function initRecipients($recipients)
    {
        $acmHelper = $this->helper;

        $recipients['acm'] =
            (new LSConditionalEmailFilter('acm'))
                ->setForm(static function ($selected, $ident) use ($acmHelper) {
                    (new LS\Template(__DIR__ . '/views/backend'))
                        ->assign('selected', $selected)
                        ->assign('ident', $ident)
                        ->assign('groupsList', $acmHelper->getGroupsList(true))
                        ->render('admin-com-center-email.phtml');
                })
                ->setFilter(static function ($recipients, $ident) use ($acmHelper) {

                    $groupsIds = wp_parse_id_list($ident);

                    foreach ($recipients as $key => $consumerId) {

                        $access = false;

                        foreach ($groupsIds as $groupId) {
                            if ($acmHelper->hasConsumerConnectionToGroup($consumerId, $groupId, true)) {
                                $access = true;
                                break;
                            }
                        }

                        if (!$access) {
                            unset($recipients[$key]);
                        }
                    }

                    return $recipients;
                })
                ->setColumn(static function ($ident) use ($acmHelper) {

                    $groups = [];

                    foreach (wp_parse_id_list($ident) as $groupId) {
                        if ($group = $acmHelper->getGroup($groupId)) {
                            $groups[] = '<a href="' . acmAdminLoyaltySuite::contentAccessEditLink($groupId) . '" target="_blank">' . \LS\Library::substrDots($group->getName(), 0, 30) . '</a>';
                        }
                    }

                    return (empty($groups) ? '—' : implode('<br />', $groups));
                });

        return $recipients;
    }

    /**
     * Connect consumer to the company defined in the connected Access Code
     *
     * @since 2.1.0
     * @internal
     *
     * @param \LS\ConsumerInterface $consumer Consumer
     * @param ACMCode $code Connected code
     */
    public function assignCompanyToConsumer(\LS\ConsumerInterface $consumer, ACMCode $code)
    {
        if ($code->canAssignCompany()) {

            $consumer->setCompanyId($code->getCompanyId());

            $this->consumersModule->helper()->updateConsumer($consumer);
            $this->consumersModule->helper()->insertLog('company_assigned', $consumer->getConsumerId(), $code->getCompanyId());
        }
    }

    /**
     * Check if a company members have access to a post
     * Note: long-chain function! Post -> AC -> Group -> Company
     *
     * @since 3.5.11
     *
     * @param int $companyId Company Id
     * @param int $postId Post Id
     * @return bool True if the company members have access to the post
     */
    public function hasCompanyAccessToPost(int $companyId, int $postId): bool
    {
        if (!$this->isPostProtected($postId)) {
            return true;
        }

        $companyGroupIds = $this->helper->getGroupIdsConnectedToCompany($companyId);
        $companyProtectedPostIds = $this->helper->getProtectedPostIds($companyGroupIds);

        return in_array($postId, $companyProtectedPostIds);
    }

    /**
     * Relevanssi integration
     *
     * @since 3.0.0
     * @internal
     *
     * @param string $where
     * @return string
     */
    public function relevanssiWhere($where)
    {
        if ($this->acLogicDisabled) {
            return $where;
        }

        $protectedPosts = $this->getProtectedPostIdsToConsumer(
            $this->consumersModule->getAuthorizedConsumerId()
        );

        if (empty($protectedPosts)) {
            return $where;
        }

        $where .= sprintf(" AND relevanssi.doc NOT IN (%s)", implode(',', $protectedPosts));

        return $where;
    }
}