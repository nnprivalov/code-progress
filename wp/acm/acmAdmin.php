<?php
class acmAdminLoyaltySuite extends acmLoyaltySuite implements \LS\ModuleAdminInterface
{
    /** @var \LS\ConsumersHelper */
    private $consumersHelper;

    /**
     * @since 3.4.0
     * @return bool
     */
    public function hasAdministrationPage()
    {
        return true;
    }

    /**
     * Main function to load models, create actions and filters, init variables
     * @since 3.4.0
     * @internal
     */
    public function load()
    {
        parent::load();

        $this->consumersHelper = $this->consumersModule->helper();

        add_action('admin_enqueue_scripts', [$this, 'adminScripts']);
        add_action('admin_init', [$this, 'adminInit']);
        add_action('wp_ajax_acmActivities', [$this, 'acmActivities']);
        add_action('wp_ajax_acmChangeNote', [$this, 'acmChangeNote']);
        add_action('ls_cron_daily', [$this, 'lsCronDaily'], 9);

        // connect AC to Consumers
        add_filter('consumerDetails', [$this, 'consumerDetailsFilter'], 10, 2);
        add_filter('ls_consumers_overview_columns', [$this, 'consumersOverviewColumns']);
        add_filter('ls_consumer_log__access_code', [$this, 'consumerLog']);
        add_filter('ls_consumer_log__access_code_removed', [$this, 'consumerLog']);
        add_filter('ls_consumer_log__access_group', [$this, 'consumerLog']);
        add_filter('ls_consumer_log__access_group_removed', [$this, 'consumerLog']);
        add_filter('ls_consumer_log__company_assigned', [$this, 'consumerLog']);
        add_filter('ls_consumer_log__prolongation', [$this, 'consumerLog']);

        // connect AC to WP posts
        add_action('add_meta_boxes', [$this, 'addMetaBoxes']);
        add_action('save_post', [$this, 'savePost']);

        // connect AC to companies
        add_action('ls_company_activated', [$this, 'companyActivated']);
        add_action('ls_company_deactivated', [$this, 'companyDeactivated']);
        add_action('ls_company_deleted', [$this, 'companyDeleted'], 10, 2);
        add_action('ls_company_wiped', [$this, 'companyDeactivated']);
    }

    /**
     * Create shortcodes
     * @since 3.4.0
     * @internal
     */
    public function initShortcodes()
    {
        parent::initShortcodes();

        LS()->addShortcodeInfo(
            'access-code-form',
            __('Form to enter Access Code by consumer', 'ls'),
            '',
            $this->getName()
        );

        LS()->addShortcodeInfo(
            'consumer-access-codes',
            __('Consumer Access Codes', 'ls'),
            __('Table with a list of Access Codes connected to current consumer. consumer_id - optional attribute. Use it to show Access Codes connected to another consumer', 'ls'),
            $this->getName()
        );

        LS()->addShortcodeInfo(
            'ac-private',
            __('Protected area', 'ls'),
            __('Display hidden content protected by Access Code. To specify Access Code use the attribute "code". If the code attribute is not specified, then will be checked if the consumer has any Access Code', 'ls'),
            $this->getName(),
            ['group_id' => __('CA-Group ID', 'ls'), 'visibility' => __('Set the value to "invisible" to reverse the logic', 'ls')],
            '[ac-private] ... [/ac-private]'
        );

        LS()->addShortcodeInfo(
            'ac-days-left',
            __('Days till Access Codes end', 'ls'),
            __('Display number of days till the end of Access Code connected to current consumer', 'ls'),
            $this->getName()
        );

        LS()->addShortcodeInfo(
            'ac-end-date',
            __('Access Code expiration date', 'ls'),
            __('Display end date of Access Code connected to current consumer', 'ls'),
            $this->getName()
        );
    }

    /**
     * After module has been installed
     * @since 3.4.0
     * @internal
     */
    public function onInstall()
    {
        require_once __DIR__ . '/install.php';

        acmInstallLoyaltySuite();
    }

    /**
     * Uninstall module
     * @since 3.4.0
     * @internal
     */
    public function onUninstall()
    {
        $this->helper->deleteTables();
        $this->helper->deleteAllGroupConnections();
    }

    /**
     * After module activation
     * @since 3.4.0
     * @internal
     */
    public function afterActivate()
    {
        $this->addCapsToRole(['ls_acm'], 'contributor');
        $this->addCapsToRole(['ls_acm', 'ls_acm_manage'], 'author');
        $this->addCapsToRole(['ls_acm', 'ls_acm_manage'], 'editor');
        $this->addCapsToRole(['ls_acm', 'ls_acm_manage', 'ls_acm_settings'], 'administrator');
    }

    /**
     * Render administration menu
     * @since 3.4.0
     * @internal
     */
    public function moduleAdministrationPage()
    {
        include_once __DIR__ . '/tables/groups.php';
        include_once __DIR__ . '/tables/codes.php';
        include_once __DIR__ . '/tables/posts.php';

        $template = new LS\Template(__DIR__ . '/views/backend');
        $template->assign('options', $this->getOptions());

        if (!isset($_REQUEST['view'])) {
            $template
                ->assign('groupsTable', new acmGroupsTable())
                ->assign('codesTable', new acmCodesTable())
                ->assign('groupsList', $this->helper->getGroupsList(true))
                ->assign('tabs', [
                    ['name' => 'ls-codes', 'title' => __('Portal access', 'ls'), 'view' => 'admin-codes.phtml'],
                    ['name' => 'ls-groups', 'title' => __('Content access', 'ls'), 'view' => 'admin-groups.phtml'],
                    ['name' => 'ls-settings', 'title' => __('Settings', 'ls'), 'view' => 'admin-settings.phtml', 'capability' => 'ls_acm_settings']
                ]);
        }

        // add/edit group
        if (isset($_REQUEST['view']) && ($_REQUEST['view'] == 'acm-new-group' || $_REQUEST['view'] == 'group-details')) {

            if ($_REQUEST['view'] == 'acm-new-group') {
                $groupId = 0;
                $group = new ACMGroup();
                $title = __('New CA-Group', 'ls');
            } else {
                $groupId = $this->getParam('groupId', 'id');
                $group = $this->helper->getGroup($groupId);
                $title = __('Edit CA-Group', 'ls');
            }

            if ($group) {

                if ($this->isAction('acm-new-group') || $this->isAction('acm-update-group')) {
                    $group->setFromArray(stripslashes_deep($_POST));
                }

                $template
                    ->assign('groupStructure', $this->getGroupsStructure())
                    ->assign('group', $group)
                    ->assign('posts', $this->helper->getProtectedPostsInGroup($groupId))
                    ->assign('page', ['name' => 'ls-group', 'title' => $title, 'view' => 'admin-group-details.phtml']);

            } else {
                $template->assign('page', ['name' => 'ls-group', 'title' => $title, 'content' => __('Can not find CA-Group', 'ls')]);
            }
        }

        // add/edit Access Code
        if (isset($_REQUEST['view']) && ($_REQUEST['view'] == 'acm-new-code' || $_REQUEST['view'] == 'code-details')) {

            if ($_REQUEST['view'] == 'acm-new-code') {
                $code = new ACMCode();
                $codeGroupIds = [];
                $title = __('New Access Code', 'ls');
                $codeStructure = $this->getAccessCodeStructureToAdd();
            } else {
                $codeId = $this->getParam('codeId', 'id');
                $code = $this->helper->getAccessCode($codeId);
                $codeGroupIds = $this->helper->getAccessCodeGroupIds($codeId);
                $title = __('Access Code details', 'ls');
                $codeStructure = $this->getAccessCodeStructureToEdit();
            }

            if ($code) {

                if ($this->isAction('acm-new-code') || $this->isAction('acm-update-code')) {
                    $code->setFromArray(stripslashes_deep($_POST));
                    $codeGroupIds = $this->getParam('groupIds', 'array');
                }

                $template
                    ->assign('code', $code)
                    ->assign('codeStructure', $codeStructure)
                    ->assign('codeGroupIds', $codeGroupIds)
                    ->assign('page', ['name' => 'ls-code', 'title' => $title, 'view' => 'admin-code-details.phtml']);

            } else {
                $template->assign('page', ['name' => 'ls-code', 'title' => $title, 'content' => __('Can not find Access Code', 'ls')]);
            }
        }

        // generate Access Codes
        if (isset($_REQUEST['view']) && $_REQUEST['view'] == 'acm-generate-codes') {

            $gcStructure = $this->getAccessCodesGenerateStructure();

            if ($this->isAction('acm-generate-codes')) {

                foreach ($gcStructure as $fieldName => $field) {
                    if (isset($_POST[$fieldName])) {
                        $field->setValue(stripslashes_deep($_POST[$fieldName]));
                    }
                }

                $codeGroupIds = $this->getParam('groupIds', 'array');

            } else {
                $codeGroupIds = [];
            }

            $gcStructure['groupIds']->setValue($codeGroupIds);

            $template
                ->assign('gcStructure', $gcStructure)
                ->assign('page', ['name' => 'ls-generate-codes', 'title' => __('Generate codes', 'ls'), 'view' => 'admin-generate-codes.phtml']);
        }

        // prolong Access Codes
        if (isset($_REQUEST['view'])) {
            if ($_REQUEST['view'] == 'acm-prolong-codes-1') {

                $template
                    ->assign('companyId', $this->getParam('companyId', 'id'))
                    ->assign('page', ['name' => 'ls-prolong-codes', 'title' => __('Prolong Access Codes', 'ls'), 'view' => 'admin-prolong-codes-1.phtml']);

            } elseif ($_REQUEST['view'] == 'acm-prolong-codes-2') {

                $codes = [];
                $companyId = $this->getParam('companyId', 'id');
                $selCodes = $this->getParam('codes', 'text');
                $selCodes = wp_parse_id_list($selCodes);

                foreach ($this->helper->getAccessCodesByCompanyId($companyId, false) as $code) {
                    $codes[$code->getCodeId()] = $code->getCode();
                }

                $template
                    ->assign('codes', $codes)
                    ->assign('selCodes', $selCodes)
                    ->assign('page', ['name' => 'ls-prolong-codes', 'title' => __('Prolong Access Codes', 'ls'), 'view' => 'admin-prolong-codes-2.phtml']);

            } elseif ($_REQUEST['view'] == 'acm-prolong-codes-3') {

                $selCodes = $this->getParam('codes', 'text');
                $endDate = $this->getParam('endDate', 'date');

                $comCenterActive = LS()->isModuleActive('comCenter');
                if ($comCenterActive) {
                    $hasProlongationEmail = ComCenterHelper::hasConditionalEmailByCondition('consumer_ac_prolongation', true);
                } else {
                    $hasProlongationEmail = false;
                }

                $template
                    ->assign('selCodes', $selCodes)
                    ->assign('endDate', $endDate)
                    ->assign('comCenterActive', $comCenterActive)
                    ->assign('hasProlongationEmail', $hasProlongationEmail)
                    ->assign('page', ['name' => 'ls-prolong-codes', 'title' => __('Prolong Access Codes', 'ls'), 'view' => 'admin-prolong-codes-3.phtml']);
            }
        }

        // save settings
        if ($this->isAction('save-acm-settings')) {
            $template->assign('options', array_merge(
                $this->getOptions(),
                stripslashes_deep($_POST)
            ));
        }

        $template->renderAdminPage();
    }

    /**
     * Load module scripts
     * @since 3.4.0
     * @internal
     */
    public function adminScripts()
    {
        wp_enqueue_style('ls-acm-admin', $this->getUrl2Module() . 'css/acm-admin.css', [], $this->getVersion());

        if ($this->isCurrentPluginPage()) {
            wp_enqueue_script('editor');
            wp_enqueue_script('thickbox');
            wp_enqueue_script('media-upload');
            wp_enqueue_script('ls-acm-admin', $this->getUrl2Module() . 'js/acm-admin.js', ['jquery'], $this->getVersion(), true);
        }
    }

    /**
     * Retrieve form fields to create/edit CA-Group
     * @since 3.4.0
     * @return LS\FieldAbstract[]
     */
    public function getGroupsStructure()
    {
        $groupsListFunc = static function () {
            return ACMHelper::getGroupsList(true);
        };

        return [
            'groupId'       => new LS\Field\Value('groupId', 'ID'),
            'name'          => new LS\Field\Text('name', __('Name', 'ls'), ['maxlength' => 400, 'required' => true]),
            'parentGroupId' => new LS\Field\Select('parentGroupId', __('Parent', 'ls'), $groupsListFunc),
            'description'   => new LS\Field\Textarea('description', __('Description (intern.)', 'ls'), ['maxlength' => 2000]),
            'active'        => new LS\Field\Checkbox('active', __('Active', 'ls'), [], true),
            'createDate'    => new LS\Field\Date('createDate', __('Create date', 'ls'), ['disabled' => true], time())
        ];
    }

    /**
     * Retrieve form fields to create/edit Access Code
     * @since 3.4.0
     * @return LS\FieldAbstract[]
     */
    public function getAccessCodeStructure()
    {
        $groupsListFunc = static function () {
            return ACMHelper::getGroupsList(true);
        };

        $companiesListFunc = static function () {
            return LSCompaniesHelper::getCompaniesList(true, true);
        };

        return [
            'codeId'        => new LS\Field\Value('codeId', 'ID'),
            'code'          => new LS\Field\Text('code', __('Access Code', 'ls'), ['minlength' => 3, 'maxlength' => 150, 'required' => true]),
            'startDate'     => new LS\Field\Date('startDate', __('Start date', 'ls'), ['required' => true], time()),
            'endDate'       => new LS\Field\Date('endDate', __('End date', 'ls'), ['required' => true]),
            'companyId'     => new LS\Field\Select('companyId', __('Company', 'ls'), $companiesListFunc),
            'assignCompany' => new LS\Field\Checkbox('assignCompany', __('Assign company to consumers', 'ls'), [], true),
            'groupIds'      => new LS\Field\Selectize('groupIds', __('CA-Groups', 'ls'), $groupsListFunc),
            'active'        => new LS\Field\Checkbox('active', __('Active', 'ls'), [], true),
            'hasLimit'      => new LS\Field\Checkbox('hasLimit', __('Has limit', 'ls')),
            'limit'         => new LS\Field\Value('limit', __('Limit, times', 'ls'), ['minValue' => 1, 'maxlength' => 9]),
            'hasLifetime'   => new LS\Field\Checkbox('hasLifetime', __('Has lifetime', 'ls')),
            'lifetime'      => new LS\Field\Value('lifetime', __('Lifetime, days', 'ls')),
            'note'          => new LS\Field\Text('note', __('Note', 'ls'), ['maxlength' => 400]),
            'hasEP'         => new LS\Field\Checkbox('hasEP', __('Check email structure on register/profile update', 'ls'), ['description' => __('Check email for a match to the specified pattern during the registration/profile update process if the submitted Access Code related to the current Access Code.', 'ls')]),
            'emailPattern'  => new LS\Field\Text('emailPattern', __('Email pattern', 'ls'), []),
            'drcActive'     => new LS\Field\Checkbox('drcActive', __('Delete inactive consumers', 'ls'), ['description' => __('Delete all inactive consumers related to this Access Code in X days after the Access Code ends.', 'ls')]),
            'drcDays'       => new LS\Field\Value('drcDays', __('Days to wait before delete')),
            'createDate'    => new LS\Field\Date('createDate', __('Create date', 'ls'), ['disabled' => true], time())
        ];
    }

    /**
     * Retrieve access code structure to add
     * @since 3.5.8
     * @return \LS\FieldAbstract[]
     */
    public function getAccessCodeStructureToAdd()
    {
        $structure = $this->getAccessCodeStructure();

        unset($structure['codeId'], $structure['createDate']);

        return $structure;
    }

    /**
     * Retrieve access code structure to edit
     * @since 3.5.8
     * @return \LS\FieldAbstract[]
     */
    public function getAccessCodeStructureToEdit()
    {
        $structure = $this->getAccessCodeStructure();

        $structure['endDate']
            ->setRequired(false)
            ->setDisabled(true);

        return $structure;
    }

    /**
     * Retrieve form fields to generate Access Codes as LS\FieldAbstract objects
     * @since 3.4.0
     * @return LS\FieldAbstract[]
     */
    private function getAccessCodesGenerateStructure()
    {
        $groupsListFunc = static function () {
            return ACMHelper::getGroupsList(true);
        };

        $companiesListFunc = static function () {
            return LSCompaniesHelper::getCompaniesList(true, true);
        };

        return [
            'codesCount'    => new LS\Field\Value('codesCount', __('Number of Access Codes', 'ls'), ['minValue' => 1, 'maxValue' => 10000, 'required' => true]),
            'numbers'       => new LS\Field\Checkbox('numbers', __('Numbers only (in the Access Code)', 'ls'), [], true),
            'codesLength'   => new LS\Field\Value('codesLength', __('Access Code length (not inc. prefix)', 'ls'), ['minValue' => 5, 'maxValue' => 150, 'required' => true], 16),
            'codesPrefix'   => new LS\Field\Text('codesPrefix', __('Prefix', 'ls'), ['maxlength' => 20]),
            'startDate'     => new LS\Field\Date('startDate', __('Start date', 'ls'), ['required' => true], time()),
            'endDate'       => new LS\Field\Date('endDate', __('End date', 'ls'), ['required' => true]),
            'companyId'     => new LS\Field\Select('companyId', __('Company', 'ls'), $companiesListFunc),
            'assignCompany' => new LS\Field\Checkbox('assignCompany', __('Assign company to consumers', 'ls'), [], true),
            'groupIds'      => new LS\Field\Selectize('groupIds', __('CA-Groups', 'ls'), $groupsListFunc),
            'active'        => new LS\Field\Checkbox('active', __('Active', 'ls'), [], true),
            'hasLimit'      => new LS\Field\Checkbox('hasLimit', __('Activate limit', 'ls')),
            'limit'         => new LS\Field\Value('limit', __('Limit, times', 'ls'), ['minValue' => 1, 'maxValue' => 100000000]),
            'hasLifetime'   => new LS\Field\Checkbox('hasLifetime', __('Has lifetime', 'ls')),
            'lifetime'      => new LS\Field\Value('lifetime', __('Lifetime, days', 'ls')),
            'note'          => new LS\Field\Text('note', __('Note', 'ls'), ['maxlength' => 400]),
            'hasEP'         => new LS\Field\Checkbox('hasEP', __('Check email structure on register/profile update', 'ls'), ['description' => __('Check email for a match to the specified pattern during the registration/profile update process if the submitted Access Code related to the current Access Code.', 'ls')]),
            'emailPattern'  => new LS\Field\Text('emailPattern', __('Email pattern', 'ls'), []),
            'drcActive'     => new LS\Field\Checkbox('drcActive', __('Delete inactive consumers', 'ls'), ['description' => __('Delete all inactive consumers related to this Access Code in X days after the Access Code ends.', 'ls')]),
            'drcDays'       => new LS\Field\Value('drcDays', __('Days to wait before delete'))
        ];
    }

    /**
     * Process admin actions and requests
     * @since 3.4.0
     * @internal
     */
    public function adminInit()
    {
        // Post overviews integration
        foreach ($this->getAvailablePostTypes() as $type) {
            add_filter('manage_' . $type->name . '_posts_columns', [$this, 'addColumn']);
            add_action('manage_' . $type->name . '_posts_custom_column', [$this, 'addRowForColumn'], 10, 2);
        }

        if (!$this->hasAction()) {
            return;
        }

        if (current_user_can('ls_acm_manage')) {
            if ($this->isAction('acm-new-group')) {
                $this->createGroupAction();
            } elseif ($this->isAction('acm-update-group')) {
                if (isset($_REQUEST['submit'])) {
                    $this->updateGroupAction();
                } elseif (isset($_REQUEST['includePost'])) {
                    $this->connectPostToGroupAction();
                }
            } elseif ($this->isAction('delete-acm-groups')) {
                $this->deleteGroupsAction();
            } elseif ($this->isAction('duplicate-acm-groups')) {
                $this->duplicateGroupsAction();
            } elseif ($this->isAction('acm-exclude-post')) {
                $this->excludedPostFromGroupAction();
            } elseif ($this->isAction('acm-new-code')) {
                $this->createCodeAction();
            } elseif ($this->isAction('acm-generate-codes')) {
                $this->generateCodesAction();
            } elseif ($this->isAction('acm-update-code')) {
                $this->updateCodeAction();
            } elseif ($this->isAction('delete-acm-codes')) {
                $this->deleteCodesAction();
            } elseif ($this->isAction('acm-create-connection')) {
                $this->createCodeConnectionAction();
            } elseif ($this->isAction('acm-remove-connection')) {
                $this->deleteCodeConnectionAction();
            } elseif ($this->isAction('acm-prolong')) {
                $this->prolongCodeConnectionAction();
            } elseif ($this->isAction('acm-create-group-connection')) {
                $this->createGroupConnectionAction();
            } elseif ($this->isAction('acm-remove-group-connection')) {
                $this->disconnectGroupFromConsumerAction();
            } elseif ($this->isAction('activate-acm-codes')) {
                $this->activateCodesAction();
            } elseif ($this->isAction('deactivate-acm-codes')) {
                $this->deactivateCodesAction();
            } elseif ($this->isAction('acm-prolong-codes-1')) {
                $this->prolongCodes1Action();
            } elseif ($this->isAction('acm-prolong-codes-2')) {
                $this->prolongCodes2Action();
            } elseif ($this->isAction('acm-prolong-codes-3')) {
                $this->prolongCodes3Action();
            }
        }

        if ($this->isAction('save-acm-settings') && isset($_REQUEST['submit']) && current_user_can('ls_acm_settings')) {
            $this->updateSettingsAction();
        }

        if ($this->isAction('export-acm-codes') && current_user_can('ls_acm')) {
            $this->exportCodesToCSVAction();
        }
    }

    /**
     * Register meta-box "Content access"
     * @since 3.4.0
     * @internal
     */
    public function addMetaBoxes()
    {
        $post = get_post();

        if ($post instanceof WP_Post && $post->post_type !== null && !in_array($post->post_type, ['revision', 'nav_menu_item'])) {
            add_meta_box('acm-box', __('Content access', 'ls'), [$this, 'metaBoxContent'], $post->post_type, 'side');
        }
    }

    /**
     * Meta-box "Access Code"
     *
     * @since 3.4.0
     * @internal
     *
     * @param WP_Post $post Current post
     */
    public function metaBoxContent(WP_Post $post)
    {
        $postId = (int) $post->ID;

        if (in_array($postId, $this->getExcludedPostsIds())) {
            echo __('This article is excluded from the list of possible AC-protected articles.', 'ls');
        } else {
            (new LS\Template(__DIR__ . '/views/backend'))
                ->assign('nonce', wp_create_nonce('acm-meta-box'))
                ->assign('groupsList', $this->helper->getGroupsList(true))
                ->assign('postGroupsIds', $this->helper->getGroupIdsByPostId($postId))
                ->assign('secureURL', $this->helper->getSecureURL($postId, true))
                ->render('admin-post-box.phtml');
        }
    }

    /**
     * Saving post - save data for the meta-box "Content access"
     *
     * @since 3.4.0
     * @internal
     *
     * @param int $postId Post Id
     */
    public function savePost($postId)
    {
        if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
            return;
        }

        if (!isset($_POST['acm_box_nonce']) || !wp_verify_nonce($_POST['acm_box_nonce'], 'acm-meta-box')) {
            return;
        }

        $groupIds = $this->getParam('acm_group_id', 'int_array', 'POST');
        $postId = (int) $postId;
        $post = get_post($postId);
        $postType = get_post_type_object($post->post_type);

        if (in_array($post->post_status, ['inherit', 'trash']) || !$postType || !current_user_can($postType->cap->edit_post, $postId)) {
            return;
        }

        $this->helper->updatePostConnections($postId, $groupIds);
    }

    /**
     * Retrieve URL to edit Access Code
     *
     * @since 3.4.0
     *
     * @param int $codeId Access Code Id
     * @param string $referer Referer
     * @return string URL
     */
    public static function codeEditLink($codeId, $referer = '')
    {
        $referer = empty($referer) ? $_SERVER['REQUEST_URI'] : $referer;
        $referer = urlencode($referer);

        return $codeId > 0 ? admin_url('admin.php?page=acmLoyaltySuite&view=code-details&codeId=' . $codeId . '&referer=' . $referer) : '';
    }

    /**
     * Retrieve URL to edit CA-Group
     *
     * @since 3.4.0
     *
     * @param int $groupId CA-Group Id
     * @param string $referer Referer
     * @return string URL
     */
    public static function contentAccessEditLink($groupId, $referer = '')
    {
        $referer = empty($referer) ? $_SERVER['REQUEST_URI'] : $referer;
        $referer = urlencode($referer);

        return $groupId > 0 ? admin_url('admin.php?page=acmLoyaltySuite&view=group-details&groupId=' . $groupId . '&referer=' . $referer) : '';
    }

    /**
     * Create content access tab on the consumer details page
     *
     * @since 3.4.0
     * @internal
     *
     * @param array $tabs A list of tabs on the consumer details page
     * @param int $consumerId Consumer Id
     * @return array Modified list of tabs on the consumer details page
     */
    public function consumerDetailsFilter($tabs, $consumerId)
    {
        if ($consumerId > 0) {

            $content = (new LS\Template(__DIR__ . '/views/backend'))
                ->assign('consumerConnections', $this->helper->getConsumerCodesConnections($consumerId))
                ->assign('consumerCodes', $this->helper->getConsumerAccessCodes($consumerId))
                ->assign('consumerGroupsConnections', $this->helper->getConsumerGroupsConnections($consumerId))
                ->assign('consumerGroups', $this->helper->getConsumerGroups($consumerId))
                ->assign('groupsList', $this->helper->getGroupsList(true))
                ->assign('consumerId', $consumerId)
                ->render('admin-consumer-codes.phtml', false);

            $tabs['acm'] = [__('Access Management', 'ls'), $content];
        }

        return $tabs;
    }

    /**
     * Add Access Codes & groups columns to the consumers overview table (LS Consumers -> Overview)
     *
     * @since 3.4.0
     * @internal
     *
     * @param array $tableData A list of table columns
     * @return array A list of table columns
     */
    public function consumersOverviewColumns($tableData)
    {
        $tableData['cols']['acmGroupId'] = __('CA-Group', 'ls');
        $tableData['filter_cols']['acmGroupId'] = ['acmGroupId', 'select', $this->helper->getGroupsList(true)];
        $tableData['displays']['acmGroupId'] = static function ($value, \LS\ConsumerInterface $consumer) {

            $value = is_array($value) ? $value : [];

            foreach (ACMHelper::getConsumerGroups($consumer->getConsumerId(), false, false) as $group) {
                $value[] = '<a href="' . acmAdminLoyaltySuite::contentAccessEditLink($group->getGroupId()) . '" ' . ($group->isActive() ? '' : 'style="color: red;"') . '>' . $group->getName() . '</a>';
            }

            return implode('<br />', $value);
        };

        $tableData['cols']['acmCode'] = __('Access Code', 'ls');
        $tableData['filter_cols']['acmCode'] = ['acmCode', 'string'];
        $tableData['displays']['acmCode'] = static function ($value, \LS\ConsumerInterface $consumer) {

            $value = is_array($value) ? $value : [];
            $consumerActualCodesIds = ACMHelper::getConsumerAccessCodesIds($consumer->getConsumerId(), true);

            foreach (ACMHelper::getConsumerAccessCodes($consumer->getConsumerId()) as $code) {
                $value[] = '<a href="' . acmAdminLoyaltySuite::codeEditLink($code->getCodeId()) . '" ' . (in_array($code->getCodeId(), $consumerActualCodesIds) ? '' : ' style="color: red;"') . '>' . esc_html($code->getCode()) . '</a>';
            }

            return implode('<br />', $value);
        };

        return $tableData;
    }

    /**
     * Company activated - activate all related access groups and Access Codes
     *
     * @since 3.4.0
     * @internal
     *
     * @param \LS\CompanyInterface $company Company
     */
    public function companyActivated(\LS\CompanyInterface $company)
    {
        $this->helper->activateAccessCodes(
            $this->helper->getAccessCodesByCompanyId($company->getCompanyId(), false)
        );
    }

    /**
     * Company deactivated - activate all related access groups and Access Codes
     *
     * @since 3.4.0
     * @internal
     *
     * @param \LS\CompanyInterface $company Company
     */
    public function companyDeactivated(\LS\CompanyInterface $company)
    {
        $this->helper->deactivateAccessCodes(
            $this->helper->getAccessCodesByCompanyId($company->getCompanyId())
        );
    }

    /**
     * Company deleted - delete all related access groups and Access Codes
     *
     * @since 3.4.0
     * @internal
     *
     * @param \LS\CompanyInterface $company Company
     * @param bool $deleteChildrenCompanies True to delete children
     */
    public function companyDeleted(\LS\CompanyInterface $company, $deleteChildrenCompanies)
    {
        foreach ($this->helper->getAccessCodesByCompanyId($company->getCompanyId(), $deleteChildrenCompanies) as $code) {
            $this->helper->deleteAccessCode($code->getCodeId());
        }
    }

    /**
     * Format message for consumer log
     *
     * @since 3.4.0
     * @internal
     *
     * @param object $log Log details
     * @return string Event text
     */
    public function consumerLog($log)
    {
        if ($log->messageType == 'access_code') {
            $code = $this->helper->getAccessCodeByCode($log->note);
            $editLink = $code ? $this->codeEditLink($code->getCodeId()) : '/';
            $codeName = empty($log->note) ? '{CODE}' : $log->note;
            $event = sprintf(__('Access Code %s has been connected', 'ls'), '<a href="' . esc_url($editLink) . '" target="_blank">' . esc_html($codeName) . '</a>');
        } elseif ($log->messageType == 'access_code_removed') {
            $code = $this->helper->getAccessCodeByCode($log->note);
            $editLink = $code ? $this->codeEditLink($code->getCodeId()) : '/';
            $codeName = empty($log->note) ? '{CODE}' : $log->note;
            $event = sprintf(__('Access Code %s has been disconnected', 'ls'), '<a href="' . esc_url($editLink) . '" target="_blank">' . esc_html($codeName) . '</a>');
        } elseif ($log->messageType == 'access_group') {
            $group = $this->helper->getGroup((int) $log->note);
            $editLink = $group ? $this->contentAccessEditLink($group->getGroupId()) : '/';
            $codeName = $group ? $group->getName() : '{GROUP}';
            $event = sprintf(__('CA-Group %s has been connected', 'ls'), '<a href="' . esc_url($editLink) . '" target="_blank">' . esc_html($codeName) . '</a>');
        } elseif ($log->messageType == 'access_group_removed') {
            $group = $this->helper->getGroup((int) $log->note);
            $editLink = $group ? $this->contentAccessEditLink($group->getGroupId()) : '/';
            $codeName = $group ? $group->getName() : '{GROUP}';
            $event = sprintf(__('CA-Group %s has been disconnected', 'ls'), '<a href="' . esc_url($editLink) . '" target="_blank">' . esc_html($codeName) . '</a>');
        } elseif ($log->messageType == 'company_assigned') {
            $company = LSCompaniesHelper::getCompany((int) $log->note);
            $companyName = $company ? $company->getCompanyName() : '{NAME}';
            $companyLink = $company ? companiesAdminLoyaltySuite::companyEditLink($company->getCompanyId()) : '#';
            $event = sprintf(__('Company %s assigned', 'ls'), '<a href="' . esc_url($companyLink) . '" target="_blank">' . ls_kses($companyName) . '</a>');
        } elseif ($log->messageType == 'prolongation') {
            $cc = $this->helper->getConsumerCodeConnection((int) $log->note);
            $editLink = $cc ? $this->codeEditLink($cc->getCodeId()) : '/';
            $code = $cc ? $this->helper->getAccessCode($cc->getCodeId()) : false;
            $codeName = $code ? $code->getCode() : '{CODE}';
            $endDate = $cc ? date('d.m.Y', $cc->getEndDate()) : '{END DATE}';
            $event = sprintf(__('Access Code %s prolonged to %s', 'ls'), '<a href="' . esc_url($editLink) . '" target="_blank">' . ls_kses($codeName) . '</a>', $endDate);
        } else {
            $event = '';
        }

        return $event;
    }

    /**
     * Action to create new group
     * @since 3.4.0
     */
    private function createGroupAction()
    {
        $data =
        $errors = [];

        foreach ($this->getGroupsStructure() as $fieldName => $field) {

            if (in_array($fieldName, ['groupId', 'createDate'])) {
                continue;
            }

            $field->setValue(
                $this->getParam($fieldName, $field->getType(), 'POST')
            );

            if (($error = $field->validate()) !== '') {
                $errors[] = $error;
            } else {
                $data[$fieldName] = $field->getDatabaseValue();
            }
        }

        if (empty($errors)) {

            $group = new ACMGroup($data);

            if ($this->helper->createGroup($group)) {
                $this->setAlert(__('New CA-Group has been added', 'ls'));
                LS()->logUserAction('ACM: CA-Group created (ID: ' . $group->getGroupId() . ')');
                $this->redirect(
                    $this->contentAccessEditLink($group->getGroupId(), $this->getReferer()) . '#acm-group-posts'
                );
            }

            $errors[] = __('Can not create CA-Group', 'ls');
        }

        foreach ($errors as $error) {
            $this->setError($error);
        }
    }

    /**
     * Action to update group
     * @since 3.4.0
     */
    private function updateGroupAction()
    {
        $data =
        $errors = [];

        $groupId = $this->getParam('groupId', 'id');
        $group = $this->helper->getGroup($groupId);

        if ($group) {

            foreach ($this->getGroupsStructure() as $fieldName => $field) {

                if (in_array($fieldName, ['groupId', 'createDate'])) {
                    continue;
                }

                $field->setValue(
                    $this->getParam($fieldName, $field->getType(), 'POST')
                );

                if (($error = $field->validate()) !== '') {
                    $errors[] = $error;
                }

                if (empty($error) && $fieldName == 'parentGroupId' && $field->getValue() == $groupId) {
                    $errors[] = __('Parent CA-Group can not be related to itself', 'ls');
                }

                if (empty($error)) {
                    $data[$fieldName] = $field->getDatabaseValue();
                }
            }

            if (empty($errors)) {

                $group->setFromArray($data);

                if ($this->helper->updateGroup($group)) {
                    $this->setAlert(__('CA-Group has been updated', 'ls'));
                    LS()->logUserAction('ACM: CA-Group updated (ID: ' . $groupId . ')');
                    $this->redirect(remove_query_arg('action'));
                }

                $errors[] = __('Can not update CA-Group', 'ls');
            }

        } else {
            $errors[] = __('Can not update CA-Group', 'ls');
        }

        foreach ($errors as $error) {
            $this->setError($error);
        }
    }

    /**
     * Action to remove selected groups
     * @since 3.4.0
     */
    private function deleteGroupsAction()
    {
        $ids = $this->getParam('groupId', 'int_array');
        if (!empty($ids)) {

            foreach ($ids as $groupId) {
                $this->helper->deleteGroup($groupId);
            }

            $this->setAlert(__('Selected records have been deleted', 'ls'));
            LS()->logUserAction('ACM: CA-Group removed (IDs: ' . implode(',', $ids) . ')', 'warning');
            $this->redirect(remove_query_arg(['action', 'action2', 'groupId']));
        }
    }

    /**
     * Action to duplicate selected groups
     * @since 3.4.0
     */
    public function duplicateGroupsAction()
    {
        $ids = $this->getParam('groupId', 'int_array');
        if (!empty($ids)) {

            foreach ($ids as $groupId) {
                $this->helper->duplicateGroupByGroupId($groupId);
            }

            $this->setAlert(__('Selected CA-Groups have been duplicated', 'ls'));
            LS()->logUserAction('ACM: CA-Group duplicated (IDs: ' . implode(',', $ids) . ')');
            $this->redirect(remove_query_arg(['action', 'action2', 'groupId']));
        }
    }

    /**
     * Action to connect post to a group
     * @since 3.4.0
     */
    private function connectPostToGroupAction()
    {
        $postId = $this->getParam('exclusivePostId', 'id');
        $groupId = $this->getParam('groupId', 'id');

        if ($this->helper->connectGroupToPost($groupId, $postId)) {
            $this->setAlert(__('Post has been added to the CA-Group', 'ls'));
            $this->redirect(remove_query_arg(['action', 'post_id', 'group_id']) . '#acm-group-posts');
        }
    }

    /**
     * Action to exclude post from a group
     * @since 3.4.0
     */
    private function excludedPostFromGroupAction()
    {
        $postId = $this->getParam('post_id', 'id');
        $groupId = $this->getParam('group_id', 'id');

        if ($this->helper->disconnectGroupFromPost($groupId, $postId)) {
            $this->setAlert(__('Post has been removed from the CA-Group', 'ls'));
            $this->redirect(remove_query_arg(['action', 'post_id', 'group_id']) . '#acm-group-posts');
        }
    }

    /**
     * Action to create new Access Code
     * @since 3.4.0
     */
    private function createCodeAction()
    {
        $data =
        $errors = [];

        foreach ($this->getAccessCodeStructureToAdd() as $fieldName => $field) {

            if (in_array($fieldName, ['codeId', 'hasLimit', 'hasLifetime', 'createDate'])) {
                continue;
            }

            $field->setValue(
                $this->getParam($fieldName, $field->getType(), 'POST')
            );

            if (($error = $field->validate()) !== '') {
                $errors[] = $error;
            } else {
                $data[$fieldName] = $field->getDatabaseValue();
            }
        }

        if (empty($errors)) {

            $groupIds = array_map('intval', $data['groupIds']);

            if (($codeId = $this->helper->createAccessCode(new ACMCode($data), $groupIds)) !== false) {
                $this->setAlert(__('New Access Code has been added', 'ls'));
                LS()->logUserAction('ACM: Access Code created (ID: ' . $codeId . ')');
                $this->redirect($this->codeEditLink($codeId, $this->getReferer()));
            }

            $errors[] = __('Can not create Access Code', 'ls');
        }

        foreach ($errors as $error) {
            $this->setError($error);
        }
    }

    /**
     * Action to generate new Access Codes
     * @since 3.4.0
     */
    private function generateCodesAction()
    {
        $data =
        $errors = [];

        foreach ($this->getAccessCodesGenerateStructure() as $fieldName => $field) {

            $field->setValue(
                $this->getParam($fieldName, $field->getType(), 'POST')
            );

            if (($error = $field->validate()) !== '') {
                $errors[] = $error;
            } else {
                $data[$fieldName] = $field->getDatabaseValue();
            }
        }

        if (empty($errors)) {

            $codes = [];
            $success = false;
            $created = 0;

            while (!$success) {

                $code = trim($data['codesPrefix']) . $this->randomString((int) $data['codesLength'], (bool) $data['numbers']);

                if (!empty($code) && !in_array($code, $codes) && !$this->helper->getAccessCodeByCode($code)) {
                    $codes[] = $code;
                    $success = count($codes) == $data['codesCount'];
                }
            }

            if ($success) {

                $groupIds = array_map('intval', $data['groupIds']);

                foreach ($codes as $code) {

                    $data['code'] = $code;

                    if ($this->helper->createAccessCode(new ACMCode($data), $groupIds)) {
                        $created++;
                    }
                }
            }

            if ($success && $created > 0) {
                $this->setAlert(sprintf(__('%d Access Codes have been created', 'ls'), $created));
                LS()->logUserAction(sprintf('ACM: %d Access Codes generated', $created));
                $this->redirect($this->getReferer());
            }

            $errors[] = __('Can not generate Access Codes', 'ls');
        }

        foreach ($errors as $error) {
            $this->setError($error);
        }
    }

    /**
     * Action to update Access Code
     * @since 3.4.0
     */
    private function updateCodeAction()
    {
        $data =
        $errors = [];

        $codeId = $this->getParam('codeId', 'id');
        $code = $this->helper->getAccessCode($codeId);

        if ($code) {

            foreach ($this->getAccessCodeStructureToEdit() as $fieldName => $field) {

                if (in_array($fieldName, ['codeId', 'createDate', 'endDate'])) {
                    continue;
                }

                $field->setValue(
                    $this->getParam($fieldName, $field->getType(), 'POST')
                );

                if (($error = $field->validate()) !== '') {
                    $errors[] = $error;
                } else {
                    $data[$fieldName] = $field->getDatabaseValue();
                }
            }

            if ($data['hasEP'] && empty($data['emailPattern'])) {
                $errors[] = __('Please enter email pattern', 'ls');
            }

            if (empty($errors)) {

                $code->setFromArray($data);

                $groupIds = array_map('intval', $data['groupIds']);

                if ($this->helper->updateAccessCode($code, $groupIds)) {
                    $this->setAlert(__('Access Code has been updated', 'ls'));
                    LS()->logUserAction('ACM: Access Code updated (ID: ' . $codeId . ')');
                    $this->redirect(remove_query_arg('action'));
                }

                $errors[] = __('Can not update Access Code', 'ls');
            }

        } else {
            $errors[] = __('Can not update Access Code', 'ls');
        }

        foreach ($errors as $error) {
            $this->setError($error);
        }
    }

    /**
     * Action to delete selected Access Codes
     * @since 3.4.0
     */
    private function deleteCodesAction()
    {
        $ids = $this->getParam('codeId', 'int_array');
        if (!empty($ids)) {

            foreach ($ids as $codeId) {
                $this->helper->deleteAccessCode($codeId);
            }

            $this->setAlert(__('Selected records have been deleted', 'ls'));
            LS()->logUserAction('ACM: Access Codes removed (IDs: ' . implode(',', $ids) . ')', 'warning');
            $this->redirect(remove_query_arg(['action', 'action2', 'codeId']));
        }
    }

    /**
     * Action to create the connection between the consumer and Access Code
     * @since 3.4.0
     */
    private function createCodeConnectionAction()
    {
        $consumerId = $this->getParam('consumerId', 'id');
        $codeValue = $this->getParam('code', 'text');
        $code = $this->helper->getAccessCodeByCode($codeValue);
        $consumer = $this->consumersHelper->getConsumer($consumerId);

        if (!$consumer) {
            $this->setError(__('Can not submit Access Code', 'ls'));
        } elseif (($error = $this->validateAccessCodeToSubmit($codeValue, $consumer->getEmail(), $consumerId)) != '') {
            $this->setError($error);
        } elseif (!$this->helper->connectConsumerToAccessCode($consumer, $code)) {
            $this->setError(__('Can not submit Access Code', 'ls'));
        } else {
            $this->setAlert(__('Access Code has been added to the consumer', 'ls'));
            LS()->logUserAction(sprintf('ACM: Access Code %d connected to the consumer %d', $code->getCodeId(), $consumerId));
        }

        $this->redirect($this->getReferer());
    }

    /**
     * Action to create the connection between the consumer and access group
     * @since 3.4.0
     */
    private function createGroupConnectionAction()
    {
        $consumerId = $this->getParam('consumerId', 'id');
        $groupId = $this->getParam('groupId', 'id');
        $consumer = $this->consumersHelper->getConsumer($consumerId);
        $group = $this->helper->getGroup($groupId);

        if (!$consumer) {
            $this->setError(__('Can not find consumer', 'ls'));
        } elseif (!$group) {
            $this->setError(__('Can not find CA-Group', 'ls'));
        } elseif ($this->helper->hasConsumerConnectionToGroup($consumerId, $groupId)) {
            $this->setError(__('Connection already available', 'ls'));
        } elseif (!$this->helper->connectConsumerToGroup($consumerId, $groupId)) {
            $this->setError(__('Can not connect consumer to the CA-Group', 'ls'));
        } else {
            $this->setAlert(__('CA-Group has been connected to the consumer', 'ls'));
            LS()->logUserAction(sprintf('ACM: CA-Group ID:%d connected to consumer ID:%d', $groupId, $consumerId));
        }

        $this->redirect($this->getReferer());
    }

    /**
     * Action to delete the connection between the consumer and Access Code
     * @since 3.4.0
     */
    private function deleteCodeConnectionAction()
    {
        $ccId = $this->getParam('ccId', 'id');
        $cc = $this->helper->getConsumerCodeConnection($ccId);

        if ($cc && $this->helper->deleteConsumerCodeConnection($cc)) {
            $this->setAlert(__('Access Code connection has been disconnected', 'ls'));
            LS()->logUserAction(sprintf('ACM: Access Code connection %d disconnected from the consumer %d', $cc->getId(), $cc->getConsumerId()));
        } else {
            $this->setError(__('Can not disconnect Access Code connection', 'ls'));
        }

        $this->redirect($this->getReferer());
    }

    /**
     * Send email on consumer code prolongation
     * @since 3.5.2
     * @param ACMConsumerCodeConnection $cc Consumer code
     */
    private function sendProlongationEmail(ACMConsumerCodeConnection $cc)
    {
        if (($code = $this->helper->getAccessCode($cc->getCodeId())) && $code->isActive()) {

            /** @var comCenterLoyaltySuite $comCenterModule */
            $comCenterModule = LS()->getModule('comCenter');
            $comCenterModule->triggerConditionalEmails('consumer_ac_prolongation', static function (LSConditionalEmail $ce) use ($code, $cc) {
                
                $ce
                    ->setConsumerExtraValue($cc->getConsumerId(), 'code', $code->getCode())
                    ->setConsumerExtraValue($cc->getConsumerId(), 'newEndDate', date_i18n(get_option('date_format'), $cc->getEndDate()));

                return $cc->getConsumerId();
            });
        }
    }

    /**
     * Prolong Access Code connection to consumer
     * @since 3.4.0
     */
    private function prolongCodeConnectionAction()
    {
        $success = false;
        $ccId = $this->getParam('ccId', 'id');
        $endDate = $this->getParam('endDate', 'date');
        $cc = $this->helper->getConsumerCodeConnection($ccId);

        if ($endDate > 0 && $cc) {

            $newCc = new ACMConsumerCodeConnection([
                'consumerId' => $cc->getConsumerId(),
                'codeId'     => $cc->getCodeId(),
                'enterDate'  => time(),
                'endDate'    => $endDate
            ]);

            $consumer = $this->consumersHelper->getConsumer($cc->getConsumerId());

            if ($consumer && ($ccId = $this->helper->addConsumerCodeConnection($newCc)) > 0) {
                $this->sendProlongationEmail($newCc);
                $this->consumersHelper->insertLog('prolongation', $cc->getConsumerId(), $ccId);
                LS()->logUserAction(sprintf('ACM: code prolongation (ID: %s)', $cc->getCodeId()));
                $success = true;
            }
        }

        if ($success) {
            $this->setAlert(__('Access Code connection has been prolonged', 'ls'));
            LS()->logUserAction(sprintf('ACM: Access Code %s prolonged for consumer %d', $cc->getCodeId(), $cc->getConsumerId()));
        } else {
            $this->setError(__('Can not prolong Access Code', 'ls'));
        }

        $this->redirect($this->getReferer());
    }

    /**
     * Action to delete the connection between the consumer and access group
     * @since 3.4.0
     */
    private function disconnectGroupFromConsumerAction()
    {
        $consumerId = $this->getParam('consumerId', 'id');
        $groupId = $this->getParam('groupId', 'id');

        if ($this->helper->disconnectConsumerFromGroup($consumerId, $groupId)) {
            $this->setAlert(__('CA-Group has been disconnected', 'ls'));
            LS()->logUserAction(sprintf('ACM: CA-Group ID:%d disconnected from consumer ID:%d', $groupId, $consumerId));
        } else {
            $this->setError(__('Can not disconnect CA-Group', 'ls'));
        }

        $this->redirect($this->getReferer());
    }

    /**
     * Action to activate Access Codes
     * @since 3.4.0
     */
    private function activateCodesAction()
    {
        $ids = $this->getParam('codeId', 'int_array');
        $codes = $this->helper->getAccessCodesByIds($ids);
        $success = $this->helper->activateAccessCodes($codes);

        if ($success) {
            $this->setAlert(sprintf(__('%d Access Codes have been activated', 'ls'), count($codes)));
            LS()->logUserAction('ACM: Access Codes activated (IDs: ' . implode(',', $ids) . ')');
        } else {
            $this->setError(__('Can not activate Access Codes', 'ls'));
        }

        $this->redirect(remove_query_arg(['action', 'action2', 'codeId']));
    }

    /**
     * Action to deactivate Access Codes
     * @since 3.4.0
     */
    private function deactivateCodesAction()
    {
        $ids = $this->getParam('codeId', 'int_array');
        $codes = $this->helper->getAccessCodesByIds($ids, true);
        $success = $this->helper->deactivateAccessCodes($codes);

        if ($success) {
            $this->setAlert(sprintf(__('%d Access Codes have been deactivated', 'ls'), count($codes)));
            LS()->logUserAction('ACM: Access Codes deactivated (IDs: ' . implode(',', $ids) . ')');
        } else {
            $this->setError(__('Can not deactivate Access Codes', 'ls'));
        }

        $this->redirect(remove_query_arg(['action', 'action2', 'codeId']));
    }

    /**
     * Prolong Access Codes [step 1]
     * @since 3.4.0
     */
    private function prolongCodes1Action()
    {
        $companyId = $this->getParam('companyId', 'id');
        if (empty($companyId)) {
            $this->setError(__('You must select a company', 'ls'));
            return;
        }

        $this->redirect(add_query_arg([
            'view'      => 'acm-prolong-codes-2',
            'companyId' => $this->getParam('companyId', 'id')
        ], remove_query_arg('action')));
    }

    /**
     * Prolong Access Codes [step 2]
     * @since 3.4.0
     */
    private function prolongCodes2Action()
    {
        if (isset($_REQUEST['prev'])) {
            $this->redirect(add_query_arg('view', 'acm-prolong-codes-1', remove_query_arg('action')));
        }

        $codes = $this->getParam('codes', 'text');

        if (empty($codes)) {
            $this->setError(__('You must select at least one Access Code to prolong', 'ls'));
            return;
        }

        $this->redirect(add_query_arg([
            'view'      => 'acm-prolong-codes-3',
            'companyId' => $this->getParam('companyId', 'id'),
            'codes'     => $codes
        ], remove_query_arg('action')));
    }

    /**
     * Prolong Access Code [step 3]
     * @since 3.4.0
     */
    private function prolongCodes3Action()
    {
        if (isset($_REQUEST['prev'])) {
            $this->redirect(add_query_arg('view', 'acm-prolong-codes-2', remove_query_arg(['action'])));
        }

        $endDate = $this->getParam('endDate', 'date');

        if (empty($endDate)) {
            $this->setError(__('You must select prolongation date', 'ls'));
            return;
        }

        $codes = $this->getParam('codes', 'text');
        $activate = $this->getParam('activate', 'checkbox');
        $sendProlongation = $this->getParam('email', 'checkbox');

        if (!empty($codes)) {

            $codes = wp_parse_id_list($codes);
            $total =
            $complete = 0;

            foreach ($codes as $codeId) {

                $code = $this->helper->getAccessCode($codeId);

                $this->helper->updateAccessCode(
                    $code->setEndDate($endDate)
                );

                foreach ($this->helper->getLatestConsumerConnectionsToAccessCode($codeId) as $cc) {
                    if ($endDate > $cc->getEndDate()) {

                        $newCc = new ACMConsumerCodeConnection([
                            'consumerId' => $cc->getConsumerId(),
                            'codeId'     => $cc->getCodeId(),
                            'enterDate'  => time(),
                            'endDate'    => $endDate
                        ]);

                        if (($ccId = $this->helper->addConsumerCodeConnection($newCc)) > 0) {

                            $consumer = $this->consumersHelper->getConsumer($cc->getConsumerId());

                            if ($consumer) {

                                if ($activate) {
                                    $this->consumersHelper->activateConsumer($consumer);
                                }

                                if ($sendProlongation) {
                                    $this->sendProlongationEmail($newCc);
                                }

                                $this->consumersHelper->insertLog('prolongation', $cc->getConsumerId(), $ccId);
                            }

                            ++$complete;
                        }
                    }

                    ++$total;
                }
            }

            if ($total === $complete) {
                $this->setAlert(sprintf(__('%d consumer(s) prolonged', 'ls'), $complete));
            } elseif ($complete === 0) {
                $this->setNotification(__('None of consumers prolonged', 'ls'));
            } else {
                $this->setNotification(sprintf(__('%d of %d consumer(s) prolonged. Some of the consumer codes can not be prolonged because they have later end dates than the prolongation date', 'ls'), $complete, $total));
            }

            if ($complete > 0) {
                LS()->logUserAction(sprintf('ACM: code(s) prolongation (IDs: %s)', implode(', ', $codes)));
            }

            $this->redirect($this->getReferer());
        }

        $this->setError(__('Can not prolong code(s)', 'ls'));
    }

    /**
     * Export Access Codes to the CSV file
     * @since 3.4.0
     */
    private function exportCodesToCSVAction()
    {
        $startTime = microtime(true);

        include_once __DIR__ . '/tables/codes.php';

        $options = (new acmCodesTable())->get_table_options();
        $limitCodes = 100;
        $groupsList = $this->helper->getGroupsList();

        $codes = $this->helper->paginateAccessCodes($limitCodes, 1, $options['orderBy'], $options['order'], $options['searchTerm'], $options['filterTerms']);
        if ($codes['totalCount'] > 0) {

            header("Content-type: text/csv");
            header("Cache-Control: no-store, no-cache");
            header('Content-Disposition: attachment; filename="AC_' . date_i18n('d.m.Y') . '.csv"');
            $fp = fopen("php://output", 'w');
            fprintf($fp, chr(0xEF) . chr(0xBB) . chr(0xBF)); // UTF-8 BOM

            $header = [
                'ID',
                __('Access Code', 'ls'),
                __('CA-Group', 'ls'),
                __('Limit', 'ls'),
                __('Used', 'ls'),
                __('Lifetime', 'ls'),
                __('Active', 'ls'),
                __('Note', 'ls'),
                __('Start date', 'ls'),
                __('End date', 'ls'),
                __('Created', 'ls')
            ];

            fputcsv($fp, $header);

            for ($i = 1; $i <= $codes['totalPages']; $i++) {

                if ($i > 1) {
                    $codes = $this->helper->paginateAccessCodes($limitCodes, $i, $options['orderBy'], $options['order'], $options['searchTerm'], $options['filterTerms']);
                }

                /** @var ACMCode $code */
                foreach ($codes['rows'] as $code) {

                    $groups = array_map(static function (int $groupId) use ($groupsList) {
                        return $groupsList[$groupId] ?? '';
                    }, $this->helper->getAccessCodeGroupIds($code->getCodeId()));

                    fputcsv($fp, [
                        $code->getCodeId(),
                        $code->getCode(),
                        implode(', ', $groups),
                        $code->getLimit(),
                        $code->getUsages(),
                        $code->getLifetime(),
                        (int) $code->isActive(),
                        $code->getNote(),
                        date('Y-m-d', (int) $code->getStartDate() + TIMEZONE_DIFF),
                        date('Y-m-d', (int) $code->getEndDate() + TIMEZONE_DIFF),
                        date('Y-m-d H:i:s', (int) $code->getCreateDate() + TIMEZONE_DIFF)
                    ]);
                }

                if ((microtime(true) - $startTime) > 500) {
                    break;
                }
            }

            fclose($fp);
            exit;
        }
    }

    /**
     * Action to save module settings
     * @since 3.4.0
     */
    private function updateSettingsAction()
    {
        $options = [
            'acRequired'               => $this->getParam('acRequired', 'checkbox'),
            'redirectConsumer'         => $this->getParam('redirectConsumer', 'text'),
            'redirectConsumerPage'     => $this->getParam('redirectConsumerPage', 'id'),
            'wrongCodeErrorMsg'        => $this->getParam('wrongCodeErrorMsg', 'html'),
            'wrongCodePatternErrorMsg' => $this->getParam('wrongCodePatternErrorMsg', 'html'),
            'inactiveCodeErrorMsg'     => $this->getParam('inactiveCodeErrorMsg', 'text'),
            'codeUsedErrorMsg'         => $this->getParam('codeUsedErrorMsg', 'text'),
            'codeNotStartedErrorMsg'   => $this->getParam('codeNotStartedErrorMsg', 'text'),
            'codeIsEndedErrorMsg'      => $this->getParam('codeIsEndedErrorMsg', 'text'),
            'noAccessErrorMsg'         => $this->getParam('noAccessErrorMsg', 'text'),
            'privateAreaMessage'       => $this->getParam('privateAreaMessage', 'html'),
            'deactivationCheck'        => $this->getParam('deactivationCheck', 'checkbox')
        ];

        if ($options['redirectConsumer'] != 'custom') {
            $options['redirectConsumerPage'] = 0;
        }

        if (!$this->hasErrors()) {

            $this->appendOptions($options);

            LS()->logUserAction('ACM: settings updated');
            $this->setAlert(__('Settings have been saved', 'ls'));
            $this->redirect(remove_query_arg('action'));
        }
    }

    /**
     * Daily CRON Job functionality
     * @since 3.4.0
     * @internal
     */
    public function lsCronDaily()
    {
        if ($this->lock('acm')) {

            $this->deactivationCheck();
            $this->deleteRelatedConsumersCheck();

            $this->unlock('acm');
        }
    }

    /**
     * Deactivate consumers without valid Access Codes
     * @since 3.4.0
     */
    private function deactivationCheck()
    {
        if ($this->getOption('deactivationCheck')) {
            foreach ($this->helper->getConsumersWithoutActiveCodes() as $consumerId) {

                $consumer = $this->consumersHelper->getConsumer($consumerId);

                if ($consumer) {
                    $consumer->setActive(false);
                    $this->consumersHelper->updateConsumer($consumer);
                }
            }
        }
    }

    /**
     * Delete inactive consumers related to expired Access Code
     * @since 3.4.0
     */
    private function deleteRelatedConsumersCheck()
    {
        foreach (array_keys($this->helper->getDRCConsumersToRemove()) as $consumerId) {
            $consumer = \LS\ConsumersHelper::getConsumer($consumerId);
            if ($consumer) {
                $this->consumersModule->quitConsumer($consumer);
            }
        }
    }

    /**
     * Add column "CA-Group" to the posts overview page
     *
     * @since 3.4.0
     * @internal
     *
     * @param array $columns A list of table columns
     * @return array A list of table columns
     */
    public function addColumn($columns)
    {
        $columns['acmGroups'] = __('CA-Group', 'ls');

        return $columns;
    }

    /**
     * Fill out the CA-Group on the posts overview page
     *
     * @since 3.4.0
     * @internal
     *
     * @param string $colName Column name
     * @param int $postId Post Id
     */
    public function addRowForColumn($colName, $postId)
    {
        if ($colName == 'acmGroups' && $postId > 0) {

            $result = [];

            foreach ($this->helper->getGroupsByPostId($postId) as $group) {
                $result[] = '<a href="' . $this->contentAccessEditLink($group->getGroupId()) . '" ' . ($group->isActive() ? '' : 'style="color: red;"') . '>' . $group->getName() . '</a>';
            }

            echo empty($result) ? ' ' : implode(', ', $result);
        }
    }

    /**
     * Change Access Code note
     *
     * @since 3.4.0
     * @ajax
     * @internal
     */
    public function acmChangeNote()
    {
        if (current_user_can('ls_acm_manage')) {

            $code = $this->helper->getAccessCode(
                $this->getParam('codeId', 'id')
            );

            if ($code) {
                $this->helper->updateAccessCode(
                    $code->setNote(
                        $this->getParam('note', 'text')
                    )
                );
            }
        }

        exit;
    }

    /**
     * Send out ac-activities figures
     *
     * @ajax
     * @since 3.4.0
     * @internal
     */
    public function acmActivities()
    {
        if (current_user_can('ls_acm')) {

            $groupId = $this->getParam('groupId', 'id');
            $startDate = $this->getParam('startDate', 'date');
            $endDate = $this->getParam('endDate', 'date');
            $columns = $this->getParam('columns', 'array');

            wp_send_json($this->helper->getStats($groupId, $startDate, $endDate, $columns));
        }

        exit;
    }
}