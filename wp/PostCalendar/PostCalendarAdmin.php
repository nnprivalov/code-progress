<?php
namespace LS;

class PostCalendarAdmin extends PostCalendar
{

    public static function getName()
    {
        return get_parent_class();
    }

    public function getDescription()
    {
        return __( 'Calendar which show all the custom post types and shows the planned dates.', 'ls' );
    }

    public function getModuleSubPages()
    {
        return [
            (object)[
                'href' => 'admin.php?page=' . $this->getParentName() . '#ls-settings/tab-pos-calendar',
                'label' => __( 'Post Calendar', 'ls' ),
                'referer' => false
            ]
        ];
    }

    public function load()
    {
        parent::load();
        add_action('wp_head', [$this, 'printScripts']);
        add_action('ls_fitco_update_settings', [$this, 'settingsUpdate']);
        add_action('ls_fitco_settings_tab', [$this, 'settingsTab']);
        add_action('ls_fitco_settings_content', [$this, 'settingsContent']);
        add_action('admin_enqueue_scripts', [$this, 'printScripts']);
        add_action( 'admin_footer', [$this ,'post_caleandar_dashboard_widget'] );
    }

    public function post_caleandar_dashboard_widget() {
        if ( get_current_screen()->base !== 'dashboard' ) {
            return;
        }

        $post_types = [
            'recipe',
            'partner',
            'i-box-tip',
            'portfolio',
            '1x1',
            'health-tipp',
            'fitco-news',
            'timeline',
            'live-stream',
            'course',
            'course-lesson',
            'post',
            'page',
            'activity-tracking',
            'b2b',
            'company-channel',
            'fit-tipps'
        ];

        $posts = get_posts([
            'post_type' => $post_types,
            'numberposts'       => -1,
        ]);
        ?>
            <div id="caleandar" class="welcome-panel">
            </div>

        <script>
            jQuery(document).ready(function($) {
                jQuery('#welcome-panel').after(jQuery('#caleandar').show());

                var events = [
                    <?php
                    foreach($posts as $key ):
                    $date = $key->post_date;
                    $timestamp = strtotime($date);
                    $year = date('Y', $timestamp);
                    $month = date('m', $timestamp);
                    $day = date('d', $timestamp);
                    /** @var \tippDerWocheLoyaltySuite $TWDModule */
                    $TWDModule = LS()->getModule('tippDerWoche');
                    if($key->post_type === 'post'){
                    $TWD = $TWDModule::getTDWDate($key->ID);

                    $TWDDate = date("Y-m-d H:i:s", $TWD);
                    }


                    ?>
                    {
                        'Date': new Date(<?=$year;?>, <?=$month;?>, <?=$day?>),
                        'Title': <?='"' . addslashes(Library::substr($key->post_title, 0, 50, true))  . '"';?>,
                        "DatePost": <?='"' . $key->post_date . '"';?>,
                        'Link': <?='"' . $key->guid . '"';?>,
                        'PostType': <?='"' . $key->post_type . '"';?>,
                        <?php if($key->post_type === 'post'):?>
                        'TDWDate': <?='"' . $TWDDate . '"';?>,
                        <?php endif; ?>
                    },
                    <?php
                    endforeach;
                    ?>
                ];
                var settings = {};
                var element = document.getElementById('caleandar');
                caleandar(element, events, settings);
            });

        </script>

    <?php }


    public function printScripts()
    {
        wp_enqueue_style('post-calendar-css', $this->getUrl2Module() . 'css/style.css', [], $this->getVersion());
        wp_enqueue_script('post-calendar-js', $this->getUrl2Module() . 'js/caleandar.js', ['jquery'], $this->getVersion(), false);
    }

    public function settingsTab()
    {
        ?>
        <li class="ls-tab"><a href="#ls-settings/tab-post-calendar"><?= $this->getTitle() ?></a></li><?php
    }


    public function settingsContent()
    {
        $post_types = get_post_types(array());
        $all_post = [];
        foreach ($post_types as $type) {
            $post = get_posts(['post_type' => $type]);
            $all_post[$type] = $post;
        }
        ?>

        <div id="tab-post-calendar" class="hide ls-tab-content">
            <div id="caleandar">
            </div>
            <div class="day-info"></div>
        </div>
        <script>
            jQuery(document).ready(function($) {
                var events = [
                    <?php
                    foreach($all_post as $type):
                    foreach($type as $key ):
                    $date = $key->post_date;
                    $timestamp = strtotime($date);
                    $year = date('Y', $timestamp);
                    $month = date('m', $timestamp);
                    $day = date('d', $timestamp);
                    /** @var \tippDerWocheLoyaltySuite $TWDModule */
                    $TWDModule = LS()->getModule('tippDerWoche');
                    $TWD = $TWDModule::getTDWDate($key->ID);

                    $TWDDate = date("Y-m-d H:i:s", $TWD);

                    ?>
                    {
                        'Date': new Date(<?=$year;?>, <?=$month;?>, <?=$day?>),
                        'Title': <?='"' . $key->post_name . '"';?>,
                        'Link': <?='"' . $key->guid . '"';?>,
                        'PostType': <?='"' . $key->post_type . '"';?>,
                        <?php if($TWD !== 0):?>
                        'TDWDate': <?='"' . $TWDDate . '"';?>,
                        <?php endif; ?>
                    },
                    <?php
                    endforeach;
                    endforeach;
                    ?>
                ];
                var settings = {};
                var element = document.getElementById('caleandar');
                caleandar(element, events, settings);
            });
        </script>
        <?php
    }

}