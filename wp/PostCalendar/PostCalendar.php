<?php
namespace LS;

class PostCalendar extends Module
{
    /**
     * @return string
     */
    public function getVersion()
    {
        return '1.0.0';
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return __( 'Calendar for Posts', 'ls' );
    }

    public function load()
    {
        parent::load();
    }

    public function printScripts()
    {

        wp_enqueue_style('fitco-calendar', $this->getUrl2Module() . 'calendar.css', [], $this->getVersion());
        wp_enqueue_script('fitco-calendar', $this->getUrl2Module() . 'calendar.js', ['jquery'], $this->getVersion(), true);


    }

    public function getModuleSubPages()
    {
        return [
            (object) [
                'href'    => 'admin.php?page=' . $this->getParentName() . '#ls-settings/tab-post-calendar',
                'label'   => 'Test',
                'referer' => false
            ]
        ];
    }







}