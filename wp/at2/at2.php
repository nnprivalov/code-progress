<?php
/**
 * @module at2LoyaltySuite
 * @title Activity Tracking
 * @description Manage Loyalty Suite challenges & activities
 * @version 2.16.6
 * @priority 11
 * @depends consumersLoyaltySuite 3.4.1
 */
class at2LoyaltySuite extends LS\Module
{
    /**
     * @since 2.12.2
     * @return string
     */
    public function getVersion()
    {
        return '2.16.6';
    }

    /**
     * @since 2.12.2
     * @return array
     */
    public function setDepends()
    {
        return [
            'consumersLoyaltySuite' => '3.4.1'
        ];
    }

    /**
     * @since 2.9.1
     * @return string Module title
     */
    public function getTitle()
    {
        return __('Activity Tracking', 'ls');
    }

    /**
     * @since 2.9.1
     * @return string
     */
    public function getDescription()
    {
        return __('Control your consumers activity', 'ls');
    }

    /**
     * @since 2.12.2
     * @return string
     */
    public function getCapability()
    {
        return 'ls_at';
    }

    /**
     * @since 2.12.0
     * @return array
     */
    public function getDefaultOptions()
    {
        return [
            'acp'        => true,
            'atTypeSlug' => 'activity-tracking'
        ];
    }

    /**
     * @since 2.0
     * @internal
     */
    public function load()
    {
        require_once __DIR__ . '/model.php';
        require_once __DIR__ . '/ATAction.php';
        require_once __DIR__ . '/ATActions.php';

        // init hooks for plugin
        add_action('init', [$this, 'initEarly'], 1);
        add_action('init', [$this, 'init']);
        add_action('wp_enqueue_scripts', [$this, 'printScripts']);
        add_filter('wp_footer', [$this, 'wpFooter']);
        add_action('wp_ajax_atChallengeRead', [$this, 'atChallengeReadAjax']);
        add_action('wp_ajax_nopriv_atChallengeRead', [$this, 'atChallengeReadAjax']);
        add_action('wp_ajax_atTrackingCode', [$this, 'atTrackingCodeAjax']);
        add_action('wp_ajax_nopriv_atTrackingCode', [$this, 'atTrackingCodeAjax']);
        add_action('wp_ajax_atChallengeSubmit', [$this, 'atChallengeSubmitAjax']);
        add_action('wp_ajax_nopriv_atChallengeSubmit', [$this, 'atChallengeSubmitAjax']);

        // Consumers
        add_action('ls_consumer_deleted', [$this, 'consumerDeleted']);
        add_filter('ls_my_account_block', [$this, 'myAccountBlockFilter']);

        // Com.Center
        add_filter('ls_cc_init', [$this, 'comCenterInit']);
        add_action('ls_at_start_challenge', [$this, 'comCenterStartChallenge'], 10, 2);

        // hook activities
        add_action('wp', [$this, 'activityTrackingCode']);
        add_action("gform_after_submission", [$this, 'gform_after_submission'], 10, 2);
        add_action('wp_insert_comment', [$this, 'wp_insert_comment']);
        add_action('ls_at_start_challenge', [$this, 'consumerStartedChallenge'], 99, 2);
    }

    /**
     * @since 2.2
     * @internal
     */
    public function initShortcodes()
    {
        add_shortcode('challenge', [$this, 'challengeShortcode']);
        add_shortcode('challenge-title', [$this, 'challengeTitleShortcode']);
        add_shortcode('challenge-period', [$this, 'challengePeriodShortcode']);
        add_shortcode('start-challenge', [$this, 'startChallengeShortcode']);
        add_shortcode('challenge-progress', [$this, 'challengeProgressShortcode']);
        add_shortcode('challenge-progress-bar', [$this, 'challengeProgressBarShortcode']);
        add_shortcode('challenge-days-left', [$this, 'challengeDaysLeftShortcode']);
        add_shortcode('challenge-participated-days', [$this, 'challengeParticipatedDaysShortcode']);
        add_shortcode('challenge-total-activities-count', [$this, 'challengeTotalActivitiesCountShortcode']);
        add_shortcode('challenge-passed-activities-count', [$this, 'challengePassedActivitiesCountShortcode']);
        add_shortcode('challenge-submit', [$this, 'challengeSubmitShortcode']);
        add_shortcode('challenge-goal-message', [$this, 'challengeGoalMessageShortcode']);
        add_shortcode('challenge-message-1', [$this, 'challengeMessageShortcode']);
        add_shortcode('challenge-message-2', [$this, 'challengeMessageShortcode']);
        add_shortcode('tracking-code-form', [$this, 'trackingCodeFormShortcode']);
        add_shortcode('tracking-code-trigger', [$this, 'trackingCodeActivityShortcode']);
        add_shortcode('challenges-overview', [$this, 'challengesOverviewShortcode']);
    }

    /**
     * Start challenge by consumer
     *
     * @since 2.0
     *
     * @param int $challengeId Challenge ID
     * @param int $consumerId Consumer ID
     * @return string Empty on success or error message on failure
     */
    public static function startChallenge(int $challengeId, int $consumerId): string
    {
        $error = '';

        if($consumerId < 1) {
            $error = __('Please authorize to start the challenge', 'ls');
        } elseif(!AT2Model::isChallengeActive($challengeId) || !$challenge = AT2Model::getChallenge($challengeId)) {
            $error = __('Can not start challenge: inactive challenge', 'ls');
        } elseif(strtotime($challenge->dateStart) > strtotime('TODAY', current_time('timestamp'))) {
            $error = sprintf(__('Can not start challenge: challenge starts on %s', 'ls'), date('d.m.Y', strtotime($challenge->dateStart)));
        } elseif(strtotime($challenge->dateEnd) < strtotime('TODAY', current_time('timestamp'))) {
            $error = sprintf(__('Can not start challenge: challenge was finished on %s', 'ls'), date('d.m.Y', strtotime($challenge->dateEnd)));
        } elseif(AT2Model::isChallengeStartedByConsumer($challengeId, $consumerId)) {
            $error = __('Can not start challenge: you have already started this challenge', 'ls');
        } elseif(!AT2Model::startChallenge($challengeId, $consumerId)) {
            $error = __('Can not start challenge. Please try again later.', 'ls');
        }

        return $error;
    }

    /**
     * Early module initialization
     * @internal
     * @since 2.9.6
     */
    public function initEarly()
    {
        $this->initDefaultActions();
    }

    /**
     * Process frontend actions and requests
     * @since 1.0
     * @internal
     */
    public function init()
    {
        $this->initPostType();

        if(\LS\Library::isFrontendRequest() && $this->hasAction()) {

            if($this->isAction('start-challenge')) {

                $consumerId = consumersLoyaltySuite::getAuthorizedConsumerId();
                $challengeId = $this->getParam('challengeId', 'id');
                $error = $this->startChallenge($challengeId, $consumerId);

                if($error) {
                    $this->setError($error);
                } else {
                    $this->setAlert(__('Participation in the challenge started', 'ls'));
                }

                \LS\Library::redirect(remove_query_arg(['action', 'challengeId']));
            }

            if($this->isAction('stop-challenge')) {

                AT2Model::disconnectedChallengeFromConsumer(
                    $this->getParam('challengeId', 'id'),
                    consumersLoyaltySuite::getAuthorizedConsumerId()
                );

                $this->setAlert(__('Participation in the challenge stopped', 'ls'));
                \LS\Library::redirect(remove_query_arg(['action', 'challengeId']));
            }
        }
    }

    /**
     * Init AT post type
     * @since 2.11.0
     */
    private function initPostType()
    {
        register_post_type('activity-tracking', [
            'labels'              => [
                'name'               => __('AT Challenges', 'ls'),
                'singular_name'      => __('AT Challenge', 'ls'),
                'add_new_item'       => __('New AT Challenge add', 'ls'),
                'edit_item'          => __('AT Challenge edit', 'ls'),
                'new_item'           => __('New AT Challenge', 'ls'),
                'all_items'          => __('All AT Challenges', 'ls'),
                'view_item'          => __('AT Challenge view', 'ls'),
                'search_items'       => __('AT Challenges search', 'ls'),
                'not_found'          => __('No AT Challenges found', 'ls'),
                'not_found_in_trash' => __('No AT Challenges in trash found', 'ls'),
                'menu_name'          => __('AT Challenges', 'ls')
            ],
            'public'              => true,
            'publicly_queryable'  => true,
            'show_ui'             => true,
            'show_in_menu'        => true,
            'query_var'           => true,
            'capability_type'     => 'post',
            'hierarchical'        => false,
            'rewrite'             => ['slug' => $this->getOption('atTypeSlug')],
            'exclude_from_search' => false,
            'supports'            => ['title', 'subtitles', 'editor', 'author', 'thumbnail', 'excerpt', 'revisions', 'custom-fields'],
            'taxonomies'          => ['post_tag', 'category']
        ]);
    }

    /**
     * Load module scripts
     * @since 2.0
     * @internal
     */
    public function printScripts()
    {
        wp_enqueue_style('ls-at', $this->getUrl2Module() . 'css/activity-tracking.css', [], $this->getVersion());
        wp_enqueue_script('ls-at', $this->getUrl2Module() . 'js/at2.js', ['jquery'], $this->getVersion(), true);
    }

    /**
     * Retrieve a list of challenge shortcodes accessible via emails
     * @since 2.1
     * @return string[] A list of challenge fields
     */
    public static function getShortcodesForChallengeEmail()
    {
        return [
            'challenge-title',
            'challenge-period',
            'challenge-goal-message',
            'challenge-message-1',
            'challenge-message-2',
            'challenge-progress',
            'challenge-days-left',
            'challenge-total-activities-count',
            'challenge-passed-activities-count',
            'challenge-participated-days'
        ];
    }

    /**
     * Append challenge ID to every challenge shortcode that starts with 'challenge-' for emails
     *
     * @since 2.1
     * @internal
     *
     * @param int $challengeId Challenge ID
     * @param string $content Email body
     * @param int $consumerId Consumer ID
     * @return string Modified email body
     */
    private static function appendChallengeEmailShortcodes($challengeId, $content, $consumerId = 0)
    {
        $result = $content;

        foreach(self::getShortcodesForChallengeEmail() as $shortcode) {
            if($consumerId > 0) {
                $result = str_replace('[' . $shortcode . ']', '[' . $shortcode . ' id=' . $challengeId . ' consumer_id=' . $consumerId . ']', $result);
            } else {
                $result = str_replace('[' . $shortcode . ']', '[' . $shortcode . ' id=' . $challengeId . ']', $result);
            }
        }

        return empty($result) ? $content : $result;
    }

    /**
     * Retrieve challenge from available attributes (as array of GET/POST elements or shortcode attributes)
     *
     * @since 2.3
     *
     * @param array $atts Attributes (possible attributes: id)
     * @return false|object Challenge or false if can't retrieve it from the specified attributes
     */
    public static function getChallengeFromAttributes($atts)
    {
        if(isset($atts['id']) && $atts['id'] > 0) {
            return AT2Model::getChallenge((int) $atts['id']);
        }

        if(isset($atts['challenge_id']) && $atts['challenge_id'] > 0) {
            return AT2Model::getChallenge((int) $atts['challenge_id']);
        }

        return false;
    }

    /**
     * Shortcode [challenge]
     * Display complete challenge information and activities
     *
     * @since 2.0
     * @internal
     *
     * @param array $atts Attributes ('id' - challenge ID)
     * @return string HTML-form with the challenge
     */
    public function challengeShortcode($atts)
    {
        $consumerId = consumersLoyaltySuite::getAuthorizedConsumerId();
        if($consumerId < 1) {
            return current_user_can('edit_users') ? 'Admin warning: only authorized consumers can use the challenge' : '';
        }

        $challenge = self::getChallengeFromAttributes($atts);
        if(!$challenge) {
            return current_user_can('edit_users') ? 'Admin warning: can not find the challenge' : '';
        }

        if(!$challenge->active) {
            return '<div class="ls-at-challenge">' . __('Currently, there is no active challenge', 'ls') . '</div>';
        }

        $started = AT2Model::isChallengeStartedByConsumer($challenge->challengeId, $consumerId);

        return (new LS\Template(__DIR__ . '/views/frontend'))
            ->assign('challenge', $challenge)
            ->assign('url', $this->getUrl2Module())
            ->assign('daysLeft', self::getChallengeDaysLeft($challenge))
            ->assign('period', self::getChallengePeriod($challenge))
            ->assign('challengeStartedByConsumer', $started)
            ->assign('totalActivities', AT2Model::getTotalActivitiesInChallenge($challenge->challengeId))
            ->assign('openActivities', $started ? AT2Model::getOpenActivitiesCount($challenge->challengeId, $consumerId) : 0)
            ->assign('passedActivities', $started ? AT2Model::getPassedActivitiesCount($challenge->challengeId, $consumerId) : 0)
            ->assign('activities', $started ? AT2Model::getConsumerActivitiesDetails($consumerId, $challenge->challengeId) : [])
            ->assign('goalMessage', $started ? AT2Model::getConsumerGoalMessageInChallenge($challenge->challengeId, $consumerId) : '')
            ->render('challenge-page.phtml', false);
    }

    /**
     * Shortcode [challenge-title]
     * Display challenge title
     *
     * @since 2.0
     * @internal
     *
     * @param array $atts Attributes ('id' - challenge ID)
     * @return string Challenge title
     */
    public function challengeTitleShortcode($atts)
    {
        $challenge = self::getChallengeFromAttributes($atts);
        if(!$challenge) {
            return current_user_can('edit_users') ? 'Admin warning: can not find the challenge' : '';
        }

        return $challenge->title;
    }

    /**
     * Retrieve challenge period
     *
     * @since 2.14.0
     *
     * @param object $challenge Challenge
     * @return string Period (text)
     */
    public static function getChallengePeriod($challenge)
    {
        return $challenge ? date('d.m.Y', strtotime($challenge->dateStart)) . ' - ' . date('d.m.Y', strtotime($challenge->dateEnd)) : '';
    }

    /**
     * Shortcode [challenge-period]
     * Display period of challenge activity (start/end)
     *
     * @since 2.0
     * @internal
     *
     * @param array $atts Attributes ('id' - challenge ID)
     * @return string HTML-block with challenge activity info
     */
    public function challengePeriodShortcode($atts)
    {
        $challenge = self::getChallengeFromAttributes($atts);
        if(!$challenge) {
            return current_user_can('edit_users') ? 'Admin warning: can not find the challenge' : '';
        }

        return self::getChallengePeriod($challenge);
    }

    /**
     * Shortcode [start-challenge]
     * Display a link to start challenge by consumer
     *
     * @since 2.0
     * @internal
     *
     * @param array $atts Attributes ('id' - challenge ID)
     * @param string $title Text in the link
     * @return string HTML-link to start challenge or failure message
     */
    public function startChallengeShortcode($atts, $title = '')
    {
        $challenge = self::getChallengeFromAttributes($atts);
        if(!$challenge) {
            return current_user_can('edit_users') ? 'Admin warning: can not find the challenge' : '';
        }

        if(!$challenge->active || AT2Model::isChallengeStartedByConsumer($challenge->challengeId, consumersLoyaltySuite::getAuthorizedConsumerId())) {
            return '';
        }

        $class = 'button normal-button large';
        $class = isset($atts['class']) ? $class . ' ' . esc_attr($atts['class']) : $class;
        $title = empty($title) ? __('Start challenge', 'ls') : $title;
        $url = add_query_arg(['action' => 'start-challenge', 'challengeId' => $challenge->challengeId]);

        return '<a href="' . esc_url($url) . '" class="' . $class . '">' . $title . '</a>';
    }

    /**
     * Shortcode [challenge-submit]
     * Display an activity as the button
     *
     * @since 2.8.0
     *
     * @param array $atts Attributes ('activity_id' - activity ID; 'consumer_id' - consumer ID)
     * @return string HTML-link to mark post as read or failure message
     */
    public function challengeSubmitShortcode($atts)
    {
        $activityId = isset($atts['activity_id']) ? (int) $atts['activity_id'] : 0;
        $activity = AT2Model::getActivity($activityId);
        if(!$activity) {
            return current_user_can('edit_users') ? 'Admin warning: incorrect activity ID' : '';
        }

        $consumerId = isset($atts['consumer_id']) ? (int) $atts['consumer_id'] : consumersLoyaltySuite::getAuthorizedConsumerId();
        if($consumerId < 1) {
            return current_user_can('edit_users') ? 'Admin warning: only authorized consumers can use the challenge' : '';
        }

        if(!AT2Model::isConsumerInActiveChallenge($activity->challengeId, $consumerId)) {
            return '';
        }

        $buttonText = isset($atts['button_text']) && !empty($atts['button_text']) ? $atts['button_text'] : __('I read this article', 'ls');
        $class = $atts['class'] ?? '';

        if(AT2Model::isActivityUsedByConsumer($activityId, $consumerId)) {
            $content = '<div class="ls-at-challenge-has-been-read">' . __('This activity has been done', 'ls') . '</div>';
        } else {
            $content = '<div class="ls-at-challenge-read">
                            <a href="#" data-id="' . $activityId . '" class="at-submit ' . esc_attr($class) . '">' . $buttonText . '</a>
                        </div>';
        }

        return $content;
    }

    /**
     * Shortcodes [challenge-message-1] and [challenge-message-2]
     * Display special challenge message (1st or 2nd depends on shortcode tag)
     *
     * @since 2.0
     * @internal
     *
     * @param array $atts Attributes ('id' - challenge ID)
     * @param string $content Not used
     * @param string $tag Shortcode tag
     * @return string Challenge message
     */
    public function challengeMessageShortcode($atts, $content, $tag)
    {
        $challenge = self::getChallengeFromAttributes($atts);
        if(!$challenge) {
            return current_user_can('edit_users') ? 'Admin warning: can not find the challenge' : '';
        }

        $message = $tag === 'challenge-message-1' ? $challenge->challengeMessage1 : $challenge->challengeMessage2;
        $message = do_shortcode($message);

        return $message;
    }

    /**
     * Shortcode [challenge-participated-days]
     * Display the number of days in which consumer is participated in the challenge
     *
     * @since 2.2
     * @internal
     *
     * @param array $atts Attributes ('id' - challenge ID; 'consumer_id' - consumer ID)
     * @return string
     */
    public function challengeParticipatedDaysShortcode($atts)
    {
        $consumerId = isset($atts['consumer_id']) ? (int) $atts['consumer_id'] : consumersLoyaltySuite::getAuthorizedConsumerId();
        if($consumerId < 1) {
            return current_user_can('edit_users') ? 'Admin warning: only authorized consumers can use the challenge' : '';
        }

        $challenge = self::getChallengeFromAttributes($atts);
        if(!$challenge) {
            return current_user_can('edit_users') ? 'Admin warning: can not find the challenge' : '';
        }

        $content = '0 ' . __('days', 'ls');
        $challengeDetails = AT2Model::getConsumerChallengeDetails($challenge->challengeId, $consumerId);
        if(!empty($challengeDetails)) {
            $daysLeft = (int) (new DateTime(date('Y-m-d', $challengeDetails->enterDate)))->diff(new DateTime(date('Y-m-d')))->format('%a');
            $content = sprintf(_n('%s day', '%s days', $daysLeft, 'ls'), $daysLeft);
        }

        return $content;
    }

    /**
     * Retrieve number of days left until the end of challenge
     *
     * @since 2.14.0
     *
     * @param object $challenge Challenge
     * @return int Number of days left
     */
    public static function getChallengeDaysLeft($challenge)
    {
        $daysLeft = 0;

        if(strtotime($challenge->dateEnd) >= strtotime('TODAY', current_time('timestamp'))) {
            $daysLeft = (int) (new DateTime($challenge->dateEnd))->diff(new DateTime(date('Y-m-d')))->format('%a');
        }

        return $daysLeft;
    }

    /**
     * Shortcode [challenge-days-left]
     * Display days till the end of the challenge
     *
     * @since 2.2
     * @internal
     *
     * @param array $atts Attributes ('id' - challenge ID)
     * @return string
     */
    public function challengeDaysLeftShortcode($atts)
    {
        $challenge = self::getChallengeFromAttributes($atts);
        if(!$challenge) {
            return current_user_can('edit_users') ? 'Admin warning: can not find the challenge' : '';
        }

        $daysLeft = self::getChallengeDaysLeft($challenge);

        return sprintf(_n('<strong>%d</strong> day', '<strong>%d</strong> days', $daysLeft, 'ls'), $daysLeft);
    }

    /**
     * Shortcode [challenge-total-activities-count]
     * Display the number of total activities in the challenge
     *
     * @since 2.2
     * @internal
     *
     * @param array $atts Attributes ('id' - challenge ID)
     * @return int Number of activities
     */
    public function challengeTotalActivitiesCountShortcode($atts)
    {
        $challenge = self::getChallengeFromAttributes($atts);
        if(!$challenge) {
            return current_user_can('edit_users') ? 'Admin warning: can not find the challenge' : '';
        }

        return AT2Model::getTotalActivitiesInChallenge($challenge->challengeId);
    }

    /**
     * Shortcode [challenge-passed-activities-count]
     * Display the number of passed activities in the challenge by consumer
     *
     * @since 2.2
     * @internal
     *
     * @param array $atts Attributes ('id' - challenge ID; 'consumer_id' - consumer ID)
     * @return int Number of activities
     */
    public function challengePassedActivitiesCountShortcode($atts)
    {
        $consumerId = isset($atts['consumer_id']) ? (int) $atts['consumer_id'] : consumersLoyaltySuite::getAuthorizedConsumerId();
        if($consumerId < 1) {
            return current_user_can('edit_users') ? 'Admin warning: only authorized consumers can use the challenge' : '';
        }

        $challenge = self::getChallengeFromAttributes($atts);
        if(!$challenge) {
            return current_user_can('edit_users') ? 'Admin warning: can not find the challenge' : '';
        }

        return AT2Model::getPassedActivitiesCount($challenge->challengeId, $consumerId);
    }

    /**
     * Shortcode [challenge-progress]
     * Display progress in the challenge for consumer
     *
     * @since 2.0
     * @internal
     *
     * @param array $atts Attributes ('id' - challenge ID; 'consumer_id' - consumer ID)
     * @return string Progress value with percentage sign
     */
    public function challengeProgressShortcode($atts)
    {
        $consumerId = isset($atts['consumer_id']) ? (int) $atts['consumer_id'] : consumersLoyaltySuite::getAuthorizedConsumerId();
        if($consumerId < 1) {
            return current_user_can('edit_users') ? 'Admin warning: only authorized consumers can use the challenge' : '';
        }

        $challenge = self::getChallengeFromAttributes($atts);
        if(!$challenge) {
            return current_user_can('edit_users') ? 'Admin warning: can not find the challenge' : '';
        }

        $progress = AT2Model::calculateConsumerProgress($challenge->challengeId, $consumerId);

        $sign = isset($atts['sign']) ? sanitize_text_field($atts['sign']) : '%';

        return $progress ? $progress['consumerProgress'] . $sign : '';
    }

    /**
     * Display challenge progress (bar) for consumer
     *
     * @since 2.13.5
     *
     * @param object Challenge
     * @param string $class Class name
     * @return string HTML
     */
    public static function showChallengeProgressBar($challenge, $class = '')
    {
        $content = '';

        if($challenge) {

            $consumerId = consumersLoyaltySuite::getAuthorizedConsumerId();
            if($consumerId < 1) {
                return current_user_can('edit_users') ? 'Admin warning: only authorized consumers can use the challenge' : '';
            }

            $progress = AT2Model::calculateConsumerProgress($challenge->challengeId, $consumerId);

            $content = (new LS\Template(__DIR__ . '/views/frontend'))
                ->assign('progress', $progress)
                ->assign('class', $class)
                ->render('progress-bar.phtml', false);
        }

        return $content;
    }

    /**
     * Shortcode [challenge-progress-bar]
     * Display challenge progress (bar) for consumer
     *
     * @since 2.0
     * @internal
     *
     * @param array $atts Attributes ('id' - challenge ID)
     * @return string Progress bar HTML-form
     */
    public function challengeProgressBarShortcode($atts)
    {
        $challenge = self::getChallengeFromAttributes($atts);
        if(!$challenge) {
            return current_user_can('edit_users') ? 'Admin warning: can not find the challenge' : '';
        }

        return self::showChallengeProgressBar($challenge, $atts['class'] ?? '');
    }

    /**
     * Shortcode [challenge-goal-message]
     * Display consumer goal message in the challenge
     *
     * @since 2.0
     * @internal
     *
     * @param array $atts Attributes ('id' - challenge ID; 'consumer_id' - consumer ID)
     * @return string Goal message
     */
    public function challengeGoalMessageShortcode($atts)
    {
        $consumerId = isset($atts['consumer_id']) ? (int) $atts['consumer_id'] : consumersLoyaltySuite::getAuthorizedConsumerId();
        if($consumerId < 1) {
            return current_user_can('edit_users') ? 'Admin warning: only authorized consumers can use the challenge' : '';
        }

        $challenge = self::getChallengeFromAttributes($atts);
        if(!$challenge) {
            return current_user_can('edit_users') ? 'Admin warning: can not find the challenge' : '';
        }

        $content = '';

        if(AT2Model::isChallengeStartedByConsumer($challenge->challengeId, $consumerId)) {
            $content = AT2Model::getConsumerGoalMessageInChallenge($challenge->challengeId, $consumerId);
        }

        return $content;
    }

    /**
     * Shortcode [tracking-code-form]
     * Display form to submit tracking code
     *
     * @since 2.0
     * @internal
     *
     * @return string HTML-form
     */
    public function trackingCodeFormShortcode()
    {
        if(!consumersLoyaltySuite::isConsumerAuthorized()) {
            $content = '<p class="ls-at-challenge-error">' . __('Please log in to get access to this form', 'ls') . '</p>';
        } else {
            $content = (new LS\Template(__DIR__ . '/views/frontend'))->render('tracking-code-form.phtml', false);
        }

        return $content;
    }

    /**
     * Shortcode [tracking-code-trigger]
     * Submit specified tracking code automatically when opening content with this shortcode
     * Note: shortcode display nothing
     *
     * @since 2.4
     * @internal
     *
     * @param array $atts Attributes ('code' - tracking code)
     * @return string Hidden script to submit tracking code automatically
     */
    public function trackingCodeActivityShortcode($atts)
    {
        $content = '';

        if(isset($atts['code']) && !empty($atts['code']) && consumersLoyaltySuite::isConsumerAuthorized(true)) {
            $content .= '<input type="hidden" class="at-tracking-code" value="' . esc_attr(trim($atts['code'])) . '" />';
        }

        return $content;
    }

    /**
     * Shortcode [challenges-overview]
     * Display consumer challenges overview table
     *
     * @since 2.11.8
     * @internal
     *
     * @return string HTML
     */
    public function challengesOverviewShortcode()
    {
        $html = '';

        $consumerId = consumersLoyaltySuite::getAuthorizedConsumerId();
        if($consumerId > 0) {

            $data = [];
            $consumerChallengesIds = AT2Model::getConsumerParticipatedChallengesIds($consumerId);

            foreach(AT2Model::getActiveChallenges() as $challenge) {
                if(in_array($challenge->challengeId, $consumerChallengesIds)) {
                    $data[] = [
                        'challenge'        => $challenge,
                        'openActivities'   => AT2Model::getOpenActivitiesCount($challenge->challengeId, $consumerId),
                        'passedActivities' => AT2Model::getPassedActivitiesCount($challenge->challengeId, $consumerId),
                        'totalActivities'  => AT2Model::getTotalActivitiesInChallenge($challenge->challengeId)
                    ];
                }
            }

            $html = (new LS\Template(__DIR__ . '/views/frontend'))
                ->assign('consumerId', $consumerId)
                ->assign('data', $data)
                ->assign('imagesUrl', $this->getUrl2Module() . 'images')
                ->render('challenges-overview.phtml', false);
        }

        return $html;
    }

    /**
     * Customize post content -> add hidden tag to mark post as read
     * @since 2.8.0
     * @internal
     */
    public function wpFooter()
    {
        $post = get_post();
        $consumerId = consumersLoyaltySuite::getAuthorizedConsumerId();

        if (
            $post instanceof WP_Post
            && $post->ID > 0
            && $consumerId > 0
            && $post->post_status === 'publish'
            && (is_single() || is_page())
            && !\LS\Library::isAdminPreview()
        ) {
            foreach (AT2Model::getMatchedActivities('readPost', $post->ID) as $activity) {
                if (!AT2Model::isActivityUsedByConsumer($activity->activityId, $consumerId)) {
                    echo '<input type="hidden" class="at-activity-rp" value="' . $activity->activityId . '" />';
                }
            }
        }
    }

    /**
     * Extend consumers widget "My Account" by adding challenge details
     *
     * @since 2.0
     * @internal
     *
     * @param string $content Widget content
     * @return string Modified widget content
     */
    public function myAccountBlockFilter($content)
    {
        $activeChallenges = AT2Model::getActiveChallenges();

        if(count($activeChallenges) === 1) {

            $content .= '<p>' . sprintf(__('You participate in the current challenge "<b>%s</b>"', 'ls'), $activeChallenges[0]->title) . '</p>';
            $content .= '<p>' . $this->challengeProgressBarShortcode(['id' => $activeChallenges[0]->challengeId]) . '</p>';
            $content .= '<p>' . $this->challengeGoalMessageShortcode(['id' => $activeChallenges[0]->challengeId]) . '</p>';

        } elseif(count($activeChallenges) > 1) {

            $content .= '<div>' . sprintf(__('You participate in %d challenges:', 'ls'), count($activeChallenges)) . '</div>';
            $content .= '<ul>';

            foreach($activeChallenges as $challenge) {
                $content .= '<li>' . $challenge->title . '</li>';
            }

            $content .= '</ul>';
        }

        return $content;
    }

    /**
     * Delete consumer hook - delete all information about consumer in the module DB tables
     *
     * @since 2.0
     * @internal
     *
     * @param int $consumerId Deleted consumer ID
     */
    public function consumerDeleted(int $consumerId)
    {
        AT2Model::deleteConsumer($consumerId);
    }

    /**
     * Retrieve URL to edit challenge
     *
     * @since 2.13.2
     *
     * @param int $challengeId Challenge Id
     * @param string $referer Referer
     * @return string URL
     */
    public static function challengeEditLink($challengeId, $referer = '')
    {
        _deprecated_function(__FUNCTION__, '2.16.0', 'AT2Model::challengeEditLink()');

        return at2AdminLoyaltySuite::challengeEditLink($challengeId, $referer);
    }

    /**
     * Init default AT actions
     * @since 2.8.0
     * @internal
     */
    private function initDefaultActions()
    {
        ATActions::addAction('readPost', (new ATAction())
            ->setLabels([
                'title'        => __('Read post', 'ls'),
                'openActivity' => __('Read now', 'ls'),
                'doneActivity' => __('Read again', 'ls'),
                'emptyIdent'   => __('Activity "%s": please select an article', 'ls')
            ])
            ->allowEmptyLink(false)
            ->setIdentId('identReadPost')
            ->setReviewFunc(static function() {
                return false;
            })
            ->setSettingsFunc(static function($activity) {
                (new LS\Template(__DIR__ . '/views/backend'))
                    ->assign('activity', $activity)
                    ->render('action.read-post.phtml');
            })
            ->validateActivity(static function($activity) {

                $valid = true;

                if($activity['ident'] > 0) {
                    $post = get_post($activity['ident']);
                    if(!($post instanceof WP_Post) || $post->post_status !== 'publish') {
                        $valid = false;
                    }
                }

                return $valid;
            })
            ->setConsumerDetailsLogFunc(static function($activity) {
                return __('Read post', 'ls') . ' "' . get_the_title($activity['ident']) . '"';
            })
            ->setExample(static function() {
                return __('Open the post and read it. Activity will be performed 5 seconds within opening the page.', 'ls');
            })
        );

        ATActions::addAction('buttonSubmit', (new ATAction())
            ->setLabels([
                'title' => __('Do it! (press the button)', 'ls')
            ])
            ->allowEmptyLink(false)
            ->setExample(static function($activity) {
                return '<textarea>[challenge-submit activity_id="' . ($activity['activityId'] ?? 0) . '" button_text="' . __('Do it!', 'ls') . '" class="button normal-button large"]</textarea>';
            })
        );

        ATActions::addAction('trackingCode', (new ATAction())
            ->setLabels([
                'title'      => __('Tracking code', 'ls'),
                'emptyIdent' => __('Activity "%s": please enter tracking code', 'ls')
            ])
            ->allowEmptyLink(false)
            ->setIdentId('identTrackingCode')
            ->setSettingsFunc(static function($activity) {
                (new LS\Template(__DIR__ . '/views/backend'))
                    ->assign('activity', $activity)
                    ->render('action.tracking-code.phtml');
            })
            ->setConsumerDetailsLogFunc(static function($activity) {
                return sprintf(__('Tracking code "%s" submitted', 'ls'), $activity['ident']);
            })
            ->setExample(static function($activity) {
                $code = empty($activity['ident']) || $activity['action'] !== 'trackingCode' ? 'CODE12345' : $activity['ident'];
                return sprintf(__('There are a several ways to perform this activity:<br />1. Submit code "%s" to the form: [tracking-code-form]<br />2. Add tracking code to the URL: http://your.page/?at_code=%s<br />3. Automatically perform the trigger on open the content with [tracking-code-trigger code="%s"]', 'ls'), $code, $code, $code);
            })
        );

        ATActions::addAction('addComment', (new ATAction())
            ->setLabels([
                'title' => __('Add comment', 'ls')
            ])
            ->allowMultipleUsage(false)
            ->setReviewFunc(static function($consumerId) {
                return self::getCommentIdByMeta('consumerId', $consumerId) > 0;
            })
            ->setExample(static function() {
                return __('Add comment. Please note that activity work only when the plugin "LS Consumers - Consumer Comments" activated!', 'ls');
            })
        );

        if(class_exists('GFFormsModel')) {
            ATActions::addAction('gravityForms', (new ATAction())
                ->setLabels([
                    'title'        => __('Gravity Forms (submit form)', 'ls'),
                    'emptyIdent'   => __('Activity "%s": please select Gravity Form', 'ls'),
                    'openActivity' => __('View', 'ls'),
                    'doneActivity' => __('Done', 'ls')
                ])
                ->allowEmptyLink(false)
                ->setIdentId('identGravityForms')
                ->setSettingsFunc(static function($activity) {
                    (new LS\Template(__DIR__ . '/views/backend'))
                        ->assign('activity', $activity)
                        ->render('action.gf.phtml');
                })
                ->validateActivity(static function($activity) {

                    $valid = true;
                    $form = GFFormsModel::get_form_meta($activity['ident']);

                    if(!empty($form['scheduleForm'])) {
                        $local_time_start = sprintf("%s %02d:%02d %s", $form["scheduleStart"], $form["scheduleStartHour"], $form["scheduleStartMinute"], $form["scheduleStartAmpm"]);
                        $local_time_end = sprintf("%s %02d:%02d %s", $form["scheduleEnd"], $form["scheduleEndHour"], $form["scheduleEndMinute"], $form["scheduleEndAmpm"]);
                        $timestamp_start = strtotime($local_time_start . ' +0000');
                        $timestamp_end = strtotime($local_time_end . ' +0000');
                        $now = current_time("timestamp");
                        if(!empty($form["scheduleStart"]) && $now < $timestamp_start) {
                            $valid = false;
                        } elseif(!empty($form["scheduleEnd"]) && $now > $timestamp_end) {
                            $valid = false;
                        }
                    }

                    return $valid;
                })
                ->setConsumerDetailsLogFunc(static function($activity) {
                    /** @var object $form */
                    $form = class_exists('RGFormsModel') ? RGFormsModel::get_form($activity['ident']) : false;
                    return sprintf(__('Submit Gravity Form "%s"', 'ls'), $form ? $form->title : '');
                })
            );
        }
    }

    /**
     * Perform activity(-ies) (make it passed by consumer)
     *
     * Note:
     * Function tries to find related activities by specified action (activity name) and activity identify.
     * All found activities will be marked as performed (if possible to perform them).
     * Can be more than one activity (e.g. because of similar activities in two active challenges)
     *
     * @since 2.0
     *
     * @param string $action Activity name
     * @param string $ident Activity identify (e.g. post ID if action is "readPost")
     * @param int $consumerId Consumer ID (current logged in consumer if null)
     * @param bool $showMessage True to show success message to the consumer after activity will be performed
     * @param null|int $challengeId Challenge ID or null to use all challenges
     * @return array A list of found/performed activities
     */
    public function activitiesLogic($action, $ident = '', $consumerId = null, $showMessage = true, $challengeId = null)
    {
        $consumerId = $consumerId === null ? consumersLoyaltySuite::getAuthorizedConsumerId() : (int) $consumerId;
        $activities = apply_filters('ls_at_activities_logic', AT2Model::getMatchedActivities($action, $ident, $challengeId));

        foreach($activities as $key => $activity) {
            $activities[$key]->connected = $this->activityLogic($activity, $consumerId, $showMessage);
        }

        return $activities;
    }

    /**
     * Perform activity (make it passed by consumer)
     *
     * @since 2.8.0
     *
     * @param int|object Activity ID or activity object
     * @param int $consumerId Consumer ID
     * @param bool $showMessage True to show success message to the consumer after activity will be performed
     * @return bool True if activity performed
     */
    public function activityLogic($activity, $consumerId, $showMessage = true)
    {
        if($consumerId < 1) {
            return false;
        }

        if(is_numeric($activity)) {
            $activity = AT2Model::getActivity($activity);
        }

        if(!$activity) {
            return false;
        }

        $challengeId = (int) $activity->challengeId;
        $activityId = (int) $activity->activityId;

        if(!AT2Model::isConsumerInActiveChallenge($challengeId, $consumerId)) {
            return false;
        }

        if(AT2Model::isActivityUsedByConsumer($activityId, $consumerId)) {
            return false;
        }

        if(!AT2Model::connectActivityToConsumer($activity, $consumerId)) {
            return false;
        }

        if($showMessage) {
            AT2Model::displayActivitySuccessMessage($activityId);
        }

        return true;
    }

    /**
     * Review all consumer activities if they were performed but not marked as complete in AT
     *
     * @since 2.8.0
     * @internal
     *
     * @param int $consumerId Consumer ID
     * @param null|int $challengeId Challenge ID
     */
    protected function reviewConsumerActivities($consumerId, $challengeId = null)
    {
        if($consumerId > 0 && $challengeId !== 0) {

            $challengesToCheck = $challengeId > 0 ? [$challengeId] : AT2Model::getConsumerParticipatedChallengesIds($consumerId);

            foreach(AT2Model::getActiveChallenges() as $challenge) {
                if(in_array($challenge->challengeId, $challengesToCheck)) {
                    foreach(AT2Model::getChallengeActivities((int) $challenge->challengeId) as $activity) {

                        $action = ATActions::getAction($activity['action']);
                        if(!$action) {
                            continue;
                        }

                        if((!$action->hasReview() && AT2Model::isActionPerformedByConsumer($consumerId, $activity)) || $action->checkIfNeedToPerform($consumerId, $activity)) {
                            $this->activitiesLogic($activity['action'], $activity['ident'], $consumerId, false, (int) $challenge->challengeId);
                        }
                    }
                }
            }
        }
    }

    /**
     * Mark post as read
     *
     * @since 2.8.0
     * @ajax
     * @internal
     */
    public function atChallengeReadAjax()
    {
        $consumerId = consumersLoyaltySuite::getAuthorizedConsumerId(true);
        $activityId = $this->getParam('activityId', 'id');

        if($consumerId < 1 || $activityId < 1) {
            exit;
        }

        $activity = AT2Model::getActivity($activityId);
        if(!$activity || $activity->ident < 1) {
            exit;
        }

        $p = get_post($activity->ident);
        if(!$p || $p->post_status !== 'publish') {
            exit;
        }

        $showMsg = $this->getParam('showMsg', 'int');

        $this->activitiesLogic('readPost', $activity->ident, $consumerId, $showMsg, (int) $activity->challengeId);

        exit;
    }

    /**
     * Submit the button
     *
     * @since 2.8.0
     * @ajax
     * @internal
     */
    public function atChallengeSubmitAjax()
    {
        $consumerId = consumersLoyaltySuite::getAuthorizedConsumerId();
        $activityId = $this->getParam('activityId', 'id');
        $error = '<div class="ls-at-activity-failed">' . __('Can not perform the activity', 'ls') . '</div>';

        if($consumerId < 1 || $activityId < 1) {
            wp_send_json($error);
        }

        $activity = AT2Model::getActivity($activityId);
        if(!$activity) {
            wp_send_json($error);
        }

        if($this->activityLogic($activity, $consumerId, false)) {
            $response = '<div class="ls-at-challenge-has-been-read">' . __('This activity has been done', 'ls') . '</div>';
            wp_send_json($response);
        }

        wp_send_json($error);
    }

    /**
     * "Tracking code" has been entered -> perform activity
     * @since 2.0
     * @internal
     */
    public function activityTrackingCode()
    {
        $consumerId = consumersLoyaltySuite::getAuthorizedConsumerId();
        if(isset($_REQUEST['at_code']) && !empty($_REQUEST['at_code']) && !empty($consumerId)) {

            $showMsg = $this->getParam('showMsg', 'int');
            $atCode = $this->getParam('at_code', 'text');

            foreach($this->activitiesLogic('trackingCode', $atCode, null, false) as $activity) {
                if($activity->connected) {
                    if($showMsg) {
                        AT2Model::displayActivitySuccessMessage($activity->activityId);
                    }
                } elseif($showMsg) {
                    $this->setError(__('Can not check tracking code or you have already used entered tracking code', 'ls'));
                }
            }
        }
    }

    /**
     * Tracking code -> ajax submission
     *
     * @since 2.8.0
     * @internal
     * @ajax
     */
    public function atTrackingCodeAjax()
    {
        $consumerId = consumersLoyaltySuite::getAuthorizedConsumerId(true);
        $code = $this->getParam('code', 'text');
        $showMsg = $this->getParam('showMsg', 'int');

        if($consumerId > 0 && !empty($code)) {
            $this->activitiesLogic('trackingCode', $code, $consumerId, $showMsg);
        }

        exit;
    }

    /**
     * Gravity Form submitted -> perform activity
     *
     * @since 2.0
     * @internal
     *
     * @param array $entry Not used
     * @param array $form GForm
     */
    public function gform_after_submission($entry, $form)
    {
        $this->activitiesLogic('gravityForms', $form['id']);
    }

    /**
     * Commend added -> perform activity
     *
     * @since 2.0
     * @internal
     *
     * @param int $comment_id Comment ID
     */
    public function wp_insert_comment($comment_id)
    {
        if($comment_id > 0 && consumersLoyaltySuite::isConsumerAuthorized()) {
            $this->activitiesLogic('addComment');
        }
    }

    /**
     * Generate a list of conditions for Com.Center
     *
     * @since 2.7.0
     * @internal
     *
     * @param LSConditionalEmailCondition[] $conditions List of conditions
     * @return LSConditionalEmailCondition[] List of conditions
     */
    public function comCenterInit($conditions)
    {
        $challengeIdField = static function () {
            return new LS\Field\Select('challengeId', __('Challenge', 'ls'), ['options' => AT2Model::getChallengesList(), 'showEmpty' => false]);
        };

        $conditions['pcp_in_challenge'] = new LSConditionalEmailCondition('pcp_in_challenge', __('Participation in the challenge', 'ls'), $this->getTitle(), [
            'allowedPeriods' => [LSConditionalEmail::PERIOD_ON, LSConditionalEmail::PERIOD_AFTER],
            'onceADay'       => false,
            'customFields'   => [
                'challengeId' => $challengeIdField
            ],
            'recipientsCb'   => static function (LSConditionalEmail $ce) {

                // 'on' should be triggered immediately on event
                if ($ce->getPeriod() == LSConditionalEmail::PERIOD_ON) {
                    return [];
                }

                return AT2Model::getPCPConsumers(
                    (int) $ce->getCustomFieldValue('challengeId', 0),
                    $ce->getDaysToEvent()
                );
            },
            'shortcodesNote' => '[' . implode('], [', self::getShortcodesForChallengeEmail()) . ']',
            'bodyCallback'   => function ($body, $consumerId, LSConditionalEmail $ce) {
                return $this->appendChallengeEmailShortcodes(
                    (int) $ce->getCustomFieldValue('challengeId', 0),
                    $body,
                    $consumerId
                );
            }
        ]);

        $conditions['not_pcp_in_challenge'] = new LSConditionalEmailCondition('not_pcp_in_challenge', __('Not participated in the challenge', 'ls'), $this->getTitle(), [
            'allowedPeriods' => [LSConditionalEmail::PERIOD_AFTER],
            'customFields'   => [
                'challengeId' => $challengeIdField
            ],
            'recipientsCb'   => static function (LSConditionalEmail $ce) {
                return AT2Model::getNotPCPConsumers(
                    (int) $ce->getCustomFieldValue('challengeId', 0),
                    $ce->getDaysToEvent()
                );
            },
            'shortcodesNote' => '[' . implode('], [', self::getShortcodesForChallengeEmail()) . ']',
            'bodyCallback'   => function ($body, $consumerId, LSConditionalEmail $ce) {
                return $this->appendChallengeEmailShortcodes(
                    (int) $ce->getCustomFieldValue('challengeId', 0),
                    $body,
                    $consumerId
                );
            }
        ]);

        $conditions['after_challenge_ends'] = new LSConditionalEmailCondition('after_challenge_ends', __('Challenge end', 'ls'), $this->getTitle(), [
            'customFields'   => [
                'challengeId' => $challengeIdField
            ],
            'recipientsCb'   => static function (LSConditionalEmail $ce) {
                return AT2Model::getACEConsumers(
                    (int) $ce->getCustomFieldValue('challengeId', 0),
                    $ce->getDaysToEvent()
                );
            },
            'shortcodesNote' => '[' . implode('], [', self::getShortcodesForChallengeEmail()) . ']',
            'bodyCallback'   => function ($body, $consumerId, LSConditionalEmail $ce) {
                return $this->appendChallengeEmailShortcodes(
                    (int) $ce->getCustomFieldValue('challengeId', 0),
                    $body,
                    $consumerId
                );
            }
        ]);

        return $conditions;
    }

    /**
     * Consumer participated in the challenge -> trigger Com.Center "Participate in the challenge"
     *
     * @since 2.7.0
     * @internal
     *
     * @param int $challengeId Challenge ID
     * @param int $consumerId Consumer ID
     */
    public function comCenterStartChallenge($challengeId, $consumerId)
    {
        /** @var comCenterLoyaltySuite $cc */
        $cc = LS()->getModule('comCenter');
        $cc->triggerConditionalEmails('pcp_in_challenge', static function(LSConditionalEmail $ce) use ($consumerId, $challengeId) {
            return $ce->getCustomFieldValue('challengeId', 0) == $challengeId ? $consumerId : false;
        });
    }

    /**
     * Check all activities in the challenge for status when consumer started challenge
     *
     * @since 2.8.0
     * @internal
     *
     * @param int $challengeId Challenge ID
     * @param int $consumerId Challenge ID
     */
    public function consumerStartedChallenge($challengeId, $consumerId)
    {
        $this->reviewConsumerActivities($consumerId, $challengeId);
    }
}