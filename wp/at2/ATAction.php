<?php
class ATAction
{
    /** @var string */
    public $label;

    /** @var string */
    public $openActivityLabel;

    /** @var string */
    public $doneActivityLabel;

    /** @var string */
    private $emptyIdentLabel;

    /** @var bool */
    private $allowEmptyLink = true;

    /** @var bool */
    private $allowMultipleUsage = true;

    /** @var string */
    private $identId = '';

    /** @var callable */
    private $settingsCb;

    /** @var callable */
    private $activityStatusCb;

    /** @var callable */
    private $consumerDetailsLog;

    /** @var callable */
    private $reviewCb;

    /** @var callable */
    private $customActivityRowCb;

    /** @var callable */
    private $exampleCb;

    /**
     * @param array $labels
     * @return self
     */
    public function setLabels($labels)
    {
        $this->label = $labels['title'] ?? '';
        $this->openActivityLabel = $labels['openActivity'] ?? __('Open action', 'ls');
        $this->doneActivityLabel = $labels['doneActivity'] ?? __('Activated', 'ls');
        $this->emptyIdentLabel = $labels['emptyIdent'] ?? __('Activity "%s": empty identifier', 'ls');

        return $this;
    }

    /**
     * @param bool $b
     * @return self
     */
    public function allowEmptyLink($b = true)
    {
        $this->allowEmptyLink = $b;

        return $this;
    }

    /**
     * @param bool $b
     * @return self
     */
    public function allowMultipleUsage($b = true)
    {
        $this->allowMultipleUsage = $b;

        return $this;
    }

    /**
     * @param string $id
     * @return self
     */
    public function setIdentId($id)
    {
        $this->identId = $id;

        return $this;
    }

    /**
     * @param callable $func
     * @return self
     */
    public function setSettingsFunc($func)
    {
        if(is_callable($func)) {
            $this->settingsCb = $func;
        }

        return $this;
    }

    /**
     * @return string
     */
    public function getIdentId()
    {
        return $this->identId;
    }

    /**
     * @param array $activity
     * @return string
     */
    public function validateActivityIdent($activity)
    {
        if($this->identId !== null && !empty($this->identId) && empty($activity['ident'])) {
            return sprintf($this->emptyIdentLabel, $activity['title']);
        }

        return '';
    }

    /**
     * @param callable $func
     * @return self
     */
    public function validateActivity($func)
    {
        if(is_callable($func)) {
            $this->activityStatusCb = $func;
        }

        return $this;
    }

    /**
     * @param callable $func
     * @return self
     */
    public function setConsumerDetailsLogFunc($func)
    {
        if(is_callable($func)) {
            $this->consumerDetailsLog = $func;
        }

        return $this;
    }

    /**
     * @param callable $func
     * @return self
     */
    public function setReviewFunc($func)
    {
        if(is_callable($func)) {
            $this->reviewCb = $func;
        }

        return $this;
    }

    /**
     * @param callable $func
     * @return self
     */
    public function setCustomActivityRowFunc($func)
    {
        if(is_callable($func)) {
            $this->customActivityRowCb = $func;
        }

        return $this;
    }

    /**
     * @param callable $func
     * @return self
     */
    public function setExample($func)
    {
        if(is_callable($func)) {
            $this->exampleCb = $func;
        }

        return $this;
    }

    /**
     * @return bool
     */
    public function isAllowMultipleUsage()
    {
        return $this->allowMultipleUsage;
    }

    /**
     * @param array $activity
     * @return bool
     */
    public function isActivityValid($activity)
    {
        $valid = true;

        if($this->activityStatusCb !== null && !empty($this->activityStatusCb)) {
            $valid = call_user_func($this->activityStatusCb, $activity);
        }

        if(is_array($valid) && (isset($activity['future']) || isset($activity['draft']))) {
            _doing_it_wrong(__FUNCTION__, 'Function must return boolean (valid/invalid activity status)', '2.14.4');
            $valid = false;
        }
        
        return $valid;
    }

    /**
     * @param null|array $activity
     */
    public function displaySettingsFunc($activity = null)
    {
        if($this->settingsCb !== null && !empty($this->settingsCb)) {
            call_user_func($this->settingsCb, is_array($activity) ? $activity : []);
        }
    }

    /**
     * @param array $activity
     * @return string
     */
    public function getConsumerLogMessage($activity)
    {
        $text = '';

        if($this->consumerDetailsLog !== null && !empty($this->consumerDetailsLog)) {
            $text = call_user_func($this->consumerDetailsLog, $activity);
        }

        return empty($text) ? $this->label : $text;
    }

    /**
     * @return bool
     */
    public function hasReview()
    {
        return $this->reviewCb !== null && !empty($this->reviewCb);
    }

    /**
     * @param int $consumerId
     * @param array $activity
     * @return bool
     */
    public function checkIfNeedToPerform($consumerId, $activity)
    {
        if($this->hasReview()) {
            return call_user_func($this->reviewCb, $consumerId, $activity);
        }

        return false;
    }

    /**
     * @return bool
     */
    public function hasCustomActivityRow()
    {
        return $this->customActivityRowCb !== null && !empty($this->customActivityRowCb);
    }

    /**
     * @param array $activity
     */
    public function displayCustomActivityRow($activity)
    {
        if($this->hasCustomActivityRow()) {
            call_user_func($this->customActivityRowCb, $activity);
        }
    }

    /**
     * @return bool
     */
    public function isAllowedEmptyLink()
    {
        return $this->allowEmptyLink;
    }

    /**
     * @return bool
     */
    public function hasExample()
    {
        return $this->exampleCb !== null && !empty($this->exampleCb);
    }

    /**
     * @param null|array $activity
     * @param string $actionId
     */
    public function displayExample($activity, $actionId)
    {
        if(!empty($actionId) && $this->hasExample()) {

            $activity = is_array($activity) ? $activity : [];
            $isSelected = isset($activity['action']) && $activity['action'] == $actionId;

            echo '<div class="ls-at-example ' . ($isSelected ? '' : ' hide') . '" data-action="' . esc_attr($actionId) . '">';
            echo '<div class="ls-at-usage">';
            echo call_user_func($this->exampleCb, $activity);
            echo '</div>';
            echo '</div>';
        }
    }
}