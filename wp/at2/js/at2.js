jQuery(function($){

    if($('.at-tracking-code').length) {
        $('.at-tracking-code').each(function() {
            $.ajax({
                url: LS.ajaxurl,
                type: 'GET',
                dataType: 'json',
                data: {
                    action: 'atTrackingCode',
                    code: $(this).val(),
                    showMsg: 1
                }
            });
        });
    }

    if($('.at-activity-rp').length) {

        $('.at-activity-rp').each(function() {

            var id = $(this).val();

            setTimeout(function() {

                $.ajax({
                    url: LS.ajaxurl,
                    type: 'GET',
                    dataType: 'json',
                    data: {
                        action: 'atChallengeRead',
                        activityId: id,
                        showMsg: 1
                    }
                });

            }, 5000);
        });
    }

    if($('.at-submit').length) {
        $('.at-submit').click(function() {

            var id = $(this).data('id');
            if(id > 0) {

                var _content = $(this).parent();

                _content.html('<div class="ls-at-loading">&nbsp;</div>');

                $.ajax({
                    url: LS.ajaxurl,
                    type: 'GET',
                    dataType: 'json',
                    data: {
                        action: 'atChallengeSubmit',
                        activityId: id
                    },
                    success: function(response) {
                        _content.html(response);
                    }
                });
            }

            return false;
        });
    }

});