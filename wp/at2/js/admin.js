// Textarea and select clone() bug workaround
(function (original) {
  jQuery.fn.clone = function () {
    var result           = original.apply(this, arguments),
        my_textareas     = this.find('textarea').add(this.filter('textarea')),
        result_textareas = result.find('textarea').add(result.filter('textarea')),
        my_selects       = this.find('select').add(this.filter('select')),
        result_selects   = result.find('select').add(result.filter('select')),
        i, l;
    for (i = 0, l = my_textareas.length; i < l; ++i) jQuery(result_textareas[i]).val(jQuery(my_textareas[i]).val());
    for (i = 0, l = my_selects.length;   i < l; ++i) result_selects[i].selectedIndex = my_selects[i].selectedIndex;
    return result;
  };
}) (jQuery.fn.clone);

jQuery(document).ready(function($) {
    
    addDeleteConfirmation('delete-challenge');

    if($('#ls-challenge').length) {

        // show/hide activity body
        $('.ls-activities-list').on('click', '.ls-activity-header', function() {

            var item = $(this).closest('.ls-at-activity-item'),
                body = item.find('.ls-activity-body');

            if(body.is(':visible')) {
                body.slideUp('fast', function() {
                    item.find('.ls-at-header-sign').html('+');
                });
            } else {
                $('.ls-at-activity-item .ls-activity-body').slideUp('fast');
                $('.ls-at-header-sign').html('+');
                item.find('.ls-at-header-sign').html('—');
                body.slideDown(function() {
                    $('html, body').animate({
                        scrollTop: body.closest('.ls-at-activity-item').offset().top - 40
                    }, 200);
                });
            }
        });

        var defaultSuccessMessage = $('.ls-new-activity .activitySuccessMessage').val();

        // create activity
        $('#addActivity').click(function() {

            var activityField = $('.ls-new-activity'),
                activity = {
                    title: activityField.find('.activityTitle').val(),
                    description: activityField.find('.activityDescription').val(),
                    image: activityField.find('.activityImage').val(),
                    link: activityField.find('.activityLink').val(),
                    showSuccessMessage: activityField.find('.activityShowSuccessMessage').val(),
                    successMessage: activityField.find('.activitySuccessMessage').val(),
                    action: activityField.find('.activityAction').val(),
                    actionLabel: activityField.find('.activityAction option:selected').text(),
                    ident: activityField.find('.activityIdent:visible').val(),
                    activityId: activityField.find('.activityId').val()
                };

            if(!activity.title.length) {
                alert(ATAdminVars.emptyActivityTitle);
            } else if(has(ATAdminVars.actionsIdents, activity.action) && !activity.ident.length) {
                alert(ATAdminVars.emptyActivityIdent);
            } else {
                var template = activityField.clone(true);
                if(template.length) {

                    $(this).trigger('ls_at_add_activity', [template, activity]);

                    // design improvements
                    template.find('.na-hide').removeClass('na-hide');
                    template.find('.ls-at-aa, .ls-at-aa-label').remove();
                    template.removeClass('ls-new-activity').addClass('ls-at-activity-item');
                    template.find('.ls-at-example[data-action="' + activity.action + '"]').removeClass('hide');
                    template.find('.ls-at-activity-examples').removeClass('hide');
                    template.find('.ls-at-header-title').html(activity.title + ' - ' + activity.actionLabel);
                    template.find('.ls-activity-body').addClass('hide');

                    $('.ls-activities-list').append(template);

                    $('.ls-at-activity-item').last().find('.ls-activity-header').click();

                    // clear form "New activity"
                    activityField.find('select').find('option:first').attr("selected", "selected");
                    activityField.find('input[type=text]').val('');
                    activityField.find('.activityShowSuccessMessage').attr('checked', 'checked');
                    activityField.find('.activitySuccessMessage').val(defaultSuccessMessage);
                    activityField.find('.activityAction').val('');
                    activityField.find('.activityIdent, .ls-new-activity .ls-at-example').addClass('hide');
                }
            }
        });

        // change action
        $('select.activityAction').change(function() {

            var activity = $(this).val(),
                item = $(this).closest('.ls-at-activity-item, .ls-new-activity');

            // show/hide idents
            item.find('.activityIdent').addClass('hide');
            var idents = ATAdminVars.actionsIdents;
            for(var i in idents) {
                if(activity === i) {
                    item.find('.' + idents[i]).removeClass('hide');
                }
            }

            // hide examples
            item
                .find('.ls-at-activity-examples, .ls-at-example')
                .addClass('hide');

            // show current example
            item
                .find('.ls-at-example[data-action="' + activity + '"]')
                .removeClass('hide')
                .closest('.ls-at-activity-examples')
                .removeClass('hide');

            $(this).trigger('ls_at_select_activity', [item, activity]);

            item.find('.activityDescription, .activityImage, .activityLink').val('');
        });

        // remove activity
        $('.activityRemove').click(function() {

            if(!confirm(ATAdminVars.removeConfirmation)) {
                return;
            }

            var item = $(this).closest('.ls-at-activity-item'),
                previousItem = item.prev('.ls-at-activity-item');

            previousItem = previousItem.length ? previousItem : $('.ls-activities-list');

            // remove record
            item.remove();

            $('html, body').animate({
                scrollTop: previousItem.offset().top - 40
            }, 200);
        });

        // convert data to formatted array when submit the form
        $('form.ls-at-form').submit(function() {
            $('.ls-at-activity-item').each(function(i) {

                var activity = $(this);

                $('[data-column], .activityIdent', activity).not('.hide').each(function() {

                    var element = $(this),
                        columnName;

                    if(element.hasClass('activityIdent')) {
                        columnName = 'ident';
                        if(element.hasClass('postPicker-field')) {
                            element = element.parent().find('.postPicker-value');
                        }
                    } else {
                        columnName = element.attr('data-column');
                    }

                    element.attr('name', 'activities[' + i + '][' + columnName + ']');
                });
            });
        });

        // event: check/uncheck "show success message"
        $('input.activityShowSuccessMessage').change(function() {
            var field = $(this).closest('.ls-activity-body').find('.activitySuccessMessage');
            if($(this).is(':checked')) {
                field.removeAttr('disabled');
            } else {
                field.attr('disabled', 'disabled');
            }
        });

        // update order
        var updateOrder = function() {
            var i = 1;
            $('.ls-activity.ls-at-activity-item').each(function() {
                $(this).find('.ls-at-header-order').html(i);
                $(this).find('.activityOrder').val(i);
                ++i;
            });
        };

        $('#addActivity, .activityRemove').click(updateOrder);

        $('.ls-activities-list').sortable({
            items: '.ls-activity.ls-at-activity-item',
            helper: 'clone',
            cancel: '.ls-activity-body',
            update: updateOrder
        });

        updateOrder();

        // dynamically change description/image for activities with post picker
        $('.postPicker').on('pp_select', function(e, postId) {

            var item = $(this).closest('.ls-activity'),
                descriptionField = item.find('.activityDescription'),
                imageField = item.find('.activityImage'),
                linkField = item.find('.activityLink');

            if(postId === 0) {

                descriptionField.val('');
                imageField.val('');
                linkField.val('');

            } else {

                $([descriptionField, imageField, linkField]).each(function() {
                    $(this).hide();
                    $(this).after('<div class="ls-at-loading">&nbsp;</div>');
                });

                $('#save-challenge').attr('disabled', 'disabled');

                $.post(ajaxurl, {
                        action: 'atLoadPostDetails',
                        postId: postId
                    }, function(response) {

                        if(response) {
                            descriptionField.val(response.title);
                            imageField.val(response.image);
                            linkField.val(response.link);
                        }

                        $([descriptionField, imageField, linkField]).each(function() {
                            $(this).next('.ls-at-loading').remove();
                            $(this).show();
                        });

                        $('#save-challenge').removeAttr('disabled')
                    }
                );
            }
        });

        // tricks: make statistics tab visible as a part of challenge tabs structure
        if($('#at-challenge-statistics-body').length) {

            var atfn = function() {
                setTimeout(function() {
                    $('#at-challenge-statistics-body').setVisible(location.hash === '#at-challenge-statistics');
                    $('p.submit').setVisible(location.hash !== '#at-challenge-statistics');
                }, 1);
            };

            atfn();

            $('#ls-challenge .ls-tab').click(atfn);
        }
    }

    if($('#atForDash').length) {

        $('#atForDash').change(function() {

            var dash = $(this),
                challengeId = dash.val();

            $('#atDashData').hide().after('<div class="loading ls-at-spinner"></div>');
            dash.attr('disabled', 'disabled');

            $.post(ajaxurl, {
                    action: 'atDashboardFigures',
                    challengeId: challengeId
                }
            ).done(function(response) {

                if(response) {
                    $('#atDbActivities').html(response.activities);
                    $('#atDbParticipants').html(response.participants);
                    $('#atDbAvgParticipants').html(response.avgParticipants);
                    $('#atDbTotalPerforms').html(response.totalPerforms);
                    $('#atDbPerformsLastMonth').html(response.performsLastMonth);
                }

                $('.ls-dash .loading').remove();
                $('#atDashData').show();
                dash.removeAttr('disabled');
            });

        });
    }
    
    initActivitiesChart(
        '.at-activity',
        '#atActivitiesMultipleSelect',
        '#statsSD',
        '#statsED',
        'atActivities',
        {
            challengeId: '#atForActivity',
            companyId: '#companyIdForActivity'
        }
    );

});