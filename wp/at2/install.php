<?php
function at2InstallLoyaltySuite()
{
    $wpdb = LS()->wpdb;

    $tb = $wpdb->get_var(sprintf("SHOW TABLES LIKE '{$wpdb->prefix}ls_at_challenges'"));
    if(empty($tb)) {
        $wpdb->query(sprintf("CREATE TABLE `{$wpdb->prefix}ls_at_challenges` (
        	`challengeId` INT(9) UNSIGNED NOT NULL AUTO_INCREMENT,
			`title` VARCHAR(500) NOT NULL,
	        `challengePageId` INT(10) UNSIGNED NOT NULL,
			`dateStart` DATE NOT NULL,
	        `dateEnd` DATE NOT NULL,
			`goalMessageText1` TEXT NOT NULL,
			`goalValue1` TINYINT(3) UNSIGNED NOT NULL,
			`goalMessageText2` TEXT NOT NULL,
			`goalValue2` TINYINT(3) UNSIGNED NOT NULL,
			`goalMessageText3` TEXT NOT NULL,
			`goalValue3` TINYINT(3) UNSIGNED NOT NULL,
			`goalMessageText4` TEXT NOT NULL,
			`goalValue4` TINYINT(3) UNSIGNED NOT NULL,
			`challengeMessage1` TEXT NOT NULL,
			`messageRemark1` VARCHAR(512) NOT NULL,
			`challengeMessage2` TEXT NOT NULL,
			`messageRemark2` VARCHAR(512) NOT NULL,
			`createDate` INT(10) UNSIGNED NOT NULL,
        	PRIMARY KEY (`challengeId`)
        ) %s ENGINE=InnoDB", LS\Library::getCharsetCollate()));
    }

    $tb = $wpdb->get_var(sprintf("SHOW TABLES LIKE '{$wpdb->prefix}ls_at_consumers_challenges'"));
    if(empty($tb)) {
        $wpdb->query(sprintf("CREATE TABLE `{$wpdb->prefix}ls_at_consumers_challenges` (
            `consumerId` INT(10) UNSIGNED NOT NULL,
			`challengeId` INT(9) UNSIGNED NOT NULL,
			`enterDate` INT(10) UNSIGNED NOT NULL,
          PRIMARY KEY (`consumerId`,`challengeId`)
        ) %s ENGINE=InnoDB", LS\Library::getCharsetCollate()));
    }

    $tb = $wpdb->get_var(sprintf("SHOW TABLES LIKE '{$wpdb->prefix}ls_at_connections'"));
    if(empty($tb)) {
        $wpdb->query(sprintf("CREATE TABLE `{$wpdb->prefix}ls_at_connections` (
        	`connectionId` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
			`consumerId` INT(10) UNSIGNED NOT NULL,
			`challengeId` INT(9) UNSIGNED NOT NULL,
			`activityId` INT(10) UNSIGNED NOT NULL,
			`createDate` INT(10) UNSIGNED NOT NULL,
        	PRIMARY KEY (`connectionId`),
        	UNIQUE INDEX `unique_connection` (`consumerId`, `challengeId`, `activityId`),
        	INDEX `consumerId` (`consumerId`),
        	INDEX `challengeId` (`challengeId`),
        	INDEX `activityId` (`activityId`)
        ) %s ENGINE=InnoDB", LS\Library::getCharsetCollate()));
    }

    $tb = $wpdb->get_var(sprintf("SHOW TABLES LIKE '{$wpdb->prefix}ls_at_activities'"));
    if(empty($tb)) {
        $wpdb->query(sprintf("CREATE TABLE `{$wpdb->prefix}ls_at_activities` (
        	`activityId` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
			`challengeId` INT(9) UNSIGNED NOT NULL,
			`title` VARCHAR(250) NOT NULL,
			`description` VARCHAR(1024) NOT NULL,
			`image` VARCHAR(1024) NOT NULL,
			`link` VARCHAR(1024) NOT NULL,
			`action` VARCHAR(40) NOT NULL,
			`ident` VARCHAR(1024) NOT NULL,
			`order` TINYINT(3) UNSIGNED NOT NULL,
			`successMessage` TEXT NOT NULL,
        	PRIMARY KEY (`activityId`),
        	INDEX `challengeId` (`challengeId`)
        ) %s ENGINE=InnoDB", LS\Library::getCharsetCollate()));
    }
}