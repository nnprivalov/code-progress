<?php
class challengeStatisticTableAT2LoyaltySuite extends LS\WP_List_Table
{
    protected $_column_headers;
    public $referer;
    protected $challengeId;

    public function __construct($challengeId)
    {
        $this->challengeId = $challengeId;

        parent::__construct([
            'singular' => 'wp_ls_challenge_statistic',
            'plural'   => 'wp_ls_challenge_statistic',
            'hash'     => '#at-challenge-statistics'
        ]);
    }

    public function get_columns()
    {
        return [
            'number'           => '#',
            'consumer'         => __('Consumer name', 'ls'),
            'email'            => __('Email', 'ls'),
            'passedActivities' => __('Passed activities', 'ls'),
            'enterDate'        => __('Enter date', 'ls'),
            'lastActivity'     => __('Last activity', 'ls')
        ];
    }

    public function get_sortable_columns()
    {
        return [
            'consumer'         => ['consumer', false],
            'email'            => ['email', false],
            'passedActivities' => ['passedActivities', true],
            'enterDate'        => ['enterDate', true],
            'lastActivity'     => ['lastActivity', true]
        ];
    }

    public function filter_box()
    {
        $filters = [
            ['consumer', 'string'],
            ['email', 'string'],
            ['passedActivities', 'value'],
            ['enterDate', 'date'],
            ['lastActivity', 'date']
        ];

        if(LS()->isModuleActive('companies')) {
            $filters[] = ['companyId', 'select', LSCompaniesHelper::getCompaniesList(false, true), __('Company', 'ls')];
        }

        $this->__filter_box($filters, 3);
    }

    public function prepare_items()
    {
        $options = $this->get_table_options();
        $records = AT2Model::getChallengeStatistic($this->challengeId, 20, $options['page'], $options['orderBy'], $options['order'], $options['filterTerms']);

        $this->set_pagination_args([
            "total_items" => $records['totalCount'],
            "total_pages" => $records['totalPages'],
            "per_page"    => $records['itemsPerPage']
        ]);

        $this->items = $records['rows'];
        $this->_column_headers = [$this->get_columns(), [], $this->get_sortable_columns()];
    }

    public function column_default($item, $columnName)
    {
        switch ($columnName) {
            case 'number' :
            case 'passedActivities' :
                return (int) $item->{$columnName};
            case 'consumer' :
                return '<a href="' . consumersAdminLoyaltySuite::consumerEditLink($item->consumerId, $this->referer) . '">' . ls_kses(consumersLoyaltySuite::getConsumerNiceName((int) $item->consumerId)) . '</a>';
            case 'email' :
                return empty($item->email) ? ' ' : ls_kses($item->email);
            case 'enterDate' :
            case 'lastActivity' :
                return empty($item->{$columnName}) ? __('No limit', 'ls') : date('d.m.Y', $item->{$columnName} + TIMEZONE_DIFF);
            default:
                return ' ';
        }
    }
}