<?php
class challengesTableAT2LoyaltySuite extends LS\WP_List_Table
{
    protected $_column_headers;
    public $referer;

    public function __construct()
    {
        parent::__construct([
            'singular' => 'wp_ls_challenge',
            'plural'   => 'wp_ls_challenges',
            'hash'     => '#ls-challenges'
        ]);
    }

    public function get_columns()
    {
        $cols = [
            'cb'              => '<input type="checkbox" />',
            'challengeId'     => 'ID',
            'title'           => __('Title', 'ls'),
            'challengePageId' => __('Challenge page', 'ls'),
            'active'          => __('Active', 'ls'),
            'dateStart'       => __('Date from', 'ls'),
            'dateEnd'         => __('Date to', 'ls')
        ];

        if(current_user_can('ls_at_manage')) {
            $cols['details'] = '';
        }

        return $cols;
    }

    public function get_sortable_columns()
    {
        return [
            'challengeId'     => ['challengeId', true],
            'title'           => ['title', false],
            'active'          => ['active', true],
            'dateStart'       => ['dateStart', false],
            'dateEnd'         => ['dateEnd', false]
        ];
    }

    public function prepare_items()
    {
        $options = $this->get_table_options();
        $records = AT2Model::paginateChallenges(20, $options['page'], $options['orderBy'], $options['order']);

        $this->set_pagination_args([
            "total_items" => $records['totalCount'],
            "total_pages" => $records['totalPages'],
            "per_page"    => $records['itemsPerPage']
        ]);

        $this->items = $records['rows'];
        $this->_column_headers = [$this->get_columns(), [], $this->get_sortable_columns()];
    }

    public function get_bulk_actions()
    {
        if(current_user_can('ls_at_manage')) {
            $actions = [
                'duplicate-challenge' => __('Duplicate', 'ls'),
                'delete-challenge'    => __('Delete', 'ls')
            ];
        } else {
            $actions = [];
        }

        return $actions;
    }

    public function column_cb($item)
    {
        return sprintf('<input type="checkbox" name="challengeId[]" value="%s" />', $item->challengeId);
    }

    public function column_default($item, $columnName)
    {
        switch ($columnName) {
            case 'challengeId' :
            case 'title' :
                return empty($item->{$columnName}) ? ' ' : $item->{$columnName};
            case 'challengePageId';
                $link = get_permalink($item->challengePageId);
                $shortLink = strlen($link) > 50 ? substr($link, 0, 50) . '...' : $link;
                return '<a href="' . esc_url($link) . '" target="_blank">' . $shortLink . '</a>';
            case 'dateStart' :
            case 'dateEnd' :
                return date('d.m.Y', strtotime($item->{$columnName}));
            case 'active' :
                return $item->active > 0 ? '<span class="ls-sign-plus">+</span>' : ' ';
            case 'details' :
                return '<a href="' . at2AdminLoyaltySuite::challengeEditLink($item->challengeId, $this->referer) . '" class="button">' . __('Details') . '</a>';
            default:
                return ' ';
        }
    }
}