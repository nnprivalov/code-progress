<?php
class statisticsTableAT2LoyaltySuite extends LS\WP_List_Table
{
    protected $_column_headers;
    public $referer;

    public function __construct()
    {
        parent::__construct([
            'singular' => 'wp_ls_statistic',
            'plural'   => 'wp_ls_statistics',
            'hash'     => '#ls-statistics'
        ]);
    }

    public function get_columns()
    {
        return [
            'cb'            => '<input type="checkbox" />',
            'challengeId'   => __('Challenge ID', 'ls'),
            'title'         => __('Challenge (title)', 'ls'),
            'date'          => __('Date (from-to)', 'ls'),
            'persons'       => __('Participating persons', 'ls'),
            'details'       => __('Details', 'ls')
        ];
    }

    public function get_sortable_columns()
    {
        return [
            'title' => ['title', true]
        ];
    }

    public function prepare_items()
    {
        $options = $this->get_table_options();
        $records = AT2Model::getStatistics($options['order']);

        $this->set_pagination_args([
            "total_items" => $records['totalCount'],
            "total_pages" => $records['totalPages'],
            "per_page"    => $records['itemsPerPage']
        ]);

        $this->items = $records['rows'];
        $this->_column_headers = [$this->get_columns(), [], $this->get_sortable_columns()];
    }

    public function column_cb($item)
    {
        return sprintf('<input type="checkbox" name="challengeId[]" value="%s" />', $item->challengeId);
    }

    public function column_default($item, $columnName)
    {
        switch($columnName) {
            case 'challengeId' :
                return (int) $item->challengeId;
            case 'title' :
                return empty($item->title) ? ' ' : $item->title;
            case 'date' :
                return date('d.m.Y', strtotime($item->dateStart)) . ' - ' . date('d.m.Y', strtotime($item->dateEnd));
            case 'persons' :
                return $item->{$columnName};
            case 'details' :
                return '<a href="' . at2AdminLoyaltySuite::challengeEditLink($item->challengeId, $this->referer) . '#at-challenge-statistics" class="button">' . __('Details', 'ls') . '</a>';
            default:
                return ' ';
        }
    }
}