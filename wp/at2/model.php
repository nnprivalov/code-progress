<?php
/**
 * Class AT2Model
 * Activity Tracking model
 */
class AT2Model
{
    /** @var array Active Challenges */
    private static $activeChallenges;

    /** @var array A list of connections between the consumer id <-> challenge id */
    private static $consumerChallenges = [];

    /** @var array A list of consumer activities in each challenge */
    private static $consumerActivities = [];

    /** @var array A list of challenge activities */
    private static $challengeActivities = [];

    /**
     * Clean all pre-saved data for this model
     * @since 2.9.3
     */
    private static function cleanCaches()
    {
        self::$activeChallenges =
        self::$consumerChallenges =
        self::$consumerActivities =
        self::$challengeActivities = null;
    }

    /**
     * Delete module tables
     * @since 2.0
     */
    public static function deleteTables()
    {
        $wpdb = LS()->wpdb;

        foreach(['ls_at_consumers_challenges', 'ls_at_connections', 'ls_at_challenges', 'ls_at_activities'] as $t) {
            $wpdb->query(sprintf("DROP TABLE IF EXISTS %s", $wpdb->prefix . $t));
        }

        self::cleanCaches();
    }

    /**
     * Retrieve default values for new challenge
     * @since 2.6.0
     * @return object A list of default values
     */
    public static function getDefaultChallengeData()
    {
        return (object) [
            'challengeId'       => 0,
            'title'             => '',
            'challengePageId'   => 0,
            'dateStart'         => date_i18n('Y-m-d'),
            'dateEnd'           => 0,
            'goalMessageText1'  => '',
            'goalValue1'        => '',
            'goalMessageText2'  => '',
            'goalValue2'        => '',
            'goalMessageText3'  => '',
            'goalValue3'        => '',
            'goalMessageText4'  => '',
            'goalValue4'        => '',
            'challengeMessage1' => '',
            'messageRemark1'    => '',
            'challengeMessage2' => '',
            'messageRemark2'    => '',
            'createDate'        => time()
        ];
    }

    /**
     * Retrieve a list of active challenges
     *
     * @since 2.0
     *
     * @param bool $idsOnly True to return ID's only
     * @param null|string $forDate Define the date to retrieve the active challenges. By default is for today
     * @return array A list of active challenges
     */
    public static function getActiveChallenges($idsOnly = false, $forDate = null)
    {
        $wpdb = LS()->wpdb;

        if(self::$activeChallenges === null || $forDate !== null) {

            $date = $forDate === null ? date('Y-m-d') : esc_sql($forDate);
            $result = $wpdb->get_results(sprintf("SELECT * FROM {$wpdb->prefix}ls_at_challenges WHERE '%s' BETWEEN dateStart AND dateEnd ORDER BY dateStart ASC", $date));
            foreach($result as $key => $ch) {
                $result[$key]->active = true;
                $result[$key]->challengeId = (int) $ch->challengeId;
            }

            self::$activeChallenges = $result;
        }

        if($idsOnly) {
            return array_map(static function($ch) {
                return $ch->challengeId;
            }, self::$activeChallenges);
        }

        return self::$activeChallenges;
    }

    /**
     * Retrieve challenge
     *
     * @since 2.0
     *
     * @param int $challengeId Challenge ID
     * @return object|false Challenge object or false on failure
     */
    public static function getChallenge($challengeId)
    {
        if($challengeId < 1) {
            return false;
        }

        foreach(self::getActiveChallenges() as $challenge) {
            if($challengeId == $challenge->challengeId) {
                $challenge->active = true;
                return $challenge;
            }
        }

        $wpdb = LS()->wpdb;
        $challenge = $wpdb->get_row(sprintf("SELECT * FROM {$wpdb->prefix}ls_at_challenges WHERE challengeId = %d LIMIT 1", $challengeId));

        if(!empty($challenge)) {
            $challenge->active = false;
            $challenge->challengeId = (int) $challenge->challengeId;
        }

        return $challenge;
    }

    /**
     * Check if specified challenge is active
     *
     * @since 2.0
     *
     * @param int $challengeId Challenge ID
     * @return bool True if specified challenge is active
     */
    public static function isChallengeActive($challengeId)
    {
        return $challengeId > 0 ? in_array($challengeId, self::getActiveChallenges(true)) : false;
    }

    /**
     * Retrieve challenges by specified conditions and filters
     *
     * @since 1.0
     *
     * @param int $itemsPerPage Number of challenges to retrieve
     * @param int $page Page number for pagination. To get all challenges without pagination set this value to null
     * @param string $orderBy Field to order
     * @param string $order Order direction
     * @param array $filters Filters
     * @return array Array(
     *   'rows' - challenges list,
     *   'totalCount' - total number of found challenges,
     *   'totalPages' - total number of pages (for pagination),
     *   'itemsPerPage' - number of challenges per page
     * )
     */
    public static function paginateChallenges($itemsPerPage = 20, $page = 1, $orderBy = '', $order = 'ASC', $filters = [])
    {
        $wpdb = LS()->wpdb;
        $order = empty($order) ? 'DESC' : $order;

        if(in_array($orderBy, ['title', 'dateStart', 'dateEnd'])) {
            $orderBy = 'ch.' . $orderBy;
        } else {
            $orderBy = sprintf("( '%s' BETWEEN ch.dateStart AND ch.dateEnd )", date('Y-m-d'));
        }

        $limit = '';

        if($page > 0 && $itemsPerPage > 0) {
            $limit = sprintf(" LIMIT %d, %d", ($page - 1) * $itemsPerPage, $itemsPerPage);
        }

        $whereConditions = [];

        if(!empty($filters['started'])) {
            $whereConditions[] = sprintf("ch.dateStart = '%s'", date('Y-m-d'));
        }

        if(!empty($filters['ended'])) {
            $whereConditions[] = sprintf("ch.dateEnd = '%s'", date('Y-m-d', strtotime('YESTERDAY')));
        }

        $where = empty($whereConditions) ? '' : ' WHERE ' . implode(' AND ', $whereConditions);

        $query = sprintf("SELECT SQL_CALC_FOUND_ROWS ch.* FROM {$wpdb->prefix}ls_at_challenges as ch %s ORDER BY %s %s %s", $where, $orderBy, $order, $limit);
        $records = $wpdb->get_results($query);
        $totalCount = (int) $wpdb->get_var("SELECT FOUND_ROWS()");
        $itemsPerPage = $itemsPerPage > 0 ? $itemsPerPage : $totalCount;
        $totalPages = $totalCount > 0 && $itemsPerPage > 0 ? ceil($totalCount / $itemsPerPage) : 0;

        foreach($records as $key => $record) {
            $records[$key]->active = strtotime($record->dateStart) <= strtotime('TODAY', current_time('timestamp')) && strtotime($record->dateEnd) >= strtotime('TODAY', current_time('timestamp'));
            $records[$key]->challengeId = (int) $records[$key]->challengeId;
        }

        return [
            'rows'         => $records,
            'totalCount'   => $totalCount,
            'totalPages'   => $totalPages,
            'itemsPerPage' => $itemsPerPage
        ];
    }

    /**
     * Retrieve challenges by specified conditions and filters
     *
     * @since 1.0
     *
     * @param array $filterTerms Filters
     * @return array Challenges
     */
    public static function getChallenges($filterTerms = [])
    {
        return self::paginateChallenges(0, 0, '', '', $filterTerms)['rows'];
    }

    /**
     * Retrieve started (today) challenges
     * @since 2.4.3
     * @return array
     */
    public static function getStartedChallenges()
    {
        return self::getChallenges(['started' => true]);
    }

    /**
     * Retrieve ended (today) challenges
     * @since 2.4.3
     * @return array
     */
    public static function getEndedChallenges()
    {
        return self::getChallenges(['ended' => true]);
    }

    /**
     * Retrieve a list of all challenges key => title
     * @since 2.0
     * @return array A list of challenges
     */
    public static function getChallengesList()
    {
        $wpdb = LS()->wpdb;
        $challenges = $wpdb->get_results(sprintf("SELECT challengeId, title FROM {$wpdb->prefix}ls_at_challenges ORDER BY createDate DESC"));

        $result = [];

        foreach($challenges as $challenge) {
            $result[(int) $challenge->challengeId] = $challenge->title;
        }

        return $result;
    }

    /**
     * Retrieve challenge activities
     *
     * @since 2.0
     *
     * @param int $challengeId Challenge ID
     * @return array A list of challenge activities as arrays
     */
    public static function getChallengeActivities($challengeId)
    {
        if($challengeId < 1) {
            return [];
        }

        if(!isset(self::$challengeActivities[$challengeId])) {
            $wpdb = LS()->wpdb;
            self::$challengeActivities[$challengeId] = $wpdb->get_results(sprintf("SELECT * FROM {$wpdb->prefix}ls_at_activities WHERE challengeId = %d ORDER BY `order` ASC", $challengeId), ARRAY_A);
        }

        return self::$challengeActivities[$challengeId];
    }

    /**
     * Save challenge
     *
     * @since 2.0
     *
     * @param array $challenge Challenge details
     * @param array $activities Challenge activities
     * @return int|false Challenge ID or false on failure
     */
    public static function saveChallenge($challenge, $activities)
    {
        if(empty($challenge)) {
            return false;
        }

        if(!apply_filters('ls_at_before_save_challenge', true, $challenge, $activities)) {
            return false;
        }

        $wpdb = LS()->wpdb;
        $challengeId = $challenge['challengeId'] ?? 0;
        $data = $challenge;

        unset($data['challengeId']);

        if(isset($data['dateStart'])) {
            $data['dateStart'] = is_numeric($data['dateStart']) ? date('Y-m-d', $data['dateStart']) : $data['dateStart'];
        }

        if(isset($data['dateEnd'])) {
            $data['dateEnd'] = is_numeric($data['dateEnd']) ? date('Y-m-d', $data['dateEnd']) : $data['dateEnd'];
        }

        if($challengeId > 0) {
            $wpdb->update($wpdb->prefix . 'ls_at_challenges', $data, ['challengeId' => $challengeId], null, '%d');
        } else {
            $data['createDate'] = time();
            if($wpdb->insert($wpdb->prefix . 'ls_at_challenges', $data)) {
                $challengeId = (int) $wpdb->insert_id;
            }
        }

        if($challengeId > 0) {

            $activitiesIds = [];
            foreach($activities as $activity) {
                if(isset($activity['activityId']) && $activity['activityId'] > 0) {
                    $activitiesIds[] = $activity['activityId'];
                }
            }

            // delete unused activities
            foreach(self::getChallengeActivities($challengeId) as $a) {
                if(!in_array($a['activityId'], $activitiesIds)) {
                    self::deleteActivity($a['activityId']);
                }
            }

            foreach($activities as $activity) {

                $activityId = $activity['activityId'] ?? 0;

                $activityData = [
                    'challengeId'    => (int) $challengeId,
                    'title'          => $activity['title'],
                    'description'    => $activity['description'],
                    'image'          => esc_url($activity['image']),
                    'link'           => esc_url($activity['link']),
                    'successMessage' => $activity['successMessage'] ?? '',
                    'action'         => $activity['action'],
                    'ident'          => isset($activity['ident']) ? sanitize_text_field($activity['ident']) : '',
                    'order'          => (int) $activity['order']
                ];

                if($activityId > 0) {
                    $wpdb->update($wpdb->prefix . 'ls_at_activities', $activityData, ['activityId' => $activityId], null, '%d');
                } else {
                    if($wpdb->insert($wpdb->prefix . 'ls_at_activities', $activityData)) {
                        $activityId = (int) $wpdb->insert_id;
                    }
                }

                do_action('ls_at_save_activity', $activityId, $activity);
            }

            self::cleanCaches();

            do_action('ls_at_save_challenge', $challengeId, $data);
        }

        return $challengeId;
    }

    /**
     * Delete Challenge(s) by specified ID's
     *
     * @since 2.0
     *
     * @param array $challenges A list of challenges ID's
     * @return bool True if challenges have been deleted
     */
    public static function deleteChallengesList($challenges = [])
    {
        if(!empty($challenges) && is_array($challenges)) {

            $wpdb = LS()->wpdb;

            foreach(['ls_at_consumers_challenges', 'ls_at_connections', 'ls_at_activities', 'ls_at_challenges'] as $t) {
                $wpdb->query(sprintf("DELETE FROM %s WHERE challengeId IN (%s)", $wpdb->prefix . $t, implode(',', $challenges)));
            }

            foreach($challenges as $challengeId) {
                do_action('ls_at_challenge_removed', $challengeId);
            }

            return true;
        }

        return false;
    }

    /**
     * Start the challenge by consumer
     *
     * @since 2.0
     *
     * @param int $challengeId Challenge ID
     * @param int $consumerId Consumer ID
     * @return bool True if relation between consumer and challenge has been created
     */
    public static function startChallenge(int $challengeId, int $consumerId): bool
    {
        if($challengeId < 1 || $consumerId < 1) {
            return false;
        }

        $data = [
            'consumerId'  => $consumerId,
            'challengeId' => $challengeId,
            'enterDate'   => time()
        ];

        $wpdb = LS()->wpdb;
        $success = $wpdb->insert($wpdb->prefix . 'ls_at_consumers_challenges', $data) > 0;

        if($success) {

            self::cleanCaches();

            \LS\ConsumersHelper::insertLog('at_challenge', $consumerId, $challengeId, $data['enterDate']);

            do_action('ls_at_start_challenge', $challengeId, $consumerId);
        }

        return $success;
    }

    /**
     * Check if challenge started by consumer
     *
     * @since 2.0
     *
     * @param int $challengeId Challenge ID
     * @param int $consumerId Consumer ID
     * @return bool True if challenge started by consumer
     */
    public static function isChallengeStartedByConsumer($challengeId, $consumerId)
    {
        if($challengeId < 1 || $consumerId < 1) {
            return false;
        }

        if(!isset(self::$consumerChallenges[$consumerId])) {

            $wpdb = LS()->wpdb;
            $challenges = $wpdb->get_col(sprintf("SELECT challengeId FROM {$wpdb->prefix}ls_at_consumers_challenges WHERE consumerId = %d", $consumerId));

            self::$consumerChallenges[$consumerId] = [];

            foreach($challenges as $chId) {
                self::$consumerChallenges[$consumerId][] = (int) $chId;
            }
        }

        return in_array($challengeId, self::$consumerChallenges[$consumerId]);
    }

    /**
     * Check if consumer participated in active challenge
     *
     * @since 2.0
     *
     * @param int $challengeId Challenge ID
     * @param int $consumerId Consumer ID
     * @return bool True if consumer participated in active challenge
     */
    public static function isConsumerInActiveChallenge($challengeId, $consumerId)
    {
        return
            self::isChallengeActive($challengeId)
            && self::isChallengeStartedByConsumer($challengeId, $consumerId);
    }

    /**
     * Check if consumer finished challenge
     *
     * @since 2.13.2
     *
     * @param int $challengeId Challenge Id
     * @param int $consumerId Consumer Id
     * @return bool True if consumer finished challenge
     */
    public static function isConsumerFinishedChallenge($challengeId, $consumerId)
    {
        return
            self::isChallengeStartedByConsumer($challengeId, $consumerId)
               && self::getOpenActivitiesCount($challengeId, $consumerId) === 0;
    }

    /**
     * Retrieve the number of finished challenges by the consumer
     *
     * @since 2.13.2
     *
     * @param int $consumerId Consumer Id
     * @return int Number of finished challenges
     */
    public static function getNumberOfFinishedChallengesByConsumer($consumerId)
    {
        $finished = 0;

        foreach(self::getConsumerParticipatedChallengesIds($consumerId) as $challengesId) {
            if(self::isConsumerFinishedChallenge($challengesId, $consumerId)) {
                $finished++;
            }
        }

        return $finished;
    }

    /**
     * Calculate consumer progress in the challenge
     *
     * @since 2.0
     *
     * @param int $challengeId Challenge ID
     * @param int $consumerId Consumer ID
     * @return array Array ('totalActivities' - total activities in the challenge; 'passedActivities' - passed activities by the consumer 'consumerProgress' - consumer progress in %)
     */
    public static function calculateConsumerProgress($challengeId, $consumerId)
    {
        $totalActivities = self::getTotalActivitiesInChallenge($challengeId);
        $passedActivities = self::getPassedActivitiesCount($challengeId, $consumerId);
        $consumerProgress = $passedActivities >= $totalActivities ? 100 : ($totalActivities > 0 ? $passedActivities / $totalActivities * 100 : 0); // in %

        return [
            'totalActivities'  => $totalActivities,
            'passedActivities' => $passedActivities,
            'consumerProgress' => round($consumerProgress)
        ];
    }

    /**
     * Retrieve total number of consumers participated in challenge
     *
     * @since 2.0
     *
     * @param int $challengeId Challenge ID
     * @return int Number of consumers
     */
    public static function getTotalConsumersInChallenge($challengeId)
    {
        if($challengeId < 1) {
            return 0;
        }

        $wpdb = LS()->wpdb;

        return (int) $wpdb->get_var(sprintf("SELECT COUNT(consumerId) FROM {$wpdb->prefix}ls_at_consumers_challenges WHERE challengeId = %d", $challengeId));
    }

    /**
     * Retrieve an array of consumer challenge participations
     *
     * @since 2.16.0
     *
     * @param int $consumerId Consumer ID
     * @return array A list of consumer challenge participations
     */
    public static function getConsumerChallengeParticipations($consumerId)
    {
        if($consumerId < 1) {
            return [];
        }

        $wpdb = LS()->wpdb;

        return $wpdb->get_results(sprintf("SELECT * FROM {$wpdb->prefix}ls_at_consumers_challenges WHERE consumerId = %d", $consumerId));
    }

    /**
     * Retrieve consumer details in the challenge
     *
     * @since 2.0
     *
     * @param int $challengeId Challenge ID
     * @param int $consumerId Consumer ID
     * @return object|false Consumer details (object) or false on failure
     */
    public static function getConsumerChallengeDetails($challengeId, $consumerId)
    {
        $challenges = array_filter(self::getConsumerChallengeParticipations($consumerId), static function($record) use ($challengeId) {
            return $record->challengeId == $challengeId;
        });

        return empty($challenges) ? false : $challenges[0];
    }

    /**
     * Retrieve a list of challenges ID's in which consumer is participated
     *
     * @since 2.0
     *
     * @param int $consumerId Consumer ID
     * @return int[] A list of challenges ID's
     */
    public static function getConsumerParticipatedChallengesIds($consumerId)
    {
        return array_map(static function($record) {
            return (int) $record->challengeId;
        }, self::getConsumerChallengeParticipations($consumerId));
    }

    /**
     * Retrieve a list of consumers ID's by challenge ID
     *
     * @since 2.6.5
     *
     * @param int $challengeId Challenge ID
     * @return int[] A list of consumers ID's
     */
    public static function getConsumersInChallenge($challengeId)
    {
        if($challengeId < 1) {
            return [];
        }

        $wpdb = LS()->wpdb;

        return $wpdb->get_col(sprintf("SELECT consumerId FROM {$wpdb->prefix}ls_at_consumers_challenges WHERE challengeId = %d", $challengeId));
    }

    /**
     * Retrieve consumer goal level in challenge
     *
     * @since 2.15.4
     *
     * @param int $challengeId Challenge Id
     * @param int $consumerId Consumer Id
     * @return int Level: 0-4 (0 = undefined)
     */
    public static function getConsumerGoalLevelInChallenge($challengeId, $consumerId)
    {
        $level = 0;
        $challenge = self::getChallenge($challengeId);
        $progress = self::calculateConsumerProgress($challengeId, $consumerId)['consumerProgress'];

        for($i = 1; $i <= 4; $i++) {
            $goalValue = (int) $challenge->{'goalValue' . $i};
            if($progress >= $goalValue) {
                $level = $i;
            }
        }

        return $level;
    }

    /**
     * Retrieve consumer goal message in challenge
     *
     * @since 2.0
     *
     * @param int $challengeId Challenge ID
     * @param int $consumerId Consumer ID
     * @return false|string Consumer goal message or false on failure
     */
    public static function getConsumerGoalMessageInChallenge($challengeId, $consumerId)
    {
        if($challengeId < 1 || $consumerId < 1) {
            return false;
        }

        $message = '';
        $challenge = self::getChallenge($challengeId);
        $consumerProgress = self::calculateConsumerProgress($challengeId, $consumerId);

        for($i = 1; $i <= 4; $i++) {
            $goalValue = $challenge->{'goalValue' . $i};
            if($consumerProgress['consumerProgress'] >= $goalValue) {
                $message = $challenge->{'goalMessageText' . $i};
            }
        }

        return empty($message) ? '' : do_shortcode($message);
    }

    /**
     * Retrieve challenges statistics
     *
     * @since 2.0
     *
     * @param string $order Challenges list order
     * @return array Array(
     *   'rows' - challenges list with statistics,
     *   'totalCount' - total number of found challenges,
     *   'totalPages' - total number of pages (for pagination),
     *   'itemsPerPage' - number of challenges per page
     * )
     */
    public static function getStatistics($order = 'ASC')
    {
        $wpdb = LS()->wpdb;

        $order = empty($order) ? 'ASC' : $order;
        $query = sprintf("SELECT ch.challengeId, ch.title, ch.dateStart, ch.dateEnd
                          FROM {$wpdb->prefix}ls_at_challenges as ch
                          INNER JOIN {$wpdb->prefix}ls_at_activities as a ON a.challengeId = ch.challengeId
                          GROUP BY ch.challengeId
                          ORDER BY ch.title %s", $order);
        $challengePcps = $wpdb->get_results($query, OBJECT_K);

        $query = sprintf("SELECT challengeId, COUNT(*) as persons 
                          FROM {$wpdb->prefix}ls_at_consumers_challenges 
                          GROUP BY challengeId");
        $persons = $wpdb->get_results($query, OBJECT_K);

        foreach($challengePcps as $key => $record) {
            $challengePcps[$key]->persons = (int) (isset($persons[$key]) ? $persons[$key]->persons : 0);
            $challengePcps[$key]->challengeId = (int) $record->challengeId;
        }

        return [
            'rows'         => $challengePcps,
            'totalCount'   => count($challengePcps),
            'totalPages'   => 1,
            'itemsPerPage' => count($challengePcps)
        ];
    }

    /**
     * Retrieve challenge dashboard for the dashboard
     *
     * @since 2.9.0
     *
     * @param int $challengeId Challenge Id
     * @param null|int $today Today date (timestamp) or null to use current day
     * @return array[string]int Dashboard figures
     */
    public static function getDashboard($challengeId, $today = null)
    {
        $today = $today ?? strtotime('TODAY');
        $monthAgo = strtotime('today -1 month');

        $default = [
            'today'             => $today,
            'activities'        => 0,
            'participants'      => 0,
            'avgParticipants'   => 0,
            'totalPerforms'     => 0,
            'performsLastMonth' => 0
        ];

        if($challengeId < 1) {
            return $default;
        }

        $result = $default;

        $result['activities'] = self::getTotalActivitiesInChallenge($challengeId);
        $result['participants'] = self::getTotalConsumersInChallenge($challengeId);

        $totalConsumers = \LS\ConsumersHelper::getConsumersCount(['active' => true, 'verified' => true]);
        $result['avgParticipants'] = $totalConsumers > 0 ? $result['participants'] / $totalConsumers * 100 : 0;

        $wpdb = LS()->wpdb;

        $query = sprintf("SELECT COUNT(DISTINCT connectionId) FROM {$wpdb->prefix}ls_at_connections WHERE challengeId = %d", $challengeId);
        $result['totalPerforms'] = (int) $wpdb->get_var($query);

        $query = sprintf("SELECT COUNT(DISTINCT connectionId) FROM {$wpdb->prefix}ls_at_connections WHERE challengeId = %d AND createDate >= %d", $challengeId, $monthAgo);
        $result['performsLastMonth'] = (int) $wpdb->get_var($query);

        return $result;
    }

    /**
     * Retrieve challenge activities stats
     *
     * @since 2.9.0
     *
     * @param int $challengeId Challenge Id
     * @param int $startDate Start date
     * @param int $endDate End date
     * @param array $columns Columns to fetch
     * @param int $companyId Company Id
     * @return array Results
     */
    public static function getStats($challengeId, $startDate = 0, $endDate = 0, $columns = [], $companyId = 0)
    {
        if($startDate < 1 || $startDate > $endDate || $endDate >= strtotime('TOMORROW')) {
            $startDate = strtotime('TODAY -1 MONTH');
            $endDate = strtotime('TODAY');
        }

        // round to start of the day
        $startDate = strtotime('TODAY', $startDate);
        $endDate = strtotime('TODAY', $endDate);

        $sd = $startDate;
        $ed = $endDate;
        $possibleColumns = ['participants', 'activities'];
        $columns = empty($columns) ? $possibleColumns : array_values(array_unique(array_intersect($columns, $possibleColumns)));
        $monthInterval = strtotime(date('Y-m-d', $endDate) . ' -1 YEAR') >= $startDate; // switch view to months
        $weekInterval = !$monthInterval && strtotime(date('Y-m-d', $endDate) . '-3 MONTH') >= $startDate; // switch view to weeks

        // format start date depends on the range
        if($monthInterval) {
            $startDate = strtotime(date('Y-m-01', $startDate)); // round to start of the month
        } elseif($weekInterval) {
            $startDate = LS\Library::getMonday($startDate); // round to start of the week
        }

        // prepare group condition depends on the range
        $groupBy = static function($field) use ($monthInterval, $weekInterval) {

            if($monthInterval) {
                return sprintf("CONCAT(YEAR(FROM_UNIXTIME(%s)), MONTH(FROM_UNIXTIME(%s)))", $field, $field); // take every 1th day of the month
            }

            if($weekInterval) {
                return sprintf("CONCAT(YEAR(FROM_UNIXTIME(%s)), WEEKOFYEAR(FROM_UNIXTIME(%s)))", $field, $field); // take every Monday
            }

            return sprintf("DATE(FROM_UNIXTIME(%s))", $field); // take every Monday
        };

        $data = $labels = [];
        $wpdb = LS()->wpdb;

        foreach($columns as $key => $column) {
            $columns[$key] = esc_sql($column);
        }

        if(!empty($columns) && $challengeId > 0) {

            $instantData = $dbData = [];

            $arrayColumn = static function($data) {
                $result = [];
                foreach($data as $row) {
                    $result[$row->eventDate] = $row->items;
                }
                return $result;
            };

            $companyJoin = static function($consumerField) use ($companyId, $wpdb) {

                if($companyId === 0 || !LS()->isModuleActive('companies')) {
                    return '';
                }

                $companiesList = LSCompaniesHelper::includeChildrenCompaniesIds((int) $companyId);

                return sprintf("INNER JOIN {$wpdb->prefix}ls_consumers c ON c.consumerId = %s AND c.companyId IN (%s)", $consumerField, implode(',', $companiesList));
            };

            /* Instant-increasing figures */
            if(in_array('participants', $columns)) {

                $query = sprintf("SELECT COUNT(DISTINCT cch.consumerId) as items, DATE(FROM_UNIXTIME(cch.enterDate)) as eventDate
                                  FROM {$wpdb->prefix}ls_at_consumers_challenges as cch
                                  %s
                                  WHERE cch.challengeId = %d AND cch.enterDate BETWEEN %d AND %d
                                  GROUP BY %s", $companyJoin('cch.consumerId'), $challengeId, $startDate, $endDate + DAY_IN_SECONDS - 1, $groupBy('cch.enterDate'));

                $instantData['participants'] = $arrayColumn($wpdb->get_results($query));

                $query = sprintf("SELECT COUNT(DISTINCT cch.consumerId) 
                                  FROM {$wpdb->prefix}ls_at_consumers_challenges as cch
                                  %s
                                  WHERE cch.challengeId = %d AND cch.enterDate < %d", $companyJoin('cch.consumerId'), $challengeId, $startDate);
                $instantData['participantsStartCount'] = (int) $wpdb->get_var($query);
            }

            /* Day-based figures */
            if(in_array('activities', $columns)) {
                $query = sprintf("SELECT COUNT(DISTINCT con.connectionId) as items, DATE(FROM_UNIXTIME(con.createDate)) as eventDate
                              FROM {$wpdb->prefix}ls_at_connections as con
                              %s
                              WHERE con.challengeId = %d AND con.createDate BETWEEN %d AND %d
                              GROUP BY %s", $companyJoin('con.consumerId'), $challengeId, $startDate, $endDate + DAY_IN_SECONDS - 1, $groupBy('con.createDate'));

                $dbData['activities'] = $arrayColumn($wpdb->get_results($query));
            }

            foreach($columns as $colNum => $column) {

                $data[$column] = [];

                for($day = $startDate; $day <= $endDate; $day += DAY_IN_SECONDS) {

                    // get only Mondays for weekly presentation
                    if($weekInterval && date('N', $day) != 1) {
                        continue;
                    }

                    // get first days of each year for month presentation
                    if($monthInterval && date('j', $day) != 1) {
                        continue;
                    }

                    $data[$column][date('Y-m-d', $day)] = 0;

                    if($colNum === 0) {
                        if($monthInterval) {
                            $labels[] = date('m.Y', $day);
                        } elseif($weekInterval) {
                            $labels[] = LS\Library::getShortDatesRange($day, min(time(), $day + WEEK_IN_SECONDS - 1));
                        } else {
                            $labels[] = date('d.m.Y', $day);
                        }
                    }
                }
            }

            // fill data with instant values
            foreach($columns as $column) {
                if(isset($instantData[$column])) {

                    $itemsCount = $instantData[$column . 'StartCount'];

                    for($day = $startDate; $day <= $endDate; $day += DAY_IN_SECONDS) {

                        $eventDate = date('Y-m-d', $day);

                        if(isset($instantData[$column][$eventDate])) {
                            $itemsCount += $instantData[$column][$eventDate];
                        }

                        if($monthInterval) {
                            $data[$column][date('Y-m-01', $day)] = $itemsCount;
                        } elseif($weekInterval) {
                            $data[$column][date('Y-m-d', LS\Library::getMonday($day))] = $itemsCount;
                        } else {
                            $data[$column][$eventDate] = $itemsCount;
                        }
                    }
                }
            }

            // fill data with daily values
            foreach($columns as $column) {
                if(isset($dbData[$column])) {
                    foreach($dbData[$column] as $eventDate => $itemsCount) {
                        if($monthInterval) {
                            $data[$column][date('Y-m-01', strtotime($eventDate))] = $itemsCount;
                        } elseif($weekInterval) {
                            $data[$column][date('Y-m-d', LS\Library::getMonday(strtotime($eventDate)))] = $itemsCount;
                        } else {
                            $data[$column][$eventDate] = $itemsCount;
                        }
                    }
                }
            }

            // skip keys
            foreach($data as $n => $col) {
                $data[$n] = array_values($col);
            }

            // custom view for a single chart line
            if(count($labels) === 1) {

                $labels[] = $labels[0];

                foreach($columns as $column) {
                    $data[$column][] = $data[$column][0];
                }
            }
        }

        return [
            'startDate' => date('Y-m-d', $sd),
            'endDate'   => date('Y-m-d', $ed),
            'columns'   => $columns,
            'labels'    => $labels,
            'data'      => $data
        ];
    }

    /**
     * Delete activity
     * @since 2.0
     * @param int $activityId Activity ID
     */
    public static function deleteActivity($activityId)
    {
        if($activityId > 0) {

            $wpdb = LS()->wpdb;
            $wpdb->delete($wpdb->prefix . 'ls_at_activities', ['activityId' => $activityId], '%d');
            $wpdb->delete($wpdb->prefix . 'ls_at_connections', ['activityId' => $activityId], '%d');

            self::cleanCaches();

            do_action('ls_at_delete_activity', $activityId);
        }
    }

    /**
     * Disconnect challenge from the consumer
     *
     * @since 2.14.0
     *
     * @param int $challengeId Challenge Id
     * @param int $consumerId Consumer Id
     */
    public static function disconnectedChallengeFromConsumer($challengeId, $consumerId)
    {
        if($consumerId > 0 && $challengeId > 0) {

            $wpdb = LS()->wpdb;
            $wpdb->delete($wpdb->prefix . 'ls_at_connections', ['consumerId' => $consumerId, 'challengeId' => $challengeId], '%d');
            $wpdb->delete($wpdb->prefix . 'ls_at_consumers_challenges', ['consumerId' => $consumerId, 'challengeId' => $challengeId], '%d');

            self::cleanCaches();
        }
    }

    /**
     * Delete any information about specified consumer related to AT from the DB tables
     * @since 2.0
     * @param int $consumerId Consumer ID
     */
    public static function deleteConsumer($consumerId)
    {
        if($consumerId > 0) {

            $wpdb = LS()->wpdb;
            $wpdb->delete($wpdb->prefix . 'ls_at_connections', ['consumerId' => $consumerId], '%d');
            $wpdb->delete($wpdb->prefix . 'ls_at_consumers_challenges', ['consumerId' => $consumerId], '%d');

            self::cleanCaches();
        }
    }

    /**
     * Retrieve a list of passed activities in challenge by consumer
     *
     * @since 2.0
     *
     * @param int $challengeId Challenge ID
     * @param int $consumerId Consumer ID
     * @return array A list of activities
     */
    public static function getConsumerActivitiesInChallenge($challengeId, $consumerId)
    {
        if($challengeId < 1 || $consumerId < 1) {
            return [];
        }

        if(!isset(self::$consumerActivities[$consumerId])) {
            self::$consumerActivities[$consumerId] = [];
        }

        if(!isset(self::$consumerActivities[$consumerId][$challengeId])) {

            $wpdb = LS()->wpdb;
            $activities = $wpdb->get_col(sprintf("SELECT activityId FROM {$wpdb->prefix}ls_at_connections WHERE challengeId = %d AND consumerId = %d", $challengeId, $consumerId));

            self::$consumerActivities[$consumerId][$challengeId] = [];

            foreach($activities as $activityId) {
                self::$consumerActivities[$consumerId][$challengeId][] = (int) $activityId;
            }
        }

        return self::$consumerActivities[$consumerId][$challengeId];
    }

    /**
     * Find and retrieve a list of activities by specified information (action, identify, challenge ID)
     *
     * @since 2.0
     *
     * @param string $actionName Activity name
     * @param string $ident Activity ident
     * @param null|int $challengeId Challenge ID or null to get though the all challenge
     * @return array An array of activities
     */
    public static function getMatchedActivities($actionName, $ident = '', $challengeId = null)
    {
        if(empty($actionName) || $challengeId === 0) {
            return [];
        }

        // if challenge ID isn't specified -> get all active challenges
        $challengesIds = $challengeId > 0 ? [$challengeId] : self::getActiveChallenges(true);
        if(empty($challengesIds)) {
            return [];
        }

        $wpdb = LS()->wpdb;
        $identWhere = $ident !== null ? sprintf("AND ident = '%s'", esc_sql($ident)) : '';
        $query = sprintf("SELECT * FROM {$wpdb->prefix}ls_at_activities WHERE challengeId IN (%s) AND `action` = '%s' %s", implode(',', $challengesIds), esc_sql($actionName), $identWhere);

        return $wpdb->get_results($query);
    }

    /**
     * Check if activity has been passed by consumer
     *
     * @since 2.0
     *
     * @param int $activityId Activity ID
     * @param int $consumerId Challenge ID
     * @return bool True if activity has been passed by consumer
     */
    public static function isActivityUsedByConsumer($activityId, $consumerId)
    {
        if($activityId < 1 || $consumerId < 1) {
            return false;
        }

        $wpdb = LS()->wpdb;
        $query = sprintf("SELECT EXISTS ( SELECT 1 FROM {$wpdb->prefix}ls_at_connections WHERE activityId = %d AND consumerId = %d )", $activityId, $consumerId);

        return $wpdb->get_var($query) > 0;
    }

    /**
     * Check if action has been performed by the specified consumer
     *
     * @since 2.8.6
     *
     * @param int $consumerId Consumer Id
     * @param array $activity Activity to check
     * @return bool True if action has been performed by the consumer
     */
    public static function isActionPerformedByConsumer($consumerId, $activity)
    {
        if($consumerId < 1) {
            return false;
        }

        $wpdb = LS()->wpdb;
        $query = sprintf("SELECT EXISTS ( 
                            SELECT 1 FROM {$wpdb->prefix}ls_at_connections as con
                            INNER JOIN {$wpdb->prefix}ls_at_activities as a ON a.activityId = con.activityId AND a.action = '%s' AND a.ident = '%s' AND a.activityId != %d
                            WHERE con.consumerId = %d
                          )", esc_sql($activity['action']), esc_sql($activity['ident']), $activity['activityId'], $consumerId);

        return $wpdb->get_var($query) > 0;
    }

    /**
     * Retrieve activity by ID
     *
     * @since 2.0
     *
     * @param int $activityId Activity ID
     * @return object|false Activity (object) or false on failure
     */
    public static function getActivity($activityId)
    {
        if($activityId < 1) {
            return false;
        }

        $wpdb = LS()->wpdb;

        return $wpdb->get_row(sprintf("SELECT * FROM {$wpdb->prefix}ls_at_activities WHERE activityId = %d LIMIT 1", $activityId));
    }

    /**
     * Make activity as passed by consumer (create connection between consumer and activity)
     *
     * @since 2.0
     *
     * @param object $activity Activity
     * @param int $consumerId Consumer ID
     * @return int Connection ID or false on failure
     */
    public static function connectActivityToConsumer($activity, $consumerId)
    {
        // @deprecated 05.2019 Legacy
        if (is_numeric($activity)) {
            _doing_it_wrong(__FUNCTION__, '1st param must be object', '2.16.4');
            $activity = self::getActivity($activity);
        }

        if(empty($activity) || $consumerId < 1) {
            return 0;
        }

        $consumerId = (int) $consumerId;
        $activityId = (int) $activity->activityId;
        $challengeId = (int) $activity->challengeId;

        if(self::isActivityUsedByConsumer($activityId, $consumerId)) {
            return 0;
        }

        $id = 0;
        $data = [
            'consumerId'  => $consumerId,
            'challengeId' => $challengeId,
            'activityId'  => $activityId,
            'createDate'  => time()
        ];

        $wpdb = LS()->wpdb;

        if($wpdb->insert($wpdb->prefix . 'ls_at_connections', $data)) {

            $id = (int) $wpdb->insert_id;

            self::cleanCaches();

            do_action('ls_at_consumer_made_activity', $consumerId, $activityId, $challengeId);

            \LS\ConsumersHelper::insertLog('at_activity', $consumerId, $activityId);
        }

        return $id;
    }

    /**
     * Delete connection between consumer and activity
     *
     * @since 2.0
     *
     * @param int $activityId Activity ID
     * @param int $consumerId Consumer ID
     * @return bool True if connection has been deleted
     */
    public static function disconnectActivityFromConsumer($activityId, $consumerId)
    {
        if($activityId < 1 || $consumerId < 1) {
            return false;
        }

        $wpdb = LS()->wpdb;
        $success = (bool) $wpdb->delete($wpdb->prefix . 'ls_at_connections', ['activityId' => $activityId, 'consumerId' => $consumerId], '%d');

        if($success) {
            self::cleanCaches();
        }

        return $success;
    }

    /**
     * Retrieve consumer activities in all available challenges
     *
     * @since 2.0
     *
     * @param int $consumerId Consumer ID
     * @return array|false Consumer challenges with activities, connections or false on failure
     */
    public static function getConsumerActivities($consumerId)
    {
        if($consumerId < 1) {
            return false;
        }

        $wpdb = LS()->wpdb;
        $query = sprintf("SELECT ch.challengeId, ch.title, con.activityId, con.createDate, a.action, a.ident, 'activity' as `type`
                          FROM {$wpdb->prefix}ls_at_challenges as ch
                          INNER JOIN {$wpdb->prefix}ls_at_consumers_challenges as cc ON cc.challengeId = ch.challengeId AND cc.consumerId = %d
                          INNER JOIN {$wpdb->prefix}ls_at_connections as con ON con.challengeId = ch.challengeId AND con.consumerId = cc.consumerId
                          LEFT JOIN {$wpdb->prefix}ls_at_activities as a ON a.challengeId = ch.challengeId AND a.activityId = con.activityId
                          GROUP BY con.connectionId
                          ORDER BY con.createDate DESC", $consumerId);
        $activities = $wpdb->get_results($query);

        $challenges = [];

        foreach($activities as $activity) {

            $challengeId = (int) $activity->challengeId;

            if(!isset($challenges[$challengeId])) {
                $challenges[$challengeId] = [];
            }

            $challenges[$challengeId][] = $activity;
        }

        return $challenges;
    }

    /**
     * Display success message after passing activity
     * @since 2.0
     * @param int $activityId Activity ID
     */
    public static function displayActivitySuccessMessage($activityId)
    {
        $activity = self::getActivity($activityId);
        if($activity) {
            $message = str_replace('{activity}', $activity->title, $activity->successMessage);
            if(!empty($message)) {
                LS\Library::setAlert($message);
            }
        }
    }

    /**
     * Retrieve total number of activities in challenge
     *
     * @since 2.0
     *
     * @param int $challengeId Challenge ID
     * @param bool $valid False to include invalid activities
     * @return int Number of activities
     */
    public static function getTotalActivitiesInChallenge($challengeId, $valid = true)
    {
        return count($valid ? self::getChallengeValidActivities($challengeId) : self::getChallengeActivities($challengeId));
    }

    /**
     * Retrieve valid activities from the challenge
     *
     * @since 2.14.4
     *
     * @param int $challengeId Challenge Id
     * @return array Activities
     */
    private static function getChallengeValidActivities($challengeId)
    {
        $activities = [];

        foreach(self::getChallengeActivities($challengeId) as $activity) {

            $trigger = ATActions::getAction($activity['action']);

            if(!$trigger) {
                continue;
            }

            if(empty($activity['link']) && !$trigger->isAllowedEmptyLink()) {
                continue;
            }

            if(!$trigger->isActivityValid($activity)) {
                continue;
            }

            $activities[] = $activity;
        }

        return $activities;
    }

    /**
     * Retrieve details about the activities related to specified consumer in specified challenge
     *
     * @since 2.8.0
     *
     * @param int $consumerId Consumer ID
     * @param int $challengeId Challenge ID
     * @return array Activities
     */
    public static function getConsumerActivitiesDetails($consumerId, $challengeId)
    {
        $activities = self::getChallengeValidActivities($challengeId);

        if(!empty($activities)) {

            $consumerActivities = self::getConsumerActivitiesInChallenge($challengeId, $consumerId);

            foreach($activities as &$activity) {
                $activity['passed'] = in_array($activity['activityId'], $consumerActivities);
            }

            unset($activity);
        }

        return $activities;
    }

    /**
     * Retrieve number of not-passed (open) activities in challenges by consumer
     *
     * @since 2.4
     *
     * @param int $challengeId Challenge ID
     * @param int $consumerId Consumer ID
     * @return int Number of open activities
     */
    public static function getOpenActivitiesCount($challengeId, $consumerId)
    {
        return count(array_filter(self::getConsumerActivitiesDetails($consumerId, $challengeId), static function($activity) {
            return !$activity['passed'];
        }));
    }

    /**
     * Retrieve total number of passed activities by consumer in challenge
     *
     * @since 2.0
     *
     * @param int $challengeId Challenge ID
     * @param int $consumerId Consumer ID
     * @return int Number of activities
     */
    public static function getPassedActivitiesCount($challengeId, $consumerId)
    {
        return count(array_filter(self::getConsumerActivitiesDetails($consumerId, $challengeId), static function($activity) {
            return $activity['passed'];
        }));
    }

    /**
     * Retrieve consumers to send email "Participation in the challenge"
     *
     * @since 2.7.0
     *
     * @param int $challengeId Challenge ID
     * @param int $daysToEvent Day to event
     * @return int[] A list of consumers ID's
     */
    public static function getPCPConsumers($challengeId, $daysToEvent)
    {
        if($challengeId < 1) {
            return [];
        }

        $wpdb = LS()->wpdb;
        $enterDate = strtotime('TODAY ' . $daysToEvent . ' DAYS');

        $query = sprintf("SELECT cc.consumerId FROM {$wpdb->prefix}ls_at_consumers_challenges as cc
                          INNER JOIN {$wpdb->prefix}ls_at_challenges as ch ON ch.challengeId = cc.challengeId AND '%s' BETWEEN ch.dateStart AND ch.dateEnd
                          INNER JOIN {$wpdb->prefix}ls_consumers as c ON c.consumerId = cc.consumerId AND c.active = 1 AND c.verified = 1 AND c.allowEmails = 1
                          WHERE cc.challengeId = %d AND cc.enterDate BETWEEN %d AND %d"
            , date('Y-m-d'), $challengeId, $enterDate, $enterDate + DAY_IN_SECONDS - 1);

        return $wpdb->get_col($query);
    }

    /**
     * Retrieve consumers to send email "Not Participated members"
     *
     * @since 2.7.0
     *
     * @param int $challengeId Challenge ID
     * @param int $daysToEvent Days to event
     * @return int[] A list of consumers ID's
     */
    public static function getNotPCPConsumers($challengeId, $daysToEvent)
    {
        if($challengeId < 1) {
            return [];
        }

        $wpdb = LS()->wpdb;
        $startDate = strtotime('TODAY ' . $daysToEvent . ' DAYS');
        $query = sprintf("SELECT EXISTS (
                            SELECT * FROM {$wpdb->prefix}ls_at_challenges
                            WHERE challengeId = %d AND dateStart = '%s'
                          )", $challengeId, date('Y-m-d', $startDate));
        $challengeAvailable = $wpdb->get_var($query);

        $consumers = [];

        if($challengeAvailable > 0) {
            $query = sprintf("SELECT c.consumerId FROM {$wpdb->prefix}ls_consumers as c
                              WHERE NOT EXISTS ( SELECT * FROM {$wpdb->prefix}ls_at_consumers_challenges as cc WHERE cc.consumerId = c.consumerId )
                                    AND c.active = 1 AND c.allowEmails = 1 AND c.verified = 1");
            $consumers = $wpdb->get_col($query);
        }

        return $consumers;
    }

    /**
     * Retrieve consumers to send email "After challenge ends"
     *
     * @since 2.7.0
     *
     * @param int $challengeId Challenge ID
     * @param int $daysToEvent Days to event
     * @return int[] A list of consumers ID's
     */
    public static function getACEConsumers($challengeId, $daysToEvent)
    {
        if($challengeId < 1) {
            return [];
        }

        $wpdb = LS()->wpdb;
        $endDate = strtotime('TODAY ' . $daysToEvent . ' DAYS');
        $query = sprintf("SELECT cc.consumerId FROM {$wpdb->prefix}ls_at_consumers_challenges as cc
                          INNER JOIN {$wpdb->prefix}ls_at_challenges as ch ON ch.challengeId = cc.challengeId AND ch.challengeId = %d AND ch.dateEnd = '%s'
                          INNER JOIN {$wpdb->prefix}ls_consumers as c ON c.consumerId = cc.consumerId AND c.active = 1 AND c.verified = 1 AND c.allowEmails = 1
                          GROUP BY cc.consumerId
                          ", $challengeId, date('Y-m-d', $endDate));

        return $wpdb->get_col($query);
    }

    /**
     * Retrieve challenge statistics by specified conditions and filters
     *
     * @since 2.1
     *
     * @param int $challengeId Challenge ID
     * @param int $itemsPerPage Number of rows to retrieve
     * @param int $page Page number for pagination. To get all rows without pagination set this value to null
     * @param string $orderBy Field to order
     * @param string $order Order direction
     * @param array $filterTerms Filters
     * @return array Array(
     *   'rows' - rows list,
     *   'totalCount' - total number of found rows,
     *   'totalPages' - total number of pages (for pagination),
     *   'itemsPerPage' - number of rows per page
     * )
     */
    public static function getChallengeStatistic($challengeId, $itemsPerPage = 20, $page = 1, $orderBy = '', $order = 'ASC', $filterTerms = [])
    {
        if($challengeId < 1) {
            return [];
        }

        $wpdb = LS()->wpdb;
        $whereConditions =
        $havingCondition = [];
        $joins = [];
        $order = empty($order) ? 'DESC' : $order;

        if($orderBy == 'consumer') {
            $orderBy = 'CONCAT(c.displayName, " ", c.lastName, c.firstName)';
        } elseif($orderBy == 'email') {
            $orderBy = 'c.email';
        } elseif($orderBy == 'passedActivities') {
            $orderBy = 'passedActivities';
        } elseif($orderBy == 'enterDate') {
            $orderBy = 'cc.enterDate';
        } elseif($orderBy == 'lastActivity') {
            $orderBy = 'lastActivity';
        } else {
            $orderBy = 'cc.challengeId';
        }

        $limit = '';
        if($page > 0 && $itemsPerPage > 0) {
            $limit = sprintf(" LIMIT %d, %d", ($page - 1) * $itemsPerPage, $itemsPerPage);
        }

        if(!empty($filterTerms)) {

            $filtersWhere = [];

            foreach($filterTerms as $filter) {

                if(!isset($filter['r'])) {
                    continue;
                }

                $record = '';
                $field = $filter['r'];
                $condition = $filter['c'] ?? '';
                $value = $filter['t'] ?? '';

                if($field === 'consumer') {
                    $record = 'CONCAT(c.displayName, " ", c.lastName, " ", c.firstName, " ", c.lastName)';
                } elseif($field === 'email') {
                    $record = 'c.email';
                } elseif($field === 'enterDate') {
                    $record = 'cc.enterDate';
                    $value = strtotime($value);
                } elseif(in_array($field, ['passedActivities', 'lastActivity'])) {
                    if($field === 'lastActivity') {
                        $value = strtotime($value);
                    }
                    $havingCondition[] = LS\Library::generateQueryFromFilter($field, $condition, $value);
                } elseif($field === 'companyId' && LS()->isModuleRegistered('companies')) {
                    $record = 'c.companyId';
                    $value = $filter['s_companyId'] ?? 0;
                    $value = !is_array($value) && $value > 0 ? LSCompaniesHelper::includeChildrenCompaniesIds((int) $value, true) : $value;
                } elseif($field === 'acmGroupId') { // @deprecated 02.2019 Legacy + acm de-integration
                    _doing_it_wrong(__FUNCTION__, 'Filter acmGroupId deprecated for AT2 stats.', '2.16.1');
                    return [];
                } elseif($field === 'acmCode') { // @deprecated 02.2019 Legacy + acm de-integration
                    _doing_it_wrong(__FUNCTION__, 'Filter acmCode deprecated for AT2 stats.', '2.16.1');
                    return [];
                }

                if(!empty($record)) {
                    $filtersWhere[] = LS\Library::generateQueryFromFilter($record, $condition, $value);
                }
            }

            if(!empty($filtersWhere)) {
                $xor = isset($filterTerms['x']) && in_array($filterTerms['x'], ['AND', 'OR']) ? $filterTerms['x'] : 'AND';
                $whereConditions[] = '(' . implode(') ' . $xor . ' (', $filtersWhere) . ')';
            }
        }

        $joins = empty($joins) ? '' : implode(PHP_EOL, array_values($joins));
        $whereConditions = empty($whereConditions) ? '' : sprintf(" AND (%s)", implode(') AND (', $whereConditions));
        $havingCondition = empty($havingCondition) ? '' : 'HAVING ' . implode(' AND ', $havingCondition);

        $query = sprintf("SELECT SQL_CALC_FOUND_ROWS cc.consumerId, c.email, cc.enterDate, MAX(con.createDate) as lastActivity, COUNT(con.connectionId) as passedActivities
                          FROM {$wpdb->prefix}ls_at_consumers_challenges as cc
                          INNER JOIN {$wpdb->prefix}ls_consumers as c ON c.consumerId = cc.consumerId AND c.active = 1
                          LEFT JOIN {$wpdb->prefix}ls_at_connections as con ON con.consumerId = cc.consumerId AND con.challengeId = cc.challengeId
                          LEFT JOIN {$wpdb->prefix}ls_at_activities as a ON a.challengeId = cc.challengeId AND a.activityId = con.activityId
                          %s
                          WHERE a.challengeId = %d %s
                          GROUP BY cc.consumerId
                          %s
                          ORDER BY %s %s %s", $joins, $challengeId, $whereConditions, $havingCondition, $orderBy, $order, $limit);

        $records = $wpdb->get_results($query);
        $totalCount = (int) $wpdb->get_var("SELECT FOUND_ROWS()");
        $itemsPerPage = $itemsPerPage > 0 ? $itemsPerPage : $totalCount;
        $totalPages = $totalCount > 0 && $itemsPerPage > 0 ? ceil($totalCount / $itemsPerPage) : 0;

        foreach($records as $key => $record) {
            $records[$key]->number = $key + 1 + ($page - 1) * $itemsPerPage;
        }

        return [
            'rows'         => $records,
            'totalCount'   => $totalCount,
            'totalPages'   => $totalPages,
            'itemsPerPage' => $itemsPerPage
        ];
    }

    /**
     * Duplicate challenges
     * @since 2.8.6
     * @param int[] $challengesIds Challenge IDs
     */
    public static function duplicateChallenges($challengesIds)
    {
        $challengesIds = array_map('intval', array_filter($challengesIds, 'is_numeric'));
        foreach($challengesIds as $challengeId) {

            $challenge = (array) self::getChallenge($challengeId);

            $challenge['title'] = 'Copy of ' . $challenge['title'];
            $challenge['challengeId'] =
            $challenge['createDate'] = 0;

            unset($challenge['active']);

            $activities = self::getChallengeActivities($challengeId);
            foreach($activities as $key => $activity) {
                unset($activities[$key]['activityId'], $activities[$key]['challengeId']);
            }

            if($newChallengeId = self::saveChallenge($challenge, $activities)) {
                do_action('ls_at_challenge_duplicated', $challengeId, $newChallengeId);
            }
        }
    }

    /**
     * Retrieve AT page meta-data
     *
     * @since 2.16.0
     *
     * @param int $postId AT page Id
     * @return array AT page meta-data
     */
    public static function getChallengePageMeta(int $postId): array
    {
        return [
            'teaserImage' => get_post_meta($postId, '_teaserImage', true),
            'teaserText'  => get_post_meta($postId, '_teaserText', true)
        ];
    }

    /**
     * Save AT page meta-data
     *
     * @since 2.16.0
     *
     * @param int $postId AT page Id
     * @param array $meta AT page meta-data
     */
    public static function saveChallengePageMeta(int $postId, array $meta)
    {
        update_post_meta($postId, '_teaserImage', $meta['teaserImage'] ?? '');
        update_post_meta($postId, '_teaserText', $meta['teaserText'] ?? '');
    }
}