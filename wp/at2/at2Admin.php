<?php
class at2AdminLoyaltySuite extends at2LoyaltySuite implements \LS\ModuleAdminInterface
{
    /**
     * @since 2.16.0
     * @return bool
     */
    public function hasAdministrationPage()
    {
        return true;
    }

    /**
     * @since 2.16.0
     * @internal
     */
    public function onInstall()
    {
        require_once __DIR__ . '/install.php';

        at2InstallLoyaltySuite();
    }

    /**
     * @since 2.16.0
     * @internal
     */
    public function onUninstall()
    {
        AT2Model::deleteTables();
    }

    /**
     * @since 2.16.0
     * @internal
     */
    public function afterActivate()
    {
        self::addCapsToRole(['ls_at'], 'contributor');
        self::addCapsToRole(['ls_at', 'ls_at_manage'], 'author');
        self::addCapsToRole(['ls_at', 'ls_at_manage'], 'editor');
        self::addCapsToRole(['ls_at', 'ls_at_manage', 'ls_at_settings'], 'administrator');
    }

    /**
     * @since 2.16.0
     * @internal
     */
    public function load()
    {
        parent::load();

        // init hooks for plugin
        add_action('admin_init', [$this, 'adminInit']);
        add_action('add_meta_boxes', [$this, 'addMetaBoxes']);
        add_action('save_post', [$this, 'savePost']);
        add_action('admin_enqueue_scripts', [$this, 'printAdminScripts']);
        add_action('wp_ajax_atLoadPostDetails', [$this, 'atLoadPostDetails']);
        add_action('delete_post', [$this, 'deletePost']); // might be ajax request
        add_action('wp_ajax_atDashboardFigures', [$this, 'atDashboardFigures']);
        add_action('wp_ajax_atActivities', [$this, 'atActivities']);
        add_action('ls_cron_daily', [$this, 'cronJobDaily']);

        // Consumers
        add_filter('consumerDetails', [$this, 'consumerDetailsFilter'], 10, 2);
        add_filter('ls_consumer_log__at_challenge', [$this, 'consumerLog']);
        add_filter('ls_consumer_log__at_activity', [$this, 'consumerLog']);
    }

    /**
     * @since 2.16.0
     * @internal
     */
    public function initShortcodes()
    {
        parent::initShortcodes();

        $defaultAtts = [
            'id' => __('Challenge ID (optional) to indicate challenge', 'ls')
        ];

        LS()->addShortcodeInfo('challenge', __('Challenge details', 'ls'), __('Display challenge complete info', 'ls'), self::getName(), $defaultAtts);
        LS()->addShortcodeInfo('challenge-title', __('Display challenge title', 'ls'), '', self::getName(), $defaultAtts);
        LS()->addShortcodeInfo('challenge-period', __('Display challenge period', 'ls'), '', self::getName(), $defaultAtts);
        LS()->addShortcodeInfo('start-challenge', __('Display button to start the challenge', 'ls'), '', self::getName(), $defaultAtts, '[start-challenge] Start Challenge [/start-challenge]');
        LS()->addShortcodeInfo('challenge-progress', __('Display consumer progress in the challenge', 'ls'), '', self::getName(), $defaultAtts);
        LS()->addShortcodeInfo('challenge-progress-bar', __('Display consumer progress bar in the challenge', 'ls'), '', self::getName(), $defaultAtts);
        LS()->addShortcodeInfo('challenge-days-left', __('Display days till the end of the challenge', 'ls'), '', self::getName(), $defaultAtts);
        LS()->addShortcodeInfo('challenge-participated-days', __('Display the number of consumer participation days in the challenge', 'ls'), '', self::getName(), $defaultAtts);
        LS()->addShortcodeInfo('challenge-total-activities-count', __('Display the number of total activities in the challenge', 'ls'), '', self::getName(), $defaultAtts);
        LS()->addShortcodeInfo('challenge-passed-activities-count', __('Display the number of passed activities in the challenge by consumer', 'ls'), '', self::getName(), $defaultAtts);
        LS()->addShortcodeInfo('challenge-submit', __('Submit the button', 'ls'), __('Display button to be submitted to perform activity', 'ls'), self::getName(), $defaultAtts);
        LS()->addShortcodeInfo('challenge-goal-message', __('Display consumer goal message in the challenge', 'ls'), '', self::getName(), $defaultAtts);
        LS()->addShortcodeInfo('challenge-message-1', __('Display challenge message 1', 'ls'), '', self::getName(), $defaultAtts);
        LS()->addShortcodeInfo('challenge-message-2', __('Display challenge message 2', 'ls'), '', self::getName(), $defaultAtts);
        LS()->addShortcodeInfo('tracking-code-form', __('Display Tracking Code form', 'ls'), __('Display form for submit Tracking Code', 'ls'), self::getName());
        LS()->addShortcodeInfo('tracking-code-trigger', __('Trigger activation', 'ls'), __('Activate trigger automatically when opening the content with this shortcode', 'ls'), self::getName(), ['code' => __('trigger code/identification name (required)', 'ls')], '[tracking-code-trigger code="{TRACING_CODE}" single="1" published="1"]');
        LS()->addShortcodeInfo('challenges-overview', __('Consumer challenges overview', 'ls'), '', self::getName());
    }

    /**
     * Load module administration scripts
     * @since 2.16.0
     * @internal
     */
    public function printAdminScripts()
    {
        wp_enqueue_style('thickbox');
        wp_enqueue_style('ls-at-admin', $this->getUrl2Module() . 'css/admin.css', [], $this->getVersion());

        if($this->isCurrentPluginPage()) {
            wp_enqueue_script('media-upload');
            wp_enqueue_script('thickbox');
        }

        wp_enqueue_script('jquery-ui-sortable');

        wp_enqueue_script('ls-at-admin', $this->getUrl2Module() . 'js/admin.js', ['jquery'], $this->getVersion(), true);
        wp_localize_script('ls', 'ATAdminVars', [
            'emptyActivityTitle'       => __('Please enter activity title', 'ls'),
            'emptyActivityIdent'       => __('Please enter activity identification value', 'ls'),
            'removeConfirmation'       => __('Are you sure?', 'ls'),
            'actionsWithMultipleUsage' => ATActions::getActionsNamesWithMultipleUsage(),
            'actionsIdents'            => ATActions::getActionsIdents()
        ]);
    }

    /**
     * Process backend actions and requests
     * @since 2.16.0
     * @internal
     */
    public function adminInit()
    {
        if(!$this->isCurrentPluginPage() || !$this->hasAction() || !current_user_can('ls_at')) {
            return;
        }

        if($this->isAction('at2-save-settings') && isset($_REQUEST['submit']) && current_user_can('ls_at_settings') && $this->updateSettingsPage()) {
            $this->setAlert(__('Settings have been saved', 'ls'));
            LS()->logUserAction('AT: settings updated');
            \LS\Library::redirect(remove_query_arg('action'));
        }

        if($this->isAction('save-challenge') && current_user_can('ls_at_manage') && isset($_POST['submit'])) {

            $challenge = [
                'challengeId'       => $this->getParam('challengeId', 'id'),
                'title'             => $this->getParam('title', 'text'),
                'challengePageId'   => $this->getParam('challengePageId', 'id'),
                'dateStart'         => $this->getParam('dateStart', 'date'),
                'dateEnd'           => $this->getParam('dateEnd', 'date'),
                'goalMessageText1'  => $this->getParam('goalMessageText1', 'html'),
                'goalValue1'        => $this->getParam('goalValue1', 'int'),
                'goalMessageText2'  => $this->getParam('goalMessageText2', 'html'),
                'goalValue2'        => $this->getParam('goalValue2', 'int'),
                'goalMessageText3'  => $this->getParam('goalMessageText3', 'html'),
                'goalValue3'        => $this->getParam('goalValue3', 'int'),
                'goalMessageText4'  => $this->getParam('goalMessageText4', 'html'),
                'goalValue4'        => $this->getParam('goalValue4', 'int'),
                'challengeMessage1' => $this->getParam('challengeMessage1', 'html'),
                'messageRemark1'    => $this->getParam('messageRemark1', 'html'),
                'challengeMessage2' => $this->getParam('challengeMessage2', 'html'),
                'messageRemark2'    => $this->getParam('messageRemark2', 'html')
            ];

            if(empty($challenge['title'])) {
                $this->setError(__('Please enter challenge title', 'ls'));
            }

            if($challenge['dateStart'] < 1 || $challenge['dateEnd'] < 1) {
                $this->setError(__('Please specify challenge start and end dates', 'ls'));
            } elseif($challenge['dateStart'] > $challenge['dateEnd']) {
                $this->setError(__('Challenge end date should be great than start date', 'ls'));
            }

            if($challenge['challengePageId'] < 0 || !(get_post($challenge['challengePageId']) instanceof WP_Post)) {
                $this->setError(__('Please select the challenge page', 'ls'));
            }

            $prevGoalValue = -1;
            for($i = 1; $i <= 4; $i++) {

                if($challenge['goalValue' . $i] <= $prevGoalValue) {
                    $this->setError(__('Each next Goal Value should be great than previous one', 'ls'));
                    break;
                }

                $prevGoalValue = $challenge['goalValue' . $i];
            }

            $activities = $this->getParam('activities', 'array');
            if(count($activities) > 0) {
                $activitiesTypes = ATActions::getActionsList();
                $registeredActivities = [];
                foreach($activities as $activity) {

                    $activity['ident'] = $activity['ident'] ?? '';
                    $uniqueIdent = implode('|', [$activity['action'], $activity['ident']]);

                    if(!isset($activity['title']) || empty($activity['title'])) {
                        $this->setError(sprintf(__('Activity #%d: please fill out activity title', 'ls'), $activity['order']));
                    } elseif(empty($activity['action']) || !isset($activitiesTypes[$activity['action']])) {
                        $this->setError(sprintf(__('Activity "%s": please select activity type/trigger', 'ls'), $activity['title']));
                    } elseif(in_array($uniqueIdent, $registeredActivities)) {
                        if(!in_array($activity['action'], ATActions::getActionsNamesWithMultipleUsage())) {
                            $this->setError(sprintf(__('You can not have more than one activity "%s" per challenge', 'ls'), $activitiesTypes[$activity['action']]));
                        } else {
                            $this->setError(__('Activity duplicate: can not have more that one activity with the same action and identification key or code', 'ls'));
                        }
                    } elseif(($action = ATActions::getAction($activity['action'])) !== false) {
                        if(($error = $action->validateActivityIdent($activity)) != '') {
                            $this->setError($error);
                        }
                    } else {
                        $registeredActivities[] = $uniqueIdent;
                    }
                }
            }

            if(count($activities) === 0) {
                $this->setError(__('Please select at least one activity in the challenge', 'ls'));
            }

            if(!$this->hasErrors()) {
                if($challengeId = AT2Model::saveChallenge($challenge, $activities)) {

                    // recheck all activities in the challenge
                    foreach(AT2Model::getConsumersInChallenge($challengeId) as $consumerId) {
                        $this->reviewConsumerActivities($consumerId, $challengeId);
                    }

                    $this->setAlert(__('Challenge has been successfully saved', 'ls'));
                    LS()->logUserAction(sprintf('AT: Challenge "%s" saved', $challenge['title']));
                    \LS\Library::redirect(add_query_arg('challengeId', $challengeId));
                }

                $this->setError(__('Can not save challenge. Please check data for errors or try again later.', 'ls'));
            }
        }

        if($this->isAction('delete-challenge') && isset($_REQUEST['challengeId']) && current_user_can('ls_at_manage')) {
            $challenges = $this->getParam('challengeId', 'int_array');
            if(!empty($challenges)) {

                if(AT2Model::deleteChallengesList($challenges)) {
                    $this->setAlert(__('Selected records have been deleted', 'ls'));
                    LS()->logUserAction(sprintf('AT: Challenge(s) deleted (ID: %s)', implode(',', $challenges)), 'warning');
                } else {
                    $this->setError(__('Can not delete selected records', 'ls'));
                }

                \LS\Library::redirect(remove_query_arg(['action', 'action2', 'challengeId']));
            }
        }

        if($this->isAction('duplicate-challenge') && isset($_REQUEST['challengeId']) && current_user_can('ls_at_manage')) {
            $challenges = $this->getParam('challengeId', 'int_array');
            if(!empty($challenges)) {

                AT2Model::duplicateChallenges($challenges);

                $this->setAlert(__('Challenge successfully duplicated', 'ls'));
                LS()->logUserAction(sprintf('AT: Challenge(s) duplicated (ID: %s)', implode(',', $challenges)));

                \LS\Library::redirect(remove_query_arg(['action', 'action2', 'challengeId']));
            }
        }

        if($this->isAction('export-challenge-statistic-csv') && isset($_REQUEST['challengeId']) && $_REQUEST['challengeId'] > 0) {

            $challengeId = $this->getParam('challengeId', 'id');
            $challenge = AT2Model::getChallenge($challengeId);
            if(!$challenge) {
                echo 'Incorrect challenge';
                exit;
            }

            include_once __DIR__ . '/tables/challenge-statistic.php';
            $challengeStatisticTable = new challengeStatisticTableAT2LoyaltySuite($challengeId);
            $options = $challengeStatisticTable->get_table_options();
            $limitConsumersPerRequest = 50;
            $statistic = AT2Model::getChallengeStatistic($challengeId, $limitConsumersPerRequest, 1, $options['orderBy'], $options['order'], $options['filterTerms']);

            if($statistic['totalCount'] > 0) {

                $columns = $challengeStatisticTable->get_columns();
                unset($columns['details']);
                $columns = array_slice($columns, 1);

                header("Content-type: text/csv");
                header("Cache-Control: no-store, no-cache");
                header('Content-Disposition: attachment; filename="' . sanitize_title($challenge->title) . '_statistic_' . date_i18n('d.m.Y') . '.csv"');
                $fp = fopen("php://output", 'w');
                fprintf($fp, chr(0xEF) . chr(0xBB) . chr(0xBF)); // UTF-8
                fputcsv($fp, array_values($columns));

                for($i = 1; $i <= $statistic['totalPages']; $i++) {

                    if($i > 1) {
                        $statistic = AT2Model::getChallengeStatistic($challengeId, $limitConsumersPerRequest, $i, $options['orderBy'], $options['order'], $options['filterTerms']);
                    }

                    foreach($statistic['rows'] as $fields) {
                        $a = [];
                        foreach($columns as $columnKey => $columnValue) {
                            if($columnKey == 'consumer') {
                                $a[$columnKey] = consumersLoyaltySuite::getConsumerNiceName((int) $fields->consumerId);
                            } elseif($columnKey != 'details') {
                                $a[$columnKey] = $challengeStatisticTable->column_default($fields, $columnKey);
                            }
                        }
                        fputcsv($fp, $a);
                    }
                }

                fclose($fp);
                exit;
            }
        }
    }

    /**
     * Save module settings
     * @since 2.16.0
     * @return bool Returns true if settings have been saved
     */
    private function updateSettingsPage()
    {
        $success = true;

        $options = [
            'acp'        => $this->getParam('acp', 'checkbox'),
            'atTypeSlug' => $this->getParam('atTypeSlug', 'text')
        ];

        flush_rewrite_rules();

        if(!$this->hasErrors()) {
            $this->setOptions(array_merge($this->getOptions(), $options));
        } else {
            $success = false;
        }

        return $success;
    }

    /**
     * Register meta-box "Activity Tracking"
     * @since 2.16.0
     * @internal
     */
    public function addMetaBoxes()
    {
        add_meta_box('at-box', __('Activity Tracking', 'ls'), [$this, 'metaBoxContent'], 'activity-tracking');
    }

    /**
     * Meta-box "Activity Tracking"
     *
     * @since 2.16.0
     * @internal
     *
     * @param WP_Post $post Current post
     */
    public function metaBoxContent(WP_Post $post)
    {
        (new LS\Template(__DIR__ . '/views/backend'))
            ->assign('nonce', wp_create_nonce('at2-meta-box'))
            ->assign('meta', AT2Model::getChallengePageMeta($post->ID))
            ->render('admin-meta-box.phtml');
    }

    /**
     * Saving post - save data for the meta-box "Content access"
     *
     * @since 3.4.0
     * @internal
     *
     * @param int $postId Post Id
     */
    public function savePost($postId)
    {
        if(defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
            return;
        }

        if(!isset($_POST['at_box_nonce']) || !wp_verify_nonce($_POST['at_box_nonce'], 'at2-meta-box')) {
            return;
        }

        $post = get_post($postId);
        $postType = get_post_type_object($post->post_type);
        if(in_array($post->post_status, ['inherit', 'trash']) || !$postType || !current_user_can($postType->cap->edit_post, $postId)) {
            return;
        }

        AT2Model::saveChallengePageMeta($postId, [
            'teaserImage' => \LS\Library::getParam('teaserImage', 'ls_image'),
            'teaserText'  => \LS\Library::getParam('teaserText', 'html')
        ]);
    }

    /**
     * Render administration menu
     * @since 2.16.0
     * @internal
     */
    public function moduleAdministrationPage()
    {
        include_once __DIR__ . '/tables/challenges.php';
        include_once __DIR__ . '/tables/statistics.php';

        $template = new LS\Template(__DIR__ . '/views/backend');
        $template
            ->assign('options', $this->getOptions())
            ->assign('activityTypes', ATActions::getActionsList());

        if(!isset($_REQUEST['view'])) {

            $challengesList = AT2Model::getChallengesList();

            // select default challenge for the dashboard
            reset($challengesList);
            $dashboardChallengeId = key($challengesList);

            $template
                ->assign('challenges', new challengesTableAT2LoyaltySuite())
                ->assign('statistics', new statisticsTableAT2LoyaltySuite())
                ->assign('challengesList', $challengesList)
                ->assign('companiesList', LSCompaniesHelper::getCompaniesList(false, true))
                ->assign('dashboardChallengeId', $dashboardChallengeId)
                ->assign('dashboard', AT2Model::getDashboard($dashboardChallengeId))
                ->assign('tabs', [
                    ['name' => 'ls-challenges', 'title' => __('Challenges', 'ls'), 'view' => 'admin-challenges.phtml'],
                    ['name' => 'ls-statistics', 'title' => __('Statistics', 'ls'), 'view' => 'admin-statistics.phtml'],
                    ['name' => 'ls-settings', 'title' => __('Settings', 'ls'), 'view' => 'admin-settings.phtml', 'capability' => 'ls_at_settings']
                ]);

            if($this->isAction('at2-save-settings')) {
                $template->assign('options', stripslashes_deep($_POST));
            }
        }

        if(isset($_REQUEST['view']) && $_REQUEST['view'] === 'challenge-details') {

            /** @var consumersLoyaltySuite $consumersModule */
            $consumersModule = LS()->getModule('consumers');

            $template
                ->assign('page', ['name' => 'ls-challenge', 'title' => __('Challenge details', 'ls'), 'view' => 'admin-challenge.phtml'])
                ->assign('consumersFields', $consumersModule->getConsumerAccountFields())
                ->assign('challengesList', AT2Model::getChallengesList())
                ->assign('actions', ATActions::getActions());

            if(isset($_REQUEST['challengeId']) && $_REQUEST['challengeId'] > 0) {

                $challengeId = $this->getParam('challengeId', 'id');
                $challenge = AT2Model::getChallenge($challengeId);

                include_once __DIR__ . '/tables/challenge-statistic.php';

                $template
                    ->assign('challenge', $challenge)
                    ->assign('activities', AT2Model::getChallengeActivities($challengeId))
                    ->assign('challengeStatisticTable', new challengeStatisticTableAT2LoyaltySuite($challengeId))
                    ->assign('totalActivities', AT2Model::getTotalActivitiesInChallenge($challengeId, false))
                    ->assign('totalValidActivities', AT2Model::getTotalActivitiesInChallenge($challengeId))
                    ->assign('totalParticipants', AT2Model::getTotalConsumersInChallenge($challengeId));

            } else {
                $template->assign('challenge', AT2Model::getDefaultChallengeData());
            }

            if(isset($_POST['submit'])) {
                $template
                    ->assign('challenge', (object) stripslashes_deep($_POST))
                    ->assign('activities', $this->getParam('activities', 'array', 'POST'));
            }
        }

        $template->renderAdminPage();
    }

    /**
     * Load dynamically post details for specified post
     *
     * @since 2.16.0
     * @internal
     * @ajax
     */
    public function atLoadPostDetails()
    {
        if(current_user_can('ls_at')) {
            $postId = $this->getParam('postId', 'id', 'POST');
            if($postId > 0) {

                $post = get_post($postId);
                $excerpt = get_the_excerpt($post);
                $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
                wp_send_json([
                    'title' => empty($excerpt) ? $post->post_title : $excerpt,
                    'image' => empty($image) ? '' : $image[0],
                    'link'  => get_permalink($post->ID)
                ]);
            }
        }
    }

    /**
     * Delete post hook - delete all related activities to specified post
     *
     * @since 2.16.0
     * @internal
     *
     * @param int $postId Post ID
     */
    public function deletePost($postId)
    {
        foreach(AT2Model::getMatchedActivities('readPost', $postId) as $activity) {
            AT2Model::deleteActivity($activity->activityId);
        }
    }

    /**
     * Display information about Activity Tracking on the consumer details page
     *
     * @since 2.16.0
     * @internal
     *
     * @param array $tabs A list of consumer tabs
     * @param int $consumerId Consumer ID
     * @return array Modified list of consumer tabs
     */
    public function consumerDetailsFilter($tabs, $consumerId)
    {
        // use only challenges in which consumer is participated
        $challenges = AT2Model::getChallengesList();
        $consumerChallenges = AT2Model::getConsumerParticipatedChallengesIds($consumerId);
        foreach($challenges as $challengeId => $challengeName) {
            if(!in_array($challengeId, $consumerChallenges)) {
                unset($challenges[$challengeId]);
            }
        }

        $content = (new LS\Template(__DIR__ . '/views/backend'))
            ->assign('activities', AT2Model::getConsumerActivities($consumerId))
            ->assign('actions', ATActions::getActions())
            ->assign('challengesList', $challenges)
            ->assign('consumerParticipatedChallengesIds', $consumerChallenges)
            ->assign('activityTypes', ATActions::getActionsList())
            ->assign('consumerId', $consumerId)
            ->render('admin-consumer-activity.phtml', false);

        $tabs['at'] = [__('Activity Tracking', 'ls'), $content];

        return $tabs;
    }

    /**
     * Retrieve URL to edit challenge
     *
     * @since 2.16.0
     *
     * @param int $challengeId Challenge Id
     * @param string $referer Referer
     * @return string URL
     */
    public static function challengeEditLink($challengeId, $referer = '')
    {
        $referer = empty($referer) ? $_SERVER['REQUEST_URI'] : $referer;
        $referer = urlencode($referer);

        return $challengeId > 0 ? admin_url('admin.php?page=at2LoyaltySuite&view=challenge-details&challengeId=' . $challengeId . '&referer=' . $referer) : '';
    }

    /**
     * Format message for consumer log
     *
     * @since 2.16.0
     * @internal
     *
     * @param object $log Log details
     * @return string Event text
     */
    public function consumerLog($log)
    {
        $event = '';

        if($log->messageType === 'at_challenge') {
            $event = __('Participate in challenge', 'ls');
            if(isset($log->note) && $log->note > 0 && $challenge = AT2Model::getChallenge((int) $log->note)) {
                $event .= ' <a href="' . self::challengeEditLink($challenge->challengeId) . '">' . $challenge->title . '</a>';
            }
        } elseif($log->messageType === 'at_activity') {
            $event = __('Challenge activity', 'ls');
            if(isset($log->note) && $log->note > 0 && $activity = AT2Model::getActivity((int) $log->note)) {
                $event .= ' ( <a href="' . self::challengeEditLink($activity->challengeId) . '">' . $activity->title . '</a>)';
            }
        }

        return $event;
    }

    /**
     * Send out challenge dashboard figures
     *
     * @ajax
     * @since 2.16.0
     * @internal
     */
    public function atDashboardFigures()
    {
        if(current_user_can('ls_at')) {
            $stats = AT2Model::getDashboard($this->getParam('challengeId', 'id'));
            $stats['avgParticipants'] = ceil($stats['avgParticipants']) . '%';
            wp_send_json($stats);
        }
    }

    /**
     * Send out challenge activities figures
     *
     * @ajax
     * @since 2.16.0
     * @internal
     */
    public function atActivities()
    {
        if(current_user_can('ls_at')) {

            $challengeId = $this->getParam('challengeId', 'id');
            $startDate = $this->getParam('startDate', 'date');
            $endDate = $this->getParam('endDate', 'date');
            $columns = $this->getParam('columns', 'array');
            $companyId = $this->getParam('companyId', 'id');

            wp_send_json(AT2Model::getStats($challengeId, $startDate, $endDate, $columns, $companyId));
        }
    }

    /**
     * Process CRON Job
     * @since 2.16.0
     * @internal
     */
    public function cronJobDaily()
    {
        if(self::lock('at2')) {

            // unpublish pages depends on challenge end date
            if($this->getOption('acp')) {

                foreach(AT2Model::getEndedChallenges() as $challenge) {
                    $post = $challenge['challengePageId'] > 0 ? get_post($challenge['challengePageId']) : false;
                    if($post && $post->post_status == 'publish') {
                        wp_update_post([
                            'ID'          => $challenge['challengePageId'],
                            'post_status' => 'draft'
                        ]);
                    }
                }
            }

            // publish pages depends on challenge start date
            if($this->getOption('acp')) {

                foreach(AT2Model::getStartedChallenges() as $challenge) {
                    $post = $challenge['challengePageId'] > 0 ? get_post($challenge['challengePageId']) : false;
                    if($post && $post->post_status == 'draft') {
                        wp_update_post([
                            'ID'          => $challenge['challengePageId'],
                            'post_status' => 'publish'
                        ]);
                    }
                }
            }

            self::unlock('at2');
        }
    }
}