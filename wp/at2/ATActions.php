<?php
/**
 * Class ATActions
 */
class ATActions
{
    /** @var ATAction[] */
    private static $actions = [];

    /**
     * @param string $id
     * @param ATAction $action
     */
    public static function addAction($id, ATAction $action)
    {
        self::$actions[$id] = $action;
    }

    /**
     * @return ATAction[]
     */
    public static function getActions()
    {
        return self::$actions;
    }

    /**
     * @return array
     */
    public static function getActionsList()
    {
        $actions = [];

        foreach(self::getActions() as $id => $action) {
            $actions[$id] = $action->label;
        }

        return $actions;
    }

    /**
     * @param string $id
     * @return ATAction|false
     */
    public static function getAction($id)
    {
        return self::$actions[$id] ?? false;
    }

    /**
     * @return array
     */
    public static function getActionsNamesWithMultipleUsage()
    {
        $list = [];

        foreach(self::$actions as $id => $action) {
            if($action->isAllowMultipleUsage()) {
                $list[] = $id;
            }
        }

        return $list;
    }

    /**
     * @return array
     */
    public static function getActionsIdents()
    {
        $result = [];

        foreach(self::$actions as $id => $action) {
            $ident = $action->getIdentId();
            if(!empty($ident)) {
                $result[$id] = $ident;
            }
        }

        return $result;
    }
}