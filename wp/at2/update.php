<?php
class at2LoyaltySuiteUpdate implements \LS\UpdateInterface
{
    /**
     * @param \LS\Module $module
     * @param string $oldVersion
     * @param string $newVersion
     */
    public function update(\LS\Module $module, string $oldVersion, string $newVersion)
    {
        $wpdb = LS()->wpdb;

        // 2.1
        if(version_compare($oldVersion, '2.1', '<')) {
            $wpdb->query(sprintf("ALTER TABLE `{$wpdb->prefix}ls_at_challenges`
                ADD COLUMN `ACE_active` TINYINT(1) UNSIGNED NULL DEFAULT '0' AFTER `challengeMessage2`,
                ADD COLUMN `ACE_subject` VARCHAR(200) NULL DEFAULT NULL AFTER `ACE_active`,
                ADD COLUMN `ACE_message` TEXT NULL DEFAULT NULL AFTER `ACE_subject`"));

            $wpdb->query(sprintf("ALTER TABLE `{$wpdb->prefix}ls_at_consumers_challenges`
                ADD COLUMN `ACE_email_received` TINYINT(1) UNSIGNED NULL DEFAULT '0' AFTER `ownGoalValue`"));
        }

        // 2.2
        if(version_compare($oldVersion, '2.2', '<')) {
            $wpdb->query(sprintf("ALTER TABLE `{$wpdb->prefix}ls_at_challenges`
                ADD COLUMN `CP_active` TINYINT(1) UNSIGNED NULL DEFAULT '0' AFTER `challengeMessage2`,
                ADD COLUMN `CP_days` TINYINT(3) UNSIGNED NULL DEFAULT '1' AFTER `CP_active`,
                ADD COLUMN `CP_subject` VARCHAR(200) NULL DEFAULT NULL AFTER `CP_days`,
                ADD COLUMN `CP_message` TEXT NULL AFTER `CP_subject`"));

            $wpdb->query(sprintf("ALTER TABLE `{$wpdb->prefix}ls_at_consumers_challenges`
                ADD COLUMN `CP_email_received` TINYINT(1) UNSIGNED NULL DEFAULT '0' AFTER `ownGoalValue`"));
        }

        // 2.2.3
        if(version_compare($oldVersion, '2.2.3', '<')) {

            $wpdb->query(sprintf("ALTER TABLE `{$wpdb->prefix}ls_at_manual_points`
                ADD INDEX `challengeId` (`challengeId`),
                ADD INDEX `consumerId` (`consumerId`)"));

            $wpdb->query(sprintf("ALTER TABLE `{$wpdb->prefix}ls_at_connections`
                    ADD UNIQUE INDEX `unique_connection` (`consumerId`, `challengeId`, `activityId`)"));
        }

        // 2.3
        if(version_compare($oldVersion, '2.3', '<')) {
            $wpdb->query(sprintf("ALTER TABLE `{$wpdb->prefix}ls_at_challenges` ADD COLUMN `slug` VARCHAR(255) null DEFAULT null AFTER `challengeId`;"));
        }

        // 2.5
        if(version_compare($oldVersion, '2.5', '<')) {

            $wpdb->query(sprintf("ALTER TABLE `{$wpdb->prefix}ls_at_challenges`
                ADD COLUMN `NP_active` TINYINT(1) UNSIGNED NULL DEFAULT '0' AFTER `CP_message`,
                ADD COLUMN `NP_days` TINYINT(3) UNSIGNED NULL DEFAULT '1' AFTER `NP_active`,
                ADD COLUMN `NP_subject` VARCHAR(200) NULL DEFAULT NULL AFTER `NP_days`,
                ADD COLUMN `NP_message` TEXT NULL AFTER `NP_subject`"));

            $options = $module->getOptions();
            if(!empty($options)) {
                $module->deleteOption('postTypes');
            }
        }

        // 2.5.2
        if(version_compare($oldVersion, '2.5.2', '<')) {
            $wpdb->query(sprintf("ALTER TABLE `{$wpdb->prefix}ls_at_challenges`
                ADD COLUMN `messageRemark1` VARCHAR(512) null AFTER `challengeMessage1`,
                ADD COLUMN `messageRemark2` VARCHAR(512) null AFTER `challengeMessage2`"));
        }

        // 2.6.0
        if(version_compare($oldVersion, '2.6.0', '<')) {

            $wpdb->query(sprintf("ALTER TABLE `{$wpdb->prefix}ls_at_challenges` ADD COLUMN `dateStartNew` DATE NOT NULL AFTER `dateStart`"));
            $wpdb->query(sprintf("ALTER TABLE `{$wpdb->prefix}ls_at_challenges` ADD COLUMN `dateEndNew` DATE NOT NULL AFTER `dateEnd`"));
            $wpdb->query(sprintf("UPDATE {$wpdb->prefix}ls_at_challenges SET dateStartNew = DATE(from_unixtime(dateStart))"));
            $wpdb->query(sprintf("UPDATE {$wpdb->prefix}ls_at_challenges SET dateEndNew = DATE(from_unixtime(dateEnd))"));
            $wpdb->query(sprintf("ALTER TABLE `{$wpdb->prefix}ls_at_challenges` DROP COLUMN `dateStart`"));
            $wpdb->query(sprintf("ALTER TABLE `{$wpdb->prefix}ls_at_challenges` DROP COLUMN `dateEnd`"));
            $wpdb->query(sprintf("ALTER TABLE `{$wpdb->prefix}ls_at_challenges` CHANGE COLUMN `dateStartNew` `dateStart` DATE NOT NULL AFTER `picture`"));
            $wpdb->query(sprintf("ALTER TABLE `{$wpdb->prefix}ls_at_challenges` CHANGE COLUMN `dateEndNew` `dateEnd` DATE NOT NULL AFTER `dateStart`"));

            $pictures = $wpdb->get_results(sprintf("SELECT challengeId, picture FROM {$wpdb->prefix}ls_at_challenges"));
            foreach($pictures as $pic) {
                $wpdb->update($wpdb->prefix . 'ls_at_challenges', ['picture' => maybe_serialize(['src' => $pic->picture, 'id' => 0])], ['challengeId' => $pic->challengeId]);
            }
        }

        // 2.7.0
        if(version_compare($oldVersion, '2.7.0', '<')) {
            $cc = $wpdb->get_results(sprintf("SELECT consumerId, challengeId, enterDate FROM {$wpdb->prefix}ls_at_consumers_challenges"));
            foreach($cc as $item) {
                \LS\ConsumersHelper::insertLog('at_challenge', $item->consumerId, $item->challengeId, $item->enterDate);
            }
        }

        // 2.8.0
        if(version_compare($oldVersion, '2.8.0', '<')) {

            $wpdb->query(sprintf("ALTER TABLE `{$wpdb->prefix}ls_at_challenges`
                DROP COLUMN `CP_active`,
                DROP COLUMN `CP_days`,
                DROP COLUMN `CP_subject`,
                DROP COLUMN `CP_message`,
                DROP COLUMN `NP_active`,
                DROP COLUMN `NP_days`,
                DROP COLUMN `NP_subject`,
                DROP COLUMN `NP_message`,
                DROP COLUMN `ACE_active`,
                DROP COLUMN `ACE_subject`,
                DROP COLUMN `ACE_message`
                LOCK"));

            $wpdb->query(sprintf("ALTER TABLE `{$wpdb->prefix}ls_at_consumers_challenges`
                DROP COLUMN `CP_email_received`,
                DROP COLUMN `ACE_email_received`"));

            $wpdb->query(sprintf("ALTER TABLE `{$wpdb->prefix}ls_at_challenges` ADD COLUMN `linkToChallenge` VARCHAR(1024) NOT NULL AFTER `description`"));
        }

        // 2.9.0
        if(version_compare($oldVersion, '2.9.0', '<')) {

            $wpdb->query(sprintf("ALTER TABLE `{$wpdb->prefix}ls_at_challenges`
            CHANGE COLUMN `title` `title` VARCHAR(500) NOT NULL AFTER `slug`,
            CHANGE COLUMN `linkToChallenge` `linkToChallenge` VARCHAR(2000) NOT NULL AFTER `description`,
            CHANGE COLUMN `picture` `picture` VARCHAR(2000) NOT NULL AFTER `linkToChallenge`"));

            $query = sprintf("SELECT challengeId, picture FROM {$wpdb->prefix}ls_at_challenges WHERE picture != ''");
            foreach($wpdb->get_results($query) as $result) {
                $picture = empty($result->picture) ? '' : maybe_unserialize($result->picture);
                $picture = is_array($picture) && isset($picture['src']) ? $picture['src'] : '';
                $wpdb->update($wpdb->prefix . 'ls_at_challenges', ['picture' => $picture], ['challengeId' => $result->challengeId]);
            }
        }

        // 2.10.0
        if(version_compare($oldVersion, '2.10.0', '<')) {
            $wpdb->query(sprintf("ALTER TABLE `{$wpdb->prefix}ls_at_challenges` DROP COLUMN `slug`"));
        }

        // 2.11.0
        if(version_compare($oldVersion, '2.11.0', '<')) {

            add_action('admin_init', static function() {

                $wpdb = LS()->wpdb;

                require_once __DIR__ . '/model.php';

                $pages = [];

                foreach(AT2Model::getChallenges() as $challenge) {

                    if(!empty($challenge->linkToChallenge)) {

                        $pageId = url_to_postid($challenge->linkToChallenge);

                        if($pageId > 0 && !in_array($pageId, $pages)) {
                            $wpdb->update($wpdb->posts, ['post_type' => 'activity-tracking'], ['ID' => $pageId]);
                            $pages[$challenge->challengeId] = $pageId;
                        }

                    } else {
                        wp_insert_post([
                            'post_title'   => (string) $challenge->title,
                            'post_excerpt' => (string) $challenge->description,
                            'post_name'    => sanitize_title($challenge->title),
                            'post_status'  => $challenge->active ? 'publish' : 'draft',
                            'post_type'    => 'activity-tracking'
                        ]);
                    }
                }

                foreach($pages as $challengeId => $pageId) {
                    $wpdb->update($wpdb->prefix . 'ls_at_challenges', ['challengePageId' => $pageId], ['challengeId' => $challengeId]);
                }

            });

            $wpdb->query(sprintf("ALTER TABLE `{$wpdb->prefix}ls_at_challenges` CHANGE COLUMN `linkToChallenge` `challengePageId` INT UNSIGNED NOT NULL AFTER `description`"));
        }

        // 2.12.0
        if(version_compare($oldVersion, '2.12.0', '<')) {

            $module->setOptions($module->getDefaultOptions());

            get_role('contributor')->add_cap('ls_at');
            get_role('author')->add_cap('ls_at');
            get_role('editor')->add_cap('ls_at');
            get_role('administrator')->add_cap('ls_at');
            get_role('author')->add_cap('ls_at_manage');
            get_role('editor')->add_cap('ls_at_manage');
            get_role('administrator')->add_cap('ls_at_manage');
            get_role('administrator')->add_cap('ls_at_settings');
        }

        // 2.12.4
        if(version_compare($oldVersion, '2.12.4', '<')) {
            $module->setOption('acp', true);
        }

        // 2.13.0
        if(version_compare($oldVersion, '2.13.0', '<')) {

            $wpdb->query(sprintf("ALTER TABLE `{$wpdb->prefix}ls_at_activities` DROP COLUMN `points`"));
            $wpdb->query(sprintf("ALTER TABLE `{$wpdb->prefix}ls_at_challenges` DROP COLUMN `goalMessageType`"));
            $wpdb->query(sprintf("DROP TABLE `{$wpdb->prefix}ls_at_manual_points`"));

            foreach($wpdb->get_col(sprintf("SELECT activityId FROM {$wpdb->prefix}ls_at_activities WHERE action IN ('orderIncentive', 'recommendation', 'loginsCount', 'participateInChallenge')")) as $activityId) {
                $wpdb->delete($wpdb->prefix . 'ls_at_connections', ['activityId' => (int) $activityId]);
                $wpdb->delete($wpdb->prefix . 'ls_at_activities', ['activityId' => (int) $activityId]);
            }
        }

        // 2.13.2
        if(version_compare($oldVersion, '2.13.2', '<')) {
            $activities = $wpdb->get_results(sprintf("SELECT consumerId, activityId, createDate FROM {$wpdb->prefix}ls_at_connections"));
            foreach($activities as $activity) {
                \LS\ConsumersHelper::insertLog('at_activity', $activity->consumerId, $activity->activityId, $activity->createDate);
            }
        }

        // 2.14.0
        if(version_compare($oldVersion, '2.14.0', '<')) {
            $wpdb->query(sprintf("ALTER TABLE `{$wpdb->prefix}ls_at_challenges` DROP COLUMN `picture`, DROP COLUMN `description`"));
            $wpdb->query(sprintf("ALTER TABLE `{$wpdb->prefix}ls_at_consumers_challenges` DROP COLUMN `ownGoal`, DROP COLUMN `ownGoalValue`"));
            $wpdb->query(sprintf("DELETE FROM `{$wpdb->prefix}ls_at_activities` WHERE `action` = 'personalGoal'"));
            $wpdb->query(sprintf("ALTER TABLE `{$wpdb->prefix}ls_at_challenges` CHANGE COLUMN `dateStart` `dateStart` DATE NOT NULL AFTER `challengePageId`, CHANGE COLUMN `goalMessageText2` `goalMessageText2` TEXT NOT NULL AFTER `goalValue1`"));
        }
    }
}