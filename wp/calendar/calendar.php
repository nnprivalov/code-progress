<?php
/**
 * Class calendarLoyaltySuite
 * Fitco Calendar
 */
class calendarLoyaltySuite extends LS\Module
{
    /**
     * @return string
     */
    public function getVersion()
    {
        return '3.4.1';
    }

    /**
     * @since 1.3.2
     * @return string
     */
    public function getTitle()
    {
        return 'Kalender';
    }

    /**
     * @since 1.3.2
     * @return string
     */
    public function getDescription()
    {
        return __('Kalenderansicht für den laufenden Monat', 'ls');
    }

    /**
     * Prepare the calendar - init main figures
     * @since 1.3.0
     * @return array
     */
    public function initCalendar()
    {
        return [
            'firstRangeDay' => strtotime('TODAY') - DAY_IN_SECONDS * 10,
            'lastRangeDay'  => strtotime('TODAY') + DAY_IN_SECONDS * 10
        ];
    }

    /**
     * Collect consumer events for Calendar
     *
     * @since 1.0
     *
     * @param array $calendar Calendar
     * @return array Consumer events as array or false on failure
     */
    public function collectConsumerEvents($calendar)
    {
        if (empty($calendar)) {
            return [];
        }

        $consumer = consumersLoyaltySuite::getAuthorizedConsumer();
        if (!$consumer || $consumer->getCompanyId() === 0) {
            return [];
        }

        $events =
        $tdwPostIds = [];
        $eventsSettings = $this->getOption('events', []);
        $wpdb = LS()->wpdb;

        // get Tipp der Woche
        if (
            (isset($eventsSettings['tdw']) && $eventsSettings['tdw']['enabled'])
            && LS()->isModuleActive('tippDerWoche')
        ) {

            /** @var tippDerWocheLoyaltySuite $tdw */
            $tdw = LS()->getModule('tippDerWoche');

            /** @var WP_Post $post */
            foreach ($tdw->getTDWPosts($calendar['firstRangeDay'], $calendar['lastRangeDay']) as $post) {

                $text = $eventsSettings['tdw']['text'];
                $text = apply_filters('ls_fitco_calendar_tdw_title', $text, $post);
                $text = str_replace('[post-name]', $post->post_title, $text);

                if(empty($text)) {
                    continue;
                }

                $day = $tdw->getTDWDate($post->ID);
                $tdwPostIds[] = $post->ID;
                $events[$day][] = [
                    'type'      => 'tdw',
                    'dateStart' => $day,
                    'allDay'    => false,
                    'link'      => $tdw->getLink($post),
                    'id'        => $post->ID,
                    'uid'       => md5('tdw:' . $post->ID),
                    'location'  => get_bloginfo('name'),
                    'title'     => $post->post_title,
                    'color'     => $eventsSettings['tdw']['color'],
                    'text'      => $text
                ];
            }
        }

        // get posts, portfolios, companies-channels, recipes
        if ((isset($eventsSettings['post']) && $eventsSettings['post']['enabled'])
            || (isset($eventsSettings['portfolio']) && $eventsSettings['portfolio']['enabled'])
            || (isset($eventsSettings['recipe']) && $eventsSettings['recipe']['enabled'])
            || (isset($eventsSettings['company-channel']) && $eventsSettings['company-channel']['enabled'])
        ) {

            $types = [];

            if (isset($eventsSettings['post']) && $eventsSettings['post']['enabled']) {
                $types[] = 'post';
            }

            if (isset($eventsSettings['portfolio']) && $eventsSettings['portfolio']['enabled']) {
                $types[] = 'portfolio';
            }

            if (isset($eventsSettings['recipe']) && $eventsSettings['recipe']['enabled']) {
                $types[] = 'recipe';
            }

            if (isset($eventsSettings['company-channel']) && $eventsSettings['company-channel']['enabled']) {
                $types[] = 'company-channel';
            }

            $posts = get_posts([
                'posts_per_page' => -1,
                'no_found_rows'  => true,
                'post_status'    => ['publish', 'future'],
                'post_type'      => $types,
                'date_query'     => [
                    'after'  => date('Y-m-d 00:00:00', $calendar['firstRangeDay']),
                    'before' => date('Y-m-d 23:59:59', $calendar['lastRangeDay'])
                ]
            ]);

            /** @var WP_Post $post */
            foreach ($posts as $post) {

                if (in_array($post->ID, $tdwPostIds)) {
                    continue;
                }

                $text = $eventsSettings[$post->post_type]['text'];
                $text = apply_filters('ls_fitco_calendar_post_title', $text, $post);
                $text = str_replace('[post-name]', $post->post_title, $text);

                if(empty($text)) {
                    continue;
                }

                $postDate = strtotime($post->post_date);
                $day = strtotime('TODAY', $postDate);

                $events[$day][] = [
                    'type'      => $post->post_type,
                    'dateStart' => $postDate,
                    'allDay'    => false,
                    'link'      => $post->post_status == 'publish' ? FitcoHelper::getPermalink($post->ID) : '',
                    'id'        => $post->ID,
                    'uid'       => md5('post:' . $post->ID),
                    'location'  => get_bloginfo('name'),
                    'title'     => $post->post_title,
                    'template'  => '',
                    'color'     => $eventsSettings[$post->post_type]['color'],
                    'text'      => $text
                ];
            }
        }

        // get Gesundheitstags
        if (
            isset($eventsSettings['prefDay'])
            && $eventsSettings['prefDay']['enabled']
            && $consumer->getDynamicValue('prefDay') > 0
            && LS()->isModuleActive('Gesundheitstag')
        ) {
            for ($day = $calendar['firstRangeDay']; $day <= $calendar['lastRangeDay']; $day += DAY_IN_SECONDS) {

                /** @var consumersLoyaltySuite $consumers */
                $consumers = LS()->getModule('consumers');
                $accountLink = $consumers->getAccountPageLink();
                $dayNumber = (int) date('N', $day);

                if ($dayNumber === $consumer->getDynamicValue('prefDay', 0, true)) {
                    $events[$day][] = [
                        'type'      => 'prefDay',
                        'dateStart' => $day,
                        'allDay'    => true,
                        'id'        => 'prefDay',
                        'uid'       => md5('prefDay:' . $day),
                        'location'  => get_bloginfo('name'),
                        'link'      => $accountLink . '#profileHealthday',
                        'title'     => __('DEIN GESUNDHEITSTAG', 'ls'),
                        'color'     => $eventsSettings['prefDay']['color'],
                        'text'      => $eventsSettings['prefDay']['text']
                    ];
                }
            }
        }

        // get AT challenges
        if (
            isset($eventsSettings['challenge'])
            && $eventsSettings['challenge']['enabled']
            && LS()->isModuleActive('at2')
        ) {

            $extraChallenges = [];
            $addExtras = static function ($data, $ch, $stage) use ($extraChallenges) {

                if (!in_array($ch->challengeId, $extraChallenges)) {

                    $link2Challenge = FitcoHelper::getPermalink($ch->challengePageId);
                    $scheme = parse_url($link2Challenge, PHP_URL_SCHEME);
                    $link = empty($scheme) ? home_url($link2Challenge) : $link2Challenge;

                    $data = array_merge($data, [
                        'type'      => 'challenge',
                        'dateStart' => strtotime($ch->dateStart),
                        'dateEnd'   => strtotime($ch->dateEnd),
                        'allDay'    => true,
                        'link'      => $link,
                        'id'        => $ch->challengeId,
                        'uid'       => md5('AT:' . $ch->challengeId),
                        'location'  => get_bloginfo('name'),
                        'title'     => $ch->title,
                        'stage'     => $stage
                    ]);

                    $extraChallenges[] = $ch->challengeId;
                }

                return $data;
            };

            $query = sprintf("SELECT challengeId, title, dateStart, dateEnd, challengePageId 
                              FROM {$wpdb->prefix}ls_at_challenges
                              WHERE dateStart BETWEEN '%s' AND '%s' 
                                 OR dateEnd BETWEEN '%s' AND '%s'"
                , date('Y-m-d', $calendar['firstRangeDay']), date('Y-m-d', $calendar['lastRangeDay']), date('Y-m-d', $calendar['firstRangeDay']), date('Y-m-d', $calendar['lastRangeDay']));

            foreach ($wpdb->get_results($query) as $ch) {

                $dateStart = strtotime($ch->dateStart);
                $dateEnd = strtotime($ch->dateEnd);

                if ($dateStart >= $calendar['firstRangeDay'] && $dateStart <= $calendar['lastRangeDay']) {

                    $data = [
                        'color' => $eventsSettings['challenge']['color'],
                        'text'  => str_replace('[challenge-name]', $ch->title, $eventsSettings['challenge']['text'])
                    ];

                    $events[$dateStart][] = $addExtras($data, $ch, 'start');
                }

                if ($dateEnd >= $calendar['firstRangeDay'] && $dateEnd <= $calendar['lastRangeDay']) {

                    $data = [
                        'color' => $eventsSettings['challenge']['color'],
                        'text'  => str_replace('[challenge-name]', $ch->title, $eventsSettings['challenge']['text2'])
                    ];

                    $events[$dateEnd][] = $addExtras($data, $ch, 'end');
                }
            }
        }

        // get 1:1 challenges
        if (
            isset($eventsSettings['pech'])
            && $eventsSettings['pech']['enabled']
            && LS()->isModuleActive('PersonalChallenge')
        ) {

            /**
             * @param string $text
             * @param PechConsumerTeam $pcc
             * @return string
             */
            $formatText = static function ($text, $pcc) {

                if (strpos($text, '[pech-name]') !== false) {
                    $text = str_replace('[pech-name]', $pcc->getChallenge()->getPostTitle(), $text);
                }

                return $text;
            };

            /**
             * @param array $data
             * @param PechConsumerTeam $myc
             * @param string $stage
             * @return array
             */
            $addExtras = static function ($data, PechConsumerTeam $myc, $stage) {

                $data = array_merge($data, [
                    'type'      => 'pech',
                    'link'      => FitcoHelper::getPermalink($myc->getChallengeId()),
                    'id'        => $myc->getChallengeId(),
                    'uid'       => md5('PECH2:' . $myc->getParticipationId()),
                    'dateStart' => $myc->getTeam()->getStartDate(),
                    'dateEnd'   => $myc->getChallengeTeam()->getTeamEndDate(),
                    'allDay'    => true,
                    'location'  => get_bloginfo('name'),
                    'title'     => get_the_title($myc->getChallengeId()),
                    'stage'     => $stage
                ]);

                return $data;
            };

            /** @var PersonalChallenge $pechModel */
            $pechModel = LS()->getModule('PersonalChallenge');

            foreach ($pechModel->getMyTeams() as $pcc) {

                $inRange = false;
                $start = $calendar['firstRangeDay'];
                $end = $calendar['lastRangeDay'];
                $pccStartDate = $pcc->getTeam()->getStartDate();
                $pccEndDate = $pcc->getChallengeTeam()->getTeamEndDate();

                if ($pccStartDate >= $calendar['firstRangeDay'] && $pccStartDate <= $calendar['lastRangeDay']) {

                    $inRange = true;
                    $start = $pccStartDate + DAY_IN_SECONDS;
                    $data = [
                        'color' => $eventsSettings['pech']['color'],
                        'text'  => $formatText($eventsSettings['pech']['text'], $pcc)
                    ];

                    $events[$pccStartDate][] = $addExtras($data, $pcc, 'start');
                }

                if ($pccEndDate >= $calendar['firstRangeDay'] && $pccEndDate <= $calendar['lastRangeDay']) {

                    $inRange = true;
                    $end = $pccEndDate - DAY_IN_SECONDS;
                    $data = [
                        'color' => $eventsSettings['pech']['color'],
                        'text'  => $formatText($eventsSettings['pech']['text3'], $pcc)
                    ];

                    $events[$pccEndDate][] = $addExtras($data, $pcc, 'end');
                }

                if ($inRange) {
                    for ($i = $start; $i <= $end; $i += DAY_IN_SECONDS) {

                        $data = [
                            'color'   => $eventsSettings['pech']['color'],
                            'text'    => $formatText($eventsSettings['pech']['text2'], $pcc),
                            'opacity' => '0.5'
                        ];

                        $events[$i][] = $addExtras($data, $pcc, 'ongoing');
                    }
                }
            }
        }

        // get course post
        if (
            isset($eventsSettings['course'])
            && $eventsSettings['course']['enabled']
            && LS()->isModuleActive('course')
        ) {

            /** @var courseLoyaltySuite $courseModule */
            $courseModule = LS()->getModule('course');

            foreach (LSCourseHelper::getConsumerConnections($consumer->getConsumerId()) as $connection) {

                $course = LSCourseHelper::getCourse($connection->getCourseId());
                
                if ($course && $course->isActual()) {

                    foreach ($courseModule->getCoursePostsForConsumer($course) as $consumerLesson) {

                        $lessonDate = $consumerLesson->getAccessibleDate();

                        if ($lessonDate >= $calendar['firstRangeDay'] && $lessonDate <= $calendar['lastRangeDay']) {

                            $text = $eventsSettings['course']['text'];

                            if (strpos($text, '[course-name]') !== false) {
                                $text = str_replace('[course-name]', $course->getCourseName(), $text);
                            }

                            if (strpos($text, '[course-article]') !== false) {
                                $text = str_replace('[course-article]', get_the_title($consumerLesson->getPostId()), $text);
                            }

                            $events[$lessonDate][] = [
                                'type'      => 'course',
                                'dateStart' => $lessonDate,
                                'allDay'    => true,
                                'link'      => FitcoHelper::getPermalink($consumerLesson->getPostId()),
                                'id'        => $consumerLesson->getCourseId(),
                                'uid'       => md5('course:' . $course->getCourseId() . ':' . $consumerLesson->getPostId()),
                                'location'  => get_bloginfo('name'),
                                'title'     => $course->getCourseName(),
                                'color'     => $eventsSettings['course']['color'],
                                'text'      => $text,
                                'inbox'     => $consumerLesson->isAccessible() && !$consumerLesson->isRead()
                            ];
                        }
                    }
                }
            }
        }

        // get Timelines
        if (
            isset($eventsSettings['timeline'])
            && $eventsSettings['timeline']['enabled']
            && LS()->isModuleActive('timeline')
        ) {

            /**
             * @param array $data
             * @param WP_Post $timeline
             * @return array
             */
            $addExtras = static function ($data, WP_Post $timeline) {

                /** @var timelineLoyaltySuite $timelineModule */
                $timelineModule = LS()->getModule('timeline');

                $data = array_merge($data, [
                    'type'      => 'timeline',
                    'link'      => FitcoHelper::getPermalink($timeline),
                    'id'        => $timeline->ID,
                    'uid'       => md5('TIMELINE:' . $timeline->ID),
                    'dateStart' => $timelineModule->getTimelineEventDate($timeline->ID),
                    'allDay'    => true,
                    'location'  => get_bloginfo('name'),
                    'title'     => get_the_title($timeline)
                ]);

                return $data;
            };

            /** @var timelineLoyaltySuite $timelineModule */
            $timelineModule = LS()->getModule('timeline');

            foreach ($timelineModule->getTimelines(true) as $timeline) {

                $ted = $timelineModule->getTimelineEventDate($timeline->ID);

                if ($ted >= $calendar['firstRangeDay'] && $ted <= $calendar['lastRangeDay']) {

                    $data = [
                        'color' => $eventsSettings['timeline']['color'],
                        'text'  => str_replace('[timeline-name]', get_the_title($timeline), $eventsSettings['timeline']['text'])
                    ];

                    $events[$ted][] = $addExtras($data, $timeline);
                }

                for ($d = $timelineModule->getTimelineCountdownStartDate($timeline->ID); $d < $ted; $d += DAY_IN_SECONDS) {
                    if ($d >= $calendar['firstRangeDay'] && $d <= $calendar['lastRangeDay']) {

                        $data = [
                            'color'   => $eventsSettings['timeline']['color'],
                            'text'    => str_replace('[timeline-name]', get_the_title($timeline), $eventsSettings['timeline']['text2']),
                            'opacity' => '0.5'
                        ];

                        $events[$d][] = $addExtras($data, $timeline);
                    }
                }
            }
        }

        $removeDuplicateKeys = static function ($key, $data) {
            $_data = [];
            foreach ($data as $v) {
                if (isset($_data[$v[$key]])) {
                    continue;
                }
                $_data[$v[$key]] = $v;
            }

            return $_data;
        };

        foreach ($events as $day => $evs) {
            $events[$day] = $removeDuplicateKeys('text', $evs);
        }

        return $events;
    }
}