<?php
class calendarAdminLoyaltySuite extends calendarLoyaltySuite implements \LS\ModuleAdminInterface
{
    /**
     * @internal
     */
    public function load()
    {
        parent::load();

        add_action('ls_fitco_update_settings', [$this, 'saveSettings']);
        add_action('ls_fitco_settings_tab', [$this, 'settingsTab']);
        add_action('ls_fitco_settings_content', [$this, 'settingsContent']);
    }

    /**
     * @return array
     */
    public function getModuleSubPages()
    {
        return [
            (object) [
                'href'    => 'admin.php?page=fitcoLoyaltySuite#ls-settings/calendar-settings',
                'label'   => 'Einstellungen',
                'referer' => false
            ]
        ];
    }

    /**
     * @internal
     */
    public function settingsTab()
    {
        ?>
        <li class="ls-tab"><a href="#ls-settings/calendar-settings"><?=$this->getTitle()?></a></li><?php
    }

    /**
     * @internal
     */
    public function settingsContent()
    {
        (new LS\Template(__DIR__ . '/'))
            ->assign('settings', $this->getSettings())
            ->render('admin-settings.phtml');
    }

    /**
     * @internal
     */
    public function saveSettings()
    {
        if (!isset($_POST['calendar']) || !is_array($_POST['calendar'])) {
            return;
        }

        $settings = stripslashes_deep($_POST['calendar']);
        $settings = [
            'events' => $settings['events'] ?? []
        ];

        foreach ($settings['events'] as $key => $event) {
            $settings['events'][$key] = [
                'enabled' => isset($event['enabled']) && $event['enabled'] === 'on',
                'color'   => $event['color'] ?? '',
                'text'    => isset($event['text']) ? trim($event['text']) : '',
                'text2'   => isset($event['text2']) ? trim($event['text2']) : '',
                'text3'   => isset($event['text3']) ? trim($event['text3']) : ''
            ];
        }

        $this->setOptions(array_merge($this->getOptions(), $settings));
    }

    /**
     * @return array
     */
    public function getSettings()
    {
        $options = $this->getOptions();

        $options['events'] = $options['events'] ?? [];

        if (isset($_POST['calendar'])) {

            $calendarOptions = stripslashes_deep($_POST['calendar']);

            if (isset($calendarOptions['events']) && is_array($calendarOptions['events'])) {
                foreach ($calendarOptions['events'] as $key => $event) {
                    $options['events'][$key] = [
                        'enabled' => isset($event['enabled']) ? $event['enabled'] === 'on' : $options['events'][$key]['enabled'],
                        'color'   => $event['color'] ?? $options['events'][$key]['color'],
                        'text'    => isset($event['text']) ? trim($event['text']) : $options['events'][$key]['text'],
                        'text2'   => isset($event['text2']) ? trim($event['text2']) : $options['events'][$key]['text2'],
                        'text3'   => isset($event['text3']) ? trim($event['text3']) : $options['events'][$key]['text3']
                    ];
                }
            }
        }

        return $options;
    }
}